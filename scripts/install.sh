#!/usr/bin/env sh

echo $PUB_CACHE
flutter pub get
flutter pub global activate intl_utils 2.8.1
flutter --no-color pub global run intl_utils:generate
flutter pub run build_runner build --delete-conflicting-outputs
dart format -l 200 .
