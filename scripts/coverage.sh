#!/usr/bin/env sh
set -e

lcov -r coverage/lcov.info '*/__test*__/*' --exclude \
lib/generated/* \
lib/generated/intl/* \
lib/core/db_manager.dart \
lib/core/syntax_highlighter.dart \
lib/core/file_uploader.dart \
lib/*/*_repository.dart \
lib/core/log_helper.dart \
lib/core/markdown_view.dart \
lib/core/plugins/install_apk_plugin.dart \
lib/core/layout/adaptive.dart \
lib/core/uri_launcher.dart \
lib/core/net/interceptor/refresh_token_interceptor.dart \
lib/core/widgets/photo_picker.dart \
lib/core/widgets/video_player_page.dart \
lib/core/domain/email.dart \
lib/core/domain/low_permission_information.dart \
lib/core/domain/url_redictor.dart \
lib/core/native_text_field.dart \
-o coverage/lcov_cleaned.info

genhtml coverage/lcov_cleaned.info --output=coverage
