# 极狐 GitLab App

[![流水线状态](https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/badges/main/pipeline.svg)](https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/commits/main)
[![覆盖率报告](https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/badges/main/coverage.svg)](https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/commits/main)

极狐 GitLab App 是官方开源客户端，用于管理您在 SaaS 或私有化实例上的项目。

支持手机和平板，而且为平板单独设计。

<a href='https://apps.apple.com/cn/app/jihu-gitlab/id6444783184' style="display: inline-block; overflow: hidden; border-radius: 13px; height: 60px;"><img alt='下载应用，请到 Apple App Store' src='public/assets/ios-badge.svg' style="border-radius: 13px; height: 60px;"/></a>

<a href='https://play.google.com/store/apps/details?id=cn.gitlab.app&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1' style="display: inline-block; overflow: hidden; border-radius: 13px; height: 60px;"><img alt='下载应用，请到 Google Play' src='https://play.google.com/intl/en_us/badges/static/images/badges/zh-cn_badge_web_generic.png' style="border-radius: 13px; height: 60px;"/></a>

<a href='https://jihulab.com/api/v4/projects/59893/packages/generic/jihu-gitlab-app/latest/app-debug.apk' style="display: inline-block; overflow: hidden; border-radius: 13px; height: 60px;"><img alt='本地下载安卓安装包' src='public/assets/apk-badge.png' style="border-radius: 13px; height: 60px;"/></a>

## 技术栈

- 框架：flutter
- 代码规范：flutter_lints
- 单元测试：flutter_test/mockito
- 持续集成：JiHu GitLab CI + Xcode Cloud

## Scrum

- 敏捷教练：@wanyouzhu
- 敏捷产品负责人：@sinkcup
- 业务分析师：@jojo0
- 开发团队： @NeilWang, @zhangling, @perity, @raymond-liao
- 迭代计划会：周一 08:30-12:00 [腾讯会议：693-7024-4737](https://meeting.tencent.com/dm/X1Ts1BYHYKVp)
- 每日站会：每个工作日 08:30-08:45 会议号同上
- 迭代评审会：周五 15:00-15:30 (
  GMT+8) [腾讯会议：605-3653-8234](https://meeting.tencent.com/dm/NfQ9fItRf4iM)
- 迭代回顾会：周五 15:30-17:30 (
  GMT+8) [腾讯会议：608-1463-9542](https://meeting.tencent.com/dm/VUQKH98EmZyT)
