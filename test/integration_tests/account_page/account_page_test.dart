import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/widgets/account/account_page.dart';
import 'package:jihu_gitlab_app/modules/auth/auth.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

import '../../core/net/http_request_test.mocks.dart';
import '../../mocker/tester.dart';
import '../../test_data/user.dart';

void main() {
  var client = MockHttpClient();
  setUp(() async {
    ConnectionProvider().fullReset();
    when(client.getWithHeader<Map<String, dynamic>>("https://jihulab.com/api/v4/user", any)).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
    when(client.getWithHeader<Map<String, dynamic>>("https://example.com/api/v4/user", any)).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
  });

  testWidgets('Should display account page as expect', (tester) async {
    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(
        onGenerateRoute: onGenerateRoute,
        home: const Scaffold(body: AccountPage()),
      ),
    ));
    await tester.pumpAndSettle();
    expect(find.byKey(const Key('AddAccountButton')), findsOneWidget);
    await tester.tap(find.byKey(const Key('AddAccountButton')));
    await tester.pumpAndSettle();
    expect(find.text('Self-Managed'), findsOneWidget);
    expect(find.text('jihulab.com'), findsOneWidget);
    expect(find.text('gitlab.com'), findsOneWidget);
    await tester.tap(find.text('jihulab.com'));
    await tester.pumpAndSettle();
    expect(find.byType(AccountPage), findsNothing);
    expect(find.byType(LoginPage), findsOneWidget);
  });

  testWidgets('Should display account page when connected jihulab.com', (tester) async {
    ConnectionProvider().reset(Tester.jihuLabUser());
    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: const MaterialApp(
        home: Scaffold(body: AccountPage()),
      ),
    ));
    await tester.pumpAndSettle();
    expect(find.text('@jihulab_tester'), findsOneWidget);
    await tester.tap(find.byKey(const Key('AddAccountButton')));
    await tester.pumpAndSettle();
    expect(find.text('Self-Managed'), findsOneWidget);
    expect(find.text('jihulab.com'), findsNWidgets(2));
    expect(find.text('gitlab.com'), findsOneWidget);
    expect(find.text('Already used'), findsOneWidget);
    await tester.tap(find.text('Already used'));
    await tester.pumpAndSettle(const Duration(seconds: 2));
    expect(find.byType(AccountPage), findsOneWidget);
  });

  testWidgets('Should display account page when connected self-managed', (tester) async {
    ConnectionProvider().reset(Tester.selfManagedUser());
    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: const MaterialApp(
        home: Scaffold(body: AccountPage()),
      ),
    ));
    await tester.pumpAndSettle();
    expect(find.text('@tester'), findsOneWidget);
    await tester.tap(find.byKey(const Key('AddAccountButton')));
    await tester.pumpAndSettle();
    expect(find.text('Self-Managed'), findsNWidgets(2));
    expect(find.text('jihulab.com'), findsOneWidget);
    expect(find.text('gitlab.com'), findsOneWidget);
    expect(find.text('Already used'), findsOneWidget);
    await tester.tap(find.text('Already used'));
    await tester.pumpAndSettle(const Duration(seconds: 2));
    expect(find.byType(AccountPage), findsOneWidget);
  });

  testWidgets('Should not connect account when connected self-managed and tap to connect self-managed', (tester) async {
    ConnectionProvider().reset(Tester.selfManagedUser());
    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: const MaterialApp(
        home: Scaffold(body: AccountPage()),
      ),
    ));
    await tester.pumpAndSettle();
    expect(find.text('@tester'), findsOneWidget);
    await tester.tap(find.byKey(const Key('AddAccountButton')));
    await tester.pumpAndSettle();
    await tester.tap(find.text('Already used'));
    await tester.pumpAndSettle(const Duration(seconds: 2));
    expect(find.text('@tester'), findsOneWidget);
  });

  testWidgets('Should disconnect account', (tester) async {
    ConnectionProvider().reset(Tester.gitLabUser());
    ConnectionProvider().injectConnectionForTest(Tester.jihuLabUser());
    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: const MaterialApp(
        home: Scaffold(body: AccountPage()),
      ),
    ));
    await tester.pumpAndSettle();
    await tester.drag(find.text('jihulab.com'), const Offset(-300, 0));
    await tester.pump();
    expect(find.byIcon(Icons.exit_to_app), findsOneWidget);
    await tester.tap(find.byIcon(Icons.exit_to_app));
    await tester.pumpAndSettle();
    expect(find.text('jihulab.com'), findsNothing);
    await tester.drag(find.text('gitlab.com'), const Offset(-300, 0));
    await tester.pump();
    expect(find.byIcon(Icons.exit_to_app), findsOneWidget);
    await tester.tap(find.byIcon(Icons.exit_to_app));
    await tester.pumpAndSettle();
    expect(find.text('gitlab.com'), findsNothing);
  });

  testWidgets('Should tap to checkout account', (tester) async {
    ConnectionProvider().reset(Tester.gitLabUser());
    ConnectionProvider().injectConnectionForTest(Tester.jihuLabUser());
    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: const MaterialApp(
        home: Scaffold(body: AccountPage()),
      ),
    ));
    await tester.pumpAndSettle();
    expect(find.text('@jihulab_tester'), findsNWidgets(1));
    expect(find.text('@gitlab_tester'), findsNWidgets(1));
    expect(ConnectionProvider.currentBaseUrl.get, 'https://jihulab.com');
    await tester.tap(find.text('gitlab.com'));
    await tester.pumpAndSettle();
    expect(ConnectionProvider.currentBaseUrl.get, 'https://gitlab.com');
  });

  testWidgets('Should tap add account with to goto another page', (tester) async {
    ConnectionProvider().reset(Tester.gitLabUser());
    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(
        onGenerateRoute: onGenerateRoute,
        home: const Scaffold(body: AccountPage()),
      ),
    ));
    await tester.pumpAndSettle();
    await tester.tap(find.byKey(const Key('AddAccountButton')));
    await tester.pumpAndSettle();
    await tester.tap(find.widgetWithText(MenuItemButton, 'Self-Managed'));
    await tester.pumpAndSettle();
    expect(find.byType(SelfManagedLoginPage), findsOneWidget);
    expect(find.byType(AccountPage), findsNothing);
  });

  tearDown(() {
    ConnectionProvider().fullReset();
    ConnectionProvider().clearActive();
    ConnectionProvider().loggedOut(byUser: true);
  });
}
