import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/db_manager.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart' as r;
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/main.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/project_repository_model.dart';
import 'package:jihu_gitlab_app/modules/root/pad_group_and_project_list_tile.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../core/net/http_request_test.mocks.dart';
import '../mocker/tester.dart';
import '../modules/ai/ai_page_test.mocks.dart';
import '../modules/community/community_page_test.dart';
import '../modules/projects/repository/response_data.dart';
import '../test_binding_setter.dart';
import '../test_data/project.dart';

void main() {
  late MockHttpClient client;
  late MockDatabase mockDatabase;

  setUp(() async {
    setupLocator();
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    LocalStorage.save('privacy-policy-agreed', true);
    await ConnectionProvider().restore();
    ConnectionProvider().reset(Tester.jihuLabUser());
    client = MockHttpClient();
    mockDatabase = MockDatabase();
    DbManager.instance().injectDatabaseForTesting(mockDatabase);
    ConnectionProvider().reset(Tester.jihuLabUser());
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets('Should be able to switch the starred project on pad', (tester) async {
    setUpDesktopBinding(tester);

    when(client.postWithConnection('/api/graphql', getCommunityTypesGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/faq"), any))
        .thenAnswer((_) => Future(() => r.Response.of(communityTypesResponse)));
    when(client.get("/api/v4/users/9527/starred_projects?page=1&per_page=50")).thenAnswer((_) => Future(() => r.Response.of([])));
    when(client.get("/api/v4/groups?top_level_only=true&page=1&per_page=50")).thenAnswer((_) => Future(() => r.Response.of([])));
    when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>([])));
    when(client.post("/api/graphql", buildRequestBodyOfCheckRepositoryIsEmpty("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app")))
        .thenAnswer((_) => Future(() => r.Response.of(checkRepositoryIsEmptyResponse(empty: true))));
    when(client.post("/api/graphql", buildRequestBodyOfCheckRepositoryIsEmpty("ultimate-plan/jihu-gitlab-app/demo")))
        .thenAnswer((_) => Future(() => r.Response.of(checkRepositoryIsEmptyResponse(empty: true))));

    when(mockDatabase.rawQuery("select * from groups_and_projects where starred = 1 and user_id = 9527 order by starred_at desc")).thenAnswer((_) => Future(() => [
          {
            "id": 1053,
            "user_id": 29758,
            "iid": 59893,
            "name": "极狐 GitLab App",
            "type": "project",
            "relative_path": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
            "parent_id": 88966,
            "version": 1679889316813,
            "starred": 1,
            "last_activity_at": 1679617295925,
            "starred_at": 1679617295925
          },
          {
            "id": 1051,
            "user_id": 29758,
            "iid": 72936,
            "name": "private demo",
            "type": "project",
            "relative_path": "ultimate-plan/jihu-gitlab-app/demo",
            "parent_id": 88966,
            "version": 1679889316813,
            "starred": 1,
            "last_activity_at": 1679582042095,
            "starred_at": 1679582042095
          }
        ]));

    when(client.get<List<dynamic>>("/api/v4/projects/72936/issues/6/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>([])));
    when(client.get<List<dynamic>>("/api/v4/projects/72936/issues/1/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>([])));
    when(client.get<List<dynamic>>("/api/v4/projects/72936/issues/2/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>([])));
    when(client.get<List<dynamic>>("/api/v4/projects/59893/issues/0/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>([])));
    when(client.get<List<dynamic>>("/api/v4/projects/0/issues/0/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>([])));
    when(client.get<Map<String, dynamic>>("/api/v4/projects/72936")).thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>(projectData)));
    await tester.pumpWidget(const App());
    await tester.pumpAndSettle(const Duration(seconds: 1));

    expect(find.widgetWithText(ListTile, "Community"), findsOneWidget);
    expect(find.widgetWithText(ListTile, "To-dos"), findsOneWidget);
    expect(find.widgetWithText(ListTile, "To-dos"), findsOneWidget);
    expect(find.widgetWithText(PadGroupAndProjectListTile, "极狐 GitLab App"), findsOneWidget);
    expect(find.widgetWithText(PadGroupAndProjectListTile, "private demo"), findsOneWidget);

    await tester.tap(find.widgetWithText(PadGroupAndProjectListTile, "极狐 GitLab App"));
    await tester.pumpAndSettle();
    expect(find.widgetWithText(CommonAppBar, "极狐 GitLab App"), findsOneWidget);
    expect(find.widgetWithText(Tab, "Repository"), findsOneWidget);
    expect(find.widgetWithText(Tab, "Issues"), findsOneWidget);
    expect(find.widgetWithText(Tab, "MR"), findsOneWidget);
    expect(find.widgetWithText(Tab, "Iterations"), findsOneWidget);
    verify(client.post("/api/graphql", buildRequestBodyOfCheckRepositoryIsEmpty("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app"))).called(1);

    await tester.tap(find.widgetWithText(PadGroupAndProjectListTile, "private demo"));
    await tester.pumpAndSettle();
    expect(find.widgetWithText(CommonAppBar, "private demo"), findsOneWidget);
    expect(find.widgetWithText(Tab, "Repository"), findsOneWidget);
    expect(find.widgetWithText(Tab, "Issues"), findsOneWidget);
    expect(find.widgetWithText(Tab, "MR"), findsOneWidget);
    expect(find.widgetWithText(Tab, "Iterations"), findsOneWidget);
    verify(client.post("/api/graphql", buildRequestBodyOfCheckRepositoryIsEmpty("ultimate-plan/jihu-gitlab-app/demo"))).called(1);
  });

  tearDown(() {
    ConnectionProvider().fullReset();
    unregister();
  });
}
