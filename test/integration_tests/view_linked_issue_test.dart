import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/db_manager.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart' as r;
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/modules/issues/details/issue_details_page.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_model.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../core/net/http_request_test.mocks.dart';
import '../finder/svg_finder.dart';
import '../mocker/tester.dart';
import '../modules/ai/ai_page_test.mocks.dart';
import '../test_binding_setter.dart';
import '../test_data/issue.dart';
import '../test_data/project.dart';

void main() {
  var issueDetailsModel = IssueDetailsModel();
  locator.registerSingleton(issueDetailsModel);
  late MockHttpClient client;
  late MockDatabase mockDatabase;

  setUp(() async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    await ConnectionProvider().restore();
    ConnectionProvider().reset(Tester.jihuLabUser());
    client = MockHttpClient();
    mockDatabase = MockDatabase();
    DbManager.instance().injectDatabaseForTesting(mockDatabase);
    ConnectionProvider().reset(Tester.jihuLabUser());
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets('Should be able to view the linked issue details', (tester) async {
    setUpCompatibleBinding(tester);

    when(mockDatabase.rawQuery("select * from discussions where issue_id = 3242")).thenAnswer((_) => Future(() => [
          {"id": 1, "discussion_id": "11", "issue_id": 3242, "issue_iid": 6, 'path_with_namespace': 'ultimate-plan/jihu-gitlab-app/demo-mr-test', "version": 0, "individual_note": 0}
        ]));
    when(mockDatabase.rawQuery("select * from notes where discussion_id = '11'")).thenAnswer((_) => Future(() => [
          {
            "id": 2,
            "note_id": 22,
            "noteable_id": 222,
            "noteable_iid": 2222,
            "discussion_id": "11",
            "system": 0,
            "body": "#1",
            "assignee_id": 22222,
            "created_at": "2022-11-25T21:05:52.114+08:00",
            "version": 0,
            "internal": 0,
            "author": {"id": 22222, "assignee_id": 22222, "name": "tester", "username": "tester", "avatar_url": "", "version": 0}
          },
          {
            "id": 3,
            "note_id": 33,
            "noteable_id": 333,
            "noteable_iid": 3333,
            "discussion_id": "11",
            "system": 0,
            "body": "#2",
            "assignee_id": 33333,
            "created_at": "2022-11-25T21:05:52.114+08:00",
            "version": 0,
            "internal": 0,
            "author": {"id": 33333, "assignee_id": 33333, "name": "tester", "username": "tester", "avatar_url": "", "version": 0}
          }
        ]));
    when(mockDatabase.query("assignees", where: " assignee_id = ? ", whereArgs: anyNamed("whereArgs"))).thenAnswer((_) => Future(() => [
          {"id": 33333, "assignee_id": 33333, "name": "tester", "username": "tester", "avatar_url": "", "version": 0}
        ]));

    when(client.get<List<dynamic>>("/api/v4/projects/72936/issues/6/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>([])));
    when(client.get<List<dynamic>>("/api/v4/projects/72936/issues/1/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>([])));
    when(client.get<List<dynamic>>("/api/v4/projects/72936/issues/2/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>([])));
    when(client.get<List<dynamic>>("/api/v4/projects/59893/issues/0/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>([])));
    when(client.get<Map<String, dynamic>>("/api/v4/projects/72936")).thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>(projectData)));

    when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 6)))
        .thenAnswer((_) => Future(() => r.Response.of<dynamic>(getGraphQLResponse("关联议题#0"))));
    when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 0)))
        .thenThrow(DioError(requestOptions: RequestOptions(path: ''), response: Response(requestOptions: RequestOptions(path: ''), statusCode: 404)));
    when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 1)))
        .thenAnswer((_) => Future(() => r.Response.of<dynamic>(getGraphQLResponse("来自议题评论"))));
    when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 2)))
        .thenAnswer((_) => Future(() => r.Response.of<dynamic>(getGraphQLResponse("来自议题评论回复"))));
    when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 1), useActiveConnection: true))
        .thenAnswer((_) => Future(() => r.Response.of<dynamic>(issuePostVotesGraphQLResponse)));
    when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 2), useActiveConnection: true))
        .thenAnswer((_) => Future(() => r.Response.of<dynamic>(issuePostVotesGraphQLResponse)));
    when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 0), useActiveConnection: true))
        .thenAnswer((_) => Future(() => r.Response.of<dynamic>(issuePostVotesGraphQLResponse)));
    when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 6), useActiveConnection: true))
        .thenAnswer((_) => Future(() => r.Response.of<dynamic>(issuePostVotesGraphQLResponse)));
    when(client.get("/api/v4/projects/72936/issues/0/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => r.Response.of([])));
    when(client.get("/api/v4/projects/59893/issues/0/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => r.Response.of([])));
    when(client.get("/api/v4/projects/72936/issues/1/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => r.Response.of([])));
    when(client.get("/api/v4/projects/72936/issues/2/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => r.Response.of([])));
    when(client.get("/api/v4/projects/72936/issues/6/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => r.Response.of([])));

    when(client.get<Map<String, dynamic>>("/api/v4/projects/59893"))
        .thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>({"path_with_namespace": "ultimate-plan/jihu-gitlab-app/demo-mr-test"})));
    when(client.get<Map<String, dynamic>>("/api/v4/projects/72936"))
        .thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>({"path_with_namespace": "ultimate-plan/jihu-gitlab-app/demo-mr-test"})));
    when(client.get<List<dynamic>>("/api/v4/projects/72936/members/all?page=1&per_page=50")).thenAnswer((_) async => Future.value(r.Response.of([])));
    HttpClient.injectInstanceForTesting(client);

    var parameters = {'projectId': 72936, 'issueId': 3242, 'issueIid': 6, 'targetId': 3242, 'targetIid': 6, 'pathWithNamespace': 'ultimate-plan/jihu-gitlab-app/demo-mr-test'};
    await tester.pumpWidget(MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
        child: MaterialApp(
          onGenerateRoute: onGenerateRoute,
          home: Scaffold(
            body: IssueDetailsPage(arguments: parameters),
          ),
          localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
        )));

    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.textContaining("#0"), findsOneWidget);
    expect(find.textContaining("#1"), findsOneWidget);
    expect(find.textContaining("#2"), findsOneWidget);

    // click '#0'
    await tester.tap(find.textContaining("#0"));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.byType(BackButton), findsOneWidget);
    await tester.tap(find.byType(BackButton));
    await tester.pumpAndSettle();
    await tester.drag(find.text("Assignees"), const Offset(0, -400));
    await tester.pumpAndSettle();

    // click '#1'
    await tester.tap(find.textContaining("#1"));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(SvgFinder("assets/images/not_found.svg"), findsNothing);
    expect(find.text("来自议题评论"), findsOneWidget);
    expect(find.byType(BackButton), findsOneWidget);
    await tester.tap(find.byType(BackButton));
    await tester.pumpAndSettle();

    // click '#2'
    await tester.tap(find.textContaining("#2"));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(SvgFinder("assets/images/not_found.svg"), findsNothing);
    expect(find.text("来自议题评论回复"), findsOneWidget);
    expect(find.byType(BackButton), findsOneWidget);
    await tester.tap(find.byType(BackButton));
    await tester.pumpAndSettle();
  });

  tearDown(() {
    ConnectionProvider().fullReset();
    ProjectProvider().fullReset();
  });
}

Map<String, dynamic> getGraphQLResponse(String description) {
  return {
    "data": {
      "project": {
        "id": "gid://gitlab/Project/59893",
        "name": "demo mr test",
        "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab App",
        "path": "demo-mr-test",
        "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
        "issue": {
          "id": "gid://gitlab/Issue/323380",
          "iid": "640",
          "projectId": 59893,
          "title": "回复评论",
          "description": description,
          "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/issues/640",
          "confidential": true,
          "state": "opened",
          "createdAt": "2022-11-25T14:19:30.461+08:00",
          "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "miaolu", "username": "perity"},
          "assignees": {
            "nodes": [
              {"id": "gid://gitlab/User/30192", "avatarUrl": "/uploads/-/system/user/avatar/30192/avatar.png", "name": "AssigneeName", "username": "assignee-username"}
            ]
          },
          "labels": {
            "nodes": [
              {"id": "gid://gitlab/ProjectLabel/45575", "title": "P::M", "description": "Must Have", "color": "#6699cc", "textColor": "#FFFFFF"}
            ]
          }
        }
      }
    }
  };
}
