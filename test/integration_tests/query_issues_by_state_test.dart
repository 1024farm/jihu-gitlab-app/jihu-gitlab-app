import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart' as r;
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/project_repository_model.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_file_content_fetcher.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_tree_data_fetcher.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../core/net/http_request_test.mocks.dart';
import '../mocker/tester.dart';
import '../test_binding_setter.dart';

void main() {
  var issueDetailsModel = IssueDetailsModel();
  locator.registerSingleton(issueDetailsModel);
  late MockHttpClient client;

  setUp(() async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    await ConnectionProvider().restore();
    ConnectionProvider().reset(Tester.jihuLabUser());
    client = MockHttpClient();
    ConnectionProvider().reset(Tester.jihuLabUser());
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets('Should be able to link issue in issue description', (tester) async {
    setUpMobileBinding(tester);
    when(client.post<dynamic>('/api/graphql', argThat(predicate((arg) => arg.toString().contains("opened")))))
        .thenAnswer((_) => Future(() => r.Response.of<dynamic>(buildResponse(projectOpenIssues))));
    when(client.post<dynamic>('/api/graphql', argThat(predicate((arg) => arg.toString().contains("closed")))))
        .thenAnswer((_) => Future(() => r.Response.of<dynamic>(buildResponse(projectClosedIssues))));
    when(client.post<dynamic>('/api/graphql', argThat(predicate((arg) => arg.toString().contains("all"))))).thenAnswer((_) => Future(() => r.Response.of<dynamic>(buildResponse(projectAllIssues))));
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfRepositoryTree("demo", '', ref: "main"))).thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>({})));
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfCheckRepositoryIsEmpty('demo'))).thenAnswer((_) => Future(() => r.Response.of<dynamic>(checkRepositoryIsEmptyResponse)));
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfFetchFileContent('demo', "README.md", "main"))).thenAnswer((_) => Future(() => r.Response.of<dynamic>({})));
    HttpClient.injectInstanceForTesting(client);

    await tester.pumpWidget(MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => ConnectionProvider()),
        ChangeNotifierProvider(create: (context) => ProjectProvider()),
      ],
      child: MaterialApp(
        onGenerateRoute: onGenerateRoute,
        home: const Scaffold(
          body: ProjectPage(arguments: {"projectId": 72936, "showLeading": true, "relativePath": "demo", "name": "demo"}),
        ),
        localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
      ),
    ));

    await tester.pumpAndSettle();
    expect(find.widgetWithText(Tab, "Issues"), findsOneWidget);
    await tester.tap(find.widgetWithText(Tab, "Issues"));
    await tester.pumpAndSettle();
    await tester.tap(find.byKey(const Key("filter")));
    await tester.pumpAndSettle();
    expect(find.text('Open'), findsOneWidget);
    expect(find.text('Closed'), findsOneWidget);
    expect(find.text('All'), findsOneWidget);
    await tester.tap(find.text('Open'));
    await tester.pumpAndSettle();
    verify(client.post<dynamic>('/api/graphql', argThat(predicate((arg) => arg.toString().contains("opened"))))).called(2);
    await tester.tap(find.text('All'));
    await tester.pumpAndSettle();
    await tester.tap(find.text('All'));
    await tester.pumpAndSettle();
    await tester.tap(find.text('Done'));
    await tester.pumpAndSettle();
    verify(client.post<dynamic>('/api/graphql', argThat(predicate((arg) => arg.toString().contains("all"))))).called(2);
    await tester.tap(find.byKey(const Key("filter")));
    await tester.pumpAndSettle();
    await tester.tap(find.text('Closed'));
    await tester.pumpAndSettle();
    await tester.tap(find.text('Closed'));
    await tester.pumpAndSettle();
    await tester.tap(find.text('Done'));
    await tester.pumpAndSettle();
    verify(client.post<dynamic>('/api/graphql', argThat(predicate((arg) => arg.toString().contains("closed"))))).called(2);
    await tester.tap(find.byKey(const Key("filter")));
    await tester.pumpAndSettle();
    await tester.tap(find.text('Open'));
    await tester.pumpAndSettle();
    await tester.tap(find.text('Open'));
    await tester.pumpAndSettle();
    expect(find.text('Open'), findsOneWidget);
  });

  tearDown(() {
    ConnectionProvider().fullReset();
  });
}

List<Map<String, dynamic>> projectOpenIssues = [
  {
    "id": "gid://gitlab/Issue/307812",
    "iid": "38",
    "state": "opened",
    "projectId": 72936,
    "title": "多张图片的议题",
    "type": "ISSUE",
    "designCollection": {
      "project": {
        "id": "gid://gitlab/Project/72936",
        "path": "demo-mr-test",
        "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
        "name": "demo mr test",
        "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
      }
    },
    "weight": 1,
    "author": {"id": "gid://gitlab/User/1795", "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png", "name": "Zhou YANG", "username": "sinkcup"},
    "createdAt": "2023-01-18T20:42:01+08:00",
    "updatedAt": "2023-02-01T11:33:32+08:00"
  },
  {
    "id": "gid://gitlab/Issue/307572",
    "iid": "37",
    "state": "opened",
    "projectId": 72936,
    "title": "删除",
    "type": "ISSUE",
    "designCollection": {
      "project": {
        "id": "gid://gitlab/Project/72936",
        "path": "demo-mr-test",
        "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
        "name": "demo mr test",
        "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
      }
    },
    "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
    "createdAt": "2023-01-18T11:40:14+08:00",
    "updatedAt": "2023-02-02T11:29:26+08:00"
  },
  {
    "id": "gid://gitlab/Issue/307570",
    "iid": "36",
    "state": "opened",
    "projectId": 72936,
    "title": "关联议题详情",
    "type": "ISSUE",
    "designCollection": {
      "project": {
        "id": "gid://gitlab/Project/72936",
        "path": "demo-mr-test",
        "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
        "name": "demo mr test",
        "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
      }
    },
    "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
    "createdAt": "2023-01-18T11:31:10+08:00",
    "updatedAt": "2023-02-01T11:33:54+08:00"
  },
  {
    "id": "gid://gitlab/Issue/305283",
    "iid": "34",
    "state": "opened",
    "projectId": 72936,
    "title": "video test",
    "type": "ISSUE",
    "designCollection": {
      "project": {
        "id": "gid://gitlab/Project/72936",
        "path": "demo-mr-test",
        "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
        "name": "demo mr test",
        "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
      }
    },
    "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
    "createdAt": "2023-01-13T09:36:56+08:00",
    "updatedAt": "2023-01-31T16:26:13+08:00"
  },
  {
    "id": "gid://gitlab/Issue/301789",
    "iid": "33",
    "state": "opened",
    "projectId": 72936,
    "title": "测试 issue 创建后刷新",
    "type": "ISSUE",
    "designCollection": {
      "project": {
        "id": "gid://gitlab/Project/72936",
        "path": "demo-mr-test",
        "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
        "name": "demo mr test",
        "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
      }
    },
    "author": {"id": "gid://gitlab/User/29758", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png", "name": "ling zhang", "username": "zhangling"},
    "createdAt": "2023-01-06T17:36:59+08:00",
    "updatedAt": "2023-02-02T11:28:19+08:00"
  },
  {
    "id": "gid://gitlab/Issue/301603",
    "iid": "32",
    "state": "opened",
    "projectId": 72936,
    "title": "ghhhgg",
    "type": "ISSUE",
    "designCollection": {
      "project": {
        "id": "gid://gitlab/Project/72936",
        "path": "demo-mr-test",
        "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
        "name": "demo mr test",
        "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
      }
    },
    "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
    "createdAt": "2023-01-06T13:03:17+08:00",
    "updatedAt": "2023-01-06T13:03:17+08:00"
  },
  {
    "id": "gid://gitlab/Issue/301602",
    "iid": "31",
    "state": "opened",
    "projectId": 72936,
    "title": "gghhh",
    "type": "ISSUE",
    "designCollection": {
      "project": {
        "id": "gid://gitlab/Project/72936",
        "path": "demo-mr-test",
        "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
        "name": "demo mr test",
        "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
      }
    },
    "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
    "createdAt": "2023-01-06T13:03:06+08:00",
    "updatedAt": "2023-02-02T11:29:43+08:00"
  },
  {
    "id": "gid://gitlab/Issue/301601",
    "iid": "30",
    "state": "opened",
    "projectId": 72936,
    "title": "chhjh",
    "type": "ISSUE",
    "designCollection": {
      "project": {
        "id": "gid://gitlab/Project/72936",
        "path": "demo-mr-test",
        "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
        "name": "demo mr test",
        "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
      }
    },
    "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
    "createdAt": "2023-01-06T13:02:16+08:00",
    "updatedAt": "2023-01-06T13:02:16+08:00"
  },
  {
    "id": "gid://gitlab/Issue/301600",
    "iid": "29",
    "state": "opened",
    "projectId": 72936,
    "title": "bbbnhhj",
    "type": "ISSUE",
    "designCollection": {
      "project": {
        "id": "gid://gitlab/Project/72936",
        "path": "demo-mr-test",
        "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
        "name": "demo mr test",
        "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
      }
    },
    "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
    "createdAt": "2023-01-06T13:02:05+08:00",
    "updatedAt": "2023-01-06T13:02:05+08:00"
  },
  {
    "id": "gid://gitlab/Issue/301599",
    "iid": "28",
    "state": "opened",
    "projectId": 72936,
    "title": "ghhhhh",
    "type": "ISSUE",
    "designCollection": {
      "project": {
        "id": "gid://gitlab/Project/72936",
        "path": "demo-mr-test",
        "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
        "name": "demo mr test",
        "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
      }
    },
    "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
    "createdAt": "2023-01-06T13:01:22+08:00",
    "updatedAt": "2023-01-06T13:01:22+08:00"
  },
  {
    "id": "gid://gitlab/Issue/300841",
    "iid": "26",
    "state": "opened",
    "projectId": 72936,
    "title": "issue detail test",
    "type": "ISSUE",
    "designCollection": {
      "project": {
        "id": "gid://gitlab/Project/72936",
        "path": "demo-mr-test",
        "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
        "name": "demo mr test",
        "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
      }
    },
    "author": {"id": "gid://gitlab/User/29758", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png", "name": "ling zhang", "username": "zhangling"},
    "createdAt": "2023-01-04T21:23:07+08:00",
    "updatedAt": "2023-01-06T13:22:13+08:00"
  },
  {
    "id": "gid://gitlab/Issue/300803",
    "iid": "25",
    "state": "opened",
    "projectId": 72936,
    "title": "I will do",
    "type": "ISSUE",
    "designCollection": {
      "project": {
        "id": "gid://gitlab/Project/72936",
        "path": "demo-mr-test",
        "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
        "name": "demo mr test",
        "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
      }
    },
    "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
    "createdAt": "2023-01-04T19:42:16+08:00",
    "updatedAt": "2023-01-04T19:42:16+08:00"
  },
  {
    "id": "gid://gitlab/Issue/300511",
    "iid": "24",
    "state": "opened",
    "projectId": 72936,
    "title": "test issue",
    "type": "ISSUE",
    "designCollection": {
      "project": {
        "id": "gid://gitlab/Project/72936",
        "path": "demo-mr-test",
        "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
        "name": "demo mr test",
        "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
      }
    },
    "author": {"id": "gid://gitlab/User/29758", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png", "name": "ling zhang", "username": "zhangling"},
    "createdAt": "2023-01-04T15:02:46+08:00",
    "updatedAt": "2023-01-04T19:23:19+08:00"
  },
  {
    "id": "gid://gitlab/Issue/300131",
    "iid": "23",
    "state": "opened",
    "projectId": 72936,
    "title": "Confidential test",
    "type": "ISSUE",
    "designCollection": {
      "project": {
        "id": "gid://gitlab/Project/72936",
        "path": "demo-mr-test",
        "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
        "name": "demo mr test",
        "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
      }
    },
    "author": {"id": "gid://gitlab/User/29758", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png", "name": "ling zhang", "username": "zhangling"},
    "createdAt": "2023-01-03T16:50:49+08:00",
    "updatedAt": "2023-01-03T16:50:49+08:00"
  },
  {
    "id": "gid://gitlab/Issue/297177",
    "iid": "22",
    "state": "opened",
    "projectId": 72936,
    "title": "保密议题",
    "type": "ISSUE",
    "designCollection": {
      "project": {
        "id": "gid://gitlab/Project/72936",
        "path": "demo-mr-test",
        "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
        "name": "demo mr test",
        "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
      }
    },
    "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
    "createdAt": "2022-12-27T09:59:23+08:00",
    "updatedAt": "2022-12-28T15:22:58+08:00"
  },
  {
    "id": "gid://gitlab/Issue/296910",
    "iid": "21",
    "state": "opened",
    "projectId": 72936,
    "title": "快看看",
    "type": "ISSUE",
    "designCollection": {
      "project": {
        "id": "gid://gitlab/Project/72936",
        "path": "demo-mr-test",
        "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
        "name": "demo mr test",
        "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
      }
    },
    "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
    "createdAt": "2022-12-26T11:26:33+08:00",
    "updatedAt": "2022-12-28T15:36:59+08:00"
  },
  {
    "id": "gid://gitlab/Issue/296032",
    "iid": "20",
    "state": "opened",
    "projectId": 72936,
    "title": "I will do",
    "type": "ISSUE",
    "designCollection": {
      "project": {
        "id": "gid://gitlab/Project/72936",
        "path": "demo-mr-test",
        "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
        "name": "demo mr test",
        "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
      }
    },
    "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
    "createdAt": "2022-12-23T16:34:46+08:00",
    "updatedAt": "2023-01-03T14:53:31+08:00"
  },
  {
    "id": "gid://gitlab/Issue/290824",
    "iid": "19",
    "state": "opened",
    "projectId": 72936,
    "title": "test-mention",
    "type": "ISSUE",
    "designCollection": {
      "project": {
        "id": "gid://gitlab/Project/72936",
        "path": "demo-mr-test",
        "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
        "name": "demo mr test",
        "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
      }
    },
    "author": {"id": "gid://gitlab/User/29758", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png", "name": "ling zhang", "username": "zhangling"},
    "createdAt": "2022-12-13T17:04:24+08:00",
    "updatedAt": "2023-02-03T10:34:58+08:00"
  },
  {
    "id": "gid://gitlab/Issue/289871",
    "iid": "18",
    "state": "opened",
    "projectId": 72936,
    "title": "bb",
    "type": "ISSUE",
    "designCollection": {
      "project": {
        "id": "gid://gitlab/Project/72936",
        "path": "demo-mr-test",
        "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
        "name": "demo mr test",
        "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
      }
    },
    "author": {"id": "gid://gitlab/User/29758", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png", "name": "ling zhang", "username": "zhangling"},
    "createdAt": "2022-12-12T15:34:37+08:00",
    "updatedAt": "2022-12-12T21:42:33+08:00"
  },
  {
    "id": "gid://gitlab/Issue/289863",
    "iid": "17",
    "state": "opened",
    "projectId": 72936,
    "title": "test-multi-assignees",
    "type": "ISSUE",
    "designCollection": {
      "project": {
        "id": "gid://gitlab/Project/72936",
        "path": "demo-mr-test",
        "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
        "name": "demo mr test",
        "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
      }
    },
    "author": {"id": "gid://gitlab/User/29758", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png", "name": "ling zhang", "username": "zhangling"},
    "createdAt": "2022-12-12T15:23:33+08:00",
    "updatedAt": "2022-12-12T15:23:33+08:00"
  }
];

List<Map<String, dynamic>> projectClosedIssues = [
  {
    "id": "gid://gitlab/Issue/311271",
    "iid": "39",
    "state": "closed",
    "projectId": 72936,
    "title": "测试关联议题",
    "type": "ISSUE",
    "designCollection": {
      "project": {
        "id": "gid://gitlab/Project/72936",
        "path": "demo-mr-test",
        "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
        "name": "demo mr test",
        "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
      }
    },
    "author": {"id": "gid://gitlab/User/29758", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png", "name": "ling zhang", "username": "zhangling"},
    "createdAt": "2023-01-31T16:26:12+08:00",
    "updatedAt": "2023-02-03T11:00:56+08:00"
  }
];

List<Map<String, dynamic>> get projectAllIssues {
  List<Map<String, dynamic>> list = List.from(projectClosedIssues);
  list.addAll(projectOpenIssues);
  return list;
}

Map<String, dynamic> buildResponse(List<Map<String, dynamic>> issueNodes) {
  return {
    "data": {
      "project": {
        "id": "gid://gitlab/Project/72936",
        "issues": {
          "pageInfo": {
            "hasNextPage": false,
            "startCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMy0wMS0zMSAxNjoyNjoxMi42MTI5MTcwMDAgKzA4MDAiLCJpZCI6IjMxMTI3MSJ9",
            "endCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMy0wMS0zMSAxNjoyNjoxMi42MTI5MTcwMDAgKzA4MDAiLCJpZCI6IjMxMTI3MSJ9"
          },
          "nodes": issueNodes
        }
      }
    }
  };
}

Map<String, dynamic> checkRepositoryIsEmptyResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "name": "极狐 GitLab App",
      "repository": {"exists": true, "empty": false, "rootRef": "main"}
    }
  }
};
