import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/clipboard.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar_and_name.dart';
import 'package:jihu_gitlab_app/core/widgets/http_fail_view.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_button.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/mr/approval_rule_view.dart';
import 'package:jihu_gitlab_app/modules/mr/changes/mr_changes_page.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_blocked_view.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_ready_view.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_request_page.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_request_tab_bar.dart';
import 'package:jihu_gitlab_app/modules/mr/models/mr_graphql_request_body.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:sticky_and_expandable_list/sticky_and_expandable_list.dart';

import '../../../finder/svg_finder.dart';
import '../../../mocker/tester.dart';
import '../../../test_binding_setter.dart';
import '../../../test_data/merge_request.dart';
import '../../helper/core.dart';
import 'merge_request_page_test.mocks.dart';

@GenerateNiceMocks([MockSpec<Clipboard>()])
Future<void> main() async {
  setUp(() async {
    TestWidgetsFlutterBinding.ensureInitialized();
    ConnectionProvider().reset(Tester.jihuLabUser());
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets('Should display HttpFailView when http request failed', (tester) async {
    when(client.get<Map<String, dynamic>>("/api/v4/projects/72936/merge_requests/17/approvals")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(approvalResponse)));
    when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 17))).thenThrow(Exception());
    when(client.post('/api/graphql', getPaidMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 17))).thenThrow(Exception());
    when(client.post('/api/graphql', queryJobsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 17, ''))).thenThrow(Exception());
    when(client.get<List<Map<String, dynamic>>>("/api/v4/projects/72936/merge_requests/17/diffs?page=1&per_page=20")).thenAnswer((_) => Future(() => Response.of<List<Map<String, dynamic>>>([])));

    var parameters = {'projectId': 72936, 'projectName': "demo", 'mergeRequestIid': 17, 'fullPath': 'ultimate-plan/jihu-gitlab-app/jihu-gitlab-app'};
    await tester.pumpWidget(MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => ConnectionProvider()),
      ],
      child: MaterialApp(
        home: Scaffold(body: MergeRequestPage(arguments: parameters)),
      ),
    ));

    await tester.pumpAndSettle();
    expect(find.byType(HttpFailView), findsOneWidget);
    expect(SvgFinder('assets/images/refresh_exception.svg'), findsOneWidget);
    expect(find.text(AppLocalizations.dictionary().refreshException), findsOneWidget);

    await tester.tap(find.text(AppLocalizations.dictionary().refreshException));
    for (int i = 0; i < 5; i++) {
      await tester.pump(const Duration(seconds: 1));
    }
    verify(client.get<Map<String, dynamic>>("/api/v4/projects/72936/merge_requests/17/approvals")).called(2);
  });

  testWidgets('Should display merge user after merged', (tester) async {
    when(client.get<Map<String, dynamic>>("/api/v4/projects/59893/merge_requests/371/approvals")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(approvalResponse)));
    when(client.get('/api/v4/projects/59893')).thenAnswer((_) => Future(() => Response.of<Map>({"path_with_namespace": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app"})));
    Map<String, dynamic> map = Map.from(mrGraphQLResponse);
    map['data']['project']['mergeRequest']['state'] = "merged";
    map['data']['project']['mergeRequest']['mergeUser'] = {"id": "gid://gitlab/User/30192", "username": "tester-username", "name": "tester-name", "avatarUrl": "tester-avatar-url"};
    when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
        .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(map)));
    when(client.post('/api/graphql', getPaidMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
        .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(paidMrGraphQLResponse)));
    when(client.post('/api/graphql', queryJobsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371, ''))).thenAnswer((_) => Future(() => Response.of(jobsGraphQLResponse)));
    when(client.post("/api/graphql", queryCommitsGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 371, ''))).thenAnswer((_) => Future(() => Response.of(commitsGraphQLResponse)));
    when(client.get<List<Map<String, dynamic>>>("/api/v4/projects/59893/merge_requests/371/diffs?page=1&per_page=20")).thenAnswer((_) => Future(() => Response.of<List<Map<String, dynamic>>>([])));

    var parameters = {'projectId': 59893, 'projectName': "demo", 'mergeRequestIid': 371, 'fullPath': 'ultimate-plan/jihu-gitlab-app/jihu-gitlab-app'};
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(home: Scaffold(body: MergeRequestPage(arguments: parameters))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(SvgFinder('assets/images/mr_merged.svg'), findsNWidgets(2));
    expect(find.text("tester-name"), findsOneWidget);
    expect(find.byType(AvatarAndName), findsNWidgets(2));
    expect(find.byType(MergeBlockedView), findsNothing);
    expect(find.byType(MergeReadyView), findsNothing);
  });

  group('Rebase', () {
    testWidgets('Should display rebase fail message when the source branch is conflicts with the target branch but response conflicts = false', (tester) async {
      // Draft MR
      when(client.get<Map<String, dynamic>>("/api/v4/projects/59893/merge_requests/371/approvals")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(approvalResponse)));
      when(client.get('/api/v4/projects/59893')).thenAnswer((_) => Future(() => Response.of<Map>({"path_with_namespace": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app"})));
      when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(mrRebaseGraphQLResponse)));
      when(client.post('/api/graphql', getPaidMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(paidMrRebaseGraphQLResponse)));
      when(client.post('/api/graphql', queryJobsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371, ''))).thenAnswer((_) => Future(() => Response.of(jobsGraphQLResponse)));
      when(client.post("/api/graphql", queryCommitsGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 371, ''))).thenAnswer((_) => Future(() => Response.of(commitsGraphQLResponse)));
      when(client.get<List<Map<String, dynamic>>>("/api/v4/projects/59893/merge_requests/371/diffs?page=1&per_page=20")).thenAnswer((_) => Future(() => Response.of<List<Map<String, dynamic>>>([])));

      var parameters = {'projectId': 59893, 'projectName': "demo", 'mergeRequestIid': 371, 'fullPath': 'ultimate-plan/jihu-gitlab-app/jihu-gitlab-app'};
      var builder = MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
        child: MaterialApp(home: Scaffold(body: MergeRequestPage(arguments: parameters))),
      );

      await tester.pumpWidget(builder);
      await tester.pumpAndSettle();
      expect(find.text('demo !371'), findsOneWidget);
      expect(find.text('Draft: Resolve "Feature: 开发人员 Rebase MR"'), findsOneWidget);
      expect(find.widgetWithText(LoadingButton, AppLocalizations.dictionary().rebase), findsOneWidget);

      when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(mrRebaseWithConflictButRespConflictsFalseGraphQLResponse)));
      when(client.post('/api/graphql', queryRebaseInProgressGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>({
                "data": {
                  "project": {
                    "mergeRequest": {"rebaseInProgress": false}
                  }
                }
              })));
      when(client.put<dynamic>("/api/v4/projects/59893/merge_requests/371/rebase?skip_ci=false", null)).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
      await tester.tap(find.widgetWithText(LoadingButton, AppLocalizations.dictionary().rebase));
      await tester.pumpAndSettle();
      verify(client.put<dynamic>("/api/v4/projects/59893/merge_requests/371/rebase?skip_ci=false", null)).called(1);
      expect(find.textContaining('Rebase failed: Rebase locally, resolve all conflicts, then push the branch.'), findsOneWidget);
    });

    testWidgets('Should not display rebase button and rebase fail message when the source branch is conflicts with the target branch and response conflicts = true', (tester) async {
      // Draft MR
      when(client.get<Map<String, dynamic>>("/api/v4/projects/59893/merge_requests/371/approvals")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(approvalResponse)));
      when(client.get('/api/v4/projects/59893')).thenAnswer((_) => Future(() => Response.of<Map>({"path_with_namespace": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app"})));
      when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(mrRebaseGraphQLResponse)));
      when(client.post('/api/graphql', getPaidMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(paidMrRebaseGraphQLResponse)));
      when(client.post('/api/graphql', queryJobsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371, ''))).thenAnswer((_) => Future(() => Response.of(jobsGraphQLResponse)));
      when(client.post("/api/graphql", queryCommitsGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 371, ''))).thenAnswer((_) => Future(() => Response.of(commitsGraphQLResponse)));
      when(client.get<List<Map<String, dynamic>>>("/api/v4/projects/59893/merge_requests/371/diffs?page=1&per_page=20")).thenAnswer((_) => Future(() => Response.of<List<Map<String, dynamic>>>([])));

      var parameters = {'projectId': 59893, 'projectName': "demo", 'mergeRequestIid': 371, 'fullPath': 'ultimate-plan/jihu-gitlab-app/jihu-gitlab-app'};
      var builder = MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
        child: MaterialApp(home: Scaffold(body: MergeRequestPage(arguments: parameters))),
      );

      await tester.pumpWidget(builder);
      await tester.pumpAndSettle();
      expect(find.text('demo !371'), findsOneWidget);
      expect(find.text('Draft: Resolve "Feature: 开发人员 Rebase MR"'), findsOneWidget);
      expect(find.widgetWithText(LoadingButton, AppLocalizations.dictionary().rebase), findsOneWidget);

      when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(mrRebaseWithRespConflictsTrueGraphQLResponse)));
      when(client.post('/api/graphql', queryRebaseInProgressGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>({
                "data": {
                  "project": {
                    "mergeRequest": {"rebaseInProgress": false}
                  }
                }
              })));
      when(client.put<dynamic>("/api/v4/projects/59893/merge_requests/371/rebase?skip_ci=false", null)).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
      await tester.tap(find.widgetWithText(LoadingButton, AppLocalizations.dictionary().rebase));
      await tester.pumpAndSettle();
      verify(client.put<dynamic>("/api/v4/projects/59893/merge_requests/371/rebase?skip_ci=false", null)).called(1);
      expect(find.widgetWithText(LoadingButton, AppLocalizations.dictionary().rebase), findsNothing);
      expect(find.byKey(const Key('MergeErrorMessage')), findsNothing);
    });

    testWidgets('Should not display rebase button and rebase fail message when rebase succeed', (tester) async {
      // Draft MR
      when(client.get<Map<String, dynamic>>("/api/v4/projects/59893/merge_requests/371/approvals")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(approvalResponse)));
      when(client.get('/api/v4/projects/59893')).thenAnswer((_) => Future(() => Response.of<Map>({"path_with_namespace": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app"})));
      when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(mrRebaseGraphQLResponse)));
      when(client.post('/api/graphql', getPaidMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(paidMrRebaseGraphQLResponse)));
      when(client.post('/api/graphql', queryJobsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371, ''))).thenAnswer((_) => Future(() => Response.of(jobsGraphQLResponse)));
      when(client.post("/api/graphql", queryCommitsGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 371, ''))).thenAnswer((_) => Future(() => Response.of(commitsGraphQLResponse)));
      when(client.get<List<Map<String, dynamic>>>("/api/v4/projects/59893/merge_requests/371/diffs?page=1&per_page=20")).thenAnswer((_) => Future(() => Response.of<List<Map<String, dynamic>>>([])));

      var parameters = {'projectId': 59893, 'projectName': "demo", 'mergeRequestIid': 371, 'fullPath': 'ultimate-plan/jihu-gitlab-app/jihu-gitlab-app'};
      var builder = MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
        child: MaterialApp(home: Scaffold(body: MergeRequestPage(arguments: parameters))),
      );

      await tester.pumpWidget(builder);
      await tester.pumpAndSettle();
      expect(find.text('demo !371'), findsOneWidget);
      expect(find.text('Draft: Resolve "Feature: 开发人员 Rebase MR"'), findsOneWidget);
      expect(find.widgetWithText(LoadingButton, AppLocalizations.dictionary().rebase), findsOneWidget);

      when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(mrRebaseWithConflictButRespConflictsFalseGraphQLResponse)));
      when(client.post('/api/graphql', queryRebaseInProgressGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>({
                "data": {
                  "project": {
                    "mergeRequest": {"rebaseInProgress": false}
                  }
                }
              })));
      when(client.put<dynamic>("/api/v4/projects/59893/merge_requests/371/rebase?skip_ci=false", null)).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
      await tester.tap(find.widgetWithText(LoadingButton, AppLocalizations.dictionary().rebase));
      await tester.pumpAndSettle();
      verify(client.put<dynamic>("/api/v4/projects/59893/merge_requests/371/rebase?skip_ci=false", null)).called(1);
      expect(find.textContaining('Rebase failed: Rebase locally, resolve all conflicts, then push the branch.'), findsOneWidget);

      when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(mrRebaseSucceedGraphQLResponse)));
      final listFinder = find.byType(Scrollable).last;
      await tester.scrollUntilVisible(find.widgetWithText(LoadingButton, AppLocalizations.dictionary().rebase), 500.0, scrollable: listFinder);
      await tester.tap(find.widgetWithText(LoadingButton, AppLocalizations.dictionary().rebase));
      await tester.pumpAndSettle();
      expect(find.widgetWithText(LoadingButton, AppLocalizations.dictionary().rebase), findsNothing);
      expect(find.byKey(const Key('MergeErrorMessage')), findsNothing);
    });

    testWidgets('Should rebase fail message able to close', (tester) async {
      // Draft MR
      when(client.get<Map<String, dynamic>>("/api/v4/projects/59893/merge_requests/371/approvals")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(approvalResponse)));
      when(client.get('/api/v4/projects/59893')).thenAnswer((_) => Future(() => Response.of<Map>({"path_with_namespace": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app"})));
      when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(mrRebaseGraphQLResponse)));
      when(client.post('/api/graphql', getPaidMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(paidMrRebaseGraphQLResponse)));
      when(client.post('/api/graphql', queryJobsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371, ''))).thenAnswer((_) => Future(() => Response.of(jobsGraphQLResponse)));
      when(client.post("/api/graphql", queryCommitsGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 371, ''))).thenAnswer((_) => Future(() => Response.of(commitsGraphQLResponse)));
      when(client.get<List<Map<String, dynamic>>>("/api/v4/projects/59893/merge_requests/371/diffs?page=1&per_page=20")).thenAnswer((_) => Future(() => Response.of<List<Map<String, dynamic>>>([])));

      var parameters = {'projectId': 59893, 'projectName': "demo", 'mergeRequestIid': 371, 'fullPath': 'ultimate-plan/jihu-gitlab-app/jihu-gitlab-app'};
      var builder = MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
        child: MaterialApp(home: Scaffold(body: MergeRequestPage(arguments: parameters))),
      );

      await tester.pumpWidget(builder);
      await tester.pumpAndSettle();
      expect(find.text('demo !371'), findsOneWidget);
      expect(find.text('Draft: Resolve "Feature: 开发人员 Rebase MR"'), findsOneWidget);
      expect(find.widgetWithText(LoadingButton, AppLocalizations.dictionary().rebase), findsOneWidget);

      when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(mrRebaseWithConflictButRespConflictsFalseGraphQLResponse)));
      when(client.post('/api/graphql', queryRebaseInProgressGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>({
                "data": {
                  "project": {
                    "mergeRequest": {"rebaseInProgress": false}
                  }
                }
              })));
      when(client.put<dynamic>("/api/v4/projects/59893/merge_requests/371/rebase?skip_ci=false", null)).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
      await tester.tap(find.widgetWithText(LoadingButton, AppLocalizations.dictionary().rebase));
      await tester.pumpAndSettle();
      expect(SvgFinder('assets/images/warning.svg'), findsOneWidget);
      expect(find.textContaining('Rebase failed: Rebase locally, resolve all conflicts, then push the branch.'), findsOneWidget);

      final listFinder = find.byType(Scrollable).last;
      await tester.scrollUntilVisible(find.byKey(const Key('MergeErrorCloseButton')), 500.0, scrollable: listFinder);
      expect(find.byKey(const Key('MergeErrorCloseButton')), findsOneWidget);
      await tester.tap(find.byKey(const Key('MergeErrorCloseButton')));
      await tester.pumpAndSettle();
      expect(SvgFinder('assets/images/warning.svg'), findsNothing);
      expect(find.textContaining('Rebase failed: Rebase locally, resolve all conflicts, then push the branch.'), findsNothing);
      expect(find.byKey(const Key('MergeErrorCloseButton')), findsNothing);
    });

    testWidgets('Should display loading animation when rebase in progress', (tester) async {
      // Draft MR
      when(client.get<Map<String, dynamic>>("/api/v4/projects/59893/merge_requests/371/approvals")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(approvalResponse)));
      when(client.get('/api/v4/projects/59893')).thenAnswer((_) => Future(() => Response.of<Map>({"path_with_namespace": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app"})));
      when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(mrRebaseGraphQLResponse)));
      when(client.post('/api/graphql', getPaidMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(paidMrRebaseGraphQLResponse)));
      when(client.post('/api/graphql', queryJobsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371, ''))).thenAnswer((_) => Future(() => Response.of(jobsGraphQLResponse)));
      when(client.post("/api/graphql", queryCommitsGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 371, ''))).thenAnswer((_) => Future(() => Response.of(commitsGraphQLResponse)));
      when(client.get<List<Map<String, dynamic>>>("/api/v4/projects/59893/merge_requests/371/diffs?page=1&per_page=20")).thenAnswer((_) => Future(() => Response.of<List<Map<String, dynamic>>>([])));

      var parameters = {'projectId': 59893, 'projectName': "demo", 'mergeRequestIid': 371, 'fullPath': 'ultimate-plan/jihu-gitlab-app/jihu-gitlab-app'};
      var builder = MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
        child: MaterialApp(home: Scaffold(body: MergeRequestPage(arguments: parameters))),
      );

      await tester.pumpWidget(builder);
      await tester.pumpAndSettle();
      expect(find.text('demo !371'), findsOneWidget);
      expect(find.text('Draft: Resolve "Feature: 开发人员 Rebase MR"'), findsOneWidget);
      expect(find.widgetWithText(LoadingButton, AppLocalizations.dictionary().rebase), findsOneWidget);

      when(client.post('/api/graphql', queryRebaseInProgressGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>({
                "data": {
                  "project": {
                    "mergeRequest": {"rebaseInProgress": true}
                  }
                }
              })));
      when(client.put<dynamic>("/api/v4/projects/59893/merge_requests/371/rebase?skip_ci=false", null)).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
      await tester.tap(find.widgetWithText(LoadingButton, AppLocalizations.dictionary().rebase));
      for (int i = 0; i < 5; i++) {
        await tester.pump(const Duration(seconds: 1));
      }
      expect(find.byKey(const Key('MergeErrorMessage')), findsNothing);
      expect(SvgFinder('assets/images/mergeable.svg'), findsNothing);
      expect(find.byType(CircularProgressIndicator), findsAtLeastNWidgets(2));

      when(client.post('/api/graphql', queryRebaseInProgressGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>({
                "data": {
                  "project": {
                    "mergeRequest": {"rebaseInProgress": false}
                  }
                }
              })));
      for (int i = 0; i < 5; i++) {
        await tester.pump(const Duration(seconds: 1));
      }
      expect(find.byType(CircularProgressIndicator), findsNothing);
    });

    testWidgets('Should display file changes diff when tapped changed file item', (tester) async {
      await setUpMobileBinding(tester);

      // Draft MR
      when(client.get<Map<String, dynamic>>("/api/v4/projects/59893/merge_requests/371/approvals")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(approvalResponse)));
      when(client.get('/api/v4/projects/59893')).thenAnswer((_) => Future(() => Response.of<Map>({"path_with_namespace": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app"})));
      when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(mrRebaseGraphQLResponse)));
      when(client.post('/api/graphql', getPaidMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(paidMrRebaseGraphQLResponse)));
      when(client.post('/api/graphql', queryJobsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371, ''))).thenAnswer((_) => Future(() => Response.of(jobsGraphQLResponse)));
      when(client.post("/api/graphql", queryCommitsGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 371, ''))).thenAnswer((_) => Future(() => Response.of(commitsGraphQLResponse)));
      when(client.get<List<Map<String, dynamic>>>("/api/v4/projects/59893/merge_requests/371/diffs?page=1&per_page=20"))
          .thenAnswer((_) => Future(() => Response.of<List<Map<String, dynamic>>>(mrDiffsResponse)));
      when(client.get<List<Map<String, dynamic>>>("/api/v4/projects/59893/merge_requests/371/diffs?page=2&per_page=20"))
          .thenAnswer((_) => Future(() => Response.of<List<Map<String, dynamic>>>(mrDiffsNextPageResponse)));

      var parameters = {'projectId': 59893, 'projectName': "demo", 'mergeRequestIid': 371, 'fullPath': 'ultimate-plan/jihu-gitlab-app/jihu-gitlab-app'};
      var builder = MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
        child: MaterialApp(home: Scaffold(body: MergeRequestPage(arguments: parameters))),
      );

      await tester.pumpWidget(builder);
      await tester.pumpAndSettle();
      expect(find.text('demo !371'), findsOneWidget);
      expect(find.text('Draft: Resolve "Feature: 开发人员 Rebase MR"'), findsOneWidget);
      expect(find.byType(MergeRequestTabBar), findsOneWidget);
      expect(find.byType(TabBarView), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().overview), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().changes), findsOneWidget);

      await tester.tap(find.text(AppLocalizations.dictionary().changes));
      await tester.pumpAndSettle();
      expect(find.text(AppLocalizations.dictionary().noChanges), findsNothing);
      expect(find.byType(ListView), findsOneWidget);
      expect(find.text('lib/core/user_provider/user_provider.dart'), findsOneWidget);
      expect(find.text('lib/core/user_provider/users.dart'), findsOneWidget);
      expect(find.text('test/core/user_provider_test.dart'), findsOneWidget);
      expect(find.text('test/integration_tests/account_page/account_page_test.dart'), findsOneWidget);
      expect(find.text('test/mocker/tester.dart'), findsOneWidget);

      final listFinder = find.byType(Scrollable).last;
      await tester.scrollUntilVisible(find.text('lib/modules/auth/login_page.dart'), 500.0, scrollable: listFinder);

      String fileName = 'lib/core/widgets/account/account_page.dart';
      await tester.scrollUntilVisible(find.text(fileName), 500.0, scrollable: listFinder);
      expect(find.text(fileName), findsOneWidget);
      await tester.tap(find.text(fileName));
      await tester.pumpAndSettle();
      expect(find.byType(MergeRequestPage), findsNothing);
      expect(find.byType(MrChangesPage), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().cancel), findsOneWidget);
      expect(find.byType(ExpandableListView), findsOneWidget);
      expect(find.text('test/core/user_provider_test.dart'), findsOneWidget);
      expect(find.text('lib/core/user_provider/users.dart'), findsOneWidget);
      expect(find.text('test/core/user_provider_test.dart'), findsOneWidget);
      expect(find.text('test/integration_tests/account_page/account_page_test.dart'), findsOneWidget);
      expect(find.text('test/mocker/tester.dart'), findsOneWidget);
      expect(find.text(fileName).hitTestable(), findsOneWidget);

      String delFileLine =
          "-                                        if (ConnectionProvider().isUsed(item)) const Text('Already used', style: TextStyle(color: AppThemeData.secondaryColor, fontSize: 10, fontWeight: FontWeight.w400))";
      String addFileLine = "+                                        if (ConnectionProvider().isUsed(item))";
      String unchangedFileLine = '                                     if (_selectedItem.displayTitle == item)';
      expect(find.textContaining('@@ -192,7 +192,8 @@ class _AccountPageState extends State<AccountPage> {'), findsOneWidget);
      expect(find.textContaining(unchangedFileLine), findsOneWidget);
      expect(find.textContaining(delFileLine), findsOneWidget);
      expect(find.textContaining(addFileLine), findsOneWidget);
      var unchangedFinder = find.byKey(Key('${fileName}_198_199_$unchangedFileLine'));
      expect(unchangedFinder, findsOneWidget);
      expect((tester.widget(unchangedFinder) as Container).color, const Color(0xFFFDF6E3));
      var delFinder = find.byKey(Key('${fileName}_195__$delFileLine'));
      expect(delFinder, findsOneWidget);
      expect((tester.widget(delFinder) as Container).color, const Color(0xFFFBD4C1));
      var addFinder = find.byKey(Key('${fileName}__195_$addFileLine'));
      expect(addFinder, findsOneWidget);
      expect((tester.widget(addFinder) as Container).color, const Color(0xFFDBE7C6));
      await tester.tap(find.text(fileName));
      await tester.pumpAndSettle();
      expect(find.textContaining('@@ -192,7 +192,8 @@ class _AccountPageState extends State<AccountPage> {'), findsNothing);
      expect(find.textContaining(unchangedFileLine), findsNothing);

      await tester.tap(find.text(AppLocalizations.dictionary().cancel));
      await tester.pumpAndSettle();
      expect(find.byType(MrChangesPage), findsNothing);
      expect(find.byType(MergeRequestPage), findsOneWidget);
    });

    testWidgets("Should display the commit list", (tester) async {
      when(client.get<Map<String, dynamic>>("/api/v4/projects/59893/merge_requests/371/approvals")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(approvalResponse)));
      var parameters = {'projectId': 59893, 'projectName': "demo", 'mergeRequestIid': 371, 'fullPath': 'ultimate-plan/jihu-gitlab-app/jihu-gitlab-app'};
      when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(mrGraphQLResponse)));
      when(client.post('/api/graphql', getPaidMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(paidMrGraphQLResponse)));
      when(client.post('/api/graphql', queryJobsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371, ''))).thenAnswer((_) => Future(() => Response.of(jobsGraphQLResponse)));
      when(client.post("/api/graphql", queryCommitsGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 371, ''))).thenAnswer((_) => Future(() => Response.of(commitsGraphQLResponse)));
      when(client.get<List<Map<String, dynamic>>>("/api/v4/projects/59893/merge_requests/371/diffs?page=1&per_page=20")).thenAnswer((_) => Future(() => Response.of<List<Map<String, dynamic>>>([])));

      var builder = MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
        child: MaterialApp(home: Scaffold(body: MergeRequestPage(arguments: parameters))),
      );

      await tester.pumpWidget(builder);
      await tester.pumpAndSettle();
      expect(find.text("Commits"), findsOneWidget);
      await tester.tap(find.text("Commits"));
      await tester.pumpAndSettle();
      expect(find.text("name1 in page 1"), findsOneWidget);
      expect(find.text("chore: #497 Merge code from main."), findsOneWidget);
      expect(find.text("name2 in page 1"), findsOneWidget);
      expect(find.text("refactor: #497 Remove no needs code and rename"), findsOneWidget);
    });
  });

  testWidgets('Should display ANY_APPROVER approval rule with approve user', (tester) async {
    // Draft MR
    when(client.get<Map<String, dynamic>>("/api/v4/projects/59893/merge_requests/371/approvals")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(approvedApprovalResponse)));
    when(client.get('/api/v4/projects/59893')).thenAnswer((_) => Future(() => Response.of<Map>({"path_with_namespace": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app"})));
    when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
        .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(mrApprovedGraphQLResponse)));
    when(client.post('/api/graphql', getPaidMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
        .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(paidMrApprovedGraphQLResponse)));
    when(client.post('/api/graphql', queryJobsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371, ''))).thenAnswer((_) => Future(() => Response.of(jobsGraphQLResponse)));
    when(client.post("/api/graphql", queryCommitsGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 371, ''))).thenAnswer((_) => Future(() => Response.of(commitsGraphQLResponse)));
    when(client.get<List<Map<String, dynamic>>>("/api/v4/projects/59893/merge_requests/371/diffs?page=1&per_page=20")).thenAnswer((_) => Future(() => Response.of<List<Map<String, dynamic>>>([])));

    var parameters = {'projectId': 59893, 'projectName': "demo", 'mergeRequestIid': 371, 'fullPath': 'ultimate-plan/jihu-gitlab-app/jihu-gitlab-app'};
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(home: Scaffold(body: MergeRequestPage(arguments: parameters))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.text('demo !371'), findsOneWidget);
    expect(find.text('Resolve "测试跳过 ci"'), findsOneWidget);

    expect(find.byType(ApprovalRuleView), findsNWidgets(3));
    expect(find.text(AppLocalizations.dictionary().allEligibleUsers), findsOneWidget);
    expect(find.text("2 approvals from License-Check"), findsOneWidget);
    expect(find.text("1 of 1 approval from Coverage-Check"), findsOneWidget);
    // finds ANY_APPROVER rule view
    expect(find.byKey(const Key('ANY_APPROVER_All Members')), findsOneWidget);
    // finds Approved user
    expect(find.byKey(const Key('All Members_wanyouzhu_approved')), findsOneWidget);
    expect(find.byKey(const Key('All Members_zhangling_approved')), findsOneWidget);
    expect(find.byKey(const Key('License-Check_wanyouzhu_approved')), findsOneWidget);
    expect(find.byKey(const Key('License-Check_zhangling_approved')), findsOneWidget);
    expect(find.byKey(const Key('License-Check_sinkcup')), findsOneWidget);
    expect(find.byKey(const Key('License-Check_raymond-liao')), findsOneWidget);
    expect(find.byKey(const Key('Coverage-Check_wanyouzhu_approved')), findsOneWidget);
    expect(find.byKey(const Key('Coverage-Check_sinkcup')), findsOneWidget);
    expect(find.byKey(const Key('Coverage-Check_raymond-liao')), findsOneWidget);
  });

  testWidgets("Should share the merge request", (tester) async {
    var clipboard = MockClipboard();
    Clipboard.injectForTest(clipboard);
    when(client.get<Map<String, dynamic>>("/api/v4/projects/59893/merge_requests/371/approvals")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(approvedApprovalResponse)));
    when(client.get('/api/v4/projects/59893')).thenAnswer((_) => Future(() => Response.of<Map>({"path_with_namespace": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app"})));
    when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
        .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(mrApprovedGraphQLResponse)));
    when(client.post('/api/graphql', getPaidMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
        .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(paidMrApprovedGraphQLResponse)));
    when(client.post('/api/graphql', queryJobsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371, ''))).thenAnswer((_) => Future(() => Response.of(jobsGraphQLResponse)));
    when(client.post("/api/graphql", queryCommitsGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 371, ''))).thenAnswer((_) => Future(() => Response.of(commitsGraphQLResponse)));
    when(client.get<List<Map<String, dynamic>>>("/api/v4/projects/59893/merge_requests/371/diffs?page=1&per_page=20")).thenAnswer((_) => Future(() => Response.of<List<Map<String, dynamic>>>([])));

    var parameters = {'projectId': 59893, 'projectName': "demo", 'mergeRequestIid': 371, 'fullPath': 'ultimate-plan/jihu-gitlab-app/jihu-gitlab-app'};
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(home: Scaffold(body: MergeRequestPage(arguments: parameters))),
    );
    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();

    expect(SvgFinder("assets/images/external-link.svg"), findsOneWidget);
    await tester.tap(SvgFinder("assets/images/external-link.svg"));
    await tester.pumpAndSettle();
    expect(find.text("Copy link"), findsOneWidget);
    expect(find.text("Copy title & link"), findsOneWidget);
    await tester.tap(find.text("Copy link"));
    verify(clipboard.setData("https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/-/merge_requests/28")).called(1);
    await tester.pumpAndSettle(const Duration(seconds: 2));

    expect(SvgFinder("assets/images/external-link.svg"), findsOneWidget);
    await tester.tap(SvgFinder("assets/images/external-link.svg"));
    await tester.pumpAndSettle();
    expect(find.text("Copy title & link"), findsOneWidget);
    await tester.tap(find.text("Copy title & link"), warnIfMissed: false);
    verify(clipboard.setData("Resolve \"测试跳过 ci\"\nhttps://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/-/merge_requests/28")).called(1);
    await tester.pumpAndSettle(const Duration(seconds: 2));
  });

  tearDown(() {
    ConnectionProvider().fullReset();
  });
}
