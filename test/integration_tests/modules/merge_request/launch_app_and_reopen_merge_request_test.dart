import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/global_keys.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/widgets/photo_picker.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_request_page.dart';
import 'package:jihu_gitlab_app/modules/mr/models/mr_graphql_request_body.dart';
import 'package:mockito/mockito.dart';

import '../../../test_data/discussion.dart';
import '../../../test_data/issue.dart';
import '../../../test_data/merge_request.dart';
import '../../../test_data/project.dart';
import '../../../test_data/to_dos.dart';
import '../../helper/core.dart';
import '../../helper/mobile_test_helper/root.dart';
import '../../helper/tablet_test_helper/root.dart';

void main() {
  setUp(() async {
    WidgetsFlutterBinding.ensureInitialized();
    setupLocator();

    when(client.postWithConnection<dynamic>("/api/graphql", getCommunityTypesGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/faq"), any))
        .thenAnswer((_) => Future(() => Response.of(faqTypesResponse)));
    when(client.get("/api/v4/users/0/starred_projects?page=1&per_page=50")).thenAnswer((realInvocation) => Future(() => Response.of([])));
    when(client.get("/api/v4/users/9527/starred_projects?page=1&per_page=50")).thenAnswer((realInvocation) => Future(() => Response.of([])));
    when(client.get<Map<String, dynamic>>("/api/v4/projects/72936/issues/6")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(issueData)));
    when(client.get<Map<String, dynamic>>("/api/v4/projects/72936")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(projectData)));
    when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>(todoPendingResponseData)));
    when(client.get<List<dynamic>>("/api/v4/projects/72936/issues/6/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>(discussionsData)));
    when(client.get<List<dynamic>>("/api/v4/projects/72936/labels?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
    when(client.getWithConnection("/api/v4/projects/89335", any)).thenAnswer((_) => Future(() => Response.of({
          "permissions": {
            "project_access": {"access_level": 50}
          }
        })));
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets('Should launch with mobile and save webUrl of Merge Request after enter in Merge Request Page and clear when exit', (tester) async {
    await launchMobileAppForTest(tester);
    await mobileLoginWithSaaSForPage(tester, PageType.todo.index, ToDosPage, onMock: onToDoRequestMock);
    await pageContentTest(tester);
  });

  testWidgets('Should app launch with tablet and save webUrl of Merge Request after enter in Merge Request Page and clear when exit', (tester) async {
    await launchTabletAppForTest(tester);
    await tabletLoginWithSaaSForPage(tester, PageType.todo.index, ToDosPage, onMock: onToDoRequestMock);
    await pageContentTest(tester);
  });

  tearDown(() async {
    ConnectionProvider().fullReset();

    locator.unregister<SubgroupListModel>();
    locator.unregister<ProjectsGroupsModel>();
    locator.unregister<ProjectsStarredModel>();
    locator.unregister<IssueDetailsModel>();
    locator.unregister<PhotoPicker>();
  });
}

Future<void> pageContentTest(tester) async {
  expect(find.text('pending test name'), findsOneWidget);
  expect(find.text('merge request item'), findsOneWidget);
  await tester.tap(find.text('merge request item'));
  await tester.pumpAndSettle();
  expect(find.byType(ToDosPage), findsNothing);
  expect(find.byType(MergeRequestPage), findsOneWidget);

  var webUrl = await LocalStorage.get(LocalStorageKeys.currentMergeRequestWebUrlKey, '');
  expect(webUrl, isNotEmpty);
  expect(webUrl, 'https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/merge_requests/84');

  await tester.tap(find.byType(BackButton));
  await tester.pumpAndSettle();
  webUrl = await LocalStorage.get(LocalStorageKeys.currentMergeRequestWebUrlKey, '');
  expect(find.byType(ToDosPage), findsOneWidget);
  expect(find.byType(MergeRequestPage), findsNothing);
  expect(webUrl, isEmpty);
}

void onToDoRequestMock() {
  when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>(todoPendingResponseData)));
  when(client.get<Map<String, dynamic>>("/api/v4/projects/59893/merge_requests/84/approvals")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(approvalResponse)));
  when(client.get('/api/v4/projects/59893')).thenAnswer((_) => Future(() => Response.of<Map>({"path_with_namespace": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app"})));
  when(client.post<Map<String, dynamic>>('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 84)))
      .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(mrGraphQLDraftResponse)));
  when(client.post('/api/graphql', getPaidMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 84)))
      .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(paidMrGraphQLDraftResponse)));
  when(client.post('/api/graphql', queryJobsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 84, ''))).thenAnswer((_) => Future(() => Response.of(jobsGraphQLResponse)));
  when(client.post("/api/graphql", queryCommitsGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 84, ''))).thenAnswer((_) => Future(() => Response.of(commitsGraphQLResponse)));
  when(client.get<List<Map<String, dynamic>>>("/api/v4/projects/59893/merge_requests/84/diffs?page=1&per_page=20")).thenAnswer((_) => Future(() => Response.of<List<Map<String, dynamic>>>([])));
  when(client.post('/api/graphql', {
    "variables": {"fullPath": 'ultimate-plan/jihu-gitlab-app/jihu-gitlab-app'},
    "query": """
        query (\$fullPath: ID!) {
          project(fullPath: \$fullPath) {
            id
            name
            fullPath
          }
        }
      """,
  })).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>({
        "data": {
          "project": {"id": "gid://gitlab/Project/59893", "name": "merge request item", "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app"}
        }
      })));
}
