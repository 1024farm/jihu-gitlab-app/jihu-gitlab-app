import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/string_extension.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar_and_name.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_button.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';
import 'package:jihu_gitlab_app/modules/mr/approval_view.dart';
import 'package:jihu_gitlab_app/modules/mr/code_quality_report_model.dart';
import 'package:jihu_gitlab_app/modules/mr/code_quality_summary_view.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_request_page.dart';
import 'package:jihu_gitlab_app/modules/mr/models/mr_graphql_request_body.dart';
import 'package:mockito/mockito.dart';

import '../../../finder/svg_finder.dart';
import '../../../test_data/merge_request.dart';
import '../../helper/core.dart';

Future<void> mergeRequestTest(WidgetTester tester, String targetString, {required bool isDraft}) async {
  expect(find.text(targetString), findsOneWidget);

  when(client.get<Map<String, dynamic>>("/api/v4/projects/59893/merge_requests/84/approvals")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(approvalResponse)));
  when(client.get('/api/v4/projects/59893')).thenAnswer((_) => Future(() => Response.of<Map>({"path_with_namespace": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app"})));
  when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 84)))
      .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(isDraft ? mrGraphQLDraftResponse : mrGraphQLResponse)));
  when(client.post('/api/graphql', getPaidMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 84)))
      .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(isDraft ? paidMrGraphQLDraftResponse : paidMrGraphQLResponse)));
  when(client.post('/api/graphql', queryJobsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 84, ''))).thenAnswer((_) => Future(() => Response.of(jobsGraphQLResponse)));
  when(client.post("/api/graphql", queryCommitsGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 84, ''))).thenAnswer((_) => Future(() => Response.of(commitsGraphQLResponse)));
  when(client.post("/api/graphql", requestBodyOfGetCodeQualityReports("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 84, 20, "")))
      .thenAnswer((_) => Future(() => Response.of(codeQualityReportsResponse)));

  await tester.tap(find.text(targetString));
  for (int i = 0; i < 5; i++) {
    await tester.pump(const Duration(seconds: 1));
  }
  expect(find.byType(ToDosPage), findsNothing);
  expect(find.byType(MergeRequestPage), findsOneWidget);

  // Page Title
  expect(find.text('merge request item !84'), findsOneWidget);

  // MR Title
  if (isDraft) {
    expect(find.text('Draft: Resolve "Feature: 开发人员查看 Merge blocked"'), findsOneWidget);
  } else {
    expect(find.text('Resolve "Feature: 用户更新议题 label"'), findsOneWidget);
  }

  // Status and author
  expect(SvgFinder('assets/images/mr_opened.svg'), findsOneWidget);
  expect(find.byKey(const Key('MergeRequestAuthorAvatar')), findsOneWidget);
  expect(find.byType(AvatarAndName), findsOneWidget);

  // Description
  expect(find.textContaining('Closes'), findsOneWidget);
  expect(find.textContaining('#47'), findsOneWidget);

  // Branches
  expect(find.byIcon(Icons.radio_button_unchecked), findsOneWidget);
  expect(find.text('${AppLocalizations.dictionary().sourceBranch}: 499-feature-kai-fa-ren-yuan-cha-kan-merge-blocked'), findsOneWidget);
  expect(find.byIcon(Icons.toll), findsOneWidget);
  expect(find.text('${AppLocalizations.dictionary().targetBranch}: main'), findsOneWidget);

  // Pipeline
  expect(SvgFinder('assets/images/warning.svg'), findsWidgets);
  expect(
      find.text(
          '${AppLocalizations.dictionary().pipeline} #943141 ${AppLocalizations.dictionary().pipelinePassedWithWarnings} ${AppLocalizations.dictionary().pipelineFor} 0334c32c ${AppLocalizations.dictionary().on} 499-feature-kai-fa-ren-yuan-cha-kan-merge-blocked'),
      findsOneWidget);

  // Coverage
  if (isDraft) {
    expect(find.textContaining('Test coverage'), findsNothing);
  } else {
    expect(find.text('Test coverage 94.2% from 1 job'), findsOneWidget);
  }

  // Jobs
  expect(SvgFinder('assets/images/warning.svg'), findsNWidgets(1));

  // Code Quality
  expect(find.text(AppLocalizations.dictionary().codeQualityDegraded(3)), findsOneWidget);
  expect(SvgFinder('assets/images/code_quality_warnings.svg'), findsOneWidget);
  expect(find.widgetWithText(CodeQualitySummaryView, "Code Quality degraded on 3 points."), findsOneWidget);
  await tester.tap(find.widgetWithText(CodeQualitySummaryView, "Code Quality degraded on 3 points."));
  await tester.pumpAndSettle();
  expect(find.widgetWithText(CommonAppBar, "Code Quality"), findsOneWidget);
  expect(find.byType(BackButton), findsOneWidget);
  expect(find.widgetWithText(CodeQualitySummaryView, "Code Quality degraded on 3 points."), findsOneWidget);
  expect(SvgFinder('assets/images/code_quality_yellow_dot.svg'), findsNWidgets(3));
  expect(
      find.text("Info - The method doesn't override an inherited method ${AppLocalizations.dictionary().inWord} ${'test/modules/ai/ai_page_test.mocks.dart'.disableAutoWrap}:${'84'.disableAutoWrap}"),
      findsOneWidget);
  expect(
      find.text("Info - The method doesn't override an inherited method ${AppLocalizations.dictionary().inWord} ${'test/modules/ai/ai_page_test.mocks.dart'.disableAutoWrap}:${'94'.disableAutoWrap}"),
      findsOneWidget);
  expect(
      find.text("Info - Avoid `print` calls in production code ${AppLocalizations.dictionary().inWord} ${'test/modules/mr/pipeline_detailed_status_test.dart'.disableAutoWrap}:${'8'.disableAutoWrap}"),
      findsOneWidget);
  await tester.tap(find.byType(BackButton));
  await tester.pumpAndSettle();

  if (!isDraft) {
    // Approval
    expect(find.byType(ApprovalView), findsOneWidget);
    expect(SvgFinder('assets/images/approval.svg'), findsOneWidget);
    expect(find.text(AppLocalizations.dictionary().approvalIsOptional), findsOneWidget);
    expect(find.text(AppLocalizations.dictionary().approveRuleTitle(1, AppLocalizations.dictionary().countOfApprovals(2), 'License-Check')), findsOneWidget);
    expect(find.text(AppLocalizations.dictionary().approveRuleTitle(1, AppLocalizations.dictionary().countOfApprovals(1), 'Coverage-Check')), findsOneWidget);
    expect(find.widgetWithText(LoadingButton, AppLocalizations.dictionary().approve), findsOneWidget);
  } else {
    // Merge
    expect(SvgFinder('assets/images/merge_blocked.svg'), findsOneWidget);
    expect(find.text(AppLocalizations.dictionary().mergeBlocked), findsOneWidget);
    expect(find.text(AppLocalizations.dictionary().draftStatusDescription), findsOneWidget);
    expect(find.widgetWithText(LoadingButton, AppLocalizations.dictionary().markAsReady), findsOneWidget);
  }

  await goBackToToDosPage(tester, targetString);
}

Future<void> goBackToToDosPage(tester, String targetString) async {
  await tester.tap(find.byType(BackButton));
  for (int i = 0; i < 5; i++) {
    await tester.pump(const Duration(seconds: 1));
  }
  expect(find.byType(ToDosPage), findsOneWidget);
  expect(find.byType(MergeRequestPage), findsNothing);
  expect(find.text(targetString), findsOneWidget);
}
