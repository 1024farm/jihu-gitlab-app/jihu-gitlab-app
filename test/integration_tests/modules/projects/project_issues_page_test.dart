import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/system_version_detector.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar/avatar.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_model.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_page.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_view.dart';
import 'package:jihu_gitlab_app/modules/issues/list/project_issues_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/issues/project_issues_page.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../mocker/tester.dart';
import '../../../modules/community/community_post_details_page_test.mocks.dart';
import '../../../test_binding_setter.dart';
import '../../../test_data/issue.dart';
import '../../helper/core.dart';

Future<void> main() async {
  setUp(() async {
    var detector = MockSystemVersionDetector();
    when(detector.detect()).thenAnswer((_) => Future(() => SystemType.latest));
    SystemVersionDetector.injectInstanceForTesting(detector);
    TestWidgetsFlutterBinding.ensureInitialized();
    locator.registerFactory<IssueDetailsModel>(() => IssueDetailsModel());

    AppLocalizations.init();
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    // TODO: set the privacy alert not showing for some test issue: "not hit test on the specified widget"
    LocalStorage.save('privacy-policy-agreed', true);

    ConnectionProvider().reset(Tester.jihuLabUser());
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets('Should load more project issues', (tester) async {
    await setUpMobileBinding(tester);
    when(client.post<dynamic>("/api/graphql", getProjectIssuesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo', '', 20)))
        .thenAnswer((_) => Future(() => Response.of(projectIssuesGraphQLResponseData)));
    when(client.post<dynamic>("/api/graphql", getProjectIssuesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo', '', 20, afterCursor: 'end_cursor')))
        .thenAnswer((_) => Future(() => Response.of(projectIssuesNextPageGraphQLResponseData)));
    when(client.get<List<dynamic>>("/api/v4/projects/59893/issues/264/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
    when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 264)))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(issueDetailsGraphQLResponse)));
    when(client.postWithConnection('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 264), any))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(issuePostVotesGraphQLResponse)));
    when(client.getWithConnection("/api/v4/projects/59893/issues/264/award_emoji?per_page=100&page=1", any)).thenAnswer((_) => Future(() => Response.of([])));

    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
      child: MaterialApp(
          onGenerateRoute: onGenerateRoute,
          home: Scaffold(
              body: ProjectIssuesPage(
            refreshKey: GlobalKey(),
            projectId: 59893,
            relativePath: 'ultimate-plan/jihu-gitlab-app/demo',
            onSelectedIssueChange: (index, param) => {},
          ))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();

    expect(find.byType(IssuesPage), findsOneWidget);
    expect(find.byType(IssuesView), findsOneWidget);
    expect(find.byType(TipsView), findsNothing);
    expect(find.byType(ListView), findsOneWidget);
    expect(find.byType(Avatar), findsNWidgets(2));
    expect(find.text('Feature：交互优化：创建议题时，如果只有一个项目，则跳过「选择项目」'), findsOneWidget);

    var listFinder = find.byType(Scrollable).last;
    await tester.scrollUntilVisible(find.text('Task: 唯力在#576卡补充已完成的 MR 业务逻辑'), 500.0, scrollable: listFinder);
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.text('Task: 唯力在#576卡补充已完成的 MR 业务逻辑'), findsOneWidget);

    await tester.scrollUntilVisible(find.text('Feature：交互优化：创建议题时，如果只有一个项目，则跳过「选择项目」2'), 500.0, scrollable: listFinder);
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.text('Feature：交互优化：创建议题时，如果只有一个项目，则跳过「选择项目」2'), findsOneWidget);
  });

  tearDown(() async {
    ConnectionProvider().fullReset();
    ProjectProvider().fullReset();
    locator.unregister<IssueDetailsModel>();
    reset(client);
  });
}
