import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/db_manager.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/photo_picker.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/index.dart';
import 'package:jihu_gitlab_app/modules/issues/list/group_issues_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/list/group_projects_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_page.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_view.dart';
import 'package:jihu_gitlab_app/modules/issues/list/project_issues_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project_list_tile.dart';
import 'package:jihu_gitlab_app/modules/projects/projects.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/project_repository_model.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_file_content_fetcher.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_tree_data_fetcher.dart';
import 'package:jihu_gitlab_app/modules/root/model/root_model.dart';
import 'package:mockito/mockito.dart';

import '../../../mocker/tester.dart';
import '../../../modules/ai/ai_page_test.mocks.dart';
import '../../../test_binding_setter.dart';
import '../../../test_data/groups.dart';
import '../../../test_data/issue.dart';
import '../../../test_data/members.dart';
import '../../../test_data/project.dart';
import '../../../test_data/to_dos.dart';
import '../../helper/core.dart';
import '../../helper/mobile_test_helper/root.dart';
import '../../helper/tablet_test_helper/root.dart';
import '../../query_issues_by_state_test.dart';
import '../issues/test_issue_details_page.dart';

part 'test_subgroup_issues_page.dart';
part 'test_subgroup_list_page.dart';
part 'test_subgroup_page.dart';

void main() {
  setUp(() async {
    WidgetsFlutterBinding.ensureInitialized();
    setupLocator();

    when(client.post<dynamic>("/api/graphql", argThat(predicate((arg) => arg.toString().contains("getIssuesEE"))))).thenAnswer((_) => Future(() => Response.of(faqIssuesGraphQLResponseData)));
    when(client.postWithConnection<dynamic>("/api/graphql", getCommunityTypesGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/faq"), any))
        .thenAnswer((_) => Future(() => Response.of(faqTypesResponse)));
    when(client.get("/api/v4/users/9527/starred_projects?page=1&per_page=50")).thenAnswer((realInvocation) => Future(() => Response.of([])));
    when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((realInvocation) => Future(() => Response.of<List<dynamic>>(todoPendingResponseData)));
    when(client.get<List<dynamic>>("/api/v4/groups?top_level_only=true&page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
    when(client.getWithConnection("/api/v4/projects/89335", any)).thenAnswer((_) => Future(() => Response.of({
          "permissions": {
            "project_access": {"access_level": 50}
          }
        })));
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfRepositoryTree("highsof-t/learn-gitlab", '', ref: "main"))).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>({})));
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfCheckRepositoryIsEmpty('highsof-t/learn-gitlab')))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(checkRepositoryIsEmptyResponse)));
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfFetchFileContent('highsof-t/learn-gitlab', "README.md", "main"))).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets('Should app launch with mobile and able to navigate to Projects page', (tester) async {
    await launchMobileAppForTest(tester);
    await navigateToTargetMobilePage(tester, PageType.projects.index, ProjectsPage);
    expect(find.text(AppLocalizations.dictionary().projectsGroups), findsOneWidget);
    expect(find.text(AppLocalizations.dictionary().projectsStarred), findsOneWidget);

    await testPageContent(tester, true);
  });

  testWidgets('Should app launch with tablet and able to navigate to Groups page', (tester) async {
    await launchTabletAppForTest(tester);
    await navigateToTargetTabletPage(tester, PageType.projects.index, ProjectsGroupsPage);
    expect(find.text(AppLocalizations.dictionary().projectsGroups), findsAtLeastNWidgets(2));

    await testPageContent(tester, false);
  });

  tearDown(() async {
    ConnectionProvider().fullReset();
    ProjectProvider().fullReset();
    locator.unregister<SubgroupListModel>();
    locator.unregister<ProjectsGroupsModel>();
    locator.unregister<ProjectsStarredModel>();
    locator.unregister<IssueDetailsModel>();
    locator.unregister<PhotoPicker>();

    reset(client);
  });
}

Future<void> testPageContent(tester, bool isMobile) async {
  if (isMobile) {
    await tester.tap(find.text(AppLocalizations.dictionary().projectsGroups));
    await tester.pumpAndSettle();
    expect(find.byType(ProjectsGroupsPage), findsOneWidget);
    expect(find.byKey(const Key('ProjectsPageTabBarView')), findsOneWidget);
    await tester.drag(find.byType(ProjectsGroupsPage), Offset(mobileSize.width, 0.0), warnIfMissed: false);
    await tester.pumpAndSettle();
    expect(find.byType(ProjectsGroupsPage), findsNothing);
    expect(find.byType(ProjectsStarredPage), findsOneWidget);
  }

  final MockDatabase mockDatabase = MockDatabase();
  DbManager.instance().injectDatabaseForTesting(mockDatabase);
  when(mockDatabase.insert(GroupAndProjectEntity.tableName, any)).thenAnswer((_) => Future(() => 1));
  when(mockDatabase.rawQuery("select * from ${GroupAndProjectEntity.tableName} where user_id = 9527 and parent_id = 0 order by type desc, last_activity_at desc"))
      .thenAnswer((_) => Future(() => topLevelDBGroups));

  if (isMobile) {
    await mobileLoginWithSaaSForPage(tester, PageType.projects.index, ProjectsPage);
    await tester.tap(find.text(AppLocalizations.dictionary().projectsGroups));
    await tester.pumpAndSettle();
  } else {
    await tabletLoginWithSaaSForPage(tester, PageType.projects.index, ProjectsGroupsPage);
  }

  expect(find.byType(ProjectsGroupsPage), findsOneWidget);
  expect(find.byType(GroupAndProjectListTile), findsNWidgets(topLevelDBGroups.length));
  expect(find.text(topLevelDBGroups[1]['name'] as String), findsOneWidget);

  when(client.get<dynamic>("/api/v4/groups/118014?all_available=true&page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
  when(client.get<List<dynamic>>("/api/v4/groups/118014/subgroups?all_available=true&page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 274)))
      .thenAnswer((_) => Future(() => Response.of<dynamic>(issueDetailsGraphQLResponse)));
  when(client.get<List<dynamic>>("/api/v4/projects/59893/members/all?page=1&per_page=50")).thenAnswer((_) async => Future.value(Response.of(members)));
  when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 274), useActiveConnection: true))
      .thenAnswer((_) => Future(() => Response.of<dynamic>(issuePostVotesGraphQLResponse)));
  when(client.get("/api/v4/projects/59893/issues/274/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => Response.of([])));

  HttpClient.injectInstanceForTesting(client);

  when(mockDatabase.rawQuery("select * from ${GroupAndProjectEntity.tableName} where user_id = 9527 and parent_id = 118014 order by type desc, last_activity_at desc"))
      .thenAnswer((_) => Future(() => subgroupListPageDBItems));

  await testSubgroupPage(tester);
  await testSubgroupListPage(tester, isMobile: isMobile);
  await testSubgroupIssuesPage(tester);
}
