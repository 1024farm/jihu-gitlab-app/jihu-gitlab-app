import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/system_version_detector.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar/avatar.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar_and_name.dart';
import 'package:jihu_gitlab_app/core/widgets/charts/burn_chart_view.dart';
import 'package:jihu_gitlab_app/core/widgets/charts/time_box_report.dart';
import 'package:jihu_gitlab_app/core/widgets/choice_view.dart';
import 'package:jihu_gitlab_app/core/widgets/permission_low_view.dart';
import 'package:jihu_gitlab_app/core/widgets/project_archived_notice_view.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_page.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_view.dart';
import 'package:jihu_gitlab_app/modules/issues/list/project_issues_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/iteration/details/iteration_model.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/iterations/projects_cadence_iteration_model.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/iterations/projects_cadence_model.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/iterations/projects_iterations_page.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestones_fetcher.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestones_page.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/project_repository_model.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/project_repository_page.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_file_content_fetcher.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_tree_data_fetcher.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../../../finder/svg_finder.dart';
import '../../../mocker/tester.dart';
import '../../../modules/community/community_post_details_page_test.mocks.dart';
import '../../../test_binding_setter.dart';
import '../../../test_data/issue.dart';
import '../../helper/core.dart';
import '../issues/test_data.dart';

Future<void> main() async {
  setUp(() async {
    var detector = MockSystemVersionDetector();
    when(detector.detect()).thenAnswer((_) => Future(() => SystemType.latest));
    SystemVersionDetector.injectInstanceForTesting(detector);
    TestWidgetsFlutterBinding.ensureInitialized();
    locator.registerFactory<IssueDetailsModel>(() => IssueDetailsModel());

    ConnectionProvider().reset(Tester.jihuLabUser());
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfRepositoryTree("ultimate-plan/jihu-gitlab-app/demo", '', ref: "main")))
        .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>({})));
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfCheckRepositoryIsEmpty('ultimate-plan/jihu-gitlab-app/demo')))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(checkRepositoryIsEmptyResponse())));
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfFetchFileContent('ultimate-plan/jihu-gitlab-app/demo', "README.md", "main"))).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets('Should display issues page and able to navigate to details', (tester) async {
    await setUpMobileBinding(tester);
    when(client.post<dynamic>("/api/graphql", getProjectIssuesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo', '', 20)))
        .thenAnswer((_) => Future(() => Response.of(projectIssuesGraphQLResponseData)));
    when(client.post<dynamic>("/api/graphql", getProjectIssuesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo', '', 20, afterCursor: 'end_cursor')))
        .thenAnswer((_) => Future(() => Response.of(projectIssuesNextPageGraphQLResponseData)));
    when(client.get<List<dynamic>>("/api/v4/projects/59893/issues/264/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
    when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 264)))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(issueDetailsGraphQLResponse)));
    when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 264), useActiveConnection: true))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(issuePostVotesGraphQLResponse)));
    when(client.get("/api/v4/projects/59893/issues/264/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => Response.of([])));
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfRepositoryTree("ultimate-plan/jihu-gitlab-app/demo", ''))).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>({})));

    var parameters = {'projectId': 59893, 'groupId': 88966, 'showLeading': false, 'name': 'test project', 'relativePath': 'ultimate-plan/jihu-gitlab-app/demo'};
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: Scaffold(body: ProjectPage(arguments: parameters))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();

    var targetString = 'Feature: 开发人员查看流水线自动合并';
    expect(find.byType(ProjectPage), findsOneWidget);
    expect(find.widgetWithText(Tab, "Issues"), findsOneWidget);
    await tester.tap(find.widgetWithText(Tab, "Issues"));
    await tester.pumpAndSettle();
    expect(find.byType(IssuesPage), findsOneWidget);
    expect(find.byType(IssuesView), findsOneWidget);
    expect(find.byType(TipsView), findsNothing);
    expect(find.byType(ListView), findsOneWidget);
    expect(find.byType(Avatar), findsNWidgets(2));
    expect(find.text('test project'), findsOneWidget);
    expect(find.text(targetString), findsOneWidget);

    await tester.tap(find.byIcon(Icons.add_box_outlined));
    await tester.pumpAndSettle();
    expect(find.text("Create issue"), findsOneWidget);
    await tester.tap(find.text("Create issue"));
    await tester.pumpAndSettle();
    expect(find.byType(IssueCreationPage), findsOneWidget);
    await tester.pumpAndSettle();
    await tester.tap(find.byType(BackButton));
    await tester.pumpAndSettle();
    await tester.tap(find.text(targetString));
    await tester.pumpAndSettle(const Duration(seconds: 1));

    expect(find.byType(IssueDetailsPage), findsOneWidget);
    expect(find.text('回复评论'), findsOneWidget);
    expect(find.text('issue description for test'), findsOneWidget);
    expect(SvgFinder("assets/images/comment.svg"), findsOneWidget);
    expect(SvgFinder("assets/images/operate.svg"), findsOneWidget);
    expect(find.byType(AvatarAndName), findsWidgets);
    expect(find.byWidgetPredicate((widget) => widget is AvatarAndName && widget.username == 'assignee-username'), findsOneWidget);

    await tester.tap(find.byType(BackButton));
    await tester.pumpAndSettle();
    expect(find.byType(IssueDetailsPage), findsNothing);
    expect(find.byType(ProjectPage), findsOneWidget);
  });

  testWidgets('Should display issues page and see archived', (tester) async {
    await setUpDesktopBinding(tester);
    when(client.post<dynamic>("/api/graphql", getProjectIssuesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo', '', 20)))
        .thenAnswer((_) => Future(() => Response.of(projectIssuesGraphQLResponseData)));
    when(client.post<dynamic>("/api/graphql", getProjectIssuesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo', '', 20, afterCursor: 'end_cursor')))
        .thenAnswer((_) => Future(() => Response.of(projectIssuesNextPageGraphQLResponseData)));
    when(client.get<List<dynamic>>("/api/v4/projects/59893/issues/264/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
    when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 264)))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(issueDetailsGraphQLResponse)));
    when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 264), useActiveConnection: true))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(issuePostVotesGraphQLResponse)));
    when(client.get("/api/v4/projects/59893/issues/264/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => Response.of([])));
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfRepositoryTree("ultimate-plan/jihu-gitlab-app/demo", ''))).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>({})));
    when(client.post('/api/graphql', cadenceGraphql('ultimate-plan/jihu-gitlab-app/demo', 20, ""))).thenAnswer((_) => Future(() => Response.of<dynamic>({
          "data": {
            "workspace": {
              "id": "gid://gitlab/Group/88966",
              "iterationCadences": {
                "nodes": [
                  {"id": "gid://gitlab/Iterations::Cadence/316", "title": "单周迭代", "durationInWeeks": 1, "automatic": true, "__typename": "IterationCadence"}
                ],
                "pageInfo": {"hasNextPage": false, "hasPreviousPage": false, "startCursor": "MQ", "endCursor": "MQ", "__typename": "PageInfo"},
              },
            }
          }
        })));
    when(client.post('/api/graphql', cadenceIterationGraphql('ultimate-plan/jihu-gitlab-app/demo', 'gid://gitlab/Iterations::Cadence/316', 20, "")))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(projectIterationItem)));
    when(client.post('/api/graphql', iterationIssuesRequestBody("ultimate-plan/jihu-gitlab-app/demo", "gid://gitlab/Iteration/915")))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(projectIssuesGraphQLResponseData)));
    when(client.get('/api/v4/projects/0/issues/0/discussions?page=1&per_page=50', connection: null, useActiveConnection: true)).thenAnswer((_) => Future(() => Response.of<dynamic>([])));
    HttpClient.injectInstanceForTesting(client);
    var parameters = {'projectId': 59893, 'groupId': 88966, 'showLeading': false, 'name': 'test project', 'relativePath': 'ultimate-plan/jihu-gitlab-app/demo'};
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: Scaffold(body: ProjectPage(arguments: parameters))),
    );
    ProjectProvider().archived = true;
    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();

    var targetString = 'Feature: 开发人员查看流水线自动合并';
    expect(find.byType(ProjectPage), findsOneWidget);
    expect(find.byType(ProjectArchivedNoticeView), findsOneWidget);
    expect(find.widgetWithText(Tab, "Issues"), findsOneWidget);
    await tester.tap(find.widgetWithText(Tab, "Issues"));
    await tester.pumpAndSettle();
    expect(find.byType(IssuesPage), findsOneWidget);
    expect(find.byType(IssuesView), findsOneWidget);
    expect(find.byType(TipsView), findsNothing);
    expect(find.byType(ListView), findsOneWidget);
    expect(find.byType(Avatar), findsNWidgets(2));
    expect(find.text('test project'), findsOneWidget);
    expect(find.text(targetString), findsOneWidget);
    expect(find.byIcon(Icons.add_box_outlined), findsNothing);
    await tester.tap(find.text(targetString));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.byType(IssueDetailsPage), findsOneWidget);
    expect(find.text('回复评论'), findsOneWidget);
    expect(find.byType(ProjectArchivedNoticeView), findsWidgets);
    expect(SvgFinder("assets/images/comment.svg"), findsNothing);
    expect(find.byType(AvatarAndName), findsWidgets);
    expect(find.byWidgetPredicate((widget) => widget is AvatarAndName && widget.username == 'assignee-username'), findsOneWidget);
  });

  testWidgets('Should display PermissionLowView when response no fields error', (tester) async {
    await setUpMobileBinding(tester);
    when(client.post('/api/graphql', cadenceGraphql('ultimate-plan/jihu-gitlab-app/demo', 20, ""))).thenAnswer((_) => Future(() => Response.of<dynamic>(projectIterationCadencesErrorsResp)));
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: const Scaffold(body: ProjectsIterationsPage(fullPath: 'ultimate-plan/jihu-gitlab-app/demo'))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(PermissionLowView), findsOneWidget);
    expect(SvgFinder('assets/images/warning.svg'), findsOneWidget);
    expect(find.text('Feature Not Supported'), findsOneWidget);
    expect(find.textContaining('contact@gitlab.cn'), findsOneWidget);
  });

  testWidgets('Should display PermissionLowView when response no permission error', (tester) async {
    when(client.post('/api/graphql', cadenceGraphql('ultimate-plan/jihu-gitlab-app/demo', 20, "")))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(projectIterationCadencesDataWithNoPermissionErrorsResp)));
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: const Scaffold(body: ProjectsIterationsPage(fullPath: 'ultimate-plan/jihu-gitlab-app/demo'))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(PermissionLowView), findsOneWidget);
    expect(SvgFinder('assets/images/warning.svg'), findsOneWidget);
    expect(find.text('Feature Not Supported'), findsOneWidget);
    expect(find.textContaining('contact@gitlab.cn'), findsOneWidget);
  });

  testWidgets('Should display PermissionLowView when response iterationCadences is null', (tester) async {
    await setUpMobileBinding(tester);
    when(client.post('/api/graphql', cadenceGraphql('ultimate-plan/jihu-gitlab-app/demo', 20, ""))).thenAnswer((_) => Future(() => Response.of<dynamic>(projectIterationCadencesNullResp)));
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: const Scaffold(body: ProjectsIterationsPage(fullPath: 'ultimate-plan/jihu-gitlab-app/demo'))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(PermissionLowView), findsOneWidget);
    expect(SvgFinder('assets/images/warning.svg'), findsOneWidget);
    expect(find.text('Feature Not Supported'), findsOneWidget);
    expect(find.textContaining('contact@gitlab.cn'), findsOneWidget);
  });

  testWidgets('Should go to iteration details page', (tester) async {
    when(client.post('/api/graphql', requestBodyOfIterationInfo('ultimate-plan/jihu-gitlab-app/demo', 'gid://gitlab/Iteration/915')))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(iterationInfoResp)));
    when(client.post('/api/graphql', cadenceGraphql('ultimate-plan/jihu-gitlab-app/demo', 20, ""))).thenAnswer((_) => Future(() => Response.of<dynamic>(projectIterationCadencesResp)));
    when(client.post('/api/graphql', cadenceIterationGraphql('ultimate-plan/jihu-gitlab-app/demo', 'gid://gitlab/Iterations::Cadence/316', 20, "")))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(projectIterationItem)));
    when(client.post('/api/graphql', iterationIssuesRequestBody("ultimate-plan/jihu-gitlab-app/demo", "gid://gitlab/Iteration/915")))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(projectIssuesGraphQLResponseData)));
    when(client.get<List<dynamic>>("/api/v4/projects/59893/issues/276/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
    when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 276)))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(issueDetailsGraphQLResponse)));
    when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 276), useActiveConnection: true))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(issuePostVotesGraphQLResponse)));
    when(client.get("/api/v4/projects/59893/issues/276/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => Response.of([])));
    await setUpDesktopBinding(tester);
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
      child: MaterialApp(
        onGenerateRoute: onGenerateRoute,
        localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
        home: const Scaffold(
          body: ProjectsIterationsPage(fullPath: 'ultimate-plan/jihu-gitlab-app/demo'),
        ),
      ),
    );
    await tester.pumpWidget(builder);
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.text('单周迭代'), findsOneWidget);
    expect(find.textContaining('Nov 14, 2022'), findsOneWidget);
    await tester.tap(find.textContaining('Nov 14, 2022'));
    await tester.pumpAndSettle();
    var finder = find.byType(HtmlWidget);
    expect(finder, findsOneWidget);
    expect(tester.widget<HtmlWidget>(finder).html, "迭代目标");
    expect(find.byType(BurnChartView), findsOneWidget);
    expect(find.text("Burndown chart"), findsOneWidget);
    expect(find.byType(ChoiceView<BurnChartDataType>), findsNWidgets(2));
    expect(find.text("Issues"), findsNWidgets(2));
    expect(find.text("Issue weight"), findsNWidgets(2));
    await tester.tap(find.text("Issue weight").first);
    await tester.pumpAndSettle();
    expect(find.textContaining('Nov 14, 2022'), findsOneWidget);
    await tester.scrollUntilVisible(find.text('Feature：交互优化：创建议题时，如果只有一个项目，则跳过「选择项目」'), 800);
    expect(find.text('Feature：交互优化：创建议题时，如果只有一个项目，则跳过「选择项目」'), findsOneWidget);
    await tester.tap(find.text('Feature：交互优化：创建议题时，如果只有一个项目，则跳过「选择项目」'));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.byType(IssueDetailsPage), findsOneWidget);
  });

  testWidgets('Should display readme file in the tab of repository', (tester) async {
    await setUpMobileBinding(tester);
    when(client.post<dynamic>("/api/graphql", buildRequestBodyOfFetchFileContent('ultimate-plan/jihu-gitlab-app/demo', "README.md", "main"))).thenAnswer((_) => Future(() => Response.of({
          "data": {
            "project": {
              "repository": {
                "blobs": {
                  "edges": [
                    {
                      "node": {"rawTextBlob": "The is the content of readme"}
                    }
                  ]
                }
              }
            }
          }
        })));

    var parameters = {'projectId': 59893, 'groupId': 88966, 'showLeading': false, 'name': 'test project', 'relativePath': 'ultimate-plan/jihu-gitlab-app/demo'};
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: Scaffold(body: ProjectPage(arguments: parameters))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(ProjectPage), findsOneWidget);
    expect(find.widgetWithText(Tab, "Repository"), findsOneWidget);
    expect(find.byType(ProjectRepositoryPage), findsOneWidget);
    expect(SvgFinder("assets/images/readme.svg"), findsOneWidget);
    expect(find.text("README.md"), findsOneWidget);
    expect(find.byType(WebViewWidget), findsOneWidget);
  });

  testWidgets('Should display the project milestones tab', (tester) async {
    await setUpMobileBinding(tester);
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfCheckRepositoryIsEmpty('ultimate-plan/jihu-gitlab-app/demo')))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(checkRepositoryIsEmptyResponse(empty: true))));

    when(client.post<dynamic>('/api/graphql', projectMilestoneRequestBody('ultimate-plan/jihu-gitlab-app/demo', 20, ""))).thenAnswer((_) => Future(() => Response.of<dynamic>({})));

    var parameters = {'projectId': 59893, 'groupId': 88966, 'showLeading': false, 'name': 'test project', 'relativePath': 'ultimate-plan/jihu-gitlab-app/demo'};
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: Scaffold(body: ProjectPage(arguments: parameters))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(ProjectPage), findsOneWidget);
    expect(find.widgetWithText(Tab, "Milestones"), findsOneWidget);
    await tester.ensureVisible(find.widgetWithText(Tab, "Milestones"));
    await tester.tap(find.widgetWithText(Tab, "Milestones"));
    await tester.pumpAndSettle();
    expect(find.byType(ProjectMilestonesPage), findsOneWidget);
  });

  tearDown(() async {
    ConnectionProvider().fullReset();
    ProjectProvider().fullReset();
    locator.unregister<IssueDetailsModel>();
    reset(client);
  });
}

Map<String, dynamic> checkRepositoryIsEmptyResponse({bool exists = true, bool empty = false}) {
  return {
    "data": {
      "project": {
        "id": "gid://gitlab/Project/59893",
        "name": "极狐 GitLab App",
        "repository": {"exists": exists, "empty": empty, "rootRef": "main"}
      }
    }
  };
}

var projectIterationCadencesResp = {
  "data": {
    "workspace": {
      "id": "gid://gitlab/Group/88966",
      "iterationCadences": {
        "nodes": [
          {"id": "gid://gitlab/Iterations::Cadence/316", "title": "单周迭代", "durationInWeeks": 1, "automatic": true, "__typename": "IterationCadence"}
        ],
        "pageInfo": {"hasNextPage": false, "hasPreviousPage": false, "startCursor": "MQ", "endCursor": "MQ", "__typename": "PageInfo"},
      },
    }
  }
};

var projectIterationCadencesNullResp = {
  "data": {
    "workspace": {
      "id": "gid://gitlab/Group/88966",
      "iterationCadences": null,
    }
  }
};

var projectIterationCadencesDataWithNoPermissionErrorsResp = {
  "data": {
    "workspace": {"id": "gid://gitlab/Project/72529", "iterationCadences": null}
  },
  "errors": [
    {
      "message": "The resource that you are attempting to access does not exist or you don't have permission to perform this action",
      "locations": [
        {"line": 4, "column": 3}
      ],
      "path": ["workspace", "iterationCadences"]
    }
  ]
};

var projectIterationCadencesErrorsResp = {
  "errors": [
    {
      "message": "Field 'iterationCadences' doesn't exist on type 'Project'",
      "locations": [
        {"line": 4, "column": 3}
      ],
      "path": ["query projectIterationCadences", "workspace", "iterationCadences"],
      "extensions": {"code": "undefinedField", "typeName": "Project", "fieldName": "iterationCadences"}
    },
    {
      "message": "Variable \$beforeCursor is declared by projectIterationCadences but not used",
      "locations": [
        {"line": 1, "column": 1}
      ],
      "path": ["query projectIterationCadences"],
      "extensions": {"code": "variableNotUsed", "variableName": "beforeCursor"}
    },
    {
      "message": "Variable \$afterCursor is declared by projectIterationCadences but not used",
      "locations": [
        {"line": 1, "column": 1}
      ],
      "path": ["query projectIterationCadences"],
      "extensions": {"code": "variableNotUsed", "variableName": "afterCursor"}
    },
    {
      "message": "Variable \$firstPageSize is declared by projectIterationCadences but not used",
      "locations": [
        {"line": 1, "column": 1}
      ],
      "path": ["query projectIterationCadences"],
      "extensions": {"code": "variableNotUsed", "variableName": "firstPageSize"}
    },
    {
      "message": "Variable \$lastPageSize is declared by projectIterationCadences but not used",
      "locations": [
        {"line": 1, "column": 1}
      ],
      "path": ["query projectIterationCadences"],
      "extensions": {"code": "variableNotUsed", "variableName": "lastPageSize"}
    }
  ]
};

Map<String, dynamic> iterationInfoResp = {
  "data": {
    "iteration": {
      "id": "gid://gitlab/Iteration/1608",
      "title": null,
      "dueDate": "2023-04-09",
      "startDate": "2023-04-03",
      "descriptionHtml": "迭代目标",
      "report": {
        "burnupTimeSeries": [
          {"date": "2023-04-03", "completedCount": 0, "scopeCount": 9, "completedWeight": 0, "scopeWeight": 7},
          {"date": "2023-04-04", "completedCount": 5, "scopeCount": 10, "completedWeight": 4, "scopeWeight": 7},
          {"date": "2023-04-05", "completedCount": 7, "scopeCount": 10, "completedWeight": 5, "scopeWeight": 7},
          {"date": "2023-04-06", "completedCount": 10, "scopeCount": 10, "completedWeight": 7, "scopeWeight": 7},
          {"date": "2023-04-07", "completedCount": 9, "scopeCount": 12, "completedWeight": 6, "scopeWeight": 10}
        ],
        "stats": {
          "total": {"count": 12, "weight": 10},
          "complete": {"count": 9, "weight": 6},
          "incomplete": {"count": 3, "weight": 4}
        }
      }
    }
  }
};
