part of 'projects_page_test.dart';

Future<void> testSubgroupListPage(tester, {required bool isMobile}) async {
  await tester.tap(find.text(AppLocalizations.dictionary().projectsChildren));
  await tester.pumpAndSettle();
  expect(find.byType(SubgroupListPage), findsOneWidget);
  expect(find.byType(ListView), findsOneWidget);
  expect(find.text('Learn GitLab'), findsOneWidget);

  await issueDetailsTest(tester, isMobile: isMobile);

  await tester.tap(find.byType(BackButton));
  for (int i = 0; i < 5; i++) {
    await tester.pump(const Duration(seconds: 1));
  }
  expect(find.byType(ProjectPage), findsNothing);
}

Future<void> issueDetailsTest(tester, {required bool isMobile}) async {
  when(client.post<dynamic>("/api/graphql", getProjectIssuesGraphQLRequestBody('highsof-t/learn-gitlab', '', 20))).thenAnswer((_) => Future(() => Response.of(projectIssuesGraphQLResponseData)));
  when(client.get<List<dynamic>>("/api/v4/projects/59893/issues/274/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  HttpClient.injectInstanceForTesting(client);
  var targetString = 'Feature: 待办列表无数据时，点击页面任意位置刷新';

  expect(find.byType(ProjectPage), findsNothing);
  await tester.tap(find.text('Learn GitLab'));
  await tester.pumpAndSettle();
  expect(find.byType(ProjectPage), findsOneWidget);
  expect(find.widgetWithText(Tab, "Issues"), findsOneWidget);
  await tester.tap(find.widgetWithText(Tab, "Issues"));
  await tester.pumpAndSettle();
  expect(find.byType(IssuesPage), findsOneWidget);
  expect(find.byType(IssuesView), findsOneWidget);
  expect(find.byType(TipsView), findsNothing);
  expect(find.byType(ListView), findsOneWidget);
  expect(find.text(targetString), findsOneWidget);
  verify(client.post<dynamic>("/api/graphql", getProjectIssuesGraphQLRequestBody('highsof-t/learn-gitlab', '', 20))).called(1);

  if (isMobile) {
    expect(find.byType(IssueDetailsPage), findsNothing);
    await tester.tap(find.text(targetString));
    await tester.pumpAndSettle();
    expect(find.byType(IssueDetailsPage), findsOneWidget);
    await tester.tap(find.byType(BackButton));
    for (int i = 0; i < 5; i++) {
      await tester.pump(const Duration(seconds: 1));
    }
    expect(find.byType(IssueDetailsPage), findsNothing);
    expect(find.text(targetString), findsOneWidget);
  }
}
