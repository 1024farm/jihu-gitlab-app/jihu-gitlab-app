part of 'projects_page_test.dart';

Future<void> testSubgroupIssuesPage(tester) async {
  when(client.post<dynamic>("/api/graphql", getGroupIssuesGraphQLRequestBody('highsof-t', '', 20))).thenAnswer((_) => Future(() => Response.of(groupIssuesGraphQLNextPageResponseData)));
  when(client.post<dynamic>("/api/graphql", getGroupIssuesGraphQLRequestBody('highsof-t', '', 20, afterCursor: 'end_cursor')))
      .thenAnswer((_) => Future(() => Response.of(groupIssuesGraphQLNextPageResponseData)));
  when(client.post<dynamic>("/api/graphql", getGroupProjectsGraphQLRequestBody('highsof-t', ['gid://gitlab/Project/59893', 'gid://gitlab/Project/72936'])))
      .thenAnswer((_) => Future(() => Response.of(groupProjectsGraphQLResponseData)));
  HttpClient.injectInstanceForTesting(client);

  await tester.tap(find.text(AppLocalizations.dictionary().projectsIssues));
  await tester.pumpAndSettle();
  expect(find.byType(SubgroupIssuesPage), findsOneWidget);
  expect(find.byType(IssuesPage), findsOneWidget);
  expect(find.byType(IssuesView), findsOneWidget);
  expect(find.byType(TipsView), findsNothing);
  expect(find.byType(ListView), findsOneWidget);
  expect(find.text('Feature: 用户可以为评论点赞/反对/取消2'), findsOneWidget);

  var targetString = 'Feature: 用户查看不同状态的议题数量 01';
  final listFinder = find.byType(Scrollable).last;
  await tester.scrollUntilVisible(find.text(targetString), 500.0, scrollable: listFinder);
  for (int i = 0; i < 5; i++) {
    await tester.pump(const Duration(seconds: 1));
  }
  await issueDetailsPageNavigationTest(tester, targetString);
}

Future<void> issueDetailsPageNavigationTest(tester, String targetString) async {
  when(client.get<List<dynamic>>("/api/v4/projects/59893/issues/451/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  when(client.get<List<dynamic>>("/api/v4/projects/59893/labels?page=1&per_page=50")).thenAnswer((_) => Future.value(Response.of<List<dynamic>>(projectLabelsFirstPageResp)));
  when(client.get<List<dynamic>>("/api/v4/projects/59893/labels?page=2&per_page=50")).thenAnswer((_) => Future.value(Response.of<List<dynamic>>(projectLabelsNextPageResponse)));
  when(client.get("/api/v4/projects/59893")).thenAnswer((_) => Future(() => Response.of(jiHuProjectData)));
  when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 451)))
      .thenAnswer((_) => Future(() => Response.of<dynamic>(issueDetailsWithNoneLabelsGraphQLResponse)));
  when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 451), useActiveConnection: true))
      .thenAnswer((_) => Future(() => Response.of<dynamic>(issuePostVotesGraphQLResponse)));
  when(client.get("/api/v4/projects/59893/issues/451/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => Response.of([])));

  ConnectionProvider().reset(Tester.jihuLabUser());
  ConnectionProvider().notifyChanged();
  expect(ConnectionProvider.isSelfManagedGitLab, isFalse);
  expect(ConnectionProvider.authorized, isTrue);

  expect(find.byType(IssueDetailsPage), findsNothing);
  await tester.tap(find.text(targetString));
  for (int i = 0; i < 5; i++) {
    await tester.pump(const Duration(seconds: 1));
  }
  await issueDetailsPageContentTest(tester, client);

  await tester.tap(find.byType(BackButton));
  for (int i = 0; i < 5; i++) {
    await tester.pump(const Duration(seconds: 1));
  }
  expect(find.byType(IssueDetailsPage), findsNothing);
  expect(find.text(targetString), findsOneWidget);
  verify(client.post<dynamic>("/api/graphql", getGroupIssuesGraphQLRequestBody('highsof-t', '', 20))).called(1);
}
