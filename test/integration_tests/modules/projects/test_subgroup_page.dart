part of 'projects_page_test.dart';

Future<void> testSubgroupPage(tester) async {
  when(client.get<dynamic>("/api/v4/groups/118014?all_available=true&page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
  when(client.get<List<dynamic>>("/api/v4/groups/118014/subgroups?all_available=true&page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  HttpClient.injectInstanceForTesting(client);

  await tester.tap(find.text(topLevelDBGroups[0]['name'] as String));
  await tester.pumpAndSettle();

  expect(find.byType(SubgroupPage), findsOneWidget);
  expect(find.text(AppLocalizations.dictionary().projectsChildren), findsOneWidget);
  expect(find.text(AppLocalizations.dictionary().projectsIssues), findsOneWidget);
  expect(find.text(AppLocalizations.dictionary().iterations), findsOneWidget);
}
