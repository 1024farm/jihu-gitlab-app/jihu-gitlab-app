import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/widgets/photo_picker.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';
import 'package:mockito/mockito.dart';

import '../../../test_data/discussion.dart';
import '../../../test_data/issue.dart';
import '../../../test_data/project.dart';
import '../../../test_data/to_dos.dart';
import '../../helper/core.dart';
import '../../helper/mobile_test_helper/root.dart';
import '../../helper/tablet_test_helper/root.dart';
import '../merge_request/test_merge_request_details.dart';

void main() {
  setUp(() async {
    WidgetsFlutterBinding.ensureInitialized();
    setupLocator();

    when(client.postWithConnection<dynamic>("/api/graphql", getCommunityTypesGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/faq"), any))
        .thenAnswer((_) => Future(() => Response.of(faqTypesResponse)));
    when(client.get("/api/v4/users/0/starred_projects?page=1&per_page=50")).thenAnswer((realInvocation) => Future(() => Response.of([])));
    when(client.get("/api/v4/users/9527/starred_projects?page=1&per_page=50")).thenAnswer((realInvocation) => Future(() => Response.of([])));
    when(client.get<Map<String, dynamic>>("/api/v4/projects/72936/issues/6")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(issueData)));
    when(client.get<Map<String, dynamic>>("/api/v4/projects/72936")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(projectData)));
    when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>(todoPendingResponseData)));
    when(client.get<List<dynamic>>("/api/v4/projects/72936/issues/6/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>(discussionsData)));
    when(client.getWithConnection("/api/v4/projects/89335", any)).thenAnswer((_) => Future(() => Response.of({
          "permissions": {
            "project_access": {"access_level": 50}
          }
        })));
    when(client.get("/api/v4/groups?top_level_only=true&page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<Map<String, dynamic>>>([])));
    when(client.get<List<Map<String, dynamic>>>("/api/v4/projects/59893/merge_requests/84/diffs?page=1&per_page=20")).thenAnswer((_) => Future(() => Response.of<List<Map<String, dynamic>>>([])));
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets('Should app launch with mobile and able to navigate to ToDos page', (tester) async {
    await launchMobileAppForTest(tester);
    await navigateToTargetMobilePage(tester, PageType.todo.index, ToDosPage);
    await pageContentTest(tester, isMobile: true);
  });

  testWidgets('Should app launch with tablet and able to navigate to ToDos page', (tester) async {
    await launchTabletAppForTest(tester);
    await navigateToTargetTabletPage(tester, PageType.todo.index, ToDosPage);
    await pageContentTest(tester, isMobile: false);
  });

  tearDown(() async {
    ConnectionProvider().fullReset();

    locator.unregister<SubgroupListModel>();
    locator.unregister<ProjectsGroupsModel>();
    locator.unregister<ProjectsStarredModel>();
    locator.unregister<IssueDetailsModel>();
    locator.unregister<PhotoPicker>();
  });
}

Future<void> pageContentTest(tester, {required bool isMobile}) async {
  expect(find.text(AppLocalizations.dictionary().todo), findsOneWidget);
  expect(find.text(AppLocalizations.dictionary().done), findsOneWidget);
  expect(find.byKey(const Key('ToDosPageTabBarView')), findsOneWidget);
  if (isMobile) {
    await mobileLoginWithSaaSForPage(tester, PageType.todo.index, ToDosPage, onMock: onToDoRequestMock);
  } else {
    await tabletLoginWithSaaSForPage(tester, PageType.todo.index, ToDosPage, onMock: onToDoRequestMock);
  }

  var targetString = 'merge request item';
  await toDosListTest(tester, targetString, isMobile: isMobile);
  await doneListTest(tester, targetString);
}

Future<void> doneListTest(tester, String targetString) async {
  await tester.tap(find.text(AppLocalizations.dictionary().done));
  await tester.pumpAndSettle();
  expect(find.text('pending test name'), findsNothing);
  expect(find.text('Issue comment test'), findsOneWidget);
  await mergeRequestTest(tester, targetString, isDraft: false);
}

Future<void> toDosListTest(tester, String targetString, {required bool isMobile}) async {
  expect(find.text('pending test name'), findsOneWidget);
  expect(find.text('Issue comment test'), findsNothing);

  await mergeRequestTest(tester, targetString, isDraft: true);
  if (isMobile) {
    // Will show [Mark as done] alert after back from Merge Request Page
    expect(find.byType(CupertinoAlertDialog), findsOneWidget);
    expect(find.text(AppLocalizations.dictionary().markTodoAsDoneTitle), findsOneWidget);
    expect(find.text(AppLocalizations.dictionary().markTodoAsDoneContent), findsOneWidget);
    expect(find.text(AppLocalizations.dictionary().cancel), findsOneWidget);
    expect(find.text(AppLocalizations.dictionary().yes), findsOneWidget);
    await tester.tap(find.text(AppLocalizations.dictionary().cancel));
    await tester.pumpAndSettle();
    expect(find.byType(CupertinoAlertDialog), findsNothing);
  }
}

void onToDoRequestMock() {
  when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>(todoPendingResponseData)));
  when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=done")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>(todoDoneResponseData)));
}
