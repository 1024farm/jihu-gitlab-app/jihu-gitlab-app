import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/home/home_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/photo_picker.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/community/community_post_list_view.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';

import '../../../finder/text_field_hint_text_finder.dart';
import '../../helper/core.dart';
import '../../helper/mobile_test_helper/root.dart';
import '../../helper/tablet_test_helper/root.dart';
import 'community_ai_test_helper.dart';
import 'community_page_modules.dart';
import 'community_post_creation_test_helper.dart';
import 'community_post_item_detail_helper.dart';

void main() {
  setUp(() async {
    WidgetsFlutterBinding.ensureInitialized();
    setupLocator();
    mockCommunity(client);
  });

  testWidgets('Should app launch with mobile and able to navigate to ToDos page', (tester) async {
    await launchMobileAppForTest(tester);
    await navigateToTargetMobilePage(tester, PageType.faq.index, CommunityPage);
    await pageContentTest(tester, true);
  });

  testWidgets('Should app launch with tablet and able to navigate to ToDos page', (tester) async {
    await launchTabletAppForTest(tester);
    await navigateToTargetTabletPage(tester, PageType.faq.index, CommunityPage);
    await pageContentTest(tester, false);
  });

  tearDown(() async {
    ConnectionProvider().fullReset();
    ProjectProvider().fullReset();
    locator.unregister<SubgroupListModel>();
    locator.unregister<ProjectsGroupsModel>();
    locator.unregister<ProjectsStarredModel>();
    locator.unregister<IssueDetailsModel>();
    locator.unregister<PhotoPicker>();
  });
}

Future<void> pageContentTest(WidgetTester tester, bool isMobile) async {
  expect(find.byType(CommunityPage), findsOneWidget);
  expect(find.text(AppLocalizations.dictionary().community), findsWidgets);
  expect(find.byType(CommunityPage), findsOneWidget);
  expect(find.widgetWithText(Tab, 'All'), findsOneWidget);
  expect(find.byType(CommunityPostListView), findsOneWidget);
  expect(find.text('Why remote?'), findsWidgets);
  expect(find.widgetWithText(Tab, 'FAQ'), findsOneWidget);
  expect(find.widgetWithText(Tab, 'WFH'), findsOneWidget);

  // Remove focus from textField
  expect(tester.testTextInput.isVisible, isFalse);
  await tester.tap(TextFieldHintTextFinder('Search'));
  await tester.pumpAndSettle();
  expect(tester.testTextInput.isVisible, isTrue);
  await tester.tap(find.widgetWithText(HomeAppBar, 'Community'));
  await tester.pumpAndSettle();
  expect(tester.testTextInput.isVisible, isFalse);

  await tester.tap(find.widgetWithText(Tab, 'All'));
  await tester.pumpAndSettle();
  await communityNavigateToAIPageTest(tester, isMobile);
  await communityPostDetailTest(isMobile, tester);
  await communityPostCreationTest(isMobile, tester);
}
