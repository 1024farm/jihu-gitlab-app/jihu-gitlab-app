import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_gq_request_body.dart';
import 'package:mockito/mockito.dart';

import '../../../core/net/http_request_test.mocks.dart';
import '../../../test_data/issue.dart';
import '../../../test_data/user.dart';

void mockCommunity(MockHttpClient client) {
  when(client.postWithConnection<dynamic>("/api/graphql", getCommunityPostListGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', ['lang::en'], '', '', 20), any))
      .thenAnswer((_) => Future(() => Response.of(allCommunityPostListGraphQLResponse)));
  when(client.postWithConnection('/api/graphql', getCommunityTypesGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/faq"), any)).thenAnswer((_) => Future(() => Response.of(faqTypesResponse)));
  when(client.postWithConnection('/api/graphql', getCommunityPostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', 238), any))
      .thenAnswer((_) => Future(() => Response.of<dynamic>(communityPostVotesGraphQLResponseData)));
  when(client.getWithConnection("/api/v4/projects/89335/issues/238/award_emoji?per_page=100&page=1", any)).thenAnswer((_) => Future(() => Response.of([])));
  when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  when(client.getWithHeader<Map<String, dynamic>>("https://example.com/api/v4/user", any)).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
  when(client.get("/api/v4/users/9529/starred_projects?page=1&per_page=50")).thenAnswer((realInvocation) => Future(() => Response.of([])));
  HttpClient.injectInstanceForTesting(client);
}

var allCommunityPostListGraphQLResponse = {
  "data": {
    "project": {
      "issues": {
        "nodes": [
          {
            "id": "gid://gitlab/Issue/326489",
            "iid": "238",
            "title": "Why remote?",
            "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/faq/-/issues/238",
            "confidential": false,
            "state": "opened",
            "createdAt": "2023-02-21T09:19:40+08:00",
            "author": {"name": "yajie xue", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png"},
            "labels": {
              "nodes": [
                {"id": "gid://gitlab/ProjectLabel/74785", "title": "for::geek", "description": "", "color": "#6699cc", "textColor": "#FFFFFF"},
                {"id": "gid://gitlab/ProjectLabel/72406", "title": "lang::en", "description": "英文", "color": "#9400d3", "textColor": "#FFFFFF"},
                {"id": "gid://gitlab/ProjectLabel/72059", "title": "type::WFH", "description": "远程办公", "color": "#6699cc", "textColor": "#FFFFFF"}
              ]
            }
          },
          {
            "id": "gid://gitlab/Issue/326473",
            "iid": "237",
            "title": "Why Digital Transformation?",
            "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/faq/-/issues/237",
            "confidential": false,
            "state": "opened",
            "createdAt": "2023-02-21T09:16:59+08:00",
            "author": {"name": "yajie xue", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png"},
            "labels": {
              "nodes": [
                {"id": "gid://gitlab/ProjectLabel/72406", "title": "lang::en", "description": "英文", "color": "#9400d3", "textColor": "#FFFFFF"},
                {"id": "gid://gitlab/ProjectLabel/72395", "title": "type::FAQ", "description": "问答", "color": "#6699cc", "textColor": "#FFFFFF"}
              ]
            }
          }
        ],
        "pageInfo": {"hasNextPage": false, "endCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMy0wMi0yMSAwOToxNjo1OS41MjQ2OTAwMDAgKzA4MDAiLCJpZCI6IjMyNjQ3MyJ9"}
      }
    }
  }
};

var projectsMembersGraphQLResponse = {
  "data": {
    "project": {
      "projectMembers": {
        "pageInfo": {"hasNextPage": false, "endCursor": "eyJ1c2Vyc19tYXRjaF9wcmlvcml0eSI6IjEiLCJ1c2Vyc19uYW1lIjoiUmF5bW9uZCBMaWFvIiwidXNlcnNfaWQiOiIzMDE5MiJ9"},
        "nodes": [
          {
            "id": "gid://gitlab/GroupMember/102615",
            "accessLevel": {"integerValue": 50, "stringValue": "OWNER"},
            "user": {
              "id": "gid://gitlab/User/30192",
              "username": "raymond-liao",
              "name": "Raymond Liao",
              "state": "active",
              "avatarUrl": "/uploads/-/system/user/avatar/30192/avatar.png",
              "webUrl": "https://jihulab.com/raymond-liao",
              "publicEmail": ""
            }
          },
          {
            "id": "gid://gitlab/GroupMember/74543",
            "accessLevel": {"integerValue": 30, "stringValue": "DEVELOPER"},
            "user": {
              "id": "gid://gitlab/User/29355",
              "username": "perity",
              "name": "miaolu",
              "state": "active",
              "avatarUrl": "/uploads/-/system/user/avatar/29355/avatar.png",
              "webUrl": "https://jihulab.com/perity",
              "publicEmail": ""
            }
          }
        ]
      }
    }
  }
};

var accessedProjectsMembersGraphQLResponse = {
  "data": {
    "project": {
      "projectMembers": {
        "pageInfo": {"hasNextPage": false, "endCursor": "eyJ1c2Vyc19tYXRjaF9wcmlvcml0eSI6IjEiLCJ1c2Vyc19uYW1lIjoiUmF5bW9uZCBMaWFvIiwidXNlcnNfaWQiOiIzMDE5MiJ9"},
        "nodes": [
          {
            "id": "gid://gitlab/GroupMember/102615",
            "accessLevel": {"integerValue": 50, "stringValue": "OWNER"},
            "user": {
              "id": "gid://gitlab/User/9527",
              "username": "tester",
              "name": "tester",
              "state": "active",
              "avatarUrl": "/uploads/-/system/user/avatar/30192/avatar.png",
              "webUrl": "https://jihulab.com/tester",
              "publicEmail": ""
            }
          },
          {
            "id": "gid://gitlab/GroupMember/74543",
            "accessLevel": {"integerValue": 30, "stringValue": "DEVELOPER"},
            "user": {
              "id": "gid://gitlab/User/29355",
              "username": "perity",
              "name": "miaolu",
              "state": "active",
              "avatarUrl": "/uploads/-/system/user/avatar/29355/avatar.png",
              "webUrl": "https://jihulab.com/perity",
              "publicEmail": ""
            }
          }
        ]
      }
    }
  }
};

Map<String, dynamic> communityPostVotesGraphQLResponseData = {
  "data": {
    "project": {
      "issue": {"downvotes": 0, "upvotes": 0}
    }
  }
};
