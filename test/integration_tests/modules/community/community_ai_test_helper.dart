import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/db_manager.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/ai/ai_page.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message_entity.dart';
import 'package:jihu_gitlab_app/modules/community/community_page.dart';
import 'package:mockito/mockito.dart';

import '../../../finder/text_field_hint_text_finder.dart';
import '../../../modules/ai/ai_page_test.mocks.dart';
import '../../helper/core.dart';

Future<void> communityNavigateToAIPageTest(WidgetTester tester, bool isMobile) async {
  AssetImage buttonIconImage = const AssetImage('assets/images/jixiaohu.png');
  await tester.tap(find.image(buttonIconImage), warnIfMissed: false);
  await tester.pumpAndSettle();
  expect(find.byType(AIPage), findsOneWidget);
  expect(find.text(AppLocalizations.dictionary().aiEnterQuestion), findsOneWidget);

  await tester.tap(find.byType(BackButton));
  await tester.pumpAndSettle();

  expect(find.byType(AIPage), findsNothing);
  expect(find.byType(CommunityPage), findsOneWidget);
  expect(find.byType(ListView), findsWidgets);

  when(client.postWithHeader('https://gitlablang.cognitiveservices.azure.com/language/:query-knowledgebases?projectName=jihu&api-version=2021-10-01&deploymentName=production', {
    "top": 1,
    "question": "text for test ai default message",
    "includeUnstructuredSources": true,
    "confidenceScoreThreshold": "0.5",
    "answerSpanRequest": {"enable": true, "topAnswersWithSpan": 1, "confidenceScoreThreshold": "0.5"}
  }, {
    'Ocp-Apim-Subscription-Key': ""
  })).thenAnswer((_) => Future(() => Response.of({
        "answers": [
          {
            "questions": ["GitLab CI 可以公司统一流程，多个项目共用一个流水线吗？"],
            "answer": "Yes!",
            "confidenceScore": 1.0,
            "id": 102,
            "source": "jihulab-presales-playground-faq_issues_2023-01-05.xlsx",
            "metadata": {},
            "dialog": {"isContextOnly": false, "prompts": []}
          }
        ]
      })));
  HttpClient.injectInstanceForTesting(client);

  final MockDatabase mockDatabase = MockDatabase();
  DbManager.instance().injectDatabaseForTesting(mockDatabase);
  when(mockDatabase.insert(MessageEntity.tableName, any)).thenAnswer((_) => Future(() => 1));
  when(mockDatabase.update(MessageEntity.tableName, any, where: anyNamed("where"), whereArgs: anyNamed("whereArgs"))).thenAnswer((_) => Future(() => 1));

  expect(TextFieldHintTextFinder('Search'), findsOneWidget);
  await tester.enterText(TextFieldHintTextFinder('Search'), 'text for test ai default message');
  await tester.pumpAndSettle();
  await tester.tap(find.image(buttonIconImage), warnIfMissed: false);
  await tester.pumpAndSettle();
  expect(find.byType(AIPage), findsOneWidget);
  expect(find.text('Enter your question'), findsOneWidget);
  expect(find.text('text for test ai default message'), findsOneWidget);

  await tester.tap(find.byType(BackButton));
  await tester.pumpAndSettle();
  expect(find.byType(AIPage), findsNothing);
  expect(find.byType(CommunityPage), findsOneWidget);
  expect(find.byType(ListView), findsWidgets);

  expect(find.text('text for test ai default message'), findsOneWidget);
}
