import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/global_keys.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/widgets/account/account_page.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/user/user_info_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/community/community_page.dart';
import 'package:jihu_gitlab_app/modules/community/community_post_details_page.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_gq_request_body.dart';
import 'package:mockito/mockito.dart';

import '../../../finder/semantics_label_finder.dart';
import '../../../finder/svg_finder.dart';
import '../../../mocker/tester.dart';
import '../../../test_data/community.dart';
import '../../../test_data/discussion.dart';
import '../../../test_data/user.dart';
import '../../helper/core.dart';

Future<void> communityPostDetailTest(bool isMobile, WidgetTester tester) async {
  const String testItemTitle = 'Why remote?';
  expect(find.text(testItemTitle), findsOneWidget);

  when(client.postWithConnection('/api/graphql', getCommunityDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', 238), any))
      .thenAnswer((_) => Future(() => Response.of<dynamic>(communityDetailsGraphQLResponse)));
  when(client.getWithConnection<Map<String, dynamic>>("/api/v4/projects/89335", any)).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>({
        "id": 89335,
        "description": "技术问答、远程工作机会",
        "name": "FAQ",
        "name_with_namespace": "旗舰版演示 / 极狐 GitLab App 产品线 / FAQ",
        "path": "faq",
        "path_with_namespace": "ultimate-plan/jihu-gitlab-app/faq"
      })));
  when(client.getWithConnection("/api/v4/projects/89335/issues/238/discussions?page=1&per_page=50", any)).thenAnswer((_) => Future(() => Response.of([])));
  if (isMobile) {
    await mobileCommunityDetailTest(tester, testItemTitle);
  } else {
    await tabletCommunityDetailTest(tester, testItemTitle);
  }
}

Future<void> tabletCommunityDetailTest(tester, String testItemTitle) async {
  // Display the details for not login user
  await tester.tap(find.text(testItemTitle));
  await tester.pumpAndSettle();
  expect(find.widgetWithText(CommonAppBar, 'WFH'), findsOneWidget);
  expect(find.text('Community detail for test'), findsOneWidget);
  expect(SvgFinder("assets/images/external-link.svg"), findsOneWidget);

  // Display the details for login user
  await tabletPrivateManagedLoginDetailTest(tester, testItemTitle);
  await tabletSaaSLoginDetailTest(tester, testItemTitle);
}

Future<void> tabletPrivateManagedLoginDetailTest(tester, String testItemTitle) async {
  await tabletCommunityDetailTestHelper(tester, testItemTitle, false);
}

Future<void> tabletSaaSLoginDetailTest(tester, String testItemTitle) async {
  await tabletCommunityDetailTestHelper(tester, testItemTitle, true);
}

Future<void> tabletCommunityDetailTestHelper(tester, String testItemTitle, bool isSaaSLogin) async {
  when(client.getWithHeader<Map<String, dynamic>>("https://gitlab.com/api/v4/user", any)).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
  when(client.getWithHeader<Map<String, dynamic>>("https://jihulab.com/api/v4/user", any)).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
  when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  when(client.get<List<dynamic>>("/api/v4/users/9527/starred_projects?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  when(client.get<List<dynamic>>("/api/v4/users/10000/starred_projects?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  when(client.getWithConnection("/api/v4/projects/89335/issues/218/discussions?page=1&per_page=50", any)).thenAnswer((_) => Future(() => Response.of(discussionsData)));
  when(client.get<List<dynamic>>("/api/v4/groups?top_level_only=true&page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  HttpClient.injectInstanceForTesting(client);

  // login
  expect(ConnectionProvider.authorized, isFalse);
  if (isSaaSLogin) {
    ConnectionProvider().reset(Tester.jihuLabUser());
  } else {
    ConnectionProvider().reset(Tester.selfManagedUser());
    expect(ConnectionProvider.isSelfManagedGitLab, true);
  }
  ConnectionProvider().notifyChanged();
  await tester.pumpAndSettle();
  expect(ConnectionProvider.authorized, isTrue);

  // Enable comment for SaaS Login but not private token login
  expect(find.widgetWithText(CommonAppBar, 'WFH'), findsOneWidget);
  expect(find.text('Community detail for test'), findsOneWidget);
  if (isSaaSLogin) {
    expect(SvgFinder("assets/images/comment.svg"), findsOneWidget);
  } else {
    expect(SvgFinder("assets/images/comment.svg"), findsNothing);
  }

  // logout
  expect(SvgFinder('assets/images/settings.svg'), findsOneWidget);
  await tester.tap(SvgFinder('assets/images/settings.svg'));
  await tester.pumpAndSettle();
  expect(find.text(testItemTitle), findsNothing);
  expect(find.byType(UserInfoView), findsOneWidget);
  expect(find.text(AppLocalizations.dictionary().accounts), findsOneWidget);

  await tester.tap(find.text(AppLocalizations.dictionary().accounts));
  await tester.pumpAndSettle();
  expect(find.byType(UserInfoView), findsNothing);
  expect(find.byType(AccountPage), findsOneWidget);

  Finder accountFinder;
  if (isSaaSLogin) {
    accountFinder = find.text('jihulab.com');
  } else {
    accountFinder = find.text('Self-Managed');
  }
  expect(accountFinder, findsOneWidget);
  await tester.drag(accountFinder, const Offset(-300, 0), warnIfMissed: false);
  await tester.pump();
  expect(find.byIcon(Icons.exit_to_app), findsOneWidget);
  await tester.tap(find.byIcon(Icons.exit_to_app));
  await tester.pumpAndSettle();
  expect(accountFinder, findsNothing);
  await tester.tap(find.byType(BackButton));
  await tester.pumpAndSettle();
  expect(find.byType(UserInfoView), findsOneWidget);
  expect(find.byType(AccountPage), findsNothing);
  await tester.tap(find.byType(BackButton));
  await tester.pumpAndSettle();
  expect(find.byType(UserInfoView), findsNothing);
  expect(find.text(testItemTitle), findsWidgets);
}

Future<void> mobileCommunityDetailTest(tester, String testItemTitle) async {
  // Display the details for not login user
  await tester.tap(find.text(testItemTitle));
  for (int i = 0; i < 5; i++) {
    await tester.pump(const Duration(seconds: 1));
  }
  expect(find.byType(CommunityPage), findsNothing);
  expect(find.byType(CommunityPostDetailsPage), findsOneWidget);
  expect(find.widgetWithText(CommonAppBar, 'WFH'), findsOneWidget);
  expect(find.text('Community detail for test'), findsOneWidget);
  expect(SemanticsLabelFinder('share'), findsOneWidget);
  expect(SemanticsLabelFinder('operate'), findsNothing);

  // Go back to list
  await tester.tap(find.byType(BackButton));
  await tester.pumpAndSettle();
  expect(find.widgetWithText(CommonAppBar, 'WFH'), findsNothing);
  expect(find.byType(CommunityPostDetailsPage), findsNothing);
  expect(find.byType(CommunityPage), findsOneWidget);
  expect(find.byType(ListView), findsWidgets);

  // Display the details for login user
  await mobilePrivateManagedLoginDetailTest(tester, testItemTitle);
  // await mobileSaaSLoginDetailTest(tester, testItemTitle);
}

Future<void> mobilePrivateManagedLoginDetailTest(tester, String testItemTitle) async {
  await mobileCommunityDetailTestHelper(tester, testItemTitle, false);
}

Future<void> mobileSaaSLoginDetailTest(tester, String testItemTitle) async {
  await mobileCommunityDetailTestHelper(tester, testItemTitle, true);
}

Future<void> mobileCommunityDetailTestHelper(tester, String testItemTitle, bool isSaaSLogin) async {
  when(client.getWithHeader<Map<String, dynamic>>("https://gitlab.com/api/v4/user", any)).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
  when(client.getWithHeader<Map<String, dynamic>>("https://jihulab.com/api/v4/user", any)).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
  when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  when(client.get<List<dynamic>>("/api/v4/users/9527/starred_projects?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  when(client.get<List<dynamic>>("/api/v4/users/10000/starred_projects?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  when(client.getWithConnection("/api/v4/projects/89335/issues/218/discussions?page=1&per_page=50", any)).thenAnswer((_) => Future(() => Response.of(discussionsData)));
  HttpClient.injectInstanceForTesting(client);

  // Login
  expect(ConnectionProvider.authorized, isFalse);
  if (isSaaSLogin) {
    ConnectionProvider().reset(Tester.jihuLabUser());
  } else {
    ConnectionProvider().reset(Tester.selfManagedUser());
    expect(ConnectionProvider.isSelfManagedGitLab, true);
  }
  ConnectionProvider().notifyChanged();
  await tester.pumpAndSettle();
  expect(ConnectionProvider.authorized, isTrue);

  // Enable comment for SaaS Login but not private token login
  await tester.tap(find.text(testItemTitle));
  await tester.pumpAndSettle();
  expect(find.byType(CommunityPostDetailsPage), findsOneWidget);
  expect(find.widgetWithText(CommonAppBar, 'WFH'), findsOneWidget);
  expect(find.text('Community detail for test'), findsOneWidget);
  if (isSaaSLogin) {
    expect(SvgFinder("assets/images/comment.svg"), findsOneWidget);
  } else {
    expect(SvgFinder("assets/images/comment.svg"), findsNothing);
  }
  expect(SvgFinder("assets/images/external-link.svg"), findsOneWidget);

  // Back to list
  await tester.tap(find.byType(BackButton));
  await tester.pumpAndSettle();
  expect(find.text(testItemTitle), findsOneWidget);
  expect(find.byType(CommunityPostDetailsPage), findsNothing);
  expect(find.byType(CommunityPage), findsOneWidget);
  expect(find.byType(ListView), findsWidgets);

  // Logout
  await tester.tap(find.byKey(const Key('menu_Community')));
  await tester.pumpAndSettle();

  expect(find.byType(Drawer), findsOneWidget);
  expect(find.text('Accounts'), findsOneWidget);

  await tester.tap(find.text('Accounts'));
  await tester.pumpAndSettle();
  expect(find.byType(Drawer), findsNothing);
  expect(find.byType(AccountPage), findsOneWidget);

  Finder accountFinder;
  if (isSaaSLogin) {
    accountFinder = find.text('jihulab.com');
  } else {
    accountFinder = find.text('Self-Managed');
  }
  expect(accountFinder, findsOneWidget);
  await tester.drag(accountFinder, const Offset(-300, 0), warnIfMissed: false);
  await tester.pump();
  expect(find.byIcon(Icons.exit_to_app), findsOneWidget);
  await tester.tap(find.byIcon(Icons.exit_to_app));
  await tester.pumpAndSettle();
  expect(accountFinder, findsNothing);
  await tester.tap(find.byType(BackButton));
  await tester.pumpAndSettle();
  expect(find.byType(Drawer), findsOneWidget);
  expect(find.byType(AccountPage), findsNothing);
  GlobalKeys.rootScaffoldKey.currentState?.closeDrawer();
  await tester.pumpAndSettle();
  expect(find.byType(Drawer), findsNothing);
  expect(find.text(testItemTitle), findsOneWidget);
}
