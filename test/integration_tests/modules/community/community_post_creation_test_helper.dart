import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/net/response.dart' as resp;
import 'package:jihu_gitlab_app/modules/community/community_post_creation_page.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';
import 'package:mockito/mockito.dart';

import '../../../finder/svg_finder.dart';
import '../../helper/core.dart';
import '../../helper/mobile_test_helper/root.dart';
import '../../helper/tablet_test_helper/root.dart';
import 'community_page_modules.dart';

Future<void> communityPostCreationTest(bool isMobile, WidgetTester tester) async {
  // Should not display creation button when user not log in
  expect(ConnectionProvider.authorized, isFalse);
  expect(find.byIcon(Icons.add_box_outlined), findsNothing);
  if (isMobile) {
    // Should not display creation button when user logged in with Private Managed Token
    await mobileLoginWithPrivateTokenForPage(tester, PageType.faq.index, CommunityPage);
    expect(find.byIcon(Icons.add_box_outlined), findsNothing);
    // Should not display creation button when user logged out
    await mobileLogoutForPage(tester, PageType.faq.index, CommunityPage);
    expect(find.byIcon(Icons.add_box_outlined), findsNothing);
    // Should display creation button when user logged in with jihulab.com SaaS account
    await mobileLoginWithSaaSForPage(tester, PageType.faq.index, CommunityPage);
  } else {
    // Should not display creation button when user logged in with Private Managed Token
    await tabletLoginWithPrivateTokenForPage(tester, PageType.faq.index, CommunityPage);
    expect(find.byIcon(Icons.add_box_outlined), findsNothing);
    // Should not display creation button when user logged out
    await tabletLogoutForPage(tester, PageType.faq.index, CommunityPage);
    expect(find.byIcon(Icons.add_box_outlined), findsNothing);
    // Should display creation button when user logged in with jihulab.com SaaS account
    await tabletLoginWithSaaSForPage(tester, PageType.faq.index, CommunityPage);
  }
  expect(find.byIcon(Icons.add_box_outlined), findsOneWidget);

  // Should display RequestAccessPage when user is not a member of FAQ project
  when(client.postWithConnection<dynamic>("/api/graphql", getProjectMembersGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', 'jihulab_tester', ''), any))
      .thenAnswer((_) => Future(() => resp.Response.of(projectsMembersGraphQLResponse)));
  await tester.tap(find.byIcon(Icons.add_box_outlined));
  await tester.pumpAndSettle();
  expect(find.byType(CommunityPage), findsNothing);
  expect(find.byType(CommunityPostCreationPage), findsOneWidget);
  expect(find.text('Only join the community to post.'), findsOneWidget);
  var requestFinder = find.widgetWithText(InkWell, 'Request Access');
  expect(requestFinder, findsOneWidget);

  // Should display keep in RequestAccessPage when user request access failed
  when(client.postWithConnection('/api/v4/projects/89335/access_requests', {}, any))
      .thenThrow(DioError(requestOptions: RequestOptions(path: ''), response: Response(requestOptions: RequestOptions(path: ''), statusCode: 500)));
  await tester.tap(requestFinder);
  for (int i = 0; i < 5; i++) {
    await tester.pump(const Duration(seconds: 1));
  }
  expect(find.text('Only join the community to post.'), findsOneWidget);
  expect(requestFinder, findsOneWidget);

  // Should display user request access succeed
  when(client.postWithConnection('/api/v4/projects/89335/access_requests', {}, any)).thenAnswer((_) => Future(() => resp.Response.of({})));
  await tester.tap(requestFinder);
  await tester.pumpAndSettle();
  for (int i = 0; i < 5; i++) {
    await tester.pump(const Duration(seconds: 1));
  }
  expect(SvgFinder('assets/images/succeed.svg'), findsOneWidget);
  expect(find.text('Your request for access has been queued for review.'), findsOneWidget);

  // Should display FaqPage when user tapped back button
  await tester.tap(find.byType(BackButton));
  await tester.pumpAndSettle();
  expect(find.byType(CommunityPage), findsOneWidget);
  expect(find.byType(CommunityPostCreationPage), findsNothing);

  // Should display RequestAccessPage with request button when user did send request but not approved
  await tester.tap(find.byIcon(Icons.add_box_outlined));
  await tester.pumpAndSettle();
  expect(find.byType(CommunityPage), findsNothing);
  expect(find.byType(CommunityPostCreationPage), findsOneWidget);
  expect(find.text('Only join the community to post.'), findsOneWidget);
  requestFinder = find.widgetWithText(InkWell, 'Request Access');
  expect(requestFinder, findsOneWidget);

  // Should display user request access succeed when user tapped request button and last request not approved
  when(client.postWithConnection('/api/v4/projects/89335/access_requests', {}, any))
      .thenThrow(DioError(requestOptions: RequestOptions(path: ''), response: Response(requestOptions: RequestOptions(path: ''), statusCode: 400)));
  await tester.tap(requestFinder);
  await tester.pumpAndSettle();
  expect(SvgFinder('assets/images/succeed.svg'), findsOneWidget);
  expect(find.text('Your request for access has been queued for review.'), findsOneWidget);

  // Should display FaqPage when user tapped back button
  await tester.tap(find.byType(BackButton));
  await tester.pumpAndSettle();
  expect(find.byType(CommunityPage), findsOneWidget);
  expect(find.byType(CommunityPostCreationPage), findsNothing);

  // Should not display creation button when user logged out
  if (isMobile) {
    await mobileLogoutForPage(tester, PageType.faq.index, CommunityPage);
  } else {
    await tabletLogoutForPage(tester, PageType.faq.index, CommunityPage);
  }
  expect(find.byIcon(Icons.add_box_outlined), findsNothing);
}
