import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/db_manager.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/stars_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/connection_selector.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/list/project_issues_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project_list_tile.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/issues/project_issues_page.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/project_repository_model.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/project_repository_page.dart';
import 'package:jihu_gitlab_app/modules/root/pad_group_and_project_list_tile.dart';
import 'package:jihu_gitlab_app/modules/root/starred_tab.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../finder/svg_finder.dart';
import '../../../mocker/tester.dart';
import '../../../modules/ai/ai_page_test.mocks.dart';
import '../../../modules/projects/repository/response_data.dart';
import '../../../test_data/groups.dart';
import '../../../test_data/issue.dart';
import '../../helper/core.dart';
import '../../helper/tablet_test_helper/root.dart';
import '../community/community_page_modules.dart';

void main() {
  setUp(() async {
    WidgetsFlutterBinding.ensureInitialized();

    AppLocalizations.init();
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    // TODO: set the privacy alert not showing for some test issue: "not hit test on the specified widget"
    LocalStorage.save('privacy-policy-agreed', true);

    setupLocator();
    mockCommunity(client);
    when(client.postWithConnection<dynamic>("/api/graphql", getCommunityTypesGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/faq"), any))
        .thenAnswer((_) => Future(() => Response.of(faqTypesResponse)));
  });

  testWidgets('Should display StarredTab when launch app in desktop mode', (tester) async {
    await launchTabletAppForTest(tester);
    expect(find.text('Connect to your server first'), findsOneWidget);
    await navigateToTargetTabletPage(tester, PageType.projects.index, ProjectsGroupsPage);
    expect(find.text(AppLocalizations.dictionary().projectsGroups), findsAtLeastNWidgets(2));
    expect(find.byType(StarredTab), findsOneWidget);
    // Show recommend project in star list
    expect(find.widgetWithText(PadGroupAndProjectListTile, 'GitLab'), findsOneWidget);
    final MockDatabase mockDatabase = MockDatabase();
    DbManager.instance().injectDatabaseForTesting(mockDatabase);

    // Show to level groups after login
    when(mockDatabase.insert(GroupAndProjectEntity.tableName, any)).thenAnswer((_) => Future(() => 1));
    when(mockDatabase.rawQuery("select * from groups_and_projects where user_id = 9527 and parent_id = 0 order by type desc, last_activity_at desc")).thenAnswer((_) => Future(() => topLevelDBGroups));
    await tabletLoginWithSaaSForPage(tester, PageType.projects.index, ProjectsGroupsPage);
    expect(find.byType(ProjectsGroupsPage), findsOneWidget);
    expect(find.byType(GroupAndProjectListTile), findsNWidgets(topLevelDBGroups.length));
    expect(find.widgetWithText(GroupAndProjectListTile, '免费版演示'), findsOneWidget);

    // Stars and un-stars
    await testStarsAndUnstars(tester, mockDatabase);
    expect(find.text('Connect to your server first'), findsNothing);

    // Switch account for star list
    await testSwitchAccountToUpdateStars(mockDatabase, tester);

    // Tap starred project
    await testProjectDetail(tester);
  });

  tearDown(() {
    ConnectionProvider().fullReset();
    StarsProvider().fullReset();
    unregister();
    reset(client);
  });
}

Future<void> testStarsAndUnstars(tester, MockDatabase mockDatabase) async {
  expect(find.byKey(const Key('14269_免费版演示')), findsOneWidget);

  // Stars group
  when(mockDatabase.rawQuery("select * from groups_and_projects where starred = 1 and user_id = 9527 order by starred_at desc")).thenAnswer((_) => Future(() => [
        {"id": 3, "iid": 14269, "user_id": 9527, "name": '免费版演示', 'type': 'group', 'relative_path': 'free-plan', "parent_id": 0, "starred": 0, "last_activity_at": 0, "starred_at": 0, "version": 1}
      ]));
  expect(find.byIcon(Icons.star_rounded), findsNothing);
  await tester.tap(find.byKey(const Key('14269_免费版演示')));
  await tester.pumpAndSettle();
  expect(find.byIcon(Icons.star_rounded), findsOneWidget);
  expect(find.widgetWithText(PadGroupAndProjectListTile, '免费版演示'), findsOneWidget);

  // Un-stars group
  when(mockDatabase.rawQuery("select * from groups_and_projects where starred = 1 and user_id = 9527 order by starred_at desc")).thenAnswer((_) => Future(() => []));
  await tester.tap(find.byKey(const Key('14269_免费版演示')));
  await tester.pumpAndSettle();
  expect(find.byIcon(Icons.star_rounded), findsNothing);
  expect(find.widgetWithText(PadGroupAndProjectListTile, '免费版演示'), findsNothing);

  // Go to subgroup list to stars project
  when(client.get<dynamic>('/api/v4/groups/14269?all_available=true&page=1&per_page=50')).thenAnswer((_) => Future(() => Response.of<dynamic>({'projects': []})));
  when(client.get<List<dynamic>>('/api/v4/groups/14269/subgroups?all_available=true&page=1&per_page=50')).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  when(mockDatabase.rawQuery("select * from groups_and_projects where user_id = 9527 and parent_id = 14269 order by type desc, last_activity_at desc"))
      .thenAnswer((_) => Future(() => subgroupListPageDBItems));
  await tester.tap(find.widgetWithText(GroupAndProjectListTile, '免费版演示'));
  await tester.pumpAndSettle();
  expect(find.byType(SubgroupPage), findsOneWidget);
  expect(find.byType(SubgroupListPage), findsOneWidget);
  expect(find.widgetWithText(PadGroupAndProjectListTile, 'Learn GitLab'), findsNothing);
  expect(find.byKey(const Key('69172_Learn GitLab')), findsOneWidget);

  // Stars project
  when(mockDatabase.rawQuery("select * from groups_and_projects where starred = 1 and user_id = 9527 order by starred_at desc")).thenAnswer((_) => Future(() => [
        {
          "id": 3,
          "iid": 69172,
          "user_id": 9527,
          "name": 'Learn GitLab',
          'type': 'project',
          'relative_path': 'highsof-t/learn-gitlab',
          "parent_id": 118014,
          "starred": 0,
          "last_activity_at": 0,
          "starred_at": 0,
          "version": 1
        }
      ]));
  when(client.post("/api/v4/projects/69172/star", {})).thenAnswer((_) => Future(() => Response.of([])));
  expect(find.byIcon(Icons.star_rounded), findsNothing);
  await tester.tap(find.byKey(const Key('69172_Learn GitLab')));
  await tester.pumpAndSettle();
  expect(find.byIcon(Icons.star_rounded), findsOneWidget);
  await tester.tap(find.byType(BackButton));
  await tester.pumpAndSettle();
  expect(find.widgetWithText(PadGroupAndProjectListTile, 'Learn GitLab'), findsOneWidget);

  // Go to subgroup list to un-stars project
  var items = List.of(subgroupListPageDBItems);
  items[2]['starred'] = 1;
  when(mockDatabase.rawQuery("select * from groups_and_projects where user_id = 9527 and parent_id = 14269 order by type desc, last_activity_at desc")).thenAnswer((_) => Future(() => items));
  await tester.tap(find.widgetWithText(GroupAndProjectListTile, '免费版演示'));
  await tester.pumpAndSettle();

  // Un-stars project
  when(mockDatabase.rawQuery("select * from groups_and_projects where starred = 1 and user_id = 9527 order by starred_at desc")).thenAnswer((_) => Future(() => []));
  when(client.post("/api/v4/projects/69172/unstar", {})).thenAnswer((_) => Future(() => Response.of([])));
  expect(find.byIcon(Icons.star_rounded), findsOneWidget);
  await tester.tap(find.byKey(const Key('69172_Learn GitLab')));
  await tester.pumpAndSettle();
  expect(find.byIcon(Icons.star_rounded), findsNothing);
  await tester.tap(find.byType(BackButton));
  await tester.pumpAndSettle();
  expect(find.widgetWithText(PadGroupAndProjectListTile, 'Learn GitLab'), findsNothing);
}

Future<void> testSwitchAccountToUpdateStars(MockDatabase mockDatabase, WidgetTester tester) async {
  when(client.get("/api/v4/users/9528/starred_projects?page=1&per_page=50")).thenAnswer((realInvocation) => Future(() => Response.of([])));
  when(mockDatabase.rawQuery("select * from groups_and_projects where starred = 1 and user_id = 9528 order by starred_at desc")).thenAnswer((_) => Future(() => []));
  ConnectionProvider().injectConnectionForTest(Tester.gitLabUser());
  await navigateToTargetTabletPage(tester, PageType.todo.index, ToDosPage);
  expect(find.byWidgetPredicate((widget) => widget is ConnectionSelector && widget.key == const Key('ToDosConnectionSelector')), findsOneWidget);
  await tester.tap(find.byKey(const Key('gitlab_tester')));
  await tester.pumpAndSettle();
  expect(find.text('@jihulab_tester'), findsOneWidget);
  expect(find.text('@gitlab_tester'), findsWidgets);

  // Switch to JiHu account
  when(mockDatabase.rawQuery("select * from groups_and_projects where starred = 1 and user_id = 9527 order by starred_at desc")).thenAnswer((_) => Future(() => [
        {"id": 3, "iid": 14269, "user_id": 9527, "name": '免费版演示', 'type': 'group', 'relative_path': 'free-plan', "parent_id": 0, "starred": 0, "last_activity_at": 0, "starred_at": 0, "version": 1}
      ]));
  expect(find.widgetWithText(PadGroupAndProjectListTile, '免费版演示'), findsNothing);
  await tester.tap(find.text('@jihulab_tester'));
  await tester.pumpAndSettle();
  expect(find.widgetWithText(PadGroupAndProjectListTile, '免费版演示'), findsOneWidget);

  // Switch to GitLab account
  await tester.tap(find.byKey(const Key('jihulab_tester')));
  await tester.pumpAndSettle();
  when(mockDatabase.rawQuery("select * from groups_and_projects where starred = 1 and user_id = 9528 order by starred_at desc")).thenAnswer((_) => Future(() => [
        {
          "id": 13,
          "iid": 15269,
          "user_id": 9528,
          "name": 'Self Managed',
          'type': 'group',
          'relative_path': 'self-managed',
          "parent_id": 0,
          "starred": 0,
          "last_activity_at": 0,
          "starred_at": 0,
          "version": 1
        }
      ]));
  await tester.tap(find.text('@gitlab_tester'));
  await tester.pumpAndSettle();
  expect(find.widgetWithText(PadGroupAndProjectListTile, '免费版演示'), findsNothing);
  expect(find.widgetWithText(PadGroupAndProjectListTile, 'Self Managed'), findsOneWidget);
}

Future<void> testProjectDetail(WidgetTester tester) async {
  // Display project with no share button
  when(client.post<dynamic>('/api/graphql', buildRequestBodyOfCheckRepositoryIsEmpty('gitlab-org/gitlab'), connection: anyNamed("connection")))
      .thenAnswer((_) => Future(() => Response.of<dynamic>(checkRepositoryIsEmptyResponse(empty: true))));
  expect(find.widgetWithText(PadGroupAndProjectListTile, 'GitLab'), findsOneWidget);
  await tester.tap(find.widgetWithText(PadGroupAndProjectListTile, 'GitLab'));
  await tester.pumpAndSettle();
  expect(find.byType(ProjectPage), findsOneWidget);
  expect(find.byType(ProjectRepositoryPage), findsOneWidget);
  expect(find.byType(ProjectIssuesPage), findsNothing);
  expect(find.byType(IssueDetailsPage), findsOneWidget);
  expect(find.text('No issue selected'), findsOneWidget);
  expect(SvgFinder('assets/images/external-link.svg'), findsNothing);

  // Switch to issues tab
  when(client.post<dynamic>("/api/graphql", getProjectIssuesGraphQLRequestBody('gitlab-org/gitlab', '', 20), connection: anyNamed("connection")))
      .thenAnswer((_) => Future(() => Response.of(projectIssuesGraphQLResponseData)));
  expect(find.widgetWithText(Tab, "Issues"), findsOneWidget);
  await tester.tap(find.widgetWithText(Tab, "Issues"));
  await tester.pumpAndSettle();
  expect(find.widgetWithText(InkWell, 'Feature: 开发人员查看流水线自动合并'), findsOneWidget);

  // Display issue detail with share button
  when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 264), connection: anyNamed("connection")))
      .thenAnswer((_) => Future(() => Response.of<dynamic>(issueDetailsGraphQLResponse)));
  when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 264), connection: anyNamed("connection")))
      .thenAnswer((_) => Future(() => Response.of<dynamic>(issuePostVotesGraphQLResponse)));
  when(client.get("/api/v4/projects/59893/issues/264/discussions?page=1&per_page=50", connection: anyNamed("connection"))).thenAnswer((_) => Future(() => Response.of([])));
  when(client.get("/api/v4/projects/59893/issues/264/award_emoji?per_page=100&page=1", connection: anyNamed("connection"))).thenAnswer((_) => Future(() => Response.of([])));
  await tester.tap(find.widgetWithText(InkWell, 'Feature: 开发人员查看流水线自动合并'));
  await tester.pumpAndSettle(const Duration(seconds: 1));
  expect(SvgFinder('assets/images/operate.svg'), findsOneWidget);
}
