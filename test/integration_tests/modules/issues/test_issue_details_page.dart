import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/db_manager.dart';
import 'package:jihu_gitlab_app/core/domain/assignee/assignee_entity.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar_and_name.dart';
import 'package:jihu_gitlab_app/core/widgets/label_view.dart';
import 'package:jihu_gitlab_app/core/widgets/search_no_result_view.dart';
import 'package:jihu_gitlab_app/core/widgets/selector/selector.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion_entity.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/note/note_entity.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';
import 'package:mockito/mockito.dart';

import '../../../core/net/http_request_test.mocks.dart';
import '../../../finder/selected_check_box_finder.dart';
import '../../../finder/semantics_label_finder.dart';
import '../../../finder/svg_finder.dart';
import '../../../finder/text_field_hint_text_finder.dart';
import '../../../modules/ai/ai_page_test.mocks.dart';
import '../../../test_data/assignee.dart';
import '../../../test_data/discussion.dart';
import '../../../test_data/issue.dart';
import '../../../test_data/note.dart';

Future<void> issueDetailsPageContentTest(tester, MockHttpClient client) async {
  expect(find.byType(IssueDetailsPage), findsOneWidget);
  expect(find.text('回复评论'), findsOneWidget);
  expect(find.text('issue description for test'), findsOneWidget);
  expect(SvgFinder("assets/images/comment.svg"), findsOneWidget);
  expect(SvgFinder("assets/images/operate.svg"), findsOneWidget);
  expect(find.byType(AvatarAndName), findsWidgets);
  expect(find.byWidgetPredicate((widget) => widget is AvatarAndName && widget.username == 'assignee-username'), findsOneWidget);
  await issueLabelsTest(tester, client);
  await issueAssigneesTest(tester, client);
}

Future<void> issueLabelsTest(tester, MockHttpClient client) async {
  // Should display none labels
  expect(find.text(AppLocalizations.dictionary().labels), findsOneWidget);
  expect(find.text(AppLocalizations.dictionary().none), findsNWidgets(3));
  expect(SvgFinder("assets/images/edit_pencil.svg"), findsNWidgets(3));

  final mockDatabase = MockDatabase();
  when(mockDatabase.insert("project_labels", any)).thenAnswer((_) => Future(() => 1));
  when(mockDatabase.update("project_labels", any)).thenAnswer((_) => Future(() => 1));
  when(mockDatabase.transaction(any)).thenAnswer((_) => Future(() => <int>[]));
  when(mockDatabase.query("project_labels", where: " project_id = ? ", whereArgs: [59893])).thenAnswer((_) => Future(() {
        return [
          {"id": 1, "label_id": 1, "project_id": 59893, "name": "CREQ::blocker", "description": "description_1", "textColor": "#FFFFFF", "color": "#FFFFFF", "version": 1},
          {"id": 2, "label_id": 2, "project_id": 59893, "name": "P::M", "description": "description_2", "textColor": "#FFFFFF", "color": "#FFFFFF", "version": 1},
          {"id": 3, "label_id": 3, "project_id": 59893, "name": "Ready::No", "description": "description_3", "textColor": "#FFFFFF", "color": "#FFFFFF", "version": 1},
          {"id": 4, "label_id": 4, "project_id": 59893, "name": "Ready::Yes", "description": "description_4", "textColor": "#FFFFFF", "color": "#FFFFFF", "version": 1},
        ];
      }));
  DbManager.instance().injectDatabaseForTesting(mockDatabase);

  // Select none label
  await tester.tap(SemanticsLabelFinder("labels_edit"));
  await tester.pumpAndSettle();
  expect(find.text(AppLocalizations.dictionary().selectLabels), findsOneWidget);
  expect(find.text(AppLocalizations.dictionary().done), findsOneWidget);
  expect(find.byType(TipsView), findsNothing);
  expect(find.text('CREQ::blocker'), findsOneWidget);
  var listFinder = find.byType(Scrollable).last;
  await tester.scrollUntilVisible(find.text('Ready::Yes'), 500.0, scrollable: listFinder);
  await tester.pumpAndSettle(const Duration(seconds: 1));
  expect(find.text('P::M'), findsOneWidget);
  expect(find.text('Ready::No'), findsOneWidget);
  expect(find.text('Ready::Yes'), findsOneWidget);
  expect(SelectedCheckBoxFinder(), findsNothing);

  await tester.tap(find.text('P::M'));
  await tester.pumpAndSettle();
  expect(SelectedCheckBoxFinder(), findsOneWidget);

  when(client.put<Map<String, dynamic>>("/api/v4/projects/59893/issues/451", {'labels': 'P::M'})).thenAnswer((_) => Future.value(Response.of<Map<String, dynamic>>(issueData)));
  when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 451)))
      .thenAnswer((_) => Future(() => Response.of<dynamic>(issueDetailsGraphQLResponse)));
  await tester.tap(find.text(AppLocalizations.dictionary().done));
  await tester.pumpAndSettle();
  expect(find.byWidgetPredicate((widget) => widget is LabelView && widget.left == 'P' && widget.right == 'M'), findsOneWidget);
  expect(find.text(AppLocalizations.dictionary().none), findsOneWidget);
  expect(find.byType(Selector), findsNothing);
  expect(find.text(AppLocalizations.dictionary().selectLabels), findsNothing);

  // Go to label selector
  await tester.tap(SemanticsLabelFinder("labels_edit"));
  await tester.pumpAndSettle();
  expect(find.text(AppLocalizations.dictionary().selectLabels), findsOneWidget);
  expect(find.text(AppLocalizations.dictionary().done), findsOneWidget);
  listFinder = find.byType(Scrollable).last;
  await tester.scrollUntilVisible(find.text('Ready::Yes'), 500.0, scrollable: listFinder);
  await tester.pumpAndSettle(const Duration(seconds: 1));
  expect(find.text('P::M'), findsOneWidget);
  expect(find.text('Ready::No'), findsOneWidget);
  expect(find.text('Ready::Yes'), findsOneWidget);
  expect(SelectedCheckBoxFinder(), findsOneWidget);

  // Search Labels
  var textFieldFinder = TextFieldHintTextFinder("Search");
  expect(textFieldFinder, findsOneWidget);
  await tester.enterText(textFieldFinder, "aaa");
  await tester.pumpAndSettle();
  expect(find.byType(Checkbox), findsNothing);
  expect(find.text("Ready::No"), findsNothing);
  expect(find.text('P::M'), findsNothing);
  expect(find.byType(SearchNoResultView), findsOneWidget);
  expect(find.text("No matches for 'aaa'"), findsOneWidget);
  await tester.enterText(textFieldFinder, "P:");
  await tester.pumpAndSettle();
  expect(find.byType(Checkbox), findsOneWidget);
  expect(find.text('P::M'), findsOneWidget);

  // Select labels
  await tester.tap(find.text('P::M'));
  await tester.pumpAndSettle();
  expect(SelectedCheckBoxFinder(), findsNothing);

  when(client.put<Map<String, dynamic>>("/api/v4/projects/59893/issues/451", {'labels': ''})).thenAnswer((_) => Future.value(Response.of<Map<String, dynamic>>(issueDataWithNoneLabels)));
  when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 451)))
      .thenAnswer((_) => Future(() => Response.of<dynamic>(issueDetailsWithNoneLabelsGraphQLResponse)));
  HttpClient.injectInstanceForTesting(client);
  await tester.tap(find.text(AppLocalizations.dictionary().done));
  await tester.pumpAndSettle();
  expect(find.byWidgetPredicate((widget) => widget is LabelView && widget.left == 'P' && widget.right == 'M'), findsNothing);
  expect(find.text(AppLocalizations.dictionary().none), findsNWidgets(3));
  expect(find.byType(Selector), findsNothing);
  expect(find.text(AppLocalizations.dictionary().selectLabels), findsNothing);
}

Future<void> issueAssigneesTest(tester, MockHttpClient client) async {
  // Select one assignee
  expect(find.text(AppLocalizations.dictionary().assignees), findsOneWidget);
  expect(find.text("assignee-username"), findsOneWidget);
  expect(find.byType(LabelView), findsNothing);
  expect(SvgFinder("assets/images/edit_pencil.svg"), findsNWidgets(3));

  when(client.get<List<dynamic>>("/api/v4/projects/59893/members/all?page=1&per_page=50")).thenAnswer((_) async => Future.value(Response.of([
        {"id": 22, "username": "tester-22-new", "avatar_url": "tester-avatar-22-new"},
        {"id": 33, "username": "tester-33", "avatar_url": "tester-avatar-33"}
      ])));

  final mockDatabase = MockDatabase();
  when(mockDatabase.query("project_members", where: " project_id = ? ", whereArgs: [59893])).thenAnswer((_) => Future(() {
        return [
          {"id": 1, "member_id": 1, "project_id": 59893, "name": "test_1", "username": "tester_1", "avatar_url": "avatar_url_1", "version": 1},
          {"id": 2, "member_id": 2, "project_id": 59893, "name": "test_2", "username": "tester_2", "avatar_url": "avatar_url_2", "version": 1},
          {"id": 3, "member_id": 3, "project_id": 59893, "name": "test_11", "username": "tester_11", "avatar_url": "avatar_url_11", "version": 1},
          {"id": 4, "member_id": 4, "project_id": 59893, "name": "test_22", "username": "tester_22", "avatar_url": "avatar_url_22", "version": 1},
        ];
      }));
  when(mockDatabase.rawQuery("select * from project_members where project_id = 59893 and member_id in (22, 33)")).thenAnswer((_) => Future(() {
        return [
          {"id": 1, "member_id": 1, "project_id": 59893, "name": "test_1", "username": "tester_1", "avatar_url": "avatar_url_1", "version": 1},
          {"id": 2, "member_id": 2, "project_id": 59893, "name": "test_2", "username": "tester_2", "avatar_url": "avatar_url_2", "version": 1},
          {"id": 3, "member_id": 3, "project_id": 59893, "name": "test_11", "username": "tester_11", "avatar_url": "avatar_url_11", "version": 1},
          {"id": 4, "member_id": 4, "project_id": 59893, "name": "test_22", "username": "tester_22", "avatar_url": "avatar_url_22", "version": 1},
        ];
      }));
  when(mockDatabase.insert(DiscussionEntity.tableName, any)).thenAnswer((_) => Future(() => 1));
  when(mockDatabase.insert(MemberEntity.tableName, any)).thenAnswer((_) => Future(() => 1));
  when(mockDatabase.transaction(any)).thenAnswer((_) => Future(() => <int>[]));
  when(mockDatabase.rawQuery("select * from ${DiscussionEntity.tableName} where issue_id = 310312")).thenAnswer((_) => Future(() => [discussionDbData]));
  when(mockDatabase.insert(NoteEntity.tableName, any)).thenAnswer((_) => Future(() => 1));
  when(mockDatabase.rawQuery("select * from ${NoteEntity.tableName} where discussion_id = '123'")).thenAnswer((_) => Future(() => [noteDbData]));
  when(mockDatabase.insert(AssigneeEntity.tableName, any)).thenAnswer((_) => Future(() => 1));
  when(mockDatabase.query(AssigneeEntity.tableName, where: " assignee_id = ? ", whereArgs: [29355])).thenAnswer((_) => Future(() => [assigneeDbData]));
  DbManager.instance().injectDatabaseForTesting(mockDatabase);

  // Select Assignee
  await tester.tap(SemanticsLabelFinder("assignees_edit"));
  await tester.pumpAndSettle();
  expect(find.byType(Selector<int, Member>), findsOneWidget);
  expect(find.text(AppLocalizations.dictionary().selectAssignees), findsOneWidget);
  expect(find.text(AppLocalizations.dictionary().done), findsOneWidget);
  expect(find.byType(TipsView), findsNothing);
  expect(find.text('tester_1'), findsOneWidget);
  expect(find.text('tester_2'), findsOneWidget);
  expect(find.text('tester_11'), findsOneWidget);
  expect(find.text('tester_22'), findsOneWidget);
  expect(SelectedCheckBoxFinder(), findsNothing);

  await tester.tap(find.text('tester_1'));
  await tester.pumpAndSettle();
  expect(SelectedCheckBoxFinder(), findsOneWidget);

  when(client.put<Map<String, dynamic>>("/api/v4/projects/59893/issues/451", {
    'assignee_ids': [1]
  })).thenAnswer((_) => Future.value(Response.of<Map<String, dynamic>>(issueData)));
  when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 451)))
      .thenAnswer((_) => Future(() => Response.of<dynamic>(issueDetailsGraphQLResponse)));
  HttpClient.injectInstanceForTesting(client);
  await tester.tap(find.text(AppLocalizations.dictionary().done));
  await tester.pumpAndSettle();
  expect(find.byWidgetPredicate((widget) => widget is LabelView && widget.left == 'P' && widget.right == 'M'), findsOneWidget);
  expect(find.text(AppLocalizations.dictionary().none), findsOneWidget);
  expect(find.byType(Selector), findsNothing);
  expect(find.text(AppLocalizations.dictionary().selectLabels), findsNothing);

  // Select assignees
  await tester.tap(SemanticsLabelFinder("assignees_edit"));
  await tester.pumpAndSettle();
  expect(find.byType(Selector<int, Member>), findsOneWidget);
  expect(find.text(AppLocalizations.dictionary().selectAssignees), findsOneWidget);
  expect(find.text(AppLocalizations.dictionary().done), findsOneWidget);
  expect(find.text('tester_1'), findsOneWidget);
  expect(find.text('tester_2'), findsOneWidget);
  expect(find.text('tester_11'), findsOneWidget);
  expect(find.text('tester_22'), findsOneWidget);
  expect(SelectedCheckBoxFinder(), findsNothing);

  when(client.put<Map<String, dynamic>>("/api/v4/projects/59893/issues/451", {'assignee_ids': []})).thenAnswer((_) => Future.value(Response.of<Map<String, dynamic>>(issueDataWithNoneLabels)));
  when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 451)))
      .thenAnswer((_) => Future(() => Response.of<dynamic>(issueDetailsWithNoneLabelsGraphQLResponse)));
  HttpClient.injectInstanceForTesting(client);
  await tester.tap(find.text(AppLocalizations.dictionary().done));
  await tester.pumpAndSettle();
  expect(find.byWidgetPredicate((widget) => widget is LabelView && widget.left == 'P' && widget.right == 'M'), findsNothing);
  expect(find.text(AppLocalizations.dictionary().none), findsNWidgets(3));
  expect(find.byType(Selector), findsNothing);
  expect(find.text(AppLocalizations.dictionary().selectLabels), findsNothing);
}
