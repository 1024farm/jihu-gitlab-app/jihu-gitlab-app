import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connections.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/widgets/unauthorized_view.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/l10n/current_locale.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

import '../core/net/http_request_test.mocks.dart';
import '../finder/text_field_hint_text_finder.dart';
import '../test_binding_setter.dart';
import '../test_data/user.dart';

void main() {
  testWidgets('Should login succeed when user submit the Host and Personal Access Token in GitLab self-managed page', (tester) async {
    await setUpMobileBinding(tester);

    final client = MockHttpClient();
    when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
    when(client.getWithHeader<Map<String, dynamic>>("https://example.com/api/v4/user", any)).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
    HttpClient.injectInstanceForTesting(client);

    await tester.pumpWidget(MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
        child: MaterialApp(
          routes: {SelfManagedLoginPage.routeName: (context) => const SelfManagedLoginPage()},
          home: const Scaffold(body: ToDosPage()),
          localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
        )));

    await tester.pumpAndSettle();
    expect(find.byType(UnauthorizedView), findsOneWidget);

    await tester.tap(find.text(LoginMenuSet.selfManaged.name));
    await tester.pumpAndSettle();
    expect(find.byType(SelfManagedLoginPage), findsOneWidget);
    expect(find.text('GitLab self-managed'), findsOneWidget);

    expect(TextFieldHintTextFinder('Host: gitlab.example.com'), findsOneWidget);
    expect(find.text('HTTPS'), findsOneWidget);
    expect(find.byType(CupertinoSwitch), findsOneWidget);
    expect(tester.widget<CupertinoSwitch>(find.byType(CupertinoSwitch)).value, isTrue);
    expect(TextFieldHintTextFinder('Access Token'), findsOneWidget);
    expect(find.text('Submit'), findsOneWidget);

    await tester.enterText(TextFieldHintTextFinder('Host: gitlab.example.com'), 'example.com');
    await tester.enterText(TextFieldHintTextFinder('Access Token'), 'access_token');
    await tester.tap(find.text('Submit'));
    await tester.pumpAndSettle();
    expect(ConnectionProvider.isSelfManagedGitLab, isTrue);
    expect(ConnectionProvider.currentBaseUrl.get, "https://example.com");
    expect(ConnectionProvider.personalAccessToken, 'access_token');
    expect(ConnectionProvider.authorized, isTrue);

    await tester.pumpAndSettle();

    expect(find.byType(ToDosPage), findsOneWidget);

    ConnectionProvider().fullReset();
  });
}
