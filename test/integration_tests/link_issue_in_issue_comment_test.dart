import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart' as r;
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/modules/issues/details/issue_details_page.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_model.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issue_selector.dart';
import 'package:jihu_gitlab_app/modules/issues/list/project_issues_gq_request_body.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../core/net/http_request_test.mocks.dart';
import '../finder/svg_finder.dart';
import '../mocker/tester.dart';

void main() {
  var issueDetailsModel = IssueDetailsModel();
  locator.registerSingleton(issueDetailsModel);
  late MockHttpClient client;

  setUp(() async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    await ConnectionProvider().restore();
    ConnectionProvider().reset(Tester.jihuLabUser());
    client = MockHttpClient();
    ConnectionProvider().reset(Tester.jihuLabUser());
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets('Should be able to link issue in issue comment', (tester) async {
    when(client.get<List<dynamic>>("/api/v4/projects/72936/issues/6/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>([])));
    when(client.get<Map<String, dynamic>>("/api/v4/projects/72936"))
        .thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>({"path_with_namespace": "ultimate-plan/jihu-gitlab-app/demo-mr-test"})));
    when(client.get<List<dynamic>>("/api/v4/projects/72936/members/all?page=1&per_page=50")).thenAnswer((_) async => Future.value(r.Response.of([])));
    when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 6)))
        .thenAnswer((_) => Future(() => r.Response.of<dynamic>(issueDetailsGraphQLResponse)));
    when(client.post<dynamic>('/api/graphql', getProjectIssuesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', '', 20)))
        .thenAnswer((_) => Future(() => r.Response.of<dynamic>(projectIssuesGraphQLResponseData)));
    when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 6), useActiveConnection: true))
        .thenAnswer((_) => Future(() => r.Response.of<dynamic>(issuePostVotesGraphQLResponse)));
    when(client.get("/api/v4/projects/72936/issues/6/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => r.Response.of([])));

    var parameters = {'projectId': 72936, 'issueId': 3242, 'issueIid': 6, 'targetId': 3242, 'targetIid': 6, 'pathWithNamespace': 'ultimate-plan/jihu-gitlab-app/demo-mr-test'};
    await tester.pumpWidget(MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => ConnectionProvider()),
        ChangeNotifierProvider(create: (context) => ProjectProvider()),
      ],
      child: MaterialApp(
        onGenerateRoute: onGenerateRoute,
        home: Scaffold(
          body: IssueDetailsPage(arguments: parameters),
        ),
        localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
      ),
    ));

    await tester.pumpAndSettle(const Duration(seconds: 1));
    var svgFinder = SvgFinder("assets/images/comment.svg");
    expect(svgFinder, findsOneWidget);
    await tester.tap(svgFinder);
    await tester.pumpAndSettle();
    expect(SvgFinder("assets/images/numbers.svg"), findsOneWidget);
    await tester.tap(SvgFinder("assets/images/numbers.svg"));
    await tester.pumpAndSettle();
    expect(find.byType(IssueSelector), findsOneWidget);
    expect(find.text("Feature：交互优化：创建议题时，如果只有一个项目，则跳过「选择项目」"), findsOneWidget);
    await tester.tap(find.text("Feature：交互优化：创建议题时，如果只有一个项目，则跳过「选择项目」"));
    await tester.pumpAndSettle();
    expect(find.byType(IssueSelector), findsNothing);
    expect(find.textContaining("#276"), findsOneWidget);
  });

  tearDown(() {
    ConnectionProvider().fullReset();
  });
}

Map<String, dynamic> projectIssuesGraphQLResponseData = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "issues": {
        "pageInfo": {
          "hasNextPage": true,
          "startCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMi0xMi0yMiAxMjoyODozMS4yMzk4MzYwMDAgKzA4MDAiLCJpZCI6IjI5NTMyOSJ9",
          "endCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMi0xMi0wMSAxOTo0MzoxNi44MjEzODIwMDAgKzA4MDAiLCJpZCI6IjI3NjA0OSJ9"
        },
        "nodes": [
          {
            "id": "gid://gitlab/Issue/295329",
            "iid": "276",
            "projectId": 59893,
            "createdAt": "2022-12-22T12:28:31+08:00",
            "title": "Feature：交互优化：创建议题时，如果只有一个项目，则跳过「选择项目」",
            "updatedAt": "2022-12-26T15:30:40+08:00",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {
              "id": "gid://gitlab/User/1795",
              "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
              "name": "Zhou YANG",
              "username": "sinkcup",
              "webUrl": "https://jihulab.com/sinkcup"
            }
          },
        ]
      }
    }
  }
};

Map<String, dynamic> issueDetailsGraphQLResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "name": "demo mr test",
      "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab App",
      "path": "demo-mr-test",
      "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
      "issue": {
        "id": "gid://gitlab/Issue/323380",
        "iid": "640",
        "projectId": 59893,
        "title": "回复评论",
        "description": "issue description for test",
        "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/issues/640",
        "confidential": true,
        "state": "opened",
        "createdAt": "2022-11-25T14:19:30.461+08:00",
        "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "miaolu", "username": "perity"},
        "assignees": {
          "nodes": [
            {"id": "gid://gitlab/User/30192", "avatarUrl": "/uploads/-/system/user/avatar/30192/avatar.png", "name": "AssigneeName", "username": "assignee-username"}
          ]
        },
        "labels": {
          "nodes": [
            {"id": "gid://gitlab/ProjectLabel/45575", "title": "P::M", "description": "Must Have", "color": "#6699cc", "textColor": "#FFFFFF"}
          ]
        }
      }
    }
  }
};

Map<String, dynamic> issuePostVotesGraphQLResponse = {
  "data": {
    "project": {
      "issue": {"downvotes": 0, "upvotes": 0}
    }
  }
};
