import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart' as r;
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/modules/issues/index.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issue_selector.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../core/net/http_request_test.mocks.dart';
import '../finder/svg_finder.dart';
import '../mocker/tester.dart';

void main() {
  var issueDetailsModel = IssueDetailsModel();
  locator.registerSingleton(issueDetailsModel);
  late MockHttpClient client;

  setUp(() async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    await ConnectionProvider().restore();
    ConnectionProvider().reset(Tester.jihuLabUser());
    client = MockHttpClient();
    ConnectionProvider().reset(Tester.jihuLabUser());
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets('Should be able to link issue in issue description', (tester) async {
    when(client.get<Map<String, dynamic>>("/api/v4/projects/72936"))
        .thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>({"path_with_namespace": "ultimate-plan/jihu-gitlab-app/demo-mr-test"})));
    when(client.post<dynamic>('/api/graphql', any)).thenAnswer((_) => Future(() => r.Response.of<dynamic>(projectIssuesGraphQLResponseData)));
    HttpClient.injectInstanceForTesting(client);

    await tester.pumpWidget(MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => ConnectionProvider()),
      ],
      child: MaterialApp(
        onGenerateRoute: onGenerateRoute,
        home: const Scaffold(
          body: IssueCreationPage(projectId: 72936, groupId: 1, from: "project"),
        ),
        localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
      ),
    ));

    await tester.pumpAndSettle();
    expect(SvgFinder("assets/images/numbers.svg"), findsOneWidget);
    await tester.tap(SvgFinder("assets/images/numbers.svg"));
    await tester.pumpAndSettle();
    expect(find.byType(IssueSelector), findsOneWidget);
    expect(find.text("Feature：交互优化：创建议题时，如果只有一个项目，则跳过「选择项目」"), findsOneWidget);
    await tester.tap(find.text("Feature：交互优化：创建议题时，如果只有一个项目，则跳过「选择项目」"));
    await tester.pumpAndSettle();
    expect(find.byType(IssueSelector), findsNothing);
    expect(find.textContaining("#276"), findsOneWidget);
  });

  tearDown(() {
    ConnectionProvider().fullReset();
  });
}

Map<String, dynamic> projectIssuesGraphQLResponseData = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "issues": {
        "pageInfo": {
          "hasNextPage": true,
          "startCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMi0xMi0yMiAxMjoyODozMS4yMzk4MzYwMDAgKzA4MDAiLCJpZCI6IjI5NTMyOSJ9",
          "endCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMi0xMi0wMSAxOTo0MzoxNi44MjEzODIwMDAgKzA4MDAiLCJpZCI6IjI3NjA0OSJ9"
        },
        "nodes": [
          {
            "id": "gid://gitlab/Issue/295329",
            "iid": "276",
            "projectId": 59893,
            "createdAt": "2022-12-22T12:28:31+08:00",
            "title": "Feature：交互优化：创建议题时，如果只有一个项目，则跳过「选择项目」",
            "updatedAt": "2022-12-26T15:30:40+08:00",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {
              "id": "gid://gitlab/User/1795",
              "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
              "name": "Zhou YANG",
              "username": "sinkcup",
              "webUrl": "https://jihulab.com/sinkcup"
            }
          },
        ]
      }
    }
  }
};
