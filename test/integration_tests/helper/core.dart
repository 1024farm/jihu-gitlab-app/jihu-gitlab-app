import 'package:flutter/foundation.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:mockito/mockito.dart';

import '../../core/net/http_request_test.mocks.dart';
import '../../mocker/tester.dart';
import '../../test_data/user.dart';

final client = MockHttpClient();

void saasLogin({VoidCallback? onMock}) {
  when(client.get<Map<String, dynamic>>("/api/v4/user")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
  when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=done")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  when(client.get<List<dynamic>>("/api/v4/groups?top_level_only=true&page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  when(client.get<List<dynamic>>("/api/v4/users/0/starred_projects?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  when(client.get<List<dynamic>>("/api/v4/users/9527/starred_projects?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  onMock?.call();
  HttpClient.injectInstanceForTesting(client);

  expect(ConnectionProvider.authorized, isFalse);
  ConnectionProvider().reset(Tester.jihuLabUser());
  ConnectionProvider().notifyChanged();
  expect(ConnectionProvider.authorized, isTrue);
}

void privateTokenLogin({VoidCallback? onMock}) {
  when(client.get<Map<String, dynamic>>("/api/v4/user")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
  when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=done")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  when(client.get<List<dynamic>>("/api/v4/groups?top_level_only=true&page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  when(client.get<List<dynamic>>("/api/v4/users/0/starred_projects?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  when(client.get<List<dynamic>>("/api/v4/users/9527/starred_projects?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  onMock?.call();
  HttpClient.injectInstanceForTesting(client);

  expect(ConnectionProvider.authorized, isFalse);
  ConnectionProvider().reset(Tester.selfManagedUser());
  expect(ConnectionProvider.isSelfManagedGitLab, true);
  ConnectionProvider().notifyChanged();
  expect(ConnectionProvider.authorized, isTrue);
}

void logout() {
  expect(ConnectionProvider.authorized, isTrue);
  var connection = ConnectionProvider.connection!;
  ConnectionProvider().loggedOutSelectedConnection(connection);
  expect(ConnectionProvider.authorized, isFalse);
}
