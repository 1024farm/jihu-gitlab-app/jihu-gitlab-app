import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/global_keys.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/main.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';
import 'package:jihu_gitlab_app/modules/root/desktop_root_page.dart';

import '../../../test_binding_setter.dart';
import '../core.dart';

Future<void> launchTabletAppForTest(tester) async {
  await setUpDesktopBinding(tester);

  await tester.pumpWidget(const App());
  await tester.pumpAndSettle();
  await tester.ensureVisible(find.byType(DesktopRootPage));
}

Future<void> navigateToTargetTabletPage(tester, int targetPageIndex, Type pageType) async {
  var navigationDestinations = RootModel.navigationDestinations(AppLocalizations.dictionary(), context: GlobalKeys.rootAppKey.currentContext!);
  var destination = navigationDestinations[targetPageIndex];
  await tester.tap(find.widgetWithText(ListTile, destination.title));
  await tester.pumpAndSettle(const Duration(seconds: 1));
  expect(find.byWidgetPredicate((widget) => widget is ListTile && (widget.title as Text).data == destination.title && widget.selected), findsOneWidget);
  expect(find.byType(pageType), findsOneWidget);
}

Future<void> tabletLoginWithSaaSForPage(tester, index, pageType, {VoidCallback? onMock}) async {
  await navigateToTargetTabletPage(tester, PageType.todo.index, ToDosPage);
  saasLogin(onMock: onMock);

  await navigateToTargetTabletPage(tester, PageType.faq.index, CommunityPage);
  await navigateToTargetTabletPage(tester, PageType.todo.index, ToDosPage);

  await navigateToTargetTabletPage(tester, index, pageType);
}

Future<void> tabletLoginWithPrivateTokenForPage(tester, index, pageType, {VoidCallback? onMock}) async {
  await navigateToTargetTabletPage(tester, PageType.todo.index, ToDosPage);
  privateTokenLogin(onMock: onMock);

  await navigateToTargetTabletPage(tester, PageType.faq.index, CommunityPage);
  await navigateToTargetTabletPage(tester, PageType.todo.index, ToDosPage);

  await navigateToTargetTabletPage(tester, index, pageType);
}

Future<void> tabletLogoutForPage(tester, index, pageType) async {
  await navigateToTargetTabletPage(tester, PageType.todo.index, ToDosPage);
  logout();

  await navigateToTargetTabletPage(tester, PageType.faq.index, CommunityPage);
  await navigateToTargetTabletPage(tester, PageType.todo.index, ToDosPage);

  await navigateToTargetTabletPage(tester, index, pageType);
}
