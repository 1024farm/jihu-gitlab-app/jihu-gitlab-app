import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/global_keys.dart';
import 'package:jihu_gitlab_app/main.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';
import 'package:jihu_gitlab_app/modules/root/mobile_root_page.dart';

import '../../../test_binding_setter.dart';
import '../core.dart';

Future<void> launchMobileAppForTest(tester) async {
  await setUpMobileBinding(tester);

  await tester.pumpWidget(const App());
  await tester.pumpAndSettle();
  await tester.ensureVisible(find.byType(MobileRootPage));
  await tester.ensureVisible(find.byType(BottomNavigationBar));
}

Future<void> navigateToTargetMobilePage(tester, int targetPageIndex, Type pageType) async {
  BottomNavigationBar navigationBar = GlobalKeys.rootBottomNavigationBarKey.currentWidget as BottomNavigationBar;
  navigationBar.onTap!(targetPageIndex);
  await tester.pumpAndSettle(const Duration(seconds: 1));
  navigationBar = GlobalKeys.rootBottomNavigationBarKey.currentWidget as BottomNavigationBar;
  expect(navigationBar.currentIndex, targetPageIndex);
  expect(find.byType(pageType), findsOneWidget);
}

Future<void> mobileLoginWithSaaSForPage(tester, index, pageType, {VoidCallback? onMock}) async {
  await navigateToTargetMobilePage(tester, PageType.todo.index, ToDosPage);
  saasLogin(onMock: onMock);

  await navigateToTargetMobilePage(tester, PageType.faq.index, CommunityPage);
  await navigateToTargetMobilePage(tester, PageType.todo.index, ToDosPage);

  await navigateToTargetMobilePage(tester, index, pageType);
}

Future<void> mobileLoginWithPrivateTokenForPage(tester, index, pageType, {VoidCallback? onMock}) async {
  await navigateToTargetMobilePage(tester, PageType.todo.index, ToDosPage);
  privateTokenLogin(onMock: onMock);

  await navigateToTargetMobilePage(tester, PageType.faq.index, CommunityPage);
  await navigateToTargetMobilePage(tester, PageType.todo.index, ToDosPage);

  await navigateToTargetMobilePage(tester, index, pageType);
}

Future<void> mobileLogoutForPage(tester, index, pageType) async {
  await navigateToTargetMobilePage(tester, PageType.todo.index, ToDosPage);
  logout();
  await navigateToTargetMobilePage(tester, PageType.faq.index, CommunityPage);
  await navigateToTargetMobilePage(tester, PageType.todo.index, ToDosPage);
  await navigateToTargetMobilePage(tester, index, pageType);
}
