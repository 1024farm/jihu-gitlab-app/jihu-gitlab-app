import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/db_manager.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/modules/ai/ai_page.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message_entity.dart';
import 'package:mockito/mockito.dart';

import '../core/net/http_request_test.mocks.dart';
import '../modules/ai/ai_page_test.mocks.dart';

void main() {
  final MockHttpClient client = MockHttpClient();
  final MockDatabase mockDatabase = MockDatabase();

  setUp(() {
    DbManager.instance().injectDatabaseForTesting(mockDatabase);
  });

  testWidgets('Should open faq page and send a message', (tester) async {
    when(client.postWithHeader('https://gitlablang.cognitiveservices.azure.com/language/:query-knowledgebases?projectName=jihu&api-version=2021-10-01&deploymentName=production', {
      "top": 1,
      "question": "abc",
      "includeUnstructuredSources": true,
      "confidenceScoreThreshold": "0.5",
      "answerSpanRequest": {"enable": true, "topAnswersWithSpan": 1, "confidenceScoreThreshold": "0.5"}
    }, {
      'Ocp-Apim-Subscription-Key': ""
    })).thenAnswer((_) => Future(() => Response.of({
          "answers": [
            {
              "questions": ["GitLab CI 可以公司统一流程，多个项目共用一个流水线吗？"],
              "answer": "Yes!",
              "confidenceScore": 1.0,
              "id": 102,
              "source": "jihulab-presales-playground-faq_issues_2023-01-05.xlsx",
              "metadata": {},
              "dialog": {"isContextOnly": false, "prompts": []}
            }
          ]
        })));
    when(client.postWithHeader('https://gitlablang.cognitiveservices.azure.com/language/:query-knowledgebases?projectName=jihu&api-version=2021-10-01&deploymentName=production', {
      "top": 1,
      "question": "e",
      "includeUnstructuredSources": true,
      "confidenceScoreThreshold": "0.5",
      "answerSpanRequest": {"enable": true, "topAnswersWithSpan": 1, "confidenceScoreThreshold": "0.5"}
    }, {
      'Ocp-Apim-Subscription-Key': ""
    })).thenThrow(Exception());
    HttpClient.injectInstanceForTesting(client);
    await tester.pumpWidget(const MaterialApp(
      home: Scaffold(
        body: Center(
          child: AIPage(arguments: {}),
        ),
      ),
    ));
    await tester.pumpAndSettle();
    expect(find.text("AI"), findsOneWidget);
    expect(find.text("Enter your question"), findsOneWidget);

    await tester.tap(find.text("Enter your question"), warnIfMissed: false);
    await tester.pumpAndSettle();
    await tester.tap(find.byKey(const Key("list-view")));
    await tester.pumpAndSettle();

    when(mockDatabase.insert(MessageEntity.tableName, any)).thenAnswer((_) => Future(() => 1));
    when(mockDatabase.update(MessageEntity.tableName, any, where: anyNamed("where"), whereArgs: anyNamed("whereArgs"))).thenAnswer((_) => Future(() => 1));
    await tester.enterText(find.byType(TextField), 'abc');
    await tester.testTextInput.receiveAction(TextInputAction.done);
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.text("Enter your question"), findsOneWidget);
    expect(find.text("abc"), findsOneWidget);
    expect(find.text("Yes!"), findsOneWidget);

    await tester.enterText(find.byType(TextField), 'e');
    await tester.testTextInput.receiveAction(TextInputAction.done);
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.text("Enter your question"), findsOneWidget);
    expect(find.text("abc"), findsOneWidget);
    expect(find.byIcon(Icons.error), findsOneWidget);
  });
}
