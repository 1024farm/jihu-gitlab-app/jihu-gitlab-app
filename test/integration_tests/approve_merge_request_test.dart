import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart' as r;
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_button.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/l10n/current_locale.dart';
import 'package:jihu_gitlab_app/modules/mr/approval_rule_view.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_request_page.dart';
import 'package:jihu_gitlab_app/modules/mr/models/mr_graphql_request_body.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../core/net/http_request_test.mocks.dart';
import '../mocker/tester.dart';
import '../test_data/merge_request.dart';

void main() {
  late MockHttpClient client;

  setUp(() async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    await ConnectionProvider().restore();
    ConnectionProvider().reset(Tester.jihuLabUser());
    client = MockHttpClient();
    ConnectionProvider().reset(Tester.jihuLabUser());
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets('Should be able to approve the mr with 2 approval rule and 1 approval left', (tester) async {
    when(client.get<Map<String, dynamic>>("/api/v4/projects/72936/merge_requests/17/approvals")).thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>(approvalResponse)));
    when(client.post("/api/v4/projects/72936/merge_requests/17/approve", {})).thenAnswer((_) => Future(() => r.Response.of({})));
    when(client.get('/api/v4/projects/72936')).thenAnswer((_) => Future(() => r.Response.of<Map>({"path_with_namespace": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app"})));
    when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 17)))
        .thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>(mrGraphQLResponse)));
    when(client.post('/api/graphql', getPaidMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 17)))
        .thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>(paidMrGraphQLResponse)));
    when(client.post('/api/graphql', queryJobsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 17, ''))).thenAnswer((_) => Future(() => r.Response.of(jobsGraphQLResponse)));
    when(client.post("/api/graphql", queryCommitsGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 17, ''))).thenAnswer((_) => Future(() => r.Response.of(commitsGraphQLResponse)));
    when(client.get<List<Map<String, dynamic>>>("/api/v4/projects/72936/merge_requests/17/diffs?page=1&per_page=20")).thenAnswer((_) => Future(() => r.Response.of<List<Map<String, dynamic>>>([])));

    var parameters = {'projectId': 72936, 'projectName': "demo", 'mergeRequestIid': 17, 'fullPath': 'ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 'test': true};
    await tester.pumpWidget(MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => ConnectionProvider()),
        ChangeNotifierProvider(create: (context) => LocaleProvider()),
      ],
      child: MaterialApp(
        onGenerateRoute: onGenerateRoute,
        home: Scaffold(
          body: MergeRequestPage(arguments: parameters),
        ),
        localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
      ),
    ));

    await tester.pumpAndSettle();
    expect(find.text("Requires 1 approval from License-Check."), findsOneWidget);
    expect(find.byType(ApprovalRuleView), findsNWidgets(3));
    expect(find.text("1 of 1 approval from Coverage-Check"), findsOneWidget);
    expect(find.text("1 of 2 approvals from License-Check"), findsOneWidget);

    final listFinder = find.byType(Scrollable).last;
    await tester.scrollUntilVisible(find.widgetWithText(LoadingButton, "Approve"), 500.0, scrollable: listFinder);
    expect(find.widgetWithText(LoadingButton, "Approve"), findsOneWidget);
    await tester.tap(find.widgetWithText(LoadingButton, "Approve"));
    await tester.pumpAndSettle();
    verify(client.post("/api/v4/projects/72936/merge_requests/17/approve", {})).called(1);
  });

  tearDown(() {
    ConnectionProvider().fullReset();
  });
}

Map<String, dynamic> approvalResponse = {
  "id": 227725,
  "iid": 17,
  "project_id": 72936,
  "title": "Draft: Resolve \"Test multi approval rules 2\"",
  "description": "Closes #46",
  "state": "opened",
  "created_at": "2023-02-08T10:01:07.483+08:00",
  "updated_at": "2023-02-08T11:38:20.329+08:00",
  "merge_status": "can_be_merged",
  "approved": false,
  "approvals_required": 3,
  "approvals_left": 1,
  "require_password_to_approve": false,
  "approved_by": [
    {
      "user": {
        "id": 23837,
        "username": "wanyouzhu",
        "name": "万友 朱",
        "state": "active",
        "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/23837/avatar.png",
        "web_url": "https://jihulab.com/wanyouzhu"
      }
    }
  ],
  "suggested_approvers": [
    {
      "id": 1795,
      "username": "sinkcup",
      "name": "Zhou YANG",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/1795/avatar.png",
      "web_url": "https://jihulab.com/sinkcup"
    },
    {
      "id": 29064,
      "username": "NeilWang",
      "name": "Neil Wang",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29064/avatar.png",
      "web_url": "https://jihulab.com/NeilWang"
    },
    {
      "id": 29355,
      "username": "perity",
      "name": "miaolu",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29355/avatar.png",
      "web_url": "https://jihulab.com/perity"
    },
    {
      "id": 30192,
      "username": "raymond-liao",
      "name": "Raymond Liao",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/30192/avatar.png",
      "web_url": "https://jihulab.com/raymond-liao"
    },
    {"id": 29473, "username": "zombie", "name": "鑫 范", "state": "active", "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29473/avatar.png", "web_url": "https://jihulab.com/zombie"},
    {
      "id": 23836,
      "username": "jojo0",
      "name": "yajie xue",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/23836/avatar.png",
      "web_url": "https://jihulab.com/jojo0"
    }
  ],
  "approvers": [],
  "approver_groups": [],
  "user_has_approved": false,
  "user_can_approve": true,
  "approval_rules_left": [
    {"id": 71387, "name": "License-Check", "rule_type": "report_approver"}
  ],
  "has_approval_rules": true,
  "merge_request_approvers_available": true,
  "multiple_approval_rules_available": true,
  "invalid_approvers_rules": []
};
