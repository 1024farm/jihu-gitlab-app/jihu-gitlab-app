import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/global_keys.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/stars_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/home/home_app_bar.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/main.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';
import 'package:jihu_gitlab_app/modules/root/desktop_root_page.dart';
import 'package:mockito/mockito.dart';

import '../core/net/http_request_test.mocks.dart';
import '../test_binding_setter.dart';
import '../test_data/user.dart';
import 'modules/community/community_page_modules.dart';

void main() {
  final client = MockHttpClient();

  setUp(() async {
    WidgetsFlutterBinding.ensureInitialized();
    setupLocator();
    mockCommunity(client);
  });

  group('Should remember the last home page of navigation bar', () {
    testWidgets('Should app first launch with projects page', (tester) async {
      await setUpDesktopBinding(tester);
      await RootStore().restore();
      await tester.pumpWidget(const App());
      for (int i = 0; i < 5; i++) {
        await tester.pumpAndSettle(const Duration(seconds: 1));
      }
      await tester.ensureVisible(find.byType(DesktopRootPage));

      expect(find.byWidgetPredicate((widget) => widget is ListTile && (widget.title as Text).data == "Groups" && widget.selected), findsOneWidget);
      expect(find.byType(ProjectsGroupsPage), findsOneWidget);
      expect(find.widgetWithText(HomeAppBar, "Groups"), findsOneWidget);
      expect(find.text("Groups"), findsWidgets);
    });

    testWidgets('Should navigation to ToDosPage page when tapped the To-Do List navigation item', (tester) async {
      await setUpDesktopBinding(tester);
      await RootStore().restore();
      await tester.pumpWidget(const App());
      await tester.pumpAndSettle(const Duration(seconds: 1));
      var finder = find.byType(IndexedStack);
      expect(tester.widget<IndexedStack>(finder).index, 1);
      expect(find.widgetWithText(HomeAppBar, "Groups"), findsOneWidget);

      await tester.tap(find.widgetWithText(ListTile, "To-dos"));
      await tester.pumpAndSettle(const Duration(seconds: 1));
      expect(find.byWidgetPredicate((widget) => widget is ListTile && (widget.title as Text).data == "To-dos" && widget.selected), findsOneWidget);
      expect(find.byType(ToDosPage), findsOneWidget);
      expect(tester.widget<IndexedStack>(finder).index, 0);
      expect(find.byType(IndexedStack), findsOneWidget);
      expect(find.widgetWithText(Tab, "To Do"), findsOneWidget);
      expect(find.widgetWithText(Tab, "Done"), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().toDos), findsWidgets);
    });

    testWidgets('Should app launch with last exited page projects page', (tester) async {
      LocalStorage.save("navigation_bar_current_index", 1);
      await RootStore().restore();
      await setUpDesktopBinding(tester);
      await tester.pumpWidget(const App());
      for (int i = 0; i < 5; i++) {
        await tester.pump(const Duration(seconds: 1));
      }
      var finder = find.byType(IndexedStack);
      expect(tester.widget<IndexedStack>(finder).index, 1);
      expect(find.byWidgetPredicate((widget) => widget is ListTile && (widget.title as Text).data == "Groups" && widget.selected), findsOneWidget);
      expect(find.byType(ToDosPage), findsOneWidget);
      expect(find.byType(ProjectsGroupsPage), findsOneWidget);
      expect(find.byType(CommunityPage), findsOneWidget);
      expect(find.widgetWithText(Tab, "To Do"), findsOneWidget);
      expect(find.widgetWithText(Tab, "Done"), findsOneWidget);
      expect(find.widgetWithText(CommonAppBar, "Groups"), findsNothing);
    });
  });

  testWidgets('Should run app in desktop mode with tablet size (1280, 850)', (tester) async {
    await setUpDesktopBinding(tester);
    when(client.getWithHeader<Map<String, dynamic>>("https://jihulab.com/api/v4/user", any)).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
    when(client.get<List<dynamic>>("/api/v4/users/10000/starred_projects?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
    when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
    when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=done")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
    when(client.get<List<dynamic>>("/api/v4/groups?top_level_only=true&page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
    when(client.get<List<dynamic>>("/api/v4/users/9527/starred_projects?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
    await tester.pumpWidget(const App());
    await tester.pumpAndSettle();
    await tester.ensureVisible(find.byType(DesktopRootPage));

    await leftNavigationBarTest(tester, client);
  });

  tearDown(() {
    ConnectionProvider().fullReset();
    StarsProvider().fullReset();
    unregister();
    reset(client);
  });
}

void mockRequestForFirstLogin(MockHttpClient client) {
  when(client.get<Map<String, dynamic>>("/api/v4/user")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
  when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  when(client.get<List<dynamic>>("/api/v4/groups?top_level_only=true&page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  when(client.get<List<dynamic>>("/api/v4/users/9527/starred_projects?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
  HttpClient.injectInstanceForTesting(client);
}

Future<void> leftNavigationBarTest(tester, MockHttpClient client) async {
  expect(find.text(AppLocalizations.dictionary().appName), findsOneWidget);
  await navigationItemsTest(PageType.faq.index, tester, CommunityPage);
  await navigationItemsTest(PageType.todo.index, tester, ToDosPage);
  await navigationItemsTest(PageType.projects.index, tester, ProjectsGroupsPage);
}

Future<void> navigationItemsTest(int index, WidgetTester tester, Type type) async {
  var navigationDestinations = RootModel.navigationDestinations(AppLocalizations.dictionary(), context: GlobalKeys.rootAppKey.currentContext!);
  var destination = navigationDestinations[index];
  expect(find.byKey(Key(destination.title)), findsOneWidget);
  await tester.pumpAndSettle();
  await tester.ensureVisible(find.byKey(Key(destination.title)));
  await tester.tap(find.byKey(Key(destination.title)));
  expect(find.byType(type), findsOneWidget);
}
