import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/global_keys.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/widgets/home/home_app_bar.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/main.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';
import 'package:jihu_gitlab_app/modules/root/mobile_root_page.dart';

import '../core/net/http_request_test.mocks.dart';
import '../test_binding_setter.dart';
import 'modules/community/community_page_modules.dart';

void main() {
  final client = MockHttpClient();

  setUp(() async {
    WidgetsFlutterBinding.ensureInitialized();
    setupLocator();
    mockCommunity(client);
  });

  group('Should remember the last home page of navigation bar', () {
    testWidgets('Should app first launch with projects page', (tester) async {
      await setUpMobileBinding(tester);
      await RootStore().restore();
      await tester.pumpWidget(const App());
      for (int i = 0; i < 5; i++) {
        await tester.pumpAndSettle(const Duration(seconds: 1));
      }
      await tester.ensureVisible(find.byType(BottomNavigationBar));
      BottomNavigationBar navigationBar = GlobalKeys.rootBottomNavigationBarKey.currentWidget as BottomNavigationBar;
      expect(navigationBar.currentIndex, 1);
      expect(find.widgetWithText(HomeAppBar, "Groups"), findsOneWidget);
      expect(find.byType(ProjectsPage), findsOneWidget);
      expect(find.text("Groups"), findsWidgets);
    });

    testWidgets('Should navigation to ToDosPage page when tapped the To-Do List navigation item', (tester) async {
      await setUpMobileBinding(tester);
      await RootStore().restore();
      await tester.pumpWidget(const App());
      await tester.pumpAndSettle();

      BottomNavigationBar navigationBar = GlobalKeys.rootBottomNavigationBarKey.currentWidget as BottomNavigationBar;
      expect(navigationBar.currentIndex, 1);
      expect(find.text("Projects"), findsOneWidget);
      expect(find.text("To-dos"), findsOneWidget);
      expect(find.byType(ProjectsPage), findsOneWidget);
      expect(find.widgetWithText(Tab, "Starred"), findsOneWidget);
      expect(find.widgetWithText(Tab, "Groups"), findsOneWidget);
      await tester.tap(find.text("To-dos"));
      await tester.pumpAndSettle(const Duration(seconds: 1));

      navigationBar = GlobalKeys.rootBottomNavigationBarKey.currentWidget as BottomNavigationBar;
      expect(navigationBar.currentIndex, 0);
      expect(find.byType(ToDosPage), findsOneWidget);
      expect(find.widgetWithText(Tab, "To Do"), findsOneWidget);
      expect(find.widgetWithText(Tab, "Done"), findsOneWidget);
    });

    testWidgets('Should app launch with last exited page', (tester) async {
      await setUpMobileBinding(tester);
      LocalStorage.save("navigation_bar_current_index", 2);
      await RootStore().restore();
      await tester.pumpWidget(const App());
      for (int i = 0; i < 5; i++) {
        await tester.pump(const Duration(seconds: 1));
      }

      BottomNavigationBar navigationBar = GlobalKeys.rootBottomNavigationBarKey.currentWidget as BottomNavigationBar;
      expect(navigationBar.currentIndex, 2);
      expect(find.byType(CommunityPage), findsOneWidget);
      expect(find.widgetWithText(HomeAppBar, "Community"), findsOneWidget);
    });
  });

  testWidgets('Should run app in mobile mode with mobile size (540, 960)', (tester) async {
    await setUpMobileBinding(tester);
    mockCommunity(client);
    await tester.pumpWidget(const App());
    await tester.pumpAndSettle();
    await tester.ensureVisible(find.byType(MobileRootPage));

    await bottomNavigationItemsTest(tester);
  });

  tearDown(() {
    ConnectionProvider().fullReset();
    unregister();
  });
}

Future<void> bottomNavigationItemsTest(tester) async {
  await tester.ensureVisible(find.byType(BottomNavigationBar));
  await pageTest(PageType.faq.index, tester, CommunityPage);
  await pageTest(PageType.todo.index, tester, ToDosPage);
  await pageTest(PageType.projects.index, tester, ProjectsPage);
}

Future<void> pageTest(int index, WidgetTester tester, Type type) async {
  var navigationDestinations = RootModel.navigationDestinations(AppLocalizations.dictionary(), context: GlobalKeys.rootAppKey.currentContext!);
  var destination = navigationDestinations[index];
  final navigationBarItem = find.byIcon(destination.icon.icon!);
  expect(navigationBarItem, findsOneWidget);

  BottomNavigationBar navigationBar = GlobalKeys.rootBottomNavigationBarKey.currentWidget as BottomNavigationBar;
  navigationBar.onTap!(index);
  await tester.pumpAndSettle();
  expect(find.byType(type), findsOneWidget);
}
