import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/subgroup_and_project_provider.dart';
import 'package:jihu_gitlab_app/modules/projects/projects.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../core/net/http_request_test.mocks.dart';
import '../mocker/tester.dart';
import '../modules/projects/starred/projects_starred_page_test.mocks.dart';
import '../test_binding_setter.dart';
import '../test_data/issue.dart';
import '../test_data/project.dart';
import 'go_into_sub_group_page_test.mocks.dart';

final client = MockHttpClient();

@GenerateNiceMocks([MockSpec<SubgroupAndProjectProvider>()])
void main() {
  late MockProjectsStarredProvider starredProvider;
  var projectsStarredModel = ProjectsStarredModel();
  locator.registerSingleton(projectsStarredModel);
  starredProvider = MockProjectsStarredProvider();
  projectsStarredModel.injectDataProviderForTesting(starredProvider);

  var subgroupListModel = SubgroupListModel();
  locator.registerSingleton(subgroupListModel);
  var provider = MockSubgroupAndProjectProvider();
  when(provider.loadFromLocal())
      .thenAnswer((_) => Future(() => [GroupAndProject(1, 59893, "极狐 GitLab APP 代码", SubgroupItemType.project, "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 0, starred: true)]));
  subgroupListModel.injectDataProviderForTesting(provider);

  testWidgets('Should go in to project page', (tester) async {
    await setUpMobileBinding(tester);

    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    ConnectionProvider().reset(Tester.jihuLabUser());
    when(provider.syncFromRemote()).thenAnswer((_) => Future(() => false));
    when(client.post<dynamic>('/api/graphql', any)).thenAnswer((_) => Future(() => Response.of<dynamic>(projectIssuesGraphQLResponseData)));
    when(client.get("/api/v4/projects/59893")).thenAnswer((_) => Future(() => Response.of(jiHuProjectData)));
    when(client.get<Map<String, dynamic>>("/api/v4/groups/0?all_available=true&page=1&per_page=50")).thenAnswer((_) async => Future.value(Response.of({})));
    when(client.get<List<dynamic>>("/api/v4/groups/0/subgroups?all_available=true&page=1&per_page=50")).thenAnswer((_) async => Future.value(Response.of([])));
    HttpClient.injectInstanceForTesting(client);
    await tester.pumpWidget(MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
        child: MaterialApp(
          routes: {
            ProjectPage.routeName: (context) => const ProjectPage(arguments: {"name": "极狐 GitLab APP 代码", "projectId": 59893, "relativePath": "relativePath"})
          },
          home: const Scaffold(
            body: SubgroupListPage(relativePath: 'relativePath', groupId: 0),
          ),
        )));

    await tester.pumpAndSettle();
    expect(find.text('极狐 GitLab APP 代码'), findsOneWidget);
    await tester.tap(find.text('极狐 GitLab APP 代码'));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.widgetWithText(Tab, "Issues"), findsOneWidget);
    await tester.tap(find.widgetWithText(Tab, "Issues"));
    await tester.pumpAndSettle();
    expect(find.text('Feature：交互优化：创建议题时，如果只有一个项目，则跳过「选择项目」'), findsOneWidget);
  });
}
