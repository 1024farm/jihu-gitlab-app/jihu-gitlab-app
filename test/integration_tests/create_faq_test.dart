import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/db_manager.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart' as r;
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/streaming_container.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/l10n/current_locale.dart';
import 'package:jihu_gitlab_app/modules/community/community_page.dart';
import 'package:jihu_gitlab_app/modules/community/community_post_creation_page.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/index.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/models/issue_draft_entity.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../core/net/http_request_test.mocks.dart';
import '../finder/svg_finder.dart';
import '../finder/text_field_hint_text_finder.dart';
import '../mocker/tester.dart';
import '../modules/ai/ai_page_test.mocks.dart';
import '../test_binding_setter.dart';
import 'modules/community/community_page_modules.dart';

void main() {
  var issueDetailsModel = IssueDetailsModel();
  locator.registerSingleton(issueDetailsModel);
  late MockHttpClient client;

  setUp(() async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    await ConnectionProvider().restore();
    ConnectionProvider().reset(Tester.jihuLabUser());
    ConnectionProvider().injectConnectionForTest(Tester.selfManagedUser());
    client = MockHttpClient();
    when(client.getWithConnection("/api/v4/projects/89335", any)).thenAnswer((_) => Future(() => r.Response.of({
          "permissions": {
            "project_access": {"access_level": 50}
          }
        })));
    when(client.postWithConnection<dynamic>("/api/graphql", getProjectMembersGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', 'jihulab_tester', ''), any))
        .thenAnswer((_) => Future(() => r.Response.of(accessedProjectsMembersGraphQLResponse)));
    when(client.postWithConnection('/api/graphql', argThat(predicate((arg) => arg.toString().contains("labels"))), any)).thenAnswer((_) => Future(() => r.Response.of(faqTypesResponse)));
    when(client.postWithConnection<dynamic>("/api/graphql", getCommunityPostListGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', ['lang::en'], '', '', 20), any))
        .thenAnswer((_) => Future(() => r.Response.of(allCommunityPostListGraphQLResponse)));
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets('Should create faq', (tester) async {
    setUpMobileBinding(tester);
    when(client.postWithConnection<Map<String, dynamic>>('/api/v4/projects/89335/issues', any, any)).thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>({})));
    HttpClient.injectInstanceForTesting(client);

    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
      child: MaterialApp(
        onGenerateRoute: onGenerateRoute,
        home: const Scaffold(body: CommunityPage()),
        localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
      ),
    ));

    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.byIcon(Icons.add_box_outlined), findsOneWidget);
    await tester.tap(find.byIcon(Icons.add_box_outlined));
    await tester.pumpAndSettle();
    expect(find.byType(CommunityPostCreationPage), findsOneWidget);
    expect(find.text("Title (required)"), findsOneWidget);
    expect(find.text("Type (required)"), findsOneWidget);
    expect(find.byType(StreamingContainer), findsOneWidget);
    expect(find.text("Write a Post title"), findsOneWidget);
    expect(find.text("Description"), findsOneWidget);
    expect(find.text("Write a description here..."), findsOneWidget);
    expect(SvgFinder("assets/images/video_picker.svg"), findsNothing);
    expect(SvgFinder("assets/images/photo_picker.svg"), findsOneWidget);
    expect(SvgFinder("assets/images/numbers.svg"), findsNothing);
    expect(SvgFinder("assets/images/alternate_email.svg"), findsNothing);
    expect(find.text("FAQ"), findsOneWidget);
    expect(find.text("WFH"), findsOneWidget);
    var buttonFinder = find.widgetWithText(TextButton, "Create");
    expect(buttonFinder, findsOneWidget);
    expect(tester.firstWidget<TextButton>(buttonFinder).onPressed, null);

    await tester.enterText(TextFieldHintTextFinder('Write a Post title'), 'example');
    await tester.pumpAndSettle();
    await tester.enterText(TextFieldHintTextFinder('Write a description here...'), 'example');
    await tester.pumpAndSettle();
    await tester.tap(find.text("FAQ"));
    await tester.pumpAndSettle();
    expect(tester.firstWidget<TextButton>(buttonFinder).onPressed, predicate((value) => value != null));
    await tester.tap(buttonFinder);
    await tester.pumpAndSettle(const Duration(seconds: 2));
    expect(find.byType(CommunityPage), findsOneWidget);
  });

  testWidgets('Should create faq failed', (tester) async {
    setUpMobileBinding(tester);

    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
      child: MaterialApp(
        onGenerateRoute: onGenerateRoute,
        home: const Scaffold(body: CommunityPage()),
        localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
      ),
    ));

    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.byIcon(Icons.add_box_outlined), findsOneWidget);
    await tester.tap(find.byIcon(Icons.add_box_outlined));
    await tester.pumpAndSettle();
    await tester.enterText(TextFieldHintTextFinder('Write a Post title'), 'example');
    await tester.pumpAndSettle();
    await tester.enterText(TextFieldHintTextFinder('Write a description here...'), 'example');
    await tester.pumpAndSettle();
    await tester.tap(find.byType(TextButton));
    await tester.pumpAndSettle(const Duration(seconds: 2));
    expect(find.byType(CommunityPostCreationPage), findsOneWidget);
  });

  testWidgets('Should cancel in faq', (tester) async {
    setUpMobileBinding(tester);

    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
      child: MaterialApp(
        onGenerateRoute: onGenerateRoute,
        home: const Scaffold(body: CommunityPage()),
        localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
      ),
    ));

    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.byIcon(Icons.add_box_outlined), findsOneWidget);
    await tester.tap(find.byIcon(Icons.add_box_outlined));
    await tester.pumpAndSettle();
    await tester.enterText(TextFieldHintTextFinder('Write a Post title'), 'example');
    await tester.pumpAndSettle();
    await tester.enterText(TextFieldHintTextFinder('Write a description here...'), 'example');
    await tester.pumpAndSettle();
    await tester.tap(find.text('Create Post'));
    await tester.pumpAndSettle();
    await tester.tap(find.byType(BackButton));
    await tester.pumpAndSettle();
    expect(find.byType(CommunityPage), findsOneWidget);
    await tester.pumpAndSettle(const Duration(seconds: 1));
    await tester.tap(find.byIcon(Icons.add_box_outlined));
  });

  testWidgets('Should not see the post creation button when no login account', (tester) async {
    setUpMobileBinding(tester);
    ConnectionProvider().loggedOut(byUser: true);
    ConnectionProvider().clearActive();
    ConnectionProvider().clearActive();
    when(client.postWithConnection<dynamic>("/api/graphql", getProjectMembersGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', '', ''), any))
        .thenAnswer((_) => Future(() => r.Response.of(faqListResponse)));

    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
      child: MaterialApp(
        onGenerateRoute: onGenerateRoute,
        home: const Scaffold(body: CommunityPage()),
        localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
      ),
    ));

    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.byIcon(Icons.add_box_outlined), findsNothing);
  });

  testWidgets('Should see the post creation button when user is login with jihulab.com SaaS account', (tester) async {
    setUpMobileBinding(tester);

    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(
        onGenerateRoute: onGenerateRoute,
        home: const Scaffold(body: CommunityPage()),
      ),
    ));

    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.byIcon(Icons.add_box_outlined), findsOneWidget);
  });

  testWidgets('Should save and load draft faq creation data from local storage', (tester) async {
    setUpMobileBinding(tester);

    final MockDatabase mockDatabase = MockDatabase();
    DbManager.instance().injectDatabaseForTesting(mockDatabase);
    when(mockDatabase.insert(IssueDraftEntity.tableName, argThat(predicate((map) => insertParamCheck(map))))).thenAnswer((_) => Future(() => 1));

    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(
        onGenerateRoute: onGenerateRoute,
        home: const Scaffold(body: CommunityPage()),
      ),
    ));

    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.byIcon(Icons.add_box_outlined), findsOneWidget);
    await tester.tap(find.byIcon(Icons.add_box_outlined));
    await tester.pumpAndSettle();
    expect(find.byType(CommunityPostCreationPage), findsOneWidget);
    expect(find.text("Title (required)"), findsOneWidget);
    expect(find.text("Type (required)"), findsOneWidget);
    expect(find.text("Description"), findsOneWidget);
    expect(find.text("FAQ"), findsOneWidget);
    expect(find.text("WFH"), findsOneWidget);

    await tester.enterText(TextFieldHintTextFinder('Write a Post title'), 'example');
    await tester.pumpAndSettle();
    await tester.enterText(TextFieldHintTextFinder('Write a description here...'), 'example');
    await tester.pumpAndSettle();
    await tester.tap(find.text("FAQ"));
    await tester.pumpAndSettle(const Duration(seconds: 5));
    var count = verify(mockDatabase.insert(IssueDraftEntity.tableName, argThat(predicate((map) => insertParamCheck(map))))).callCount;
    expect(count, greaterThan(0));
    var buttonFinder = find.widgetWithText(TextButton, "Create");
    expect(buttonFinder, findsOneWidget);
    expect(tester.firstWidget<TextButton>(buttonFinder).onPressed, predicate((value) => value != null));

    await tester.tap(find.byType(BackButton));
    await tester.pumpAndSettle();
    when(mockDatabase.query(IssueDraftEntity.tableName, where: " project_id = ? and user_id = ? ", whereArgs: [89335, 9527])).thenAnswer((_) => Future(() {
          return [
            {
              "user_id": 30192,
              "project_id": 89335,
              "title": "tester",
              "description": "tester-description",
              "template": null,
              "selected_assignees": "null",
              "selected_labels": '[{"id": 72059, "name": "type::WFH","description":"远程办公"}]',
              "version": 2,
              "confidential": 0
            }
          ];
        }));
    await tester.tap(find.byIcon(Icons.add_box_outlined));
    await tester.pumpAndSettle();
    expect(find.text("tester"), findsOneWidget);
    expect(find.text("tester-description"), findsOneWidget);
    expect(find.text("WFH"), findsOneWidget);
    expect(tester.renderObject<RenderParagraph>(find.text('WFH')).text.style!.color, AppThemeData.primaryColor);

    buttonFinder = find.widgetWithText(TextButton, "Create");
    expect(buttonFinder, findsOneWidget);
    expect(tester.firstWidget<TextButton>(buttonFinder).onPressed, predicate((value) => value != null));

    when(client.postWithConnection<Map<String, dynamic>>('/api/v4/projects/89335/issues', {'title': 'tester', 'description': 'tester-description', 'labels': "type::WFH,lang::en"}, any))
        .thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>({})));
    await tester.tap(buttonFinder);
    await tester.pumpAndSettle(const Duration(seconds: 2));
    expect(find.byType(CommunityPage), findsOneWidget);
    when(mockDatabase.query(IssueDraftEntity.tableName, where: " project_id = ? and user_id = ? ", whereArgs: [89335, 9527])).thenAnswer((_) => Future(() => []));
    await tester.tap(find.byIcon(Icons.add_box_outlined));
    await tester.pumpAndSettle();
    expect(find.text("tester"), findsNothing);
  });

  tearDown(() {
    ConnectionProvider().fullReset();
    reset(client);
  });
}

bool insertParamCheck(Object? map) {
  return (map as Map).containsKey("user_id") &&
      map.containsKey("project_id") &&
      map.containsKey("title") &&
      map.containsKey("description") &&
      map.containsKey("selected_labels") &&
      map.containsValue(9527) &&
      map.containsValue(89335) &&
      map.containsValue("example") &&
      map.containsValue("[{\"id\":72395,\"name\":\"type::FAQ\",\"description\":\"问答\",\"text_color\":\"#FFFFFF\",\"color\":\"#FFFFFF\"}]");
}

Map<String, dynamic> faqListResponse = {
  "data": {
    "group": {
      "id": "gid://gitlab/Group/88966",
      "fullName": "旗舰版演示 / 极狐 GitLab App 产品线",
      "name": "极狐 GitLab App 产品线",
      "path": "jihu-gitlab-app",
      "issues": {
        "pageInfo": {"hasNextPage": true, "startCursor": "start_cursor", "endCursor": "end_cursor"},
        "nodes": []
      }
    }
  }
};

Map<String, dynamic> faqTypesResponse = {
  "data": {
    "project": {
      "labels": {
        "nodes": [
          {
            "id": "gid://gitlab/ProjectLabel/72395",
            "title": "type::FAQ",
            "description": "问答",
          },
          {
            "id": "gid://gitlab/ProjectLabel/72059",
            "title": "type::WFH",
            "description": "远程办公",
          },
        ]
      }
    }
  }
};
