import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/l10n/current_locale.dart';
import 'package:jihu_gitlab_app/modules/issues/details/issue_details_page.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_model.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

import '../core/net/http_request_test.mocks.dart';
import '../finder/semantics_label_finder.dart';
import '../finder/text_field_hint_text_finder.dart';
import '../mocker/tester.dart';
import '../modules/issues/details/issue_details_page_test.mocks.dart';
import '../test_data/discussion.dart';
import '../test_data/issue.dart';
import '../test_data/note.dart';

final client = MockHttpClient();

void main() {
  var params = {'projectId': 1, 'issueId': 1, 'issueIid': 1, 'targetId': 1, 'targetIid': 1, 'pathWithNamespace': 'ultimate-plan/jihu-gitlab-app/demo-mr-test'};

  testWidgets('Should update issue', (tester) async {
    ConnectionProvider().reset(Tester.jihuLabUser());
    await tester.pumpWidget(MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (context) => ConnectionProvider()),
          ChangeNotifierProvider(create: (context) => ProjectProvider()),
          ChangeNotifierProvider(create: (context) => LocaleProvider())
        ],
        child: MaterialApp(
          home: Scaffold(body: IssueDetailsPage(arguments: params)),
        )));
    await tester.pumpAndSettle();
    await tester.tap(SemanticsLabelFinder('operate'));
    await tester.pumpAndSettle();
    expect(find.text("Close issue"), findsOneWidget);
    expect(find.text("Edit issue"), findsOneWidget);
    await tester.tap(find.text('Edit issue'));
    await tester.pumpAndSettle();
    expect(find.text('Edit issue'), findsOneWidget);
    await tester.tap(find.byType(BackButton));
    await tester.pumpAndSettle();
    await tester.tap(SemanticsLabelFinder('operate'));
    await tester.pumpAndSettle();
    expect(find.text("Close issue"), findsOneWidget);
    expect(find.text("Edit issue"), findsOneWidget);
    await tester.tap(find.text('Edit issue'));
    await tester.pumpAndSettle();
    await tester.enterText(TextFieldHintTextFinder('Write a issue title'), "new title");
    await tester.pumpAndSettle();
    await tester.tap(find.byType(BackButton));
    await tester.pumpAndSettle();
    expect(find.textContaining('Are you sure'), findsOneWidget);
    await tester.tap(find.text('Cancel'));
    await tester.pumpAndSettle();
    await tester.tap(find.byType(BackButton));
    await tester.pumpAndSettle();
    expect(find.textContaining('Are you sure'), findsOneWidget);
    await tester.tap(find.text('OK'));
    await tester.pumpAndSettle();
    await tester.tap(SemanticsLabelFinder('operate'));
    await tester.pumpAndSettle();
    expect(find.text("Close issue"), findsOneWidget);
    expect(find.text("Edit issue"), findsOneWidget);
    await tester.tap(find.text('Edit issue'));
    await tester.pumpAndSettle();
    expect(find.text('Edit issue'), findsOneWidget);
    await tester.enterText(TextFieldHintTextFinder('Write a issue title'), "new title");
    await tester.pumpAndSettle();
    await tester.tap(find.text('Update'));
    await tester.pumpAndSettle(const Duration(seconds: 2));
  });

  setUp(() async {
    await ConnectionProvider().restore();
    when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 1)))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(issueDetailsGraphQLResponse)));
    when(client.get<List<dynamic>>("/api/v4/projects/1/issues/1/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>(discussionsData)));
    when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 1), useActiveConnection: true))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(issuePostVotesGraphQLResponse)));
    when(client.get("/api/v4/projects/1/issues/1/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => Response.of([])));

    HttpClient.injectInstanceForTesting(client);

    var issueDetailsModel = IssueDetailsModel();
    locator.registerSingleton(issueDetailsModel);
    var provider = MockDiscussionProvider();
    issueDetailsModel.injectDataProviderForTesting(provider);
    when(provider.loadFromLocal()).thenAnswer((_) => Future(() => [Discussion('abc123', noteList, individualNote: false)]));
  });
}
