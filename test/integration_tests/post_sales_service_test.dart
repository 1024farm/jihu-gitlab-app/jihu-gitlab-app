import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/browser_launcher.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/global_keys.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/widgets/home/home_drawer.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/l10n/current_locale.dart';
import 'package:jihu_gitlab_app/modules/home/language_settings_page.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../core/net/http_request_test.mocks.dart';
import '../finder/semantics_label_finder.dart';
import '../mocker/tester.dart';
import '../test_binding_setter.dart';
import '../test_data/all_groups_for_user.dart';
import '../test_data/iterations.dart';
import '../test_data/to_dos.dart';
import '../test_data/user.dart';

void main() {
  final client = MockHttpClient();

  setUp(() async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    ConnectionProvider().reset(Tester.jihuLabUser());

    when(client.getWithHeader<Map<String, dynamic>>("https://jihulab.com/api/v4/user", any)).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
    when(client.getWithHeader<Map<String, dynamic>>("https://example.com/api/v4/user", any)).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
    when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>(todoPendingResponseData)));
  });

  tearDown(() {
    ConnectionProvider().loggedOut(byUser: true);
    ConnectionProvider().clearActive();
  });

  testWidgets('Should display Post-Sales service page with Free User state when user is not paying', (tester) async {
    await setUpCompatibleBinding(tester);

    when(client.get<List<dynamic>>("/api/v4/groups?page=1&per_page=100")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
    HttpClient.injectInstanceForTesting(client);

    await tester.pumpWidget(MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
        child: MaterialApp(
          routes: {PostSalesServicePage.routeName: (context) => const PostSalesServicePage()},
          home: Scaffold(key: GlobalKeys.rootScaffoldKey, drawer: const HomeDrawer(), body: const ToDosPage()),
          localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
        )));

    await tester.pumpAndSettle();
    expect(find.byType(ToDosPage), findsOneWidget);
    await tester.tap(find.byKey(const Key('menu_null')));
    await tester.pumpAndSettle();

    expect(find.byType(Drawer), findsOneWidget);
    expect(SemanticsLabelFinder('Support'), findsOneWidget);

    await tester.tap(find.text('Support'));
    await tester.pumpAndSettle();

    expect(find.byType(PostSalesServicePage), findsOneWidget);
    expect(find.text('Post-Sales service'), findsOneWidget);
    expect(find.byType(BackButton), findsOneWidget);
    expect(find.text('Free User'), findsOneWidget);
    expect(find.text('Support Forum'), findsOneWidget);
    expect(find.image(const AssetImage('assets/images/browser_icon.png')), findsWidgets);

    expect(find.text('current'), findsOneWidget);

    expect(find.text('Paying User'), findsOneWidget);
    expect(find.text('Engineers provide technical support'), findsOneWidget);

    expect(find.text('No post-sales service for the current, please contact:'), findsOneWidget);
    expect(find.text('4000-888-738'), findsOneWidget);

    expect(UrlLauncher.isCalledUrlLauncherSuccess, isFalse);
    await tester.tap(find.text('Support Forum'));
    await tester.pumpAndSettle();
    expect(UrlLauncher.isCalledUrlLauncherSuccess, isTrue);
    UrlLauncher.isCalledUrlLauncherSuccess = false;

    await tester.tap(find.text('Engineers provide technical support'));
    await tester.pumpAndSettle();
    expect(UrlLauncher.isCalledUrlLauncherSuccess, isFalse);

    await tester.tap(find.text('4000-888-738'));
    await tester.pumpAndSettle();
    expect(UrlLauncher.isCalledUrlLauncherSuccess, isTrue);
    UrlLauncher.isCalledUrlLauncherSuccess = false;

    ConnectionProvider().fullReset();

    UrlLauncher.reset();
  });

  testWidgets('Should display Post-Sales service page with Paying User state when user is payed', (tester) async {
    await setUpCompatibleBinding(tester);

    when(client.get<List<dynamic>>("/api/v4/groups?page=1&per_page=100")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>(allGroupsForUser)));
    when(client.get<List<dynamic>>("/api/v4/groups/32052/iterations")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>(iterations)));
    when(client.get<List<dynamic>>("/api/v4/groups/78437/iterations")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
    HttpClient.injectInstanceForTesting(client);

    await tester.pumpWidget(MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
        child: MaterialApp(
          routes: {PostSalesServicePage.routeName: (context) => const PostSalesServicePage()},
          home: Scaffold(key: GlobalKeys.rootScaffoldKey, drawer: const HomeDrawer(), body: const ToDosPage()),
          localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
        )));

    await tester.pumpAndSettle();
    expect(find.byType(ToDosPage), findsOneWidget);
    await tester.tap(find.byKey(const Key('menu_null')));
    await tester.pumpAndSettle();

    await tester.tap(find.text('Support'));
    await tester.pumpAndSettle();

    expect(find.byType(PostSalesServicePage), findsOneWidget);
    expect(find.text('Free User'), findsOneWidget);
    expect(find.text('Support Forum'), findsOneWidget);
    expect(find.image(const AssetImage('assets/images/browser_icon.png')), findsWidgets);

    expect(find.text('current'), findsOneWidget);

    expect(find.text('Paying User'), findsOneWidget);
    expect(find.text('Engineers provide technical support'), findsOneWidget);

    expect(find.text('No post-sales service for the current, please contact:'), findsNothing);
    expect(find.text('4000-888-738'), findsNothing);

    expect(UrlLauncher.isCalledUrlLauncherSuccess, isFalse);
    await tester.tap(find.text('Support Forum'));
    await tester.pumpAndSettle();
    expect(UrlLauncher.isCalledUrlLauncherSuccess, isTrue);
    UrlLauncher.isCalledUrlLauncherSuccess = false;

    expect(UrlLauncher.isCalledUrlLauncherSuccess, isFalse);
    await tester.tap(find.text('Engineers provide technical support'));
    await tester.pumpAndSettle();
    expect(UrlLauncher.isCalledUrlLauncherSuccess, isTrue);
    UrlLauncher.isCalledUrlLauncherSuccess = false;

    ConnectionProvider().fullReset();

    UrlLauncher.reset();
  });

  testWidgets('Should back to To-do list page when user tapped back button on Post-Sales service page, if user come to Post-Sales service page from Todo list page.', (tester) async {
    await setUpCompatibleBinding(tester);

    when(client.get<List<dynamic>>("/api/v4/groups?page=1&per_page=100")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
    HttpClient.injectInstanceForTesting(client);

    await tester.pumpWidget(MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
        child: MaterialApp(
          routes: {PostSalesServicePage.routeName: (context) => const PostSalesServicePage()},
          home: Scaffold(key: GlobalKeys.rootScaffoldKey, drawer: const HomeDrawer(), body: const ToDosPage()),
          localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
        )));

    await tester.pumpAndSettle();
    expect(find.byType(ToDosPage), findsOneWidget);
    await tester.tap(find.byKey(const Key('menu_null')));
    await tester.pumpAndSettle();

    await tester.tap(find.text('Support'));
    await tester.pumpAndSettle();

    expect(find.byType(PostSalesServicePage), findsOneWidget);

    await tester.tap(find.byType(BackButton));
    await tester.pumpAndSettle();
    expect(find.byType(ToDosPage), findsOneWidget);

    ConnectionProvider().fullReset();
  });

  testWidgets('Should display Post-Sales service page for self managed Paying User', (tester) async {
    await setUpCompatibleBinding(tester);

    when(client.get<List<dynamic>>("/api/v4/groups?page=1&per_page=1")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([allGroupsForUser.first])));
    when(client.get<List<dynamic>>("/api/v4/groups/32052/iterations")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>(iterations)));
    HttpClient.injectInstanceForTesting(client);

    await tester.pumpWidget(MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
        child: MaterialApp(
          routes: {PostSalesServicePage.routeName: (context) => const PostSalesServicePage()},
          home: Scaffold(key: GlobalKeys.rootScaffoldKey, drawer: const HomeDrawer(), body: const ToDosPage()),
          localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
        )));

    await tester.pumpAndSettle();
    expect(find.byType(ToDosPage), findsOneWidget);
    await tester.tap(find.byKey(const Key('menu_null')));
    await tester.pumpAndSettle();

    await tester.tap(find.text('Support'));
    await tester.pumpAndSettle();

    expect(find.byType(PostSalesServicePage), findsOneWidget);

    ConnectionProvider().fullReset();
  });

  testWidgets('Should display Post-Sales service page for self managed Paying User', (tester) async {
    await setUpCompatibleBinding(tester);

    when(client.get<List<dynamic>>("/api/v4/groups?page=1&per_page=1")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([allGroupsForUser.first])));
    when(client.get<List<dynamic>>("/api/v4/groups/32052/iterations")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>(iterations)));
    HttpClient.injectInstanceForTesting(client);

    await tester.pumpWidget(MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
        child: MaterialApp(
          routes: {PostSalesServicePage.routeName: (context) => const PostSalesServicePage()},
          home: Scaffold(key: GlobalKeys.rootScaffoldKey, drawer: const HomeDrawer(), body: const ToDosPage()),
          localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
        )));

    await tester.pumpAndSettle();
    expect(find.byType(ToDosPage), findsOneWidget);
    await tester.tap(find.byKey(const Key('menu_null')));
    await tester.pumpAndSettle();

    await tester.tap(find.text('Support'));
    await tester.pumpAndSettle();

    expect(find.byType(PostSalesServicePage), findsOneWidget);

    ConnectionProvider().fullReset();
  });

  testWidgets('Should display settings', (tester) async {
    await setUpCompatibleBinding(tester);

    when(client.get<List<dynamic>>("/api/v4/groups?page=1&per_page=100")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
    HttpClient.injectInstanceForTesting(client);

    ConnectionProvider().initSelfManagedConfigs(host: 'example.com', token: 'access_token', isHttps: true);
    ConnectionProvider().reset(Tester.jihuLabUser());

    await tester.pumpWidget(MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
        child: MaterialApp(
          routes: {PostSalesServicePage.routeName: (context) => const PostSalesServicePage(), LanguageSettingsPage.routeName: (context) => const LanguageSettingsPage()},
          home: Scaffold(key: GlobalKeys.rootScaffoldKey, drawer: const HomeDrawer(), body: const ToDosPage()),
          localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
        )));

    await tester.pumpAndSettle();
    expect(find.byType(ToDosPage), findsOneWidget);
    await tester.tap(find.byKey(const Key('menu_null')));
    await tester.pumpAndSettle();

    expect(find.byType(Drawer), findsOneWidget);
    expect(SemanticsLabelFinder('Support'), findsOneWidget);
    expect(find.text('Language'), findsOneWidget);

    await tester.tap(find.text("Language"));
    await tester.pumpAndSettle();
    expect(find.byType(LanguageSettingsPage), findsOneWidget);
    expect(find.text("Settings"), findsOneWidget);
    expect(find.text("English"), findsOneWidget);
    expect(find.text("Automatic"), findsOneWidget);
    expect(find.text("简体中文"), findsOneWidget);

    await tester.tap(find.text("简体中文"));
    await tester.tap(find.text("Done"));
    await tester.pumpAndSettle();

    ConnectionProvider().fullReset();
  });
}
