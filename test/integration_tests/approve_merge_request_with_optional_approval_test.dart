import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart' as r;
import 'package:jihu_gitlab_app/core/widgets/loading_button.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/l10n/current_locale.dart';
import 'package:jihu_gitlab_app/modules/mr/approval_rule_view.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_request_page.dart';
import 'package:jihu_gitlab_app/modules/mr/models/mr_graphql_request_body.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../core/net/http_request_test.mocks.dart';
import '../mocker/tester.dart';
import '../test_data/merge_request.dart';

void main() {
  late MockHttpClient client;

  setUp(() async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    await ConnectionProvider().restore();
    ConnectionProvider().reset(Tester.jihuLabUser());
    client = MockHttpClient();
    ConnectionProvider().reset(Tester.jihuLabUser());
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets('Should be able to approve the mr with optional approval', (tester) async {
    when(client.get<Map<String, dynamic>>("/api/v4/projects/72936/merge_requests/18/approvals")).thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>(approvalResponse)));
    when(client.post("/api/v4/projects/72936/merge_requests/18/approve", {})).thenAnswer((_) => Future(() => r.Response.of({})));
    when(client.get('/api/v4/projects/72936')).thenAnswer((_) => Future(() => r.Response.of<Map>({"path_with_namespace": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app"})));
    when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 18)))
        .thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>(mrGraphQLResponse)));
    when(client.post('/api/graphql', getPaidMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 18)))
        .thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>(paidMrGraphQLResponse)));
    when(client.post('/api/graphql', queryJobsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 18, ''))).thenAnswer((_) => Future(() => r.Response.of(jobsGraphQLResponse)));
    when(client.post("/api/graphql", queryCommitsGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 18, ''))).thenAnswer((_) => Future(() => r.Response.of(commitsGraphQLResponse)));
    when(client.get<List<Map<String, dynamic>>>("/api/v4/projects/72936/merge_requests/18/diffs?page=1&per_page=20")).thenAnswer((_) => Future(() => r.Response.of<List<Map<String, dynamic>>>([])));

    var parameters = {'projectId': 72936, 'projectName': "demo", 'mergeRequestIid': 18, 'fullPath': 'ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 'test': true};
    await tester.pumpWidget(MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => ConnectionProvider()),
        ChangeNotifierProvider(create: (context) => LocaleProvider()),
      ],
      child: MaterialApp(
        onGenerateRoute: onGenerateRoute,
        home: Scaffold(
          body: MergeRequestPage(arguments: parameters),
        ),
        localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
      ),
    ));

    await tester.pumpAndSettle();
    expect(find.text("Approval is optional"), findsOneWidget);
    expect(find.byType(ApprovalRuleView), findsNWidgets(3));
    expect(find.text("0 approvals from Coverage-Check"), findsOneWidget);
    expect(find.text("0 approvals from License-Check"), findsOneWidget);

    final listFinder = find.byType(Scrollable).last;
    await tester.scrollUntilVisible(find.widgetWithText(LoadingButton, "Approve"), 500.0, scrollable: listFinder);
    expect(find.widgetWithText(LoadingButton, "Approve"), findsOneWidget);
    await tester.tap(find.widgetWithText(LoadingButton, "Approve"));
    await tester.pumpAndSettle();
    verify(client.post("/api/v4/projects/72936/merge_requests/18/approve", {})).called(1);
  });

  tearDown(() {
    ConnectionProvider().fullReset();
  });
}

Map<String, dynamic> approvalResponse = {
  "id": 227910,
  "iid": 18,
  "project_id": 72936,
  "title": "Resolve \"Test optional approvals\"",
  "description": "Closes #47",
  "state": "opened",
  "created_at": "2023-02-08T14:24:54.580+08:00",
  "updated_at": "2023-02-08T15:37:54.653+08:00",
  "merge_status": "can_be_merged",
  "approved": true,
  "approvals_required": 0,
  "approvals_left": 0,
  "require_password_to_approve": false,
  "approved_by": [],
  "suggested_approvers": [
    {
      "id": 1795,
      "username": "sinkcup",
      "name": "Zhou YANG",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/1795/avatar.png",
      "web_url": "https://jihulab.com/sinkcup"
    },
    {
      "id": 29355,
      "username": "perity",
      "name": "miaolu",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29355/avatar.png",
      "web_url": "https://jihulab.com/perity"
    },
    {
      "id": 29064,
      "username": "NeilWang",
      "name": "Neil Wang",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29064/avatar.png",
      "web_url": "https://jihulab.com/NeilWang"
    },
    {
      "id": 30192,
      "username": "raymond-liao",
      "name": "Raymond Liao",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/30192/avatar.png",
      "web_url": "https://jihulab.com/raymond-liao"
    },
    {"id": 29473, "username": "zombie", "name": "鑫 范", "state": "active", "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29473/avatar.png", "web_url": "https://jihulab.com/zombie"},
    {
      "id": 23836,
      "username": "jojo0",
      "name": "yajie xue",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/23836/avatar.png",
      "web_url": "https://jihulab.com/jojo0"
    },
    {
      "id": 23837,
      "username": "wanyouzhu",
      "name": "万友 朱",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/23837/avatar.png",
      "web_url": "https://jihulab.com/wanyouzhu"
    }
  ],
  "approvers": [],
  "approver_groups": [],
  "user_has_approved": false,
  "user_can_approve": true,
  "approval_rules_left": [],
  "has_approval_rules": true,
  "merge_request_approvers_available": true,
  "multiple_approval_rules_available": true,
  "invalid_approvers_rules": []
};

Map<String, dynamic> mrGraphQLResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "name": "极狐 GitLab APP 代码",
      "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码",
      "path": "jihu-gitlab-app",
      "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
      "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
      "allowMergeOnSkippedPipeline": false,
      "onlyAllowMergeIfAllStatusChecksPassed": false,
      "onlyAllowMergeIfAllDiscussionsAreResolved": true,
      "onlyAllowMergeIfPipelineSucceeds": true,
      "removeSourceBranchAfterMerge": true,
      "mergeRequest": {
        "id": "gid://gitlab/MergeRequest/230540",
        "iid": "84",
        "state": "opened",
        "title": "Resolve \"Feature: 用户更新议题 label\"",
        "description": "Closes #47",
        "draft": false,
        "createdAt": "2023-02-13T15:49:42+08:00",
        "rebaseInProgress": false,
        "allowCollaboration": false,
        "approved": false,
        "approvalsLeft": 2,
        "approvalsRequired": 3,
        "approvalState": {
          "approvalRulesOverwritten": true,
          "rules": [
            {"id": "gid://gitlab/ApprovalMergeRequestRule/74131", "name": "All Members", "type": "ANY_APPROVER", "approvalsRequired": 1, "eligibleApprovers": []},
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/74132",
              "name": "License-Check",
              "type": "REGULAR",
              "approvalsRequired": 0,
              "eligibleApprovers": [
                {
                  "id": "gid://gitlab/User/23836",
                  "username": "jojo0",
                  "name": "yajie xue",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png",
                  "webUrl": "https://jihulab.com/jojo0",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/23837",
                  "username": "wanyouzhu",
                  "name": "万友 朱",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                  "webUrl": "https://jihulab.com/wanyouzhu",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/1795",
                  "username": "sinkcup",
                  "name": "Zhou YANG",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
                  "webUrl": "https://jihulab.com/sinkcup",
                  "publicEmail": "zhouyang@jihulab.com",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29064",
                  "username": "NeilWang",
                  "name": "Neil Wang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png",
                  "webUrl": "https://jihulab.com/NeilWang",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29758",
                  "username": "zhangling",
                  "name": "ling zhang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png",
                  "webUrl": "https://jihulab.com/zhangling",
                  "publicEmail": "",
                  "commitEmail": null
                }
              ]
            },
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/74134",
              "name": "Coverage-Check",
              "type": "REPORT_APPROVER",
              "approvalsRequired": 0,
              "eligibleApprovers": [
                {
                  "id": "gid://gitlab/User/23837",
                  "username": "wanyouzhu",
                  "name": "万友 朱",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                  "webUrl": "https://jihulab.com/wanyouzhu",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/1795",
                  "username": "sinkcup",
                  "name": "Zhou YANG",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
                  "webUrl": "https://jihulab.com/sinkcup",
                  "publicEmail": "zhouyang@jihulab.com",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29064",
                  "username": "NeilWang",
                  "name": "Neil Wang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png",
                  "webUrl": "https://jihulab.com/NeilWang",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29758",
                  "username": "zhangling",
                  "name": "ling zhang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png",
                  "webUrl": "https://jihulab.com/zhangling",
                  "publicEmail": "",
                  "commitEmail": null
                }
              ]
            }
          ]
        },
        "approvedBy": {"nodes": []},
        "commitCount": 2,
        "commits": {
          "nodes": [
            {
              "id": "gid://gitlab/CommitPresenter/0334c32c55457cf7269db25c812284cfd4afe24f",
              "shortId": "0334c32c",
              "sha": "0334c32c55457cf7269db25c812284cfd4afe24f",
              "title": "chore: #497 Merge code from main.",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/1494c07a894a8f38e53b2fe9f683f09efff955db",
              "shortId": "1494c07a",
              "sha": "1494c07a894a8f38e53b2fe9f683f09efff955db",
              "title": "refactor: #497 Remove no needs code and rename",
              "description": ""
            }
          ]
        },
        "assignees": {
          "nodes": [
            {"id": "gid://gitlab/User/30192", "name": "Raymond Liao"}
          ]
        },
        "author": {"id": "gid://gitlab/User/30192", "name": "ling zhang", "username": "raymond-liao", "avatarUrl": null},
        "autoMergeEnabled": false,
        "autoMergeStrategy": null,
        "availableAutoMergeStrategies": [],
        "conflicts": false,
        "mergeError": null,
        "mergeOngoing": false,
        "detailedMergeStatus": "NOT_APPROVED",
        "mergeStatusEnum": "CANNOT_BE_MERGED",
        "mergeUser": null,
        "mergeWhenPipelineSucceeds": false,
        "mergeable": false,
        "mergeableDiscussionsState": true,
        "mergedAt": null,
        "rebaseCommitSha": null,
        "shouldBeRebased": true,
        "shouldRemoveSourceBranch": null,
        "sourceBranch": "499-feature-kai-fa-ren-yuan-cha-kan-merge-blocked",
        "sourceBranchExists": true,
        "sourceBranchProtected": false,
        "targetBranch": "main",
        "targetBranchExists": true,
        "squash": true,
        "squashOnMerge": true,
        "defaultMergeCommitMessage":
            "Merge branch '461-feature-yong-hu-geng-xin-yi-ti-label' into 'main'\n\nResolve \"Feature: 用户更新议题 label\"\n\nSee merge request ultimate-plan/jihu-gitlab-app/jihu-gitlab-app!360",
        "defaultSquashCommitMessage": "Resolve \"Feature: 用户更新议题 label\"",
        "headPipeline": {
          "id": "gid://gitlab/Ci::Pipeline/943141",
          "iid": "1861",
          "active": false,
          "cancelable": false,
          "complete": true,
          "coverage": 94.2,
          "status": "SUCCESS",
          "ref": "499-feature-kai-fa-ren-yuan-cha-kan-merge-blocked",
          "detailedStatus": {"icon": "status_warning", "label": "passed with warnings"},
          "codeQualityReportSummary": {"count": 3},
          "codeQualityReports": {
            "edges": [
              {
                "cursor": "MQ",
                "node": {
                  "description": "The method doesn't override an inherited method",
                  "fingerprint": "1d3e96a04c34977ae6bdd4ea70070095",
                  "line": 84,
                  "path": "test/modules/ai/ai_page_test.mocks.dart",
                  "severity": "INFO",
                  "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/3c01fc4a9e2a87c1d7660f0ab3180917ee7992b0/test/modules/ai/ai_page_test.mocks.dart#L84"
                }
              },
              {
                "cursor": "Mg",
                "node": {
                  "description": "The method doesn't override an inherited method",
                  "fingerprint": "5b52cf3a23677a78008b449059c7e94a",
                  "line": 94,
                  "path": "test/modules/ai/ai_page_test.mocks.dart",
                  "severity": "INFO",
                  "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/3c01fc4a9e2a87c1d7660f0ab3180917ee7992b0/test/modules/ai/ai_page_test.mocks.dart#L94"
                }
              },
              {
                "cursor": "Mw",
                "node": {
                  "description": "Avoid `print` calls in production code",
                  "fingerprint": "baf77a8fff3c7e68e1e0d8b7f1c6999f",
                  "line": 8,
                  "path": "test/modules/mr/pipeline_detailed_status_test.dart",
                  "severity": "INFO",
                  "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/3c01fc4a9e2a87c1d7660f0ab3180917ee7992b0/test/modules/mr/pipeline_detailed_status_test.dart#L8"
                }
              }
            ]
          },
          "jobs": {
            "nodes": [
              {
                "id": "gid://gitlab/Ci::Build/6378824",
                "name": "build_apk",
                "active": false,
                "status": "FAILED",
                "allowFailure": true,
                "duration": 208,
                "startedAt": "2023-02-14T15:34:33+08:00",
                "finishedAt": "2023-02-14T15:38:02+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2118204",
                  "name": "build",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6378824", "name": "build_apk", "status": "FAILED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6378822", "name": "code_quality", "status": "SUCCESS", "allowFailure": false},
                      {"id": "gid://gitlab/Ci::Build/6378823", "name": "unit_test", "status": "SUCCESS", "allowFailure": false}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/6378823",
                "name": "unit_test",
                "active": false,
                "status": "SUCCESS",
                "allowFailure": false,
                "duration": 558,
                "startedAt": "2023-02-14T15:34:33+08:00",
                "finishedAt": "2023-02-14T15:43:52+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2118204",
                  "name": "build",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6378824", "name": "build_apk", "status": "FAILED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6378822", "name": "code_quality", "status": "SUCCESS", "allowFailure": false},
                      {"id": "gid://gitlab/Ci::Build/6378823", "name": "unit_test", "status": "SUCCESS", "allowFailure": false}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/6378822",
                "name": "code_quality",
                "active": false,
                "status": "SUCCESS",
                "allowFailure": false,
                "duration": 263,
                "startedAt": "2023-02-14T15:34:33+08:00",
                "finishedAt": "2023-02-14T15:38:57+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2118204",
                  "name": "build",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6378824", "name": "build_apk", "status": "FAILED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6378822", "name": "code_quality", "status": "SUCCESS", "allowFailure": false},
                      {"id": "gid://gitlab/Ci::Build/6378823", "name": "unit_test", "status": "SUCCESS", "allowFailure": false}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/6378821",
                "name": "set_mirror",
                "active": false,
                "status": "SUCCESS",
                "allowFailure": true,
                "duration": 14,
                "startedAt": "2023-02-14T15:34:17+08:00",
                "finishedAt": "2023-02-14T15:34:32+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2118203",
                  "name": ".pre",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6378820", "name": "check_internet", "status": "FAILED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6378821", "name": "set_mirror", "status": "SUCCESS", "allowFailure": true}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/6378820",
                "name": "check_internet",
                "active": false,
                "status": "FAILED",
                "allowFailure": true,
                "duration": 17,
                "startedAt": "2023-02-14T15:33:59+08:00",
                "finishedAt": "2023-02-14T15:34:17+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2118203",
                  "name": ".pre",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6378820", "name": "check_internet", "status": "FAILED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6378821", "name": "set_mirror", "status": "SUCCESS", "allowFailure": true}
                    ]
                  }
                }
              }
            ]
          },
          "warnings": true,
          "warningMessages": [],
          "createdAt": "2023-02-13T15:49:25+08:00",
          "finishedAt": "2023-02-13T15:59:15+08:00",
          "committedAt": null,
          "startedAt": "2023-02-13T15:49:26+08:00"
        },
        "userPermissions": {"adminMergeRequest": true, "canMerge": true, "pushToSourceBranch": true, "readMergeRequest": true, "removeSourceBranch": true}
      }
    }
  }
};

Map<String, dynamic> paidMrGraphQLResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "onlyAllowMergeIfAllStatusChecksPassed": false,
      "mergeRequest": {
        "id": "gid://gitlab/MergeRequest/230540",
        "iid": "84",
        "approved": false,
        "approvalsLeft": 2,
        "approvalsRequired": 3,
        "approvalState": {
          "approvalRulesOverwritten": true,
          "rules": [
            {"id": "gid://gitlab/ApprovalMergeRequestRule/74131", "name": "All Members", "type": "ANY_APPROVER", "approvalsRequired": 1, "eligibleApprovers": []},
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/74132",
              "name": "License-Check",
              "type": "REGULAR",
              "approvalsRequired": 0,
              "eligibleApprovers": [
                {
                  "id": "gid://gitlab/User/23836",
                  "username": "jojo0",
                  "name": "yajie xue",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png",
                  "webUrl": "https://jihulab.com/jojo0",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/23837",
                  "username": "wanyouzhu",
                  "name": "万友 朱",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                  "webUrl": "https://jihulab.com/wanyouzhu",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/1795",
                  "username": "sinkcup",
                  "name": "Zhou YANG",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
                  "webUrl": "https://jihulab.com/sinkcup",
                  "publicEmail": "zhouyang@jihulab.com",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29064",
                  "username": "NeilWang",
                  "name": "Neil Wang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png",
                  "webUrl": "https://jihulab.com/NeilWang",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29758",
                  "username": "zhangling",
                  "name": "ling zhang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png",
                  "webUrl": "https://jihulab.com/zhangling",
                  "publicEmail": "",
                  "commitEmail": null
                }
              ]
            },
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/74134",
              "name": "Coverage-Check",
              "type": "REPORT_APPROVER",
              "approvalsRequired": 0,
              "eligibleApprovers": [
                {
                  "id": "gid://gitlab/User/23837",
                  "username": "wanyouzhu",
                  "name": "万友 朱",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                  "webUrl": "https://jihulab.com/wanyouzhu",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/1795",
                  "username": "sinkcup",
                  "name": "Zhou YANG",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
                  "webUrl": "https://jihulab.com/sinkcup",
                  "publicEmail": "zhouyang@jihulab.com",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29064",
                  "username": "NeilWang",
                  "name": "Neil Wang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png",
                  "webUrl": "https://jihulab.com/NeilWang",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29758",
                  "username": "zhangling",
                  "name": "ling zhang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png",
                  "webUrl": "https://jihulab.com/zhangling",
                  "publicEmail": "",
                  "commitEmail": null
                }
              ]
            }
          ]
        },
        "detailedMergeStatus": "NOT_APPROVED",
        "headPipeline": {
          "id": "gid://gitlab/Ci::Pipeline/943141",
          "iid": "1861",
          "codeQualityReportSummary": {"count": 3},
          "codeQualityReports": {
            "edges": [
              {
                "cursor": "MQ",
                "node": {
                  "description": "The method doesn't override an inherited method",
                  "fingerprint": "1d3e96a04c34977ae6bdd4ea70070095",
                  "line": 84,
                  "path": "test/modules/ai/ai_page_test.mocks.dart",
                  "severity": "INFO",
                  "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/3c01fc4a9e2a87c1d7660f0ab3180917ee7992b0/test/modules/ai/ai_page_test.mocks.dart#L84"
                }
              },
              {
                "cursor": "Mg",
                "node": {
                  "description": "The method doesn't override an inherited method",
                  "fingerprint": "5b52cf3a23677a78008b449059c7e94a",
                  "line": 94,
                  "path": "test/modules/ai/ai_page_test.mocks.dart",
                  "severity": "INFO",
                  "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/3c01fc4a9e2a87c1d7660f0ab3180917ee7992b0/test/modules/ai/ai_page_test.mocks.dart#L94"
                }
              },
              {
                "cursor": "Mw",
                "node": {
                  "description": "Avoid `print` calls in production code",
                  "fingerprint": "baf77a8fff3c7e68e1e0d8b7f1c6999f",
                  "line": 8,
                  "path": "test/modules/mr/pipeline_detailed_status_test.dart",
                  "severity": "INFO",
                  "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/3c01fc4a9e2a87c1d7660f0ab3180917ee7992b0/test/modules/mr/pipeline_detailed_status_test.dart#L8"
                }
              }
            ]
          }
        },
      }
    }
  }
};

Map<String, dynamic> commitsGraphQLResponse = {
  "data": {
    "project": {
      "mergeRequest": {
        "commits": {
          "pageInfo": {"hasNextPage": false, "endCursor": "MQ"},
          "nodes": [
            {
              "id": "gid://gitlab/CommitPresenter/0334c32c55457cf7269db25c812284cfd4afe24f",
              "shortId": "0334c32c",
              "sha": "0334c32c55457cf7269db25c812284cfd4afe24f",
              "title": "chore: #497 Merge code from main.",
              "description": "",
              "author": {"name": "name1 in page 1", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png"}
            },
            {
              "id": "gid://gitlab/CommitPresenter/1494c07a894a8f38e53b2fe9f683f09efff955db",
              "shortId": "1494c07a",
              "sha": "1494c07a894a8f38e53b2fe9f683f09efff955db",
              "title": "refactor: #497 Remove no needs code and rename",
              "description": "",
              "author": {"name": "name2 in page 1", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png"}
            }
          ]
        }
      }
    }
  }
};
