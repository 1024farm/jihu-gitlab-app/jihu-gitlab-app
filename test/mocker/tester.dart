import 'package:jihu_gitlab_app/core/connection_provider/connection.dart';
import 'package:jihu_gitlab_app/core/domain/token.dart';

class Tester {
  static Token token() {
    return Token('access_token', 'token', 7200, 'Bearer', 1669793770);
  }

  static Connection jihuLabUser() {
    var userInfo = User.fromJson({
      "id": 9527,
      "username": "jihulab_tester",
      "name": "tester",
      "state": "active",
      "two_factor_enabled": true,
      "external": false,
      "private_profile": true,
    });
    var user = Connection(userInfo, BaseUrl('https://jihulab.com'), active: true);
    user.token = token();
    return user;
  }

  static Connection gitLabUser() {
    var userInfo = User.fromJson({
      "id": 9528,
      "username": "gitlab_tester",
      "name": "tester",
      "state": "active",
      "two_factor_enabled": true,
      "external": false,
      "private_profile": true,
    });
    var user = Connection(userInfo, BaseUrl('https://gitlab.com'), active: true);
    user.token = token();
    return user;
  }

  static Connection selfManagedUser() {
    var userInfo = User.fromJson({
      "id": 9529,
      "username": "tester",
      "name": "tester",
      "state": "active",
      "two_factor_enabled": true,
      "external": false,
      "private_profile": true,
    });
    var user = Connection(userInfo, BaseUrl('https://gitlab.com'), active: true);
    user.personalAccessToken = 'access_token';
    user.isSelfManaged = true;
    return user;
  }
}
