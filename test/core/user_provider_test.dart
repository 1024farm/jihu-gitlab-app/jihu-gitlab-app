import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../mocker/tester.dart';
import '../test_data/user.dart';
import 'net/http_request_test.mocks.dart';

final client = MockHttpClient();

void main() {
  setUp(() async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
  });

  test('Should User Provider saved token info in Local Storage and can restore back', () async {
    expect(ConnectionProvider.canRefreshToken, isFalse);

    ConnectionProvider().reset(Tester.jihuLabUser());
    await ConnectionProvider().restore();

    expect(ConnectionProvider.accessToken, Tester.token().accessToken);
    expect(ConnectionProvider.refreshToken, Tester.token().refreshToken);
    expect(ConnectionProvider.canRefreshToken, isTrue);
    expect(ConnectionProvider.shouldRefreshToken, isTrue);
    expect(ConnectionProvider().communityConnection.authorized, isTrue);
    await ConnectionProvider().clearActive();
    await ConnectionProvider().restore();
    expect(ConnectionProvider().communityConnection.authorized, isFalse);
  });

  test('Should not get jihulab.com connection from connections when there has no jihu connection', () async {
    ConnectionProvider connectionProvider = ConnectionProvider();
    expect(connectionProvider.communityConnection.authorized, false);
  });

  test('Should not get jihulab.com connection from connections', () async {
    ConnectionProvider().reset(Tester.jihuLabUser());
    ConnectionProvider connectionProvider = ConnectionProvider();
    expect(connectionProvider.communityConnection.userInfo.id, 9527);
  });

  test('Should create User Provider object with singleton instance', () {
    ConnectionProvider connectionProvider = ConnectionProvider();
    expect(connectionProvider, ConnectionProvider());
  });

  test('Should User Provider fetch user info be succeed when response correct user info', () async {
    when(client.getWithHeader<Map<String, dynamic>>("https://jihulab.com/api/v4/user", any)).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
    HttpClient.injectInstanceForTesting(client);
    ConnectionProvider().reset(Tester.jihuLabUser());
    var result = await ConnectionProvider().refresh();
    expect(result["success"], true);
    expect(ConnectionProvider.connection, isNotNull);

    var resp = ConnectionProvider.connection!;
    expect(resp.userInfo.id, resp.userInfo.hashCode);
    expect(resp.userInfo.id, userInfo['id']);
    expect(resp.userInfo.username, userInfo['username']);
    expect(resp.userInfo.name, userInfo['name']);
    expect(resp.userInfo.state, userInfo['state']);
    expect(resp.userInfo.avatarUrl, userInfo['avatar_url'] ?? '');
    expect(resp.userInfo.webUrl, userInfo['web_url'] ?? '');
    expect(resp.userInfo.publicEmail, userInfo['public_email'] ?? '');

    await ConnectionProvider().clearActive();
    expect(ConnectionProvider.connection, isNull);
  });

  test('Should User Provider saved user info in Local Storage after fetched succeed.', () async {
    when(client.getWithHeader<Map<String, dynamic>>("https://jihulab.com/api/v4/user", any)).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
    HttpClient.injectInstanceForTesting(client);
    ConnectionProvider().reset(Tester.jihuLabUser());
    var result = await ConnectionProvider().refresh();
    expect(result["success"], true);
    List<String> userJson = await LocalStorage.get<List<String>>("app-user", []);
    expect(userJson.length, 1);
    var map = jsonDecode(userJson[0]);
    var connection = Connection.fromJson(map);
    expect(connection.userInfo.id, userInfo['id']);

    await ConnectionProvider().restore();
    expect(ConnectionProvider.connection, isNotNull);
    expect(connection.userInfo, ConnectionProvider.connection!.userInfo);

    await ConnectionProvider().clearActive();
    expect(LocalStorage.get<List<String>>("app-user-info", []), []);
  });

  test('Should get isLoggedOutByUser state by User Provider', () async {
    await ConnectionProvider().restore();
    await ConnectionProvider().clearActive();
    expect(ConnectionProvider.isLoggedOutByUser, isFalse);
  });
}
