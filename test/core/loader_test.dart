import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/loader.dart';

typedef LoaderProgressCallBack = void Function(BuildContext context);

void main() {
  final TestWidgetsFlutterBinding binding = TestWidgetsFlutterBinding.ensureInitialized();

  testWidgets('Should display loader with default message', (tester) async {
    await testLoaderProgress(binding, tester, loadingTitle: 'Loading ...', onButtonTap: (context) => Loader.showProgress(context), onHideTap: (context) => Loader.hideProgress(context));
  });

  testWidgets('Should display loader with message "test" when given message "test"', (tester) async {
    String testTitle = 'test';
    await testLoaderProgress(binding, tester, loadingTitle: testTitle, onButtonTap: (context) => Loader.showProgress(context, text: testTitle), onHideTap: (context) => Loader.hideProgress(context));
  });

  testWidgets('Should display loader with message "test" when given message "test" in Future', (tester) async {
    String testTitle = 'test';
    await testLoaderProgress(binding, tester,
        loadingTitle: testTitle,
        onButtonTap: (context) => Future.delayed(const Duration(seconds: 1), () => Loader.showFutureProgress(context, text: testTitle)),
        onHideTap: (context) => Loader.hideFutureProgress(context));
  });
}

Future<void> testLoaderProgress(TestWidgetsFlutterBinding binding, WidgetTester tester,
    {required String loadingTitle, required LoaderProgressCallBack onButtonTap, required LoaderProgressCallBack onHideTap}) async {
  const testShowButtonTitle = 'TestShowButton';
  await binding.setSurfaceSize(const Size(430, 932));
  late GlobalKey currentRouteKey;

  await tester.pumpWidget(MaterialApp(
    home: Scaffold(
      key: currentRouteKey = GlobalKey(),
      body: Builder(
        builder: (BuildContext context) {
          return Center(
            child: InkWell(
              onTap: () {
                onButtonTap(context);
              },
              child: const Text(testShowButtonTitle),
            ),
          );
        },
      ),
    ),
  ));

  await tester.tap(find.text(testShowButtonTitle));
  for (int i = 0; i < 5; i++) {
    await tester.pump(const Duration(seconds: 1));
  }
  expect(find.text(loadingTitle), findsOneWidget);
  onHideTap(currentRouteKey.currentContext!);
  await tester.pumpAndSettle();
  expect(find.text(loadingTitle), findsNothing);
}
