import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/package_device_info.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  const channel = MethodChannel('dev.fluttercommunity.plus/package_info');
  final log = <MethodCall>[];

  channel.setMockMethodCallHandler((MethodCall methodCall) async {
    log.add(methodCall);
    switch (methodCall.method) {
      case 'getAll':
        return <String, dynamic>{
          'appName': 'package_info_example',
          'buildNumber': '1',
          'packageName': 'io.flutter.plugins.packageinfoexample',
          'version': '1.0',
          'installerStore': null,
        };
      default:
        assert(false);
        return null;
    }
  });

  tearDown(() {
    log.clear();
  });

  test('Should get version and build value after PackageDeviceInfo load info', () async {
    expect(PackageDeviceInfo.versionNumber, isNull);
    expect(PackageDeviceInfo.buildNumber, isNull);

    await PackageDeviceInfo.loadInfo();

    expect(PackageDeviceInfo.versionNumber, isNotNull);
    expect(PackageDeviceInfo.buildNumber, isNotNull);
  });
}
