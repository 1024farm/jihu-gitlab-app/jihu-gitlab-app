import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/settings/settings.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  setUp(() async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
  });

  test('Should get settings', () {
    expect(Settings.instance().runtimeType, Settings);
  });

  test('Should get settings when has not setting', () {
    expect(Settings.instance().precisionTime().isEnabled(), false);
  });

  test('Should enable one setting', () {
    Settings.instance().precisionTime().enable();
    expect(Settings.instance().precisionTime().isEnabled(), true);
  });

  test('Should disable one setting', () {
    Settings.instance().precisionTime().disable();
    expect(Settings.instance().precisionTime().isEnabled(), false);
  });

  test('Should change setting', () {
    Settings.instance().precisionTime().enable();
    Settings.instance().precisionTime().change();
    expect(Settings.instance().precisionTime().isEnabled(), false);
    Settings.instance().precisionTime().change();
    expect(Settings.instance().precisionTime().isEnabled(), true);
  });

  test('Should change when time is not exist', () {
    LocalStorage.remove('precisionTime');
    Settings.instance().precisionTime().change();
    expect(Settings.instance().precisionTime().isEnabled(), true);
  });
}
