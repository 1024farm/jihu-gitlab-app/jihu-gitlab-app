import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/domain/assignee/assignee.dart';
import 'package:jihu_gitlab_app/core/domain/avatar_url.dart';

void main() {
  test('Should construct empty assignee', () async {
    var assignee = Assignee.empty();
    expect(assignee.id, 0);
    expect(assignee.name, '');
    expect(assignee.username, '');
    expect(assignee.avatarUrl, '');
  });

  var params = [
    {"source": "/a.png", "expect": "https://example.com/a.png"},
    {"source": "https://example2.com/a.png", "expect": "https://example2.com/a.png"}
  ];
  for (var param in params) {
    test('Should get ${param['expect']} from ${param['source']} assignee', () async {
      var assignee = Assignee.empty();
      assignee.avatarUrl = param['source']!;
      expect(AvatarUrl(assignee.avatarUrl).realUrl("https://example.com"), param['expect']!);
    });
  }
}
