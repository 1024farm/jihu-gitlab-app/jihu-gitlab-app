import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/domain/action_name.dart';

void main() {
  test('Should create an action_name with expect value', () async {
    var parameters = {
      "": "",
      "assigned": "assigned you",
      "mentioned": "mentioned you on",
      "build_failed": "the pipeline failed in",
      "marked": "marked",
      "approval_required": "set you as an approver for",
      "unmergeable": "unmergeable",
      "directly_addressed": "mentioned you on",
      "merge_train_removed": "merge_train_removed"
    };
    parameters.forEach((key, value) {
      expect(value, ActionName.init(key).actionName());
    });
  });
}
