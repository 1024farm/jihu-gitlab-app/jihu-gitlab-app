import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/domain/system_locale.dart';

void main() {
  test('Should find system locale', () {
    SystemLocale().set("CN");
    expect(SystemLocale().code, 'CN');
  });

  var params = [
    {
      "localeCode": "CN",
      "isOnePartOfChina": true,
    },
    {
      "localeCode": "HK",
      "isOnePartOfChina": true,
    },
    {
      "localeCode": "MO",
      "isOnePartOfChina": true,
    },
    {
      "localeCode": "US",
      "isOnePartOfChina": false,
    }
  ];
  for (var param in params) {
    var localCode = param['localeCode'] as String;
    var isOnePartOfChina = param['isOnePartOfChina'] as bool;
    test('Should parse locale code: "$localCode" as ${isOnePartOfChina ? 'one' : 'not one'} part of china ', () {
      SystemLocale().set(localCode);
      expect(SystemLocale().isOnePartOfChina, isOnePartOfChina);
    });
  }
}
