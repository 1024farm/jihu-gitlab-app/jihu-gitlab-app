import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/domain/assignee/assignee_entity.dart';

import '../../test_data/assignee.dart';

void main() {
  test("Should construct from map", () {
    var assigneeEntity = AssigneeEntity.create(assigneeData, 2);
    expect(assigneeEntity.id, null);
    expect(assigneeEntity.assigneeId, 1);
    expect(assigneeEntity.name, 'name');
    expect(assigneeEntity.username, 'username');
    expect(assigneeEntity.avatarUrl, 'avatar_url');
    expect(assigneeEntity.version, 2);
  });

  test("Should construct success from group comment map", () {
    var assigneeEntity = AssigneeEntity.create({"id": null, "username": 'group_88966_bot', "name": "****", "state": "active", "avatar_url": null, "web_url": null}, 2);
    expect(assigneeEntity.id, null);
    expect(assigneeEntity.assigneeId, 0);
    expect(assigneeEntity.name, 'group_88966_bot');
    expect(assigneeEntity.username, 'group_88966_bot');
    expect(assigneeEntity.avatarUrl, '');
    expect(assigneeEntity.version, 2);
  });

  test("Should construct success from map with null values", () {
    var assigneeEntity = AssigneeEntity.create(assigneeDataWithNullValues, 2);
    expect(assigneeEntity.id, null);
    expect(assigneeEntity.assigneeId, 0);
    expect(assigneeEntity.name, '');
    expect(assigneeEntity.username, '');
    expect(assigneeEntity.avatarUrl, '');
    expect(assigneeEntity.version, 2);
  });

  test("Should restore from repository", () {
    var assigneeEntity = AssigneeEntity.restore(assigneeDbData);
    expect(assigneeEntity.id, assigneeDbData['id']);
    expect(assigneeEntity.assigneeId, assigneeDbData['assignee_id']);
    expect(assigneeEntity.name, assigneeDbData['name']);
    expect(assigneeEntity.username, assigneeDbData['username']);
    expect(assigneeEntity.avatarUrl, assigneeDbData['avatar_url']);
    expect(assigneeEntity.version, 4);
    expect(assigneeEntity == AssigneeEntity.restore(assigneeDbData), isTrue);
  });

  test("Should convert to map", () {
    var map = AssigneeEntity.restore(assigneeDbData).toMap();
    expect(map, assigneeDbData);
  });

  test("Should get discussion entity hashcode", () {
    expect(AssigneeEntity.restore(assigneeDbData).hashCode, greaterThan(0));
  });

  test("Should generate the sql of create table", () {
    var expectSql = """ 
    create table assignees(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        assignee_id INTEGER NOT NULL,
        name TEXT ,
        username TEXT ,
        avatar_url TEXT,
        version INTEGER
    );
    """;
    expect(AssigneeEntity.createTableSql(), expectSql);
  });

  test("Should update", () {
    Map<String, dynamic> dbData2 = {'id': 1, 'assignee_id': 29355, 'name': 'name', 'username': 'username', 'avatar_url': 'avatar_url', 'version': 5};
    var entity1 = AssigneeEntity.restore(assigneeDbData);
    var entity2 = AssigneeEntity.restore(dbData2);
    entity1.update(entity2);
    expect(entity1.id, assigneeDbData['id']);
    expect(entity1.assigneeId, assigneeDbData['assignee_id']);
    expect(entity1.name, dbData2['name']);
    expect(entity1.username, dbData2['username']);
    expect(entity1.avatarUrl, dbData2['avatar_url']);
    expect(entity1.version, dbData2['version']);
    expect(entity1 == entity2, isTrue);
  });
}
