import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/description_content.dart';

void main() {
  test("Include uploads no image", () {
    String content = "![创建议题页面样式问题](/uploads/ab1ce029b7a1f057e80105d04ef7e0ae/创建议题页面样式问题.png)";
    var descriptionContent = DescriptionContent.init(content, "123");
    expect(descriptionContent.value, "![创建议题页面样式问题](/123/uploads/ab1ce029b7a1f057e80105d04ef7e0ae/创建议题页面样式问题.png)");
  });

  test("Include uploads has image", () {
    String content = "![image](/uploads/ab1ce029b7a1f057e80105d04ef7e0ae/创建议题页面样式问题.png)";
    var descriptionContent = DescriptionContent.init(content, "123");
    expect(descriptionContent.value, "![image](/123/uploads/ab1ce029b7a1f057e80105d04ef7e0ae/创建议题页面样式问题.png)");
  });

  test("Include uploads content", () {
    String content = "/uploads";
    var descriptionContent = DescriptionContent.init(content, "123");
    expect(descriptionContent.value, "/uploads");
  });

  test("Include uploads with https", () {
    String content = "![image](https://jihulab.com/123/uploads/ab1ce029b7a1f057e80105d04ef7e0ae/创建议题页面样式问题.png)";
    var descriptionContent = DescriptionContent.init(content, "123");
    expect(descriptionContent.value, "![image](https://jihulab.com/123/uploads/ab1ce029b7a1f057e80105d04ef7e0ae/创建议题页面样式问题.png)");
  });
}
