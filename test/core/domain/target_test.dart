import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/domain/target.dart';

void main() {
  test('Should construct empty target', () async {
    var target = Target.fromJson({});
    expect(target.iid, 0);
    expect(target.createdAt, '');
    expect(target.title, '');
  });
}
