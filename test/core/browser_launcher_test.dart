import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/browser_launcher.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:mockito/annotations.dart';

@GenerateMocks([UrlLauncher])
void main() {
  setUp(() {});

  group('Browser Launcher', () {
    testWidgets('Should show dialog before open browser with default', (tester) async {
      const testButtonTitle = 'TestButton';
      await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: Builder(
            builder: (BuildContext context) {
              return InkWell(
                onTap: () {
                  UrlLauncher.open(context, 'href');
                },
                child: const Text(testButtonTitle),
              );
            },
          ),
        ),
      ));

      await tester.tap(find.text(testButtonTitle));
      await tester.pumpAndSettle();
      expect(find.byType(CupertinoAlertDialog), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().openBrowserAlertContent), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().open), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().cancel), findsOneWidget);
      await tester.tap(find.text(AppLocalizations.dictionary().open));
      expect(UrlLauncher.isCalledUrlLauncherSuccess, isTrue);

      UrlLauncher.reset();
    });

    testWidgets('Should show nothing when tap cancel in dialog with default', (tester) async {
      const testButtonTitle = 'TestButton';
      await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: Builder(
            builder: (BuildContext context) {
              return InkWell(
                onTap: () {
                  UrlLauncher.open(context, 'href');
                },
                child: const Text(testButtonTitle),
              );
            },
          ),
        ),
      ));

      await tester.tap(find.text(testButtonTitle));
      await tester.pumpAndSettle();
      expect(find.byType(CupertinoAlertDialog), findsOneWidget);
      await tester.tap(find.text(AppLocalizations.dictionary().cancel));
      expect(UrlLauncher.isCalledUrlLauncherSuccess, isFalse);

      UrlLauncher.reset();
    });

    testWidgets('Should show nothing before open browser with shouldAlert=false', (tester) async {
      const testButtonTitle = 'TestButton';
      await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: Builder(
            builder: (BuildContext context) {
              return InkWell(
                onTap: () {
                  UrlLauncher.open(context, 'href', shouldLaunchBrowserAlert: false);
                },
                child: const Text(testButtonTitle),
              );
            },
          ),
        ),
      ));

      await tester.tap(find.text(testButtonTitle));
      await tester.pumpAndSettle();
      expect(find.byType(CupertinoAlertDialog), findsNothing);
    });
  });
}
