import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/list_comparator.dart';
import 'package:jihu_gitlab_app/modules/issues/member/member_entity.dart';

void main() {
  test("Should compare list", () {
    List<MemberEntity> oldValue = [
      MemberEntity.create({"id": 2, "name": "name2", "username": "username2", "avatar_url": "avatar_url_2"}, 59893, 1)
    ];
    List<MemberEntity> newValue = [
      MemberEntity.create({"id": 1, "name": "name1", "username": "username1", "avatar_url": "avatar_url_1"}, 59893, 1),
      MemberEntity.create({"id": 2, "name": "name2_new", "username": "username2_new", "avatar_url": "avatar_url_2_new"}, 59893, 1)
    ];
    var compareResult = ListComparator.compare<MemberEntity>(oldValue, newValue);
    expect(compareResult.hasAdd, true);
    expect(compareResult.hasUpdate, true);
    expect(compareResult.add.length, 1);
    expect(compareResult.add[0].memberId, 1);
    expect(compareResult.update.length, 1);
    expect(compareResult.update[0].left.toMap(), {"id": null, "member_id": 2, "name": "name2", "username": "username2", "avatar_url": "avatar_url_2", "project_id": 59893, "version": 1});
    expect(compareResult.update[0].right.toMap(), {"id": null, "member_id": 2, "name": "name2_new", "username": "username2_new", "avatar_url": "avatar_url_2_new", "project_id": 59893, "version": 1});
  });
}
