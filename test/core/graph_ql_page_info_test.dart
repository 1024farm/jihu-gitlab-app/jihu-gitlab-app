import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/graph_ql_page_info.dart';

var pageInfo = {"hasNextPage": false, "hasPreviousPage": false, "startCursor": "MQ", "endCursor": "NQ", "__typename": "PageInfo"};

void main() {
  test('Should create Graph QL page info model object', () {
    GraphQLPageInfo info = GraphQLPageInfo.fromJson(pageInfo);
    expect(info.endCursor, pageInfo['endCursor']);
    expect(info.hasNextPage, pageInfo['hasNextPage']);
  });
}
