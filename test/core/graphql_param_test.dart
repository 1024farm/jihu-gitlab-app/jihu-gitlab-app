import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/graphql_param.dart';

void main() {
  var params = [
    {'input': 'a', 'output': 'a'},
    {'input': 'a""', 'output': 'a\\"\\"'},
    {'input': 'a\\\\', 'output': 'a\\\\\\\\'},
  ];
  for (var param in params) {
    test('Should parse graphql param to ${param['output']} when ${param['input']}', () {
      final graphqlParam = GraphqlParam(param['input']!);
      expect(graphqlParam.get(), param['output']);
    });
  }
}
