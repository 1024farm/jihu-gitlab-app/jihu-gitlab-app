import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/system_version_detector.dart';

void main() {
  test("Should be able to match the system type version", () {
    expect(SystemType.matchAndroid(null), SystemType.unmaintainable);
    expect(SystemType.matchAndroid(""), SystemType.unmaintainable);
    expect(SystemType.matchAndroid("9"), SystemType.unmaintainable);
    expect(SystemType.matchAndroid("10"), SystemType.maintainable);
    expect(SystemType.matchAndroid("12"), SystemType.maintainable);
    expect(SystemType.matchAndroid("13"), SystemType.latest);
    expect(SystemType.matchAndroid("18"), SystemType.latest);
    expect(SystemType.matchIos(null), SystemType.unmaintainable);
    expect(SystemType.matchIos(""), SystemType.unmaintainable);
    expect(SystemType.matchIos("12.5.7"), SystemType.unmaintainable);
    expect(SystemType.matchIos("13.7"), SystemType.unmaintainable);
    expect(SystemType.matchIos("15.7.7"), SystemType.unmaintainable);
    expect(SystemType.matchIos("15.7.9"), SystemType.maintainable);
    expect(SystemType.matchIos("16.5"), SystemType.maintainable);
    expect(SystemType.matchIos("16.6"), SystemType.maintainable);
    expect(SystemType.matchIos("16.6.1"), SystemType.latest);
    expect(SystemType.matchIos("16.7-beta"), SystemType.latest);
    expect(SystemType.matchIos("17.0.0-alpha.1"), SystemType.latest);
    expect(SystemType.matchIos("17.0.0-alpha.beta"), SystemType.latest);
    expect(SystemType.matchIos("17.0.0-beta.8"), SystemType.latest);
    expect(SystemType.matchIos("17.0.0-rc.1"), SystemType.latest);
    expect(SystemType.matchIos("17.0.0."), SystemType.latest);
  });
}
