import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:mockito/mockito.dart';

import '../../mocker/tester.dart';
import 'http_request_test.mocks.dart';

void main() {
  test('Should make put request when specified host has connection', () {
    var client = MockHttpClient();
    HttpClient.injectInstanceForTesting(client);
    when(client.put('/a', {}, connection: anyNamed("connection"), useActiveConnection: true)).thenAnswer((_) => Future(() => Response<String>("pong")));
    ConnectionProvider().reset(Tester.jihuLabUser());
    ProjectProvider().changeProject(null, "https://jihulab.com");
    ProjectRequestSender.instance().put('/a', {});
    verify(client.put('/a', {}, connection: anyNamed("connection"), useActiveConnection: true)).called(1);
  });

  test('Should make put request when specified host has not connection', () {
    var client = MockHttpClient();
    HttpClient.injectInstanceForTesting(client);
    when(client.put('https://jihulab.com/a', {}, connection: null, useActiveConnection: false)).thenAnswer((_) => Future(() => Response<String>("pong")));
    ProjectProvider().changeProject(null, "https://jihulab.com");
    ProjectRequestSender.instance().put('/a', {});
    verify(client.put('https://jihulab.com/a', {}, connection: null, useActiveConnection: false)).called(1);
  });

  test('Should make delete request when specified host has connection', () {
    var client = MockHttpClient();
    HttpClient.injectInstanceForTesting(client);
    when(client.delete('/a', connection: anyNamed("connection"), useActiveConnection: true)).thenAnswer((_) => Future(() => Response<String>("pong")));
    ConnectionProvider().reset(Tester.jihuLabUser());
    ProjectProvider().changeProject(null, "https://jihulab.com");
    ProjectRequestSender.instance().delete('/a');
    verify(client.delete('/a', connection: anyNamed("connection"), useActiveConnection: true)).called(1);
  });

  test('Should make delete request when specified host has not connection', () {
    var client = MockHttpClient();
    HttpClient.injectInstanceForTesting(client);
    when(client.delete('https://jihulab.com/a', connection: null, useActiveConnection: false)).thenAnswer((_) => Future(() => Response<String>("pong")));
    ProjectProvider().changeProject(null, "https://jihulab.com");
    ProjectRequestSender.instance().delete('/a');
    verify(client.delete('https://jihulab.com/a', connection: null, useActiveConnection: false)).called(1);
  });

  tearDown(() {
    ConnectionProvider().fullReset();
    ProjectProvider().fullReset();
  });
}
