import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection.dart';

void main() {
  test('Should get user server type', () {
    expect(Connection(User(0, 'username', 'name', 'state', 'avatarUrl', 'webUrl', 'publicEmail'), BaseUrl(''), active: true).serverType, '');
  });
}
