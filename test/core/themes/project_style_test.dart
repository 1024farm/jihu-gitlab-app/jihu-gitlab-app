import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/themes/project_style.dart';

void main() {
  test('Should display small text project style if textStyle is null', () {
    expect(ProjectStyle.build(null, Colors.black), isNotNull);
  });

  test('Should display project style mixed', () {
    expect(ProjectStyle.build(const TextStyle(), Colors.black), isNotNull);
  });
}
