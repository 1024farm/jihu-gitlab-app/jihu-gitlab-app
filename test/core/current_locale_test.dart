import 'dart:ui';

import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/l10n/current_locale.dart';

void main() {
  test("description", () {
    var locale = LocaleProvider();

    expect(locale.value, const Locale("en"));

    locale.setLocale(const Locale("zh"));

    expect(locale.value, const Locale("zh"));
  });
}
