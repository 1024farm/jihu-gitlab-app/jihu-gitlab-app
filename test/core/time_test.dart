import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/settings/settings.dart';
import 'package:jihu_gitlab_app/core/time.dart';

void main() {
  test('Should time return precise format', () {
    Settings.instance().precisionTime().enable();
    expect(Time.init('2022-01-30T16:55:31.081+08:00').value, predicate((arg) => arg.toString().contains(' Jan 30, 2022 ')));
    expect(Time.init('2022-02-28T16:55:31.081+08:00').value, predicate((arg) => arg.toString().contains(' Feb 28, 2022 ')));
    expect(Time.init('2022-03-30T16:55:31.081+08:00').value, predicate((arg) => arg.toString().contains(' Mar 30, 2022 ')));
    expect(Time.init('2022-04-30T16:55:31.081+08:00').value, predicate((arg) => arg.toString().contains(' Apr 30, 2022 ')));
    expect(Time.init('2022-05-30T16:55:31.081+08:00').value, predicate((arg) => arg.toString().contains(' May 30, 2022 ')));
    expect(Time.init('2022-06-30T16:55:31.081+08:00').value, predicate((arg) => arg.toString().contains(' Jun 30, 2022 ')));
    expect(Time.init('2022-07-30T16:55:31.081+08:00').value, predicate((arg) => arg.toString().contains(' Jul 30, 2022 ')));
    expect(Time.init('2022-08-30T16:55:31.081+08:00').value, predicate((arg) => arg.toString().contains(' Aug 30, 2022 ')));
    expect(Time.init('2022-09-30T16:55:31.081+08:00').value, predicate((arg) => arg.toString().contains(' Sep 30, 2022 ')));
    expect(Time.init('2022-10-30T16:55:31.081+08:00').value, predicate((arg) => arg.toString().contains(' Oct 30, 2022 ')));
    expect(Time.init('2022-11-30T16:55:31.081+08:00').value, predicate((arg) => arg.toString().contains(' Nov 30, 2022 ')));
    expect(Time.init('2022-12-30T16:55:31.081+08:00').value, predicate((arg) => arg.toString().contains(' Dec 30, 2022 ')));
    expect(Time.init('2022-12-30T16:05:31.081+08:00').value, predicate((arg) => arg.toString().contains(':05')));
  });

  test('Should time return recent format', () {
    Settings.instance().precisionTime().disable();
    expect(Time.init('2022-11-30T16:55:31.081+08:00').value, contains('ago'));
  });

  test('Should return empty string', () {
    Settings.instance().precisionTime().disable();
    expect(Time.init('').value, ' ');
  });

  test('Should return empty when throws', () {
    Settings.instance().precisionTime().enable();
    expect(Time.init('abc').value, ' ');
  });

  test('Should return empty when throws 2', () {
    Settings.instance().precisionTime().disable();
    expect(Time.init('abc').value, ' ');
  });

  var params = [
    {"seconds": 0, "minutes": 0, "hours": 0, "days": 0, "expect": "just now"},
    {"seconds": 1, "minutes": 0, "hours": 0, "days": 0, "expect": "1 seconds ago"},
    {"seconds": 70, "minutes": 1, "hours": 0, "days": 0, "expect": "1 minutes ago"},
    {"seconds": 70, "minutes": 70, "hours": 3, "days": 0, "expect": "3 hours ago"},
    {"seconds": 70, "minutes": 70, "hours": 25, "days": 1, "expect": "1 days ago"},
    {"seconds": 70, "minutes": 70, "hours": 25, "days": 31, "expect": "1 months ago"},
    {"seconds": 70, "minutes": 70, "hours": 25, "days": 60, "expect": "2 months ago"},
    {"seconds": 70, "minutes": 70, "hours": 25, "days": 79, "expect": "2 months ago"},
    {"seconds": 70, "minutes": 70, "hours": 25, "days": 122, "expect": "4 months ago"},
    {"seconds": 70, "minutes": 70, "hours": 25, "days": 500, "expect": "16 months ago"},
  ];

  for (var element in params) {
    var seconds = element['seconds'] as int;
    var minutes = element['minutes'] as int;
    var hours = element['hours'] as int;
    var days = element['days'] as int;
    var expects = element['expect'] as String;
    test('Should $seconds seconds $minutes minutes $hours hours $days days convert to recent format as $expects', () {
      expect(Time.init('').calculateDisplayFormat(seconds, minutes, hours, days), expects);
    });
  }
}
