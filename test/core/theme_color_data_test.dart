import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';

void main() {
  test('Expected light theme data', () {
    const ColorScheme colorScheme = AppThemeData.lightColorScheme;
    expect(colorScheme.background, const Color(0xFFF8F8FA));
    var lightTheme = AppThemeData.lightThemeData;
    expect(lightTheme.appBarTheme.backgroundColor, const Color(0xFFF8F8FA));
    var darkTheme = AppThemeData.darkThemeData;
    expect(darkTheme.appBarTheme.backgroundColor, const Color(0xFFF8F8FA));
  });
}
