import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/file_uploader.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';

import '../mocker/tester.dart';

void main() {
  test("Should be able to generate markdown text after image uploaded", () {
    ConnectionProvider().reset(Tester.jihuLabUser());
    var info = UploadedFileInfo.fromJson({
      "alt": "1678158302583",
      "url": "/uploads/cfc0d5ed97cebb7b0c042903f9b0e4ae/image.jpg",
      "full_path": "/ultimate-plan/jihu-gitlab-app/demo/uploads/cfc0d5ed97cebb7b0c042903f9b0e4ae/image.jpg",
      "markdown": "![1678158302583](/uploads/cfc0d5ed97cebb7b0c042903f9b0e4ae/1678158302583.jpg)"
    }, FileType.image);

    expect(info.url, "/uploads/cfc0d5ed97cebb7b0c042903f9b0e4ae/image.jpg");
    expect(info.fullPath, "/ultimate-plan/jihu-gitlab-app/demo/uploads/cfc0d5ed97cebb7b0c042903f9b0e4ae/image.jpg");
    expect(info.markdown, "![image](https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/uploads/cfc0d5ed97cebb7b0c042903f9b0e4ae/image.jpg)");
  });

  test("Should be able to generate markdown text after image uploaded", () {
    ConnectionProvider().reset(Tester.jihuLabUser());
    var info = UploadedFileInfo.fromJson({
      "alt": "1678158302583",
      "url": "/uploads/cfc0d5ed97cebb7b0c042903f9b0e4ae/video.mp4",
      "full_path": "/ultimate-plan/jihu-gitlab-app/demo/uploads/cfc0d5ed97cebb7b0c042903f9b0e4ae/video.mp4",
      "markdown": "![video](/uploads/cfc0d5ed97cebb7b0c042903f9b0e4ae/video.mp4)"
    }, FileType.image);
    expect(info.markdown, "![image](https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/uploads/cfc0d5ed97cebb7b0c042903f9b0e4ae/video.mp4)");
  });
}
