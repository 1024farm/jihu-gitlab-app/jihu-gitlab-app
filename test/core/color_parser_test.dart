import 'dart:ui';

import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/color_parser.dart';

void main() {
  var params = [
    {'input': '#333', 'output': const Color(0xFF333333)},
    {'input': '#ffffff', 'output': const Color(0xFFFFFFFF)},
    {'input': '#ffff', 'output': null}
  ];
  for (var param in params) {
    String input = param['input'] as String;
    Color? output = param['output'] as Color?;
    test('Should parse $input to $output', () {
      expect(ColorParser(input).parse(), output);
    });
  }
}
