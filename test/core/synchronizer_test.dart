import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/project_synchronizer.dart';
import 'package:jihu_gitlab_app/core/synchronizer.dart';
import 'package:mockito/mockito.dart';

import 'net/http_request_test.mocks.dart';

void main() {
  test("Should sync data", () async {
    List<Map<String, Object>> allData = [];
    int counter = 0;
    MockHttpClient client = MockHttpClient();
    HttpClient.injectInstanceForTesting(client);
    when(client.get<List<Map<String, Object>>>("http://remote-data-repository?page=1&per_page=1")).thenAnswer((_) => Future(() => Response.of<List<Map<String, Object>>>([
          {"id": 1}
        ])));
    when(client.get<List<Map<String, Object>>>("http://remote-data-repository?page=2&per_page=1")).thenAnswer((_) => Future(() => Response.of<List<Map<String, Object>>>([
          {"id": 2}
        ])));
    when(client.get<List<Map<String, Object>>>("http://remote-data-repository?page=3&per_page=1")).thenAnswer((_) => Future(() => Response.of<List<Map<String, Object>>>([])));
    var result = await Synchronizer<List<Map<String, Object>>>(
        url: "http://remote-data-repository",
        pageSize: 1,
        dataProcessor: (data, page, pageSize) {
          allData.addAll(data);
          counter++;
          return Future(() => counter < 3);
        }).run();

    expect(result, true);
    expect(allData.length, 2);
    expect(allData[0], {"id": 1});
    expect(allData[1], {"id": 2});
  });

  test("Should sync data 2", () async {
    List<Map<String, Object>> allData = [];
    int counter = 0;
    MockHttpClient client = MockHttpClient();
    HttpClient.injectInstanceForTesting(client);
    when(client.get<List<Map<String, Object>>>("http://remote-data-repository?page=1&per_page=1")).thenAnswer((_) => Future(() => Response.of<List<Map<String, Object>>>([
          {"id": 1}
        ])));
    when(client.get<List<Map<String, Object>>>("http://remote-data-repository?page=2&per_page=1")).thenAnswer((_) => Future(() => Response.of<List<Map<String, Object>>>([
          {"id": 2}
        ])));
    when(client.get<List<Map<String, Object>>>("http://remote-data-repository?page=3&per_page=1")).thenAnswer((_) => Future(() => Response.of<List<Map<String, Object>>>([])));
    var result = await ProjectSynchronizer<List<Map<String, Object>>>(
        url: "http://remote-data-repository",
        pageSize: 1,
        dataProcessor: (data, page, pageSize) {
          allData.addAll(data);
          counter++;
          return Future(() => counter < 3);
        }).run();

    expect(result, true);
    expect(allData.length, 2);
    expect(allData[0], {"id": 1});
    expect(allData[1], {"id": 2});
  });
}
