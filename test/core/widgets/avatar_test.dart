import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar/avatar.dart';

void main() {
  testWidgets('Should show error view when the avatar url is empty', (tester) async {
    await tester.pumpWidget(const MaterialApp(home: Scaffold(body: Avatar(avatarUrl: '', size: 32))));
    expect(find.byType(CachedNetworkImage), findsNothing);
    expect(find.image(const AssetImage('assets/images/no_avatar.png')), findsOneWidget);
  });

  testWidgets('Should show image when the avatar url is not empty', (tester) async {
    await tester.pumpWidget(const MaterialApp(home: Scaffold(body: Avatar(avatarUrl: 'avatar-url', size: 32))));
    expect(find.byType(CachedNetworkImage), findsOneWidget);
    expect(find.image(const AssetImage('assets/images/no_avatar.png')), findsOneWidget);
  });
}
