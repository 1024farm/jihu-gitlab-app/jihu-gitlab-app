import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart' as resp;
import 'package:jihu_gitlab_app/core/widgets/star.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/subgroups/subgroup_list_model.dart';
import 'package:jihu_gitlab_app/modules/projects/starred/projects_starred_model.dart';
import 'package:mockito/mockito.dart';

import '../../modules/projects/starred/projects_starred_page_test.mocks.dart';
import '../net/http_request_test.mocks.dart';

void main() {
  late MockProjectsStarredProvider starredProvider;
  var projectsStarredModel = ProjectsStarredModel();
  locator.registerSingleton(projectsStarredModel);
  starredProvider = MockProjectsStarredProvider();
  projectsStarredModel.injectDataProviderForTesting(starredProvider);

  testWidgets("Should star or unstar a project", (tester) async {
    var client = MockHttpClient();
    HttpClient.injectInstanceForTesting(client);
    when(client.post("/api/v4/projects/59893/star", {})).thenAnswer((_) => Future(() => resp.Response([])));
    when(client.post("/api/v4/projects/59893/unstar", {})).thenAnswer((_) => Future(() => resp.Response.of([])));
    var data = GroupAndProject(1, 59893, "demo", SubgroupItemType.project, "relative_path", 59894, starred: false);
    await tester.pumpWidget(MaterialApp(
        home: Scaffold(
      body: Center(child: Star(itemDataProvider: () => data)),
    )));

    await tester.pumpAndSettle();
    expect(find.byType(Star), findsOneWidget);
    expect(find.byIcon(Icons.star_border_rounded), findsOneWidget);

    await tester.ensureVisible(find.byType(Star));
    await tester.tap(find.byIcon(Icons.star_border_rounded));
    await tester.pumpAndSettle();
    verify(client.post("/api/v4/projects/59893/star", any)).called(1);
    expect(data.starred, true);
    expect(find.byIcon(Icons.star_border_rounded), findsNothing);
    expect(find.byIcon(Icons.star_rounded), findsOneWidget);

    await tester.tap(find.byIcon(Icons.star_rounded));
    await tester.pumpAndSettle();
    verify(client.post("/api/v4/projects/59893/unstar", any)).called(1);
    expect(data.starred, false);
    expect(find.byIcon(Icons.star_border_rounded), findsOneWidget);
    expect(find.byIcon(Icons.star_rounded), findsNothing);
  });

  testWidgets("Should star a starred project", (tester) async {
    var client = MockHttpClient();
    HttpClient.injectInstanceForTesting(client);
    when(client.post("/api/v4/projects/59893/star", {})).thenThrow(DioError(requestOptions: RequestOptions(path: ''), response: Response(requestOptions: RequestOptions(path: ''), statusCode: 304)));
    // when(client.post("/api/v4/projects/59893/unstar", {})).thenAnswer((_) => Future(() => Response.of([])));
    var data = GroupAndProject(1, 59893, "demo", SubgroupItemType.project, "relative_path", 59894, starred: false);
    await tester.pumpWidget(MaterialApp(
        home: Scaffold(
      body: Center(child: Star(itemDataProvider: () => data)),
    )));

    await tester.pumpAndSettle();
    expect(find.byType(Star), findsOneWidget);
    expect(find.byIcon(Icons.star_border_rounded), findsOneWidget);

    await tester.ensureVisible(find.byType(Star));
    await tester.tap(find.byIcon(Icons.star_border_rounded));
    await tester.pumpAndSettle();
    expect(data.starred, true);
  });
}
