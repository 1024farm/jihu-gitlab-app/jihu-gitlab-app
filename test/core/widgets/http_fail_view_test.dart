import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/widgets/http_fail_view.dart';

void main() {
  testWidgets('Should view http fail view', (tester) async {
    await tester.pumpWidget(const MaterialApp(home: Scaffold(body: HttpFailView())));
    await tester.pumpAndSettle();
    expect(find.text(' Refresh exception, please try again later '), findsOneWidget);
  });
}
