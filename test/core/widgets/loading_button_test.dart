import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_button.dart';

void main() {
  testWidgets('Should show progress indicator and reject click event as a elevated button', (tester) async {
    bool called = false;
    await tester.pumpWidget(MaterialApp(
        home: Scaffold(
      body: LoadingButton.asElevatedButton(
        text: const Text("提交"),
        isLoading: true,
        onPressed: () {
          called = true;
        },
      ),
    )));
    expect(find.byType(CircularProgressIndicator), findsOneWidget);
    expect(find.byWidgetPredicate((widget) => widget.runtimeType == Opacity && (widget as Opacity).opacity == 1), findsNothing);
    expect(find.byWidgetPredicate((widget) => widget.runtimeType == Opacity && (widget as Opacity).opacity == 0.5), findsOneWidget);
    expect(
        find.byWidgetPredicate(
            (widget) => widget.runtimeType == Container && (widget as Container).decoration.runtimeType == BoxDecoration && (widget.decoration as BoxDecoration).color == AppThemeData.primaryColor),
        findsOneWidget);

    await tester.tap(find.byType(LoadingButton));
    expect(called, false);
  });

  testWidgets('Should hide progress indicator and accept click event as outlined button', (tester) async {
    bool called = false;
    await tester.pumpWidget(MaterialApp(
      home: Material(
          child: LoadingButton.asOutlinedButton(
              text: const Text("提交"),
              isLoading: false,
              disabled: false,
              onPressed: () {
                called = true;
              })),
    ));
    expect(find.byType(CircularProgressIndicator), findsNothing);
    expect(called, false);
    var opacityFinder = find.byType(Opacity);
    expect(opacityFinder, findsOneWidget);
    expect(find.byWidgetPredicate((widget) => widget.runtimeType == Opacity && (widget as Opacity).opacity == 1), findsOneWidget);
    expect(find.byWidgetPredicate((widget) => widget.runtimeType == Opacity && (widget as Opacity).opacity == 0.5), findsNothing);
    expect(
        find.byWidgetPredicate((widget) =>
            widget.runtimeType == Container &&
            (widget as Container).decoration.runtimeType == BoxDecoration &&
            (widget.decoration as BoxDecoration).border == const Border.fromBorderSide(BorderSide(color: AppThemeData.primaryColor, width: 1.0, style: BorderStyle.solid))),
        findsOneWidget);

    await tester.tap(find.byType(LoadingButton));
  });
}
