import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/uri_launcher.dart';
import 'package:jihu_gitlab_app/core/widgets/app_tab_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/home/home_drawer.dart';
import 'package:jihu_gitlab_app/core/widgets/markdown/markdown_input_box.dart';
import 'package:jihu_gitlab_app/core/widgets/photo_picker.dart';
import 'package:jihu_gitlab_app/core/widgets/unauthorized_view.dart';
import 'package:jihu_gitlab_app/l10n/current_locale.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_view.dart';
import 'package:jihu_gitlab_app/modules/issues/list/project_issues_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/issues/project_issues_page.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/project_repository_page.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

import '../../../finder/text_field_hint_text_finder.dart';
import '../../../mocker/tester.dart';
import '../../../test_binding_setter.dart';
import '../../../test_data/issue.dart';
import '../../../test_data/user.dart';
import '../../net/http_request_test.mocks.dart';
import 'home_drawer_test.mocks.dart';

@GenerateNiceMocks([MockSpec<UriLauncher>()])
void main() {
  final client = MockHttpClient();

  setUp(() async {
    WidgetsFlutterBinding.ensureInitialized();

    ConnectionProvider().reset(Tester.jihuLabUser());
    when(client.getWithHeader<Map<String, dynamic>>("https://jihulab.com/api/v4/user", any)).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
    HttpClient.injectInstanceForTesting(client);
  });

  tearDown(() async {
    ConnectionProvider().fullReset();
  });

  testWidgets('Should display drawer view with version when user opened the drawer', (tester) async {
    const testButtonTitle = 'TestButton';
    GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
      child: MaterialApp(
        home: Scaffold(
          key: scaffoldKey,
          drawer: const HomeDrawer(),
          body: Center(
            child: InkWell(
                onTap: () {
                  scaffoldKey.currentState?.openDrawer();
                },
                child: const Text(testButtonTitle)),
          ),
        ),
      ),
    ));

    await tester.tap(find.text(testButtonTitle), warnIfMissed: false);
    await tester.pumpAndSettle();

    expect(find.byType(HomeDrawer), findsOneWidget);
    expect(find.textContaining('Version'), findsOneWidget);
    expect(find.textContaining('ICP备案号：鄂ICP备2021008419号-7A'), findsOneWidget);
  });

  group('Feedback via issue', () {
    testWidgets('Should display issue create succeed after user go to create issue after login', (tester) async {
      await setUpMobileBinding(tester);
      ConnectionProvider().fullReset();

      const testButtonTitle = 'TestButton';
      GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();
      setupLocator();
      await tester.pumpWidget(MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (context) => ConnectionProvider()),
          ChangeNotifierProvider(create: (context) => ProjectProvider()),
        ],
        child: MaterialApp(
          onGenerateRoute: onGenerateRoute,
          home: Scaffold(
            key: scaffoldKey,
            drawer: const HomeDrawer(),
            body: Center(
              child: InkWell(onTap: () => scaffoldKey.currentState?.openDrawer(), child: const Text(testButtonTitle)),
            ),
          ),
        ),
      ));

      // open drawer
      await tester.tap(find.text(testButtonTitle), warnIfMissed: false);
      await tester.pumpAndSettle();
      expect(find.byType(HomeDrawer), findsOneWidget);
      expect(find.text('Feedback via issue'), findsOneWidget);

      // issue list
      when(client.post<dynamic>("https://jihulab.com/api/graphql", getProjectIssuesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', '', 20),
              connection: null, useActiveConnection: false))
          .thenAnswer((_) => Future(() => Response.of(projectIssuesGraphQLResponseData)));
      await tester.tap(find.text('Feedback via issue'));
      await tester.pumpAndSettle();
      expect(find.byType(ProjectPage), findsOneWidget);
      expect(find.byType(AppTabBar), findsOneWidget);
      final tabBar = find.byType(AppTabBar).evaluate().first.widget as AppTabBar;
      expect(tabBar.controller?.index, 1);
      expect(find.byType(ProjectRepositoryPage), findsNothing);
      expect(find.byType(ProjectIssuesPage), findsOneWidget);
      expect(find.byType(IssuesView), findsOneWidget);

      // Tap the create issue button go to login page
      await tester.tap(find.byIcon(Icons.add_box_outlined));
      await tester.pumpAndSettle();
      expect(find.text("Create issue"), findsOneWidget);
      await tester.tap(find.text("Create issue"));
      await tester.pumpAndSettle();
      expect(find.byType(IssueCreationPage), findsNothing);
      expect(find.byType(UnauthorizedView), findsNothing);
      expect(find.byType(LoginPage), findsOneWidget);

      // User canceled login
      await tester.tap(find.byType(BackButton));
      await tester.pumpAndSettle();
      expect(find.byType(LoginPage), findsNothing);
      expect(find.byType(IssueCreationPage), findsNothing);
      expect(find.byType(ProjectPage), findsOneWidget);
      expect(find.byType(ProjectIssuesPage), findsOneWidget);

      // Tap the create issue button go to login page
      await tester.tap(find.byIcon(Icons.add_box_outlined));
      await tester.pumpAndSettle();
      expect(find.text("Create issue"), findsOneWidget);
      await tester.tap(find.text("Create issue"));
      await tester.pumpAndSettle();
      expect(find.byType(IssueCreationPage), findsNothing);
      expect(find.byType(UnauthorizedView), findsNothing);
      expect(find.byType(LoginPage), findsOneWidget);

      // login and go to IssueCreationPage
      ConnectionProvider().reset(Tester.jihuLabUser());
      await tester.tap(find.byType(BackButton));
      await tester.pumpAndSettle();
      expect(find.byType(LoginPage), findsNothing);
      expect(find.byType(IssueCreationPage), findsOneWidget);
      expect(find.text('Create'), findsOneWidget);
      expect(find.text('Title (required)'), findsOneWidget);
      expect(find.text('Write a issue title'), findsOneWidget);
      expect(find.byType(MarkdownInputBox), findsOneWidget);

      // create issue
      when(client.post<Map<String, dynamic>>("/api/v4/projects/59893/issues", {"title": "issue title", "description": "issue description", "labels": "", "confidential": false},
              connection: anyNamed("connection")))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>({})));
      when(client.post<dynamic>("/api/graphql", getProjectIssuesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', '', 20), connection: anyNamed("connection")))
          .thenAnswer((_) => Future(() => Response.of(projectIssuesGraphQLResponseData)));
      const issueTitle = 'issue title';
      const issueDescription = 'issue description';
      await tester.enterText(TextFieldHintTextFinder('Write a issue title'), issueTitle);
      await tester.enterText(TextFieldHintTextFinder('Write a description here...'), issueDescription);
      await tester.tap(find.text('Create'));
      await tester.pumpAndSettle(const Duration(seconds: 2));
      verify(client.post("/api/v4/projects/59893/issues", {"title": "issue title", "description": "issue description", "labels": "", "confidential": false}, connection: anyNamed("connection")))
          .called(1);
      verify(client.post<dynamic>("/api/graphql", getProjectIssuesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', '', 20), connection: anyNamed("connection"))).called(1);
      expect(find.byType(IssueCreationPage), findsNothing);
      expect(find.byType(ProjectIssuesPage), findsOneWidget);

      // go to IssueCreationPage again
      await tester.tap(find.byIcon(Icons.add_box_outlined));
      await tester.pumpAndSettle();
      expect(find.text("Create issue"), findsOneWidget);
      await tester.tap(find.text("Create issue"));
      await tester.pumpAndSettle();
      expect(find.byType(IssueCreationPage), findsOneWidget);
      expect(find.byType(UnauthorizedView), findsNothing);
      expect(find.text('Create'), findsOneWidget);
      expect(find.text('Title (required)'), findsOneWidget);
      expect(find.text('Write a issue title'), findsOneWidget);
      expect(find.byType(MarkdownInputBox), findsOneWidget);

      ProjectProvider().fullReset();
      locator.unregister<SubgroupListModel>();
      locator.unregister<ProjectsGroupsModel>();
      locator.unregister<ProjectsStarredModel>();
      locator.unregister<IssueDetailsModel>();
      locator.unregister<PhotoPicker>();
    });

    testWidgets('Should create feedback issue succeed when user has logged in to create', (tester) async {
      await setUpMobileBinding(tester);

      const testButtonTitle = 'TestButton';
      GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();
      setupLocator();
      await tester.pumpWidget(MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (context) => ConnectionProvider()),
          ChangeNotifierProvider(create: (context) => ProjectProvider()),
        ],
        child: MaterialApp(
          onGenerateRoute: onGenerateRoute,
          home: Scaffold(
            key: scaffoldKey,
            drawer: const HomeDrawer(),
            body: Center(
              child: InkWell(onTap: () => scaffoldKey.currentState?.openDrawer(), child: const Text(testButtonTitle)),
            ),
          ),
        ),
      ));

      // open drawer
      await tester.tap(find.text(testButtonTitle), warnIfMissed: false);
      await tester.pumpAndSettle();
      expect(find.byType(HomeDrawer), findsOneWidget);
      expect(find.text('Feedback via issue'), findsOneWidget);

      // issue list
      when(client.post<dynamic>("/api/graphql", getProjectIssuesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', '', 20), connection: anyNamed("connection")))
          .thenAnswer((_) => Future(() => Response.of(projectIssuesGraphQLResponseData)));
      await tester.tap(find.text('Feedback via issue'));
      await tester.pumpAndSettle();
      expect(find.byType(ProjectPage), findsOneWidget);
      expect(find.byType(AppTabBar), findsOneWidget);
      final tabBar = find.byType(AppTabBar).evaluate().first.widget as AppTabBar;
      expect(tabBar.controller?.index, 1);
      expect(find.byType(ProjectRepositoryPage), findsNothing);
      expect(find.byType(ProjectIssuesPage), findsOneWidget);

      // Tap the create issue button
      await tester.tap(find.byIcon(Icons.add_box_outlined));
      await tester.pumpAndSettle();
      expect(find.text("Create issue"), findsOneWidget);
      await tester.tap(find.text("Create issue"));
      await tester.pumpAndSettle();
      expect(find.byType(IssueCreationPage), findsOneWidget);
      expect(find.byType(UnauthorizedView), findsNothing);
      expect(find.text('Create'), findsOneWidget);
      expect(find.text('Title (required)'), findsOneWidget);
      expect(find.text('Write a issue title'), findsOneWidget);
      expect(find.byType(MarkdownInputBox), findsOneWidget);

      // create issue
      when(client.post<Map<String, dynamic>>("/api/v4/projects/59893/issues", {"title": "issue title", "description": "issue description", "labels": "", "confidential": false},
              connection: anyNamed("connection")))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>({})));
      const issueTitle = 'issue title';
      const issueDescription = 'issue description';
      await tester.enterText(TextFieldHintTextFinder('Write a issue title'), issueTitle);
      await tester.enterText(TextFieldHintTextFinder('Write a description here...'), issueDescription);
      await tester.tap(find.text('Create'));
      await tester.pumpAndSettle(const Duration(seconds: 2));
      verify(client.post("/api/v4/projects/59893/issues", {"title": "issue title", "description": "issue description", "labels": "", "confidential": false}, connection: anyNamed("connection")))
          .called(1);
      await tester.pumpAndSettle(const Duration(seconds: 2));
      expect(find.byType(IssueCreationPage), findsNothing);
      expect(find.byType(ProjectIssuesPage), findsOneWidget);

      // go to IssueCreationPage again
      await tester.tap(find.byIcon(Icons.add_box_outlined));
      await tester.pumpAndSettle();
      expect(find.text("Create issue"), findsOneWidget);
      await tester.tap(find.text("Create issue"));
      await tester.pumpAndSettle();
      expect(find.byType(IssueCreationPage), findsOneWidget);
      expect(find.byType(UnauthorizedView), findsNothing);
      expect(find.text('Create'), findsOneWidget);
      expect(find.text('Title (required)'), findsOneWidget);
      expect(find.text('Write a issue title'), findsOneWidget);
      expect(find.byType(MarkdownInputBox), findsOneWidget);

      ProjectProvider().fullReset();
      locator.unregister<SubgroupListModel>();
      locator.unregister<ProjectsGroupsModel>();
      locator.unregister<ProjectsStarredModel>();
      locator.unregister<IssueDetailsModel>();
      locator.unregister<PhotoPicker>();
    });
  });
  group('Feedback via email', () {
    testWidgets('Should open the mail app ', (tester) async {
      await setUpMobileBinding(tester);
      var mockUriLauncher = MockUriLauncher();
      when(mockUriLauncher.canLaunch(any)).thenAnswer((realInvocation) => Future(() => true));
      when(mockUriLauncher.launch(any)).thenAnswer((realInvocation) => Future(() => false));
      UriLauncher.instance().injectInstanceForTesting(mockUriLauncher);

      const testButtonTitle = 'TestButton';
      GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

      await tester.pumpWidget(MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
        child: MaterialApp(
          home: Scaffold(
            key: scaffoldKey,
            drawer: const SizedBox(
              width: 320,
              child: HomeDrawer(),
            ),
            body: Center(
              child: InkWell(
                  onTap: () {
                    scaffoldKey.currentState?.openDrawer();
                  },
                  child: const Text(testButtonTitle)),
            ),
          ),
        ),
      ));

      await tester.tap(find.text(testButtonTitle), warnIfMissed: false);
      await tester.pumpAndSettle();

      expect(find.widgetWithText(ListTile, "Feedback via email"), findsOneWidget);
      await tester.tap(find.widgetWithText(ListTile, "Feedback via email"));
      await tester.pumpAndSettle();
      expect(find.byType(CupertinoAlertDialog), findsOneWidget);
      expect(find.widgetWithText(CupertinoDialogAction, "Copy Address"), findsOneWidget);
      expect(find.widgetWithText(CupertinoDialogAction, "Cancel"), findsOneWidget);
      await tester.tap(find.widgetWithText(CupertinoDialogAction, "Copy Address"));
      await tester.pumpAndSettle();
      expect(find.byType(CupertinoAlertDialog), findsNothing);
    });

    testWidgets('Should cancel open the mail app', (tester) async {
      await setUpMobileBinding(tester);
      var mockUriLauncher = MockUriLauncher();
      when(mockUriLauncher.canLaunch(any)).thenAnswer((realInvocation) => Future(() => true));
      when(mockUriLauncher.launch(any)).thenAnswer((realInvocation) => Future(() => false));
      UriLauncher.instance().injectInstanceForTesting(mockUriLauncher);

      const testButtonTitle = 'TestButton';
      GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

      await tester.pumpWidget(MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
        child: MaterialApp(
          home: Scaffold(
            key: scaffoldKey,
            drawer: const SizedBox(
              width: 320,
              child: HomeDrawer(),
            ),
            body: Center(
              child: InkWell(
                  onTap: () {
                    scaffoldKey.currentState?.openDrawer();
                  },
                  child: const Text(testButtonTitle)),
            ),
          ),
        ),
      ));

      await tester.tap(find.text(testButtonTitle), warnIfMissed: false);
      await tester.pumpAndSettle();
      await tester.tap(find.widgetWithText(ListTile, "Feedback via email"));
      await tester.pumpAndSettle();
      await tester.tap(find.widgetWithText(CupertinoDialogAction, "Cancel"));
      await tester.pumpAndSettle();
      expect(find.byType(CupertinoAlertDialog), findsNothing);
    });
  });

  tearDown(() {
    ConnectionProvider().fullReset();
    reset(client);
  });
}
