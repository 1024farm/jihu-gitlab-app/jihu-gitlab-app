import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/widgets/popup_string_menu_button.dart';

void main() {
  final menuItems = [PopupStringMenuItem('value1', 'name1'), PopupStringMenuItem('value2', 'name2')];

  testWidgets('Should display PopupStringMenuButton with the first item name of give list', (tester) async {
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: Center(
          child: PopupStringMenuButton(menuItems: menuItems, selectedItem: menuItems.first),
        ),
      ),
    ));
    await tester.pumpAndSettle();
    expect(find.byType(PopupStringMenuButton), findsOneWidget);
    expect(find.text('name1'), findsOneWidget);
    expect(find.text('name2'), findsNothing);
  });

  testWidgets('Should display items from give list when tapped button', (tester) async {
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: Center(
          child: PopupStringMenuButton(key: const Key('test_key'), menuItems: menuItems, selectedItem: menuItems.first),
        ),
      ),
    ));
    await tester.pumpAndSettle();
    expect(find.byKey(const Key('test_key')), findsOneWidget);
    expect(find.text('name1'), findsOneWidget);
    expect(find.text('name2'), findsNothing);

    await tester.tap(find.byKey(const Key('test_key')));
    await tester.pumpAndSettle();
    expect(find.text('name1'), findsNWidgets(2));
    expect(find.text('name2'), findsOneWidget);
  });

  testWidgets('Should display the name of selected item on the button and send back the item value', (tester) async {
    String selectValue = '';

    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: Center(
          child: PopupStringMenuButton(key: const Key('test_key'), menuItems: menuItems, onItemSelected: (value) => selectValue = value),
        ),
      ),
    ));
    await tester.pumpAndSettle();
    expect(find.byType(PopupStringMenuButton), findsOneWidget);
    expect(find.byKey(const Key('test_key')), findsOneWidget);
    expect(find.text('name1'), findsOneWidget);
    expect(find.text('name2'), findsNothing);

    await tester.tap(find.byKey(const Key('test_key')));
    await tester.pumpAndSettle();
    expect(find.text('name1'), findsNWidgets(2));
    expect(find.text('name2'), findsOneWidget);

    expect(selectValue, isEmpty);
    await tester.tap(find.text('name2'));
    await tester.pumpAndSettle();
    expect(find.text('name1'), findsNothing);
    expect(find.text('name2'), findsOneWidget);
    expect(selectValue, 'value2');
  });
}
