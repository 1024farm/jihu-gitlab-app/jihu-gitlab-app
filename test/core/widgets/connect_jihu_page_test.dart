import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/widgets/connect_jihu_page.dart';
import 'package:jihu_gitlab_app/modules/auth/auth.dart';
import 'package:jihu_gitlab_app/routers.dart';

import '../../test_binding_setter.dart';

void main() {
  testWidgets('Should display connect jihu page', (tester) async {
    await setUpMobileBinding(tester);
    await tester.pumpWidget(MaterialApp(
      onGenerateRoute: onGenerateRoute,
      home: const Scaffold(
        body: ConnectJiHuPage(),
      ),
    ));
    await tester.pumpAndSettle();
    expect(find.textContaining('Connect with'), findsOneWidget);
    await tester.tap(find.text('To Connect'));
    await tester.pumpAndSettle();
    expect(find.byType(LoginPage), findsOneWidget);
  });
}
