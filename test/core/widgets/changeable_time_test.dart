import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/settings/settings.dart';
import 'package:jihu_gitlab_app/core/time.dart';
import 'package:jihu_gitlab_app/core/widgets/changeable_time.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  testWidgets('Should render time', (tester) async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    Settings.instance().precisionTime().enable();
    await tester.pumpWidget(MaterialApp(home: Scaffold(body: ChangeableTime.show(() => {}, Time.init('2022-11-30T16:55:31.081+08:00'), const Color(0xFF95979A)))));
    expect(find.textContaining(' Nov 30, 2022'), findsOneWidget);
  });

  testWidgets('Should render time and user cannot edit it', (tester) async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    Settings.instance().precisionTime().enable();
    await tester.pumpWidget(MaterialApp(home: Scaffold(body: ChangeableTime.show(() => {}, Time.init('2022-11-30T16:55:31.081+08:00'), const Color(0xFF95979A), editable: false))));
    await tester.tap(find.textContaining(' Nov 30, 2022'));
    await tester.pumpAndSettle();
    expect(find.textContaining(' Nov 30, 2022'), findsOneWidget);
  });
}
