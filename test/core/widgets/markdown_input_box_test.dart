import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:image_picker/image_picker.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/file_uploader.dart';
import 'package:jihu_gitlab_app/core/markdown_view.dart';
import 'package:jihu_gitlab_app/core/widgets/markdown/markdown_input_box.dart';
import 'package:jihu_gitlab_app/core/widgets/photo_picker.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../finder/svg_finder.dart';
import '../../test_binding_setter.dart';
import 'markdown_input_box_test.mocks.dart';

const tabs = <String>['Write', 'Preview'];

@GenerateNiceMocks([MockSpec<PhotoPicker>(), MockSpec<FileUploader>()])
void main() {
  late MockPhotoPicker mockPhotoPicker;

  setUp(() {
    mockPhotoPicker = MockPhotoPicker();
    locator.registerSingleton<PhotoPicker>(mockPhotoPicker);
  });

  group('Markdown Input Box', () {
    testWidgets('Should display with Tab Bar', (tester) async {
      await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: MarkdownInputBox(
            projectId: 123,
            controller: TextEditingController(),
            onChanged: () {},
          ),
        ),
      ));

      expect(find.byType(TabBar), findsOneWidget);
      expect(tabs.length, 2);
      expect(find.text(tabs[0]), findsOneWidget);
      expect(find.text(tabs[1]), findsOneWidget);
    });

    testWidgets('Should display TextFiled without Markdown', (tester) async {
      await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: MarkdownInputBox(
            projectId: 123,
            controller: TextEditingController(),
            onChanged: () {},
          ),
        ),
      ));

      await tester.pumpAndSettle();
      await tester.tap(find.text(tabs[0]));
      await tester.pumpAndSettle();
      expect(find.byType(TextField), findsOneWidget);
      expect(find.byType(Markdown), findsNothing);
    });

    testWidgets('Should display with Markdown without TextField', (tester) async {
      await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: MarkdownInputBox(
            projectId: 123,
            controller: TextEditingController(),
            onChanged: () {},
          ),
        ),
      ));

      await tester.tap(find.text(tabs[1]));
      await tester.pumpAndSettle();
      expect(find.byType(MarkdownView), findsOneWidget);
      expect(find.byType(TextField), findsNothing);
    });

    testWidgets('Should display nothing to preview in Markdown which TextField is empty', (tester) async {
      await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: MarkdownInputBox(
            projectId: 123,
            controller: TextEditingController(),
            onChanged: () {},
          ),
        ),
      ));

      await tester.tap(find.text(tabs[1]));
      await tester.pumpAndSettle();
      expect(find.byType(MarkdownView), findsOneWidget);
      expect(find.text('Nothing to preview.'), findsOneWidget);
    });

    testWidgets('Should display string in Markdown which did input in TextField', (tester) async {
      const inputString = 'abc';

      await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: MarkdownInputBox(
            projectId: 123,
            controller: TextEditingController(),
            onChanged: () {},
          ),
        ),
      ));

      await tester.tap(find.text(tabs[0]));
      await tester.pumpAndSettle();
      await tester.enterText(find.byType(TextField), inputString);
      await tester.pumpAndSettle();
      await tester.tap(find.text(tabs[1]));
      await tester.pumpAndSettle();
      expect(find.byType(MarkdownView), findsOneWidget);
      expect(find.text(inputString), findsOneWidget);
    });

    testWidgets('Should url available jump to browser in Markdown which did input in TextField', (tester) async {
      const inputString = '[test_url](https://www.test.url)';

      await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: MarkdownInputBox(
            projectId: 123,
            controller: TextEditingController(),
            onChanged: () {},
          ),
        ),
      ));

      await tester.tap(find.text(tabs[0]));
      await tester.pumpAndSettle();
      await tester.enterText(find.byType(TextField), inputString);
      await tester.pumpAndSettle();
      await tester.tap(find.text(tabs[1]));
      await tester.pumpAndSettle();
      expect(find.byType(MarkdownView), findsOneWidget);
      await tester.tap(find.text('test_url'));
      await tester.pumpAndSettle();
    });

    testWidgets('Should display with photo icon', (tester) async {
      await tester.pumpWidget(MaterialApp(home: Scaffold(body: MarkdownInputBox(projectId: 123, controller: TextEditingController(), onChanged: () {}))));
      expect(SvgFinder('assets/images/photo_picker.svg'), findsOneWidget);
    });

    testWidgets('Should display with video icon', (tester) async {
      await tester.pumpWidget(MaterialApp(home: Scaffold(body: MarkdownInputBox(projectId: 123, controller: TextEditingController(), onChanged: () {}))));
      expect(SvgFinder('assets/images/video_picker.svg'), findsOneWidget);
    });
  });

  testWidgets('Should be able to pick image on iOS', (tester) async {
    setUpMobileBinding(tester);
    var mockFileUploader = MockFileUploader();
    when(mockPhotoPicker.checkAccess()).thenAnswer((_) => Future(() => true));
    when(mockPhotoPicker.pickImage()).thenAnswer((_) => Future(() => PhotoPickerResult.success(XFile("a/b.png"))));
    when(mockPhotoPicker.retrieveLostData()).thenAnswer((_) => Future(() => PhotoPickerResult.fail("")));
    when(mockFileUploader.upload(projectId: 123, file: anyNamed("file")))
        .thenAnswer((realInvocation) => Future(() => FileUploadResult.success(UploadedFileInfo("mock-alt", "mock-url", "mock-full-path", "mock-markdown"))));
    FileUploader.instance().injectInstanceForTesting(mockFileUploader);

    await tester.pumpWidget(MaterialApp(
        home: Scaffold(
            body: MarkdownInputBox(
      projectId: 123,
      controller: TextEditingController(),
      onChanged: () {},
    ))));

    var imageFinder = SvgFinder('assets/images/photo_picker.svg');
    expect(imageFinder, findsOneWidget);
    await tester.ensureVisible(imageFinder);
    await tester.tap(imageFinder, warnIfMissed: false);
    await tester.pumpAndSettle();
    expect(find.text("mock-markdown"), findsOneWidget);

    await tester.tap(imageFinder, warnIfMissed: false);
    await tester.pumpAndSettle();
    expect(find.text("mock-markdownmock-markdown"), findsOneWidget);

    reset(mockFileUploader);
    reset(mockPhotoPicker);
  });

  tearDown(() {
    locator.unregister<PhotoPicker>();
  });
}
