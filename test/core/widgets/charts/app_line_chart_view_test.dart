import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/widgets/charts/app_line_chart_line.dart';
import 'package:jihu_gitlab_app/core/widgets/charts/app_line_chart_view.dart';

void main() {
  testWidgets("Should show all points on X-axis when the line points is lte 7", (tester) async {
    List<AppLineChartPoint> points = [
      AppLineChartPoint("2023-04-06", 1),
      AppLineChartPoint("2023-04-07", 2),
      AppLineChartPoint("2023-04-08", 3),
      AppLineChartPoint("2023-04-09", 4),
      AppLineChartPoint("2023-04-10", 5),
      AppLineChartPoint("2023-04-11", 6),
      AppLineChartPoint("2023-04-12", 7),
    ];
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: AppLineChartView(
          lines: [AppLineChartLine(Colors.red, AppLineChartLineStyle.solid, points, showGuideLine: false)],
          onChartTouched: (List<AppLineChartPoint> pointsOnOneDay) {
            return [];
          },
        ),
      ),
    ));
    await tester.pumpAndSettle();

    expect(find.text("4.6"), findsOneWidget);
    expect(find.text("4.7"), findsOneWidget);
    expect(find.text("4.8"), findsOneWidget);
    expect(find.text("4.9"), findsOneWidget);
    expect(find.text("4.10"), findsOneWidget);
    expect(find.text("4.11"), findsOneWidget);
    expect(find.text("4.12"), findsOneWidget);
  });
  testWidgets("Should show 5 points on X-axis when the line points is gt 7", (tester) async {
    List<AppLineChartPoint> points = [
      AppLineChartPoint("2023-04-06", 1),
      AppLineChartPoint("2023-04-07", 2),
      AppLineChartPoint("2023-04-08", 3),
      AppLineChartPoint("2023-04-09", 4),
      AppLineChartPoint("2023-04-10", 5),
      AppLineChartPoint("2023-04-11", 6),
      AppLineChartPoint("2023-04-12", 7),
      AppLineChartPoint("2023-04-13", 8),
      AppLineChartPoint("2023-04-14", 9),
      AppLineChartPoint("2023-04-15", 10),
      AppLineChartPoint("2023-04-16", 11),
      AppLineChartPoint("2023-04-17", 12),
      AppLineChartPoint("2023-04-18", 13),
      AppLineChartPoint("2023-04-19", 14),
    ];
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: AppLineChartView(
          lines: [AppLineChartLine(Colors.red, AppLineChartLineStyle.solid, points, showGuideLine: false)],
          onChartTouched: (List<AppLineChartPoint> pointsOnOneDay) {
            return [];
          },
        ),
      ),
    ));
    await tester.pumpAndSettle();

    expect(find.text("4.6"), findsOneWidget);
    expect(find.text("4.7"), findsNothing);
    expect(find.text("4.8"), findsNothing);
    expect(find.text("4.9"), findsOneWidget);
    expect(find.text("4.10"), findsNothing);
    expect(find.text("4.11"), findsNothing);
    expect(find.text("4.12"), findsNothing);
    expect(find.text("4.13"), findsOneWidget);
    expect(find.text("4.14"), findsNothing);
    expect(find.text("4.15"), findsNothing);
    expect(find.text("4.16"), findsOneWidget);
    expect(find.text("4.17"), findsNothing);
    expect(find.text("4.18"), findsNothing);
    expect(find.text("4.19"), findsOneWidget);
  });
}
