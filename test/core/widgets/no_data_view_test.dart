import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';

void main() {
  group('No Data View', () {
    String noDataIconPath = "assets/images/no_groups.png";
    String noDataMessageStr = "No groups";
    testWidgets('Should display no data image icon', (tester) async {
      await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: TipsView(icon: noDataIconPath),
        ),
      ));

      expect(find.byType(Image), findsOneWidget);
    });

    testWidgets('Should display no data message', (tester) async {
      await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: TipsView(message: noDataMessageStr),
        ),
      ));

      expect(find.text(noDataMessageStr), findsOneWidget);
    });
  });
}
