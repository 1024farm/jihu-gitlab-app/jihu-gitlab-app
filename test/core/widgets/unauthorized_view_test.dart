import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connections.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/widgets/unauthorized_view.dart';
import 'package:jihu_gitlab_app/modules/auth/auth.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  group('Unauthorized View', () {
    testWidgets('Should display Unauthorized image icon', (tester) async {
      await tester.pumpWidget(const MaterialApp(
        home: Scaffold(
          body: UnauthorizedView(),
        ),
      ));

      expect(find.byType(Image), findsOneWidget);
    });

    testWidgets('Should display "Connect to your server first."', (tester) async {
      await tester.pumpWidget(const MaterialApp(
        home: Scaffold(
          body: UnauthorizedView(),
        ),
      ));
      expect(find.text('Connect to your server first.'), findsOneWidget);
    });

    testWidgets('Should display login button with default title', (tester) async {
      await tester.pumpWidget(const MaterialApp(
        home: Scaffold(
          body: UnauthorizedView(),
        ),
      ));

      expect(find.byWidgetPredicate((Widget widget) => widget is Center && widget.child is Text && (widget.child as Text).data == 'Self-Managed'), findsOneWidget);
    });

    testWidgets('Should display 3 PopupMenuItem when user tapped drop down button', (tester) async {
      final Map<String, LoginConfig> menuItems = loginMenuList;

      await tester.pumpWidget(const MaterialApp(
        home: Scaffold(
          body: UnauthorizedView(),
        ),
      ));

      expect(find.byType(PopupMenuButton<String>), findsOneWidget);
      expect(find.byIcon(Icons.arrow_drop_down), findsOneWidget);

      await tester.tap(find.byType(PopupMenuButton<String>));
      await tester.pumpAndSettle();
      expect(find.byType(PopupMenuItem<String>), findsNWidgets(menuItems.length));

      for (var title in menuItems.keys) {
        expect(find.byWidgetPredicate((Widget widget) => widget is PopupMenuItem<String> && widget.value == title), findsOneWidget);
      }
    });

    testWidgets('Should change title of login button when user choose item from popup menu', (tester) async {
      final Map<String, LoginConfig> menuItems = loginMenuList;

      await tester.pumpWidget(const MaterialApp(
        home: Scaffold(
          body: UnauthorizedView(),
        ),
      ));

      expect(find.text('Self-Managed'), findsOneWidget);

      for (var title in menuItems.keys) {
        await tester.tap(find.byType(PopupMenuButton<String>));
        await tester.pumpAndSettle();
        await tester.tap(find.byWidgetPredicate((Widget widget) => widget is PopupMenuItem<String> && widget.value == title));
        await tester.pumpAndSettle();
        expect(find.text(title), findsOneWidget);
      }
    });

    testWidgets('Should display correct page when login button tapped', (tester) async {
      SharedPreferences.setMockInitialValues(<String, Object>{});
      await LocalStorage.init();

      final Map<String, LoginConfig> menuItems = loginMenuList;

      await tester.pumpWidget(MaterialApp(
        routes: {
          LoginPage.routeName: (context) => const LoginPage(arguments: {'host': 'test.host'}),
          SelfManagedLoginPage.routeName: (context) => const SelfManagedLoginPage()
        },
        home: const Scaffold(
          body: UnauthorizedView(),
        ),
      ));

      await tester.pumpAndSettle();
      expect(find.text('Self-Managed'), findsOneWidget);
      expect(find.byIcon(Icons.arrow_drop_down), findsOneWidget);

      for (var title in menuItems.keys) {
        await tester.tap(find.byIcon(Icons.arrow_drop_down));
        await tester.pumpAndSettle();
        await tester.tap(find.byWidgetPredicate((Widget widget) => widget is PopupMenuItem<String> && widget.value == title));
        await tester.pumpAndSettle();

        await tester.tap(find.text(title));
        await tester.pumpAndSettle();
        expect(find.byType(menuItems[title]!.pageType), findsOneWidget);

        await tester.tap(find.byType(BackButton));
        await tester.pumpAndSettle();
        expect(find.byType(UnauthorizedView), findsOneWidget);
      }
    });
  });
}
