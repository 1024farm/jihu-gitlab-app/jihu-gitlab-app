import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/widgets/label_view.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label.dart';

void main() {
  var params = [
    {
      'name': 'BBC',
      'displays': ['BBC'],
    },
    {
      'name': 'P::S',
      'displays': ['P', 'S'],
    },
    {
      'name': 'P:S:A',
      'displays': ['P:S:A'],
    },
    {
      'name': 'P:S::A',
      'displays': ['P:S', 'A'],
    },
    {
      'name': 'A::P:S',
      'displays': ['A', 'P:S'],
    },
    {
      'name': 'A::P::S',
      'displays': ['A::P', 'S'],
    }
  ];
  for (var param in params) {
    String name = param['name'] as String;
    List<String> displays = param['displays'] as List<String>;
    testWidgets("Should show $displays when name is $name", (tester) async {
      var label = Label.fromJson({"id": 1, "name": name, "color": "#333"});
      await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: LabelView(labelName: label.name, textColor: label.textColor, color: label.color),
        ),
      ));
      await tester.pumpAndSettle();
      for (var element in displays) {
        expect(find.text(element), findsOneWidget);
      }
    });
  }
}
