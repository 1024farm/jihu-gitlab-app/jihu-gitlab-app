import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/widgets/linker_text.dart';

void main() {
  testWidgets('LinkerText renders text', (tester) async {
    await tester.pumpWidget(const MaterialApp(
      home: Scaffold(
        body: Center(child: LinkerText(key: Key('test'), text: 'Test https://example.com')),
      ),
    ));

    expect(find.byKey(const Key('test')), findsOneWidget);
    expect(find.byType(LinkerText), findsOneWidget);
    expect(find.textContaining('Test ', findRichText: true), findsOneWidget);
    expect(find.text('example.com', findRichText: true), findsOneWidget);
  });

  testWidgets('LinkerText renders text without humanizing', (tester) async {
    await tester.pumpWidget(const MaterialApp(
      home: LinkerText(
        text: 'Test https://example.com',
        options: LinkifyOptions(
          humanize: false,
        ),
      ),
    ));

    expect(find.byType(LinkerText), findsOneWidget);
    expect(find.textContaining('Test ', findRichText: true), findsOneWidget);
    expect(find.text('https://example.com', findRichText: true), findsOneWidget);
  });

  testWidgets('LinkerText renders text with loose URLs', (tester) async {
    await tester.pumpWidget(const MaterialApp(
      home: LinkerText(
        text: 'Test example.com',
        options: LinkifyOptions(
          looseUrl: true,
        ),
      ),
    ));

    expect(find.byType(LinkerText), findsOneWidget);
    expect(find.textContaining('Test ', findRichText: true), findsOneWidget);
    expect(find.text('example.com', findRichText: true), findsOneWidget);
  });

  testWidgets('LinkerText renders without remove WWW', (tester) async {
    await tester.pumpWidget(const MaterialApp(
      home: LinkerText(
        text: 'Test www.example.com',
      ),
    ));

    expect(find.byType(LinkerText), findsOneWidget);
    expect(find.textContaining('Test ', findRichText: true), findsOneWidget);
    expect(find.text('www.example.com', findRichText: true), findsOneWidget);
  });

  testWidgets('LinkerText renders with remove WWW', (tester) async {
    await tester.pumpWidget(const MaterialApp(
      home: LinkerText(
        text: 'Test www.example.com',
        options: LinkifyOptions(
          removeWww: true,
        ),
      ),
    ));

    expect(find.byType(LinkerText), findsOneWidget);
    expect(find.textContaining('Test ', findRichText: true), findsOneWidget);
    expect(find.text('example.com', findRichText: true), findsOneWidget);
  });

  testWidgets('LinkerText renders text with email', (tester) async {
    await tester.pumpWidget(const MaterialApp(
      home: Scaffold(
        body: Center(child: LinkerText(text: 'Test mail@test.com')),
      ),
    ));

    expect(find.byType(LinkerText), findsOneWidget);
    expect(find.textContaining('Test ', findRichText: true), findsOneWidget);
    expect(find.text('mail@test.com', findRichText: true), findsOneWidget);
  });
}
