import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/permission_low_view.dart';
import 'package:mockito/mockito.dart';

import '../../mocker/tester.dart';
import '../../test_data/user.dart';
import '../net/http_request_test.mocks.dart';

void main() {
  final client = MockHttpClient();

  setUp(() async {
    ConnectionProvider().reset(Tester.jihuLabUser());
    when(client.getWithHeader<Map<String, dynamic>>("https://example.com/api/v4/user", any)).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
  });

  testWidgets("Should get warning notice when current user login with SaaS and has no license", (tester) async {
    when(client.get<List<dynamic>>("/api/v4/groups/0/iterations")).thenThrow(Exception());
    HttpClient.injectInstanceForTesting(client);
    await tester.pumpWidget(const MaterialApp(
      home: Scaffold(
        body: PermissionLowView(featureName: 'a'),
      ),
    ));
    expect(find.byType(PermissionLowView), findsOneWidget);
  });
}
