import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/widgets/search_result.dart';

void main() {
  var params = [
    {
      'testName': 'Should mark search word if search word is in the middle of text',
      'searchWord': 'Neil',
      'text': 'My name is Neil.',
    },
    {
      'testName': 'Should mark search word if search word is in the last of text',
      'searchWord': '.',
      'text': 'My name is Neil.',
    },
    {
      'testName': 'Should mark search word if search word is in the first of text',
      'searchWord': 'My',
      'text': 'My name is Neil.',
    },
    {
      'testName': 'Should mark search word if search word is matches ignore case',
      'searchWord': 'NEIL',
      'text': 'My name is Neil.',
    },
    {
      'testName': 'Should mark search word if search word is empty',
      'searchWord': '',
      'text': 'My name is Neil.',
    },
    {
      'testName': 'Should mark search word if search word is Chinese',
      'searchWord': '老',
      'text': '我是老王',
    },
    {
      'testName': 'Should mark search word if search word appears more than once',
      'searchWord': '老',
      'text': '我老是老王',
    }
  ];
  for (var element in params) {
    testWidgets(element['testName'] as String, (tester) async {
      await tester.pumpWidget(MaterialApp(
          home: Scaffold(
              body: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [SearchResult(keyword: element['searchWord'] as String, text: element['text'] as String)],
        )
      ]))));
      expect(find.text(element['text'] as String), findsOneWidget);
    });
  }
}
