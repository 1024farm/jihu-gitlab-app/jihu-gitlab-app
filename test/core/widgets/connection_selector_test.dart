import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar/avatar.dart';
import 'package:jihu_gitlab_app/core/widgets/connection_selector.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../finder/text_finder.dart';
import '../../mocker/tester.dart';
import '../../test_binding_setter.dart';

void main() {
  setUp(() async {
    AppLocalizations.init();
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();

    ConnectionProvider().fullReset();
    ConnectionProvider().reset(Tester.jihuLabUser());
    ConnectionProvider().injectConnectionForTest(Tester.gitLabUser());
  });

  testWidgets('Should change account in selector.', (tester) async {
    await setUpMobileBinding(tester);

    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: const MaterialApp(
        home: Scaffold(body: ConnectionSelector()),
      ),
    ));
    await tester.pumpAndSettle();
    await tester.tap(find.byType(Avatar));
    await tester.pumpAndSettle();
    expect(find.text('gitlab.com'), findsOneWidget);
    expect(find.text('jihulab.com'), findsOneWidget);
    expect(find.text('@jihulab_tester'), findsNWidgets(1));
    expect(find.text('@gitlab_tester'), findsNWidgets(1));
    expect(ConnectionProvider.connection!.baseUrl.get, 'https://gitlab.com');
    await tester.tap(find.text('jihulab.com'));
    await tester.pumpAndSettle();
    expect(ConnectionProvider.connection!.baseUrl.get, 'https://jihulab.com');
  });

  testWidgets('Should display add account button in selector.', (tester) async {
    await setUpMobileBinding(tester);

    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: const MaterialApp(
        home: Scaffold(body: ConnectionSelector()),
      ),
    ));
    await tester.pumpAndSettle();
    await tester.tap(find.byType(Avatar));
    await tester.pumpAndSettle();
    expect(find.text('@jihulab_tester'), findsNWidgets(1));
    expect(find.text('@gitlab_tester'), findsNWidgets(1));
    expect(find.byKey(const Key('AddAccountButton')), findsOneWidget);
  });

  testWidgets('Should display account type list when tap add account button.', (tester) async {
    await setUpMobileBinding(tester);

    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: const MaterialApp(
        home: Scaffold(body: ConnectionSelector()),
      ),
    ));
    await tester.pumpAndSettle();
    await tester.tap(find.byType(Avatar));
    await tester.pumpAndSettle();
    expect(find.text('@jihulab_tester'), findsNWidgets(1));
    expect(find.text('@gitlab_tester'), findsNWidgets(1));
    expect(find.byType(PopupMenuDivider), findsNWidgets(2));
    await tester.tap(find.byKey(const Key('AddAccountButton')));
    await tester.pumpAndSettle();
    expect(find.widgetWithText(MenuItemButton, 'Self-Managed'), findsOneWidget);
    expect(find.widgetWithText(MenuItemButton, 'jihulab.com'), findsOneWidget);
    expect(find.widgetWithText(MenuItemButton, 'gitlab.com'), findsOneWidget);

    expect(getTextRenderObjectFromMenuItemButton(tester, 'jihulab.com').text.style!.color, const Color(0xFF87878C));
    expect(getTextRenderObjectFromMenuItemButton(tester, 'gitlab.com').text.style!.color, const Color(0xFF87878C));
    expect(find.text('Already used'), findsNWidgets(2));
    expect(find.byType(PopupMenuDivider), findsNWidgets(4));
  });
}
