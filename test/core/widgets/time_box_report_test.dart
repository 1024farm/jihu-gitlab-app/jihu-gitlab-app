import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/clock/global_glock_setter.dart';
import 'package:jihu_gitlab_app/core/widgets/charts/app_line_chart_line.dart';
import 'package:jihu_gitlab_app/core/widgets/charts/time_box_report.dart';

void main() {
  setUp(() {
    GlobalClockSetter.fixAt(DateTime(2023, 4, 10));
  });

  test("Should be able to generate count points of burn down chart", () {
    var burnupChartDailyTotals = BurnupChartDailyTotals("2023-04-10", TimeboxMetrics(4, 0), TimeboxMetrics(0, 0));
    var start = DateTime(2023, 4, 3);
    var due = DateTime(2023, 4, 16);
    var timeBoxReport = TimeBoxReport([burnupChartDailyTotals], start, due);
    var points = timeBoxReport.asBurndownPoints(BurnChartDataType.count);
    expect(points.length, 14);
    expect(points[0], AppLineChartPoint("2023-04-03", 0));
    expect(points[1], AppLineChartPoint("2023-04-04", 0));
    expect(points[2], AppLineChartPoint("2023-04-05", 0));
    expect(points[3], AppLineChartPoint("2023-04-06", 0));
    expect(points[4], AppLineChartPoint("2023-04-07", 0));
    expect(points[5], AppLineChartPoint("2023-04-08", 0));
    expect(points[6], AppLineChartPoint("2023-04-09", 0));
    expect(points[7], AppLineChartPoint("2023-04-10", 4));
    expect(points[8], AppLineChartPoint("2023-04-11", null));
    expect(points[9], AppLineChartPoint("2023-04-12", null));
    expect(points[10], AppLineChartPoint("2023-04-13", null));
    expect(points[11], AppLineChartPoint("2023-04-14", null));
    expect(points[12], AppLineChartPoint("2023-04-15", null));
    expect(points[13], AppLineChartPoint("2023-04-16", null));
  });

  test("Should be able to generate weight points of burn down chart", () {
    var burnupChartDailyTotals = BurnupChartDailyTotals("2023-04-10", TimeboxMetrics(4, 4), TimeboxMetrics(0, 0));
    var start = DateTime(2023, 4, 3);
    var due = DateTime(2023, 4, 16);
    var timeBoxReport = TimeBoxReport([burnupChartDailyTotals], start, due);
    var points = timeBoxReport.asBurndownPoints(BurnChartDataType.weight);
    expect(points.length, 14);
    expect(points[0], AppLineChartPoint("2023-04-03", 0));
    expect(points[1], AppLineChartPoint("2023-04-04", 0));
    expect(points[2], AppLineChartPoint("2023-04-05", 0));
    expect(points[3], AppLineChartPoint("2023-04-06", 0));
    expect(points[4], AppLineChartPoint("2023-04-07", 0));
    expect(points[5], AppLineChartPoint("2023-04-08", 0));
    expect(points[6], AppLineChartPoint("2023-04-09", 0));
    expect(points[7], AppLineChartPoint("2023-04-10", 4));
    expect(points[8], AppLineChartPoint("2023-04-11", null));
    expect(points[9], AppLineChartPoint("2023-04-12", null));
    expect(points[10], AppLineChartPoint("2023-04-13", null));
    expect(points[11], AppLineChartPoint("2023-04-14", null));
    expect(points[12], AppLineChartPoint("2023-04-15", null));
    expect(points[13], AppLineChartPoint("2023-04-16", null));
  });

  test("Should be able to generate empty points when the burn data is empty", () {
    var start = DateTime(2023, 4, 3);
    var due = DateTime(2023, 4, 16);
    var timeBoxReport = TimeBoxReport([], start, due);
    var points = timeBoxReport.asBurndownPoints(BurnChartDataType.count);
    expect(points.length, 0);
  });

  test("Should be able to generate empty points when the start date is null", () {
    var burnupChartDailyTotals = BurnupChartDailyTotals("2023-04-10", TimeboxMetrics(4, 0), TimeboxMetrics(0, 0));
    var due = DateTime(2023, 4, 16);
    var timeBoxReport = TimeBoxReport([burnupChartDailyTotals], null, due);
    var points = timeBoxReport.asBurndownPoints(BurnChartDataType.count);
    expect(points.length, 0);
  });

  test("Should be able to generate empty points when the due date is null", () {
    var burnupChartDailyTotals = BurnupChartDailyTotals("2023-04-10", TimeboxMetrics(4, 0), TimeboxMetrics(0, 0));
    var start = DateTime(2023, 4, 16);
    var timeBoxReport = TimeBoxReport([burnupChartDailyTotals], start, null);
    var points = timeBoxReport.asBurndownPoints(BurnChartDataType.count);
    expect(points.length, 0);
  });

  tearDown(() {
    GlobalClockSetter.reset();
  });
}
