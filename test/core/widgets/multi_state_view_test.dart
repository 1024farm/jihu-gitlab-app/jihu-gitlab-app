import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/widgets/http_fail_view.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_view.dart';
import 'package:jihu_gitlab_app/core/widgets/multi_state_view.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';

void main() {
  testWidgets("Should be able to show LoadingView when is load state is loading", (tester) async {
    await tester.pumpWidget(const MaterialApp(
      home: Scaffold(
          body: MultiStateView(
        loadState: LoadState.loadingState,
        onRetry: null,
        child: SizedBox(),
      )),
    ));
    expect(find.byType(LoadingView), findsOneWidget);
  });

  testWidgets("Should be able to show TipsView when is load state no item", (tester) async {
    int count = 0;
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
          body: MultiStateView(
        loadState: LoadState.noItemState,
        onRetry: () async => count++,
        child: const SizedBox(),
      )),
    ));
    expect(find.byType(TipsView), findsOneWidget);
    await tester.tap(find.byType(TipsView));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(count, 1);
  });

  testWidgets("Should be able to show HttpFailView when is load state error", (tester) async {
    int count = 0;
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
          body: MultiStateView(
        loadState: LoadState.errorState,
        onRetry: () async => count++,
        child: const SizedBox(),
      )),
    ));
    expect(find.byType(HttpFailView), findsOneWidget);
    await tester.tap(find.byType(HttpFailView));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(count, 1);
  });

  testWidgets("Should be able to show target view when is load state is success", (tester) async {
    int count = 0;
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
          body: MultiStateView(
        loadState: LoadState.successState,
        onRetry: () async => count++,
        child: const Text("Loading Success"),
      )),
    ));
    expect(find.text("Loading Success"), findsOneWidget);
  });
}
