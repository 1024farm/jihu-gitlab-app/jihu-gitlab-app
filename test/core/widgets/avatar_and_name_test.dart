import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar/avatar.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar_and_name.dart';

void main() {
  testWidgets('Should display user`s avatar and name', (tester) async {
    await tester.pumpWidget(
      const MaterialApp(
          home: Scaffold(
              body: AvatarAndName(
        username: "tester",
        avatarUrl: "avatar-tester",
      ))),
    );
    expect(find.byType(Text), findsOneWidget);
    expect(find.byType(Avatar), findsOneWidget);
  });
}
