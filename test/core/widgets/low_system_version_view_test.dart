import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/system_version_detector.dart';
import 'package:jihu_gitlab_app/core/widgets/linker_text.dart';
import 'package:jihu_gitlab_app/core/widgets/low_system_version_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

void main() {
  AppLocalizations.init();
  var testCases = [
    {
      "alert": "You are using a discontinued OS: iOS 15.7.8\nIt has security vulnerabilities\nShould upgrade to maintained versions: 15.7.8+",
      "currentVersion": "iOS 15.7.8",
      "matchResult": SystemType.maintainable,
      "forGeek": false,
      "title": "Security Warning"
    },
    {
      "alert": "You are using a discontinued OS: iOS 13.0.0\nIt has security vulnerabilities\nShould upgrade to maintained versions: 15.7.8+",
      "currentVersion": "iOS 13.0.0",
      "matchResult": SystemType.unmaintainable,
      "forGeek": false,
      "title": "Security Warning"
    },
    {
      "alert": "You are using an outdated OS: iOS 15.7.8\nCan’t use this feature\nShould upgrade to the latest version: 16.6",
      "currentVersion": "iOS 15.7.8",
      "matchResult": SystemType.maintainable,
      "forGeek": true,
      "title": "Geek Exclusive"
    },
    {
      "alert": "You are using a discontinued OS: iOS 13\nIt has security vulnerabilities\nShould upgrade to maintained versions: 15.7.8+",
      "currentVersion": "iOS 13",
      "matchResult": SystemType.unmaintainable,
      "forGeek": true,
      "title": "Security Warning"
    }
  ];
  group('Should user see low system version warnings by low system version phone.', () {
    SystemType.maxVersion = "iOS 16.6";
    SystemType.maintainVersion = "iOS 15.7.8";
    for (var testCase in testCases) {
      String alertContent = testCase['alert'] as String;
      String title = testCase['title'] as String;
      String currentMobileVersion = testCase['currentVersion'] as String;
      SystemType matchResult = testCase['matchResult'] as SystemType;
      var forGeek = testCase['forGeek'] as bool;
      testWidgets(
          'Support view "$alertContent alert when using $currentMobileVersion", '
          'requires $matchResult ${forGeek ? 'with' : 'without'} geek', (tester) async {
        SystemType.version = currentMobileVersion;
        await tester.pumpWidget(MaterialApp(
          home: Scaffold(
            body: LowSystemVersionView(
              systemType: matchResult,
              isForGeek: forGeek,
            ),
          ),
        ));
        await tester.pumpAndSettle();
        expect(find.text(title), findsOneWidget);
        expect(find.byType(LinkerText), findsOneWidget);
      });
    }
  });
}
