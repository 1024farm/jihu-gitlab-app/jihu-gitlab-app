import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/clipboard.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/paste_type_url_navigation.dart';
import 'package:jihu_gitlab_app/modules/issues/details/issue_details_page.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_model.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_request_page.dart';
import 'package:jihu_gitlab_app/modules/mr/models/mr_graphql_request_body.dart';
import 'package:jihu_gitlab_app/modules/root/model/root_store.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

import '../../finder/svg_finder.dart';
import '../../mocker/tester.dart';
import '../../test_data/issue.dart';
import '../../test_data/merge_request.dart';
import '../net/http_request_test.mocks.dart';
import 'paste_type_url_navigation_test.mocks.dart';

@GenerateMocks([Clipboard])
void main() {
  final client = MockHttpClient();
  final clipboard = MockClipboard();

  setUp(() async {
    WidgetsFlutterBinding.ensureInitialized();

    Clipboard.injectForTest(clipboard);
    HttpClient.injectInstanceForTesting(client);

    ConnectionProvider().reset(Tester.jihuLabUser());
  });

  testWidgets('Should not navigate to expect page when url parse error', (tester) async {
    when(clipboard.fromClipboard()).thenAnswer((_) => Future(() => ''));
    await tester.pumpWidget(
      MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (context) => RootStore()),
          ChangeNotifierProvider(create: (context) => ConnectionProvider()),
        ],
        child: MaterialApp(
          home: Scaffold(
            body: PasteTypeUrlNavigation(
                child: SvgPicture.asset(
              'assets/images/paste_and_go.svg',
              width: 20,
              height: 20,
            )),
          ),
        ),
      ),
    );
    await tester.pumpAndSettle();
    expect(find.byType(SvgPicture), findsOneWidget);
    await tester.tap(find.byType(SvgPicture));
    for (int i = 0; i < 5; i++) {
      await tester.pump(const Duration(seconds: 1));
    }
    expect(find.byType(SvgPicture), findsOneWidget);
    ConnectionProvider().fullReset();
  });

  testWidgets('Should navigate to expect issue details page', (tester) async {
    when(clipboard.fromClipboard()).thenAnswer((_) => Future(() => 'https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/issues/531/'));
    when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 6))).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
    when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 531)))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(issueDetailsGraphQLResponse)));
    when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 531), useActiveConnection: true))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(issuePostVotesGraphQLResponse)));
    when(client.get("/api/v4/projects/59893/issues/531/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => Response.of([])));
    when(client.get<List<dynamic>>("/api/v4/projects/59893/issues/531/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
    when(client.post('/api/graphql', {
      "variables": {"fullPath": 'ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', "iid": "531"},
      "query": """
        query (\$fullPath: ID!, \$iid: String!) {
          project(fullPath: \$fullPath) {
            id
            fullPath
            issue(iid: \$iid) {
              id
              iid
            }
          }
        }
      """,
    })).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>({
          "data": {
            "project": {
              "id": "gid://gitlab/Project/59893",
              "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
              "issue": {"id": "gid://gitlab/Issue/299306", "iid": "531"}
            }
          }
        })));
    var issueDetailsModel = IssueDetailsModel();
    locator.registerSingleton(issueDetailsModel);
    await tester.pumpWidget(
      MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (context) => RootStore()),
          ChangeNotifierProvider(create: (context) => ConnectionProvider()),
          ChangeNotifierProvider(create: (context) => ProjectProvider()),
        ],
        child: MaterialApp(
          onGenerateRoute: onGenerateRoute,
          home: Scaffold(
            body: PasteTypeUrlNavigation(
                child: SvgPicture.asset(
              'assets/images/paste_and_go.svg',
              width: 20,
              height: 20,
            )),
          ),
        ),
      ),
    );
    await tester.pumpAndSettle();
    expect(find.byType(SvgPicture), findsOneWidget);
    await tester.tap(find.byType(SvgPicture));
    for (int i = 0; i < 5; i++) {
      await tester.pump(const Duration(seconds: 1));
    }
    expect(find.byType(IssueDetailsPage), findsOneWidget);
    expect(find.text('回复评论'), findsOneWidget);
    expect(find.text('issue description for test'), findsOneWidget);
    expect(SvgFinder("assets/images/comment.svg"), findsOneWidget);
    expect(SvgFinder("assets/images/operate.svg"), findsOneWidget);
    ConnectionProvider().fullReset();
    locator.unregister<IssueDetailsModel>();
  });

  testWidgets('Should navigate to expect MR details page', (tester) async {
    ConnectionProvider().reset(Tester.jihuLabUser());
    when(clipboard.fromClipboard()).thenAnswer((_) => Future(() => 'https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/merge_requests/373'));

    when(client.get<Map<String, dynamic>>("/api/v4/projects/59893/merge_requests/371/approvals")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(approvalResponse)));
    when(client.get('/api/v4/projects/59893')).thenAnswer((_) => Future(() => Response.of<Map>({"path_with_namespace": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app"})));
    when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
        .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(mrRebaseGraphQLResponse)));
    when(client.post('/api/graphql', getPaidMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
        .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(paidMrRebaseGraphQLResponse)));
    when(client.post('/api/graphql', queryJobsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371, ''))).thenAnswer((_) => Future(() => Response.of(jobsGraphQLResponse)));
    when(client.post("/api/graphql", queryCommitsGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 371, ''))).thenAnswer((_) => Future(() => Response.of(commitsGraphQLResponse)));
    when(client.get<List<Map<String, dynamic>>>("/api/v4/projects/59893/merge_requests/371/diffs?page=1&per_page=20")).thenAnswer((_) => Future(() => Response.of<List<Map<String, dynamic>>>([])));
    when(client.post('/api/graphql', {
      "variables": {"fullPath": 'ultimate-plan/jihu-gitlab-app/jihu-gitlab-app'},
      "query": """
        query (\$fullPath: ID!) {
          project(fullPath: \$fullPath) {
            id
            name
            fullPath
          }
        }
      """,
    })).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>({
          "data": {
            "project": {"id": "gid://gitlab/Project/59893", "name": "demo", "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app"}
          }
        })));

    var issueDetailsModel = IssueDetailsModel();
    locator.registerSingleton(issueDetailsModel);

    var parameters = {'projectId': 59893, 'projectName': "demo", 'mergeRequestIid': 371, 'fullPath': 'ultimate-plan/jihu-gitlab-app/jihu-gitlab-app'};
    await tester.pumpWidget(
      MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (context) => RootStore()),
          ChangeNotifierProvider(create: (context) => ConnectionProvider()),
          ChangeNotifierProvider(create: (context) => ProjectProvider()),
        ],
        child: MaterialApp(
          routes: {MergeRequestPage.routeName: (context) => MergeRequestPage(arguments: parameters)},
          home: Scaffold(
            body: PasteTypeUrlNavigation(
                child: SvgPicture.asset(
              'assets/images/paste_and_go.svg',
              width: 20,
              height: 20,
            )),
          ),
        ),
      ),
    );
    await tester.pumpAndSettle();
    expect(find.byType(SvgPicture), findsOneWidget);
    await tester.tap(find.byType(SvgPicture));
    for (int i = 0; i < 5; i++) {
      await tester.pump(const Duration(seconds: 1));
    }
    expect(find.byType(MergeRequestPage), findsOneWidget);
    expect(find.text('demo !371'), findsOneWidget);
    expect(find.text('Draft: Resolve "Feature: 开发人员 Rebase MR"'), findsOneWidget);
  });

  tearDown(() {
    ConnectionProvider().fullReset();
    ProjectProvider().fullReset();
    reset(client);
  });
}
