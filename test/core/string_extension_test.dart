import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/string_extension.dart';
import 'package:jihu_gitlab_app/core/urls.dart';

const _httpUrlExample = 'http://e.cn';
const _httpsUrlExample = 'https://e.cn';

void main() {
  List<Map<String, dynamic>> params = [
    {
      'source': _httpUrlExample,
      'url': [_httpUrlExample]
    },
    {
      'source': _httpsUrlExample,
      'url': [_httpsUrlExample]
    },
    {'source': 'empty', 'url': []},
    {
      'source': '$_httpsUrlExample/picture.png',
      'url': ['$_httpsUrlExample/picture.png']
    },
    {
      'source': '$_httpsUrlExample#note_2035964',
      'url': ['$_httpsUrlExample#note_2035964']
    },
    {
      'source': '这是一个链接：$_httpsUrlExample',
      'url': [_httpsUrlExample]
    },
    {
      'source': '这是一个链接：$_httpUrlExample',
      'url': [_httpUrlExample]
    },
    {
      'source': '这是一个链接：$_httpUrlExample 这个链接很强大！',
      'url': [_httpUrlExample]
    },
    {
      'source': '这是一个链接：$_httpsUrlExample 这个链接很强大！',
      'url': [_httpsUrlExample]
    },
    ..._testCases(),
    {
      'source': '这是一个链接：$_httpUrlExample这个链接很强大！',
      'url': ['$_httpUrlExample这个链接很强大！']
    },
    {
      'source': '''$_httpUrlExample
      $_httpUrlExample
      $_httpsUrlExample''',
      'url': [_httpUrlExample, _httpUrlExample, _httpsUrlExample]
    },
    {
      'source': '这是图片：$_httpsUrlExample/picture.png，这是文案：$_httpsUrlExample#note_220',
      'url': ['$_httpsUrlExample/picture.png', '$_httpsUrlExample#note_220']
    },
    ..._testCases2(),
  ];
  for (var param in params) {
    test('Should "${param['source']}" parsed ${param['url']}', () {
      expect(Urls((param['source'] as String)).urls, param['url']);
    });
  }

  List<Map<String, dynamic>> catchParams = [
    {'source': '@@ -32,6 +32,10 @@ ', 'start': '@@ ', 'end': ' @@ ', 'include_from': true, 'include_to': true, 'substring': '@@ -32,6 +32,10 @@ '},
    {'source': '@@ -32,6 +32,10 @@ ', 'start': '@@ ', 'end': ' @@ ', 'include_from': false, 'include_to': true, 'substring': '-32,6 +32,10 @@ '},
    {'source': '@@ -32,6 +32,10 @@ ', 'start': '@@ ', 'end': ' @@ ', 'include_from': true, 'include_to': false, 'substring': '@@ -32,6 +32,10'},
    {'source': '@@ -32,6 +32,10 @@ ', 'start': '@@ ', 'end': ' @@ ', 'include_from': false, 'include_to': false, 'substring': '-32,6 +32,10'},
    {'source': '@@ -32,6 +32,10 @@ ', 'start': '@@ ', 'end': null, 'include_from': true, 'include_to': true, 'substring': '@@ -32,6 +32,10 @@ '},
    {'source': '@@ -32,6 +32,10 @@ ', 'start': '@@ ', 'end': null, 'include_from': false, 'include_to': true, 'substring': '-32,6 +32,10 @@ '},
    {'source': '@@ -32,6 +32,10 @@ ', 'start': null, 'end': ' @@ ', 'include_from': true, 'include_to': true, 'substring': '@@ -32,6 +32,10 @@ '},
    {'source': '@@ -32,6 +32,10 @@ ', 'start': null, 'end': ' @@ ', 'include_from': true, 'include_to': false, 'substring': '@@ -32,6 +32,10'},
    {'source': '@@ -32,6 +32,10 @@ class ;\n@@ ', 'start': ' @@ ', 'end': '@@ ', 'include_from': true, 'include_to': true, 'substring': ' @@ class ;\n@@ '},
    {'source': '@@ -32,6 +32,10 @@ class ;\n@@ ', 'start': ' @@ ', 'end': '@@ ', 'include_from': false, 'include_to': true, 'substring': 'class ;\n@@ '},
    {'source': '@@ -32,6 +32,10 @@ class ;\n@@ ', 'start': ' @@ ', 'end': '@@ ', 'include_from': true, 'include_to': false, 'substring': ' @@ class ;\n'},
    {'source': '@@ -32,6 +32,10 @@ class ;\n@@ ', 'start': ' @@ ', 'end': '@@ ', 'include_from': false, 'include_to': false, 'substring': 'class ;\n'},
    {'source': '-32,6 +32,10', 'start': '@@', 'end': null, 'include_from': true, 'include_to': true, 'substring': ''},
    {'source': '-32,6 +32,10', 'start': null, 'end': ' @@ ', 'include_from': true, 'include_to': true, 'substring': ''},
    {'source': '-32,6 +32,10', 'start': '@@ ', 'end': ' @@ ', 'include_from': true, 'include_to': true, 'substring': ''},
    {'source': '-32,6 +32,10', 'start': ' @@ ', 'end': ' @@', 'include_from': true, 'include_to': true, 'substring': ''},
    {'source': '-32,6 +32,10', 'start': '-32,6', 'end': ' @@', 'include_from': true, 'include_to': true, 'substring': ''},
    {'source': '-32,6 +32,10', 'start': ' @@ ', 'end': ',10', 'include_from': true, 'include_to': true, 'substring': ''},
  ];
  for (var param in catchParams) {
    test('Should catch substring "${param['substring']}" from "${param['source']}" by start "${param['start']}" end "${param['end']}"', () {
      expect((param['source'] as String).catchSubstring(from: param['start'], includeFrom: param['include_from'], to: param['end'], includeTo: param['include_to']), param['substring']);
    });
  }

  test('Should catch substring assert error when "from" and "to" are null value', () {
    expect(() => '@@ -32,6 +32,10 @@ '.catchSubstring(from: null, includeFrom: true, to: null, includeTo: true), throwsException);
  });
}

List<Map<String, dynamic>> _testCases() {
  const tags = [',', '，', '。', ';', '；', '{', '}', '「', '」', '<', '>', '《', '》', '(', ')', '（', '）', '|', '｜', '[', ']', '【', '】', '"', '“', '”', '’', '‘', "'", ' ', '\n'];
  return tags
      .map((tag) => {
            'source': '这是一个链接：$_httpsUrlExample$tag这个链接很强大！',
            'url': [_httpsUrlExample]
          })
      .toList();
}

List<Map<String, dynamic>> _testCases2() {
  const tags = [',', '，', '。', ';', '；', '{', '}', '「', '」', '<', '>', '《', '》', '(', ')', '（', '）', '|', '｜', '[', ']', '【', '】', '"', '“', '”', '’', '‘', "'", ' ', '\n'];
  const tags2 = ['<', '>', '《', '》', '(', ')', '（', '）', '|', '｜', '[', ']', '【', '】', '"', '“', '”', '’', '‘', "'", ' ', '\n', ',', '，', '。', ';', '；', '{', '}', '「', '」'];
  const tags3 = ['<', '>', '《', '》', '(', ')', '（', '）', '|', '｜', '\n', ',', '，', '。', ';', '；', '{', '}', '「', '」', '[', ']', '【', '】', '"', '“', '”', '’', '‘', "'", ' '];
  List<Map<String, dynamic>> result = [];
  for (int i = 0; i < tags.length; i++) {
    result.add({
      'source': 'Issue详情：$_httpsUrlExample${tags[i]}$_httpUrlExample${tags2[i]}$_httpsUrlExample${tags3[i]}https://b.n',
      'url': [_httpsUrlExample, _httpUrlExample, _httpsUrlExample, 'https://b.n']
    });
  }
  return result;
}
