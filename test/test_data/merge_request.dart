Map<String, dynamic> mrGraphQLResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "name": "极狐 GitLab APP 代码",
      "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码",
      "path": "jihu-gitlab-app",
      "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
      "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
      "allowMergeOnSkippedPipeline": false,
      "onlyAllowMergeIfAllStatusChecksPassed": false,
      "onlyAllowMergeIfAllDiscussionsAreResolved": true,
      "onlyAllowMergeIfPipelineSucceeds": true,
      "removeSourceBranchAfterMerge": true,
      "mergeRequest": {
        "id": "gid://gitlab/MergeRequest/230540",
        "iid": "84",
        "state": "opened",
        "title": "Resolve \"Feature: 用户更新议题 label\"",
        "description": "Closes #47",
        "draft": false,
        "createdAt": "2023-02-13T15:49:42+08:00",
        "rebaseInProgress": false,
        "allowCollaboration": false,
        "approved": false,
        "approvalsLeft": 2,
        "approvalsRequired": 3,
        "approvalState": {
          "approvalRulesOverwritten": true,
          "rules": [
            {"id": "gid://gitlab/ApprovalMergeRequestRule/74131", "name": "All Members", "type": "ANY_APPROVER", "approvalsRequired": 1, "eligibleApprovers": []},
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/74132",
              "name": "License-Check",
              "type": "REGULAR",
              "approvalsRequired": 2,
              "approvedBy": {
                "nodes": [
                  {
                    "id": "gid://gitlab/User/23837",
                    "username": "wanyouzhu",
                    "name": "万友 朱",
                    "state": "active",
                    "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                    "webUrl": "https://jihulab.com/wanyouzhu",
                    "publicEmail": "",
                    "commitEmail": null
                  }
                ]
              },
              "eligibleApprovers": [
                {
                  "id": "gid://gitlab/User/23836",
                  "username": "jojo0",
                  "name": "yajie xue",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png",
                  "webUrl": "https://jihulab.com/jojo0",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/23837",
                  "username": "wanyouzhu",
                  "name": "万友 朱",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                  "webUrl": "https://jihulab.com/wanyouzhu",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/1795",
                  "username": "sinkcup",
                  "name": "Zhou YANG",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
                  "webUrl": "https://jihulab.com/sinkcup",
                  "publicEmail": "zhouyang@jihulab.com",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29064",
                  "username": "NeilWang",
                  "name": "Neil Wang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png",
                  "webUrl": "https://jihulab.com/NeilWang",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29758",
                  "username": "zhangling",
                  "name": "ling zhang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png",
                  "webUrl": "https://jihulab.com/zhangling",
                  "publicEmail": "",
                  "commitEmail": null
                }
              ]
            },
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/74134",
              "name": "Coverage-Check",
              "type": "REPORT_APPROVER",
              "approvalsRequired": 1,
              "approvedBy": {
                "nodes": [
                  {
                    "id": "gid://gitlab/User/23837",
                    "username": "wanyouzhu",
                    "name": "万友 朱",
                    "state": "active",
                    "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                    "webUrl": "https://jihulab.com/wanyouzhu",
                    "publicEmail": "",
                    "commitEmail": null
                  }
                ]
              },
              "eligibleApprovers": [
                {
                  "id": "gid://gitlab/User/23837",
                  "username": "wanyouzhu",
                  "name": "万友 朱",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                  "webUrl": "https://jihulab.com/wanyouzhu",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/1795",
                  "username": "sinkcup",
                  "name": "Zhou YANG",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
                  "webUrl": "https://jihulab.com/sinkcup",
                  "publicEmail": "zhouyang@jihulab.com",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29064",
                  "username": "NeilWang",
                  "name": "Neil Wang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png",
                  "webUrl": "https://jihulab.com/NeilWang",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29758",
                  "username": "zhangling",
                  "name": "ling zhang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png",
                  "webUrl": "https://jihulab.com/zhangling",
                  "publicEmail": "",
                  "commitEmail": null
                }
              ]
            }
          ]
        },
        "approvedBy": {"nodes": []},
        "commitCount": 2,
        "commits": {
          "nodes": [
            {
              "id": "gid://gitlab/CommitPresenter/0334c32c55457cf7269db25c812284cfd4afe24f",
              "shortId": "0334c32c",
              "sha": "0334c32c55457cf7269db25c812284cfd4afe24f",
              "title": "chore: #497 Merge code from main.",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/1494c07a894a8f38e53b2fe9f683f09efff955db",
              "shortId": "1494c07a",
              "sha": "1494c07a894a8f38e53b2fe9f683f09efff955db",
              "title": "refactor: #497 Remove no needs code and rename",
              "description": ""
            }
          ]
        },
        "assignees": {
          "nodes": [
            {"id": "gid://gitlab/User/30192", "name": "Raymond Liao"}
          ]
        },
        "author": {"id": "gid://gitlab/User/30192", "name": "ling zhang", "username": "raymond-liao", "avatarUrl": null},
        "autoMergeEnabled": false,
        "autoMergeStrategy": null,
        "availableAutoMergeStrategies": [],
        "conflicts": false,
        "mergeError": null,
        "mergeOngoing": false,
        "detailedMergeStatus": "NOT_APPROVED",
        "mergeStatusEnum": "CANNOT_BE_MERGED",
        "mergeUser": null,
        "mergeWhenPipelineSucceeds": false,
        "mergeable": false,
        "mergeableDiscussionsState": true,
        "mergedAt": null,
        "rebaseCommitSha": null,
        "shouldBeRebased": true,
        "shouldRemoveSourceBranch": null,
        "sourceBranch": "499-feature-kai-fa-ren-yuan-cha-kan-merge-blocked",
        "sourceBranchExists": true,
        "sourceBranchProtected": false,
        "targetBranch": "main",
        "targetBranchExists": true,
        "squash": true,
        "squashOnMerge": true,
        "defaultMergeCommitMessage":
            "Merge branch '461-feature-yong-hu-geng-xin-yi-ti-label' into 'main'\n\nResolve \"Feature: 用户更新议题 label\"\n\nSee merge request ultimate-plan/jihu-gitlab-app/jihu-gitlab-app!360",
        "defaultSquashCommitMessage": "Resolve \"Feature: 用户更新议题 label\"",
        "headPipeline": {
          "id": "gid://gitlab/Ci::Pipeline/943141",
          "iid": "1861",
          "active": false,
          "cancelable": false,
          "complete": true,
          "coverage": 94.2,
          "status": "SUCCESS",
          "ref": "499-feature-kai-fa-ren-yuan-cha-kan-merge-blocked",
          "detailedStatus": {"icon": "status_warning", "label": "passed with warnings"},
          "codeQualityReportSummary": {"count": 3},
          "codeQualityReports": {
            "edges": [
              {
                "cursor": "MQ",
                "node": {
                  "description": "The method doesn't override an inherited method",
                  "fingerprint": "1d3e96a04c34977ae6bdd4ea70070095",
                  "line": 84,
                  "path": "test/modules/ai/ai_page_test.mocks.dart",
                  "severity": "INFO",
                  "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/3c01fc4a9e2a87c1d7660f0ab3180917ee7992b0/test/modules/ai/ai_page_test.mocks.dart#L84"
                }
              },
              {
                "cursor": "Mg",
                "node": {
                  "description": "The method doesn't override an inherited method",
                  "fingerprint": "5b52cf3a23677a78008b449059c7e94a",
                  "line": 94,
                  "path": "test/modules/ai/ai_page_test.mocks.dart",
                  "severity": "INFO",
                  "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/3c01fc4a9e2a87c1d7660f0ab3180917ee7992b0/test/modules/ai/ai_page_test.mocks.dart#L94"
                }
              },
              {
                "cursor": "Mw",
                "node": {
                  "description": "Avoid `print` calls in production code",
                  "fingerprint": "baf77a8fff3c7e68e1e0d8b7f1c6999f",
                  "line": 8,
                  "path": "test/modules/mr/pipeline_detailed_status_test.dart",
                  "severity": "INFO",
                  "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/3c01fc4a9e2a87c1d7660f0ab3180917ee7992b0/test/modules/mr/pipeline_detailed_status_test.dart#L8"
                }
              }
            ]
          },
          "jobs": {
            "nodes": [
              {
                "id": "gid://gitlab/Ci::Build/6378824",
                "name": "build_apk",
                "active": false,
                "status": "FAILED",
                "allowFailure": true,
                "duration": 208,
                "startedAt": "2023-02-14T15:34:33+08:00",
                "finishedAt": "2023-02-14T15:38:02+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2118204",
                  "name": "build",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6378824", "name": "build_apk", "status": "FAILED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6378822", "name": "code_quality", "status": "SUCCESS", "allowFailure": false},
                      {"id": "gid://gitlab/Ci::Build/6378823", "name": "unit_test", "status": "SUCCESS", "allowFailure": false}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/6378823",
                "name": "unit_test",
                "active": false,
                "status": "SUCCESS",
                "allowFailure": false,
                "duration": 558,
                "startedAt": "2023-02-14T15:34:33+08:00",
                "finishedAt": "2023-02-14T15:43:52+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2118204",
                  "name": "build",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6378824", "name": "build_apk", "status": "FAILED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6378822", "name": "code_quality", "status": "SUCCESS", "allowFailure": false},
                      {"id": "gid://gitlab/Ci::Build/6378823", "name": "unit_test", "status": "SUCCESS", "allowFailure": false}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/6378822",
                "name": "code_quality",
                "active": false,
                "status": "SUCCESS",
                "allowFailure": false,
                "duration": 263,
                "startedAt": "2023-02-14T15:34:33+08:00",
                "finishedAt": "2023-02-14T15:38:57+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2118204",
                  "name": "build",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6378824", "name": "build_apk", "status": "FAILED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6378822", "name": "code_quality", "status": "SUCCESS", "allowFailure": false},
                      {"id": "gid://gitlab/Ci::Build/6378823", "name": "unit_test", "status": "SUCCESS", "allowFailure": false}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/6378821",
                "name": "set_mirror",
                "active": false,
                "status": "SUCCESS",
                "allowFailure": true,
                "duration": 14,
                "startedAt": "2023-02-14T15:34:17+08:00",
                "finishedAt": "2023-02-14T15:34:32+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2118203",
                  "name": ".pre",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6378820", "name": "check_internet", "status": "FAILED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6378821", "name": "set_mirror", "status": "SUCCESS", "allowFailure": true}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/6378820",
                "name": "check_internet",
                "active": false,
                "status": "FAILED",
                "allowFailure": true,
                "duration": 17,
                "startedAt": "2023-02-14T15:33:59+08:00",
                "finishedAt": "2023-02-14T15:34:17+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2118203",
                  "name": ".pre",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6378820", "name": "check_internet", "status": "FAILED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6378821", "name": "set_mirror", "status": "SUCCESS", "allowFailure": true}
                    ]
                  }
                }
              }
            ]
          },
          "warnings": true,
          "warningMessages": [],
          "createdAt": "2023-02-13T15:49:25+08:00",
          "finishedAt": "2023-02-13T15:59:15+08:00",
          "committedAt": null,
          "startedAt": "2023-02-13T15:49:26+08:00"
        },
        "userPermissions": {"adminMergeRequest": true, "canMerge": true, "pushToSourceBranch": true, "readMergeRequest": true, "removeSourceBranch": true}
      }
    }
  }
};

Map<String, dynamic> paidMrGraphQLResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "onlyAllowMergeIfAllStatusChecksPassed": false,
      "mergeRequest": {
        "id": "gid://gitlab/MergeRequest/230540",
        "iid": "84",
        "approved": false,
        "approvalsLeft": 2,
        "approvalsRequired": 3,
        "approvalState": {
          "approvalRulesOverwritten": true,
          "rules": [
            {"id": "gid://gitlab/ApprovalMergeRequestRule/74131", "name": "All Members", "type": "ANY_APPROVER", "approvalsRequired": 1, "eligibleApprovers": []},
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/74132",
              "name": "License-Check",
              "type": "REGULAR",
              "approvalsRequired": 2,
              "approvedBy": {
                "nodes": [
                  {
                    "id": "gid://gitlab/User/23837",
                    "username": "wanyouzhu",
                    "name": "万友 朱",
                    "state": "active",
                    "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                    "webUrl": "https://jihulab.com/wanyouzhu",
                    "publicEmail": "",
                    "commitEmail": null
                  }
                ]
              },
              "eligibleApprovers": [
                {
                  "id": "gid://gitlab/User/23836",
                  "username": "jojo0",
                  "name": "yajie xue",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png",
                  "webUrl": "https://jihulab.com/jojo0",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/23837",
                  "username": "wanyouzhu",
                  "name": "万友 朱",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                  "webUrl": "https://jihulab.com/wanyouzhu",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/1795",
                  "username": "sinkcup",
                  "name": "Zhou YANG",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
                  "webUrl": "https://jihulab.com/sinkcup",
                  "publicEmail": "zhouyang@jihulab.com",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29064",
                  "username": "NeilWang",
                  "name": "Neil Wang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png",
                  "webUrl": "https://jihulab.com/NeilWang",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29758",
                  "username": "zhangling",
                  "name": "ling zhang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png",
                  "webUrl": "https://jihulab.com/zhangling",
                  "publicEmail": "",
                  "commitEmail": null
                }
              ]
            },
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/74134",
              "name": "Coverage-Check",
              "type": "REPORT_APPROVER",
              "approvalsRequired": 1,
              "approvedBy": {
                "nodes": [
                  {
                    "id": "gid://gitlab/User/23837",
                    "username": "wanyouzhu",
                    "name": "万友 朱",
                    "state": "active",
                    "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                    "webUrl": "https://jihulab.com/wanyouzhu",
                    "publicEmail": "",
                    "commitEmail": null
                  }
                ]
              },
              "eligibleApprovers": [
                {
                  "id": "gid://gitlab/User/23837",
                  "username": "wanyouzhu",
                  "name": "万友 朱",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                  "webUrl": "https://jihulab.com/wanyouzhu",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/1795",
                  "username": "sinkcup",
                  "name": "Zhou YANG",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
                  "webUrl": "https://jihulab.com/sinkcup",
                  "publicEmail": "zhouyang@jihulab.com",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29064",
                  "username": "NeilWang",
                  "name": "Neil Wang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png",
                  "webUrl": "https://jihulab.com/NeilWang",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29758",
                  "username": "zhangling",
                  "name": "ling zhang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png",
                  "webUrl": "https://jihulab.com/zhangling",
                  "publicEmail": "",
                  "commitEmail": null
                }
              ]
            }
          ]
        },
        "detailedMergeStatus": "NOT_APPROVED",
        "headPipeline": {
          "id": "gid://gitlab/Ci::Pipeline/943141",
          "iid": "1861",
          "codeQualityReportSummary": {"count": 3},
          "codeQualityReports": {
            "nodes": [
              {
                "description": "The method doesn't override an inherited method",
                "fingerprint": "1d3e96a04c34977ae6bdd4ea70070095",
                "line": 84,
                "path": "test/modules/ai/ai_page_test.mocks.dart",
                "severity": "INFO",
                "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/3c01fc4a9e2a87c1d7660f0ab3180917ee7992b0/test/modules/ai/ai_page_test.mocks.dart#L84"
              },
              {
                "description": "The method doesn't override an inherited method",
                "fingerprint": "5b52cf3a23677a78008b449059c7e94a",
                "line": 94,
                "path": "test/modules/ai/ai_page_test.mocks.dart",
                "severity": "INFO",
                "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/3c01fc4a9e2a87c1d7660f0ab3180917ee7992b0/test/modules/ai/ai_page_test.mocks.dart#L94"
              },
              {
                "description": "Avoid `print` calls in production code",
                "fingerprint": "baf77a8fff3c7e68e1e0d8b7f1c6999f",
                "line": 8,
                "path": "test/modules/mr/pipeline_detailed_status_test.dart",
                "severity": "INFO",
                "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/3c01fc4a9e2a87c1d7660f0ab3180917ee7992b0/test/modules/mr/pipeline_detailed_status_test.dart#L8"
              }
            ]
          }
        },
      }
    }
  }
};

Map<String, dynamic> mrGraphQLDraftResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "name": "极狐 GitLab APP 代码",
      "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码",
      "path": "jihu-gitlab-app",
      "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
      "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
      "allowMergeOnSkippedPipeline": false,
      "onlyAllowMergeIfAllStatusChecksPassed": false,
      "onlyAllowMergeIfAllDiscussionsAreResolved": true,
      "onlyAllowMergeIfPipelineSucceeds": true,
      "removeSourceBranchAfterMerge": true,
      "mergeRequest": {
        "id": "gid://gitlab/MergeRequest/231076",
        "iid": "84",
        "state": "opened",
        "title": "Draft: Resolve \"Feature: 开发人员查看 Merge blocked\"",
        "description": "Closes #47",
        "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/merge_requests/84",
        "draft": true,
        "createdAt": "2023-02-14T15:34:08+08:00",
        "rebaseInProgress": false,
        "allowCollaboration": false,
        "approved": false,
        "approvalsLeft": 2,
        "approvalsRequired": 3,
        "approvalState": {
          "approvalRulesOverwritten": true,
          "rules": [
            {"id": "gid://gitlab/ApprovalMergeRequestRule/74131", "name": "All Members", "type": "ANY_APPROVER", "approvalsRequired": 1, "eligibleApprovers": []},
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/74132",
              "name": "License-Check",
              "type": "REGULAR",
              "approvalsRequired": 1,
              "eligibleApprovers": [
                {
                  "id": "gid://gitlab/User/23836",
                  "username": "jojo0",
                  "name": "yajie xue",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png",
                  "webUrl": "https://jihulab.com/jojo0",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/23837",
                  "username": "wanyouzhu",
                  "name": "万友 朱",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                  "webUrl": "https://jihulab.com/wanyouzhu",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/1795",
                  "username": "sinkcup",
                  "name": "Zhou YANG",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
                  "webUrl": "https://jihulab.com/sinkcup",
                  "publicEmail": "zhouyang@jihulab.com",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29064",
                  "username": "NeilWang",
                  "name": "Neil Wang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png",
                  "webUrl": "https://jihulab.com/NeilWang",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29758",
                  "username": "zhangling",
                  "name": "ling zhang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png",
                  "webUrl": "https://jihulab.com/zhangling",
                  "publicEmail": "",
                  "commitEmail": null
                }
              ]
            },
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/74134",
              "name": "Coverage-Check",
              "type": "REPORT_APPROVER",
              "approvalsRequired": 1,
              "eligibleApprovers": [
                {
                  "id": "gid://gitlab/User/23837",
                  "username": "wanyouzhu",
                  "name": "万友 朱",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                  "webUrl": "https://jihulab.com/wanyouzhu",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/1795",
                  "username": "sinkcup",
                  "name": "Zhou YANG",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
                  "webUrl": "https://jihulab.com/sinkcup",
                  "publicEmail": "zhouyang@jihulab.com",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29064",
                  "username": "NeilWang",
                  "name": "Neil Wang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png",
                  "webUrl": "https://jihulab.com/NeilWang",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29758",
                  "username": "zhangling",
                  "name": "ling zhang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png",
                  "webUrl": "https://jihulab.com/zhangling",
                  "publicEmail": "",
                  "commitEmail": null
                }
              ]
            }
          ]
        },
        "approvedBy": {"nodes": []},
        "commitCount": 2,
        "commits": {
          "nodes": [
            {
              "id": "gid://gitlab/CommitPresenter/0334c32c55457cf7269db25c812284cfd4afe24f",
              "shortId": "0334c32c",
              "sha": "0334c32c55457cf7269db25c812284cfd4afe24f",
              "title": "chore: #497 Merge code from main.",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/1494c07a894a8f38e53b2fe9f683f09efff955db",
              "shortId": "1494c07a",
              "sha": "1494c07a894a8f38e53b2fe9f683f09efff955db",
              "title": "refactor: #497 Remove no needs code and rename",
              "description": ""
            }
          ]
        },
        "assignees": {
          "nodes": [
            {"id": "gid://gitlab/User/29758", "name": "ling zhang"}
          ]
        },
        "author": {"id": "gid://gitlab/User/29758", "name": "ling zhang", "username": "zhangling", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png"},
        "autoMergeEnabled": false,
        "autoMergeStrategy": null,
        "availableAutoMergeStrategies": [],
        "conflicts": false,
        "mergeError": null,
        "mergeOngoing": false,
        "detailedMergeStatus": "DRAFT_STATUS",
        "mergeStatusEnum": "CANNOT_BE_MERGED",
        "mergeUser": null,
        "mergeWhenPipelineSucceeds": false,
        "mergeable": false,
        "mergeableDiscussionsState": true,
        "mergedAt": null,
        "rebaseCommitSha": null,
        "shouldBeRebased": true,
        "shouldRemoveSourceBranch": null,
        "sourceBranch": "499-feature-kai-fa-ren-yuan-cha-kan-merge-blocked",
        "sourceBranchExists": true,
        "sourceBranchProtected": false,
        "targetBranch": "main",
        "targetBranchExists": true,
        "squash": true,
        "squashOnMerge": true,
        "defaultMergeCommitMessage":
            "Merge branch '499-feature-kai-fa-ren-yuan-cha-kan-merge-blocked' into 'main'\n\nDraft: Resolve \"Feature: 开发人员查看 Merge blocked\"\n\nSee merge request ultimate-plan/jihu-gitlab-app/jihu-gitlab-app!365",
        "defaultSquashCommitMessage": "Draft: Resolve \"Feature: 开发人员查看 Merge blocked\"",
        "headPipeline": {
          "id": "gid://gitlab/Ci::Pipeline/943141",
          "iid": "1878",
          "active": false,
          "cancelable": false,
          "complete": true,
          "coverage": 0,
          "status": "SUCCESS",
          "ref": "499-feature-kai-fa-ren-yuan-cha-kan-merge-blocked",
          "detailedStatus": {"icon": "status_warning", "label": "passed with warnings"},
          "codeQualityReportSummary": {"count": 3},
          "codeQualityReports": {
            "edges": [
              {
                "cursor": "MQ",
                "node": {
                  "description": "The method doesn't override an inherited method",
                  "fingerprint": "1d3e96a04c34977ae6bdd4ea70070095",
                  "line": 84,
                  "path": "test/modules/ai/ai_page_test.mocks.dart",
                  "severity": "INFO",
                  "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/3c01fc4a9e2a87c1d7660f0ab3180917ee7992b0/test/modules/ai/ai_page_test.mocks.dart#L84"
                }
              },
              {
                "cursor": "Mg",
                "node": {
                  "description": "The method doesn't override an inherited method",
                  "fingerprint": "5b52cf3a23677a78008b449059c7e94a",
                  "line": 94,
                  "path": "test/modules/ai/ai_page_test.mocks.dart",
                  "severity": "INFO",
                  "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/3c01fc4a9e2a87c1d7660f0ab3180917ee7992b0/test/modules/ai/ai_page_test.mocks.dart#L94"
                }
              },
              {
                "cursor": "Mw",
                "node": {
                  "description": "Avoid `print` calls in production code",
                  "fingerprint": "baf77a8fff3c7e68e1e0d8b7f1c6999f",
                  "line": 8,
                  "path": "test/modules/mr/pipeline_detailed_status_test.dart",
                  "severity": "INFO",
                  "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/3c01fc4a9e2a87c1d7660f0ab3180917ee7992b0/test/modules/mr/pipeline_detailed_status_test.dart#L8"
                }
              }
            ]
          },
          "jobs": {
            "nodes": [
              {
                "id": "gid://gitlab/Ci::Build/6378824",
                "name": "build_apk",
                "active": false,
                "status": "FAILED",
                "allowFailure": true,
                "duration": 208,
                "startedAt": "2023-02-14T15:34:33+08:00",
                "finishedAt": "2023-02-14T15:38:02+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2118204",
                  "name": "build",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6378824", "name": "build_apk", "status": "FAILED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6378822", "name": "code_quality", "status": "SUCCESS", "allowFailure": false},
                      {"id": "gid://gitlab/Ci::Build/6378823", "name": "unit_test", "status": "SUCCESS", "allowFailure": false}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/6378823",
                "name": "unit_test",
                "active": false,
                "status": "SUCCESS",
                "allowFailure": false,
                "duration": 558,
                "startedAt": "2023-02-14T15:34:33+08:00",
                "finishedAt": "2023-02-14T15:43:52+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2118204",
                  "name": "build",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6378824", "name": "build_apk", "status": "FAILED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6378822", "name": "code_quality", "status": "SUCCESS", "allowFailure": false},
                      {"id": "gid://gitlab/Ci::Build/6378823", "name": "unit_test", "status": "SUCCESS", "allowFailure": false}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/6378822",
                "name": "code_quality",
                "active": false,
                "status": "SUCCESS",
                "allowFailure": false,
                "duration": 263,
                "startedAt": "2023-02-14T15:34:33+08:00",
                "finishedAt": "2023-02-14T15:38:57+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2118204",
                  "name": "build",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6378824", "name": "build_apk", "status": "FAILED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6378822", "name": "code_quality", "status": "SUCCESS", "allowFailure": false},
                      {"id": "gid://gitlab/Ci::Build/6378823", "name": "unit_test", "status": "SUCCESS", "allowFailure": false}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/6378821",
                "name": "set_mirror",
                "active": false,
                "status": "SUCCESS",
                "allowFailure": true,
                "duration": 14,
                "startedAt": "2023-02-14T15:34:17+08:00",
                "finishedAt": "2023-02-14T15:34:32+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2118203",
                  "name": ".pre",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6378820", "name": "check_internet", "status": "FAILED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6378821", "name": "set_mirror", "status": "SUCCESS", "allowFailure": true}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/6378820",
                "name": "check_internet",
                "active": false,
                "status": "FAILED",
                "allowFailure": true,
                "duration": 17,
                "startedAt": "2023-02-14T15:33:59+08:00",
                "finishedAt": "2023-02-14T15:34:17+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2118203",
                  "name": ".pre",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6378820", "name": "check_internet", "status": "FAILED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6378821", "name": "set_mirror", "status": "SUCCESS", "allowFailure": true}
                    ]
                  }
                }
              }
            ]
          },
          "warnings": true,
          "warningMessages": [],
          "createdAt": "2023-02-14T15:33:59+08:00",
          "finishedAt": "2023-02-14T15:43:52+08:00",
          "committedAt": null,
          "startedAt": "2023-02-14T15:34:00+08:00"
        },
        "userPermissions": {"adminMergeRequest": true, "canMerge": true, "pushToSourceBranch": true, "readMergeRequest": true, "removeSourceBranch": true}
      }
    }
  }
};

Map<String, dynamic> paidMrGraphQLDraftResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "onlyAllowMergeIfAllStatusChecksPassed": false,
      "mergeRequest": {
        "id": "gid://gitlab/MergeRequest/231076",
        "iid": "84",
        "approved": false,
        "approvalsLeft": 2,
        "approvalsRequired": 3,
        "approvalState": {
          "approvalRulesOverwritten": true,
          "rules": [
            {"id": "gid://gitlab/ApprovalMergeRequestRule/74131", "name": "All Members", "type": "ANY_APPROVER", "approvalsRequired": 1, "eligibleApprovers": []},
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/74132",
              "name": "License-Check",
              "type": "REGULAR",
              "approvalsRequired": 1,
              "eligibleApprovers": [
                {
                  "id": "gid://gitlab/User/23836",
                  "username": "jojo0",
                  "name": "yajie xue",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png",
                  "webUrl": "https://jihulab.com/jojo0",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/23837",
                  "username": "wanyouzhu",
                  "name": "万友 朱",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                  "webUrl": "https://jihulab.com/wanyouzhu",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/1795",
                  "username": "sinkcup",
                  "name": "Zhou YANG",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
                  "webUrl": "https://jihulab.com/sinkcup",
                  "publicEmail": "zhouyang@jihulab.com",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29064",
                  "username": "NeilWang",
                  "name": "Neil Wang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png",
                  "webUrl": "https://jihulab.com/NeilWang",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29758",
                  "username": "zhangling",
                  "name": "ling zhang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png",
                  "webUrl": "https://jihulab.com/zhangling",
                  "publicEmail": "",
                  "commitEmail": null
                }
              ]
            },
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/74134",
              "name": "Coverage-Check",
              "type": "REPORT_APPROVER",
              "approvalsRequired": 1,
              "eligibleApprovers": [
                {
                  "id": "gid://gitlab/User/23837",
                  "username": "wanyouzhu",
                  "name": "万友 朱",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                  "webUrl": "https://jihulab.com/wanyouzhu",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/1795",
                  "username": "sinkcup",
                  "name": "Zhou YANG",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
                  "webUrl": "https://jihulab.com/sinkcup",
                  "publicEmail": "zhouyang@jihulab.com",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29064",
                  "username": "NeilWang",
                  "name": "Neil Wang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png",
                  "webUrl": "https://jihulab.com/NeilWang",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29758",
                  "username": "zhangling",
                  "name": "ling zhang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png",
                  "webUrl": "https://jihulab.com/zhangling",
                  "publicEmail": "",
                  "commitEmail": null
                }
              ]
            }
          ]
        },
        "detailedMergeStatus": "DRAFT_STATUS",
        "headPipeline": {
          "id": "gid://gitlab/Ci::Pipeline/943141",
          "iid": "1878",
          "codeQualityReportSummary": {"count": 3},
          "codeQualityReports": {
            "nodes": [
              {
                "description": "The method doesn't override an inherited method",
                "fingerprint": "1d3e96a04c34977ae6bdd4ea70070095",
                "line": 84,
                "path": "test/modules/ai/ai_page_test.mocks.dart",
                "severity": "INFO",
                "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/3c01fc4a9e2a87c1d7660f0ab3180917ee7992b0/test/modules/ai/ai_page_test.mocks.dart#L84"
              },
              {
                "description": "The method doesn't override an inherited method",
                "fingerprint": "5b52cf3a23677a78008b449059c7e94a",
                "line": 94,
                "path": "test/modules/ai/ai_page_test.mocks.dart",
                "severity": "INFO",
                "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/3c01fc4a9e2a87c1d7660f0ab3180917ee7992b0/test/modules/ai/ai_page_test.mocks.dart#L94"
              },
              {
                "description": "Avoid `print` calls in production code",
                "fingerprint": "baf77a8fff3c7e68e1e0d8b7f1c6999f",
                "line": 8,
                "path": "test/modules/mr/pipeline_detailed_status_test.dart",
                "severity": "INFO",
                "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/3c01fc4a9e2a87c1d7660f0ab3180917ee7992b0/test/modules/mr/pipeline_detailed_status_test.dart#L8"
              }
            ]
          }
        },
      }
    }
  }
};

Map<String, dynamic> mrApprovedGraphQLResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/72936",
      "name": "private demo",
      "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / private demo",
      "path": "demo",
      "fullPath": "ultimate-plan/jihu-gitlab-app/demo",
      "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo",
      "allowMergeOnSkippedPipeline": false,
      "onlyAllowMergeIfAllStatusChecksPassed": false,
      "onlyAllowMergeIfAllDiscussionsAreResolved": false,
      "onlyAllowMergeIfPipelineSucceeds": false,
      "removeSourceBranchAfterMerge": true,
      "mergeRequestsFfOnlyEnabled": true,
      "squashReadOnly": false,
      "mergeRequest": {
        "id": "gid://gitlab/MergeRequest/230988",
        "iid": "28",
        "state": "opened",
        "title": "Resolve \"测试跳过 ci\"",
        "description": "#15 #16 #17 Close #28",
        "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/-/merge_requests/28",
        "draft": false,
        "createdAt": "2023-02-14T13:38:00+08:00",
        "rebaseInProgress": false,
        "allowCollaboration": false,
        "approved": true,
        "approvalsLeft": 0,
        "approvalsRequired": 1,
        "approvalState": {
          "approvalRulesOverwritten": true,
          "rules": [
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/73798",
              "name": "All Members",
              "type": "ANY_APPROVER",
              "approvalsRequired": 0,
              "approvedBy": {
                "nodes": [
                  {
                    "id": "gid://gitlab/User/29758",
                    "username": "zhangling",
                    "name": "ling zhang",
                    "state": "active",
                    "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png",
                    "webUrl": "https://jihulab.com/zhangling",
                    "publicEmail": "",
                    "commitEmail": null
                  },
                  {
                    "id": "gid://gitlab/User/23837",
                    "username": "wanyouzhu",
                    "name": "万友 朱",
                    "state": "active",
                    "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                    "webUrl": "https://jihulab.com/wanyouzhu",
                    "publicEmail": "",
                    "commitEmail": null
                  }
                ]
              },
              "eligibleApprovers": []
            },
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/73799",
              "name": "License-Check",
              "type": "REPORT_APPROVER",
              "approvalsRequired": 0,
              "approvedBy": {
                "nodes": [
                  {
                    "id": "gid://gitlab/User/29758",
                    "username": "zhangling",
                    "name": "ling zhang",
                    "state": "active",
                    "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png",
                    "webUrl": "https://jihulab.com/zhangling",
                    "publicEmail": "",
                    "commitEmail": null
                  },
                  {
                    "id": "gid://gitlab/User/23837",
                    "username": "wanyouzhu",
                    "name": "万友 朱",
                    "state": "active",
                    "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                    "webUrl": "https://jihulab.com/wanyouzhu",
                    "publicEmail": "",
                    "commitEmail": null
                  }
                ]
              },
              "eligibleApprovers": [
                {
                  "id": "gid://gitlab/User/1795",
                  "username": "sinkcup",
                  "name": "Zhou YANG",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
                  "webUrl": "https://jihulab.com/sinkcup",
                  "publicEmail": "zhouyang@jihulab.com",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/30192",
                  "username": "raymond-liao",
                  "name": "Raymond Liao",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/30192/avatar.png",
                  "webUrl": "https://jihulab.com/raymond-liao",
                  "publicEmail": "",
                  "commitEmail": ""
                },
                {
                  "id": "gid://gitlab/User/38060",
                  "username": "group_88966_bot1",
                  "name": "group-jojo",
                  "state": "active",
                  "avatarUrl": null,
                  "webUrl": "https://jihulab.com/group_88966_bot1",
                  "publicEmail": null,
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29064",
                  "username": "NeilWang",
                  "name": "Neil Wang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png",
                  "webUrl": "https://jihulab.com/NeilWang",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/23837",
                  "username": "wanyouzhu",
                  "name": "万友 朱",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                  "webUrl": "https://jihulab.com/wanyouzhu",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/23836",
                  "username": "jojo0",
                  "name": "yajie xue",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png",
                  "webUrl": "https://jihulab.com/jojo0",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29355",
                  "username": "perity",
                  "name": "miaolu",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29355/avatar.png",
                  "webUrl": "https://jihulab.com/perity",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/38010",
                  "username": "group_88966_bot",
                  "name": "Issue comment test",
                  "state": "active",
                  "avatarUrl": null,
                  "webUrl": "https://jihulab.com/group_88966_bot",
                  "publicEmail": null,
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29473",
                  "username": "zombie",
                  "name": "鑫 范",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29473/avatar.png",
                  "webUrl": "https://jihulab.com/zombie",
                  "publicEmail": "",
                  "commitEmail": null
                }
              ]
            },
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/73800",
              "name": "Coverage-Check",
              "type": "REPORT_APPROVER",
              "approvalsRequired": 1,
              "approvedBy": {
                "nodes": [
                  {
                    "id": "gid://gitlab/User/23837",
                    "username": "wanyouzhu",
                    "name": "万友 朱",
                    "state": "active",
                    "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                    "webUrl": "https://jihulab.com/wanyouzhu",
                    "publicEmail": "",
                    "commitEmail": null
                  }
                ]
              },
              "eligibleApprovers": [
                {
                  "id": "gid://gitlab/User/1795",
                  "username": "sinkcup",
                  "name": "Zhou YANG",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
                  "webUrl": "https://jihulab.com/sinkcup",
                  "publicEmail": "zhouyang@jihulab.com",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/30192",
                  "username": "raymond-liao",
                  "name": "Raymond Liao",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/30192/avatar.png",
                  "webUrl": "https://jihulab.com/raymond-liao",
                  "publicEmail": "",
                  "commitEmail": ""
                },
                {
                  "id": "gid://gitlab/User/38060",
                  "username": "group_88966_bot1",
                  "name": "group-jojo",
                  "state": "active",
                  "avatarUrl": null,
                  "webUrl": "https://jihulab.com/group_88966_bot1",
                  "publicEmail": null,
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29064",
                  "username": "NeilWang",
                  "name": "Neil Wang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png",
                  "webUrl": "https://jihulab.com/NeilWang",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/23837",
                  "username": "wanyouzhu",
                  "name": "万友 朱",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                  "webUrl": "https://jihulab.com/wanyouzhu",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/23836",
                  "username": "jojo0",
                  "name": "yajie xue",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png",
                  "webUrl": "https://jihulab.com/jojo0",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29355",
                  "username": "perity",
                  "name": "miaolu",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29355/avatar.png",
                  "webUrl": "https://jihulab.com/perity",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/38010",
                  "username": "group_88966_bot",
                  "name": "Issue comment test",
                  "state": "active",
                  "avatarUrl": null,
                  "webUrl": "https://jihulab.com/group_88966_bot",
                  "publicEmail": null,
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29473",
                  "username": "zombie",
                  "name": "鑫 范",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29473/avatar.png",
                  "webUrl": "https://jihulab.com/zombie",
                  "publicEmail": "",
                  "commitEmail": null
                }
              ]
            }
          ]
        },
        "approvedBy": {
          "nodes": [
            {
              "id": "gid://gitlab/User/29758",
              "username": "zhangling",
              "name": "ling zhang",
              "state": "active",
              "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png",
              "webUrl": "https://jihulab.com/zhangling",
              "publicEmail": "",
              "commitEmail": null
            },
            {
              "id": "gid://gitlab/User/23837",
              "username": "wanyouzhu",
              "name": "万友 朱",
              "state": "active",
              "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
              "webUrl": "https://jihulab.com/wanyouzhu",
              "publicEmail": "",
              "commitEmail": null
            }
          ]
        },
        "assignees": {
          "nodes": [
            {"id": "gid://gitlab/User/30192", "name": "Raymond Liao"},
            {"id": "gid://gitlab/User/29758", "name": "ling zhang"}
          ]
        },
        "commitCount": 3,
        "commits": {
          "nodes": [
            {
              "id": "gid://gitlab/CommitPresenter/db14d697af79cedf243cd6dfb782a385b51c43a5",
              "shortId": "db14d697",
              "sha": "db14d697af79cedf243cd6dfb782a385b51c43a5",
              "title": "Merge remote-tracking branch 'origin/main' into 53-ce-shi-tiao-guo-ci",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/339e0ef4538c94d198c9f5b732d611854d3f7b8c",
              "shortId": "339e0ef4",
              "sha": "339e0ef4538c94d198c9f5b732d611854d3f7b8c",
              "title": "mr 28 2",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/ad0cac6376052cedc7b0c1fd8229a3d632c0366e",
              "shortId": "ad0cac63",
              "sha": "ad0cac6376052cedc7b0c1fd8229a3d632c0366e",
              "title": "mr 28",
              "description": ""
            }
          ]
        },
        "author": {"id": "gid://gitlab/User/29758", "name": "ling zhang", "username": "zhangling", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png"},
        "mergeUser": null,
        "mergedAt": null,
        "autoMergeEnabled": false,
        "autoMergeStrategy": null,
        "availableAutoMergeStrategies": [],
        "conflicts": false,
        "mergeError": "Rebase failed: Rebase locally, resolve all conflicts, then push the branch.",
        "mergeOngoing": false,
        "detailedMergeStatus": "MERGEABLE",
        "mergeStatusEnum": "CAN_BE_MERGED",
        "mergeWhenPipelineSucceeds": false,
        "mergeable": false,
        "mergeableDiscussionsState": true,
        "rebaseCommitSha": null,
        "shouldBeRebased": true,
        "shouldRemoveSourceBranch": null,
        "sourceBranch": "53-ce-shi-tiao-guo-ci",
        "sourceBranchExists": true,
        "sourceBranchProtected": false,
        "targetBranch": "main",
        "targetBranchExists": true,
        "squash": false,
        "squashOnMerge": false,
        "forceRemoveSourceBranch": true,
        "defaultMergeCommitMessage": "Merge branch '53-ce-shi-tiao-guo-ci' into 'main'\n\nResolve \"测试跳过 ci\"\n\nCloses #28\n\nSee merge request ultimate-plan/jihu-gitlab-app/demo!28",
        "defaultSquashCommitMessage": "Resolve \"测试跳过 ci\"",
        "headPipeline": null,
        "userPermissions": {"adminMergeRequest": true, "canMerge": true, "pushToSourceBranch": true, "readMergeRequest": true, "removeSourceBranch": true}
      }
    }
  }
};

Map<String, dynamic> paidMrApprovedGraphQLResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/72936",
      "onlyAllowMergeIfAllStatusChecksPassed": false,
      "mergeRequest": {
        "id": "gid://gitlab/MergeRequest/230988",
        "iid": "28",
        "approved": true,
        "approvalsLeft": 0,
        "approvalsRequired": 1,
        "approvalState": {
          "approvalRulesOverwritten": true,
          "rules": [
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/73798",
              "name": "All Members",
              "type": "ANY_APPROVER",
              "approvalsRequired": 0,
              "approvedBy": {
                "nodes": [
                  {
                    "id": "gid://gitlab/User/29758",
                    "username": "zhangling",
                    "name": "ling zhang",
                    "state": "active",
                    "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png",
                    "webUrl": "https://jihulab.com/zhangling",
                    "publicEmail": "",
                    "commitEmail": null
                  },
                  {
                    "id": "gid://gitlab/User/23837",
                    "username": "wanyouzhu",
                    "name": "万友 朱",
                    "state": "active",
                    "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                    "webUrl": "https://jihulab.com/wanyouzhu",
                    "publicEmail": "",
                    "commitEmail": null
                  }
                ]
              },
              "eligibleApprovers": []
            },
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/73799",
              "name": "License-Check",
              "type": "REPORT_APPROVER",
              "approvalsRequired": 0,
              "approvedBy": {
                "nodes": [
                  {
                    "id": "gid://gitlab/User/29758",
                    "username": "zhangling",
                    "name": "ling zhang",
                    "state": "active",
                    "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png",
                    "webUrl": "https://jihulab.com/zhangling",
                    "publicEmail": "",
                    "commitEmail": null
                  },
                  {
                    "id": "gid://gitlab/User/23837",
                    "username": "wanyouzhu",
                    "name": "万友 朱",
                    "state": "active",
                    "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                    "webUrl": "https://jihulab.com/wanyouzhu",
                    "publicEmail": "",
                    "commitEmail": null
                  }
                ]
              },
              "eligibleApprovers": [
                {
                  "id": "gid://gitlab/User/1795",
                  "username": "sinkcup",
                  "name": "Zhou YANG",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
                  "webUrl": "https://jihulab.com/sinkcup",
                  "publicEmail": "zhouyang@jihulab.com",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/30192",
                  "username": "raymond-liao",
                  "name": "Raymond Liao",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/30192/avatar.png",
                  "webUrl": "https://jihulab.com/raymond-liao",
                  "publicEmail": "",
                  "commitEmail": ""
                },
                {
                  "id": "gid://gitlab/User/38060",
                  "username": "group_88966_bot1",
                  "name": "group-jojo",
                  "state": "active",
                  "avatarUrl": null,
                  "webUrl": "https://jihulab.com/group_88966_bot1",
                  "publicEmail": null,
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29064",
                  "username": "NeilWang",
                  "name": "Neil Wang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png",
                  "webUrl": "https://jihulab.com/NeilWang",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/23837",
                  "username": "wanyouzhu",
                  "name": "万友 朱",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                  "webUrl": "https://jihulab.com/wanyouzhu",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/23836",
                  "username": "jojo0",
                  "name": "yajie xue",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png",
                  "webUrl": "https://jihulab.com/jojo0",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29355",
                  "username": "perity",
                  "name": "miaolu",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29355/avatar.png",
                  "webUrl": "https://jihulab.com/perity",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/38010",
                  "username": "group_88966_bot",
                  "name": "Issue comment test",
                  "state": "active",
                  "avatarUrl": null,
                  "webUrl": "https://jihulab.com/group_88966_bot",
                  "publicEmail": null,
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29473",
                  "username": "zombie",
                  "name": "鑫 范",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29473/avatar.png",
                  "webUrl": "https://jihulab.com/zombie",
                  "publicEmail": "",
                  "commitEmail": null
                }
              ]
            },
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/73800",
              "name": "Coverage-Check",
              "type": "REPORT_APPROVER",
              "approvalsRequired": 1,
              "approvedBy": {
                "nodes": [
                  {
                    "id": "gid://gitlab/User/23837",
                    "username": "wanyouzhu",
                    "name": "万友 朱",
                    "state": "active",
                    "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                    "webUrl": "https://jihulab.com/wanyouzhu",
                    "publicEmail": "",
                    "commitEmail": null
                  }
                ]
              },
              "eligibleApprovers": [
                {
                  "id": "gid://gitlab/User/1795",
                  "username": "sinkcup",
                  "name": "Zhou YANG",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
                  "webUrl": "https://jihulab.com/sinkcup",
                  "publicEmail": "zhouyang@jihulab.com",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/30192",
                  "username": "raymond-liao",
                  "name": "Raymond Liao",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/30192/avatar.png",
                  "webUrl": "https://jihulab.com/raymond-liao",
                  "publicEmail": "",
                  "commitEmail": ""
                },
                {
                  "id": "gid://gitlab/User/38060",
                  "username": "group_88966_bot1",
                  "name": "group-jojo",
                  "state": "active",
                  "avatarUrl": null,
                  "webUrl": "https://jihulab.com/group_88966_bot1",
                  "publicEmail": null,
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29064",
                  "username": "NeilWang",
                  "name": "Neil Wang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png",
                  "webUrl": "https://jihulab.com/NeilWang",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/23837",
                  "username": "wanyouzhu",
                  "name": "万友 朱",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                  "webUrl": "https://jihulab.com/wanyouzhu",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/23836",
                  "username": "jojo0",
                  "name": "yajie xue",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png",
                  "webUrl": "https://jihulab.com/jojo0",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29355",
                  "username": "perity",
                  "name": "miaolu",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29355/avatar.png",
                  "webUrl": "https://jihulab.com/perity",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/38010",
                  "username": "group_88966_bot",
                  "name": "Issue comment test",
                  "state": "active",
                  "avatarUrl": null,
                  "webUrl": "https://jihulab.com/group_88966_bot",
                  "publicEmail": null,
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29473",
                  "username": "zombie",
                  "name": "鑫 范",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29473/avatar.png",
                  "webUrl": "https://jihulab.com/zombie",
                  "publicEmail": "",
                  "commitEmail": null
                }
              ]
            }
          ]
        },
        "detailedMergeStatus": "MERGEABLE"
      }
    }
  }
};

Map<String, dynamic> mrRebaseGraphQLResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "name": "极狐 GitLab APP 代码",
      "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码",
      "path": "jihu-gitlab-app",
      "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
      "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
      "allowMergeOnSkippedPipeline": false,
      "onlyAllowMergeIfAllStatusChecksPassed": false,
      "onlyAllowMergeIfAllDiscussionsAreResolved": true,
      "onlyAllowMergeIfPipelineSucceeds": true,
      "removeSourceBranchAfterMerge": true,
      "mergeRequest": {
        "id": "gid://gitlab/MergeRequest/231586",
        "iid": "371",
        "state": "opened",
        "title": "Draft: Resolve \"Feature: 开发人员 Rebase MR\"",
        "description": "Some descriptions",
        "draft": true,
        "createdAt": "2023-02-15T15:28:30+08:00",
        "rebaseInProgress": false,
        "allowCollaboration": false,
        "approved": false,
        "approvalsLeft": 1,
        "approvalsRequired": 2,
        "approvalState": {
          "approvalRulesOverwritten": true,
          "rules": [
            {"id": "gid://gitlab/ApprovalMergeRequestRule/74289", "name": "All Members", "type": "ANY_APPROVER", "approvalsRequired": 1, "eligibleApprovers": []},
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/74290",
              "name": "高级工程师",
              "type": "REGULAR",
              "approvalsRequired": 1,
              "eligibleApprovers": [
                {
                  "id": "gid://gitlab/User/23836",
                  "username": "jojo0",
                  "name": "yajie xue",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png",
                  "webUrl": "https://jihulab.com/jojo0",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/23837",
                  "username": "wanyouzhu",
                  "name": "万友 朱",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                  "webUrl": "https://jihulab.com/wanyouzhu",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29064",
                  "username": "NeilWang",
                  "name": "Neil Wang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png",
                  "webUrl": "https://jihulab.com/NeilWang",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29758",
                  "username": "zhangling",
                  "name": "ling zhang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png",
                  "webUrl": "https://jihulab.com/zhangling",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/1795",
                  "username": "sinkcup",
                  "name": "Zhou YANG",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
                  "webUrl": "https://jihulab.com/sinkcup",
                  "publicEmail": "zhouyang@jihulab.com",
                  "commitEmail": null
                }
              ]
            },
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/74291",
              "name": "Coverage-Check",
              "type": "REPORT_APPROVER",
              "approvalsRequired": 0,
              "eligibleApprovers": [
                {
                  "id": "gid://gitlab/User/23837",
                  "username": "wanyouzhu",
                  "name": "万友 朱",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                  "webUrl": "https://jihulab.com/wanyouzhu",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29064",
                  "username": "NeilWang",
                  "name": "Neil Wang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png",
                  "webUrl": "https://jihulab.com/NeilWang",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29758",
                  "username": "zhangling",
                  "name": "ling zhang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png",
                  "webUrl": "https://jihulab.com/zhangling",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/1795",
                  "username": "sinkcup",
                  "name": "Zhou YANG",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
                  "webUrl": "https://jihulab.com/sinkcup",
                  "publicEmail": "zhouyang@jihulab.com",
                  "commitEmail": null
                }
              ]
            }
          ]
        },
        "approvedBy": {"nodes": []},
        "assignees": {
          "nodes": [
            {"id": "gid://gitlab/User/30192", "name": "Raymond Liao"}
          ]
        },
        "commitCount": 33,
        "commits": {
          "nodes": [
            {
              "id": "gid://gitlab/CommitPresenter/c4f32b6ed74b40e8aaca5d4ce0e35442ed241a8a",
              "shortId": "c4f32b6e",
              "sha": "c4f32b6ed74b40e8aaca5d4ce0e35442ed241a8a",
              "title": "chore: #497 Bump version",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/d3961a9dc1570de8b0aa98d39268e9ec96b8ad51",
              "shortId": "d3961a9d",
              "sha": "d3961a9dc1570de8b0aa98d39268e9ec96b8ad51",
              "title": "refactor: #497 MergeRequestState",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/cfaf1681500bc4e5ddb1c0434bbf1f88fd6ec90a",
              "shortId": "cfaf1681",
              "sha": "cfaf1681500bc4e5ddb1c0434bbf1f88fd6ec90a",
              "title": "refactor: #497 Get ApprovalRules from GraphQL API",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/2501b112772ece847882265d1e2f7e3a7616569b",
              "shortId": "2501b112",
              "sha": "2501b112772ece847882265d1e2f7e3a7616569b",
              "title": "refactor: #497 Get ApprovalRules from GraphQL API",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/d5b1949c4456a7c7e67f4910e66b3673b8cd67ee",
              "shortId": "d5b1949c",
              "sha": "d5b1949c4456a7c7e67f4910e66b3673b8cd67ee",
              "title": "refactor: #497 Get ApprovalRules from GraphQL API",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/eeb808a49fa928a9560b0641b7b9b231f68fcee6",
              "shortId": "eeb808a4",
              "sha": "eeb808a49fa928a9560b0641b7b9b231f68fcee6",
              "title": "refactor: #497 Improve PipelineStatus",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/2bf02f3b028552c9903e9705d44371498b0e6cab",
              "shortId": "2bf02f3b",
              "sha": "2bf02f3b028552c9903e9705d44371498b0e6cab",
              "title": "refactor: #497 Remove no needs Skeleton",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/5b80a3c562a1ba829c5b0598db93525a36d22e40",
              "shortId": "5b80a3c5",
              "sha": "5b80a3c562a1ba829c5b0598db93525a36d22e40",
              "title": "test: #497 Should display HttpFailView when http request failed",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/7a280d570a6a992bbfdb82b4100e0b48eaea1a63",
              "shortId": "7a280d57",
              "sha": "7a280d570a6a992bbfdb82b4100e0b48eaea1a63",
              "title": "test: #497 MR approved",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/727e1babe1d261057aa042745369e7e56ca71bc4",
              "shortId": "727e1bab",
              "sha": "727e1babe1d261057aa042745369e7e56ca71bc4",
              "title": "refactor: #497 Remove await",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/4b8f4ca7a3f6230aa2e3bf9de7f352f0233a56f3",
              "shortId": "4b8f4ca7",
              "sha": "4b8f4ca7a3f6230aa2e3bf9de7f352f0233a56f3",
              "title": "refactor: #497 Improve logic for display Skeleton",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/cab7209b4589f9f5a1418e325656220f73a8d163",
              "shortId": "cab7209b",
              "sha": "cab7209b4589f9f5a1418e325656220f73a8d163",
              "title": "refactor: #497 Add mr test data to data file",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/d86315f33526d1675e8cb1c0577e8197dea27a9d",
              "shortId": "d86315f3",
              "sha": "d86315f33526d1675e8cb1c0577e8197dea27a9d",
              "title": "refactor: #497 Remove no needs jobs request",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/470aa68b7296ad2fe294601b1a9361c2d54503e8",
              "shortId": "470aa68b",
              "sha": "470aa68b7296ad2fe294601b1a9361c2d54503e8",
              "title": "refactor: #497 Remove no needs jobs request",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/e6ac457a15a2e2f6f5bd0358ba755f1771bdf39b",
              "shortId": "e6ac457a",
              "sha": "e6ac457a15a2e2f6f5bd0358ba755f1771bdf39b",
              "title": "refactor: #497 JobState with label",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/edeaf610a0ca4aba1ca1cfadfea6471af46f71a7",
              "shortId": "edeaf610",
              "sha": "edeaf610a0ca4aba1ca1cfadfea6471af46f71a7",
              "title": "refactor: #497 Remove no needs report request",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/e6451b9caac5e853274369c379ea44892cae1511",
              "shortId": "e6451b9c",
              "sha": "e6451b9caac5e853274369c379ea44892cae1511",
              "title": "refactor: #497 Remove no needs stub get commits",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/90e351796ff473f8c42f4a26b186b245bd83e504",
              "shortId": "90e35179",
              "sha": "90e351796ff473f8c42f4a26b186b245bd83e504",
              "title": "refactor: #497 Get commits from MR.",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/0334c32c55457cf7269db25c812284cfd4afe24f",
              "shortId": "0334c32c",
              "sha": "0334c32c55457cf7269db25c812284cfd4afe24f",
              "title": "chore: #497 Merge code from main.",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/1494c07a894a8f38e53b2fe9f683f09efff955db",
              "shortId": "1494c07a",
              "sha": "1494c07a894a8f38e53b2fe9f683f09efff955db",
              "title": "refactor: #497 Remove no needs code and rename",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/bd7cfb661ed793164fec20c70cfcca67d08c19bc",
              "shortId": "bd7cfb66",
              "sha": "bd7cfb661ed793164fec20c70cfcca67d08c19bc",
              "title": "refactor: #497 Remove no needs label and method",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/649f26417bb53917891691c621b87b22f8004acd",
              "shortId": "649f2641",
              "sha": "649f26417bb53917891691c621b87b22f8004acd",
              "title": "refactor: #497 Use GraphQL API instead RESTful API for Merge Request",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/08553ea46f6c91780e77de5dea184d4a0c25e26a",
              "shortId": "08553ea4",
              "sha": "08553ea46f6c91780e77de5dea184d4a0c25e26a",
              "title": "refactor: #497 Use GraphQL API instead RESTful API for Merge Request",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/4c850e4327a5223a0e4536081ea76620bb90d002",
              "shortId": "4c850e43",
              "sha": "4c850e4327a5223a0e4536081ea76620bb90d002",
              "title": "refactor: #497 Move files to folder",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/9d8bc667a488e8caa740bf32f80a825c7bbedac3",
              "shortId": "9d8bc667",
              "sha": "9d8bc667a488e8caa740bf32f80a825c7bbedac3",
              "title": "test: #497 Should show Merge Request details successful.",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/fe05b1bf36ffac6495d856c6f426a735dc103f84",
              "shortId": "fe05b1bf",
              "sha": "fe05b1bf36ffac6495d856c6f426a735dc103f84",
              "title": "test: #497 Should show Merge Request details successful.",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/0c2bd373e318eed93fb8693dcd09fc4069641abe",
              "shortId": "0c2bd373",
              "sha": "0c2bd373e318eed93fb8693dcd09fc4069641abe",
              "title": "test: #497 Should show Merge Request details successful.",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/05332dae2187f379d5fd704d78435e4d10eb71d7",
              "shortId": "05332dae",
              "sha": "05332dae2187f379d5fd704d78435e4d10eb71d7",
              "title": "chore: #497 Merge code from main.",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/aee396bae215303fbfa732bbf947c5e5142bf8d3",
              "shortId": "aee396ba",
              "sha": "aee396bae215303fbfa732bbf947c5e5142bf8d3",
              "title": "test: #497 Should go in yo MergeRequestPage successful",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/319ee4f8e34fb752ba2fa06a312d1fa9bc254977",
              "shortId": "319ee4f8",
              "sha": "319ee4f8e34fb752ba2fa06a312d1fa9bc254977",
              "title": "refactor: #497 Fix not stub warnings.",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/9fa19d634c339f384b61b5011ff4f4c5832e15b4",
              "shortId": "9fa19d63",
              "sha": "9fa19d634c339f384b61b5011ff4f4c5832e15b4",
              "title": "refactor: #497 Fix not stub warnings.",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/831fed84f7a93786b2564585f57e13c221b01a6a",
              "shortId": "831fed84",
              "sha": "831fed84f7a93786b2564585f57e13c221b01a6a",
              "title": "refactor: #461 Remove no needs test",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/b2d44fd278819f28ff7b50f511dba6f5b54841a9",
              "shortId": "b2d44fd2",
              "sha": "b2d44fd278819f28ff7b50f511dba6f5b54841a9",
              "title": "refactor: #461 Rename",
              "description": ""
            }
          ]
        },
        "author": {"id": "gid://gitlab/User/30192", "name": "Raymond Liao", "username": "raymond-liao", "avatarUrl": null},
        "autoMergeEnabled": false,
        "autoMergeStrategy": null,
        "availableAutoMergeStrategies": [],
        "conflicts": false,
        "mergeError": null,
        "mergeOngoing": false,
        "detailedMergeStatus": "NOT_APPROVED",
        "mergeStatusEnum": "CAN_BE_MERGED",
        "mergeUser": null,
        "mergeWhenPipelineSucceeds": false,
        "mergeable": false,
        "mergeableDiscussionsState": true,
        "mergedAt": null,
        "rebaseCommitSha": null,
        "shouldBeRebased": true,
        "shouldRemoveSourceBranch": null,
        "sourceBranch": "497-feature-kai-fa-ren-yuan-rebase-mr",
        "sourceBranchExists": true,
        "sourceBranchProtected": false,
        "targetBranch": "main",
        "targetBranchExists": true,
        "squash": true,
        "squashOnMerge": true,
        "defaultMergeCommitMessage":
            "Merge branch '497-feature-kai-fa-ren-yuan-rebase-mr' into 'main'\n\nDraft: Resolve \"Feature: 开发人员 Rebase MR\"\n\nSee merge request ultimate-plan/jihu-gitlab-app/jihu-gitlab-app!371",
        "defaultSquashCommitMessage": "Draft: Resolve \"Feature: 开发人员 Rebase MR\"",
        "headPipeline": {
          "id": "gid://gitlab/Ci::Pipeline/947165",
          "iid": "1899",
          "active": false,
          "cancelable": false,
          "complete": true,
          "coverage": 94.2,
          "status": "SUCCESS",
          "ref": "497-feature-kai-fa-ren-yuan-rebase-mr",
          "detailedStatus": {"icon": "status_warning", "label": "passed with warnings"},
          "codeQualityReportSummary": null,
          "codeQualityReports": null,
          "jobs": {
            "nodes": [
              {
                "id": "gid://gitlab/Ci::Build/6395269",
                "name": "build_apk",
                "active": false,
                "status": "FAILED",
                "allowFailure": true,
                "duration": 834,
                "startedAt": "2023-02-15T15:28:43+08:00",
                "finishedAt": "2023-02-15T15:42:37+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2124193",
                  "name": "build",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6395269", "name": "build_apk", "status": "FAILED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6395267", "name": "code_quality", "status": "SUCCESS", "allowFailure": false},
                      {"id": "gid://gitlab/Ci::Build/6395268", "name": "unit_test", "status": "SUCCESS", "allowFailure": false}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/6395268",
                "name": "unit_test",
                "active": false,
                "status": "SUCCESS",
                "allowFailure": false,
                "duration": 556,
                "startedAt": "2023-02-15T15:28:43+08:00",
                "finishedAt": "2023-02-15T15:38:00+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2124193",
                  "name": "build",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6395269", "name": "build_apk", "status": "FAILED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6395267", "name": "code_quality", "status": "SUCCESS", "allowFailure": false},
                      {"id": "gid://gitlab/Ci::Build/6395268", "name": "unit_test", "status": "SUCCESS", "allowFailure": false}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/6395267",
                "name": "code_quality",
                "active": false,
                "status": "SUCCESS",
                "allowFailure": false,
                "duration": 260,
                "startedAt": "2023-02-15T15:28:43+08:00",
                "finishedAt": "2023-02-15T15:33:04+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2124193",
                  "name": "build",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6395269", "name": "build_apk", "status": "FAILED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6395267", "name": "code_quality", "status": "SUCCESS", "allowFailure": false},
                      {"id": "gid://gitlab/Ci::Build/6395268", "name": "unit_test", "status": "SUCCESS", "allowFailure": false}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/6395266",
                "name": "set_mirror",
                "active": false,
                "status": "SUCCESS",
                "allowFailure": true,
                "duration": 15,
                "startedAt": "2023-02-15T15:28:24+08:00",
                "finishedAt": "2023-02-15T15:28:40+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2124192",
                  "name": ".pre",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6395265", "name": "check_internet", "status": "FAILED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6395266", "name": "set_mirror", "status": "SUCCESS", "allowFailure": true}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/6395265",
                "name": "check_internet",
                "active": false,
                "status": "FAILED",
                "allowFailure": true,
                "duration": 17,
                "startedAt": "2023-02-15T15:28:07+08:00",
                "finishedAt": "2023-02-15T15:28:24+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2124192",
                  "name": ".pre",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6395265", "name": "check_internet", "status": "FAILED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6395266", "name": "set_mirror", "status": "SUCCESS", "allowFailure": true}
                    ]
                  }
                }
              }
            ]
          },
          "warnings": true,
          "warningMessages": [],
          "createdAt": "2023-02-15T15:28:04+08:00",
          "finishedAt": "2023-02-15T15:42:37+08:00",
          "committedAt": null,
          "startedAt": "2023-02-15T15:28:07+08:00"
        },
        "userPermissions": {"adminMergeRequest": true, "canMerge": true, "pushToSourceBranch": true, "readMergeRequest": true, "removeSourceBranch": true}
      }
    }
  }
};

Map<String, dynamic> paidMrRebaseGraphQLResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "onlyAllowMergeIfAllStatusChecksPassed": false,
      "mergeRequest": {
        "id": "gid://gitlab/MergeRequest/231586",
        "iid": "371",
        "approved": false,
        "approvalsLeft": 1,
        "approvalsRequired": 2,
        "approvalState": {
          "approvalRulesOverwritten": true,
          "rules": [
            {"id": "gid://gitlab/ApprovalMergeRequestRule/74289", "name": "All Members", "type": "ANY_APPROVER", "approvalsRequired": 1, "eligibleApprovers": []},
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/74290",
              "name": "高级工程师",
              "type": "REGULAR",
              "approvalsRequired": 1,
              "eligibleApprovers": [
                {
                  "id": "gid://gitlab/User/23836",
                  "username": "jojo0",
                  "name": "yajie xue",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png",
                  "webUrl": "https://jihulab.com/jojo0",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/23837",
                  "username": "wanyouzhu",
                  "name": "万友 朱",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                  "webUrl": "https://jihulab.com/wanyouzhu",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29064",
                  "username": "NeilWang",
                  "name": "Neil Wang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png",
                  "webUrl": "https://jihulab.com/NeilWang",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29758",
                  "username": "zhangling",
                  "name": "ling zhang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png",
                  "webUrl": "https://jihulab.com/zhangling",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/1795",
                  "username": "sinkcup",
                  "name": "Zhou YANG",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
                  "webUrl": "https://jihulab.com/sinkcup",
                  "publicEmail": "zhouyang@jihulab.com",
                  "commitEmail": null
                }
              ]
            },
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/74291",
              "name": "Coverage-Check",
              "type": "REPORT_APPROVER",
              "approvalsRequired": 0,
              "eligibleApprovers": [
                {
                  "id": "gid://gitlab/User/23837",
                  "username": "wanyouzhu",
                  "name": "万友 朱",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                  "webUrl": "https://jihulab.com/wanyouzhu",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29064",
                  "username": "NeilWang",
                  "name": "Neil Wang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png",
                  "webUrl": "https://jihulab.com/NeilWang",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29758",
                  "username": "zhangling",
                  "name": "ling zhang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png",
                  "webUrl": "https://jihulab.com/zhangling",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/1795",
                  "username": "sinkcup",
                  "name": "Zhou YANG",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
                  "webUrl": "https://jihulab.com/sinkcup",
                  "publicEmail": "zhouyang@jihulab.com",
                  "commitEmail": null
                }
              ]
            }
          ]
        },
        "detailedMergeStatus": "NOT_APPROVED",
        "headPipeline": {
          "id": "gid://gitlab/Ci::Pipeline/947165",
          "iid": "1899",
          "codeQualityReportSummary": null,
          "codeQualityReports": null,
        },
      }
    }
  }
};

Map<String, dynamic> mrRebaseWithConflictButRespConflictsFalseGraphQLResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "name": "private demo",
      "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / private demo",
      "path": "demo",
      "fullPath": "ultimate-plan/jihu-gitlab-app/demo",
      "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo",
      "allowMergeOnSkippedPipeline": false,
      "onlyAllowMergeIfAllStatusChecksPassed": false,
      "onlyAllowMergeIfAllDiscussionsAreResolved": false,
      "onlyAllowMergeIfPipelineSucceeds": false,
      "removeSourceBranchAfterMerge": false,
      "mergeRequest": {
        "id": "gid://gitlab/MergeRequest/230988",
        "iid": "371",
        "state": "opened",
        "title": "Resolve \"测试跳过 ci\"",
        "description": "#15 #16 #17 Close #28",
        "draft": false,
        "createdAt": "2023-02-14T13:38:00+08:00",
        "rebaseInProgress": false,
        "allowCollaboration": false,
        "approved": true,
        "approvalsLeft": 0,
        "approvalsRequired": 1,
        "approvalState": {
          "approvalRulesOverwritten": true,
          "rules": [
            {"id": "gid://gitlab/ApprovalMergeRequestRule/73798", "name": "All Members", "type": "ANY_APPROVER", "approvalsRequired": 0, "eligibleApprovers": []},
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/73799",
              "name": "License-Check",
              "type": "REPORT_APPROVER",
              "approvalsRequired": 0,
              "eligibleApprovers": [
                {
                  "id": "gid://gitlab/User/38060",
                  "username": "group_88966_bot1",
                  "name": "group-jojo",
                  "state": "active",
                  "avatarUrl": null,
                  "webUrl": "https://jihulab.com/group_88966_bot1",
                  "publicEmail": null,
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/1795",
                  "username": "sinkcup",
                  "name": "Zhou YANG",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
                  "webUrl": "https://jihulab.com/sinkcup",
                  "publicEmail": "zhouyang@jihulab.com",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/23837",
                  "username": "wanyouzhu",
                  "name": "万友 朱",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                  "webUrl": "https://jihulab.com/wanyouzhu",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29355",
                  "username": "perity",
                  "name": "miaolu",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29355/avatar.png",
                  "webUrl": "https://jihulab.com/perity",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/38010",
                  "username": "group_88966_bot",
                  "name": "Issue comment test",
                  "state": "active",
                  "avatarUrl": null,
                  "webUrl": "https://jihulab.com/group_88966_bot",
                  "publicEmail": null,
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29473",
                  "username": "zombie",
                  "name": "鑫 范",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29473/avatar.png",
                  "webUrl": "https://jihulab.com/zombie",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/30192",
                  "username": "raymond-liao",
                  "name": "Raymond Liao",
                  "state": "active",
                  "avatarUrl": null,
                  "webUrl": "https://jihulab.com/raymond-liao",
                  "publicEmail": "",
                  "commitEmail": ""
                },
                {
                  "id": "gid://gitlab/User/29064",
                  "username": "NeilWang",
                  "name": "Neil Wang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png",
                  "webUrl": "https://jihulab.com/NeilWang",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/23836",
                  "username": "jojo0",
                  "name": "yajie xue",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png",
                  "webUrl": "https://jihulab.com/jojo0",
                  "publicEmail": "",
                  "commitEmail": null
                }
              ]
            },
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/73800",
              "name": "Coverage-Check",
              "type": "REPORT_APPROVER",
              "approvalsRequired": 1,
              "eligibleApprovers": [
                {
                  "id": "gid://gitlab/User/38060",
                  "username": "group_88966_bot1",
                  "name": "group-jojo",
                  "state": "active",
                  "avatarUrl": null,
                  "webUrl": "https://jihulab.com/group_88966_bot1",
                  "publicEmail": null,
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/1795",
                  "username": "sinkcup",
                  "name": "Zhou YANG",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
                  "webUrl": "https://jihulab.com/sinkcup",
                  "publicEmail": "zhouyang@jihulab.com",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/23837",
                  "username": "wanyouzhu",
                  "name": "万友 朱",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                  "webUrl": "https://jihulab.com/wanyouzhu",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29355",
                  "username": "perity",
                  "name": "miaolu",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29355/avatar.png",
                  "webUrl": "https://jihulab.com/perity",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/38010",
                  "username": "group_88966_bot",
                  "name": "Issue comment test",
                  "state": "active",
                  "avatarUrl": null,
                  "webUrl": "https://jihulab.com/group_88966_bot",
                  "publicEmail": null,
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29473",
                  "username": "zombie",
                  "name": "鑫 范",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29473/avatar.png",
                  "webUrl": "https://jihulab.com/zombie",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/30192",
                  "username": "raymond-liao",
                  "name": "Raymond Liao",
                  "state": "active",
                  "avatarUrl": null,
                  "webUrl": "https://jihulab.com/raymond-liao",
                  "publicEmail": "",
                  "commitEmail": ""
                },
                {
                  "id": "gid://gitlab/User/29064",
                  "username": "NeilWang",
                  "name": "Neil Wang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png",
                  "webUrl": "https://jihulab.com/NeilWang",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/23836",
                  "username": "jojo0",
                  "name": "yajie xue",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png",
                  "webUrl": "https://jihulab.com/jojo0",
                  "publicEmail": "",
                  "commitEmail": null
                }
              ]
            }
          ]
        },
        "approvedBy": {
          "nodes": [
            {
              "id": "gid://gitlab/User/29758",
              "username": "zhangling",
              "name": "ling zhang",
              "state": "active",
              "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png",
              "webUrl": "https://jihulab.com/zhangling",
              "publicEmail": "",
              "commitEmail": null
            },
            {
              "id": "gid://gitlab/User/23837",
              "username": "wanyouzhu",
              "name": "万友 朱",
              "state": "active",
              "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
              "webUrl": "https://jihulab.com/wanyouzhu",
              "publicEmail": "",
              "commitEmail": null
            }
          ]
        },
        "assignees": {
          "nodes": [
            {"id": "gid://gitlab/User/29758", "name": "ling zhang"}
          ]
        },
        "commitCount": 3,
        "commits": {
          "nodes": [
            {
              "id": "gid://gitlab/CommitPresenter/db14d697af79cedf243cd6dfb782a385b51c43a5",
              "shortId": "db14d697",
              "sha": "db14d697af79cedf243cd6dfb782a385b51c43a5",
              "title": "Merge remote-tracking branch 'origin/main' into 53-ce-shi-tiao-guo-ci",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/339e0ef4538c94d198c9f5b732d611854d3f7b8c",
              "shortId": "339e0ef4",
              "sha": "339e0ef4538c94d198c9f5b732d611854d3f7b8c",
              "title": "mr 28 2",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/ad0cac6376052cedc7b0c1fd8229a3d632c0366e",
              "shortId": "ad0cac63",
              "sha": "ad0cac6376052cedc7b0c1fd8229a3d632c0366e",
              "title": "mr 28",
              "description": ""
            }
          ]
        },
        "author": {"id": "gid://gitlab/User/29758", "name": "ling zhang", "username": "zhangling", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png"},
        "autoMergeEnabled": false,
        "autoMergeStrategy": null,
        "availableAutoMergeStrategies": [],
        "conflicts": false,
        "mergeError": "Rebase failed: Rebase locally, resolve all conflicts, then push the branch.",
        "mergeOngoing": false,
        "detailedMergeStatus": "MERGEABLE",
        "mergeStatusEnum": "CAN_BE_MERGED",
        "mergeUser": null,
        "mergeWhenPipelineSucceeds": false,
        "mergeable": false,
        "mergeableDiscussionsState": true,
        "mergedAt": null,
        "rebaseCommitSha": null,
        "shouldBeRebased": true,
        "shouldRemoveSourceBranch": null,
        "sourceBranch": "53-ce-shi-tiao-guo-ci",
        "sourceBranchExists": true,
        "sourceBranchProtected": false,
        "targetBranch": "main",
        "targetBranchExists": true,
        "squash": false,
        "squashOnMerge": false,
        "defaultMergeCommitMessage": "Merge branch '53-ce-shi-tiao-guo-ci' into 'main'\n\nResolve \"测试跳过 ci\"\n\nCloses #28\n\nSee merge request ultimate-plan/jihu-gitlab-app/demo!28",
        "defaultSquashCommitMessage": "Resolve \"测试跳过 ci\"",
        "headPipeline": null,
        "userPermissions": {"adminMergeRequest": true, "canMerge": true, "pushToSourceBranch": true, "readMergeRequest": true, "removeSourceBranch": true}
      }
    }
  }
};

Map<String, dynamic> mrRebaseWithRespConflictsTrueGraphQLResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "name": "极狐 GitLab APP 代码",
      "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码",
      "path": "jihu-gitlab-app",
      "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
      "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
      "allowMergeOnSkippedPipeline": false,
      "onlyAllowMergeIfAllStatusChecksPassed": false,
      "onlyAllowMergeIfAllDiscussionsAreResolved": true,
      "onlyAllowMergeIfPipelineSucceeds": true,
      "removeSourceBranchAfterMerge": true,
      "mergeRequest": {
        "id": "gid://gitlab/MergeRequest/230799",
        "iid": "371",
        "state": "opened",
        "title": "Resolve \"Feature: 用户更新议题 label\"",
        "description": "",
        "draft": false,
        "createdAt": "2023-02-14T08:42:03+08:00",
        "rebaseInProgress": false,
        "allowCollaboration": false,
        "approved": false,
        "approvalsLeft": 1,
        "approvalsRequired": 2,
        "approvalState": {
          "approvalRulesOverwritten": true,
          "rules": [
            {"id": "gid://gitlab/ApprovalMergeRequestRule/73640", "name": "All Members", "type": "ANY_APPROVER", "approvalsRequired": 1, "eligibleApprovers": []},
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/73641",
              "name": "高级工程师",
              "type": "REGULAR",
              "approvalsRequired": 1,
              "eligibleApprovers": [
                {
                  "id": "gid://gitlab/User/23836",
                  "username": "jojo0",
                  "name": "yajie xue",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png",
                  "webUrl": "https://jihulab.com/jojo0",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/23837",
                  "username": "wanyouzhu",
                  "name": "万友 朱",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                  "webUrl": "https://jihulab.com/wanyouzhu",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29064",
                  "username": "NeilWang",
                  "name": "Neil Wang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png",
                  "webUrl": "https://jihulab.com/NeilWang",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/1795",
                  "username": "sinkcup",
                  "name": "Zhou YANG",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
                  "webUrl": "https://jihulab.com/sinkcup",
                  "publicEmail": "zhouyang@jihulab.com",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29758",
                  "username": "zhangling",
                  "name": "ling zhang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png",
                  "webUrl": "https://jihulab.com/zhangling",
                  "publicEmail": "",
                  "commitEmail": null
                }
              ]
            },
            {
              "id": "gid://gitlab/ApprovalMergeRequestRule/73642",
              "name": "Coverage-Check",
              "type": "REPORT_APPROVER",
              "approvalsRequired": 0,
              "eligibleApprovers": [
                {
                  "id": "gid://gitlab/User/23837",
                  "username": "wanyouzhu",
                  "name": "万友 朱",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png",
                  "webUrl": "https://jihulab.com/wanyouzhu",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29064",
                  "username": "NeilWang",
                  "name": "Neil Wang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png",
                  "webUrl": "https://jihulab.com/NeilWang",
                  "publicEmail": "",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/1795",
                  "username": "sinkcup",
                  "name": "Zhou YANG",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png",
                  "webUrl": "https://jihulab.com/sinkcup",
                  "publicEmail": "zhouyang@jihulab.com",
                  "commitEmail": null
                },
                {
                  "id": "gid://gitlab/User/29758",
                  "username": "zhangling",
                  "name": "ling zhang",
                  "state": "active",
                  "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png",
                  "webUrl": "https://jihulab.com/zhangling",
                  "publicEmail": "",
                  "commitEmail": null
                }
              ]
            }
          ]
        },
        "approvedBy": {"nodes": []},
        "assignees": {
          "nodes": [
            {"id": "gid://gitlab/User/30192", "name": "Raymond Liao"}
          ]
        },
        "commitCount": 22,
        "commits": {
          "nodes": [
            {
              "id": "gid://gitlab/CommitPresenter/b96097ed68f8cbe7f38c19ebbe0dd42fc22cc726",
              "shortId": "b96097ed",
              "sha": "b96097ed68f8cbe7f38c19ebbe0dd42fc22cc726",
              "title": "refactor: #461 Improve text font",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/cf5a509effc35c762edff5513fa64aff3bef953f",
              "shortId": "cf5a509e",
              "sha": "cf5a509effc35c762edff5513fa64aff3bef953f",
              "title": "refactor: #461 Use IssueUpdateModel for issue update",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/aefeca666336cd78891aa570bfc2ce7a4a08acfd",
              "shortId": "aefeca66",
              "sha": "aefeca666336cd78891aa570bfc2ce7a4a08acfd",
              "title": "feat: #461 Should remove labels for issue success",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/9559ec8825450b51b4aac075f702b726bc45f9c3",
              "shortId": "9559ec88",
              "sha": "9559ec8825450b51b4aac075f702b726bc45f9c3",
              "title": "refactor: #461 Move files to folder",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/7220ffbc4b7322f6f4855b90bf1a9d8f17718aea",
              "shortId": "7220ffbc",
              "sha": "7220ffbc4b7322f6f4855b90bf1a9d8f17718aea",
              "title": "feat: #461 Should be able to select a label",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/29984e20a8c05ef0adc47e987cf0606cac21ff16",
              "shortId": "29984e20",
              "sha": "29984e20a8c05ef0adc47e987cf0606cac21ff16",
              "title": "chore: #461 Merge code from main.",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/830d0813bacb766bf437ae701f0a17e5db046c68",
              "shortId": "830d0813",
              "sha": "830d0813bacb766bf437ae701f0a17e5db046c68",
              "title": "chore: #461 Merge code from main.",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/6c5409ecd4c9c0b5e06f58556ff8cb0c97f034bb",
              "shortId": "6c5409ec",
              "sha": "6c5409ecd4c9c0b5e06f58556ff8cb0c97f034bb",
              "title": "chore: #461 Remove no needs icon",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/ecbfa4ff541ff9b78dd34006e0950d7841df6ebf",
              "shortId": "ecbfa4ff",
              "sha": "ecbfa4ff541ff9b78dd34006e0950d7841df6ebf",
              "title": "feat: #461 Should display Select labels page after Issue Labels edit button tapped.",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/d3c7c8494bf5cd08e75bdc2520cd9ef07050804f",
              "shortId": "d3c7c849",
              "sha": "d3c7c8494bf5cd08e75bdc2520cd9ef07050804f",
              "title": "feat: #461 Shows edit button for Issue Labels box",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/d214928c27f8880282cfae1fa0ef811e8245200a",
              "shortId": "d214928c",
              "sha": "d214928c27f8880282cfae1fa0ef811e8245200a",
              "title": "test: #461 improve tests",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/8fb6aa1b38ee20770e2c16dac7672132895ffb1f",
              "shortId": "8fb6aa1b",
              "sha": "8fb6aa1b38ee20770e2c16dac7672132895ffb1f",
              "title": "chore: #461 Merge code from main.",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/393d49ba72a78701ce6f0fcda86cfc17a49c2fe4",
              "shortId": "393d49ba",
              "sha": "393d49ba72a78701ce6f0fcda86cfc17a49c2fe4",
              "title": "chore: #461 Merge code from main.",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/94afcc1f8a650600d28f61878aed1c48fe9934e6",
              "shortId": "94afcc1f",
              "sha": "94afcc1f8a650600d28f61878aed1c48fe9934e6",
              "title": "test: #461 improve tests",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/37316c7cc055a8cac16469af91d8fc32b43a96d7",
              "shortId": "37316c7c",
              "sha": "37316c7cc055a8cac16469af91d8fc32b43a96d7",
              "title": "test: #461 test_issue_details_page.dart",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/53d68bb8737ed32affca017981d94904e3fe6c55",
              "shortId": "53d68bb8",
              "sha": "53d68bb8737ed32affca017981d94904e3fe6c55",
              "title": "test: #461 test_issue_details_page.dart",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/faf688b09572de01a34cd774e9ae09d0c4848d53",
              "shortId": "faf688b0",
              "sha": "faf688b09572de01a34cd774e9ae09d0c4848d53",
              "title": "refactor: #461 Move codes",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/576f04cde09f61386661ec4b7566f375e6c9fd91",
              "shortId": "576f04cd",
              "sha": "576f04cde09f61386661ec4b7566f375e6c9fd91",
              "title": "refactor: #461 Remove IssueInfo.empty()",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/d9025d90d482f024bc7bee2186fe1865e2f62447",
              "shortId": "d9025d90",
              "sha": "d9025d90d482f024bc7bee2186fe1865e2f62447",
              "title": "refactor: #461 Improve tests",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/becb640454ba5c972f87587573bc96cad55a0aa8",
              "shortId": "becb6404",
              "sha": "becb640454ba5c972f87587573bc96cad55a0aa8",
              "title": "refactor: #461 Improve tests",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/1857551934da892b908cfe5e7995676663452bf5",
              "shortId": "18575519",
              "sha": "1857551934da892b908cfe5e7995676663452bf5",
              "title": "refactor: #461 Rename folder",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/e224f01d5d5fe85c3d8e135534182411efc275d9",
              "shortId": "e224f01d",
              "sha": "e224f01d5d5fe85c3d8e135534182411efc275d9",
              "title": "refactor: #461 Rename folder",
              "description": ""
            }
          ]
        },
        "author": {"id": "gid://gitlab/User/30192", "name": "Raymond Liao", "username": "raymond-liao", "avatarUrl": null},
        "autoMergeEnabled": false,
        "autoMergeStrategy": null,
        "availableAutoMergeStrategies": [],
        "conflicts": true,
        "mergeError": null,
        "mergeOngoing": false,
        "detailedMergeStatus": "NOT_APPROVED",
        "mergeStatusEnum": "CANNOT_BE_MERGED",
        "mergeUser": null,
        "mergeWhenPipelineSucceeds": false,
        "mergeable": false,
        "mergeableDiscussionsState": true,
        "mergedAt": null,
        "rebaseCommitSha": null,
        "shouldBeRebased": true,
        "shouldRemoveSourceBranch": null,
        "sourceBranch": "461-feature-yong-hu-geng-xin-yi-ti-label",
        "sourceBranchExists": true,
        "sourceBranchProtected": false,
        "targetBranch": "main",
        "targetBranchExists": true,
        "squash": true,
        "squashOnMerge": true,
        "defaultMergeCommitMessage":
            "Merge branch '461-feature-yong-hu-geng-xin-yi-ti-label' into 'main'\n\nResolve \"Feature: 用户更新议题 label\"\n\nSee merge request ultimate-plan/jihu-gitlab-app/jihu-gitlab-app!362",
        "defaultSquashCommitMessage": "Resolve \"Feature: 用户更新议题 label\"",
        "headPipeline": {
          "id": "gid://gitlab/Ci::Pipeline/942987",
          "iid": "1861",
          "active": false,
          "cancelable": false,
          "complete": true,
          "coverage": 94.2,
          "status": "SUCCESS",
          "ref": "461-feature-yong-hu-geng-xin-yi-ti-label",
          "detailedStatus": {"icon": "status_warning", "label": "passed with warnings"},
          "codeQualityReportSummary": null,
          "codeQualityReports": null,
          "jobs": {
            "nodes": [
              {
                "id": "gid://gitlab/Ci::Build/6362590",
                "name": "build_apk",
                "active": false,
                "status": "SUCCESS",
                "allowFailure": true,
                "duration": 447,
                "startedAt": "2023-02-13T15:50:02+08:00",
                "finishedAt": "2023-02-13T15:57:30+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2112009",
                  "name": "build",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6362590", "name": "build_apk", "status": "SUCCESS", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6362588", "name": "code_quality", "status": "SUCCESS", "allowFailure": false},
                      {"id": "gid://gitlab/Ci::Build/6362589", "name": "unit_test", "status": "SUCCESS", "allowFailure": false}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/6362589",
                "name": "unit_test",
                "active": false,
                "status": "SUCCESS",
                "allowFailure": false,
                "duration": 552,
                "startedAt": "2023-02-13T15:50:02+08:00",
                "finishedAt": "2023-02-13T15:59:14+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2112009",
                  "name": "build",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6362590", "name": "build_apk", "status": "SUCCESS", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6362588", "name": "code_quality", "status": "SUCCESS", "allowFailure": false},
                      {"id": "gid://gitlab/Ci::Build/6362589", "name": "unit_test", "status": "SUCCESS", "allowFailure": false}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/6362588",
                "name": "code_quality",
                "active": false,
                "status": "SUCCESS",
                "allowFailure": false,
                "duration": 259,
                "startedAt": "2023-02-13T15:50:02+08:00",
                "finishedAt": "2023-02-13T15:54:21+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2112009",
                  "name": "build",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6362590", "name": "build_apk", "status": "SUCCESS", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6362588", "name": "code_quality", "status": "SUCCESS", "allowFailure": false},
                      {"id": "gid://gitlab/Ci::Build/6362589", "name": "unit_test", "status": "SUCCESS", "allowFailure": false}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/6362587",
                "name": "set_mirror",
                "active": false,
                "status": "SUCCESS",
                "allowFailure": true,
                "duration": 16,
                "startedAt": "2023-02-13T15:49:44+08:00",
                "finishedAt": "2023-02-13T15:50:00+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2112008",
                  "name": ".pre",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6362586", "name": "check_internet", "status": "FAILED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6362587", "name": "set_mirror", "status": "SUCCESS", "allowFailure": true}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/6362586",
                "name": "check_internet",
                "active": false,
                "status": "FAILED",
                "allowFailure": true,
                "duration": 17,
                "startedAt": "2023-02-13T15:49:26+08:00",
                "finishedAt": "2023-02-13T15:49:43+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2112008",
                  "name": ".pre",
                  "status": "success",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6362586", "name": "check_internet", "status": "FAILED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6362587", "name": "set_mirror", "status": "SUCCESS", "allowFailure": true}
                    ]
                  }
                }
              }
            ]
          },
          "warnings": true,
          "warningMessages": [],
          "createdAt": "2023-02-13T15:49:25+08:00",
          "finishedAt": "2023-02-13T15:59:15+08:00",
          "committedAt": null,
          "startedAt": "2023-02-13T15:49:26+08:00"
        },
        "userPermissions": {"adminMergeRequest": true, "canMerge": true, "pushToSourceBranch": true, "readMergeRequest": true, "removeSourceBranch": true}
      }
    }
  }
};

Map<String, dynamic> mrRebaseSucceedGraphQLResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "name": "极狐 GitLab APP 代码",
      "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码",
      "path": "jihu-gitlab-app",
      "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
      "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
      "allowMergeOnSkippedPipeline": false,
      "onlyAllowMergeIfAllStatusChecksPassed": false,
      "onlyAllowMergeIfAllDiscussionsAreResolved": true,
      "onlyAllowMergeIfPipelineSucceeds": true,
      "removeSourceBranchAfterMerge": true,
      "mergeRequest": {
        "id": "gid://gitlab/MergeRequest/231076",
        "iid": "365",
        "state": "opened",
        "title": "feat: #499 开发人员查看 Merge blocked",
        "description": "",
        "draft": false,
        "createdAt": "2023-02-14T15:34:08+08:00",
        "rebaseInProgress": false,
        "allowCollaboration": false,
        "commitCount": 16,
        "commits": {
          "nodes": [
            {
              "id": "gid://gitlab/CommitPresenter/ddfb6c2f53ad1e74ca8ccd0e1cf376cebceca586",
              "shortId": "ddfb6c2f",
              "sha": "ddfb6c2f53ad1e74ca8ccd0e1cf376cebceca586",
              "title": "chore: #499 Merge remote-tracking branch 'origin/main' into...",
              "description": "chore: #499 Merge remote-tracking branch 'origin/main' into 499-feature-kai-fa-ren-yuan-cha-kan-merge-blocked\n"
            },
            {
              "id": "gid://gitlab/CommitPresenter/6c6bb3849ff325f421fb08fe4999a246e527c424",
              "shortId": "6c6bb384",
              "sha": "6c6bb3849ff325f421fb08fe4999a246e527c424",
              "title": "refactor: #499 Remove duplicate parameters",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/169f6aa337dc279828a5efd2ef7f230fedaeace0",
              "shortId": "169f6aa3",
              "sha": "169f6aa337dc279828a5efd2ef7f230fedaeace0",
              "title": "fix: #499 The description of test",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/95818314fd147d9ac8e94a3778a4a868b6a41118",
              "shortId": "95818314",
              "sha": "95818314fd147d9ac8e94a3778a4a868b6a41118",
              "title": "chore: #499 Merge remote-tracking branch 'origin/main' into...",
              "description": "chore: #499 Merge remote-tracking branch 'origin/main' into 499-feature-kai-fa-ren-yuan-cha-kan-merge-blocked\n"
            },
            {
              "id": "gid://gitlab/CommitPresenter/2b05472e9029a769ba2288e3495668bbdd5f254f",
              "shortId": "2b05472e",
              "sha": "2b05472e9029a769ba2288e3495668bbdd5f254f",
              "title": "feat: #499 Modify the translation",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/48ee672410082da4b6f8af0caa2a5443b84d09e4",
              "shortId": "48ee6724",
              "sha": "48ee672410082da4b6f8af0caa2a5443b84d09e4",
              "title": "feat: #499 测试 Merge blocked 在主题未解决状态下的展示",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/3690b22f0b6574d543b41f07abd273677f8bce95",
              "shortId": "3690b22f",
              "sha": "3690b22f0b6574d543b41f07abd273677f8bce95",
              "title": "feat: #499 MR 没有冲突时才显示草稿状态",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/ab626ce7bd9acf9ca3d35e9672fd35e4340bd031",
              "shortId": "ab626ce7",
              "sha": "ab626ce7bd9acf9ca3d35e9672fd35e4340bd031",
              "title": "fix: #499 Error test",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/271bcd148fe6fef4ca64f92d57001fc65256c738",
              "shortId": "271bcd14",
              "sha": "271bcd148fe6fef4ca64f92d57001fc65256c738",
              "title": "chore: #499 Merge remote-tracking branch 'origin/main' into...",
              "description": "chore: #499 Merge remote-tracking branch 'origin/main' into 499-feature-kai-fa-ren-yuan-cha-kan-merge-blocked\n"
            },
            {
              "id": "gid://gitlab/CommitPresenter/70f6a1d2bbbddfe590403bb60e67387a73fa6791",
              "shortId": "70f6a1d2",
              "sha": "70f6a1d2bbbddfe590403bb60e67387a73fa6791",
              "title": "feat: #499 用户查看 Merge blocked",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/fef4ee1a1183183f2775ca5a08fa46a3bd87e77f",
              "shortId": "fef4ee1a",
              "sha": "fef4ee1a1183183f2775ca5a08fa46a3bd87e77f",
              "title": "refactor: #499 Code style",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/5c32504b499d9f1bb332ba426620c8145182941a",
              "shortId": "5c32504b",
              "sha": "5c32504b499d9f1bb332ba426620c8145182941a",
              "title": "refactor: #499 Rename image",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/05fc642699f698a8b34e69485bbfb0ddbcfa5fa5",
              "shortId": "05fc6426",
              "sha": "05fc642699f698a8b34e69485bbfb0ddbcfa5fa5",
              "title": "refactor: #499 Remove unused translation",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/5ca18646b03a9822399c940c1a774ee38b72b93c",
              "shortId": "5ca18646",
              "sha": "5ca18646b03a9822399c940c1a774ee38b72b93c",
              "title": "feat: #499 Display the merge blocked with the status of CI_MUST_PASS and NOT_APPROVED",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/3f6e4b262d54d6cde2ddb4d63541e463e4f74ffc",
              "shortId": "3f6e4b26",
              "sha": "3f6e4b262d54d6cde2ddb4d63541e463e4f74ffc",
              "title": "feat: #499 Display the merge blocked with the status of EXTERNAL_STATUS_CHECKS",
              "description": ""
            },
            {
              "id": "gid://gitlab/CommitPresenter/e7a34fbbd624a492039f7ee6b3f6b41997b2358b",
              "shortId": "e7a34fbb",
              "sha": "e7a34fbbd624a492039f7ee6b3f6b41997b2358b",
              "title": "refactor: #499 Rename image",
              "description": ""
            }
          ]
        },
        "approvalState": {
          "approvalRulesOverwritten": true,
          "invalidApproversRules": [],
          "rules": [
            {"id": "gid://gitlab/ApprovalMergeRequestRule/73873", "name": "All Members"},
            {"id": "gid://gitlab/ApprovalMergeRequestRule/73874", "name": "高级工程师"},
            {"id": "gid://gitlab/ApprovalMergeRequestRule/73875", "name": "Coverage-Check"}
          ]
        },
        "approved": false,
        "approvedBy": {"nodes": []},
        "assignees": {
          "nodes": [
            {"id": "gid://gitlab/User/29758", "name": "ling zhang"}
          ]
        },
        "author": {"id": "gid://gitlab/User/29758", "name": "ling zhang", "username": "zhangling", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png"},
        "autoMergeEnabled": false,
        "autoMergeStrategy": null,
        "availableAutoMergeStrategies": ["merge_when_pipeline_succeeds"],
        "conflicts": false,
        "mergeError": null,
        "mergeOngoing": false,
        "detailedMergeStatus": "NOT_APPROVED",
        "mergeStatusEnum": "CAN_BE_MERGED",
        "mergeUser": null,
        "mergeWhenPipelineSucceeds": false,
        "mergeable": false,
        "mergeableDiscussionsState": true,
        "mergedAt": null,
        "rebaseCommitSha": "ddfb6c2f53ad1e74ca8ccd0e1cf376cebceca586",
        "shouldBeRebased": false,
        "shouldRemoveSourceBranch": null,
        "sourceBranch": "499-feature-kai-fa-ren-yuan-cha-kan-merge-blocked",
        "sourceBranchExists": true,
        "sourceBranchProtected": false,
        "targetBranch": "main",
        "targetBranchExists": true,
        "squash": true,
        "squashOnMerge": true,
        "defaultMergeCommitMessage":
            "Merge branch '499-feature-kai-fa-ren-yuan-cha-kan-merge-blocked' into 'main'\n\nfeat: #499 开发人员查看 Merge blocked\n\nCloses #499\n\nSee merge request ultimate-plan/jihu-gitlab-app/jihu-gitlab-app!365",
        "defaultSquashCommitMessage": "feat: #499 开发人员查看 Merge blocked",
        "headPipeline": {
          "id": "gid://gitlab/Ci::Pipeline/948597",
          "iid": "1915",
          "active": true,
          "cancelable": true,
          "complete": false,
          "coverage": null,
          "status": "RUNNING",
          "ref": "499-feature-kai-fa-ren-yuan-cha-kan-merge-blocked",
          "detailedStatus": {"icon": "status_running", "label": "running"},
          "codeQualityReportSummary": null,
          "codeQualityReports": null,
          "jobs": {
            "nodes": [
              {
                "id": "gid://gitlab/Ci::Build/6409986",
                "name": "build_apk",
                "active": false,
                "status": "CREATED",
                "allowFailure": true,
                "duration": null,
                "startedAt": null,
                "finishedAt": null,
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2128045",
                  "name": "build",
                  "status": "created",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6409986", "name": "build_apk", "status": "CREATED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6409984", "name": "code_quality", "status": "CREATED", "allowFailure": false},
                      {"id": "gid://gitlab/Ci::Build/6409985", "name": "unit_test", "status": "CREATED", "allowFailure": false}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/6409985",
                "name": "unit_test",
                "active": false,
                "status": "CREATED",
                "allowFailure": false,
                "duration": null,
                "startedAt": null,
                "finishedAt": null,
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2128045",
                  "name": "build",
                  "status": "created",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6409986", "name": "build_apk", "status": "CREATED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6409984", "name": "code_quality", "status": "CREATED", "allowFailure": false},
                      {"id": "gid://gitlab/Ci::Build/6409985", "name": "unit_test", "status": "CREATED", "allowFailure": false}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/6409984",
                "name": "code_quality",
                "active": false,
                "status": "CREATED",
                "allowFailure": false,
                "duration": null,
                "startedAt": null,
                "finishedAt": null,
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2128045",
                  "name": "build",
                  "status": "created",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6409986", "name": "build_apk", "status": "CREATED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6409984", "name": "code_quality", "status": "CREATED", "allowFailure": false},
                      {"id": "gid://gitlab/Ci::Build/6409985", "name": "unit_test", "status": "CREATED", "allowFailure": false}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/6409983",
                "name": "set_mirror",
                "active": true,
                "status": "PENDING",
                "allowFailure": true,
                "duration": null,
                "startedAt": null,
                "finishedAt": null,
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2128044",
                  "name": ".pre",
                  "status": "pending",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6409982", "name": "check_internet", "status": "FAILED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6409983", "name": "set_mirror", "status": "PENDING", "allowFailure": true}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/6409982",
                "name": "check_internet",
                "active": false,
                "status": "FAILED",
                "allowFailure": true,
                "duration": 15,
                "startedAt": "2023-02-16T09:31:24+08:00",
                "finishedAt": "2023-02-16T09:31:40+08:00",
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2128044",
                  "name": ".pre",
                  "status": "pending",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/6409982", "name": "check_internet", "status": "FAILED", "allowFailure": true},
                      {"id": "gid://gitlab/Ci::Build/6409983", "name": "set_mirror", "status": "PENDING", "allowFailure": true}
                    ]
                  }
                }
              }
            ]
          },
          "warnings": true,
          "warningMessages": [],
          "createdAt": "2023-02-16T09:31:22+08:00",
          "finishedAt": null,
          "committedAt": null,
          "startedAt": "2023-02-16T09:31:25+08:00"
        },
        "userPermissions": {"adminMergeRequest": true, "canMerge": true, "pushToSourceBranch": true, "readMergeRequest": true, "removeSourceBranch": true}
      }
    }
  }
};

Map<String, dynamic> approvalResponse = {
  "id": 227910,
  "iid": 18,
  "project_id": 72936,
  "title": "Resolve \"Test optional approvals\"",
  "description": "Closes #47",
  "state": "opened",
  "created_at": "2023-02-08T14:24:54.580+08:00",
  "updated_at": "2023-02-08T15:37:54.653+08:00",
  "merge_status": "can_be_merged",
  "approved": true,
  "approvals_required": 0,
  "approvals_left": 0,
  "require_password_to_approve": false,
  "approved_by": [],
  "suggested_approvers": [
    {
      "id": 1795,
      "username": "sinkcup",
      "name": "Zhou YANG",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/1795/avatar.png",
      "web_url": "https://jihulab.com/sinkcup"
    },
    {
      "id": 29355,
      "username": "perity",
      "name": "miaolu",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29355/avatar.png",
      "web_url": "https://jihulab.com/perity"
    },
    {
      "id": 29064,
      "username": "NeilWang",
      "name": "Neil Wang",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29064/avatar.png",
      "web_url": "https://jihulab.com/NeilWang"
    },
    {
      "id": 30192,
      "username": "raymond-liao",
      "name": "Raymond Liao",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/30192/avatar.png",
      "web_url": "https://jihulab.com/raymond-liao"
    },
    {"id": 29473, "username": "zombie", "name": "鑫 范", "state": "active", "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29473/avatar.png", "web_url": "https://jihulab.com/zombie"},
    {
      "id": 23836,
      "username": "jojo0",
      "name": "yajie xue",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/23836/avatar.png",
      "web_url": "https://jihulab.com/jojo0"
    },
    {
      "id": 23837,
      "username": "wanyouzhu",
      "name": "万友 朱",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/23837/avatar.png",
      "web_url": "https://jihulab.com/wanyouzhu"
    }
  ],
  "approvers": [],
  "approver_groups": [],
  "user_has_approved": false,
  "user_can_approve": true,
  "approval_rules_left": [],
  "has_approval_rules": true,
  "merge_request_approvers_available": true,
  "multiple_approval_rules_available": true,
  "invalid_approvers_rules": []
};

Map<String, dynamic> approvedApprovalResponse = {
  "id": 230988,
  "iid": 28,
  "project_id": 72936,
  "title": "Resolve \"测试跳过 ci\"",
  "description": "#15 #16 #17 Close #28",
  "state": "opened",
  "created_at": "2023-02-14T13:38:00.075+08:00",
  "updated_at": "2023-02-20T11:28:56.429+08:00",
  "merge_status": "can_be_merged",
  "approved": true,
  "approvals_required": 1,
  "approvals_left": 0,
  "require_password_to_approve": false,
  "approved_by": [
    {
      "user": {
        "id": 29758,
        "username": "zhangling",
        "name": "ling zhang",
        "state": "active",
        "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29758/avatar.png",
        "web_url": "https://jihulab.com/zhangling"
      }
    },
    {
      "user": {
        "id": 23837,
        "username": "wanyouzhu",
        "name": "万友 朱",
        "state": "active",
        "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/23837/avatar.png",
        "web_url": "https://jihulab.com/wanyouzhu"
      }
    }
  ],
  "suggested_approvers": [
    {
      "id": 1795,
      "username": "sinkcup",
      "name": "Zhou YANG",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/1795/avatar.png",
      "web_url": "https://jihulab.com/sinkcup"
    },
    {
      "id": 30192,
      "username": "raymond-liao",
      "name": "Raymond Liao",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/30192/avatar.png",
      "web_url": "https://jihulab.com/raymond-liao"
    },
    {"id": 38060, "username": "group_88966_bot1", "name": "group-jojo", "state": "active", "avatar_url": null, "web_url": "https://jihulab.com/group_88966_bot1"},
    {
      "id": 29064,
      "username": "NeilWang",
      "name": "Neil Wang",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29064/avatar.png",
      "web_url": "https://jihulab.com/NeilWang"
    },
    {
      "id": 23836,
      "username": "jojo0",
      "name": "yajie xue",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/23836/avatar.png",
      "web_url": "https://jihulab.com/jojo0"
    },
    {
      "id": 29355,
      "username": "perity",
      "name": "miaolu",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29355/avatar.png",
      "web_url": "https://jihulab.com/perity"
    },
    {"id": 38010, "username": "group_88966_bot", "name": "Issue comment test", "state": "active", "avatar_url": null, "web_url": "https://jihulab.com/group_88966_bot"},
    {"id": 29473, "username": "zombie", "name": "鑫 范", "state": "active", "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29473/avatar.png", "web_url": "https://jihulab.com/zombie"}
  ],
  "approvers": [],
  "approver_groups": [],
  "user_has_approved": false,
  "user_can_approve": true,
  "approval_rules_left": [],
  "has_approval_rules": true,
  "merge_request_approvers_available": true,
  "multiple_approval_rules_available": true,
  "invalid_approvers_rules": []
};

List<Map<String, dynamic>> mrDiffsResponse = [
  {
    "diff":
        "@@ -32,6 +32,10 @@ class ConnectionProvider extends ChangeNotifier {\n \n   static Future<String> get mergeRequestWebUrl async => await LocalStorage.get(LocalStorageKeys.currentMergeRequestWebUrlKey, '');\n \n+  Connection? get jiHuConnection {\n+    return _connections.jiHuConnection;\n+  }\n+\n   Future<Map<String, dynamic>> addUserByToken(Token token, String baseUrl) async {\n     try {\n       User user = await _fetchUserByToken(baseUrl, token);\n@@ -155,6 +159,13 @@ class ConnectionProvider extends ChangeNotifier {\n     notifyListeners();\n   }\n \n+  @visibleForTesting\n+  void injectConnectionForTest(Connection connection) {\n+    _connections.add(connection);\n+    changeAccount(connection);\n+    notifyListeners();\n+  }\n+\n   @visibleForTesting\n   fullReset() {\n     clearActive(notify: false);\n",
    "new_path": "lib/core/user_provider/user_provider.dart",
    "old_path": "lib/core/user_provider/user_provider.dart",
    "a_mode": "100644",
    "b_mode": "100644",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  },
  {
    "diff":
        "@@ -13,6 +13,11 @@ class Connections {\n \n   List<Connection> get get => _connections;\n \n+  Connection? get jiHuConnection {\n+    var matches = _connections.where((o) => o.baseUrl.isJiHu() && o.personalAccessToken == null);\n+    return matches.isEmpty ? null : matches.first;\n+  }\n+\n   void add(Connection connection) {\n     if (_connections.any((o) => o.personalAccessToken != null) && connection.personalAccessToken != null) throw LoginTypeSameException();\n     if (connection.baseUrl.isJiHu() && connection.personalAccessToken == null && _connections.any((o) => o.baseUrl.isJiHu() && o.personalAccessToken == null)) throw LoginTypeSameException();\n",
    "new_path": "lib/core/user_provider/users.dart",
    "old_path": "lib/core/user_provider/users.dart",
    "a_mode": "100644",
    "b_mode": "100644",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  },
  {
    "diff":
        "@@ -34,9 +34,20 @@ void main() {\n     await ConnectionProvider().clearActive();\n   });\n \n+  test('Should not get jihulab.com connection from connections when there has no jihu connection', () async {\n+    ConnectionProvider connectionProvider = ConnectionProvider();\n+    expect(connectionProvider.jiHuConnection, null);\n+  });\n+\n+  test('Should not get jihulab.com connection from connections', () async {\n+    ConnectionProvider().reset(Tester.user());\n+    ConnectionProvider connectionProvider = ConnectionProvider();\n+    expect(connectionProvider.jiHuConnection!.userInfo.id, 9527);\n+  });\n+\n   test('Should create User Provider object with singleton instance', () {\n-    ConnectionProvider userProvider = ConnectionProvider();\n-    expect(userProvider, ConnectionProvider());\n+    ConnectionProvider connectionProvider = ConnectionProvider();\n+    expect(connectionProvider, ConnectionProvider());\n   });\n \n   test('Should User Provider fetch user info be succeed when response correct user info', () async {\n",
    "new_path": "test/core/user_provider_test.dart",
    "old_path": "test/core/user_provider_test.dart",
    "a_mode": "100644",
    "b_mode": "100644",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  },
  {
    "diff":
        "@@ -0,0 +1,179 @@\n+import 'package:flutter/material.dart';\n+import 'package:flutter_test/flutter_test.dart';\n+import 'package:jihu_gitlab_app/core/local_storage.dart';\n+import 'package:jihu_gitlab_app/core/net/response.dart';\n+import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';\n+import 'package:jihu_gitlab_app/core/widgets/account/account_page.dart';\n+import 'package:jihu_gitlab_app/l10n/app_localizations.dart';\n+import 'package:jihu_gitlab_app/modules/auth/auth.dart';\n+import 'package:mockito/mockito.dart';\n+import 'package:provider/provider.dart';\n+import 'package:shared_preferences/shared_preferences.dart';\n+\n+import '../../core/net/http_request_test.mocks.dart';\n+import '../../mocker/tester.dart';\n+import '../../test_data/user.dart';\n+\n+void main() {\n+  var client = MockHttpClient();\n+  setUp(() async {\n+    JiHuLocalizations.init();\n+    SharedPreferences.setMockInitialValues(<String, Object>{});\n+    await LocalStorage.init();\n+    LocalStorage.save('privacy-policy-agreed', true);\n+    ConnectionProvider().fullReset();\n+    when(client.getWithHeader<Map<String, dynamic>>(\"https://jihulab.com/api/v4/user\", any)).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));\n+    when(client.getWithHeader<Map<String, dynamic>>(\"https://example.com/api/v4/user\", any)).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));\n+  });\n+\n+  testWidgets('Should display account page as expect', (tester) async {\n+    await tester.pumpWidget(MultiProvider(\n+      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],\n+      child: const MaterialApp(\n+        home: Scaffold(body: AccountPage()),\n+      ),\n+    ));\n+    await tester.pumpAndSettle();\n+    expect(find.text('Add account with'), findsOneWidget);\n+    expect(find.text('Self-Managed'), findsOneWidget);\n+    await tester.tap(find.byIcon(Icons.arrow_drop_down));\n+    await tester.pumpAndSettle();\n+    expect(find.text('Self-Managed'), findsNWidgets(2));\n+    expect(find.text('jihulab.com'), findsOneWidget);\n+    expect(find.text('gitlab.com'), findsOneWidget);\n+    await tester.tap(find.text('jihulab.com'));\n+    await tester.pumpAndSettle();\n+    expect(find.text('jihulab.com'), findsOneWidget);\n+    await tester.tap(find.byIcon(Icons.arrow_drop_down));\n+    await tester.pumpAndSettle();\n+    expect(find.text('jihulab.com'), findsNWidgets(2));\n+    await tester.tap(find.text('gitlab.com'));\n+    await tester.pumpAndSettle();\n+    expect(find.text('gitlab.com'), findsOneWidget);\n+    await tester.tap(find.byIcon(Icons.arrow_drop_down));\n+    await tester.pumpAndSettle();\n+    expect(find.text('gitlab.com'), findsNWidgets(2));\n+  });\n+\n+  testWidgets('Should display account page when connected jihulab.com', (tester) async {\n+    ConnectionProvider().reset(Tester.user());\n+    await tester.pumpWidget(MultiProvider(\n+      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],\n+      child: const MaterialApp(\n+        home: Scaffold(body: AccountPage()),\n+      ),\n+    ));\n+    await tester.pumpAndSettle();\n+    expect(find.text('tester @tester'), findsOneWidget);\n+    await tester.tap(find.byIcon(Icons.arrow_drop_down));\n+    await tester.pumpAndSettle();\n+    expect(find.text('Self-Managed'), findsNWidgets(2));\n+    expect(find.text('jihulab.com'), findsNWidgets(2));\n+    expect(find.text('Already used'), findsOneWidget);\n+    expect(find.text('gitlab.com'), findsOneWidget);\n+    await tester.tap(find.text('Already used'));\n+    await tester.pumpAndSettle(const Duration(seconds: 2));\n+    expect(find.text('Self-Managed'), findsOneWidget);\n+  });\n+\n+  testWidgets('Should display account page when connected self-managed', (tester) async {\n+    ConnectionProvider().reset(Tester.selfManagedUser());\n+    await tester.pumpWidget(MultiProvider(\n+      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],\n+      child: const MaterialApp(\n+        home: Scaffold(body: AccountPage()),\n+      ),\n+    ));\n+    await tester.pumpAndSettle();\n+    expect(find.text('tester @tester'), findsOneWidget);\n+    await tester.tap(find.byIcon(Icons.arrow_drop_down));\n+    await tester.pumpAndSettle();\n+    expect(find.text('Self-Managed'), findsNWidgets(3));\n+    expect(find.text('jihulab.com'), findsOneWidget);\n+    expect(find.text('Already used'), findsOneWidget);\n+    expect(find.text('gitlab.com'), findsOneWidget);\n+    await tester.tap(find.text('Already used'));\n+    await tester.pumpAndSettle(const Duration(seconds: 2));\n+    expect(find.text('Self-Managed'), findsNWidgets(2));\n+  });\n+\n+  testWidgets('Should not connect account when connected self-managed', (tester) async {\n+    ConnectionProvider().reset(Tester.selfManagedUser());\n+    await tester.pumpWidget(MultiProvider(\n+      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],\n+      child: const MaterialApp(\n+        home: Scaffold(body: AccountPage()),\n+      ),\n+    ));\n+    await tester.pumpAndSettle();\n+    expect(find.text('tester @tester'), findsOneWidget);\n+    await tester.tap(find.text('Add account with'));\n+    await tester.pumpAndSettle(const Duration(seconds: 2));\n+    expect(find.text('tester @tester'), findsOneWidget);\n+    expect(find.text('Add account with'), findsOneWidget);\n+  });\n+\n+  testWidgets('Should disconnect account', (tester) async {\n+    ConnectionProvider().reset(Tester.gitLabUser());\n+    ConnectionProvider().injectConnectionForTest(Tester.user());\n+    await tester.pumpWidget(MultiProvider(\n+      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],\n+      child: const MaterialApp(\n+        home: Scaffold(body: AccountPage()),\n+      ),\n+    ));\n+    await tester.pumpAndSettle();\n+    await tester.drag(find.text('jihulab.com'), const Offset(-300, 0));\n+    await tester.pump();\n+    expect(find.byIcon(Icons.exit_to_app), findsOneWidget);\n+    await tester.tap(find.byIcon(Icons.exit_to_app));\n+    await tester.pumpAndSettle();\n+    expect(find.text('jihulab.com'), findsNothing);\n+    await tester.drag(find.text('gitlab.com'), const Offset(-300, 0));\n+    await tester.pump();\n+    expect(find.byIcon(Icons.exit_to_app), findsOneWidget);\n+    await tester.tap(find.byIcon(Icons.exit_to_app));\n+    await tester.pumpAndSettle();\n+    expect(find.text('gitlab.com'), findsNothing);\n+  });\n+\n+  testWidgets('Should tap to checkout account', (tester) async {\n+    ConnectionProvider().reset(Tester.gitLabUser());\n+    ConnectionProvider().injectConnectionForTest(Tester.user());\n+    await tester.pumpWidget(MultiProvider(\n+      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],\n+      child: const MaterialApp(\n+        home: Scaffold(body: AccountPage()),\n+      ),\n+    ));\n+    await tester.pumpAndSettle();\n+    expect(find.text('tester @tester'), findsNWidgets(2));\n+    await tester.tap(find.text('gitlab.com'));\n+    await tester.pumpAndSettle();\n+    expect(ConnectionProvider.currentBaseUrl.get, 'https://gitlab.com');\n+    await tester.tap(find.text('jihulab.com'));\n+    await tester.pumpAndSettle();\n+    expect(ConnectionProvider.currentBaseUrl.get, 'https://jihulab.com');\n+  });\n+\n+  testWidgets('Should tap add account with to goto another page', (tester) async {\n+    ConnectionProvider().reset(Tester.gitLabUser());\n+    await tester.pumpWidget(MultiProvider(\n+      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],\n+      child: MaterialApp(\n+        routes: {SelfManagedLoginPage.routeName: (context) => const SelfManagedLoginPage()},\n+        home: const Scaffold(body: AccountPage()),\n+      ),\n+    ));\n+    await tester.pumpAndSettle();\n+    await tester.tap(find.text('Add account with'));\n+    await tester.pumpAndSettle();\n+    expect(find.byType(SelfManagedLoginPage), findsOneWidget);\n+  });\n+\n+  tearDown(() {\n+    ConnectionProvider().fullReset();\n+    ConnectionProvider().clearActive();\n+    ConnectionProvider().loggedOut(true);\n+  });\n+}\n",
    "new_path": "test/integration_tests/account_page/account_page_test.dart",
    "old_path": "test/integration_tests/account_page/account_page_test.dart",
    "a_mode": "0",
    "b_mode": "100644",
    "new_file": true,
    "renamed_file": false,
    "deleted_file": false
  },
  {
    "diff":
        "@@ -20,4 +20,35 @@ class Tester {\n     user.token = token();\n     return user;\n   }\n+\n+  static Connection gitLabUser() {\n+    var userInfo = User.fromJson({\n+      \"id\": 9527,\n+      \"username\": \"tester\",\n+      \"name\": \"tester\",\n+      \"state\": \"active\",\n+      \"two_factor_enabled\": true,\n+      \"external\": false,\n+      \"private_profile\": true,\n+    });\n+    var user = Connection(userInfo, true, BaseUrl('https://gitlab.com'));\n+    user.token = token();\n+    return user;\n+  }\n+\n+  static Connection selfManagedUser() {\n+    var userInfo = User.fromJson({\n+      \"id\": 9527,\n+      \"username\": \"tester\",\n+      \"name\": \"tester\",\n+      \"state\": \"active\",\n+      \"two_factor_enabled\": true,\n+      \"external\": false,\n+      \"private_profile\": true,\n+    });\n+    var user = Connection(userInfo, true, BaseUrl('https://gitlab.com'));\n+    user.personalAccessToken = 'access_token';\n+    user.isSelfManaged = true;\n+    return user;\n+  }\n }\n",
    "new_path": "test/mocker/tester.dart",
    "old_path": "test/mocker/tester.dart",
    "a_mode": "100644",
    "b_mode": "100644",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  },
  {
    "diff":
        "@@ -15,13 +15,15 @@ PODS:\n     - Flutter\n   - package_info_plus (0.4.5):\n     - Flutter\n-  - path_provider_ios (0.0.1):\n+  - path_provider_foundation (0.0.1):\n     - Flutter\n+    - FlutterMacOS\n   - permission_handler_apple (9.0.4):\n     - Flutter\n   - ReachabilitySwift (5.0.0)\n-  - shared_preferences_ios (0.0.1):\n+  - shared_preferences_foundation (0.0.1):\n     - Flutter\n+    - FlutterMacOS\n   - sqflite (0.0.2):\n     - Flutter\n     - FMDB (>= 2.7.5)\n@@ -44,9 +46,9 @@ DEPENDENCIES:\n   - image_picker_ios (from `.symlinks/plugins/image_picker_ios/ios`)\n   - open_filex (from `.symlinks/plugins/open_filex/ios`)\n   - package_info_plus (from `.symlinks/plugins/package_info_plus/ios`)\n-  - path_provider_ios (from `.symlinks/plugins/path_provider_ios/ios`)\n+  - path_provider_foundation (from `.symlinks/plugins/path_provider_foundation/ios`)\n   - permission_handler_apple (from `.symlinks/plugins/permission_handler_apple/ios`)\n-  - shared_preferences_ios (from `.symlinks/plugins/shared_preferences_ios/ios`)\n+  - shared_preferences_foundation (from `.symlinks/plugins/shared_preferences_foundation/ios`)\n   - sqflite (from `.symlinks/plugins/sqflite/ios`)\n   - updater (from `.symlinks/plugins/updater/ios`)\n   - url_launcher_ios (from `.symlinks/plugins/url_launcher_ios/ios`)\n@@ -73,12 +75,12 @@ EXTERNAL SOURCES:\n     :path: \".symlinks/plugins/open_filex/ios\"\n   package_info_plus:\n     :path: \".symlinks/plugins/package_info_plus/ios\"\n-  path_provider_ios:\n-    :path: \".symlinks/plugins/path_provider_ios/ios\"\n+  path_provider_foundation:\n+    :path: \".symlinks/plugins/path_provider_foundation/ios\"\n   permission_handler_apple:\n     :path: \".symlinks/plugins/permission_handler_apple/ios\"\n-  shared_preferences_ios:\n-    :path: \".symlinks/plugins/shared_preferences_ios/ios\"\n+  shared_preferences_foundation:\n+    :path: \".symlinks/plugins/shared_preferences_foundation/ios\"\n   sqflite:\n     :path: \".symlinks/plugins/sqflite/ios\"\n   updater:\n@@ -100,14 +102,14 @@ SPEC CHECKSUMS:\n   image_picker_ios: b786a5dcf033a8336a657191401bfdf12017dabb\n   open_filex: 6e26e659846ec990262224a12ef1c528bb4edbe4\n   package_info_plus: 6c92f08e1f853dc01228d6f553146438dafcd14e\n-  path_provider_ios: 14f3d2fd28c4fdb42f44e0f751d12861c43cee02\n+  path_provider_foundation: 37748e03f12783f9de2cb2c4eadfaa25fe6d4852\n   permission_handler_apple: 44366e37eaf29454a1e7b1b7d736c2cceaeb17ce\n   ReachabilitySwift: 985039c6f7b23a1da463388634119492ff86c825\n-  shared_preferences_ios: 548a61f8053b9b8a49ac19c1ffbc8b92c50d68ad\n+  shared_preferences_foundation: 297b3ebca31b34ec92be11acd7fb0ba932c822ca\n   sqflite: 6d358c025f5b867b29ed92fc697fd34924e11904\n   Toast: 91b396c56ee72a5790816f40d3a94dd357abc196\n   updater: d6c70e66a13a3704329f41f5653dd6e503753fa5\n-  url_launcher_ios: 839c58cdb4279282219f5e248c3321761ff3c4de\n+  url_launcher_ios: fb12c43172927bb5cf75aeebd073f883801f1993\n   video_player_avfoundation: e489aac24ef5cf7af82702979ed16f2a5ef84cff\n   wakelock: d0fc7c864128eac40eba1617cb5264d9c940b46f\n   webview_flutter_wkwebview: b7e70ef1ddded7e69c796c7390ee74180182971f\n",
    "new_path": "ios/Podfile.lock",
    "old_path": "ios/Podfile.lock",
    "a_mode": "100644",
    "b_mode": "100644",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  },
  {
    "diff":
        "@@ -8,6 +8,9 @@ import 'package:jihu_gitlab_app/modules/auth/login_model.dart';\n class RefreshTokenInterceptor extends Interceptor {\n   final Dio _dio;\n \n+  static Map<String, String> cacheHeader = {};\n+  static int index = -1;\n+\n   RefreshTokenInterceptor(this._dio);\n \n   @override\n@@ -17,7 +20,7 @@ class RefreshTokenInterceptor extends Interceptor {\n     if (ConnectionProvider.shouldRefreshToken && ConnectionProvider.canRefreshToken && !options.path.contains(\"/oauth/token\")) {\n       LogHelper.info(\"onRequest refreshToken path \${options.method}: \${options.path}\");\n       if (await LoginModel().refreshToken()) {\n-        _addAuthorizationHeader(options);\n+        _reAddAuthorizationHeader(options);\n         LogHelper.info(\"token 即将过期，并自动刷新成功\");\n       } else if (!LoginModel().onAuthTokenRequest) {\n         LogHelper.debug(\"onRequest refreshToken failed path \${options.method}: \${options.path}\");\n@@ -60,9 +63,13 @@ class RefreshTokenInterceptor extends Interceptor {\n \n   _addAuthorizationHeader(RequestOptions options) {\n     if (options.headers.containsKey('token-exist') && options.headers['token-exist'] == 'yes') {\n+      index = ConnectionProvider.connections.map((e) => e.authHeaders).toList().indexOf(_currentHeader(options));\n       options.headers.remove('token-exist');\n       return;\n     }\n+    if (ConnectionProvider.connection == null) return;\n+    index = ConnectionProvider.connections.indexOf(ConnectionProvider.connection!);\n+    cacheHeader = ConnectionProvider.connection!.authHeaders;\n     if (ConnectionProvider.isSelfManagedGitLab) {\n       options.headers[\"PRIVATE-TOKEN\"] = ConnectionProvider.personalAccessToken!;\n       options.headers.remove(HttpHeaders.authorizationHeader);\n@@ -71,4 +78,21 @@ class RefreshTokenInterceptor extends Interceptor {\n       options.headers.remove(\"PRIVATE-TOKEN\");\n     }\n   }\n+\n+  void _reAddAuthorizationHeader(RequestOptions options) {\n+    Map<String, String> authHeaders = index == -1 ? cacheHeader : ConnectionProvider.connections[index].authHeaders;\n+    options.headers.addAll(authHeaders);\n+    if (authHeaders.containsKey(HttpHeaders.authorizationHeader) && authHeaders[HttpHeaders.authorizationHeader]!.isNotEmpty) options.headers.remove(\"PRIVATE-TOKEN\");\n+    if (authHeaders.containsKey(\"PRIVATE-TOKEN\") && authHeaders[\"PRIVATE-TOKEN\"]!.isNotEmpty) options.headers.remove(HttpHeaders.authorizationHeader);\n+  }\n+\n+  Map<String, String> _currentHeader(RequestOptions options) {\n+    Map<String, String> currentHeader = {};\n+    if (options.headers.containsKey(HttpHeaders.authorizationHeader) && options.headers[HttpHeaders.authorizationHeader]!.isNotEmpty) {\n+      currentHeader = {HttpHeaders.authorizationHeader: options.headers[HttpHeaders.authorizationHeader]};\n+    }\n+    if (options.headers.containsKey(\"PRIVATE-TOKEN\") && options.headers[\"PRIVATE-TOKEN\"]!.isNotEmpty) cacheHeader = {\"PRIVATE-TOKEN\": options.headers[\"PRIVATE-TOKEN\"]};\n+    cacheHeader = currentHeader;\n+    return cacheHeader;\n+  }\n }\n",
    "new_path": "lib/core/net/interceptor/refresh_token_interceptor.dart",
    "old_path": "lib/core/net/interceptor/refresh_token_interceptor.dart",
    "a_mode": "100644",
    "b_mode": "100644",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  },
  {
    "diff":
        "@@ -1,14 +1,25 @@\n import 'package:path/path.dart';\n \n class Api {\n-  static const version = \"v4\";\n-  static const prefix = \"/api/\$version\";\n-  static const graphql = \"/api/graphql\";\n-  static const faqHost = \"https://jihulab.com\";\n+  static const _version = \"v4\";\n+  static const _prefix = \"/api/\$_version\";\n+  static const _graphql = \"/api/graphql\";\n+  static const jihuHost = \"https://jihulab.com\";\n \n-  static String join(String path) {\n-    return normalize('\$prefix/\$path');\n+  String _host = '';\n+\n+  Api();\n+\n+  Api jihu() {\n+    _host = jihuHost;\n+    return this;\n   }\n \n-  static String get faqGraphql => \"\$faqHost\$graphql\";\n+  String join(String path) {\n+    return _host + normalize('\$_prefix/\$path');\n+  }\n+\n+  String graphQL() {\n+    return _host + _graphql;\n+  }\n }\n",
    "new_path": "lib/core/net/api.dart",
    "old_path": "lib/core/net/api.dart",
    "a_mode": "100644",
    "b_mode": "100644",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  },
  {
    "diff":
        "@@ -1,3 +1,5 @@\n+import 'dart:io';\n+\n import 'package:dio/dio.dart' as dio;\n import 'package:flutter/cupertino.dart';\n import 'package:jihu_gitlab_app/core/net/authorized_denied_exception.dart';\n@@ -27,7 +29,7 @@ class HttpClient {\n \n   Future<r.Response<T>> getWithHeader<T>(String path, Map<String, String> header) async {\n     return _request(() async {\n-      header['token-exist'] = 'yes';\n+      if (header.containsKey(HttpHeaders.authorizationHeader) || header.containsKey(\"PRIVATE-TOKEN\")) header['token-exist'] = 'yes';\n       var response = (await _dio.get(path, options: dio.Options(headers: header)));\n       return r.Response(response.data);\n     });\n@@ -42,6 +44,7 @@ class HttpClient {\n \n   Future<r.Response<T>> postWithHeader<T>(String path, data, Map<String, String> header) async {\n     return _request(() async {\n+      if (header.containsKey(HttpHeaders.authorizationHeader) || header.containsKey(\"PRIVATE-TOKEN\")) header['token-exist'] = 'yes';\n       var response = await _dio.post(path, data: data, options: dio.Options(headers: header, receiveTimeout: 10000));\n       return r.Response(response.data);\n     });\n",
    "new_path": "lib/core/net/http_client.dart",
    "old_path": "lib/core/net/http_client.dart",
    "a_mode": "100644",
    "b_mode": "100644",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  },
  {
    "diff":
        "@@ -1,3 +1,5 @@\n+import 'dart:io';\n+\n import 'package:jihu_gitlab_app/core/domain/token.dart';\n import 'package:jihu_gitlab_app/core/id.dart';\n import 'package:jihu_gitlab_app/l10n/app_localizations.dart';\n@@ -22,6 +24,14 @@ class Connection {\n     return user;\n   }\n \n+  Map<String, String> get authHeaders {\n+    if (personalAccessToken != null) {\n+      return {\"PRIVATE-TOKEN\": personalAccessToken!};\n+    } else {\n+      return {HttpHeaders.authorizationHeader: 'Bearer \${token!.accessToken}'};\n+    }\n+  }\n+\n   Map<String, dynamic> toJson() {\n     return <String, dynamic>{\n       \"info\": userInfo.toJson(),\n",
    "new_path": "lib/core/user_provider/user.dart",
    "old_path": "lib/core/user_provider/user.dart",
    "a_mode": "100644",
    "b_mode": "100644",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  },
  {
    "diff":
        "@@ -0,0 +1,80 @@\n+import 'package:flutter/material.dart';\n+import 'package:jihu_gitlab_app/l10n/app_localizations.dart';\n+import 'package:jihu_gitlab_app/modules/auth/auth.dart';\n+\n+class ConnectJihuPage extends StatefulWidget {\n+  const ConnectJihuPage({super.key});\n+\n+  @override\n+  State<ConnectJihuPage> createState() => _ConnectJihuPageState();\n+}\n+\n+class _ConnectJihuPageState extends State<ConnectJihuPage> {\n+  bool inLoad = false;\n+\n+  @override\n+  void initState() {\n+    super.initState();\n+  }\n+\n+  @override\n+  Widget build(BuildContext context) {\n+    return SafeArea(\n+      child: Container(\n+        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.15),\n+        child: Column(\n+          crossAxisAlignment: CrossAxisAlignment.center,\n+          children: [\n+            Padding(\n+                padding: const EdgeInsets.only(left: 20, right: 20),\n+                child: Text.rich(\n+                  TextSpan(children: [\n+                    TextSpan(text: JiHuLocalizations.dictionary().connect_with),\n+                    const TextSpan(text: \" jihulab.com \", style: TextStyle(color: Color(0xFFFC6D26))),\n+                    TextSpan(text: JiHuLocalizations.dictionary().to_participate),\n+                  ]),\n+                  textAlign: TextAlign.center,\n+                  style: const TextStyle(fontSize: 16, color: Color(0xFF03162F), fontWeight: FontWeight.w600),\n+                )),\n+            const SizedBox(height: 4),\n+            Padding(\n+                padding: const EdgeInsets.only(left: 20, right: 20),\n+                child: Text(JiHuLocalizations.dictionary().in_discussions_and_create_posts,\n+                    textAlign: TextAlign.center, style: const TextStyle(fontSize: 16, color: Color(0xFF03162F), fontWeight: FontWeight.w600))),\n+            Padding(padding: const EdgeInsets.only(left: 20, right: 20), child: line2Widget()),\n+            Padding(padding: const EdgeInsets.only(left: 20, right: 20), child: toConnectButton()),\n+          ],\n+        ),\n+      ),\n+    );\n+  }\n+\n+  Widget line2Widget() {\n+    return Column(children: [\n+      Container(\n+        height: 8,\n+      ),\n+      Text(JiHuLocalizations.dictionary().other_connection_not_allow, style: const TextStyle(fontSize: 12, color: AppThemeData.secondaryColor))\n+    ]);\n+  }\n+\n+  Widget toConnectButton() {\n+    var colorScheme = Theme.of(context).colorScheme;\n+    return Column(\n+      children: [\n+        const SizedBox(height: 16),\n+        InkWell(\n+          onTap: () {\n+            Navigator.of(context).pushNamed(LoginPage.routeName, arguments: {'host': 'jihulab.com'});\n+          },\n+          child: Container(\n+              height: 40,\n+              width: 140,\n+              margin: const EdgeInsets.all(16),\n+              decoration: BoxDecoration(color: colorScheme.primary, borderRadius: BorderRadius.circular(4.0)),\n+              child: Center(child: Text(JiHuLocalizations.dictionary().to_connect, style: const TextStyle(color: Colors.white, fontSize: 15)))),\n+        ),\n+      ],\n+    );\n+  }\n+}\n",
    "new_path": "lib/core/widgets/connect_jihu_page.dart",
    "old_path": "lib/core/widgets/connect_jihu_page.dart",
    "a_mode": "0",
    "b_mode": "100644",
    "new_file": true,
    "renamed_file": false,
    "deleted_file": false
  },
  {
    "diff":
        "@@ -73,7 +73,7 @@ class StarModel {\n     bool star = !data.starred;\n     bool result = false;\n     if (data.type == SubgroupItemType.project) {\n-      String path = Api.join(\"projects/\${data.iid}/\${star ? 'star' : 'unstar'}\");\n+      String path = Api().join(\"projects/\${data.iid}/\${star ? 'star' : 'unstar'}\");\n       try {\n         await HttpClient.instance().post(path, {});\n         result = true;\n",
    "new_path": "lib/core/widgets/star.dart",
    "old_path": "lib/core/widgets/star.dart",
    "a_mode": "100644",
    "b_mode": "100644",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  },
  {
    "diff":
        "@@ -247,5 +247,10 @@\n   \"allEligibleUsers\": \"All eligible users\",\n   \"detail\": \"Detail\",\n   \"community\": \"Community\",\n+  \"connect_with\": \"Connect with\",\n+  \"to_participate\": \"to participate\",\n+  \"in_discussions_and_create_posts\": \"in discussions and create posts\",\n+  \"other_connection_not_allow\": \"Other connect methods are only support to view\",\n+  \"to_connect\": \"To Connect\",\n   \"post\": \"Post\"\n }\n",
    "new_path": "lib/l10n/intl_en.arb",
    "old_path": "lib/l10n/intl_en.arb",
    "a_mode": "100644",
    "b_mode": "100644",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  },
  {
    "diff":
        "@@ -247,5 +247,10 @@\n   \"allEligibleUsers\": \"所有符合条件的用户\",\n   \"detail\": \"详情\",\n   \"community\": \"社区\",\n+  \"connect_with\": \"连接至\",\n+  \"to_participate\": \"以参与讨论\",\n+  \"in_discussions_and_create_posts\": \"或创建帖子\",\n+  \"other_connection_not_allow\": \"其他连接方式仅支持浏览\",\n+  \"to_connect\": \"连接\",\n   \"post\": \"帖子\"\n }\n",
    "new_path": "lib/l10n/intl_zh.arb",
    "old_path": "lib/l10n/intl_zh.arb",
    "a_mode": "100644",
    "b_mode": "100644",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  },
  {
    "diff":
        "@@ -3,6 +3,7 @@ import 'package:jihu_gitlab_app/core/domain/token.dart';\n import 'package:jihu_gitlab_app/core/log_helper.dart';\n import 'package:jihu_gitlab_app/core/net/http_client.dart';\n import 'package:jihu_gitlab_app/core/connection_provider/login_type_same_exception.dart';\n+import 'package:jihu_gitlab_app/core/connection_provider/connection.dart';\n import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';\n \n enum LoginClientIdSet { jihulab, gitlab }\n@@ -66,12 +67,15 @@ class LoginModel {\n \n   Future<bool> refreshToken() async {\n     if (_onAuthTokenRequest) return false;\n-    if (ConnectionProvider.personalAccessToken != null) return false;\n     _onAuthTokenRequest = true;\n-    _clientId = ConnectionProvider.currentBaseUrl.get.contains(LoginClientIdSet.jihulab.name) ? LoginClientIdSet.jihulab : LoginClientIdSet.gitlab;\n-    Map<String, dynamic> data = {\"client_id\": _clientId!.id, \"grant_type\": _grantTypeRefresh, \"redirect_uri\": _redirectUri, \"refresh_token\": ConnectionProvider.refreshToken};\n+    List<Connection> connections = ConnectionProvider.connections.where((o) => o.personalAccessToken == null).toList();\n     try {\n-      ConnectionProvider().resetCurrentToken(await _doRequest(data, ConnectionProvider.currentBaseUrl.get));\n+      for (var connection in connections) {\n+        _clientId = connection.baseUrl.get.contains(LoginClientIdSet.jihulab.name) ? LoginClientIdSet.jihulab : LoginClientIdSet.gitlab;\n+        Map<String, dynamic> data = {\"client_id\": _clientId!.id, \"grant_type\": _grantTypeRefresh, \"redirect_uri\": _redirectUri, \"refresh_token\": connection.token!.refreshToken};\n+        connection.token = await _doRequest(data, connection.baseUrl.get);\n+        ConnectionProvider().notifyChanged();\n+      }\n       _onAuthTokenRequest = false;\n       return Future.value(true);\n     } catch (e) {\n",
    "new_path": "lib/modules/auth/login_model.dart",
    "old_path": "lib/modules/auth/login_model.dart",
    "a_mode": "100644",
    "b_mode": "100644",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  },
  {
    "diff":
        "@@ -0,0 +1,54 @@\n+import 'package:jihu_gitlab_app/core/net/http_client.dart';\n+import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';\n+import 'package:jihu_gitlab_app/modules/issues/manage/models/issue_draft_entity.dart';\n+import 'package:jihu_gitlab_app/modules/issues/manage/models/issue_draft_repository.dart';\n+\n+import '../../core/net/api.dart';\n+\n+class FaqCreationModel {\n+  late int _projectId;\n+  final IssueDraftRepository _repository = IssueDraftRepository();\n+\n+  void init(int projectId) {\n+    _projectId = projectId;\n+  }\n+\n+  Future<bool> create(int projectId, String title, {String? description, String? labels}) async {\n+    try {\n+      String uri = Api().jihu().join('/projects/\$projectId/issues');\n+      var body = <String, dynamic>{'title': title};\n+      if (description != null) {\n+        body['description'] = description;\n+      }\n+      await HttpClient.instance().postWithHeader<Map<String, dynamic>>(uri, body, ConnectionProvider().jiHuConnection!.authHeaders);\n+      _repository.delete(projectId, ConnectionProvider().jiHuConnection!.userInfo.id);\n+      return Future.value(true);\n+    } catch (error) {\n+      return Future.value(false);\n+    }\n+  }\n+\n+  int get projectId => _projectId;\n+\n+  void saveAsDraft(String title, String description) async {\n+    var userId = ConnectionProvider().jiHuConnection!.userInfo.id;\n+    var faqDraft = {\n+      \"user_id\": userId,\n+      \"title\": title,\n+      \"description\": description,\n+    };\n+    int version = DateTime.now().millisecondsSinceEpoch;\n+    var faqDraftEntity = IssueDraftEntity.create(faqDraft, projectId, version);\n+    List<IssueDraftEntity> issueDrafts = await _repository.query(projectId, userId);\n+    if (issueDrafts.isNotEmpty) {\n+      _repository.update(faqDraftEntity);\n+    } else {\n+      _repository.insert(faqDraftEntity);\n+    }\n+  }\n+\n+  Future<List<IssueDraftEntity>> loadDraft() async {\n+    List<IssueDraftEntity> issueDrafts = await _repository.query(projectId, ConnectionProvider().jiHuConnection!.userInfo.id);\n+    return issueDrafts;\n+  }\n+}\n",
    "new_path": "lib/modules/faq/community_post_creation_model.dart",
    "old_path": "lib/modules/faq/community_post_creation_model.dart",
    "a_mode": "0",
    "b_mode": "100644",
    "new_file": true,
    "renamed_file": false,
    "deleted_file": false
  },
  {
    "diff": "Binary files a/android/app/src/main/res/mipmap-hdpi/ic_launcher.png and b/android/app/src/main/res/mipmap-hdpi/ic_launcher.png differ\n",
    "new_path": "android/app/src/main/res/mipmap-hdpi/ic_launcher.png",
    "old_path": "android/app/src/main/res/mipmap-hdpi/ic_launcher.png",
    "a_mode": "100644",
    "b_mode": "100644",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  },
  {
    "diff": "Binary files a/android/app/src/main/res/mipmap-mdpi/ic_launcher.png and b/android/app/src/main/res/mipmap-mdpi/ic_launcher.png differ\n",
    "new_path": "android/app/src/main/res/mipmap-mdpi/ic_launcher.png",
    "old_path": "android/app/src/main/res/mipmap-mdpi/ic_launcher.png",
    "a_mode": "100644",
    "b_mode": "100644",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  },
  {
    "diff": "Binary files a/android/app/src/main/res/mipmap-xhdpi/ic_launcher.png and b/android/app/src/main/res/mipmap-xhdpi/ic_launcher.png differ\n",
    "new_path": "android/app/src/main/res/mipmap-xhdpi/ic_launcher.png",
    "old_path": "android/app/src/main/res/mipmap-xhdpi/ic_launcher.png",
    "a_mode": "100644",
    "b_mode": "100644",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  },
  {
    "diff":
        "@@ -192,7 +192,8 @@ class _AccountPageState extends State<AccountPage> {\n                                               textTheme.titleSmall,\n                                               ConnectionProvider().isUsed(item) ? const Color(0xFF87878C) : Colors.black,\n                                             )),\n-                                        if (ConnectionProvider().isUsed(item)) const Text('Already used', style: TextStyle(color: AppThemeData.secondaryColor, fontSize: 10, fontWeight: FontWeight.w400))\n+                                        if (ConnectionProvider().isUsed(item))\n+                                          Text(JiHuLocalizations.dictionary().alreadyUsed, style: const TextStyle(color: AppThemeData.secondaryColor, fontSize: 10, fontWeight: FontWeight.w400))\n                                       ],\n                                     ),\n                                     if (_selectedItem.displayTitle == item)\n",
    "new_path": "lib/core/widgets/account/account_page.dart",
    "old_path": "lib/core/widgets/account/account_page.dart",
    "a_mode": "100644",
    "b_mode": "100644",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  }
];
List<Map<String, dynamic>> mrDiffsNextPageResponse = [
  {
    "diff": "Binary files a/ios/Runner/Assets.xcassets/AppIcon.appiconset/Icon-App-83.5x83.5@2x.png and b/ios/Runner/Assets.xcassets/AppIcon.appiconset/Icon-App-83.5x83.5@2x.png differ\n",
    "new_path": "ios/Runner/Assets.xcassets/AppIcon.appiconset/Icon-App-83.5x83.5@2x.png",
    "old_path": "ios/Runner/Assets.xcassets/AppIcon.appiconset/Icon-App-83.5x83.5@2x.png",
    "a_mode": "100644",
    "b_mode": "100644",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  },
  {
    "diff":
        "@@ -17,15 +17,13 @@ PODS:\n     - Flutter\n   - package_info_plus (0.4.5):\n     - Flutter\n-  - path_provider_foundation (0.0.1):\n+  - path_provider_ios (0.0.1):\n     - Flutter\n-    - FlutterMacOS\n   - permission_handler_apple (9.0.4):\n     - Flutter\n   - ReachabilitySwift (5.0.0)\n-  - shared_preferences_foundation (0.0.1):\n+  - shared_preferences_ios (0.0.1):\n     - Flutter\n-    - FlutterMacOS\n   - sqflite (0.0.2):\n     - Flutter\n     - FMDB (>= 2.7.5)\n@@ -49,9 +47,9 @@ DEPENDENCIES:\n   - image_picker_ios (from `.symlinks/plugins/image_picker_ios/ios`)\n   - open_filex (from `.symlinks/plugins/open_filex/ios`)\n   - package_info_plus (from `.symlinks/plugins/package_info_plus/ios`)\n-  - path_provider_foundation (from `.symlinks/plugins/path_provider_foundation/ios`)\n+  - path_provider_ios (from `.symlinks/plugins/path_provider_ios/ios`)\n   - permission_handler_apple (from `.symlinks/plugins/permission_handler_apple/ios`)\n-  - shared_preferences_foundation (from `.symlinks/plugins/shared_preferences_foundation/ios`)\n+  - shared_preferences_ios (from `.symlinks/plugins/shared_preferences_ios/ios`)\n   - sqflite (from `.symlinks/plugins/sqflite/ios`)\n   - updater (from `.symlinks/plugins/updater/ios`)\n   - url_launcher_ios (from `.symlinks/plugins/url_launcher_ios/ios`)\n@@ -80,12 +78,12 @@ EXTERNAL SOURCES:\n     :path: \".symlinks/plugins/open_filex/ios\"\n   package_info_plus:\n     :path: \".symlinks/plugins/package_info_plus/ios\"\n-  path_provider_foundation:\n-    :path: \".symlinks/plugins/path_provider_foundation/ios\"\n+  path_provider_ios:\n+    :path: \".symlinks/plugins/path_provider_ios/ios\"\n   permission_handler_apple:\n     :path: \".symlinks/plugins/permission_handler_apple/ios\"\n-  shared_preferences_foundation:\n-    :path: \".symlinks/plugins/shared_preferences_foundation/ios\"\n+  shared_preferences_ios:\n+    :path: \".symlinks/plugins/shared_preferences_ios/ios\"\n   sqflite:\n     :path: \".symlinks/plugins/sqflite/ios\"\n   updater:\n@@ -105,21 +103,21 @@ SPEC CHECKSUMS:\n   Flutter: f04841e97a9d0b0a8025694d0796dd46242b2854\n   fluttertoast: eb263d302cc92e04176c053d2385237e9f43fad0\n   FMDB: 2ce00b547f966261cd18927a3ddb07cb6f3db82a\n-  image_picker_ios: 4a8aadfbb6dc30ad5141a2ce3832af9214a705b5\n+  image_picker_ios: b786a5dcf033a8336a657191401bfdf12017dabb\n   open_filex: 6e26e659846ec990262224a12ef1c528bb4edbe4\n   package_info_plus: 6c92f08e1f853dc01228d6f553146438dafcd14e\n-  path_provider_foundation: c68054786f1b4f3343858c1e1d0caaded73f0be9\n+  path_provider_ios: 14f3d2fd28c4fdb42f44e0f751d12861c43cee02\n   permission_handler_apple: 44366e37eaf29454a1e7b1b7d736c2cceaeb17ce\n   ReachabilitySwift: 985039c6f7b23a1da463388634119492ff86c825\n-  shared_preferences_foundation: 986fc17f3d3251412d18b0265f9c64113a8c2472\n+  shared_preferences_ios: 548a61f8053b9b8a49ac19c1ffbc8b92c50d68ad\n   sqflite: 6d358c025f5b867b29ed92fc697fd34924e11904\n   Toast: 91b396c56ee72a5790816f40d3a94dd357abc196\n   updater: d6c70e66a13a3704329f41f5653dd6e503753fa5\n-  url_launcher_ios: 08a3dfac5fb39e8759aeb0abbd5d9480f30fc8b4\n-  video_player_avfoundation: 81e49bb3d9fb63dccf9fa0f6d877dc3ddbeac126\n+  url_launcher_ios: 839c58cdb4279282219f5e248c3321761ff3c4de\n+  video_player_avfoundation: e489aac24ef5cf7af82702979ed16f2a5ef84cff\n   wakelock: d0fc7c864128eac40eba1617cb5264d9c940b46f\n   webview_flutter_wkwebview: b7e70ef1ddded7e69c796c7390ee74180182971f\n \n PODFILE CHECKSUM: 136f12f2c3fbfa1b446c67051cc113af5903053d\n \n-COCOAPODS: 1.12.0\n+COCOAPODS: 1.11.3\n",
    "new_path": "ios/Podfile.lock",
    "old_path": "ios/Podfile.lock",
    "a_mode": "100644",
    "b_mode": "100644",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  },
  {
    "diff":
        "@@ -72,7 +72,7 @@ class _LoginPageState extends State<LoginPage> {\n                       _controller = webViewController;\n                       _completer.complete(webViewController);\n \n-                      if (!ConnectionProvider.authorized && ConnectionProvider.isLoggedOutByUser) {\n+                      if (ConnectionProvider.isLoggedOutByUser) {\n                         webViewController.clearCache();\n                         final cookieManager = CookieManager();\n                         cookieManager.clearCookies();\n",
    "new_path": "lib/modules/auth/login_page.dart",
    "old_path": "lib/modules/auth/login_page.dart",
    "a_mode": "100644",
    "b_mode": "100644",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  }
];

Map<String, dynamic> commitsGraphQLResponse = {
  "data": {
    "project": {
      "mergeRequest": {
        "commits": {
          "pageInfo": {"hasNextPage": false, "endCursor": "MQ"},
          "nodes": [
            {
              "id": "gid://gitlab/CommitPresenter/0334c32c55457cf7269db25c812284cfd4afe24f",
              "shortId": "0334c32c",
              "sha": "0334c32c55457cf7269db25c812284cfd4afe24f",
              "title": "chore: #497 Merge code from main.",
              "description": "",
              "author": {"name": "name1 in page 1", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png"}
            },
            {
              "id": "gid://gitlab/CommitPresenter/1494c07a894a8f38e53b2fe9f683f09efff955db",
              "shortId": "1494c07a",
              "sha": "1494c07a894a8f38e53b2fe9f683f09efff955db",
              "title": "refactor: #497 Remove no needs code and rename",
              "description": "",
              "author": {"name": "name2 in page 1", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png"}
            }
          ]
        }
      }
    }
  }
};

Map<String, dynamic> jobsGraphQLResponse = {
  "data": {
    "project": {
      "mergeRequest": {
        "headPipeline": {
          "jobs": {
            "pageInfo": {"hasNextPage": false, "endCursor": "eyJpZCI6IjcxNTIwMDQifQ"},
            "nodes": [
              {
                "id": "gid://gitlab/Ci::Build/7152007",
                "name": "unit_test",
                "active": false,
                "status": "CREATED",
                "allowFailure": false,
                "duration": null,
                "startedAt": null,
                "finishedAt": null,
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2407677",
                  "name": "test",
                  "status": "created",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/7152007", "name": "unit_test", "status": "CREATED", "allowFailure": false}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/7152006",
                "name": "code_quality",
                "active": false,
                "status": "CREATED",
                "allowFailure": false,
                "duration": null,
                "startedAt": null,
                "finishedAt": null,
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2407676",
                  "name": "lint",
                  "status": "created",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/7152006", "name": "code_quality", "status": "CREATED", "allowFailure": false}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/7152005",
                "name": "install",
                "active": false,
                "status": "CREATED",
                "allowFailure": false,
                "duration": null,
                "startedAt": null,
                "finishedAt": null,
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2407675",
                  "name": "install",
                  "status": "created",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/7152005", "name": "install", "status": "CREATED", "allowFailure": false}
                    ]
                  }
                }
              },
              {
                "id": "gid://gitlab/Ci::Build/7152004",
                "name": "style_check",
                "active": true,
                "status": "PENDING",
                "allowFailure": false,
                "duration": null,
                "startedAt": null,
                "finishedAt": null,
                "stage": {
                  "id": "gid://gitlab/Ci::Stage/2407674",
                  "name": "style",
                  "status": "pending",
                  "jobs": {
                    "nodes": [
                      {"id": "gid://gitlab/Ci::Build/7152004", "name": "style_check", "status": "PENDING", "allowFailure": false}
                    ]
                  }
                }
              }
            ]
          }
        }
      }
    }
  }
};

Map<String, dynamic> codeQualityReportsResponse = {
  "data": {
    "project": {
      "mergeRequest": {
        "headPipeline": {
          "codeQualityReports": {
            "nodes": [
              {
                "description": "The method doesn't override an inherited method",
                "fingerprint": "1d3e96a04c34977ae6bdd4ea70070095",
                "line": 84,
                "path": "test/modules/ai/ai_page_test.mocks.dart",
                "severity": "INFO",
                "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/3c01fc4a9e2a87c1d7660f0ab3180917ee7992b0/test/modules/ai/ai_page_test.mocks.dart#L84"
              },
              {
                "description": "The method doesn't override an inherited method",
                "fingerprint": "5b52cf3a23677a78008b449059c7e94a",
                "line": 94,
                "path": "test/modules/ai/ai_page_test.mocks.dart",
                "severity": "INFO",
                "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/3c01fc4a9e2a87c1d7660f0ab3180917ee7992b0/test/modules/ai/ai_page_test.mocks.dart#L94"
              },
              {
                "description": "Avoid `print` calls in production code",
                "fingerprint": "baf77a8fff3c7e68e1e0d8b7f1c6999f",
                "line": 8,
                "path": "test/modules/mr/pipeline_detailed_status_test.dart",
                "severity": "INFO",
                "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/3c01fc4a9e2a87c1d7660f0ab3180917ee7992b0/test/modules/mr/pipeline_detailed_status_test.dart#L8"
              }
            ]
          }
        },
      }
    }
  }
};
