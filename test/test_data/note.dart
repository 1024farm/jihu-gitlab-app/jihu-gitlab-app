import 'package:jihu_gitlab_app/core/domain/assignee/assignee_entity.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/note/note.dart';

import 'assignee.dart';

var noteDbData = {
  'id': 11,
  'note_id': 1574834,
  "noteable_id": 265567,
  "noteable_iid": 6,
  'discussion_id': 'abc124',
  'system': 1,
  'body': 'assigned to @perity',
  'assignee_id': 29355,
  'created_at': '2022-11-25T14:21:47.711+08:00',
  'version': 4,
  'author': AssigneeEntity.restore(assigneeDbData).toMap()
};

Map<String, dynamic> noteData = {
  "id": 1574825,
  "type": null,
  "body": "changed the description",
  "attachment": null,
  "author": {
    "id": 29355,
    "username": "perity",
    "name": "miaolu",
    "state": "active",
    "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29355/avatar.png",
    "web_url": "https://jihulab.com/perity"
  },
  "created_at": "2022-11-25T14:19:57.067+08:00",
  "updated_at": "2022-11-25T14:19:57.068+08:00",
  "system": true,
  "noteable_id": 265567,
  "noteable_type": "Issue",
  "resolvable": false,
  "confidential": false,
  "internal": false,
  "noteable_iid": 6,
  "commands_changes": {}
};

Map<String, dynamic> noteDataWithNullValues = {
  "id": 1574825,
  "type": null,
  "body": null,
  "attachment": null,
  "author": {"id": 29355, "username": null, "name": null, "state": "active", "avatar_url": null, "web_url": null},
  "created_at": "2022-11-25T14:19:57.067+08:00",
  "updated_at": "2022-11-25T14:19:57.068+08:00",
  "system": true,
  "noteable_id": 265567,
  "noteable_type": "Issue",
  "resolvable": false,
  "confidential": false,
  "internal": false,
  "noteable_iid": 6,
  "commands_changes": {}
};

List<Note> noteList = [
  Note.fromJson({
    "id": 1574825,
    "type": null,
    "body": "changed the description",
    "attachment": null,
    "author": {
      "id": 29355,
      "username": "perity",
      "name": "miaolu",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29355/avatar.png",
      "web_url": "https://jihulab.com/perity"
    },
    "created_at": "2022-11-25T14:19:57.067+08:00",
    "updated_at": "2022-11-25T14:19:57.068+08:00",
    "system": false,
    "noteable_id": 265567,
    "noteable_type": "Issue",
    "resolvable": false,
    "confidential": false,
    "internal": false,
    "noteable_iid": 6,
    "commands_changes": {}
  }, 'pathWithNamespace'),
  Note.fromJson({
    "id": 1574834,
    "type": null,
    "body": "assigned to @perity",
    "attachment": null,
    "author": {
      "id": 29355,
      "username": "perity",
      "name": "miaolu",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29355/avatar.png",
      "web_url": "https://jihulab.com/perity"
    },
    "created_at": "2022-11-25T14:21:47.711+08:00",
    "updated_at": "2022-11-25T14:21:47.712+08:00",
    "system": false,
    "noteable_id": 657567,
    "noteable_type": "Issue",
    "resolvable": false,
    "confidential": false,
    "internal": false,
    "noteable_iid": 7,
    "commands_changes": {}
  }, 'pathWithNamespace'),
  Note.fromJson({
    "id": 1929423,
    "type": "DiscussionNote",
    "body": "@raymond_liao new try",
    "attachment": null,
    "author": {
      "id": 30192,
      "username": "raymond_liao",
      "name": "Raymond Liao",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/30192/avatar.png",
      "web_url": "https://jihulab.com/raymond_liao"
    },
    "created_at": "2023-01-02T13:11:05.966+08:00",
    "updated_at": "2023-01-02T13:11:05.966+08:00",
    "system": false,
    "noteable_id": 657657,
    "noteable_type": "Issue",
    "resolvable": false,
    "confidential": true,
    "internal": true,
    "noteable_iid": 8,
    "commands_changes": {}
  }, 'pathWithNamespace'),
  Note.fromJson({
    "id": 1930145,
    "type": "DiscussionNote",
    "body": "the other one @raymond_liao",
    "attachment": null,
    "author": {
      "id": 30192,
      "username": "raymond_liao",
      "name": "Raymond Liao",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/30192/avatar.png",
      "web_url": "https://jihulab.com/raymond_liao"
    },
    "created_at": "2023-01-03T10:40:54.714+08:00",
    "updated_at": "2023-01-03T10:40:54.714+08:00",
    "system": false,
    "noteable_id": 456456,
    "noteable_type": "Issue",
    "resolvable": false,
    "confidential": true,
    "internal": true,
    "noteable_iid": 9,
    "commands_changes": {}
  }, 'pathWithNamespace'),
  Note.fromJson({
    "id": 1930275,
    "type": "DiscussionNote",
    "body": "new comment @raymond_liao",
    "attachment": null,
    "author": {
      "id": 30192,
      "username": "raymond_liao",
      "name": "Raymond Liao",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/30192/avatar.png",
      "web_url": "https://jihulab.com/raymond_liao"
    },
    "created_at": "2023-01-03T11:22:13.850+08:00",
    "updated_at": "2023-01-03T11:22:13.850+08:00",
    "system": false,
    "noteable_id": 345345,
    "noteable_type": "Issue",
    "resolvable": false,
    "confidential": true,
    "internal": true,
    "noteable_iid": 10,
    "commands_changes": {}
  }, 'pathWithNamespace'),
  Note.fromJson({
    "id": 1930307,
    "type": "DiscussionNote",
    "body": "new comment 12 @raymond_liao",
    "attachment": null,
    "author": {
      "id": 30192,
      "username": "raymond_liao",
      "name": "Raymond Liao",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/30192/avatar.png",
      "web_url": "https://jihulab.com/raymond_liao"
    },
    "created_at": "2023-01-03T11:30:53.986+08:00",
    "updated_at": "2023-01-03T11:30:53.986+08:00",
    "system": false,
    "noteable_id": 879678,
    "noteable_type": "Issue",
    "resolvable": false,
    "confidential": true,
    "internal": true,
    "noteable_iid": 11,
    "commands_changes": {}
  }, 'pathWithNamespace'),
];
