var communityItemDetail = {
  "id": 326489,
  "iid": 238,
  "project_id": 89335,
  "title": "Why remote?",
  "description": "Community detail for test",
  "state": "opened",
  "created_at": "2022-11-25T14:19:30.461+08:00",
  "updated_at": "2022-12-09T14:10:31.987+08:00",
  "closed_at": null,
  "closed_by": null,
  "labels": ["for::geek", "lang::en", "type::WFH"],
  "milestone": null,
  "assignees": [],
  "author": {
    "id": 23836,
    "username": "jojo0",
    "name": "yajie xue",
    "state": "active",
    "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/23836/avatar.png",
    "web_url": "https://jihulab.com/jojo0"
  },
  "type": "ISSUE",
  "assignee": null,
  "user_notes_count": 0,
  "merge_requests_count": 0,
  "upvotes": 0,
  "downvotes": 0,
  "due_date": null,
  "confidential": false,
  "discussion_locked": null,
  "issue_type": "issue",
  "web_url": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/faq/-/issues/238",
  "time_stats": {"time_estimate": 0, "total_time_spent": 0, "human_time_estimate": null, "human_total_time_spent": null},
  "task_completion_status": {"count": 0, "completed_count": 0},
  "weight": null,
  "blocking_issues_count": 0,
  "has_tasks": true,
  "task_status": "0 of 0 checklist items completed",
  "_links": {
    "self": "https://jihulab.com/api/v4/projects/89335/issues/238",
    "notes": "https://jihulab.com/api/v4/projects/89335/issues/238/notes",
    "award_emoji": "https://jihulab.com/api/v4/projects/89335/issues/238/award_emoji",
    "project": "https://jihulab.com/api/v4/projects/89335",
    "closed_as_duplicate_of": null
  },
  "references": {"short": "#238", "relative": "#238", "full": "ultimate-plan/jihu-gitlab-app/faq#238"},
  "severity": "UNKNOWN",
  "subscribed": false,
  "moved_to_id": null,
  "service_desk_reply_to": null,
  "epic_iid": null,
  "epic": null,
  "iteration": null,
  "health_status": null
};

Map<String, dynamic> communityDetailsGraphQLResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/89335",
      "name": "开源社区",
      "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 开源社区",
      "path": "faq",
      "fullPath": "ultimate-plan/jihu-gitlab-app/faq",
      "issue": {
        "id": "gid://gitlab/Issue/326489",
        "iid": "238",
        "projectId": 89335,
        "title": "Why remote?",
        "description": "Community detail for test",
        "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/faq/-/issues/238",
        "confidential": false,
        "state": "opened",
        "createdAt": "2022-11-25T14:19:30.461+08:00",
        "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
        "assignees": {"nodes": []},
        "userPermissions": {"updateIssue": true},
        "labels": {
          "nodes": [
            {"id": "gid://gitlab/ProjectLabel/72406", "title": "lang::en", "description": "英文", "color": "#9400d3", "textColor": "#FFFFFF"},
            {"id": "gid://gitlab/ProjectLabel/72059", "title": "type::WFH", "description": "远程办公", "color": "#6699cc", "textColor": "#FFFFFF"}
          ]
        }
      }
    }
  }
};

Map<String, dynamic> communityPostVotesGraphQLResponse = {
  "data": {
    "project": {
      "issue": {"downvotes": 0, "upvotes": 0}
    }
  }
};

Map<String, dynamic> communityDetailsWithNoEditPermissionGraphQLResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/89335",
      "name": "开源社区",
      "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 开源社区",
      "path": "faq",
      "fullPath": "ultimate-plan/jihu-gitlab-app/faq",
      "issue": {
        "id": "gid://gitlab/Issue/326489",
        "iid": "238",
        "projectId": 89335,
        "title": "Why remote?",
        "description": "Community detail for test",
        "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/faq/-/issues/238",
        "confidential": false,
        "state": "opened",
        "createdAt": "2022-11-25T14:19:30.461+08:00",
        "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
        "assignees": {"nodes": []},
        "userPermissions": {"updateIssue": false},
        "labels": {
          "nodes": [
            {"id": "gid://gitlab/ProjectLabel/72406", "title": "lang::en", "description": "英文", "color": "#9400d3", "textColor": "#FFFFFF"},
            {"id": "gid://gitlab/ProjectLabel/72059", "title": "type::WFH", "description": "远程办公", "color": "#6699cc", "textColor": "#FFFFFF"}
          ]
        }
      }
    }
  }
};

Map<String, dynamic> geekCommunityDetailsGraphQLResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/89335",
      "name": "开源社区",
      "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 开源社区",
      "path": "faq",
      "fullPath": "ultimate-plan/jihu-gitlab-app/faq",
      "issue": {
        "id": "gid://gitlab/Issue/326489",
        "iid": "238",
        "projectId": 89335,
        "title": "Why remote?2",
        "description": "Community detail for test2",
        "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/faq/-/issues/238",
        "confidential": false,
        "state": "opened",
        "createdAt": "2022-11-25T14:19:30.461+08:00",
        "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
        "assignees": {"nodes": []},
        "userPermissions": {"updateIssue": true},
        "labels": {
          "nodes": [
            {"id": "gid://gitlab/ProjectLabel/74785", "title": "for::geek", "description": "", "color": "#6699cc", "textColor": "#FFFFFF"},
            {"id": "gid://gitlab/ProjectLabel/72406", "title": "lang::en", "description": "英文", "color": "#9400d3", "textColor": "#FFFFFF"},
            {"id": "gid://gitlab/ProjectLabel/72059", "title": "type::WFH", "description": "远程办公", "color": "#6699cc", "textColor": "#FFFFFF"}
          ]
        }
      }
    }
  }
};
