Map<String, dynamic> issueData = {
  "id": 265567,
  "iid": 6,
  "project_id": 72936,
  "title": "回复评论",
  "description": "issue description for test",
  "state": "opened",
  "created_at": "2022-11-25T14:19:30.461+08:00",
  "updated_at": "2022-12-09T14:10:31.987+08:00",
  "closed_at": null,
  "closed_by": null,
  "labels": ["P::M"],
  "milestone": null,
  "assignees": [
    {
      "id": 29355,
      "username": "assignee-username",
      "name": "AssigneeName",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29355/avatar.png",
      "web_url": "https://jihulab.com/perity"
    }
  ],
  "author": {
    "id": 29355,
    "username": "perity",
    "name": "miaolu",
    "state": "active",
    "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29355/avatar.png",
    "web_url": "https://jihulab.com/perity"
  },
  "type": "ISSUE",
  "assignee": {
    "id": 29355,
    "username": "assignee-username",
    "name": "AssigneeName",
    "state": "active",
    "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29355/avatar.png",
    "web_url": "https://jihulab.com/perity"
  },
  "user_notes_count": 21,
  "merge_requests_count": 0,
  "upvotes": 0,
  "downvotes": 0,
  "due_date": null,
  "confidential": true,
  "discussion_locked": null,
  "issue_type": "issue",
  "web_url": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo-mr-test/-/issues/6",
  "time_stats": {"time_estimate": 0, "total_time_spent": 0, "human_time_estimate": null, "human_total_time_spent": null},
  "task_completion_status": {"count": 0, "completed_count": 0},
  "weight": null,
  "blocking_issues_count": 0,
  "has_tasks": false,
  "_links": {
    "self": "https://jihulab.com/api/v4/projects/72936/issues/6",
    "notes": "https://jihulab.com/api/v4/projects/72936/issues/6/notes",
    "award_emoji": "https://jihulab.com/api/v4/projects/72936/issues/6/award_emoji",
    "project": "https://jihulab.com/api/v4/projects/72936",
    "closed_as_duplicate_of": null
  },
  "references": {"short": "#6", "relative": "#6", "full": "ultimate-plan/jihu-gitlab-app/demo-mr-test#6"},
  "severity": "UNKNOWN",
  "subscribed": true,
  "moved_to_id": null,
  "service_desk_reply_to": null,
  "epic_iid": null,
  "epic": null,
  "iteration": null,
  "health_status": null
};

var issueDataWithNoneLabels = {
  "id": 265567,
  "iid": 6,
  "project_id": 72936,
  "title": "回复评论",
  "description": "issue description for test",
  "state": "opened",
  "created_at": "2022-11-25T14:19:30.461+08:00",
  "updated_at": "2022-12-09T14:10:31.987+08:00",
  "closed_at": null,
  "closed_by": null,
  "labels": [],
  "milestone": null,
  "assignees": [],
  "author": {
    "id": 29355,
    "username": "perity",
    "name": "miaolu",
    "state": "active",
    "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29355/avatar.png",
    "web_url": "https://jihulab.com/perity"
  },
  "type": "ISSUE",
  "assignee": {
    "id": 29355,
    "username": "assignee-username",
    "name": "AssigneeName",
    "state": "active",
    "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29355/avatar.png",
    "web_url": "https://jihulab.com/perity"
  },
  "user_notes_count": 21,
  "merge_requests_count": 0,
  "upvotes": 0,
  "downvotes": 0,
  "due_date": null,
  "confidential": true,
  "discussion_locked": null,
  "issue_type": "issue",
  "web_url": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo-mr-test/-/issues/6",
  "time_stats": {"time_estimate": 0, "total_time_spent": 0, "human_time_estimate": null, "human_total_time_spent": null},
  "task_completion_status": {"count": 0, "completed_count": 0},
  "weight": null,
  "blocking_issues_count": 0,
  "has_tasks": false,
  "_links": {
    "self": "https://jihulab.com/api/v4/projects/72936/issues/6",
    "notes": "https://jihulab.com/api/v4/projects/72936/issues/6/notes",
    "award_emoji": "https://jihulab.com/api/v4/projects/72936/issues/6/award_emoji",
    "project": "https://jihulab.com/api/v4/projects/72936",
    "closed_as_duplicate_of": null
  },
  "references": {"short": "#6", "relative": "#6", "full": "ultimate-plan/jihu-gitlab-app/demo-mr-test#6"},
  "severity": "UNKNOWN",
  "subscribed": true,
  "moved_to_id": null,
  "service_desk_reply_to": null,
  "epic_iid": null,
  "epic": null,
  "iteration": null,
  "health_status": null
};

Map<String, dynamic> issueDetailsGraphQLResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "name": "demo mr test",
      "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab App",
      "path": "demo-mr-test",
      "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
      "issue": {
        "id": "gid://gitlab/Issue/323380",
        "iid": "640",
        "projectId": 59893,
        "title": "回复评论",
        "description": "issue description for test",
        "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/issues/640",
        "confidential": true,
        "state": "opened",
        "createdAt": "2022-11-25T14:19:30.461+08:00",
        "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "miaolu", "username": "perity"},
        "assignees": {
          "nodes": [
            {"id": "gid://gitlab/User/30192", "avatarUrl": "/uploads/-/system/user/avatar/30192/avatar.png", "name": "AssigneeName", "username": "assignee-username"}
          ]
        },
        "labels": {
          "nodes": [
            {"id": "gid://gitlab/ProjectLabel/2", "title": "P::M", "description": "Must Have", "color": "#6699cc", "textColor": "#FFFFFF"}
          ]
        },
        "milestone": {"id": "gid://gitlab/Milestone/6674", "title": "里程碑1"}
      }
    }
  }
};

Map<String, dynamic> issueDetailsWithNoneLabelsGraphQLResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "name": "demo mr test",
      "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab App",
      "path": "demo-mr-test",
      "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
      "issue": {
        "id": "gid://gitlab/Issue/323380",
        "iid": "640",
        "projectId": 59893,
        "title": "回复评论",
        "description": "issue description for test",
        "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/issues/640",
        "confidential": true,
        "state": "opened",
        "createdAt": "2022-11-25T14:19:30.461+08:00",
        "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "miaolu", "username": "perity"},
        "assignees": {
          "nodes": [
            {"id": "gid://gitlab/User/30192", "avatarUrl": "/uploads/-/system/user/avatar/30192/avatar.png", "name": "AssigneeName", "username": "assignee-username"}
          ]
        },
        "labels": {"nodes": []}
      }
    }
  }
};

Map<String, dynamic> projectIssuesGraphQLResponseData = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "path": "jihu-gitlab-app",
      "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
      "name": "极狐 GitLab APP 代码",
      "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码",
      "issues": {
        "nodes": [
          {
            "id": "gid://gitlab/Issue/332648",
            "iid": "276",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature：交互优化：创建议题时，如果只有一个项目，则跳过「选择项目」",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-27T14:32:57+08:00",
            "updatedAt": "2023-02-27T15:46:25+08:00",
            "assignees": {
              "nodes": [
                {"id": "gid://gitlab/User/29758", "name": "ling zhang", "username": "zhangling", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png"},
                {"id": "gid://gitlab/User/29759", "name": "ling zhang", "username": "zhangling", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png"}
              ]
            }
          },
          {
            "id": "gid://gitlab/Issue/332467",
            "iid": "274",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 待办列表无数据时，点击页面任意位置刷新",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/29064", "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png", "name": "Neil Wang", "username": "NeilWang"},
            "createdAt": "2023-02-27T10:58:12+08:00",
            "updatedAt": "2023-02-27T15:45:27+08:00"
          },
          {
            "id": "gid://gitlab/Issue/332407",
            "iid": "264",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 开发人员查看流水线自动合并",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-27T10:26:01+08:00",
            "updatedAt": "2023-02-27T14:35:16+08:00"
          },
          {
            "id": "gid://gitlab/Issue/332388",
            "iid": "710",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户更新帖子 type",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-27T09:55:01+08:00",
            "updatedAt": "2023-02-27T09:55:01+08:00"
          },
          {
            "id": "gid://gitlab/Issue/332291",
            "iid": "709",
            "state": "opened",
            "projectId": 59893,
            "title": "Retro10: BA 在下周一 IPM 会议时使用蓝湖演示设计稿",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-27T07:26:35+08:00",
            "updatedAt": "2023-02-27T14:25:32+08:00"
          },
          {
            "id": "gid://gitlab/Issue/332290",
            "iid": "708",
            "state": "opened",
            "projectId": 59893,
            "title": "Retro10: BA 创建 bug 描述模版",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-27T07:26:23+08:00",
            "updatedAt": "2023-02-27T14:25:37+08:00"
          },
          {
            "id": "gid://gitlab/Issue/332289",
            "iid": "707",
            "state": "opened",
            "projectId": 59893,
            "title": "Retro10: 测试未覆盖的业务需要建卡跟踪",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-27T07:26:07+08:00",
            "updatedAt": "2023-02-27T14:25:45+08:00"
          },
          {
            "id": "gid://gitlab/Issue/332288",
            "iid": "706",
            "state": "opened",
            "projectId": 59893,
            "title": "Retro10: 故事卡估点时需要考虑功能影响范围及风险",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-27T07:25:54+08:00",
            "updatedAt": "2023-02-27T14:26:05+08:00"
          },
          {
            "id": "gid://gitlab/Issue/332287",
            "iid": "705",
            "state": "closed",
            "projectId": 59893,
            "title": "Retro10: 唯力在回顾会结束后预约多账号实现方案同步会",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-27T07:25:39+08:00",
            "updatedAt": "2023-02-27T15:24:46+08:00"
          },
          {
            "id": "gid://gitlab/Issue/332286",
            "iid": "704",
            "state": "opened",
            "projectId": 59893,
            "title": "Retro: 当重构花费时间大于1天时，需要重新评估",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-27T07:25:15+08:00",
            "updatedAt": "2023-02-27T07:42:14+08:00"
          },
          {
            "id": "gid://gitlab/Issue/332285",
            "iid": "703",
            "state": "closed",
            "projectId": 59893,
            "title": "Retro: BA 在迭代结束后将未完成的卡拖到 PB",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-27T07:24:40+08:00",
            "updatedAt": "2023-02-27T10:04:04+08:00"
          },
          {
            "id": "gid://gitlab/Issue/331239",
            "iid": "702",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户为帖子点赞/踩/取消",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-25T18:16:11+08:00",
            "updatedAt": "2023-02-27T08:27:44+08:00"
          },
          {
            "id": "gid://gitlab/Issue/331238",
            "iid": "701",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 管理员删除别人的帖子",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-25T18:11:23+08:00",
            "updatedAt": "2023-02-27T14:35:01+08:00"
          },
          {
            "id": "gid://gitlab/Issue/331207",
            "iid": "700",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户未登录 jihulab 时浏览帖子评论看到登录提示",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-25T17:59:27+08:00",
            "updatedAt": "2023-02-27T14:58:28+08:00"
          },
          {
            "id": "gid://gitlab/Issue/330495",
            "iid": "699",
            "state": "opened",
            "projectId": 59893,
            "title": "Bugfix: 多账户-私有化登录后不显示登录信息",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-24T20:02:01+08:00",
            "updatedAt": "2023-02-27T08:07:43+08:00"
          },
          {
            "id": "gid://gitlab/Issue/330476",
            "iid": "698",
            "state": "opened",
            "projectId": 59893,
            "title": "远程办公下班打卡",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/30192", "avatarUrl": "/uploads/-/system/user/avatar/30192/avatar.png", "name": "Raymond Liao", "username": "raymond-liao"},
            "createdAt": "2023-02-24T19:04:31+08:00",
            "updatedAt": "2023-02-24T19:58:37+08:00"
          },
          {
            "id": "gid://gitlab/Issue/330460",
            "iid": "697",
            "state": "opened",
            "projectId": 59893,
            "title": "Bugfix: MR Changes 代码与行号没对齐",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/30192", "avatarUrl": "/uploads/-/system/user/avatar/30192/avatar.png", "name": "Raymond Liao", "username": "raymond-liao"},
            "createdAt": "2023-02-24T18:39:27+08:00",
            "updatedAt": "2023-02-24T18:39:27+08:00"
          },
          {
            "id": "gid://gitlab/Issue/330436",
            "iid": "696",
            "state": "opened",
            "projectId": 59893,
            "title": "Perf: 用户新增帖子后可以在列表看到帖子",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-24T18:06:55+08:00",
            "updatedAt": "2023-02-24T18:06:55+08:00"
          },
          {
            "id": "gid://gitlab/Issue/330427",
            "iid": "695",
            "state": "opened",
            "projectId": 59893,
            "title": "Task: 垚雨在#576卡补充已完成的 MR 业务逻辑",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-24T17:53:52+08:00",
            "updatedAt": "2023-02-24T17:55:36+08:00"
          },
          {
            "id": "gid://gitlab/Issue/330426",
            "iid": "694",
            "state": "opened",
            "projectId": 59893,
            "title": "Task: 唯力在#576卡补充已完成的 MR 业务逻辑",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-24T17:53:43+08:00",
            "updatedAt": "2023-02-24T17:55:38+08:00"
          }
        ],
        "pageInfo": {"hasNextPage": true, "endCursor": "end_cursor"}
      }
    }
  }
};

Map<String, dynamic> projectIssuesNextPageGraphQLResponseData = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "path": "jihu-gitlab-app",
      "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
      "name": "极狐 GitLab APP 代码",
      "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码",
      "issues": {
        "nodes": [
          {
            "id": "gid://gitlab/Issue/332648",
            "iid": "276",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature：交互优化：创建议题时，如果只有一个项目，则跳过「选择项目」2",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-27T14:32:57+08:00",
            "updatedAt": "2023-02-27T15:46:25+08:00"
          },
          {
            "id": "gid://gitlab/Issue/332467",
            "iid": "274",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 待办列表无数据时，点击页面任意位置刷新2",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/29064", "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png", "name": "Neil Wang", "username": "NeilWang"},
            "createdAt": "2023-02-27T10:58:12+08:00",
            "updatedAt": "2023-02-27T15:45:27+08:00"
          },
          {
            "id": "gid://gitlab/Issue/332407",
            "iid": "264",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 开发人员查看流水线自动合并2",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-27T10:26:01+08:00",
            "updatedAt": "2023-02-27T14:35:16+08:00"
          },
          {
            "id": "gid://gitlab/Issue/332388",
            "iid": "710",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户更新帖子 type2",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-27T09:55:01+08:00",
            "updatedAt": "2023-02-27T09:55:01+08:00"
          },
          {
            "id": "gid://gitlab/Issue/332291",
            "iid": "709",
            "state": "opened",
            "projectId": 59893,
            "title": "Retro10: BA 在下周一 IPM 会议时使用蓝湖演示设计稿2",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-27T07:26:35+08:00",
            "updatedAt": "2023-02-27T14:25:32+08:00"
          },
          {
            "id": "gid://gitlab/Issue/332290",
            "iid": "708",
            "state": "opened",
            "projectId": 59893,
            "title": "Retro10: BA 创建 bug 描述模版",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-27T07:26:23+08:00",
            "updatedAt": "2023-02-27T14:25:37+08:00"
          },
          {
            "id": "gid://gitlab/Issue/332289",
            "iid": "707",
            "state": "opened",
            "projectId": 59893,
            "title": "Retro10: 测试未覆盖的业务需要建卡跟踪",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-27T07:26:07+08:00",
            "updatedAt": "2023-02-27T14:25:45+08:00"
          },
          {
            "id": "gid://gitlab/Issue/332288",
            "iid": "706",
            "state": "opened",
            "projectId": 59893,
            "title": "Retro10: 故事卡估点时需要考虑功能影响范围及风险",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-27T07:25:54+08:00",
            "updatedAt": "2023-02-27T14:26:05+08:00"
          },
          {
            "id": "gid://gitlab/Issue/332287",
            "iid": "705",
            "state": "closed",
            "projectId": 59893,
            "title": "Retro10: 唯力在回顾会结束后预约多账号实现方案同步会",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-27T07:25:39+08:00",
            "updatedAt": "2023-02-27T15:24:46+08:00"
          },
          {
            "id": "gid://gitlab/Issue/332286",
            "iid": "704",
            "state": "opened",
            "projectId": 59893,
            "title": "Retro: 当重构花费时间大于1天时，需要重新评估",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-27T07:25:15+08:00",
            "updatedAt": "2023-02-27T07:42:14+08:00"
          },
          {
            "id": "gid://gitlab/Issue/332285",
            "iid": "703",
            "state": "closed",
            "projectId": 59893,
            "title": "Retro: BA 在迭代结束后将未完成的卡拖到 PB",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-27T07:24:40+08:00",
            "updatedAt": "2023-02-27T10:04:04+08:00"
          },
          {
            "id": "gid://gitlab/Issue/331239",
            "iid": "702",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户为帖子点赞/踩/取消",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-25T18:16:11+08:00",
            "updatedAt": "2023-02-27T08:27:44+08:00"
          },
          {
            "id": "gid://gitlab/Issue/331238",
            "iid": "701",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 管理员删除别人的帖子",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-25T18:11:23+08:00",
            "updatedAt": "2023-02-27T14:35:01+08:00"
          },
          {
            "id": "gid://gitlab/Issue/331207",
            "iid": "700",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户未登录 jihulab 时浏览帖子评论看到登录提示",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-25T17:59:27+08:00",
            "updatedAt": "2023-02-27T14:58:28+08:00"
          },
          {
            "id": "gid://gitlab/Issue/330495",
            "iid": "699",
            "state": "opened",
            "projectId": 59893,
            "title": "Bugfix: 多账户-私有化登录后不显示登录信息",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-24T20:02:01+08:00",
            "updatedAt": "2023-02-27T08:07:43+08:00"
          },
          {
            "id": "gid://gitlab/Issue/330476",
            "iid": "698",
            "state": "opened",
            "projectId": 59893,
            "title": "远程办公下班打卡",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/30192", "avatarUrl": "/uploads/-/system/user/avatar/30192/avatar.png", "name": "Raymond Liao", "username": "raymond-liao"},
            "createdAt": "2023-02-24T19:04:31+08:00",
            "updatedAt": "2023-02-24T19:58:37+08:00"
          },
          {
            "id": "gid://gitlab/Issue/330460",
            "iid": "697",
            "state": "opened",
            "projectId": 59893,
            "title": "Bugfix: MR Changes 代码与行号没对齐",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/30192", "avatarUrl": "/uploads/-/system/user/avatar/30192/avatar.png", "name": "Raymond Liao", "username": "raymond-liao"},
            "createdAt": "2023-02-24T18:39:27+08:00",
            "updatedAt": "2023-02-24T18:39:27+08:00"
          },
          {
            "id": "gid://gitlab/Issue/330436",
            "iid": "696",
            "state": "opened",
            "projectId": 59893,
            "title": "Perf: 用户新增帖子后可以在列表看到帖子",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-24T18:06:55+08:00",
            "updatedAt": "2023-02-24T18:06:55+08:00"
          },
          {
            "id": "gid://gitlab/Issue/330427",
            "iid": "695",
            "state": "opened",
            "projectId": 59893,
            "title": "Task: 垚雨在#576卡补充已完成的 MR 业务逻辑",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-24T17:53:52+08:00",
            "updatedAt": "2023-02-24T17:55:36+08:00"
          },
          {
            "id": "gid://gitlab/Issue/330426",
            "iid": "694",
            "state": "opened",
            "projectId": 59893,
            "title": "Task: 唯力在#576卡补充已完成的 MR 业务逻辑",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-24T17:53:43+08:00",
            "updatedAt": "2023-02-24T17:55:38+08:00"
          }
        ],
        "pageInfo": {"hasNextPage": true, "endCursor": "end_cursor2"}
      }
    }
  }
};

Map<String, dynamic> groupIssuesGraphQLResponseData = {
  "data": {
    "group": {
      "id": "gid://gitlab/Group/88966",
      "fullName": "旗舰版演示 / 极狐 GitLab App 产品线",
      "name": "极狐 GitLab App 产品线",
      "path": "jihu-gitlab-app",
      "issues": {
        "pageInfo": {"hasNextPage": true, "startCursor": "start_cursor", "endCursor": "end_cursor"},
        "nodes": [
          {
            "id": "gid://gitlab/Issue/312805",
            "iid": "477",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户可以为评论点赞/反对/取消",
            "type": "ISSUE",
            "weight": 1,
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-02T08:57:28+08:00",
            "updatedAt": "2023-02-02T08:57:28+08:00"
          },
          {
            "id": "gid://gitlab/Issue/312802",
            "iid": "476",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 删除议题评论",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "weight": 1,
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-02T08:50:59+08:00",
            "updatedAt": "2023-02-02T08:50:59+08:00"
          },
          {
            "id": "gid://gitlab/Issue/312801",
            "iid": "475",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 修改议题评论",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-02T08:49:14+08:00",
            "updatedAt": "2023-02-02T08:49:14+08:00"
          },
          {
            "id": "gid://gitlab/Issue/312309",
            "iid": "474",
            "state": "opened",
            "projectId": 59893,
            "title": "Bugfix:议题详情视频与图片显示问题",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-01T16:27:21+08:00",
            "updatedAt": "2023-02-01T16:27:44+08:00"
          },
          {
            "id": "gid://gitlab/Issue/311926",
            "iid": "473",
            "state": "opened",
            "projectId": 59893,
            "title": "Bugfix: 中文语言下，精确时间未翻译",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-01T13:35:49+08:00",
            "updatedAt": "2023-02-01T16:27:57+08:00"
          },
          {
            "id": "gid://gitlab/Issue/311387",
            "iid": "471",
            "state": "opened",
            "projectId": 59893,
            "title": "Bugfix: To-Do List 空页面的文案未跟随语言切换",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-31T17:54:50+08:00",
            "updatedAt": "2023-02-01T22:07:51+08:00"
          },
          {
            "id": "gid://gitlab/Issue/311271",
            "iid": "39",
            "state": "opened",
            "projectId": 72936,
            "title": "测试关联议题",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/72936",
                "path": "demo-mr-test",
                "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
                "name": "demo mr test",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
              }
            },
            "author": {"id": "gid://gitlab/User/29758", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png", "name": "ling zhang", "username": "zhangling"},
            "createdAt": "2023-01-31T16:26:12+08:00",
            "updatedAt": "2023-02-01T11:33:32+08:00"
          },
          {
            "id": "gid://gitlab/Issue/311179",
            "iid": "470",
            "state": "opened",
            "projectId": 59893,
            "title": "Refactor: Use GraphQL Client in project",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/30192", "avatarUrl": "/uploads/-/system/user/avatar/30192/avatar.png", "name": "Raymond Liao", "username": "raymond-liao"},
            "createdAt": "2023-01-31T14:21:28+08:00",
            "updatedAt": "2023-01-31T14:28:34+08:00"
          },
          {
            "id": "gid://gitlab/Issue/311066",
            "iid": "469",
            "state": "opened",
            "projectId": 59893,
            "title": "Bugfix: iPad 待办列表未自动加载数据",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-31T11:43:37+08:00",
            "updatedAt": "2023-01-31T11:52:47+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310809",
            "iid": "466",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 平板用户打开 app 时显示上次退出时选 Starred 页面",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T18:47:49+08:00",
            "updatedAt": "2023-01-31T17:05:11+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310732",
            "iid": "464",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户编辑议题时可以保存为草稿",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T17:42:54+08:00",
            "updatedAt": "2023-01-30T17:42:54+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310555",
            "iid": "463",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户在 iPad 浏览 groups 页面",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T15:48:40+08:00",
            "updatedAt": "2023-02-01T19:33:39+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310554",
            "iid": "462",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户更新议题 assignee",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T15:46:07+08:00",
            "updatedAt": "2023-01-31T10:55:15+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310553",
            "iid": "461",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户更新议题 label",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T15:45:39+08:00",
            "updatedAt": "2023-02-02T08:46:09+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310386",
            "iid": "457",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 统一超链接样式",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T11:57:50+08:00",
            "updatedAt": "2023-02-02T08:12:28+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310346",
            "iid": "453",
            "state": "opened",
            "projectId": 59893,
            "title": "Spike: 平板两列布局最右侧页面打开详情",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T11:26:21+08:00",
            "updatedAt": "2023-02-02T09:40:43+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310344",
            "iid": "452",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 平板用户浏览关联议题详情",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T11:19:13+08:00",
            "updatedAt": "2023-01-30T14:49:29+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310312",
            "iid": "451",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户查看不同状态的议题数量",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T11:09:17+08:00",
            "updatedAt": "2023-02-02T08:44:47+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310295",
            "iid": "450",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户查看所有议题列表",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T10:56:14+08:00",
            "updatedAt": "2023-02-02T08:45:11+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310292",
            "iid": "449",
            "state": "opened",
            "projectId": 59893,
            "title": "Spike: 墓碑调研",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T10:43:09+08:00",
            "updatedAt": "2023-01-30T10:43:09+08:00"
          }
        ]
      }
    }
  }
};

Map<String, dynamic> groupIssuesCeGraphQLResponseData = {
  "data": {
    "group": {
      "id": "gid://gitlab/Group/88966",
      "fullName": "旗舰版演示 / 极狐 GitLab App 产品线",
      "name": "极狐 GitLab App 产品线",
      "path": "jihu-gitlab-app",
      "issues": {
        "pageInfo": {"hasNextPage": true, "startCursor": "start_cursor", "endCursor": "end_cursor"},
        "nodes": [
          {
            "id": "gid://gitlab/Issue/312805",
            "iid": "477",
            "state": "opened",
            "title": "Feature: 用户可以为评论点赞/反对/取消",
            "type": "ISSUE",
            "weight": 1,
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-02T08:57:28+08:00",
            "updatedAt": "2023-02-02T08:57:28+08:00"
          },
          {
            "id": "gid://gitlab/Issue/312802",
            "iid": "476",
            "state": "opened",
            "title": "Feature: 删除议题评论",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "weight": 1,
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-02T08:50:59+08:00",
            "updatedAt": "2023-02-02T08:50:59+08:00"
          },
          {
            "id": "gid://gitlab/Issue/312801",
            "iid": "475",
            "state": "opened",
            "title": "Feature: 修改议题评论",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-02T08:49:14+08:00",
            "updatedAt": "2023-02-02T08:49:14+08:00"
          },
          {
            "id": "gid://gitlab/Issue/312309",
            "iid": "474",
            "state": "opened",
            "title": "Bugfix:议题详情视频与图片显示问题",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-01T16:27:21+08:00",
            "updatedAt": "2023-02-01T16:27:44+08:00"
          },
          {
            "id": "gid://gitlab/Issue/311926",
            "iid": "473",
            "state": "opened",
            "title": "Bugfix: 中文语言下，精确时间未翻译",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-01T13:35:49+08:00",
            "updatedAt": "2023-02-01T16:27:57+08:00"
          },
          {
            "id": "gid://gitlab/Issue/311387",
            "iid": "471",
            "state": "opened",
            "title": "Bugfix: To-Do List 空页面的文案未跟随语言切换",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-31T17:54:50+08:00",
            "updatedAt": "2023-02-01T22:07:51+08:00"
          },
          {
            "id": "gid://gitlab/Issue/311271",
            "iid": "39",
            "state": "opened",
            "title": "测试关联议题",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/72936",
                "path": "demo-mr-test",
                "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
                "name": "demo mr test",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
              }
            },
            "author": {"id": "gid://gitlab/User/29758", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png", "name": "ling zhang", "username": "zhangling"},
            "createdAt": "2023-01-31T16:26:12+08:00",
            "updatedAt": "2023-02-01T11:33:32+08:00"
          },
          {
            "id": "gid://gitlab/Issue/311179",
            "iid": "470",
            "state": "opened",
            "title": "Refactor: Use GraphQL Client in project",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/30192", "avatarUrl": "/uploads/-/system/user/avatar/30192/avatar.png", "name": "Raymond Liao", "username": "raymond-liao"},
            "createdAt": "2023-01-31T14:21:28+08:00",
            "updatedAt": "2023-01-31T14:28:34+08:00"
          },
          {
            "id": "gid://gitlab/Issue/311066",
            "iid": "469",
            "state": "opened",
            "title": "Bugfix: iPad 待办列表未自动加载数据",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-31T11:43:37+08:00",
            "updatedAt": "2023-01-31T11:52:47+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310809",
            "iid": "466",
            "state": "opened",
            "title": "Feature: 平板用户打开 app 时显示上次退出时选 Starred 页面",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T18:47:49+08:00",
            "updatedAt": "2023-01-31T17:05:11+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310732",
            "iid": "464",
            "state": "opened",
            "title": "Feature: 用户编辑议题时可以保存为草稿",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T17:42:54+08:00",
            "updatedAt": "2023-01-30T17:42:54+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310555",
            "iid": "463",
            "state": "opened",
            "title": "Feature: 用户在 iPad 浏览 groups 页面",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T15:48:40+08:00",
            "updatedAt": "2023-02-01T19:33:39+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310554",
            "iid": "462",
            "state": "opened",
            "title": "Feature: 用户更新议题 assignee",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T15:46:07+08:00",
            "updatedAt": "2023-01-31T10:55:15+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310553",
            "iid": "461",
            "state": "opened",
            "title": "Feature: 用户更新议题 label",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T15:45:39+08:00",
            "updatedAt": "2023-02-02T08:46:09+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310386",
            "iid": "457",
            "state": "opened",
            "title": "Feature: 统一超链接样式",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T11:57:50+08:00",
            "updatedAt": "2023-02-02T08:12:28+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310346",
            "iid": "453",
            "state": "opened",
            "title": "Spike: 平板两列布局最右侧页面打开详情",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T11:26:21+08:00",
            "updatedAt": "2023-02-02T09:40:43+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310344",
            "iid": "452",
            "state": "opened",
            "title": "Feature: 平板用户浏览关联议题详情",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T11:19:13+08:00",
            "updatedAt": "2023-01-30T14:49:29+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310312",
            "iid": "451",
            "state": "opened",
            "title": "Feature: 用户查看不同状态的议题数量",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T11:09:17+08:00",
            "updatedAt": "2023-02-02T08:44:47+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310295",
            "iid": "450",
            "state": "opened",
            "title": "Feature: 用户查看所有议题列表",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T10:56:14+08:00",
            "updatedAt": "2023-02-02T08:45:11+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310292",
            "iid": "449",
            "state": "opened",
            "title": "Spike: 墓碑调研",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T10:43:09+08:00",
            "updatedAt": "2023-01-30T10:43:09+08:00"
          }
        ]
      }
    }
  }
};
List groupIssuesCeGraphQLProjectResponseData = [
  {"iid": 477, "project_id": 59893},
  {"iid": 476, "project_id": 59893},
  {"iid": 475, "project_id": 59893},
  {"iid": 474, "project_id": 59893},
  {"iid": 473, "project_id": 72936},
  {"iid": 471, "project_id": 59893},
  {"iid": 39, "project_id": 59893},
  {"iid": 470, "project_id": 59893},
  {"iid": 469, "project_id": 59893},
  {"iid": 466, "project_id": 59893},
  {"iid": 464, "project_id": 59893},
  {"iid": 463, "project_id": 59893},
  {"iid": 462, "project_id": 59893},
  {"iid": 461, "project_id": 59893},
  {"iid": 457, "project_id": 59893},
  {"iid": 453, "project_id": 59893},
  {"iid": 452, "project_id": 59893},
  {"iid": 451, "project_id": 59893},
  {"iid": 450, "project_id": 59893},
  {"iid": 449, "project_id": 59893},
];

Map<String, dynamic> groupIssuesGraphQLNextPageResponseData = {
  "data": {
    "group": {
      "id": "gid://gitlab/Group/88966",
      "fullName": "旗舰版演示 / 极狐 GitLab App 产品线",
      "name": "极狐 GitLab App 产品线",
      "path": "jihu-gitlab-app",
      "issues": {
        "pageInfo": {"hasNextPage": false, "startCursor": "start_cursor", "endCursor": "end_cursor"},
        "nodes": [
          {
            "id": "gid://gitlab/Issue/312805",
            "iid": "477",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户可以为评论点赞/反对/取消2",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-02T08:57:28+08:00",
            "updatedAt": "2023-02-02T08:57:28+08:00"
          },
          {
            "id": "gid://gitlab/Issue/312802",
            "iid": "476",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 删除议题评论2",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-02T08:50:59+08:00",
            "updatedAt": "2023-02-02T08:50:59+08:00"
          },
          {
            "id": "gid://gitlab/Issue/312801",
            "iid": "475",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 修改议题评论",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-02T08:49:14+08:00",
            "updatedAt": "2023-02-02T08:49:14+08:00"
          },
          {
            "id": "gid://gitlab/Issue/312309",
            "iid": "474",
            "state": "opened",
            "projectId": 59893,
            "title": "Bugfix:议题详情视频与图片显示问题",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-01T16:27:21+08:00",
            "updatedAt": "2023-02-01T16:27:44+08:00"
          },
          {
            "id": "gid://gitlab/Issue/311926",
            "iid": "473",
            "state": "opened",
            "projectId": 59893,
            "title": "Bugfix: 中文语言下，精确时间未翻译",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-01T13:35:49+08:00",
            "updatedAt": "2023-02-01T16:27:57+08:00"
          },
          {
            "id": "gid://gitlab/Issue/311387",
            "iid": "471",
            "state": "opened",
            "projectId": 59893,
            "title": "Bugfix: To-Do List 空页面的文案未跟随语言切换",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-31T17:54:50+08:00",
            "updatedAt": "2023-02-01T22:07:51+08:00"
          },
          {
            "id": "gid://gitlab/Issue/311271",
            "iid": "39",
            "state": "opened",
            "projectId": 72936,
            "title": "测试关联议题",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/72936",
                "path": "demo-mr-test",
                "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
                "name": "demo mr test",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
              }
            },
            "author": {"id": "gid://gitlab/User/29758", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png", "name": "ling zhang", "username": "zhangling"},
            "createdAt": "2023-01-31T16:26:12+08:00",
            "updatedAt": "2023-02-01T11:33:32+08:00"
          },
          {
            "id": "gid://gitlab/Issue/311179",
            "iid": "470",
            "state": "opened",
            "projectId": 59893,
            "title": "Refactor: Use GraphQL Client in project",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/30192", "avatarUrl": "/uploads/-/system/user/avatar/30192/avatar.png", "name": "Raymond Liao", "username": "raymond-liao"},
            "createdAt": "2023-01-31T14:21:28+08:00",
            "updatedAt": "2023-01-31T14:28:34+08:00"
          },
          {
            "id": "gid://gitlab/Issue/311066",
            "iid": "469",
            "state": "opened",
            "projectId": 59893,
            "title": "Bugfix: iPad 待办列表未自动加载数据",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-31T11:43:37+08:00",
            "updatedAt": "2023-01-31T11:52:47+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310809",
            "iid": "466",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 平板用户打开 app 时显示上次退出时选 Starred 页面",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T18:47:49+08:00",
            "updatedAt": "2023-01-31T17:05:11+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310732",
            "iid": "464",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户编辑议题时可以保存为草稿",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T17:42:54+08:00",
            "updatedAt": "2023-01-30T17:42:54+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310555",
            "iid": "463",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户在 iPad 浏览 groups 页面",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T15:48:40+08:00",
            "updatedAt": "2023-02-01T19:33:39+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310554",
            "iid": "462",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户更新议题 assignee",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T15:46:07+08:00",
            "updatedAt": "2023-01-31T10:55:15+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310553",
            "iid": "461",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户更新议题 label",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T15:45:39+08:00",
            "updatedAt": "2023-02-02T08:46:09+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310386",
            "iid": "457",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 统一超链接样式",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T11:57:50+08:00",
            "updatedAt": "2023-02-02T08:12:28+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310346",
            "iid": "453",
            "state": "opened",
            "projectId": 59893,
            "title": "Spike: 平板两列布局最右侧页面打开详情",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T11:26:21+08:00",
            "updatedAt": "2023-02-02T09:40:43+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310344",
            "iid": "452",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 平板用户浏览关联议题详情",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T11:19:13+08:00",
            "updatedAt": "2023-01-30T14:49:29+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310312",
            "iid": "451",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户查看不同状态的议题数量",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T11:09:17+08:00",
            "updatedAt": "2023-02-02T08:44:47+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310295",
            "iid": "450",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户查看所有议题列表",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T10:56:14+08:00",
            "updatedAt": "2023-02-02T08:45:11+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310292",
            "iid": "449",
            "state": "opened",
            "projectId": 59893,
            "title": "Spike: 墓碑调研",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T10:43:09+08:00",
            "updatedAt": "2023-01-30T10:43:09+08:00"
          },
          {
            "id": "gid://gitlab/Issue/312805",
            "iid": "477",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户可以为评论点赞/反对/取消 01",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-02T08:57:28+08:00",
            "updatedAt": "2023-02-02T08:57:28+08:00"
          },
          {
            "id": "gid://gitlab/Issue/312802",
            "iid": "476",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 删除议题评论 01",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-02T08:50:59+08:00",
            "updatedAt": "2023-02-02T08:50:59+08:00"
          },
          {
            "id": "gid://gitlab/Issue/312801",
            "iid": "475",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 修改议题评论 01",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-02T08:49:14+08:00",
            "updatedAt": "2023-02-02T08:49:14+08:00"
          },
          {
            "id": "gid://gitlab/Issue/312309",
            "iid": "474",
            "state": "opened",
            "projectId": 59893,
            "title": "Bugfix:议题详情视频与图片显示问题 01",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-01T16:27:21+08:00",
            "updatedAt": "2023-02-01T16:27:44+08:00"
          },
          {
            "id": "gid://gitlab/Issue/311926",
            "iid": "473",
            "state": "opened",
            "projectId": 59893,
            "title": "Bugfix: 中文语言下，精确时间未翻译 01",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-02-01T13:35:49+08:00",
            "updatedAt": "2023-02-01T16:27:57+08:00"
          },
          {
            "id": "gid://gitlab/Issue/311387",
            "iid": "471",
            "state": "opened",
            "projectId": 59893,
            "title": "Bugfix: To-Do List 空页面的文案未跟随语言切换 01",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-31T17:54:50+08:00",
            "updatedAt": "2023-02-01T22:07:51+08:00"
          },
          {
            "id": "gid://gitlab/Issue/311271",
            "iid": "39",
            "state": "opened",
            "projectId": 72936,
            "title": "测试关联议题 01",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/72936",
                "path": "demo-mr-test",
                "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
                "name": "demo mr test",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
              }
            },
            "author": {"id": "gid://gitlab/User/29758", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png", "name": "ling zhang", "username": "zhangling"},
            "createdAt": "2023-01-31T16:26:12+08:00",
            "updatedAt": "2023-02-01T11:33:32+08:00"
          },
          {
            "id": "gid://gitlab/Issue/311179",
            "iid": "470",
            "state": "opened",
            "projectId": 59893,
            "title": "Refactor: Use GraphQL Client in project 01",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/30192", "avatarUrl": "/uploads/-/system/user/avatar/30192/avatar.png", "name": "Raymond Liao", "username": "raymond-liao"},
            "createdAt": "2023-01-31T14:21:28+08:00",
            "updatedAt": "2023-01-31T14:28:34+08:00"
          },
          {
            "id": "gid://gitlab/Issue/311066",
            "iid": "469",
            "state": "opened",
            "projectId": 59893,
            "title": "Bugfix: iPad 待办列表未自动加载数据 01",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-31T11:43:37+08:00",
            "updatedAt": "2023-01-31T11:52:47+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310809",
            "iid": "466",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 平板用户打开 app 时显示上次退出时选 Starred 页面 01",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T18:47:49+08:00",
            "updatedAt": "2023-01-31T17:05:11+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310732",
            "iid": "464",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户编辑议题时可以保存为草稿 01",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T17:42:54+08:00",
            "updatedAt": "2023-01-30T17:42:54+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310555",
            "iid": "463",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户在 iPad 浏览 groups 页面 01",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T15:48:40+08:00",
            "updatedAt": "2023-02-01T19:33:39+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310554",
            "iid": "462",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户更新议题 assignee 01",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T15:46:07+08:00",
            "updatedAt": "2023-01-31T10:55:15+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310553",
            "iid": "461",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户更新议题 label 01",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T15:45:39+08:00",
            "updatedAt": "2023-02-02T08:46:09+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310386",
            "iid": "457",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 统一超链接样式 01",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T11:57:50+08:00",
            "updatedAt": "2023-02-02T08:12:28+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310346",
            "iid": "453",
            "state": "opened",
            "projectId": 59893,
            "title": "Spike: 平板两列布局最右侧页面打开详情 01",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T11:26:21+08:00",
            "updatedAt": "2023-02-02T09:40:43+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310344",
            "iid": "452",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 平板用户浏览关联议题详情 01",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T11:19:13+08:00",
            "updatedAt": "2023-01-30T14:49:29+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310312",
            "iid": "451",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户查看不同状态的议题数量 01",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T11:09:17+08:00",
            "updatedAt": "2023-02-02T08:44:47+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310295",
            "iid": "450",
            "state": "opened",
            "projectId": 59893,
            "title": "Feature: 用户查看所有议题列表 01",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T10:56:14+08:00",
            "updatedAt": "2023-02-02T08:45:11+08:00"
          },
          {
            "id": "gid://gitlab/Issue/310292",
            "iid": "449",
            "state": "opened",
            "projectId": 59893,
            "title": "Spike: 墓碑调研 01",
            "type": "ISSUE",
            "designCollection": {
              "project": {
                "id": "gid://gitlab/Project/59893",
                "path": "jihu-gitlab-app",
                "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
                "name": "极狐 GitLab APP 代码",
                "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
              }
            },
            "author": {"id": "gid://gitlab/User/23836", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "name": "yajie xue", "username": "jojo0"},
            "createdAt": "2023-01-30T10:43:09+08:00",
            "updatedAt": "2023-01-30T10:43:09+08:00"
          }
        ]
      }
    }
  }
};

Map<String, dynamic> groupProjectsGraphQLResponseData = {
  "data": {
    "group": {
      "id": "gid://gitlab/Group/118014",
      "projects": {
        "nodes": [
          {"id": "gid://gitlab/Project/69172", "path": "learn-gitlab", "fullPath": "highsof-t/learn-gitlab", "name": "Learn GitLab", "nameWithNamespace": "highsoft-for-test / Learn GitLab"},
          {"id": "gid://gitlab/Project/69171", "path": "demo", "fullPath": "highsof-t/demo", "name": "demo", "nameWithNamespace": "highsoft-for-test / demo"},
          {
            "id": "gid://gitlab/Project/59893",
            "path": "jihu-gitlab-app",
            "fullPath": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
            "name": "极狐 GitLab APP 代码",
            "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码"
          },
          {
            "id": "gid://gitlab/Project/72936",
            "path": "demo-mr-test",
            "fullPath": "ultimate-plan/jihu-gitlab-app/demo-mr-test",
            "name": "demo mr test",
            "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / demo mr test"
          }
        ]
      }
    }
  }
};

Map<String, dynamic> groupIssuesGraphQLEmptyResponseData = {
  "data": {
    "group": {
      "id": "gid://gitlab/Group/88966",
      "issues": {
        "pageInfo": {
          "hasNextPage": true,
          "startCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMi0xMi0yMiAyMDozNzo1Mi43MDc4ODIwMDAgKzA4MDAiLCJpZCI6IjI5NTYwNSJ9",
          "endCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMi0xMi0wNCAyMjo0NjowOS44MjQ3NjgwMDAgKzA4MDAiLCJpZCI6IjI4MzkwNCJ9"
        },
        "nodes": []
      }
    }
  }
};

Map<String, dynamic> faqIssuesGraphQLResponseData = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/89335",
      "issues": {
        "pageInfo": {
          "hasNextPage": true,
          "startCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMy0wMS0xOCAyMTozOToxMC43NzkwMDAwMDAgKzA4MDAiLCJpZCI6IjMxMzUyMCJ9",
          "endCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMi0xMi0wNyAwOTo0MzowMC4xNzUwMDAwMDAgKzA4MDAiLCJpZCI6IjMxMzUwMSJ9"
        },
        "nodes": [
          {
            "id": "gid://gitlab/Issue/313520",
            "iid": "218",
            "state": "opened",
            "projectId": 89335,
            "title": "极狐还招人吗？有什么远程岗位？",
            "type": "ISSUE",
            "designCollection": {
              "project": {"id": "gid://gitlab/Project/89335", "path": "faq", "fullPath": "ultimate-plan/jihu-gitlab-app/faq", "name": "FAQ", "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / FAQ"}
            },
            "author": {"id": "gid://gitlab/User/1795", "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png", "name": "Zhou YANG", "username": "sinkcup"},
            "createdAt": "2023-01-18T21:39:10+08:00",
            "updatedAt": "2023-01-18T21:39:12+08:00"
          },
          {
            "id": "gid://gitlab/Issue/313519",
            "iid": "217",
            "state": "opened",
            "projectId": 89335,
            "title": "极狐是一家什么样的公司？",
            "type": "ISSUE",
            "designCollection": {
              "project": {"id": "gid://gitlab/Project/89335", "path": "faq", "fullPath": "ultimate-plan/jihu-gitlab-app/faq", "name": "FAQ", "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / FAQ"}
            },
            "author": {"id": "gid://gitlab/User/1795", "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png", "name": "Zhou YANG", "username": "sinkcup"},
            "createdAt": "2023-01-18T15:58:07+08:00",
            "updatedAt": "2023-01-19T10:11:49+08:00"
          },
          {
            "id": "gid://gitlab/Issue/313518",
            "iid": "216",
            "state": "opened",
            "projectId": 89335,
            "title": "新年快乐",
            "type": "ISSUE",
            "designCollection": {
              "project": {"id": "gid://gitlab/Project/89335", "path": "faq", "fullPath": "ultimate-plan/jihu-gitlab-app/faq", "name": "FAQ", "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / FAQ"}
            },
            "author": {"id": "gid://gitlab/User/1795", "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png", "name": "Zhou YANG", "username": "sinkcup"},
            "createdAt": "2023-01-18T15:39:15+08:00",
            "updatedAt": "2023-01-30T10:25:02+08:00"
          },
          {
            "id": "gid://gitlab/Issue/313517",
            "iid": "215",
            "state": "opened",
            "projectId": 89335,
            "title": "社区版用户如何获得支持",
            "type": "ISSUE",
            "designCollection": {
              "project": {"id": "gid://gitlab/Project/89335", "path": "faq", "fullPath": "ultimate-plan/jihu-gitlab-app/faq", "name": "FAQ", "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / FAQ"}
            },
            "author": {"id": "gid://gitlab/User/1795", "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png", "name": "Zhou YANG", "username": "sinkcup"},
            "createdAt": "2023-01-16T15:26:32+08:00",
            "updatedAt": "2023-02-02T20:42:27+08:00"
          },
          {
            "id": "gid://gitlab/Issue/313516",
            "iid": "214",
            "state": "opened",
            "projectId": 89335,
            "title": "如何修复GitLab项目删除的500报错",
            "type": "ISSUE",
            "designCollection": {
              "project": {"id": "gid://gitlab/Project/89335", "path": "faq", "fullPath": "ultimate-plan/jihu-gitlab-app/faq", "name": "FAQ", "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / FAQ"}
            },
            "author": {"id": "gid://gitlab/User/1795", "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png", "name": "Zhou YANG", "username": "sinkcup"},
            "createdAt": "2023-01-16T15:22:30+08:00",
            "updatedAt": "2023-02-02T20:42:46+08:00"
          },
          {
            "id": "gid://gitlab/Issue/313515",
            "iid": "213",
            "state": "opened",
            "projectId": 89335,
            "title": "GitLab部署时是否可以使用如MySQL等其他关系型数据库替换PostgreSQL",
            "type": "ISSUE",
            "designCollection": {
              "project": {"id": "gid://gitlab/Project/89335", "path": "faq", "fullPath": "ultimate-plan/jihu-gitlab-app/faq", "name": "FAQ", "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / FAQ"}
            },
            "author": {"id": "gid://gitlab/User/1795", "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png", "name": "Zhou YANG", "username": "sinkcup"},
            "createdAt": "2023-01-16T09:23:26+08:00",
            "updatedAt": "2023-02-02T20:44:24+08:00"
          },
          {
            "id": "gid://gitlab/Issue/313514",
            "iid": "212",
            "state": "opened",
            "projectId": 89335,
            "title": "RedHat7.6第一次omnibus安装出现migrate gitlab-rails database错误",
            "type": "ISSUE",
            "designCollection": {
              "project": {"id": "gid://gitlab/Project/89335", "path": "faq", "fullPath": "ultimate-plan/jihu-gitlab-app/faq", "name": "FAQ", "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / FAQ"}
            },
            "author": {"id": "gid://gitlab/User/1795", "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png", "name": "Zhou YANG", "username": "sinkcup"},
            "createdAt": "2023-01-13T10:40:52+08:00",
            "updatedAt": "2023-01-14T17:54:20+08:00"
          },
          {
            "id": "gid://gitlab/Issue/313513",
            "iid": "211",
            "state": "opened",
            "projectId": 89335,
            "title": "议题（issue）支持上传视频吗？",
            "type": "ISSUE",
            "designCollection": {
              "project": {"id": "gid://gitlab/Project/89335", "path": "faq", "fullPath": "ultimate-plan/jihu-gitlab-app/faq", "name": "FAQ", "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / FAQ"}
            },
            "author": {"id": "gid://gitlab/User/1795", "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png", "name": "Zhou YANG", "username": "sinkcup"},
            "createdAt": "2023-01-12T17:27:46+08:00",
            "updatedAt": "2023-01-13T16:01:27+08:00"
          },
          {
            "id": "gid://gitlab/Issue/313512",
            "iid": "210",
            "state": "opened",
            "projectId": 89335,
            "title": "你是谁？多大年纪？",
            "type": "ISSUE",
            "designCollection": {
              "project": {"id": "gid://gitlab/Project/89335", "path": "faq", "fullPath": "ultimate-plan/jihu-gitlab-app/faq", "name": "FAQ", "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / FAQ"}
            },
            "author": {"id": "gid://gitlab/User/1795", "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png", "name": "Zhou YANG", "username": "sinkcup"},
            "createdAt": "2023-01-10T12:17:30+08:00",
            "updatedAt": "2023-01-18T22:10:53+08:00"
          },
          {
            "id": "gid://gitlab/Issue/313511",
            "iid": "209",
            "state": "opened",
            "projectId": 89335,
            "title": "Manifest导入的并行数量有上限设定",
            "type": "ISSUE",
            "designCollection": {
              "project": {"id": "gid://gitlab/Project/89335", "path": "faq", "fullPath": "ultimate-plan/jihu-gitlab-app/faq", "name": "FAQ", "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / FAQ"}
            },
            "author": {"id": "gid://gitlab/User/1795", "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png", "name": "Zhou YANG", "username": "sinkcup"},
            "createdAt": "2023-01-05T18:24:27+08:00",
            "updatedAt": "2023-02-02T20:44:29+08:00"
          },
          {
            "id": "gid://gitlab/Issue/313510",
            "iid": "208",
            "state": "opened",
            "projectId": 89335,
            "title": "如何从Gitee迁移到极狐GitLab",
            "type": "ISSUE",
            "designCollection": {
              "project": {"id": "gid://gitlab/Project/89335", "path": "faq", "fullPath": "ultimate-plan/jihu-gitlab-app/faq", "name": "FAQ", "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / FAQ"}
            },
            "author": {"id": "gid://gitlab/User/1795", "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png", "name": "Zhou YANG", "username": "sinkcup"},
            "createdAt": "2023-01-04T15:09:44+08:00",
            "updatedAt": "2023-02-02T20:44:16+08:00"
          },
          {
            "id": "gid://gitlab/Issue/313509",
            "iid": "207",
            "state": "opened",
            "projectId": 89335,
            "title": "流水线作业日志达到上限了导致被截断应该如何处理",
            "type": "ISSUE",
            "designCollection": {
              "project": {"id": "gid://gitlab/Project/89335", "path": "faq", "fullPath": "ultimate-plan/jihu-gitlab-app/faq", "name": "FAQ", "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / FAQ"}
            },
            "author": {"id": "gid://gitlab/User/1795", "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png", "name": "Zhou YANG", "username": "sinkcup"},
            "createdAt": "2022-12-16T11:56:57+08:00",
            "updatedAt": "2023-01-12T13:39:43+08:00"
          },
          {
            "id": "gid://gitlab/Issue/313508",
            "iid": "206",
            "state": "opened",
            "projectId": 89335,
            "title": "我已经选中自动取消冗余的流水线功能；为何系统没有在推送新的提交时，取消原来未执行完成的流水线？",
            "type": "ISSUE",
            "designCollection": {
              "project": {"id": "gid://gitlab/Project/89335", "path": "faq", "fullPath": "ultimate-plan/jihu-gitlab-app/faq", "name": "FAQ", "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / FAQ"}
            },
            "author": {"id": "gid://gitlab/User/1795", "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png", "name": "Zhou YANG", "username": "sinkcup"},
            "createdAt": "2022-12-13T18:35:08+08:00",
            "updatedAt": "2023-02-02T20:44:50+08:00"
          },
          {
            "id": "gid://gitlab/Issue/313507",
            "iid": "205",
            "state": "opened",
            "projectId": 89335,
            "title": "GitLab的API查询分支只会返回20条，即使有25条分支也不全返回，怎么破？",
            "type": "ISSUE",
            "designCollection": {
              "project": {"id": "gid://gitlab/Project/89335", "path": "faq", "fullPath": "ultimate-plan/jihu-gitlab-app/faq", "name": "FAQ", "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / FAQ"}
            },
            "author": {"id": "gid://gitlab/User/1795", "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png", "name": "Zhou YANG", "username": "sinkcup"},
            "createdAt": "2022-12-12T15:28:59+08:00",
            "updatedAt": "2023-01-12T13:39:43+08:00"
          },
          {
            "id": "gid://gitlab/Issue/313506",
            "iid": "204",
            "state": "opened",
            "projectId": 89335,
            "title": "部署令牌可以修改代码吗？",
            "type": "ISSUE",
            "designCollection": {
              "project": {"id": "gid://gitlab/Project/89335", "path": "faq", "fullPath": "ultimate-plan/jihu-gitlab-app/faq", "name": "FAQ", "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / FAQ"}
            },
            "author": {"id": "gid://gitlab/User/1795", "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png", "name": "Zhou YANG", "username": "sinkcup"},
            "createdAt": "2022-12-08T13:33:37+08:00",
            "updatedAt": "2023-01-12T13:39:44+08:00"
          },
          {
            "id": "gid://gitlab/Issue/313505",
            "iid": "203",
            "state": "opened",
            "projectId": 89335,
            "title": "合并列车的使用场景是什么",
            "type": "ISSUE",
            "designCollection": {
              "project": {"id": "gid://gitlab/Project/89335", "path": "faq", "fullPath": "ultimate-plan/jihu-gitlab-app/faq", "name": "FAQ", "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / FAQ"}
            },
            "author": {"id": "gid://gitlab/User/1795", "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png", "name": "Zhou YANG", "username": "sinkcup"},
            "createdAt": "2022-12-07T10:07:04+08:00",
            "updatedAt": "2023-01-12T13:39:44+08:00"
          },
          {
            "id": "gid://gitlab/Issue/313504",
            "iid": "202",
            "state": "opened",
            "projectId": 89335,
            "title": "如何开启合并列车功能",
            "type": "ISSUE",
            "designCollection": {
              "project": {"id": "gid://gitlab/Project/89335", "path": "faq", "fullPath": "ultimate-plan/jihu-gitlab-app/faq", "name": "FAQ", "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / FAQ"}
            },
            "author": {"id": "gid://gitlab/User/1795", "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png", "name": "Zhou YANG", "username": "sinkcup"},
            "createdAt": "2022-12-07T10:03:15+08:00",
            "updatedAt": "2023-01-12T13:39:45+08:00"
          },
          {
            "id": "gid://gitlab/Issue/313503",
            "iid": "201",
            "state": "opened",
            "projectId": 89335,
            "title": "目前主流的Workflow有哪些",
            "type": "ISSUE",
            "designCollection": {
              "project": {"id": "gid://gitlab/Project/89335", "path": "faq", "fullPath": "ultimate-plan/jihu-gitlab-app/faq", "name": "FAQ", "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / FAQ"}
            },
            "author": {"id": "gid://gitlab/User/1795", "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png", "name": "Zhou YANG", "username": "sinkcup"},
            "createdAt": "2022-12-07T09:54:17+08:00",
            "updatedAt": "2023-01-12T13:39:45+08:00"
          },
          {
            "id": "gid://gitlab/Issue/313502",
            "iid": "200",
            "state": "opened",
            "projectId": 89335,
            "title": "我们提到的Workflow是指什么",
            "type": "ISSUE",
            "designCollection": {
              "project": {"id": "gid://gitlab/Project/89335", "path": "faq", "fullPath": "ultimate-plan/jihu-gitlab-app/faq", "name": "FAQ", "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / FAQ"}
            },
            "author": {"id": "gid://gitlab/User/1795", "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png", "name": "Zhou YANG", "username": "sinkcup"},
            "createdAt": "2022-12-07T09:47:11+08:00",
            "updatedAt": "2023-01-12T13:39:46+08:00"
          },
          {
            "id": "gid://gitlab/Issue/313501",
            "iid": "199",
            "state": "opened",
            "projectId": 89335,
            "title": "有没有推荐的工作流最佳实践",
            "type": "ISSUE",
            "designCollection": {
              "project": {"id": "gid://gitlab/Project/89335", "path": "faq", "fullPath": "ultimate-plan/jihu-gitlab-app/faq", "name": "FAQ", "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / FAQ"}
            },
            "author": {"id": "gid://gitlab/User/1795", "avatarUrl": "/uploads/-/system/user/avatar/1795/avatar.png", "name": "Zhou YANG", "username": "sinkcup"},
            "createdAt": "2022-12-07T09:43:00+08:00",
            "updatedAt": "2023-01-12T13:39:46+08:00"
          }
        ]
      }
    }
  }
};

Map<String, dynamic> faqIssuesGraphQLEmptyResponseData = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "issues": {
        "pageInfo": {
          "hasNextPage": true,
          "startCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMi0xMi0yMiAxMjoyODozMS4yMzk4MzYwMDAgKzA4MDAiLCJpZCI6IjI5NTMyOSJ9",
          "endCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMi0xMi0wMSAxOTo0MzoxNi44MjEzODIwMDAgKzA4MDAiLCJpZCI6IjI3NjA0OSJ9"
        },
        "nodes": []
      }
    }
  }
};

Map<String, dynamic> faqTypesResponse = {
  "data": {
    "project": {
      "archived": false,
      "labels": {
        "nodes": [
          {
            "id": "gid://gitlab/ProjectLabel/72395",
            "title": "type::FAQ",
            "description": "问答",
          },
          {
            "id": "gid://gitlab/ProjectLabel/72059",
            "title": "type::WFH",
            "description": "远程办公",
          },
        ]
      }
    }
  }
};

var projectLabelsResponse = [
  {
    "id": 45575,
    "name": "P::M",
    "description": "Must Have",
    "description_html": "Must Have",
    "text_color": "#FFFFFF",
    "color": "#6699cc",
    "subscribed": false,
    "priority": null,
    "is_project_label": false
  },
  {"id": 68571, "name": "Ready::No", "description": "", "description_html": "", "text_color": "#FFFFFF", "color": "#dc143c", "subscribed": false, "priority": null, "is_project_label": true},
  {"id": 68572, "name": "Ready::Yes", "description": "", "description_html": "", "text_color": "#FFFFFF", "color": "#3cb371", "subscribed": false, "priority": null, "is_project_label": true}
];

var projectLabelsFirstPageResp = [
  {
    "id": 21524,
    "name": "CREQ::blocker",
    "description": "Essential requirements for the existing customers or new opportunities, or the blocking issues found in the customer's production environment",
    "description_html": "Essential requirements for the existing customers or new opportunities, or the blocking issues found in the customer's production environment",
    "text_color": "#FFFFFF",
    "color": "#ff0000",
    "subscribed": false,
    "priority": null,
    "is_project_label": false
  },
  {
    "id": 21525,
    "name": "CREQ::high",
    "description": "High priority requirements for customer but not block deals / renewals, or the high priority issues but not block the usage in customer's production environment",
    "description_html": "High priority requirements for customer but not block deals / renewals, or the high priority issues but not block the usage in customer's production environment",
    "text_color": "#FFFFFF",
    "color": "#eee600",
    "subscribed": false,
    "priority": null,
    "is_project_label": false
  },
  {
    "id": 21526,
    "name": "CREQ::low",
    "description": "Low priority but nice to have",
    "description_html": "Low priority but nice to have",
    "text_color": "#FFFFFF",
    "color": "#00b140",
    "subscribed": false,
    "priority": null,
    "is_project_label": false
  },
  {
    "id": 6267,
    "name": "Engineering Productivity",
    "description": null,
    "description_html": "",
    "text_color": "#FFFFFF",
    "color": "#428BCA",
    "subscribed": false,
    "priority": null,
    "is_project_label": false
  },
  {"id": 30228, "name": "Escalation", "description": "", "description_html": "", "text_color": "#FFFFFF", "color": "#330066", "subscribed": false, "priority": null, "is_project_label": false},
  {"id": 35247, "name": "GitLab Basic", "description": "", "description_html": "", "text_color": "#FFFFFF", "color": "#9400d3", "subscribed": false, "priority": null, "is_project_label": false},
  {
    "id": 3689,
    "name": "GitLab Free",
    "description": "Features or changes intended as part of GitLab Free and available to all tiers.",
    "description_html": "Features or changes intended as part of GitLab Free and available to all tiers.",
    "text_color": "#FFFFFF",
    "color": "#9400d3",
    "subscribed": false,
    "priority": null,
    "is_project_label": false
  },
  {
    "id": 3688,
    "name": "GitLab Premium",
    "description": "Features or changes limited or intended as part of GitLab Premium.",
    "description_html": "Features or changes limited or intended as part of GitLab Premium.",
    "text_color": "#FFFFFF",
    "color": "#9400d3",
    "subscribed": false,
    "priority": null,
    "is_project_label": false
  },
  {
    "id": 3687,
    "name": "GitLab Ultimate",
    "description": "Features or changes limited or intended as part of GitLab Ultimate.",
    "description_html": "Features or changes limited or intended as part of GitLab Ultimate.",
    "text_color": "#FFFFFF",
    "color": "#9400d3",
    "subscribed": false,
    "priority": null,
    "is_project_label": false
  },
  {"id": 30264, "name": "Integrations", "description": null, "description_html": "", "text_color": "#FFFFFF", "color": "#9400d3", "subscribed": false, "priority": null, "is_project_label": true},
  {"id": 6374, "name": "JiHu Only", "description": null, "description_html": "", "text_color": "#FFFFFF", "color": "#ed9121", "subscribed": false, "priority": null, "is_project_label": true},
  {"id": 3375, "name": "OrganizeCode", "description": null, "description_html": "", "text_color": "#FFFFFF", "color": "#0000ff", "subscribed": false, "priority": null, "is_project_label": true},
  {
    "id": 36435,
    "name": "Pick into 15.0",
    "description": "Merge requests to cherry-pick into the `15-0-stable-jh` branch.",
    "description_html": "Merge requests to cherry-pick into the `15-0-stable-jh` branch.",
    "text_color": "#FFFFFF",
    "color": "#00C8CA",
    "subscribed": false,
    "priority": null,
    "is_project_label": true
  },
  {
    "id": 36431,
    "name": "Pick into 15.1",
    "description": "Merge requests to cherry-pick into the `15-1-stable-jh` branch.",
    "description_html": "Merge requests to cherry-pick into the `15-1-stable-jh` branch.",
    "text_color": "#FFFFFF",
    "color": "#00C8CA",
    "subscribed": false,
    "priority": null,
    "is_project_label": true
  },
  {
    "id": 36432,
    "name": "Pick into 15.2",
    "description": "Merge requests to cherry-pick into the `15-2-stable-jh` branch.",
    "description_html": "Merge requests to cherry-pick into the `15-2-stable-jh` branch.",
    "text_color": "#FFFFFF",
    "color": "#00C8CA",
    "subscribed": false,
    "priority": null,
    "is_project_label": true
  },
  {
    "id": 36433,
    "name": "Pick into 15.3",
    "description": "Merge requests to cherry-pick into the `15-3-stable-jh` branch.",
    "description_html": "Merge requests to cherry-pick into the `15-3-stable-jh` branch.",
    "text_color": "#FFFFFF",
    "color": "#00C8CA",
    "subscribed": false,
    "priority": null,
    "is_project_label": true
  },
  {
    "id": 36434,
    "name": "Pick into 15.4",
    "description": "Merge requests to cherry-pick into the `15-4-stable-jh` branch.",
    "description_html": "Merge requests to cherry-pick into the `15-4-stable-jh` branch.",
    "text_color": "#FFFFFF",
    "color": "#00C8CA",
    "subscribed": false,
    "priority": null,
    "is_project_label": true
  },
  {
    "id": 62462,
    "name": "Pick into 15.5",
    "description": "Merge requests to cherry-pick into the `15-5-stable-jh` branch.",
    "description_html": "Merge requests to cherry-pick into the `15-5-stable-jh` branch.",
    "text_color": "#FFFFFF",
    "color": "#00C8CA",
    "subscribed": false,
    "priority": null,
    "is_project_label": true
  },
  {
    "id": 62463,
    "name": "Pick into 15.6",
    "description": "Merge requests to cherry-pick into the `15-6-stable-jh` branch.",
    "description_html": "Merge requests to cherry-pick into the `15-6-stable-jh` branch.",
    "text_color": "#FFFFFF",
    "color": "#00C8CA",
    "subscribed": false,
    "priority": null,
    "is_project_label": true
  },
  {
    "id": 62464,
    "name": "Pick into 15.7",
    "description": "Merge requests to cherry-pick into the `15-7-stable-jh` branch.",
    "description_html": "Merge requests to cherry-pick into the `15-7-stable-jh` branch.",
    "text_color": "#FFFFFF",
    "color": "#00C8CA",
    "subscribed": false,
    "priority": null,
    "is_project_label": true
  },
  {
    "id": 62465,
    "name": "Pick into 15.8",
    "description": "Merge requests to cherry-pick into the `15-8-stable-jh` branch.",
    "description_html": "Merge requests to cherry-pick into the `15-8-stable-jh` branch.",
    "text_color": "#FFFFFF",
    "color": "#00C8CA",
    "subscribed": false,
    "priority": null,
    "is_project_label": true
  },
  {
    "id": 62466,
    "name": "Pick into 15.9",
    "description": "Merge requests to cherry-pick into the `15-9-stable-jh` branch.",
    "description_html": "Merge requests to cherry-pick into the `15-9-stable-jh` branch.",
    "text_color": "#FFFFFF",
    "color": "#00C8CA",
    "subscribed": false,
    "priority": null,
    "is_project_label": true
  },
  {"id": 25067, "name": "QA", "description": null, "description_html": "", "text_color": "#FFFFFF", "color": "#6699cc", "subscribed": false, "priority": null, "is_project_label": true},
  {
    "id": 3684,
    "name": "SaaS",
    "description": "Issues or MRs that are only for jihulab.com (SaaS) rather than self-managed",
    "description_html": "Issues or MRs that are only for jihulab.com (SaaS) rather than self-managed",
    "text_color": "#FFFFFF",
    "color": "#009966",
    "subscribed": false,
    "priority": null,
    "is_project_label": false
  },
  {
    "id": 8231,
    "name": "Seeking community contributions",
    "description": null,
    "description_html": "",
    "text_color": "#FFFFFF",
    "color": "#00b140",
    "subscribed": false,
    "priority": null,
    "is_project_label": true
  },
  {
    "id": 22463,
    "name": "Team::CS",
    "description": "由Customer Success团队提出的需求",
    "description_html": "由Customer Success团队提出的需求",
    "text_color": "#FFFFFF",
    "color": "#cd5b45",
    "subscribed": false,
    "priority": null,
    "is_project_label": true
  },
  {
    "id": 21303,
    "name": "Team::Presales",
    "description": "由售前团队提出的需求",
    "description_html": "由售前团队提出的需求",
    "text_color": "#FFFFFF",
    "color": "#0000ff",
    "subscribed": false,
    "priority": null,
    "is_project_label": false
  },
  {
    "id": 22464,
    "name": "Team::Sales",
    "description": "由销售团队提出的需求",
    "description_html": "由销售团队提出的需求",
    "text_color": "#FFFFFF",
    "color": "#6699cc",
    "subscribed": false,
    "priority": null,
    "is_project_label": true
  },
  {"id": 22549, "name": "Time tracking", "description": "", "description_html": "", "text_color": "#FFFFFF", "color": "#3cb371", "subscribed": false, "priority": null, "is_project_label": false},
  {
    "id": 57404,
    "name": "Triage Report",
    "description": "Triage Ops auto generated report issues https://gitlab.cn/handbook/engineering/triage-ops#工作报告",
    "description_html":
        "Triage Ops auto generated report issues <a href=\"https://gitlab.cn/handbook/engineering/triage-ops#%E5%B7%A5%E4%BD%9C%E6%8A%A5%E5%91%8A\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">https://gitlab.cn/handbook/engineering/triage-ops#工作报告</a>",
    "text_color": "#FFFFFF",
    "color": "#00b140",
    "subscribed": false,
    "priority": null,
    "is_project_label": false
  },
  {
    "id": 16279,
    "name": "UX",
    "description": "Issues for the UX team. Covers new or existing functionality that needs a design proposal",
    "description_html": "Issues for the UX team. Covers new or existing functionality that needs a design proposal",
    "text_color": "#FFFFFF",
    "color": "#D10069",
    "subscribed": false,
    "priority": null,
    "is_project_label": false
  },
  {
    "id": 14500,
    "name": "UX",
    "description": "Issues for the UX team. Covers new or existing functionality that needs a design proposal",
    "description_html": "Issues for the UX team. Covers new or existing functionality that needs a design proposal",
    "text_color": "#FFFFFF",
    "color": "#D10069",
    "subscribed": false,
    "priority": null,
    "is_project_label": true
  },
  {"id": 7083, "name": "api::missing", "description": null, "description_html": "", "text_color": "#FFFFFF", "color": "#dc143c", "subscribed": false, "priority": null, "is_project_label": true},
  {"id": 777, "name": "backend", "description": null, "description_html": "", "text_color": "#FFFFFF", "color": "#808080", "subscribed": false, "priority": null, "is_project_label": false},
  {"id": 3456, "name": "backlog", "description": null, "description_html": "", "text_color": "#FFFFFF", "color": "#9400d3", "subscribed": false, "priority": null, "is_project_label": false},
  {
    "id": 11286,
    "name": "bug:: availability",
    "description": "Defects related to GitLab SaaS availability. Read more at https://about.gitlab.com/handbook/engineering/metrics/#work-type-classification",
    "description_html":
        "Defects related to GitLab SaaS availability. Read more at <a href=\"https://about.gitlab.com/handbook/engineering/metrics/#work-type-classification\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">https://about.gitlab.com/handbook/engineering/metrics/#work-type-classification</a>",
    "text_color": "#FFFFFF",
    "color": "#eee600",
    "subscribed": false,
    "priority": null,
    "is_project_label": true
  },
  {
    "id": 14843,
    "name": "bug::availability",
    "description": " Issues impacting the availability of JihuLab.com",
    "description_html": " Issues impacting the availability of JihuLab.com",
    "text_color": "#FFFFFF",
    "color": "#ff0000",
    "subscribed": false,
    "priority": null,
    "is_project_label": false
  },
  {
    "id": 14844,
    "name": "bug::mobile",
    "description": " Bugs that appear on mobile browsers",
    "description_html": " Bugs that appear on mobile browsers",
    "text_color": "#FFFFFF",
    "color": "#ff0000",
    "subscribed": false,
    "priority": null,
    "is_project_label": false
  },
  {
    "id": 14845,
    "name": "bug::performance",
    "description": " Performance defects or response time degradation",
    "description_html": " Performance defects or response time degradation",
    "text_color": "#FFFFFF",
    "color": "#ff0000",
    "subscribed": false,
    "priority": null,
    "is_project_label": false
  },
  {
    "id": 6417,
    "name": "bug::transient",
    "description": "Bugs that are transient",
    "description_html": "Bugs that are transient",
    "text_color": "#FFFFFF",
    "color": "#ff0000",
    "subscribed": false,
    "priority": null,
    "is_project_label": false
  },
  {
    "id": 14846,
    "name": "bug::vulnerability",
    "description": "A security vulnerability",
    "description_html": "A security vulnerability",
    "text_color": "#FFFFFF",
    "color": "#ff0000",
    "subscribed": false,
    "priority": null,
    "is_project_label": false
  },
  {"id": 3372, "name": "ci::templates", "description": null, "description_html": "", "text_color": "#FFFFFF", "color": "#6699cc", "subscribed": false, "priority": null, "is_project_label": true},
  {
    "id": 3462,
    "name": "customer",
    "description": "Issues that were reported by JiHu customers",
    "description_html": "Issues that were reported by JiHu customers",
    "text_color": "#FFFFFF",
    "color": "#ff0000",
    "subscribed": false,
    "priority": null,
    "is_project_label": false
  },
  {"id": 3464, "name": "database", "description": null, "description_html": "", "text_color": "#FFFFFF", "color": "#6699cc", "subscribed": false, "priority": null, "is_project_label": false},
  {
    "id": 3648,
    "name": "database::review pending",
    "description": null,
    "description_html": "",
    "text_color": "#FFFFFF",
    "color": "#6699cc",
    "subscribed": false,
    "priority": null,
    "is_project_label": true
  },
  {"id": 3457, "name": "documentation", "description": null, "description_html": "", "text_color": "#FFFFFF", "color": "#6699cc", "subscribed": false, "priority": null, "is_project_label": false},
  {
    "id": 46653,
    "name": "failure::broken-test",
    "description": "Tests that fail due to a bug in the test itself, not in the application code",
    "description_html": "Tests that fail due to a bug in the test itself, not in the application code",
    "text_color": "#FFFFFF",
    "color": "#ff0000",
    "subscribed": false,
    "priority": null,
    "is_project_label": false
  },
  {
    "id": 46655,
    "name": "failure::flaky-test",
    "description": "Tests that fail intermittently including those that cause \"broken master\"",
    "description_html": "Tests that fail intermittently including those that cause \"broken master\"",
    "text_color": "#FFFFFF",
    "color": "#ff0000",
    "subscribed": false,
    "priority": null,
    "is_project_label": false
  },
  {
    "id": 46652,
    "name": "failure::investigating",
    "description": "Tests that are under investigation",
    "description_html": "Tests that are under investigation",
    "text_color": "#FFFFFF",
    "color": "#ff0000",
    "subscribed": false,
    "priority": null,
    "is_project_label": false
  },
  {
    "id": 46654,
    "name": "failure::stale-test",
    "description": "Tests that fail because the application was changed without updating the test",
    "description_html": "Tests that fail because the application was changed without updating the test",
    "text_color": "#FFFFFF",
    "color": "#ff0000",
    "subscribed": false,
    "priority": null,
    "is_project_label": false
  }
];

var projectLabelsNextPageResponse = [
  {
    "id": 45575,
    "name": "P::M",
    "description": "Must Have",
    "description_html": "Must Have",
    "text_color": "#FFFFFF",
    "color": "#6699cc",
    "subscribed": false,
    "priority": null,
    "is_project_label": false
  },
  {"id": 68571, "name": "Ready::No", "description": "", "description_html": "", "text_color": "#FFFFFF", "color": "#dc143c", "subscribed": false, "priority": null, "is_project_label": true},
  {"id": 68572, "name": "Ready::Yes", "description": "", "description_html": "", "text_color": "#FFFFFF", "color": "#3cb371", "subscribed": false, "priority": null, "is_project_label": true}
];

var projectAssigneesNextPageResponse = [
  {"id": 1, "member_id": 1, "project_id": 59893, "name": "test_1", "username": "tester_1", "avatar_url": "avatar_url_1", "version": 1},
  {"id": 2, "member_id": 2, "project_id": 59893, "name": "test_2", "username": "tester_2", "avatar_url": "avatar_url_2", "version": 1},
  {"id": 3, "member_id": 3, "project_id": 59893, "name": "test_11", "username": "tester_11", "avatar_url": "avatar_url_11", "version": 1},
  {"id": 4, "member_id": 4, "project_id": 59893, "name": "test_22", "username": "tester_22", "avatar_url": "avatar_url_22", "version": 1},
];

Map<String, dynamic> issuePostVotesGraphQLResponse = {
  "data": {
    "project": {
      "issue": {"downvotes": 0, "upvotes": 0}
    }
  }
};

var projectIssuesArchivedGraphQLResponseData = {
  "data": {
    "project": {"archived": true}
  }
};
