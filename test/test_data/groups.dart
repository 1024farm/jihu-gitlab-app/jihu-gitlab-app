final topLevelDBGroups = [
  {
    "id": 1,
    "iid": 118014,
    "user_id": 9527,
    "name": 'highsoft-for-test',
    'type': 'group',
    'relative_path': 'highsof-t',
    "parent_id": 0,
    "starred": 0,
    "last_activity_at": 0,
    "starred_at": 0,
    "version": 1
  },
  {"id": 2, "iid": 39526, "user_id": 9527, "name": '1024 town', 'type': 'group', 'relative_path': '1024town', "parent_id": 0, "starred": 0, "last_activity_at": 0, "starred_at": 0, "version": 1},
  {"id": 3, "iid": 14269, "user_id": 9527, "name": '免费版演示', 'type': 'group', 'relative_path': 'free-plan', "parent_id": 0, "starred": 0, "last_activity_at": 0, "starred_at": 0, "version": 1}
];

final subgroupListPageDBItems = [
  {
    "id": 1,
    "iid": 154670,
    "user_id": 9527,
    "name": 'Raymond Demo',
    'type': 'subgroup',
    'relative_path': 'highsof-t/raymond-demo',
    "parent_id": 118014,
    "starred": 0,
    "last_activity_at": 0,
    "starred_at": 0,
    "version": 1
  },
  {
    "id": 2,
    "iid": 137710,
    "user_id": 9527,
    "name": 'test group',
    'type': 'subgroup',
    'relative_path': 'highsof-t/test-group',
    "parent_id": 118014,
    "starred": 0,
    "last_activity_at": 0,
    "starred_at": 0,
    "version": 1
  },
  {
    "id": 3,
    "iid": 69172,
    "user_id": 9527,
    "name": 'Learn GitLab',
    'type': 'project',
    'relative_path': 'highsof-t/learn-gitlab',
    "parent_id": 118014,
    "starred": 0,
    "last_activity_at": 0,
    "starred_at": 0,
    "version": 1
  },
  {
    "id": 4,
    "iid": 69171,
    "user_id": 9527,
    "name": 'demo',
    'type': 'project',
    'relative_path': 'highsof-t/demo',
    "parent_id": 118014,
    "starred": 0,
    "last_activity_at": 0,
    "starred_at": 0,
    "version": 1
  }
];

final groupDetailsResponse = {
  "id": 118014,
  "web_url": "https://jihulab.com/groups/highsof-t",
  "name": "highsoft-for-test",
  "path": "highsof-t",
  "description": "",
  "visibility": "public",
  "share_with_group_lock": false,
  "require_two_factor_authentication": false,
  "two_factor_grace_period": 48,
  "project_creation_level": "developer",
  "auto_devops_enabled": null,
  "subgroup_creation_level": "maintainer",
  "emails_disabled": null,
  "mentions_disabled": null,
  "lfs_enabled": true,
  "default_branch_protection": 2,
  "avatar_url": null,
  "request_access_enabled": true,
  "full_name": "highsoft-for-test",
  "full_path": "highsof-t",
  "created_at": "2022-11-09T15:54:15.587+08:00",
  "parent_id": null,
  "ldap_cn": null,
  "ldap_access": null,
  "shared_with_groups": [],
  "runners_token": "GR1348941Yd2Rw3-5Re8eGz388C--",
  "prevent_sharing_groups_outside_hierarchy": false,
  "projects": [
    {
      "id": 69172,
      "description": "Learn how to use GitLab to support your software development life cycle.",
      "name": "Learn GitLab",
      "name_with_namespace": "highsoft-for-test / Learn GitLab",
      "path": "learn-gitlab",
      "path_with_namespace": "highsof-t/learn-gitlab",
      "created_at": "2022-11-09T15:54:16.402+08:00",
      "default_branch": "master",
      "tag_list": [],
      "topics": [],
      "ssh_url_to_repo": "git@jihulab.com:highsof-t/learn-gitlab.git",
      "http_url_to_repo": "https://jihulab.com/highsof-t/learn-gitlab.git",
      "web_url": "https://jihulab.com/highsof-t/learn-gitlab",
      "readme_url": "https://jihulab.com/highsof-t/learn-gitlab/-/blob/master/README.md",
      "forks_count": 0,
      "avatar_url": "https://jihulab.com/uploads/-/system/project/avatar/69172/Artboard.jpg",
      "star_count": 1,
      "last_activity_at": "2023-02-02T15:24:34.627+08:00",
      "namespace": {
        "id": 118014,
        "name": "highsoft-for-test",
        "path": "highsof-t",
        "kind": "group",
        "full_path": "highsof-t",
        "parent_id": null,
        "avatar_url": null,
        "web_url": "https://jihulab.com/groups/highsof-t"
      },
      "container_registry_image_prefix": "registry.jihulab.com/highsof-t/learn-gitlab",
      "_links": {
        "self": "https://jihulab.com/api/v4/projects/69172",
        "issues": "https://jihulab.com/api/v4/projects/69172/issues",
        "merge_requests": "https://jihulab.com/api/v4/projects/69172/merge_requests",
        "repo_branches": "https://jihulab.com/api/v4/projects/69172/repository/branches",
        "labels": "https://jihulab.com/api/v4/projects/69172/labels",
        "events": "https://jihulab.com/api/v4/projects/69172/events",
        "members": "https://jihulab.com/api/v4/projects/69172/members",
        "cluster_agents": "https://jihulab.com/api/v4/projects/69172/cluster_agents"
      },
      "packages_enabled": true,
      "empty_repo": false,
      "archived": false,
      "visibility": "public",
      "resolve_outdated_diff_discussions": false,
      "container_expiration_policy": {
        "cadence": "1d",
        "enabled": false,
        "keep_n": 10,
        "older_than": "90d",
        "name_regex": ".*",
        "name_regex_keep": null,
        "next_run_at": "2022-11-10T15:54:16.436+08:00"
      },
      "issues_enabled": true,
      "merge_requests_enabled": true,
      "wiki_enabled": true,
      "jobs_enabled": true,
      "snippets_enabled": true,
      "container_registry_enabled": true,
      "service_desk_enabled": true,
      "service_desk_address": "contact-project+highsof-t-learn-gitlab-69172-issue-@mg.jihulab.com",
      "can_create_merge_request_in": true,
      "issues_access_level": "enabled",
      "repository_access_level": "enabled",
      "merge_requests_access_level": "enabled",
      "forking_access_level": "enabled",
      "wiki_access_level": "enabled",
      "builds_access_level": "enabled",
      "snippets_access_level": "enabled",
      "pages_access_level": "private",
      "operations_access_level": "enabled",
      "analytics_access_level": "enabled",
      "container_registry_access_level": "enabled",
      "security_and_compliance_access_level": "private",
      "releases_access_level": "enabled",
      "environments_access_level": "enabled",
      "feature_flags_access_level": "enabled",
      "infrastructure_access_level": "enabled",
      "monitor_access_level": "enabled",
      "emails_disabled": false,
      "shared_runners_enabled": true,
      "lfs_enabled": true,
      "creator_id": 29064,
      "import_url": null,
      "import_type": "gitlab_project",
      "import_status": "finished",
      "open_issues_count": 40,
      "ci_default_git_depth": 20,
      "ci_forward_deployment_enabled": true,
      "ci_job_token_scope_enabled": false,
      "ci_separated_caches": true,
      "ci_opt_in_jwt": false,
      "ci_allow_fork_pipelines_to_run_in_parent_project": true,
      "public_jobs": true,
      "build_timeout": 3600,
      "auto_cancel_pending_pipelines": "enabled",
      "ci_config_path": "",
      "shared_with_groups": [],
      "only_allow_merge_if_pipeline_succeeds": false,
      "allow_merge_on_skipped_pipeline": null,
      "restrict_user_defined_variables": false,
      "request_access_enabled": true,
      "only_allow_merge_if_all_discussions_are_resolved": false,
      "remove_source_branch_after_merge": true,
      "printing_merge_request_link_enabled": true,
      "merge_method": "merge",
      "squash_option": "default_off",
      "enforce_auth_checks_on_uploads": true,
      "suggestion_commit_message": null,
      "merge_commit_template": null,
      "squash_commit_template": null,
      "issue_branch_template": null,
      "auto_devops_enabled": false,
      "auto_devops_deploy_strategy": "continuous",
      "autoclose_referenced_issues": true,
      "keep_latest_artifact": true,
      "runner_token_expiration_interval": null,
      "external_authorization_classification_label": null,
      "requirements_enabled": false,
      "requirements_access_level": "enabled",
      "security_and_compliance_enabled": true,
      "compliance_frameworks": []
    },
    {
      "id": 69171,
      "description": null,
      "name": "demo",
      "name_with_namespace": "highsoft-for-test / demo",
      "path": "demo",
      "path_with_namespace": "highsof-t/demo",
      "created_at": "2022-11-09T15:54:15.909+08:00",
      "default_branch": "main",
      "tag_list": [],
      "topics": [],
      "ssh_url_to_repo": "git@jihulab.com:highsof-t/demo.git",
      "http_url_to_repo": "https://jihulab.com/highsof-t/demo.git",
      "web_url": "https://jihulab.com/highsof-t/demo",
      "readme_url": null,
      "forks_count": 0,
      "avatar_url": null,
      "star_count": 3,
      "last_activity_at": "2023-02-06T19:07:19.068+08:00",
      "namespace": {
        "id": 118014,
        "name": "highsoft-for-test",
        "path": "highsof-t",
        "kind": "group",
        "full_path": "highsof-t",
        "parent_id": null,
        "avatar_url": null,
        "web_url": "https://jihulab.com/groups/highsof-t"
      },
      "container_registry_image_prefix": "registry.jihulab.com/highsof-t/demo",
      "_links": {
        "self": "https://jihulab.com/api/v4/projects/69171",
        "issues": "https://jihulab.com/api/v4/projects/69171/issues",
        "merge_requests": "https://jihulab.com/api/v4/projects/69171/merge_requests",
        "repo_branches": "https://jihulab.com/api/v4/projects/69171/repository/branches",
        "labels": "https://jihulab.com/api/v4/projects/69171/labels",
        "events": "https://jihulab.com/api/v4/projects/69171/events",
        "members": "https://jihulab.com/api/v4/projects/69171/members",
        "cluster_agents": "https://jihulab.com/api/v4/projects/69171/cluster_agents"
      },
      "packages_enabled": true,
      "empty_repo": true,
      "archived": false,
      "visibility": "public",
      "resolve_outdated_diff_discussions": false,
      "container_expiration_policy": {
        "cadence": "1d",
        "enabled": false,
        "keep_n": 10,
        "older_than": "90d",
        "name_regex": ".*",
        "name_regex_keep": null,
        "next_run_at": "2022-11-10T15:54:15.929+08:00"
      },
      "issues_enabled": true,
      "merge_requests_enabled": true,
      "wiki_enabled": true,
      "jobs_enabled": true,
      "snippets_enabled": true,
      "container_registry_enabled": true,
      "service_desk_enabled": true,
      "service_desk_address": "contact-project+highsof-t-demo-69171-issue-@mg.jihulab.com",
      "can_create_merge_request_in": true,
      "issues_access_level": "enabled",
      "repository_access_level": "enabled",
      "merge_requests_access_level": "enabled",
      "forking_access_level": "enabled",
      "wiki_access_level": "enabled",
      "builds_access_level": "enabled",
      "snippets_access_level": "enabled",
      "pages_access_level": "private",
      "operations_access_level": "enabled",
      "analytics_access_level": "enabled",
      "container_registry_access_level": "enabled",
      "security_and_compliance_access_level": "private",
      "releases_access_level": "enabled",
      "environments_access_level": "enabled",
      "feature_flags_access_level": "enabled",
      "infrastructure_access_level": "enabled",
      "monitor_access_level": "enabled",
      "emails_disabled": false,
      "shared_runners_enabled": true,
      "lfs_enabled": true,
      "creator_id": 29064,
      "import_url": null,
      "import_type": null,
      "import_status": "none",
      "open_issues_count": 87,
      "ci_default_git_depth": 20,
      "ci_forward_deployment_enabled": true,
      "ci_job_token_scope_enabled": false,
      "ci_separated_caches": true,
      "ci_opt_in_jwt": false,
      "ci_allow_fork_pipelines_to_run_in_parent_project": true,
      "public_jobs": true,
      "build_timeout": 3600,
      "auto_cancel_pending_pipelines": "enabled",
      "ci_config_path": "",
      "shared_with_groups": [],
      "only_allow_merge_if_pipeline_succeeds": false,
      "allow_merge_on_skipped_pipeline": null,
      "restrict_user_defined_variables": false,
      "request_access_enabled": true,
      "only_allow_merge_if_all_discussions_are_resolved": false,
      "remove_source_branch_after_merge": true,
      "printing_merge_request_link_enabled": true,
      "merge_method": "merge",
      "squash_option": "default_off",
      "enforce_auth_checks_on_uploads": true,
      "suggestion_commit_message": null,
      "merge_commit_template": null,
      "squash_commit_template": null,
      "issue_branch_template": null,
      "auto_devops_enabled": false,
      "auto_devops_deploy_strategy": "continuous",
      "autoclose_referenced_issues": true,
      "keep_latest_artifact": true,
      "runner_token_expiration_interval": null,
      "external_authorization_classification_label": null,
      "requirements_enabled": false,
      "requirements_access_level": "enabled",
      "security_and_compliance_enabled": true,
      "compliance_frameworks": []
    }
  ],
  "shared_projects": [],
  "shared_runners_minutes_limit": null,
  "extra_shared_runners_minutes_limit": null,
  "prevent_forking_outside_group": null,
  "membership_lock": false
};

final subgroupListResponse = [
  {
    "id": 154670,
    "web_url": "https://jihulab.com/groups/highsof-t/raymond-demo",
    "name": "Raymond Demo",
    "path": "raymond-demo",
    "description": "",
    "visibility": "public",
    "share_with_group_lock": false,
    "require_two_factor_authentication": false,
    "two_factor_grace_period": 48,
    "project_creation_level": "developer",
    "auto_devops_enabled": null,
    "subgroup_creation_level": "maintainer",
    "emails_disabled": null,
    "mentions_disabled": null,
    "lfs_enabled": true,
    "default_branch_protection": 2,
    "avatar_url": null,
    "request_access_enabled": true,
    "full_name": "highsoft-for-test / Raymond Demo",
    "full_path": "highsof-t/raymond-demo",
    "created_at": "2023-02-07T09:29:01.466+08:00",
    "parent_id": 118014,
    "ldap_cn": null,
    "ldap_access": null
  },
  {
    "id": 137710,
    "web_url": "https://jihulab.com/groups/highsof-t/test-group",
    "name": "test group",
    "path": "test-group",
    "description": "",
    "visibility": "public",
    "share_with_group_lock": false,
    "require_two_factor_authentication": false,
    "two_factor_grace_period": 48,
    "project_creation_level": "developer",
    "auto_devops_enabled": null,
    "subgroup_creation_level": "maintainer",
    "emails_disabled": null,
    "mentions_disabled": null,
    "lfs_enabled": true,
    "default_branch_protection": 2,
    "avatar_url": null,
    "request_access_enabled": true,
    "full_name": "highsoft-for-test / test group",
    "full_path": "highsof-t/test-group",
    "created_at": "2022-12-15T14:23:23.404+08:00",
    "parent_id": 118014,
    "ldap_cn": null,
    "ldap_access": null
  }
];
