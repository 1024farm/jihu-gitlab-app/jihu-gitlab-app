import 'note.dart';

var discussionDbData = {
  'id': 111,
  'discussion_id': '123',
  'issue_id': 33,
  'issue_iid': 3,
  'individual_note': 1,
  'path_with_namespace': 'pathWithNamespace',
  'version': 0,
  'notes': [noteDbData]
};

List<dynamic> discussionsData = [
  {
    "id": "abc123",
    "individual_note": true,
    "notes": [
      {
        "id": 1574825,
        "type": null,
        "body": "changed the description",
        "attachment": null,
        "author": {
          "id": 29355,
          "username": "perity",
          "name": "miaolu",
          "state": "active",
          "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29355/avatar.png",
          "web_url": "https://jihulab.com/perity"
        },
        "created_at": "2022-11-25T14:19:57.067+08:00",
        "updated_at": "2022-11-25T14:19:57.068+08:00",
        "system": false,
        "noteable_id": 265567,
        "noteable_type": "Issue",
        "resolvable": false,
        "confidential": false,
        "internal": false,
        "noteable_iid": 6,
        "commands_changes": {}
      }
    ]
  },
  {
    "id": "abc124",
    "individual_note": true,
    "notes": [
      {
        "id": 1574834,
        "type": null,
        "body": "assigned to @perity",
        "attachment": null,
        "author": {
          "id": 29355,
          "username": "perity",
          "name": "miaolu",
          "state": "active",
          "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29355/avatar.png",
          "web_url": "https://jihulab.com/perity"
        },
        "created_at": "2022-11-25T14:21:47.711+08:00",
        "updated_at": "2022-11-25T14:21:47.712+08:00",
        "system": false,
        "noteable_id": 657567,
        "noteable_type": "Issue",
        "resolvable": false,
        "confidential": false,
        "internal": false,
        "noteable_iid": 7,
        "commands_changes": {}
      },
      {
        "id": 1929423,
        "type": "DiscussionNote",
        "body": "@raymond_liao new try",
        "attachment": null,
        "author": {
          "id": 30192,
          "username": "raymond_liao",
          "name": "Raymond Liao",
          "state": "active",
          "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/30192/avatar.png",
          "web_url": "https://jihulab.com/raymond_liao"
        },
        "created_at": "2023-01-02T13:11:05.966+08:00",
        "updated_at": "2023-01-02T13:11:05.966+08:00",
        "system": false,
        "noteable_id": 657657,
        "noteable_type": "Issue",
        "resolvable": false,
        "confidential": true,
        "internal": true,
        "noteable_iid": 8,
        "commands_changes": {}
      },
      {
        "id": 1930145,
        "type": "DiscussionNote",
        "body": "the other one @raymond_liao",
        "attachment": null,
        "author": {
          "id": 30192,
          "username": "raymond_liao",
          "name": "Raymond Liao",
          "state": "active",
          "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/30192/avatar.png",
          "web_url": "https://jihulab.com/raymond_liao"
        },
        "created_at": "2023-01-03T10:40:54.714+08:00",
        "updated_at": "2023-01-03T10:40:54.714+08:00",
        "system": false,
        "noteable_id": 456456,
        "noteable_type": "Issue",
        "resolvable": false,
        "confidential": true,
        "internal": true,
        "noteable_iid": 9,
        "commands_changes": {}
      },
      {
        "id": 1930275,
        "type": "DiscussionNote",
        "body": "new comment @raymond_liao",
        "attachment": null,
        "author": {
          "id": 30192,
          "username": "raymond_liao",
          "name": "Raymond Liao",
          "state": "active",
          "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/30192/avatar.png",
          "web_url": "https://jihulab.com/raymond_liao"
        },
        "created_at": "2023-01-03T11:22:13.850+08:00",
        "updated_at": "2023-01-03T11:22:13.850+08:00",
        "system": false,
        "noteable_id": 345345,
        "noteable_type": "Issue",
        "resolvable": false,
        "confidential": true,
        "internal": true,
        "noteable_iid": 10,
        "commands_changes": {}
      },
      {
        "id": 1930307,
        "type": "DiscussionNote",
        "body": "new comment 12 @raymond_liao",
        "attachment": null,
        "author": {
          "id": 30192,
          "username": "raymond_liao",
          "name": "Raymond Liao",
          "state": "active",
          "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/30192/avatar.png",
          "web_url": "https://jihulab.com/raymond_liao"
        },
        "created_at": "2023-01-03T11:30:53.986+08:00",
        "updated_at": "2023-01-03T11:30:53.986+08:00",
        "system": false,
        "noteable_id": 879678,
        "noteable_type": "Issue",
        "resolvable": false,
        "confidential": true,
        "internal": true,
        "noteable_iid": 11,
        "commands_changes": {}
      }
    ]
  },
];

var discussionsWithOutGroupItem = [
  {
    "id": "6f18f61086fdd66b7b4ce5986d3299625aaca22e",
    "individual_note": true,
    "notes": [
      {
        "id": 2037056,
        "type": null,
        "body": "assigned to @zhangling",
        "attachment": null,
        "author": {
          "id": 29758,
          "username": "zhangling",
          "name": "ling zhang",
          "state": "active",
          "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29758/avatar.png",
          "web_url": "https://jihulab.com/zhangling"
        },
        "created_at": "2023-02-08T14:24:47.131+08:00",
        "updated_at": "2023-02-08T14:24:47.132+08:00",
        "system": true,
        "noteable_id": 316863,
        "noteable_type": "Issue",
        "resolvable": false,
        "confidential": false,
        "internal": false,
        "noteable_iid": 47,
        "commands_changes": {}
      }
    ]
  },
  {
    "id": "23f97f5588e6dec29553968bba54fa658082a036",
    "individual_note": true,
    "notes": [
      {
        "id": 2037057,
        "type": null,
        "body": "created branch [`47-test-optional-approvals`](/ultimate-plan/jihu-gitlab-app/demo/-/compare/main...47-test-optional-approvals) to address this issue",
        "attachment": null,
        "author": {
          "id": 29758,
          "username": "zhangling",
          "name": "ling zhang",
          "state": "active",
          "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29758/avatar.png",
          "web_url": "https://jihulab.com/zhangling"
        },
        "created_at": "2023-02-08T14:24:50.113+08:00",
        "updated_at": "2023-02-08T14:24:50.114+08:00",
        "system": true,
        "noteable_id": 316863,
        "noteable_type": "Issue",
        "resolvable": false,
        "confidential": false,
        "internal": false,
        "noteable_iid": 47,
        "commands_changes": {}
      }
    ]
  },
  {
    "id": "f96b3c4c6bc4ac14119e06e08c7e8e599cc805f6",
    "individual_note": true,
    "notes": [
      {
        "id": 2037058,
        "type": null,
        "body": "mentioned in merge request !18",
        "attachment": null,
        "author": {
          "id": 29758,
          "username": "zhangling",
          "name": "ling zhang",
          "state": "active",
          "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29758/avatar.png",
          "web_url": "https://jihulab.com/zhangling"
        },
        "created_at": "2023-02-08T14:24:54.932+08:00",
        "updated_at": "2023-02-08T14:24:54.934+08:00",
        "system": true,
        "noteable_id": 316863,
        "noteable_type": "Issue",
        "resolvable": false,
        "confidential": false,
        "internal": false,
        "noteable_iid": 47,
        "commands_changes": {}
      }
    ]
  },
  {
    "id": "5806362475d92f4a4ef3ed248f1bb49358b96749",
    "individual_note": true,
    "notes": [
      {
        "id": 2038241,
        "type": null,
        "body": "1",
        "attachment": null,
        "author": {
          "id": 29758,
          "username": "zhangling",
          "name": "ling zhang",
          "state": "active",
          "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29758/avatar.png",
          "web_url": "https://jihulab.com/zhangling"
        },
        "created_at": "2023-02-08T17:43:38.771+08:00",
        "updated_at": "2023-02-08T17:43:38.771+08:00",
        "system": false,
        "noteable_id": 316863,
        "noteable_type": "Issue",
        "resolvable": false,
        "confidential": false,
        "internal": false,
        "noteable_iid": 47,
        "commands_changes": {}
      }
    ]
  },
  {
    "id": "1f92758fa0513643ad62a77c5d6a781fa48552cf",
    "individual_note": true,
    "notes": [
      {
        "id": 2038292,
        "type": null,
        "body": "qqqqq",
        "attachment": null,
        "author": {
          "id": 29758,
          "username": "zhangling",
          "name": "ling zhang",
          "state": "active",
          "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29758/avatar.png",
          "web_url": "https://jihulab.com/zhangling"
        },
        "created_at": "2023-02-08T17:56:18.080+08:00",
        "updated_at": "2023-02-08T17:56:18.080+08:00",
        "system": false,
        "noteable_id": 316863,
        "noteable_type": "Issue",
        "resolvable": false,
        "confidential": false,
        "internal": false,
        "noteable_iid": 47,
        "commands_changes": {}
      }
    ]
  }
];

var discussionsWithGroupItem = [
  {
    "id": "3c2019573be080e14b58c818f87e76969032a6cd",
    "individual_note": true,
    "notes": [
      {
        "id": 2040462,
        "type": null,
        "body": "test for comment message @raymond-liao",
        "attachment": null,
        "author": {"id": 38010, "username": "group_88966_bot", "name": "****", "state": "active", "avatar_url": null, "web_url": "https://jihulab.com/group_88966_bot"},
        "created_at": "2023-02-09T09:42:35.385+08:00",
        "updated_at": "2023-02-09T09:42:35.385+08:00",
        "system": false,
        "noteable_id": 315899,
        "noteable_type": "Issue",
        "resolvable": false,
        "confidential": false,
        "internal": false,
        "noteable_iid": 42,
        "commands_changes": {}
      }
    ]
  },
  {
    "id": "67769eead57532c23fe3d58ffd0f5c1a1477f882",
    "individual_note": true,
    "notes": [
      {
        "id": 2040472,
        "type": null,
        "body": "group message not found @group_88966_bot",
        "attachment": null,
        "author": {
          "id": 30192,
          "username": "raymond-liao",
          "name": "Raymond Liao",
          "state": "active",
          "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/30192/avatar.png",
          "web_url": "https://jihulab.com/raymond-liao"
        },
        "created_at": "2023-02-09T09:44:53.631+08:00",
        "updated_at": "2023-02-09T09:44:53.631+08:00",
        "system": false,
        "noteable_id": 315899,
        "noteable_type": "Issue",
        "resolvable": false,
        "confidential": false,
        "internal": false,
        "noteable_iid": 42,
        "commands_changes": {}
      }
    ]
  },
  {
    "id": "6d78ef22e6f9d5283d145038d1faa2e8870e026a",
    "individual_note": true,
    "notes": [
      {
        "id": 2040489,
        "type": null,
        "body": "mentioned in issue jihu-gitlab-app#521",
        "attachment": null,
        "author": {
          "id": 30192,
          "username": "raymond-liao",
          "name": "Raymond Liao",
          "state": "active",
          "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/30192/avatar.png",
          "web_url": "https://jihulab.com/raymond-liao"
        },
        "created_at": "2023-02-09T09:46:43.382+08:00",
        "updated_at": "2023-02-09T09:46:43.385+08:00",
        "system": true,
        "noteable_id": 315899,
        "noteable_type": "Issue",
        "resolvable": false,
        "confidential": false,
        "internal": false,
        "noteable_iid": 42,
        "commands_changes": {}
      }
    ]
  }
];
