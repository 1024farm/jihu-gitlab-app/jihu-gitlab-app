import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/iterable_extension.dart';

class MultiFinder extends MatchFinder {
  List<MatchFinder> finders;

  MultiFinder._(this.finders);

  factory MultiFinder.build(MatchFinder finder) {
    return MultiFinder._([finder]);
  }

  void concat(MatchFinder finder) {
    finders.add(finder);
  }

  @override
  bool matches(Element candidate) {
    for (var finder in finders) {
      if (!finder.matches(candidate)) return false;
    }
    return true;
  }

  @override
  String get description => finders.map((e) => e.description).reduceOr((value, element) => '$value and $element', '');
}
