import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_test/flutter_test.dart';

class SemanticsLabelFinder extends MatchFinder {
  final String semanticsLabel;

  SemanticsLabelFinder(this.semanticsLabel);

  @override
  bool matches(Element candidate) {
    return candidate.widget.runtimeType == SvgPicture &&
        (candidate.widget as SvgPicture).bytesLoader.runtimeType == SvgAssetLoader &&
        (candidate.widget as SvgPicture).semanticsLabel == semanticsLabel;
  }

  @override
  String get description => semanticsLabel;
}
