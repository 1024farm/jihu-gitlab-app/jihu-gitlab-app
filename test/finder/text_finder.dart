import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_test/flutter_test.dart';

RenderParagraph getTextRenderObject(tester, String text) {
  return tester.renderObject<RenderParagraph>(find.text(text));
}

RenderParagraph getTextRenderObjectFromMenuItemButton(tester, String text) {
  return tester.renderObject<RenderParagraph>(find.descendant(of: find.byType(MenuItemButton), matching: find.text(text)));
}
