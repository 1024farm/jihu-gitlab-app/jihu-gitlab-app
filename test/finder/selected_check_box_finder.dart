import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

class SelectedCheckBoxFinder extends MatchFinder {
  SelectedCheckBoxFinder();

  @override
  String get description => 'selected checkbox';

  @override
  bool matches(Element candidate) {
    if (candidate.widget is! Checkbox) return false;
    return (candidate.widget as Checkbox).value ?? false;
  }
}
