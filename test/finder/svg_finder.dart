import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_test/flutter_test.dart';

class SvgFinder extends MatchFinder {
  final String? assetName;
  final String? semanticsLabel;

  SvgFinder(this.assetName, {this.semanticsLabel});

  @override
  bool matches(Element candidate) {
    bool matches = candidate.widget.runtimeType == SvgPicture;
    if (assetName != null) {
      matches = matches && (candidate.widget as SvgPicture).bytesLoader.runtimeType == SvgAssetLoader && ((candidate.widget as SvgPicture).bytesLoader as SvgAssetLoader).assetName == assetName;
    }
    if (semanticsLabel != null) {
      matches = matches && (candidate.widget as SvgPicture).bytesLoader.runtimeType == SvgAssetLoader && (candidate.widget as SvgPicture).semanticsLabel == semanticsLabel;
    }
    return matches;
  }

  @override
  String get description => assetName ?? semanticsLabel ?? '';
}
