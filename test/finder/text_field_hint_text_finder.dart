import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

class TextFieldHintTextFinder extends MatchFinder {
  TextFieldHintTextFinder(this.hintText);

  final String hintText;

  @override
  String get description => 'text field with hint text "$hintText"';

  @override
  bool matches(Element candidate) {
    if (candidate.widget is! TextField) return false;
    final TextField textField = candidate.widget as TextField;
    return textField.decoration.runtimeType == InputDecoration && textField.decoration?.hintText == hintText;
  }
}
