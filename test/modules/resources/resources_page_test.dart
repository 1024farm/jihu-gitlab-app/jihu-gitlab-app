import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/l10n/current_locale.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter_android/webview_flutter_android.dart';
import 'package:webview_flutter_platform_interface/webview_flutter_platform_interface.dart';

import '../../test_binding_setter.dart';

void main() {
  group('Resources Page', () {
    testWidgets('Should display menu button on the top left corner', (tester) async {
      WebViewPlatform.instance = AndroidWebViewPlatform();
      await setUpCompatibleBinding(tester);
      await tester.pumpWidget(MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
        child: MaterialApp(
          onGenerateRoute: onGenerateRoute,
          home: const Scaffold(
            body: ResourcesPage(),
          ),
          localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
        ),
      ));
      await tester.pumpAndSettle();
      expect(find.byType(BackButton), findsOneWidget);

      ConnectionProvider().fullReset();
    });
  });
}
