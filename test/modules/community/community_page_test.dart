import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/db_manager.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/domain/assignee/assignee_entity.dart';
import 'package:jihu_gitlab_app/core/global_keys.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart' as r;
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/http_fail_view.dart';
import 'package:jihu_gitlab_app/core/widgets/label_view.dart';
import 'package:jihu_gitlab_app/core/widgets/search_box.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/l10n/current_locale.dart';
import 'package:jihu_gitlab_app/modules/community/community_page.dart';
import 'package:jihu_gitlab_app/modules/community/community_post_details_page.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion_entity.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/note/note_entity.dart';
import 'package:jihu_gitlab_app/modules/issues/index.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../core/net/http_request_test.mocks.dart';
import '../../finder/svg_finder.dart';
import '../../finder/text_field_hint_text_finder.dart';
import '../../mocker/tester.dart';
import '../../test_binding_setter.dart';
import '../../test_data/assignee.dart';
import '../../test_data/community.dart';
import '../../test_data/discussion.dart';
import '../../test_data/note.dart';
import '../ai/ai_page_test.mocks.dart';

void main() {
  var issueDetailsModel = IssueDetailsModel();
  locator.registerSingleton(issueDetailsModel);
  late MockHttpClient client;

  setUp(() async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();

    await ConnectionProvider().restore();
    ConnectionProvider().reset(Tester.jihuLabUser());
    client = MockHttpClient();
    ConnectionProvider().reset(Tester.jihuLabUser());
    when(client.postWithConnection('/api/graphql', getCommunityTypesGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/faq"), any))
        .thenAnswer((_) => Future(() => r.Response.of(communityTypesResponse)));
    when(client.postWithConnection('/api/graphql', getCommunityPostListGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', ['lang::en', 'type::WFH'], "", "", 20), any))
        .thenAnswer((_) => Future(() => r.Response.of(buildResponse(wfhResponse))));
    when(client.postWithConnection('/api/graphql', getCommunityPostListGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', ['lang::en', 'type::FAQ'], "", "", 20), any))
        .thenAnswer((_) => Future(() => r.Response.of(buildResponse(faqResponse))));
    when(client.postWithConnection('/api/graphql', getCommunityPostListGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', ['lang::en'], "", "", 20), any))
        .thenAnswer((_) => Future(() => r.Response.of(buildResponse(List.from(faqResponse)..addAll(wfhResponse)))));
    when(client.postWithConnection('/api/graphql', getCommunityPostListGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', ['lang::en'], "", "aaa", 20), any))
        .thenAnswer((_) => Future(() => r.Response.of(buildResponse([]))));
    when(client.postWithConnection('/api/graphql', getCommunityDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', 238), any))
        .thenAnswer((_) => Future(() => r.Response.of<dynamic>(communityDetailsGraphQLResponse)));
    when(client.postWithConnection('/api/graphql', getCommunityPostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', 238), any))
        .thenAnswer((_) => Future(() => r.Response.of<dynamic>(communityPostVotesGraphQLResponse)));
    when(client.getWithConnection("/api/v4/projects/89335/issues/238/award_emoji?per_page=100&page=1", any)).thenAnswer((_) => Future(() => r.Response.of([])));
    when(client.getWithConnection<Map<String, dynamic>>("/api/v4/projects/89335", any)).thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>({
          "id": 89335,
          "description": "技术问答、远程工作机会",
          "name": "FAQ",
          "name_with_namespace": "旗舰版演示 / 极狐 GitLab App 产品线 / FAQ",
          "path": "faq",
          "path_with_namespace": "ultimate-plan/jihu-gitlab-app/faq"
        })));
    when(client.getWithConnection("/api/v4/projects/89335/issues/238/discussions?page=1&per_page=50", any)).thenAnswer((_) => Future(() => r.Response.of([])));
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets('Should be able to view faq', (tester) async {
    setUpMobileBinding(tester);
    await tester.pumpWidget(MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => ConnectionProvider()),
        ChangeNotifierProvider(create: (context) => ProjectProvider()),
        ChangeNotifierProvider(create: (context) => LocaleProvider())
      ],
      child: MaterialApp(
        onGenerateRoute: onGenerateRoute,
        home: const Scaffold(body: CommunityPage()),
        localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
      ),
    ));

    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.byType(Tab), findsNWidgets(3));
    expect(find.widgetWithText(Tab, "All"), findsOneWidget);
    expect(find.widgetWithText(Tab, "FAQ"), findsOneWidget);
    expect(find.widgetWithText(Tab, "WFH"), findsOneWidget);

    expect(find.text("Why remote?"), findsOneWidget);
    expect(find.text("Why Digital Transformation?"), findsOneWidget);
    expect(find.byType(SearchBox), findsOneWidget);

    await tester.tap(find.widgetWithText(Tab, "FAQ"));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    verify(client.postWithConnection('/api/graphql', getCommunityPostListGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', ['lang::en', 'type::FAQ'], "", "", 20), any)).called(1);
    expect(find.text("Why Digital Transformation?"), findsOneWidget);
    expect(find.text("Why remote?"), findsNothing);

    await tester.tap(find.widgetWithText(Tab, "All"));
    await tester.pumpAndSettle();
    verify(client.postWithConnection('/api/graphql', getCommunityPostListGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', ['lang::en'], "", "", 20), any)).called(1);
    expect(find.text("Why Digital Transformation?"), findsOneWidget);
    expect(find.text("Why remote?"), findsOneWidget);
    var textFieldFinder = TextFieldHintTextFinder('Search');
    expect(textFieldFinder, findsOneWidget);
    await tester.enterText(textFieldFinder, "aaa");
    await tester.testTextInput.receiveAction(TextInputAction.done);
    await tester.pumpAndSettle();
    expect(find.text("No matches for 'aaa'"), findsOneWidget);
    await tester.tap(find.byType(SearchBox));
    await tester.pumpAndSettle();
    expect(SvgFinder("assets/images/clear.svg"), findsOneWidget);
    await tester.tap(SvgFinder("assets/images/clear.svg"));
    await tester.pumpAndSettle();
    expect(find.text("No matches for 'aaa'"), findsNothing);
    expect(find.text("Why remote?"), findsOneWidget);

    await tester.tap(find.text("Why remote?"));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.byType(CommunityPostDetailsPage), findsOneWidget);
  });

  testWidgets("Should be able to switch faq type in pad", (tester) async {
    setUpDesktopBinding(tester);
    await tester.pumpWidget(MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (context) => ConnectionProvider()),
          ChangeNotifierProvider(create: (context) => ProjectProvider()),
          ChangeNotifierProvider(create: (context) => LocaleProvider())
        ],
        child: MaterialApp(
            onGenerateRoute: onGenerateRoute,
            home: Scaffold(key: GlobalKeys.rootAppKey, body: const CommunityPage()),
            localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate])));

    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.byType(Tab), findsNWidgets(3));
    expect(find.widgetWithText(Tab, "All"), findsOneWidget);
    expect(find.widgetWithText(Tab, "FAQ"), findsOneWidget);
    expect(find.widgetWithText(Tab, "WFH"), findsOneWidget);
    expect(find.byType(CommunityPostDetailsPage), findsOneWidget);
    expect(find.text("No post selected"), findsOneWidget);
    expect(find.text("Why remote?"), findsOneWidget);

    await tester.tap(find.text("Why remote?"));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.widgetWithText(CommonAppBar, 'WFH'), findsOneWidget);
    expect(find.text("Community detail for test"), findsOneWidget);
    expect(find.byType(BackButton), findsNothing);
    var listItemFinder = find.byWidgetPredicate((widget) {
      return widget.runtimeType == Container &&
          (widget as Container).decoration.runtimeType == BoxDecoration &&
          (widget.decoration as BoxDecoration).border == Border.all(color: Theme.of(GlobalKeys.rootAppKey.currentContext!).colorScheme.primary);
    });
    expect(listItemFinder, findsOneWidget);

    await tester.tap(find.widgetWithText(Tab, "FAQ"));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.text("Why remote?"), findsNothing);
    expect(find.text("Why Digital Transformation?"), findsOneWidget);
    expect(find.text("No post selected"), findsOneWidget);

    await tester.tap(find.widgetWithText(Tab, "All"));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(listItemFinder, findsNothing);
  });

  testWidgets('Should display request error view when fetch types failed', (tester) async {
    setUpMobileBinding(tester);
    when(client.postWithConnection('/api/graphql', getCommunityTypesGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/faq"), any)).thenThrow(Exception());

    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
      child: MaterialApp(
        onGenerateRoute: onGenerateRoute,
        home: const Scaffold(body: CommunityPage()),
      ),
    ));

    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.byType(Tab), findsNothing);
    expect(find.byType(HttpFailView), findsOneWidget);

    when(client.postWithConnection('/api/graphql', argThat(predicate((arg) => arg.toString().contains("labels"))), any)).thenAnswer((_) => Future(() => r.Response.of(communityTypesResponse)));
    await tester.tap(SvgFinder('assets/images/refresh_exception.svg'));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.byType(HttpFailView), findsNothing);
    expect(find.byType(Tab), findsNWidgets(3));
    expect(find.widgetWithText(Tab, "All"), findsOneWidget);
    expect(find.widgetWithText(Tab, "FAQ"), findsOneWidget);
    expect(find.widgetWithText(Tab, "WFH"), findsOneWidget);
  });

  testWidgets('Should display labels which not start with "lang" and "type"', (tester) async {
    setUpMobileBinding(tester);

    when(client.postWithConnection('/api/graphql', getCommunityPostListGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/faq", ["lang::en", "type::WFH"], '', '', 20), any))
        .thenAnswer((_) => Future(() => r.Response.of(buildResponse(wfhWithCustomLabelsResponse))));

    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
      child: MaterialApp(
        onGenerateRoute: onGenerateRoute,
        home: const Scaffold(body: CommunityPage()),
      ),
    ));

    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.byType(Tab), findsNWidgets(3));
    expect(find.widgetWithText(Tab, "All"), findsOneWidget);
    expect(find.widgetWithText(Tab, "FAQ"), findsOneWidget);
    expect(find.widgetWithText(Tab, "WFH"), findsOneWidget);
    expect(find.text("Why remote?"), findsOneWidget);
    expect(find.text("Why Digital Transformation?"), findsOneWidget);

    await tester.tap(find.widgetWithText(Tab, "WFH"));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.text("Why Digital Transformation?"), findsNothing);
    expect(find.text("Why remote?"), findsOneWidget);
    expect(find.byWidgetPredicate((widget) => widget is LabelView && widget.left == 'for' && widget.right == 'geek'), findsOneWidget);
    expect(find.byWidgetPredicate((widget) => widget is LabelView && widget.left == 'type' && widget.right == 'WFH'), findsNothing);
    expect(find.byWidgetPredicate((widget) => widget is LabelView && widget.left == 'lang' && widget.right == 'en'), findsNothing);
    expect(find.byWidgetPredicate((widget) => widget is LabelView && widget.left == 'test' && widget.right == '24'), findsOneWidget);
  });

  group('Should check logic of notice of maintenance', () {
    MockDatabase mockDatabase = MockDatabase();
    DbManager.instance().injectDatabaseForTesting(mockDatabase);
    when(mockDatabase.insert(DiscussionEntity.tableName, any)).thenAnswer((_) => Future(() => 1));
    when(mockDatabase.transaction(any)).thenAnswer((_) => Future(() => <int>[]));
    when(mockDatabase.rawQuery("select * from ${DiscussionEntity.tableName} where issue_id = 326489")).thenAnswer((_) => Future(() => [discussionDbData]));
    when(mockDatabase.insert(NoteEntity.tableName, any)).thenAnswer((_) => Future(() => 1));
    when(mockDatabase.rawQuery("select * from ${NoteEntity.tableName} where discussion_id = '123'")).thenAnswer((_) => Future(() => [noteDbData]));
    when(mockDatabase.insert(AssigneeEntity.tableName, any)).thenAnswer((_) => Future(() => 1));
    when(mockDatabase.query(AssigneeEntity.tableName, where: " assignee_id = ? ", whereArgs: [29355])).thenAnswer((_) => Future(() => [assigneeDbData]));

    testWidgets('Should display notice of maintenance when project is archived', (tester) async {
      setUpMobileBinding(tester);

      when(client.postWithConnection('/api/graphql', getCommunityTypesGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/faq"), any))
          .thenAnswer((_) => Future(() => r.Response.of(archivedCommunityTypesResponse)));

      await tester.pumpWidget(MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
        child: MaterialApp(
          onGenerateRoute: onGenerateRoute,
          home: const Scaffold(body: CommunityPage()),
        ),
      ));

      await tester.pumpAndSettle(const Duration(seconds: 1));
      expect(find.byType(Tab), findsNWidgets(3));
      expect(find.widgetWithText(Tab, "All"), findsOneWidget);
      expect(find.widgetWithText(Tab, "FAQ"), findsOneWidget);
      expect(find.widgetWithText(Tab, "WFH"), findsOneWidget);
      expect(SvgFinder('assets/images/warning.svg'), findsOneWidget);
      expect(find.text('The community is under maintenance, only browse.'), findsOneWidget);

      expect(find.text("Why remote?"), findsOneWidget);
      expect(find.text("Why Digital Transformation?"), findsOneWidget);
      await tester.tap(find.text("Why remote?"));
      await tester.pumpAndSettle(const Duration(seconds: 1));
      expect(find.byType(CommunityPage), findsNothing);
      expect(find.byType(CommunityPostDetailsPage), findsOneWidget);
      expect(find.widgetWithText(CommonAppBar, 'WFH'), findsOneWidget);
      expect(SvgFinder("assets/images/comment.svg"), findsNothing);
      expect(SvgFinder('assets/images/warning.svg'), findsOneWidget);
      expect(find.text('The community is under maintenance, only browse.'), findsOneWidget);
      expect(find.text('assigned to @perity'), findsOneWidget);

      final listFinder = find.byType(Scrollable).last;
      await tester.scrollUntilVisible(find.text('assigned to @perity'), 500.0, scrollable: listFinder);
      expect(SvgFinder('assets/images/reply.svg'), findsNothing);
    });

    testWidgets('Should not display notice of maintenance when project is not archived', (tester) async {
      setUpMobileBinding(tester);

      when(client.postWithConnection('/api/graphql', getCommunityTypesGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/faq"), any))
          .thenAnswer((_) => Future(() => r.Response.of(communityTypesResponse)));

      await tester.pumpWidget(MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
        child: MaterialApp(
          onGenerateRoute: onGenerateRoute,
          home: const Scaffold(body: CommunityPage()),
        ),
      ));

      await tester.pumpAndSettle(const Duration(seconds: 1));
      expect(find.byType(Tab), findsNWidgets(3));
      expect(find.widgetWithText(Tab, "All"), findsOneWidget);
      expect(find.widgetWithText(Tab, "FAQ"), findsOneWidget);
      expect(find.widgetWithText(Tab, "WFH"), findsOneWidget);
      expect(SvgFinder('assets/images/warning.svg'), findsNothing);
      expect(find.text('The community is under maintenance, only browse.'), findsNothing);

      expect(find.text("Why remote?"), findsOneWidget);
      expect(find.text("Why Digital Transformation?"), findsOneWidget);
      await tester.tap(find.text("Why remote?"));
      await tester.pumpAndSettle(const Duration(seconds: 1));
      expect(find.byType(CommunityPage), findsNothing);
      expect(find.byType(CommunityPostDetailsPage), findsOneWidget);
      expect(find.widgetWithText(CommonAppBar, 'WFH'), findsOneWidget);
      expect(SvgFinder("assets/images/comment.svg"), findsOneWidget);
      expect(SvgFinder('assets/images/warning.svg'), findsNothing);
      expect(find.text('The community is under maintenance, only browse.'), findsNothing);
      expect(SvgFinder('assets/images/reply.svg'), findsWidgets);
    });
  });

  tearDown(() {
    ConnectionProvider().fullReset();
    ProjectProvider().fullReset();
    reset(client);
  });
}

Map<String, dynamic> communityTypesResponse = {
  "data": {
    "project": {
      "archived": false,
      "labels": {
        "nodes": [
          {
            "id": "gid://gitlab/ProjectLabel/72395",
            "title": "type::FAQ",
            "description": "问答",
          },
          {
            "id": "gid://gitlab/ProjectLabel/72059",
            "title": "type::WFH",
            "description": "远程办公",
          },
        ]
      }
    }
  }
};

Map<String, dynamic> archivedCommunityTypesResponse = {
  "data": {
    "project": {
      "archived": true,
      "labels": {
        "nodes": [
          {
            "id": "gid://gitlab/ProjectLabel/72395",
            "title": "type::FAQ",
            "description": "问答",
          },
          {
            "id": "gid://gitlab/ProjectLabel/72059",
            "title": "type::WFH",
            "description": "远程办公",
          },
        ]
      }
    }
  }
};

List<Map<String, dynamic>> faqResponse = [
  {
    "id": "gid://gitlab/Issue/326473",
    "iid": "237",
    "title": "Why Digital Transformation?",
    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/faq/-/issues/237",
    "confidential": false,
    "state": "opened",
    "createdAt": "2023-02-21T09:16:59+08:00",
    "author": {"name": "yajie xue", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png"},
    "labels": {
      "nodes": [
        {"id": "gid://gitlab/ProjectLabel/72406", "title": "lang::en", "description": "英文", "color": "#9400d3", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72395", "title": "type::FAQ", "description": "问答", "color": "#6699cc", "textColor": "#FFFFFF"}
      ]
    }
  }
];

List<Map<String, dynamic>> wfhResponse = [
  {
    "id": "gid://gitlab/Issue/326489",
    "iid": "238",
    "title": "Why remote?",
    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/faq/-/issues/238",
    "confidential": false,
    "state": "opened",
    "createdAt": "2023-02-21T09:19:40+08:00",
    "author": {"name": "yajie xue", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png"},
    "labels": {
      "nodes": [
        {"id": "gid://gitlab/ProjectLabel/74785", "title": "for::geek", "description": "", "color": "#6699cc", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72406", "title": "lang::en", "description": "英文", "color": "#9400d3", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72059", "title": "type::WFH", "description": "远程办公", "color": "#6699cc", "textColor": "#FFFFFF"}
      ]
    }
  }
];

List<Map<String, dynamic>> wfhWithCustomLabelsResponse = [
  {
    "id": "gid://gitlab/Issue/326489",
    "iid": "238",
    "title": "Why remote?",
    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/faq/-/issues/238",
    "confidential": false,
    "state": "opened",
    "createdAt": "2023-02-21T09:19:40+08:00",
    "author": {"name": "yajie xue", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png"},
    "labels": {
      "nodes": [
        {"id": "gid://gitlab/ProjectLabel/74785", "title": "for::geek", "description": "", "color": "#6699cc", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72406", "title": "lang::en", "description": "英文", "color": "#9400d3", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72059", "title": "type::WFH", "description": "远程办公", "color": "#6699cc", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72001", "title": "test::1", "description": "远程办公1", "color": "#6699cc", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72002", "title": "test::2", "description": "远程办公2", "color": "#6699cc", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72003", "title": "test::3", "description": "远程办公3", "color": "#6699cc", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72004", "title": "test::4", "description": "远程办公4", "color": "#6699cc", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72005", "title": "test::5", "description": "远程办公5", "color": "#6699cc", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72006", "title": "test::6", "description": "远程办公6", "color": "#6699cc", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72007", "title": "test::7", "description": "远程办公7", "color": "#6699cc", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72008", "title": "test::8", "description": "远程办公8", "color": "#6699cc", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72009", "title": "test::9", "description": "远程办公9", "color": "#6699cc", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72010", "title": "test::12", "description": "远程办公11", "color": "#6699cc", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72011", "title": "test::13", "description": "远程办公12", "color": "#6699cc", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72012", "title": "test::14", "description": "远程办公13", "color": "#6699cc", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72013", "title": "test::15", "description": "远程办公14", "color": "#6699cc", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72014", "title": "test::16", "description": "远程办公15", "color": "#6699cc", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72015", "title": "test::17", "description": "远程办公111", "color": "#6699cc", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72016", "title": "test::18", "description": "远程办公222", "color": "#6699cc", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72017", "title": "test::19", "description": "远程办公333", "color": "#6699cc", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72018", "title": "test::22", "description": "远程办公444", "color": "#6699cc", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72019", "title": "test::23", "description": "远程办公555", "color": "#6699cc", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72020", "title": "test::25", "description": "远程办公666", "color": "#6699cc", "textColor": "#FFFFFF"},
        {"id": "gid://gitlab/ProjectLabel/72021", "title": "test::24", "description": "远程办公777", "color": "#6699cc", "textColor": "#FFFFFF"}
      ]
    }
  }
];

Map<String, dynamic> buildResponse(List<Map<String, dynamic>> faqs) => {
      "data": {
        "project": {
          "fullPath": "ultimate-plan/jihu-gitlab-app/faq",
          "id": "gid://gitlab/Project/89335",
          "issues": {
            "nodes": faqs,
            "pageInfo": {"hasNextPage": false, "endCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMy0wMi0yMSAwOToxOTo0MC43Nzk0MjYwMDAgKzA4MDAiLCJpZCI6IjMyNjQ4OSJ9"}
          }
        }
      }
    };
