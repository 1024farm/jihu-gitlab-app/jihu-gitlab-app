import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/db_manager.dart';
import 'package:jihu_gitlab_app/core/global_keys.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/system_version_detector.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar_and_name.dart';
import 'package:jihu_gitlab_app/core/widgets/award_emoji_button.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/input_field_with_title.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_button.dart';
import 'package:jihu_gitlab_app/core/widgets/markdown/markdown_input_box.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/l10n/current_locale.dart';
import 'package:jihu_gitlab_app/modules/auth/auth.dart';
import 'package:jihu_gitlab_app/modules/community/community_post_details_page.dart';
import 'package:jihu_gitlab_app/modules/community/community_post_edit_page.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion_provider.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../core/net/http_request_test.mocks.dart';
import '../../finder/semantics_label_finder.dart';
import '../../finder/svg_finder.dart';
import '../../finder/text_field_hint_text_finder.dart';
import '../../mocker/tester.dart';
import '../../test_binding_setter.dart';
import '../../test_data/community.dart';
import '../../test_data/discussion.dart';
import '../../test_data/project.dart';
import '../ai/ai_page_test.mocks.dart';
import '../issues/issue_creation_page_test.mocks.dart';
import 'community_post_details_page_test.mocks.dart';

final client = MockHttpClient();

@GenerateNiceMocks([MockSpec<DiscussionProvider>(), MockSpec<SystemVersionDetector>()])
void main() {
  var params = {'issueId': 3242, 'issueIid': 6, 'communityTypeName': 'WFH'};

  setUp(() async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    await ConnectionProvider().restore();
    ConnectionProvider().reset(Tester.jihuLabUser());

    when(client.getWithConnection("/api/v4/projects/89335/issues/6/discussions?page=1&per_page=50", any)).thenAnswer((_) => Future(() => Response.of(discussionsData)));
    when(client.getWithConnection("/api/v4/projects/89335/issues/6/award_emoji?per_page=100&page=1", any)).thenAnswer((_) => Future(() => Response.of([])));
    when(client.postWithConnection('/api/graphql', getCommunityDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', 6), any))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(communityDetailsGraphQLResponse)));
    when(client.postWithConnection('/api/graphql', getCommunityPostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', 6), any))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(communityPostVotesGraphQLResponse)));
    when(client.getWithHeader<Map<String, dynamic>>("https://jihulab.com/api/v4/projects/89335", {"PRIVATE-TOKEN": "", "authorization": ""}))
        .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(projectData)));
    when(client.post<dynamic>("/api/v4/projects/89335/issues/6/notes", any)).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
    HttpClient.injectInstanceForTesting(client);
  });

  //TODO: Load image
  testWidgets('Should display issues data view with data', (tester) async {
    await setUpMobileBinding(tester);
    when(client.post<dynamic>("/api/v4/projects/89335/issues/6/discussions", {'body': "test-comment"})).thenAnswer((_) => Future(() => Response.of<dynamic>({
          "id": "200",
          "individual_note": false,
        })));
    HttpClient.injectInstanceForTesting(client);

    await tester.pumpWidget(MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (context) => ConnectionProvider()),
          ChangeNotifierProvider(create: (context) => ProjectProvider()),
        ],
        child: MaterialApp(
          home: Scaffold(
            body: CommunityPostDetailsPage(arguments: params),
          ),
        )));

    await tester.pumpAndSettle();
    expect(find.widgetWithText(CommonAppBar, 'WFH'), findsOneWidget);
    expect(find.text(AppLocalizations.dictionary().commentPlaceholder), findsOneWidget);
    await tester.tap(find.text(AppLocalizations.dictionary().commentPlaceholder));
    await tester.pumpAndSettle();
    await tester.enterText(TextFieldHintTextFinder(AppLocalizations.dictionary().commentPlaceholder), "test-comment");
    expect(find.text("test-comment"), findsOneWidget);
    await tester.pumpAndSettle();
    expect(find.byKey(const Key('comment-reply-inkwell')), findsOneWidget);
    await tester.tap(find.byKey(const Key('comment-reply-inkwell')));
    await tester.pumpAndSettle();
    for (int i = 0; i < 5; i++) {
      await tester.pump(const Duration(seconds: 1));
    }
    expect(find.text("test-comment"), findsNothing);
  });

  testWidgets('Should display empty view with none params provide', (tester) async {
    await tester.pumpWidget(const MaterialApp(
      home: Scaffold(
        body: CommunityPostDetailsPage(arguments: {}),
      ),
    ));

    await tester.pumpAndSettle();
    expect(find.textContaining(AppLocalizations.dictionary().noItemSelected("post")), findsOneWidget);
  });

  testWidgets("Should be able to copy", (tester) async {
    await tester.pumpWidget(MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (context) => ConnectionProvider()),
          ChangeNotifierProvider(create: (context) => ProjectProvider()),
        ],
        child: MaterialApp(
          home: Scaffold(
            body: CommunityPostDetailsPage(arguments: params),
          ),
          localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
        )));

    await tester.pumpAndSettle();
    expect(SemanticsLabelFinder('share'), findsOneWidget);
    await tester.pumpAndSettle();
    await tester.tap(SemanticsLabelFinder('share'));
    await tester.pumpAndSettle();
    expect(find.text("Copy link"), findsOneWidget);
    expect(find.text("Copy title & link"), findsOneWidget);

    await tester.tap(find.text('Copy link'));
    for (int i = 0; i < 5; i++) {
      await tester.pumpAndSettle(const Duration(seconds: 1));
    }

    await tester.tap(SemanticsLabelFinder('share'));
    await tester.pumpAndSettle();
    await tester.tap(find.text('Copy title & link'));
    for (int i = 0; i < 5; i++) {
      await tester.pumpAndSettle(const Duration(seconds: 1));
    }
    await tester.tap(find.textContaining("months ago"));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.textContaining("Nov 25, 2022"), findsWidgets);
    await tester.tap(find.textContaining("Nov 25, 2022"));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.textContaining("months ago"), findsWidgets);
  });

  group('Should test Discussion', () {
    testWidgets('Should able to send comment when @ someone only', (tester) async {
      await setUpMobileBinding(tester);
      when(client.post<dynamic>("/api/v4/projects/89335/issues/6/discussions", {'body': "test-comment"})).thenAnswer((_) => Future(() => Response.of<dynamic>({
            "id": "200",
            "individual_note": false,
          })));
      when(client.get<List<dynamic>>("/api/v4/projects/89335/members/all?page=1&per_page=50")).thenAnswer((_) async => Future.value(Response.of([])));
      HttpClient.injectInstanceForTesting(client);

      MockDatabase mockDatabase = MockDatabase();
      DbManager.instance().injectDatabaseForTesting(mockDatabase);
      when(mockDatabase.query("project_members", where: " project_id = ? ", whereArgs: [89335])).thenAnswer((_) => Future(() {
            return [
              {"id": 1, "member_id": 1, "project_id": 89335, "name": "test_1", "username": "tester_1", "avatar_url": "avatar_url_1", "version": 1},
              {"id": 2, "member_id": 2, "project_id": 89335, "name": "test_2", "username": "tester_2", "avatar_url": "avatar_url_2", "version": 1},
              {"id": 3, "member_id": 3, "project_id": 89335, "name": "test_11", "username": "tester_11", "avatar_url": "avatar_url_11", "version": 1},
              {"id": 4, "member_id": 4, "project_id": 89335, "name": "test_22", "username": "tester_22", "avatar_url": "avatar_url_22", "version": 1},
            ];
          }));

      var navigatorObserver = MockNavigatorObserver();
      await tester.pumpWidget(MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (context) => ConnectionProvider()),
            ChangeNotifierProvider(create: (context) => ProjectProvider()),
            ChangeNotifierProvider(create: (context) => LocaleProvider())
          ],
          child: MaterialApp(
            navigatorObservers: [navigatorObserver],
            onGenerateRoute: onGenerateRoute,
            home: Scaffold(
              body: CommunityPostDetailsPage(arguments: params),
            ),
            localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
          )));

      await tester.pumpAndSettle();
      expect(find.widgetWithText(CommonAppBar, 'WFH'), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().commentPlaceholder), findsOneWidget);
      await tester.tap(find.text(AppLocalizations.dictionary().commentPlaceholder));
      await tester.pumpAndSettle();
      expect(find.byWidgetPredicate((widget) => widget is LoadingButton && widget.disabled), findsOneWidget);
      expect(SvgFinder('assets/images/alternate_email.svg'), findsOneWidget);

      await tester.tap(SvgFinder('assets/images/alternate_email.svg'));
      await tester.pumpAndSettle();
      verify(navigatorObserver.didPush(any, any));
      expect(find.text(AppLocalizations.dictionary().selectContact), findsOneWidget);
      expect(find.byType(TextField), findsOneWidget);
      await tester.pumpAndSettle();
      expect(find.byType(AvatarAndName), findsNWidgets(4));

      await tester.tap(find.text("tester_22"));
      await tester.pumpAndSettle();
      verify(navigatorObserver.didPop(any, any));
      expect(find.byWidgetPredicate((widget) => widget is LoadingButton && widget.disabled), findsNothing);
      expect(find.byWidgetPredicate((widget) => widget is LoadingButton && !widget.disabled), findsOneWidget);
    });

    testWidgets('Should display discussions when discussions response with individualNote is false', (tester) async {
      var parameters = {
        'issueId': 3242,
        'issueIid': 6,
        'targetUrl': "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo-mr-test/-/issues/6#note_11",
      };
      await tester.pumpWidget(MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (context) => ConnectionProvider()),
            ChangeNotifierProvider(create: (context) => ProjectProvider()),
          ],
          child: MaterialApp(
            home: Scaffold(
              body: CommunityPostDetailsPage(arguments: parameters),
            ),
            localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
          )));

      for (int i = 0; i < 5; i++) {
        await tester.pump(const Duration(seconds: 1));
      }
      for (int i = 0; i < 5; i++) {
        await tester.pump(const Duration(seconds: 1));
      }

      // TODO: 缓存 DiscussionProvider 会导致页面数据不刷新

      await tester.tap(find.byType(CommunityPostDetailsPage));
    });

    testWidgets('Should display discussions when discussions response with individualNote is true', (tester) async {
      var parameters = {
        'issueId': 3242,
        'issueIid': 6,
        'targetUrl': "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo-mr-test/-/issues/6#note_6",
      };

      await tester.pumpWidget(MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (context) => ConnectionProvider()),
            ChangeNotifierProvider(create: (context) => ProjectProvider()),
          ],
          child: MaterialApp(
            home: Scaffold(
              body: CommunityPostDetailsPage(arguments: parameters),
            ),
            localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
          )));

      for (int i = 0; i < 5; i++) {
        await tester.pump(const Duration(seconds: 1));
      }
      for (int i = 0; i < 5; i++) {
        await tester.pump(const Duration(seconds: 1));
      }

      // TODO: 缓存 DiscussionProvider 会导致页面数据不刷新
    });
  });

  group('Should test hint display logic', () {
    testWidgets('Should be able to see hint when the system type is unmaintainable', (tester) async {
      var mockSystemVersionDetector = MockSystemVersionDetector();
      when(mockSystemVersionDetector.detect()).thenAnswer((_) => Future(() => SystemType.unmaintainable));
      SystemVersionDetector.injectInstanceForTesting(mockSystemVersionDetector);
      when(client.postWithConnection('/api/graphql', getCommunityDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', 6), any))
          .thenAnswer((_) => Future(() => Response.of<dynamic>(geekCommunityDetailsGraphQLResponse)));

      await tester.pumpWidget(MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (context) => ConnectionProvider()),
            ChangeNotifierProvider(create: (context) => ProjectProvider()),
          ],
          child: MaterialApp(
              home: Scaffold(
                body: CommunityPostDetailsPage(arguments: params),
              ),
              localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate])));

      await tester.pumpAndSettle(const Duration(seconds: 1));
      expect(SvgFinder("assets/images/warning.svg"), findsOneWidget);
    });

    testWidgets('Should be able to see hint when the system type is maintainable', (tester) async {
      var mockSystemVersionDetector = MockSystemVersionDetector();
      when(mockSystemVersionDetector.detect()).thenAnswer((_) => Future(() => SystemType.maintainable));
      SystemVersionDetector.injectInstanceForTesting(mockSystemVersionDetector);
      when(client.postWithConnection('/api/graphql', getCommunityDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', 6), any))
          .thenAnswer((_) => Future(() => Response.of<dynamic>(geekCommunityDetailsGraphQLResponse)));

      await tester.pumpWidget(MaterialApp(
          home: Scaffold(
            body: CommunityPostDetailsPage(arguments: params),
          ),
          localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate]));

      await tester.pumpAndSettle(const Duration(seconds: 1));
      expect(SvgFinder("assets/images/warning.svg"), findsOneWidget);
    });
  });

  testWidgets('Should login from login box in issue comments and show discussions', (tester) async {
    await setUpMobileBinding(tester);

    ConnectionProvider().fullReset();

    await tester.pumpWidget(MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => ConnectionProvider()),
        ChangeNotifierProvider(create: (context) => ProjectProvider()),
      ],
      child: MaterialApp(
          onGenerateRoute: onGenerateRoute,
          home: Scaffold(
            key: GlobalKeys.rootAppKey,
            body: CommunityPostDetailsPage(arguments: params),
          ),
          localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate]),
    ));

    await tester.pumpAndSettle();
    expect(find.widgetWithText(CommonAppBar, 'WFH'), findsOneWidget);
    expect(find.text('Why remote?'), findsOneWidget);
    expect(find.text('Community detail for test'), findsOneWidget);
    expect(find.text('Comments'), findsOneWidget);
    expect(find.textContaining('Connect with'), findsOneWidget);
    expect(find.textContaining('to continue'), findsOneWidget);
    Finder tapItem = find.textContaining('jihulab.com', findRichText: true);
    expect(tapItem, findsOneWidget);
    await tester.tap(tapItem);
    await tester.pumpAndSettle();
    expect(find.byType(LoginPage), findsOneWidget);
    expect(find.byType(CommunityPostDetailsPage), findsNothing);
    ConnectionProvider().reset(Tester.jihuLabUser());
    await tester.tap(find.byType(BackButton));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.byType(LoginPage), findsNothing);
    expect(find.byType(CommunityPostDetailsPage), findsOneWidget);
    expect(find.textContaining('Connect with'), findsNothing);
    expect(find.textContaining('to continue'), findsNothing);
    expect(tapItem, findsNothing);
    expect(find.text(AppLocalizations.dictionary().commentPlaceholder), findsOneWidget);
    expect(SvgFinder("assets/images/comment.svg"), findsOneWidget);
    expect(SvgFinder("assets/images/operate.svg"), findsOneWidget);
  });

  group('Should test edit issue permission of user', () {
    testWidgets('Should display edit button when user has edit permission', (tester) async {
      await setUpMobileBinding(tester);

      when(client.postWithConnection('/api/graphql', getCommunityDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', 6), any))
          .thenAnswer((_) => Future(() => Response.of<dynamic>(communityDetailsGraphQLResponse)));

      await tester.pumpWidget(MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (context) => ConnectionProvider()),
            ChangeNotifierProvider(create: (context) => ProjectProvider()),
          ],
          child: MaterialApp(
            home: Scaffold(
              key: GlobalKeys.rootAppKey,
              body: CommunityPostDetailsPage(arguments: params),
            ),
          )));

      await tester.pumpAndSettle();
      expect(find.widgetWithText(CommonAppBar, 'WFH'), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().commentPlaceholder), findsOneWidget);
      expect(SvgFinder("assets/images/comment.svg"), findsOneWidget);
      expect(SvgFinder("assets/images/operate.svg"), findsOneWidget);

      await tester.tap(SvgFinder("assets/images/operate.svg"));
      await tester.pumpAndSettle();
      expect(find.widgetWithText(InkWell, 'Edit'), findsOneWidget);
      await tester.tap(find.widgetWithText(InkWell, 'Edit'));
      await tester.pumpAndSettle();
      expect(find.byType(CommunityPostEditPage), findsOneWidget);
      expect(find.byType(CommunityPostDetailsPage), findsNothing);
      expect(find.widgetWithText(InkWell, 'Edit'), findsNothing);
      expect(find.widgetWithText(CommonAppBar, 'Edit'), findsOneWidget);
      expect(find.widgetWithText(InputFieldWithTitle, 'Title (required)'), findsOneWidget);
      expect(find.text('Title (required)'), findsOneWidget);
      expect(find.text('Description'), findsOneWidget);
      expect(find.byType(MarkdownInputBox), findsOneWidget);

      var titleTextField = find.widgetWithText(TextField, 'Why remote?');
      expect(titleTextField, findsOneWidget);
      var descriptionTextField = find.widgetWithText(TextField, 'Community detail for test');
      expect(descriptionTextField, findsOneWidget);

      // Remove focus from textField
      await tester.tap(titleTextField);
      await tester.pumpAndSettle();
      expect(tester.testTextInput.isVisible, isTrue);
      await tester.tap(find.text('Title (required)'));
      await tester.pumpAndSettle();
      expect(tester.testTextInput.isVisible, isFalse);

      // Enable update
      var buttonFinder = find.widgetWithText(TextButton, "Update");
      expect(buttonFinder, findsOneWidget);
      expect((tester.firstWidget<TextButton>(buttonFinder).child as Text).style!.color, Theme.of(GlobalKeys.rootAppKey.currentContext!).primaryColor.withOpacity(0.5));

      await tester.enterText(titleTextField, 'New Title');
      await tester.pumpAndSettle();
      expect((tester.firstWidget<TextButton>(buttonFinder).child as Text).style!.color, Theme.of(GlobalKeys.rootAppKey.currentContext!).primaryColor.withOpacity(1));

      titleTextField = find.widgetWithText(TextField, 'New Title');
      await tester.enterText(titleTextField, 'Why remote?');
      await tester.pumpAndSettle();
      expect((tester.firstWidget<TextButton>(buttonFinder).child as Text).style!.color, Theme.of(GlobalKeys.rootAppKey.currentContext!).primaryColor.withOpacity(0.5));

      await tester.enterText(descriptionTextField, 'New Description');
      await tester.pumpAndSettle();
      expect((tester.firstWidget<TextButton>(buttonFinder).child as Text).style!.color, Theme.of(GlobalKeys.rootAppKey.currentContext!).primaryColor.withOpacity(1));

      descriptionTextField = find.widgetWithText(TextField, 'New Description');
      await tester.enterText(descriptionTextField, 'Community detail for test');
      await tester.pumpAndSettle();
      expect((tester.firstWidget<TextButton>(buttonFinder).child as Text).style!.color, Theme.of(GlobalKeys.rootAppKey.currentContext!).primaryColor.withOpacity(0.5));

      // Update
      titleTextField = find.widgetWithText(TextField, 'Why remote?');
      descriptionTextField = find.widgetWithText(TextField, 'Community detail for test');
      await tester.enterText(titleTextField, 'New Title');
      await tester.enterText(descriptionTextField, 'New Description');
      await tester.pumpAndSettle();

      when(client.put<Map<String, dynamic>>('/api/v4/projects/89335/issues/238', {'title': 'New Title', 'description': 'New Description'}, connection: argThat(isNotNull, named: 'connection')))
          .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>({})));
      when(client.postWithConnection('/api/graphql', getCommunityDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', 6), any))
          .thenAnswer((_) => Future(() => Response.of<dynamic>(geekCommunityDetailsGraphQLResponse)));
      await tester.tap(buttonFinder);
      await tester.pumpAndSettle(const Duration(seconds: 2));
      expect(find.byType(CommunityPostEditPage), findsNothing);
      expect(find.byType(CommunityPostDetailsPage), findsOneWidget);
      expect(find.text('Why remote?'), findsNothing);
      expect(find.text('Community detail for test'), findsNothing);
      expect(find.text('Why remote?2'), findsOneWidget);
      expect(find.text('Community detail for test2'), findsOneWidget);
    });

    testWidgets('Should display confirm alert when user tapped back button after changed title or description', (tester) async {
      await setUpMobileBinding(tester);

      when(client.postWithConnection('/api/graphql', getCommunityDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', 6), any))
          .thenAnswer((_) => Future(() => Response.of<dynamic>(communityDetailsGraphQLResponse)));

      await tester.pumpWidget(MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (context) => ConnectionProvider()),
            ChangeNotifierProvider(create: (context) => ProjectProvider()),
          ],
          child: MaterialApp(
            home: Scaffold(
              key: GlobalKeys.rootAppKey,
              body: CommunityPostDetailsPage(arguments: params),
            ),
          )));

      await tester.pumpAndSettle();
      expect(find.widgetWithText(CommonAppBar, 'WFH'), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().commentPlaceholder), findsOneWidget);
      expect(SvgFinder("assets/images/comment.svg"), findsOneWidget);
      expect(SvgFinder("assets/images/operate.svg"), findsOneWidget);

      // alert if user changed anything

      await tester.tap(SvgFinder("assets/images/operate.svg"));
      await tester.pumpAndSettle();
      expect(find.widgetWithText(InkWell, 'Edit'), findsOneWidget);
      await tester.tap(find.widgetWithText(InkWell, 'Edit'));
      await tester.pumpAndSettle();
      expect(find.byType(CommunityPostEditPage), findsOneWidget);
      expect(find.byType(CommunityPostDetailsPage), findsNothing);

      await tester.enterText(find.widgetWithText(TextField, 'Why remote?'), 'New Title');
      await tester.pumpAndSettle();
      await tester.enterText(find.widgetWithText(TextField, 'Community detail for test'), 'New Description');
      await tester.pumpAndSettle();
      await tester.tap(find.byType(BackButton));
      await tester.pumpAndSettle();
      expect(find.byType(CupertinoAlertDialog), findsOneWidget);
      expect(find.text('Warning'), findsOneWidget);
      expect(find.text('Are you sure you want to leave this page? Changes you made may not be saved.'), findsOneWidget);
      expect(find.widgetWithText(CupertinoDialogAction, 'Cancel'), findsOneWidget);
      expect(find.widgetWithText(CupertinoDialogAction, 'Confirm'), findsOneWidget);

      // cancel
      await tester.tap(find.widgetWithText(CupertinoDialogAction, 'Cancel'));
      await tester.pumpAndSettle();
      expect(find.byType(CupertinoAlertDialog), findsNothing);
      expect(find.byType(CommunityPostEditPage), findsOneWidget);
      expect(find.byType(CommunityPostDetailsPage), findsNothing);

      // confirm
      await tester.tap(find.byType(BackButton));
      await tester.pumpAndSettle();
      expect(find.byType(CupertinoAlertDialog), findsOneWidget);
      await tester.tap(find.widgetWithText(CupertinoDialogAction, 'Confirm'));
      await tester.pumpAndSettle();
      expect(find.byType(CupertinoAlertDialog), findsNothing);
      expect(find.byType(CommunityPostEditPage), findsNothing);
      expect(find.byType(CommunityPostDetailsPage), findsOneWidget);

      // no alert if user not changed anything

      await tester.tap(SvgFinder("assets/images/operate.svg"));
      await tester.pumpAndSettle();
      expect(find.widgetWithText(InkWell, 'Edit'), findsOneWidget);
      await tester.tap(find.widgetWithText(InkWell, 'Edit'));
      await tester.pumpAndSettle();
      expect(find.byType(CommunityPostEditPage), findsOneWidget);
      expect(find.byType(CommunityPostDetailsPage), findsNothing);
      await tester.tap(find.byType(BackButton));
      await tester.pumpAndSettle();
      expect(find.byType(CupertinoAlertDialog), findsNothing);
      expect(find.byType(CommunityPostEditPage), findsNothing);
      expect(find.byType(CommunityPostDetailsPage), findsOneWidget);
    });

    testWidgets('Should not display edit button when user has no edit permission', (tester) async {
      await setUpMobileBinding(tester);

      when(client.postWithConnection('/api/graphql', getCommunityDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', 6), any))
          .thenAnswer((_) => Future(() => Response.of<dynamic>(communityDetailsWithNoEditPermissionGraphQLResponse)));

      await tester.pumpWidget(MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (context) => ConnectionProvider()),
            ChangeNotifierProvider(create: (context) => ProjectProvider()),
          ],
          child: MaterialApp(
            home: Scaffold(
              body: CommunityPostDetailsPage(arguments: params),
            ),
          )));

      await tester.pumpAndSettle();
      expect(find.widgetWithText(CommonAppBar, 'WFH'), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().commentPlaceholder), findsOneWidget);
      expect(SvgFinder("assets/images/comment.svg"), findsOneWidget);
      expect(SvgFinder("assets/images/operate.svg"), findsNothing);
    });
  });

  group('Should test up and down votes', () {
    testWidgets('Should display thumbs up button', (tester) async {
      await setUpMobileBinding(tester);
      Map<String, dynamic> map = Map.from(communityPostVotesGraphQLResponse);
      map['data']['project']['issue']['upvotes'] = 0;
      when(client.postWithConnection('/api/graphql', getCommunityPostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', 6), any)).thenAnswer((_) => Future(() => Response.of<dynamic>(map)));
      when(client.getWithConnection("/api/v4/projects/89335/issues/6/award_emoji?per_page=100&page=1", any)).thenAnswer((_) => Future(() => Response.of([])));

      await tester.pumpWidget(MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (context) => ConnectionProvider()),
            ChangeNotifierProvider(create: (context) => ProjectProvider()),
          ],
          child: MaterialApp(
            home: Scaffold(
              key: GlobalKeys.rootAppKey,
              body: CommunityPostDetailsPage(arguments: params),
            ),
          )));

      await tester.pumpAndSettle();
      expect(find.widgetWithText(CommonAppBar, 'WFH'), findsOneWidget);

      // no up votes for default
      var thumbsUpButton = find.byWidgetPredicate((widget) => widget is AwardEmojiButton && widget.svgAssetPath == 'assets/images/thumb-up.svg');
      expect(thumbsUpButton, findsOneWidget);
      expect(find.byWidgetPredicate((widget) {
        return widget is AwardEmojiButton &&
            widget.svgAssetPath == 'assets/images/thumb-up.svg' &&
            widget.emojiColor == const Color(0xFF87878C) &&
            widget.title == null &&
            widget.backgroundColor == const Color(0xFFEAEAEA);
      }), findsOneWidget);

      // tap thumbsUpButton for up votes
      map['data']['project']['issue']['upvotes'] = 1;
      when(client.postWithConnection('/api/graphql', getCommunityPostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', 6), any)).thenAnswer((_) => Future(() => Response.of<dynamic>(map)));
      when(client.getWithConnection("/api/v4/projects/89335/issues/6/award_emoji?per_page=100&page=1", any)).thenAnswer((_) => Future(() => Response.of([
            {
              "id": 15138,
              "name": "thumbsup",
              "user": {
                "id": 9527,
                "username": "tester",
                "name": "tester",
                "state": "active",
                "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/9527/avatar1.png",
                "web_url": "https://jihulab.com/tester"
              },
              "created_at": "2023-03-16T08:58:51.589+08:00",
              "updated_at": "2023-03-16T08:58:51.589+08:00",
              "awardable_id": 360522,
              "awardable_type": "Issue",
              "url": null
            }
          ])));
      when(client.postWithConnection<dynamic>('/api/v4/projects/89335/issues/6/award_emoji?name=thumbsup', {}, any)).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
      await tester.tap(thumbsUpButton);
      await tester.pumpAndSettle();
      expect(find.byWidgetPredicate((widget) {
        return widget is AwardEmojiButton &&
            widget.svgAssetPath == 'assets/images/thumb-up.svg' &&
            widget.emojiColor == Theme.of(GlobalKeys.rootAppKey.currentContext!).primaryColor &&
            widget.title == '1' &&
            widget.backgroundColor == const Color(0xFFFFEAE0);
      }), findsOneWidget);

      // tap thumbsUpButton for remove up votes
      map['data']['project']['issue']['upvotes'] = 0;
      when(client.postWithConnection('/api/graphql', getCommunityPostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', 6), any)).thenAnswer((_) => Future(() => Response.of<dynamic>(map)));
      when(client.getWithConnection("/api/v4/projects/89335/issues/6/award_emoji?per_page=100&page=1", any)).thenAnswer((_) => Future(() => Response.of([])));
      when(client.delete<dynamic>('/api/v4/projects/89335/issues/6/award_emoji/15138', connection: anyNamed('connection'))).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
      await tester.tap(thumbsUpButton);
      await tester.pumpAndSettle();
      expect(find.byWidgetPredicate((widget) {
        return widget is AwardEmojiButton &&
            widget.svgAssetPath == 'assets/images/thumb-up.svg' &&
            widget.emojiColor == const Color(0xFF87878C) &&
            widget.title == null &&
            widget.backgroundColor == const Color(0xFFEAEAEA);
      }), findsOneWidget);
    });

    testWidgets('Should display thumbs down button', (tester) async {
      await setUpMobileBinding(tester);
      Map<String, dynamic> map = Map.from(communityPostVotesGraphQLResponse);
      map['data']['project']['issue']['downvotes'] = 0;
      when(client.postWithConnection('/api/graphql', getCommunityPostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', 6), any)).thenAnswer((_) => Future(() => Response.of<dynamic>(map)));
      when(client.getWithConnection("/api/v4/projects/89335/issues/6/award_emoji?per_page=100&page=1", any)).thenAnswer((_) => Future(() => Response.of([])));

      await tester.pumpWidget(MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (context) => ConnectionProvider()),
            ChangeNotifierProvider(create: (context) => ProjectProvider()),
          ],
          child: MaterialApp(
            home: Scaffold(
              key: GlobalKeys.rootAppKey,
              body: CommunityPostDetailsPage(arguments: params),
            ),
          )));

      await tester.pumpAndSettle();
      expect(find.widgetWithText(CommonAppBar, 'WFH'), findsOneWidget);

      // no down votes for default
      var thumbsDownButton = find.byWidgetPredicate((widget) => widget is AwardEmojiButton && widget.svgAssetPath == 'assets/images/thumb-down.svg');
      expect(thumbsDownButton, findsOneWidget);

      expect(find.byWidgetPredicate((widget) {
        return widget is AwardEmojiButton &&
            widget.svgAssetPath == 'assets/images/thumb-down.svg' &&
            widget.emojiColor == const Color(0xFF87878C) &&
            widget.title == null &&
            widget.backgroundColor == const Color(0xFFEAEAEA);
      }), findsOneWidget);

      // tap thumbsDownButton for down votes
      map['data']['project']['issue']['downvotes'] = 1;
      when(client.postWithConnection('/api/graphql', getCommunityPostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', 6), any)).thenAnswer((_) => Future(() => Response.of<dynamic>(map)));
      when(client.getWithConnection("/api/v4/projects/89335/issues/6/award_emoji?per_page=100&page=1", any)).thenAnswer((_) => Future(() => Response.of([
            {
              "id": 15138,
              "name": "thumbsdown",
              "user": {
                "id": 9527,
                "username": "tester",
                "name": "tester",
                "state": "active",
                "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/9527/avatar1.png",
                "web_url": "https://jihulab.com/tester"
              },
              "created_at": "2023-03-16T08:58:51.589+08:00",
              "updated_at": "2023-03-16T08:58:51.589+08:00",
              "awardable_id": 360522,
              "awardable_type": "Issue",
              "url": null
            }
          ])));
      when(client.postWithConnection<dynamic>('/api/v4/projects/89335/issues/6/award_emoji?name=thumbsdown', {}, any)).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
      await tester.tap(thumbsDownButton);
      await tester.pumpAndSettle();
      expect(find.byWidgetPredicate((widget) {
        return widget is AwardEmojiButton &&
            widget.svgAssetPath == 'assets/images/thumb-down.svg' &&
            widget.emojiColor == Theme.of(GlobalKeys.rootAppKey.currentContext!).primaryColor &&
            widget.title == '1' &&
            widget.backgroundColor == const Color(0xFFFFEAE0);
      }), findsOneWidget);

      // tap thumbsDownButton for remove down votes
      map['data']['project']['issue']['downvotes'] = 0;
      when(client.postWithConnection('/api/graphql', getCommunityPostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', 6), any)).thenAnswer((_) => Future(() => Response.of<dynamic>(map)));
      when(client.getWithConnection("/api/v4/projects/89335/issues/6/award_emoji?per_page=100&page=1", any)).thenAnswer((_) => Future(() => Response.of([])));
      when(client.delete<dynamic>('/api/v4/projects/89335/issues/6/award_emoji/15138', connection: anyNamed('connection'))).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
      await tester.tap(thumbsDownButton);
      await tester.pumpAndSettle();
      expect(find.byWidgetPredicate((widget) {
        return widget is AwardEmojiButton &&
            widget.svgAssetPath == 'assets/images/thumb-down.svg' &&
            widget.emojiColor == const Color(0xFF87878C) &&
            widget.title == null &&
            widget.backgroundColor == const Color(0xFFEAEAEA);
      }), findsOneWidget);
    });
  });

  tearDown(() {
    reset(client);
    ConnectionProvider().fullReset();
    ProjectProvider().fullReset();
  });
}
