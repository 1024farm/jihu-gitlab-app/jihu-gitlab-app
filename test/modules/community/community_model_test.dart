import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_project.dart';

void main() {
  test('Should get faq type name from json', () {
    var json = {"id": "/0", "title": "title", "description": "description"};
    CommunityType type = CommunityType.fromGraphqlJson(json);
    expect(type.id.id, 0);
    expect(type.description, "description");
    expect(type.name, "title");
  });
}
