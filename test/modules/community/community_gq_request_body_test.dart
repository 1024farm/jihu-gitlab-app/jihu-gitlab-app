import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_gq_request_body.dart';

void main() {
  test('Should getCommunityInfoGraphQLRequestBody has expected params', () {
    expect(getCommunityTypesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq'), {
      "query": """
       {
          project(fullPath:"ultimate-plan/jihu-gitlab-app/faq"){
            archived
            labels{
              nodes{
                id
                title
                description
              }
            }
          }
       } 
    """
    });
  });

  test('Should getCommunityPostListGraphQLRequestBody has expected params', () {
    expect(getCommunityPostListGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', ['1', '2'], 'afterCursor', 'search', 10), {
      "query": """
       {
        project(fullPath:"ultimate-plan/jihu-gitlab-app/faq"){
          issues(labelName: ["1","2"], first:10, after:"afterCursor", search: "search", state: opened){
            nodes{
              id
              iid
              title
              webUrl
              confidential
              state
              createdAt
              author{
                name
                avatarUrl
              }
              labels {
                nodes {
                  id
                  title
                  description
                  color
                  textColor
                }
              }
            }
            pageInfo{
              hasNextPage
              endCursor
            }
          }
        }
       }
      """
    });
  });

  test('Should getProjectMembersGraphQLRequestBody has expected params', () {
    expect(getProjectMembersGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', 'search', 'endCursor'), {
      "query": """
     {
        project(fullPath: "ultimate-plan/jihu-gitlab-app/faq") {
          projectMembers(search: "search", first: 50, after: "endCursor") {
            pageInfo {
              hasNextPage
              endCursor
            }
            nodes {
              id
              accessLevel {
                integerValue
                stringValue
              }
              user {
                ...UserCoreFragment
              }
            }
          }
        }
     }
     fragment UserCoreFragment on UserCore {
        id
        username
        name
        state
        avatarUrl
        webUrl
        publicEmail
      }
    """
    });
  });

  test('Should getCommunityDetailsGraphQLRequestBody has expected params', () {
    expect(getCommunityDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', 6), {
      "query": """
       {
          project(fullPath: "ultimate-plan/jihu-gitlab-app/faq") {
            id
            name
            nameWithNamespace
            path
            fullPath
            issue(iid: "6") {
              id
              iid
              projectId
              title
              description
              webUrl
              confidential
              state
              createdAt
              author {
                id
                avatarUrl
                name
                username
              }
              assignees {
                nodes {
                  id
                  avatarUrl
                  name
                  username
                }
              }
              userPermissions {
                updateIssue
              }
              labels {
                nodes {
                  id
                  title
                  description
                  color
                  textColor
                }
              }
            }
          }
       }
       """
    });
  });

  test('Should getCommunityPostVotesGraphQLRequestBody has expected params', () {
    expect(getCommunityPostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', 6), {
      "query": """
           {
              project(fullPath: "ultimate-plan/jihu-gitlab-app/faq") {
                issue(iid: "6") {
                  downvotes
                  upvotes
                }
              }
           }
       """
    });
  });
}
