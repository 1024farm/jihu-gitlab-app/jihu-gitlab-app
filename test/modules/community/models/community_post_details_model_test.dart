import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_post_details_model.dart';
import 'package:mockito/mockito.dart';

import '../../../core/net/http_request_test.mocks.dart';

void main() {
  HttpClient client = MockHttpClient();
  when(client.postWithConnection("/api/graphql", getCommunityDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/faq', 1), ConnectionProvider().communityConnection)).thenThrow(Exception());
  test('Should error when response error', () async {
    CommunityPostDetailsModel model = CommunityPostDetailsModel();
    model.config(1, 1);
    await model.getPostInfo();
    expect(model.loadState, LoadState.errorState);
  });
}
