import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar/avatar.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/modules/mr/commits/commits_view.dart';
import 'package:jihu_gitlab_app/modules/mr/models/commits.dart';

import '../../../finder/svg_finder.dart';

void main() {
  testWidgets('Should display empty view when commits is empty', (tester) async {
    await tester.pumpWidget(const MaterialApp(home: Scaffold(body: CommitsView(commits: []))));
    await tester.pumpAndSettle();

    expect(find.byType(TipsView), findsOneWidget);
    expect(SvgFinder('assets/images/no_commits.svg'), findsOneWidget);
    expect(find.text('There are no commits yet.'), findsOneWidget);
    expect(find.byType(ListView), findsNothing);
  });

  testWidgets('Should be able to display the commit list', (tester) async {
    List<Commit> items = [
      Commit.fromJson({
        "title": "title1",
        "authoredDate": "2023-03-23T13:54:25+08:00",
        "author": {"name": "ling zhang", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png"}
      }),
      Commit.fromJson({
        "title": "title2",
        "authoredDate": "2023-03-24T13:54:25+08:00",
        "author": {"name": "ling zhang", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png"}
      }),
      Commit.fromJson({
        "title": "title3",
        "authoredDate": "2023-03-27T13:54:25+08:00",
        "author": {"name": "ling zhang", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png"}
      }),
    ];

    await tester.pumpWidget(MaterialApp(home: Scaffold(body: CommitsView(commits: items))));
    await tester.pumpAndSettle();

    expect(find.byType(ListView), findsOneWidget);
    expect(find.byType(Avatar), findsNWidgets(3));
    expect(find.text("title1"), findsOneWidget);
    expect(find.text("title2"), findsOneWidget);
    expect(find.text("title3"), findsOneWidget);
  });
}
