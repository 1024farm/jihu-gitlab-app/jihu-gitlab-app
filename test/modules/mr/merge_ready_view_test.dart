import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar_and_name.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_button.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_option_view.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_ready_view.dart';
import 'package:jihu_gitlab_app/modules/mr/models/commits.dart';
import 'package:jihu_gitlab_app/modules/mr/models/merge_request.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';

import '../../core/net/http_request_test.mocks.dart';
import '../../finder/svg_finder.dart';

void main() {
  MockHttpClient client = MockHttpClient();
  HttpClient.injectInstanceForTesting(client);

  testWidgets('Should be able to display merge options with mergeRequestsFfOnlyEnabled is false and squashReadOnly is false', (tester) async {
    MergeRequest mr = mockMergeRequestWith(mergeRequestsFfOnlyEnabled: false, squashReadOnly: false);
    await tester.pumpWidget(MaterialApp(
      onGenerateRoute: onGenerateRoute,
      home: Scaffold(
        body: MergeReadyView(mergeRequest: mr, onMergeFinished: () => Future(() => {})),
      ),
      localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
    ));
    await tester.pumpAndSettle();
    expect(SvgFinder("assets/images/mergeable.svg"), findsOneWidget);
    expect(find.text("Ready to merge!"), findsOneWidget);
    expect(find.widgetWithText(MergeOptionView, "Delete source branch"), findsOneWidget);
    expect(find.widgetWithText(MergeOptionView, "Squash commits"), findsOneWidget);
    expect(find.widgetWithText(MergeOptionView, "Edit commit message"), findsOneWidget);
    expect(find.byKey(const Key("Delete source branch")), findsOneWidget);
    var finder1 = find.byKey(const Key("Delete source branch"));
    expect(finder1, findsOneWidget);
    expect(tester.firstWidget<Checkbox>(finder1).value, false);
    var finder2 = find.byKey(const Key("Squash commits"));
    expect(finder2, findsOneWidget);
    expect(tester.firstWidget<Checkbox>(finder2).value, false);
    var finder3 = find.byKey(const Key("Edit commit message"));
    expect(finder3, findsOneWidget);
    expect(tester.firstWidget<Checkbox>(finder3).value, false);
    expect(find.byKey(const Key("Merge commit message")), findsNothing);
    expect(find.byKey(const Key("Squash commit message")), findsNothing);

    await tester.tap(finder2);
    await tester.pumpAndSettle();
    expect(find.byKey(const Key("Merge commit message")), findsNothing);
    expect(find.byKey(const Key("Squash commit message")), findsNothing);
    await tester.tap(finder3);
    await tester.pumpAndSettle();
    expect(find.byKey(const Key("Merge commit message")), findsOneWidget);
    expect(find.byKey(const Key("Squash commit message")), findsOneWidget);
    expect(find.widgetWithText(TextField, "This is default merge commit message"), findsOneWidget);
    expect(find.widgetWithText(TextField, "This is default squash commit message"), findsOneWidget);

    await tester.tap(finder2);
    await tester.pumpAndSettle();
    expect(find.byKey(const Key("Merge commit message")), findsOneWidget);
    expect(find.byKey(const Key("Squash commit message")), findsNothing);

    expect(find.widgetWithText(LoadingButton, "Merge"), findsOneWidget);
    await tester.tap(find.widgetWithText(LoadingButton, "Merge"));
    verify(client.put("/api/v4/projects/0/merge_requests/28/merge", {
      "merge_commit_message": "This is default merge commit message",
      "merge_when_pipeline_succeeds": false,
      "should_remove_source_branch": false,
      "squash_commit_message": "This is default squash commit message",
      "squash": false
    })).called(1);
  });

  testWidgets('Should be able to display merge options with mergeRequestsFfOnlyEnabled is false and squashReadOnly is true', (tester) async {
    MergeRequest mr = mockMergeRequestWith(mergeRequestsFfOnlyEnabled: false, squashReadOnly: true);
    await tester.pumpWidget(MaterialApp(
      onGenerateRoute: onGenerateRoute,
      home: Scaffold(
        body: MergeReadyView(mergeRequest: mr, onMergeFinished: () => Future(() => {})),
      ),
      localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
    ));
    await tester.pumpAndSettle();
    expect(SvgFinder("assets/images/mergeable.svg"), findsOneWidget);
    expect(find.text("Ready to merge!"), findsOneWidget);
    expect(find.widgetWithText(MergeOptionView, "Delete source branch"), findsOneWidget);
    expect(find.widgetWithText(MergeOptionView, "Squash commits"), findsNothing);
    expect(find.widgetWithText(MergeOptionView, "Edit commit message"), findsOneWidget);
    var finder1 = find.byKey(const Key("Delete source branch"));
    expect(finder1, findsOneWidget);
    expect(tester.firstWidget<Checkbox>(finder1).value, false);
    var finder3 = find.byKey(const Key("Edit commit message"));
    expect(finder3, findsOneWidget);
    expect(tester.firstWidget<Checkbox>(finder3).value, false);
    expect(find.byKey(const Key("Merge commit message")), findsNothing);
    expect(find.byKey(const Key("Squash commit message")), findsNothing);

    await tester.tap(finder3);
    await tester.pumpAndSettle();
    expect(find.byKey(const Key("Merge commit message")), findsOneWidget);
    expect(find.widgetWithText(TextField, "This is default merge commit message"), findsOneWidget);
  });

  testWidgets('Should be able to display merge options with mergeRequestsFfOnlyEnabled is true and squashReadOnly is true', (tester) async {
    MergeRequest mr = mockMergeRequestWith(mergeRequestsFfOnlyEnabled: true, squashReadOnly: true, forceRemoveSourceBranch: true);
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: MergeReadyView(mergeRequest: mr, onMergeFinished: () => Future(() => {})),
      ),
      localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
    ));
    await tester.pumpAndSettle();
    expect(SvgFinder("assets/images/mergeable.svg"), findsOneWidget);
    expect(find.text("Ready to merge!"), findsOneWidget);
    expect(find.widgetWithText(MergeOptionView, "Delete source branch"), findsOneWidget);
    expect(find.widgetWithText(MergeOptionView, "Squash commits"), findsNothing);
    expect(find.widgetWithText(MergeOptionView, "Edit commit message"), findsNothing);
    var finder1 = find.byKey(const Key("Delete source branch"));
    expect(finder1, findsOneWidget);
    expect(tester.firstWidget<Checkbox>(finder1).value, true);

    await tester.tap(finder1);
    await tester.pumpAndSettle();
    expect(tester.firstWidget<Checkbox>(find.byKey(const Key("Delete source branch"))).value, false);
  });

  testWidgets('Should be able to display merge options with mergeRequestsFfOnlyEnabled is true and squashReadOnly is false', (tester) async {
    MergeRequest mr = mockMergeRequestWith(mergeRequestsFfOnlyEnabled: true, squashReadOnly: false, commits: duplicatesCommits);
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: MergeReadyView(mergeRequest: mr, onMergeFinished: () => Future(() => {})),
      ),
      localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
    ));
    await tester.pumpAndSettle();
    expect(SvgFinder("assets/images/mergeable.svg"), findsOneWidget);
    expect(find.text("Ready to merge!"), findsOneWidget);
    expect(find.widgetWithText(MergeOptionView, "Delete source branch"), findsOneWidget);
    expect(find.widgetWithText(MergeOptionView, "Squash commits"), findsOneWidget);
    expect(find.widgetWithText(MergeOptionView, "Edit commit message"), findsOneWidget);
    var finder1 = find.byKey(const Key("Delete source branch"));
    expect(finder1, findsOneWidget);
    expect(tester.firstWidget<Checkbox>(finder1).value, false);
    var finder2 = find.byKey(const Key("Squash commits"));
    expect(finder2, findsOneWidget);
    expect(tester.firstWidget<Checkbox>(finder2).value, true);
    var finder3 = find.byKey(const Key("Edit commit message"));
    expect(finder3, findsOneWidget);
    expect(tester.firstWidget<Checkbox>(finder3).value, false);
    expect(find.byKey(const Key("Merge commit message")), findsNothing);
    expect(find.byKey(const Key("Squash commit message")), findsNothing);

    await tester.tap(finder2);
    await tester.pumpAndSettle();
    expect(tester.firstWidget<Checkbox>(find.byKey(const Key("Squash commits"))).value, false);
    expect(find.widgetWithText(MergeOptionView, "Edit commit message"), findsNothing);
    expect(find.byKey(const Key("Merge commit message")), findsNothing);
    expect(find.byKey(const Key("Squash commit message")), findsNothing);

    await tester.tap(finder2);
    await tester.pumpAndSettle();
    expect(tester.firstWidget<Checkbox>(find.byKey(const Key("Squash commits"))).value, true);
    expect(find.widgetWithText(MergeOptionView, "Edit commit message"), findsOneWidget);
    await tester.tap(finder3);
    await tester.pumpAndSettle();
    expect(find.byKey(const Key("Merge commit message")), findsNothing);
    expect(find.byKey(const Key("Squash commit message")), findsOneWidget);
    expect(find.widgetWithText(TextField, "This is default squash commit message"), findsOneWidget);

    await tester.tap(finder2);
    await tester.pumpAndSettle();
    expect(tester.firstWidget<Checkbox>(find.byKey(const Key("Squash commits"))).value, false);
    expect(find.widgetWithText(MergeOptionView, "Edit commit message"), findsNothing);
    expect(find.byKey(const Key("Merge commit message")), findsNothing);
    expect(find.byKey(const Key("Squash commit message")), findsNothing);

    await tester.tap(finder2);
    await tester.pumpAndSettle();
    expect(tester.firstWidget<Checkbox>(find.byKey(const Key("Squash commits"))).value, true);
    expect(tester.firstWidget<Checkbox>(find.byKey(const Key("Edit commit message"))).value, true);
    expect(find.widgetWithText(MergeOptionView, "Edit commit message"), findsOneWidget);
    expect(find.byKey(const Key("Merge commit message")), findsNothing);
    expect(find.byKey(const Key("Squash commit message")), findsOneWidget);
    expect(find.widgetWithText(TextField, "This is default squash commit message"), findsOneWidget);
  });

  testWidgets('Should be able to display merge options with squashOnMerge is true and squashReadOnly is true', (tester) async {
    MergeRequest mr = mockMergeRequestWith(squashReadOnly: true, squashOnMerge: true);
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: MergeReadyView(mergeRequest: mr, onMergeFinished: () => Future(() => {})),
      ),
      localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
    ));
    await tester.pumpAndSettle();
    expect(SvgFinder("assets/images/mergeable.svg"), findsOneWidget);
    expect(find.text("Ready to merge!"), findsOneWidget);
    expect(find.widgetWithText(MergeOptionView, "Delete source branch"), findsOneWidget);
    expect(find.widgetWithText(MergeOptionView, "Squash commits"), findsOneWidget);
    expect(find.widgetWithText(MergeOptionView, "Edit commit message"), findsOneWidget);
    var finder1 = find.byKey(const Key("Delete source branch"));
    expect(finder1, findsOneWidget);
    expect(tester.firstWidget<Checkbox>(finder1).value, false);
    var finder2 = find.byKey(const Key("Squash commits"));
    expect(finder2, findsOneWidget);
    expect(tester.firstWidget<Checkbox>(finder2).value, true);
    expect(tester.firstWidget<Checkbox>(finder2).onChanged, null);

    var finder3 = find.byKey(const Key("Edit commit message"));
    expect(finder3, findsOneWidget);
    expect(tester.firstWidget<Checkbox>(finder3).value, false);
    expect(find.byKey(const Key("Merge commit message")), findsNothing);
    expect(find.byKey(const Key("Squash commit message")), findsNothing);
  });

  group('Squash commits', () {
    testWidgets('Should not display Squash commits when squashReadOnly is true and squashOnMerge is false', (tester) async {
      MergeRequest mr = mockMergeRequestWith(squashReadOnly: true, squashOnMerge: false);
      await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: MergeReadyView(mergeRequest: mr, onMergeFinished: () => Future(() => {})),
        ),
      ));
      await tester.pumpAndSettle();
      expect(find.widgetWithText(MergeOptionView, "Squash commits"), findsNothing);
    });

    testWidgets('Should display Squash commits with checked but disabled for change when squashReadOnly is true and squashOnMerge is true', (tester) async {
      MergeRequest mr = mockMergeRequestWith(squashReadOnly: true, squashOnMerge: true);
      await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: MergeReadyView(mergeRequest: mr, onMergeFinished: () => Future(() => {})),
        ),
      ));
      await tester.pumpAndSettle();
      expect(find.widgetWithText(MergeOptionView, "Squash commits"), findsOneWidget);

      var squashCheckbox = find.byKey(const Key("Squash commits"));
      expect(squashCheckbox, findsOneWidget);
      expect(tester.firstWidget<Checkbox>(squashCheckbox).value, true);
      expect(tester.firstWidget<Checkbox>(squashCheckbox).onChanged, null);
    });

    group('Should display Squash commits checkbox and able to change', () {
      var params = [
        {"checkDescription": 'unchecked', "squashReadOnly": false, "squashOnMerge": false, "commits": hasNoDuplicatesCommits, "checked": false},
        {"checkDescription": 'checked', "squashReadOnly": false, "squashOnMerge": false, "commits": duplicatesCommits, "checked": true},
        {"checkDescription": 'unchecked', "squashReadOnly": false, "squashOnMerge": true, "commits": hasNoDuplicatesCommits, "checked": false},
        {"checkDescription": 'checked', "squashReadOnly": false, "squashOnMerge": true, "commits": duplicatesCommits, "checked": true},
      ];

      for (var element in params) {
        var checkDescription = element['checkDescription'] as String;
        var squashReadOnly = element['squashReadOnly'] as bool;
        var squashOnMerge = element['squashOnMerge'] as bool;
        var commits = element['commits'] as List<Commit>;
        var checked = element['checked'] as bool;
        testWidgets('Should Squash commits checkbox with $checkDescription when squashReadOnly is $squashReadOnly and squashOnMerge is $squashOnMerge', (tester) async {
          MergeRequest mr = mockMergeRequestWith(squashReadOnly: squashReadOnly, squashOnMerge: squashOnMerge, commits: commits);
          await tester.pumpWidget(MaterialApp(
            home: Scaffold(
              body: MergeReadyView(mergeRequest: mr, onMergeFinished: () => Future(() => {})),
            ),
          ));
          await tester.pumpAndSettle();
          expect(find.widgetWithText(MergeOptionView, "Squash commits"), findsOneWidget);
          var squashCheckbox = find.byKey(const Key("Squash commits"));
          expect(squashCheckbox, findsOneWidget);
          expect(tester.firstWidget<Checkbox>(squashCheckbox).value, checked);
          var aiRecommendsMessage = 'AI detects duplicate commit messages, recommends squashing.';
          var aiRecommendsMessageNot = 'AI detects that commit messages are not duplicated and recommends not to squash.';
          if (checked) {
            expect(find.text(aiRecommendsMessage), findsOneWidget);
          } else {
            expect(find.text(aiRecommendsMessageNot), findsOneWidget);
          }

          await tester.tap(squashCheckbox);
          await tester.pumpAndSettle();
          squashCheckbox = find.byKey(const Key("Squash commits"));
          expect(tester.firstWidget<Checkbox>(squashCheckbox).value, !checked);
          if (checked) {
            expect(find.text(aiRecommendsMessage), findsOneWidget);
          } else {
            expect(find.text(aiRecommendsMessageNot), findsOneWidget);
          }
        });
      }
    });
  });

  testWidgets('Should be able to loop the result of the merge after click the merge button', (tester) async {
    when(client.put("/api/v4/projects/0/merge_requests/28/merge", argThat(predicate((arg) => arg != null)))).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>({"iid": 28})));
    MergeRequest mr = mockMergeRequestWith(mergeRequestsFfOnlyEnabled: true, squashReadOnly: false, squashOnMerge: true);
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: MergeReadyView(mergeRequest: mr, onMergeFinished: () => Future(() => {})),
      ),
      localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
    ));
    await tester.pumpAndSettle();
    expect(SvgFinder("assets/images/mergeable.svg"), findsOneWidget);
    expect(find.text("Ready to merge!"), findsOneWidget);

    when(client.post("/api/graphql", argThat(predicate((arg) => arg.toString().contains("28"))))).thenAnswer((_) => Future(() => Response.of({
          "data": {
            "project": {
              "mergeRequest": {"mergeOngoing": true}
            }
          }
        })));
    expect(find.widgetWithText(LoadingButton, "Merge"), findsOneWidget);
    await tester.tap(find.widgetWithText(LoadingButton, "Merge"));
    await tester.pump();
    expect(find.byType(CircularProgressIndicator), findsNWidgets(2));
    expect(find.text("Merger in progress! Sending changes..."), findsOneWidget);

    when(client.post("/api/graphql", argThat(predicate((arg) => arg.toString().contains("28"))))).thenAnswer((_) => Future(() => Response.of({
          "data": {
            "project": {
              "mergeRequest": {"mergeOngoing": false}
            }
          }
        })));
    await tester.pump(const Duration(seconds: 3));
  });

  testWidgets('Should be able to display the looping status when merge is in progress', (tester) async {
    when(client.put("/api/v4/projects/0/merge_requests/28/merge", argThat(predicate((arg) => arg != null)))).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>({"iid": 28})));
    MergeRequest mr = mockMergeRequestWith(mergeRequestsFfOnlyEnabled: true, squashReadOnly: false, squashOnMerge: true, mergeOngoing: true);
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: MergeReadyView(mergeRequest: mr, onMergeFinished: () => Future(() => {})),
      ),
      localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
    ));
    await tester.pump();
    expect(SvgFinder("assets/images/mergeable.svg"), findsNothing);
    expect(find.text("Ready to merge!"), findsNothing);
    expect(find.widgetWithText(LoadingButton, "Merge"), findsOneWidget);
    LoadingButton loadingButton = tester.firstWidget<LoadingButton>(find.widgetWithText(LoadingButton, "Merge"));
    expect(loadingButton.disabled, true);
    expect(find.byType(CircularProgressIndicator), findsNWidgets(1));
    expect(find.text("Merger in progress! Sending changes..."), findsOneWidget);
  });

  testWidgets('Should be able to display the automatically merge button', (tester) async {
    when(client.put("/api/v4/projects/0/merge_requests/28/merge", argThat(predicate((arg) => arg != null)))).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>({"iid": 28})));
    MergeRequest mr = mockMergeRequestWith(availableAutoMergeStrategies: ["merge_when_pipeline_succeeds"]);
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: MergeReadyView(mergeRequest: mr, onMergeFinished: () => Future(() => {})),
      ),
      localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
    ));
    await tester.pumpAndSettle();
    expect(SvgFinder("assets/images/mergeable.svg"), findsOneWidget);
    expect(find.text("Ready to merge!"), findsOneWidget);
    var finder = find.widgetWithText(LoadingButton, "Merge when pipeline succeeds");
    expect(finder, findsOneWidget);
    LoadingButton loadingButton = tester.firstWidget<LoadingButton>(finder);
    expect(loadingButton.disabled, false);
    await tester.tap(finder);
    verify(client.put("/api/v4/projects/0/merge_requests/28/merge", {
      "merge_commit_message": "This is default merge commit message",
      "merge_when_pipeline_succeeds": true,
      "should_remove_source_branch": false,
      "squash_commit_message": "This is default squash commit message",
      "squash": false
    })).called(1);
    await tester.pumpAndSettle();
  });

  testWidgets('Should be able to display the merge user after set merge automatically', (tester) async {
    when(client.put("/api/v4/projects/0/merge_requests/28/merge", argThat(predicate((arg) => arg != null)))).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>({"iid": 28})));
    MergeRequest mr = mockMergeRequestWith(availableAutoMergeStrategies: ["merge_when_pipeline_succeeds"], mergeWhenPipelineSucceeds: true);
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: MergeReadyView(mergeRequest: mr, onMergeFinished: () => Future(() => {})),
      ),
      localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
    ));
    await tester.pumpAndSettle();
    expect(SvgFinder("assets/images/auto_merge.svg"), findsOneWidget);
    expect(find.byType(AvatarAndName), findsOneWidget);
    expect(find.byType(RichText), findsNWidgets(2));
  });

  tearDown(() => reset(client));
}

MergeRequest mockMergeRequestWith(
    {bool mergeRequestsFfOnlyEnabled = false,
    bool squashReadOnly = false,
    bool squashOnMerge = false,
    bool forceRemoveSourceBranch = false,
    bool mergeOngoing = false,
    List<String> availableAutoMergeStrategies = const <String>[],
    bool mergeWhenPipelineSucceeds = false,
    List<Commit>? commits}) {
  var mr = MergeRequest.fromJson({
    "mergeRequestsFfOnlyEnabled": mergeRequestsFfOnlyEnabled,
    "squashReadOnly": squashReadOnly,
    "fullPath": "demo",
    "mergeRequest": {
      "mergeable": true,
      "iid": "28",
      "defaultMergeCommitMessage": "This is default merge commit message",
      "defaultSquashCommitMessage": "This is default squash commit message",
      "squashOnMerge": squashOnMerge,
      "mergeOngoing": mergeOngoing,
      "forceRemoveSourceBranch": forceRemoveSourceBranch,
      "commits": {"nodes": []},
      "availableAutoMergeStrategies": availableAutoMergeStrategies,
      "mergeWhenPipelineSucceeds": mergeWhenPipelineSucceeds
    }
  });
  mr.updateCommits(commits ?? hasNoDuplicatesCommits);
  return mr;
}

final List<Commit> hasNoDuplicatesCommits = [
  Commit.fromJson({
    "title": "title1",
    "authoredDate": "2023-03-23T13:54:25+08:00",
    "author": {"name": "ling zhang", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png"}
  }),
  Commit.fromJson({
    "title": "title2",
    "authoredDate": "2023-03-24T13:54:25+08:00",
    "author": {"name": "ling zhang", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png"}
  }),
  Commit.fromJson({
    "title": "title3",
    "authoredDate": "2023-03-27T13:54:25+08:00",
    "author": {"name": "ling zhang", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png"}
  }),
];

final List<Commit> duplicatesCommits = [
  Commit.fromJson({
    "title": "title1",
    "authoredDate": "2023-03-23T13:54:25+08:00",
    "author": {"name": "ling zhang", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png"}
  }),
  Commit.fromJson({
    "title": "title2",
    "authoredDate": "2023-03-24T13:54:25+08:00",
    "author": {"name": "ling zhang", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png"}
  }),
  Commit.fromJson({
    "title": "title2",
    "authoredDate": "2023-03-25T14:54:25+08:00",
    "author": {"name": "ling zhang", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png"}
  }),
  Commit.fromJson({
    "title": "title3",
    "authoredDate": "2023-03-27T13:54:25+08:00",
    "author": {"name": "ling zhang", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png"}
  }),
];
