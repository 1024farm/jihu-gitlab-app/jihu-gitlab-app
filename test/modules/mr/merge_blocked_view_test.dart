import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_blocked_view.dart';
import 'package:jihu_gitlab_app/modules/mr/models/merge_request.dart';
import 'package:jihu_gitlab_app/routers.dart';

import '../../finder/svg_finder.dart';

void main() {
  test("Should be able to get the description of the detail merge status", () {
    expect(DetailMergeStatus.blockedStatus.description, "Blocked status");
    expect(DetailMergeStatus.brokenStatus.description, "Broken status");
    expect(DetailMergeStatus.checking.description, "checking");
    expect(DetailMergeStatus.ciStillRunning.description, "CI still running");
    expect(DetailMergeStatus.notOpen.description, "Not open");
    expect(DetailMergeStatus.policiesDenied.description, "Policies denied");
    expect(DetailMergeStatus.unchecked.description, "unchecked");
  });

  testWidgets('Should be able to display merge blocked with status of EXTERNAL_STATUS_CHECKS', (tester) async {
    MergeRequest mr = mockMergeRequestWith(detailMergeStatus: DetailMergeStatus.externalStatusChecks);
    await tester.pumpWidget(MaterialApp(
      onGenerateRoute: onGenerateRoute,
      home: Scaffold(
        body: MergeBlockedView(mergeRequest: mr, onMarkedAsReady: () => Future(() => {}), onRebaseFinished: () => Future(() => {})),
      ),
      localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
    ));
    await tester.pumpAndSettle();
    expect(SvgFinder("assets/images/merge_blocked.svg"), findsOneWidget);
    expect(find.text("Merge blocked"), findsOneWidget);
    expect(find.text("All status checks must pass."), findsOneWidget);
  });

  testWidgets('Should be able to display merge blocked with status of NOT_APPROVED', (tester) async {
    MergeRequest mr = mockMergeRequestWith(detailMergeStatus: DetailMergeStatus.notApproved);
    await tester.pumpWidget(MaterialApp(
        onGenerateRoute: onGenerateRoute,
        home: Scaffold(
          body: MergeBlockedView(mergeRequest: mr, onMarkedAsReady: () => Future(() => {}), onRebaseFinished: () => Future(() => {})),
        ),
        localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate]));
    await tester.pumpAndSettle();
    expect(SvgFinder("assets/images/merge_blocked.svg"), findsOneWidget);
    expect(find.text("Merge blocked"), findsOneWidget);
    expect(find.text("All required approvals must be given."), findsOneWidget);
  });

  testWidgets('Should be able to display merge blocked with status of CI_MUST_PASS', (tester) async {
    MergeRequest mr = mockMergeRequestWith(detailMergeStatus: DetailMergeStatus.ciMustPass);
    await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: MergeBlockedView(mergeRequest: mr, onMarkedAsReady: () => Future(() => {}), onRebaseFinished: () => Future(() => {})),
        ),
        localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate]));
    await tester.pumpAndSettle();
    expect(SvgFinder("assets/images/merge_blocked.svg"), findsOneWidget);
    expect(find.text("Merge blocked"), findsOneWidget);
    expect(find.text("Pipeline must succeed. Push a commit that fixes the failure. "), findsOneWidget);
  });

  testWidgets('Should be able to display merge blocked and remind to resolve all conflicts', (tester) async {
    MergeRequest mr = mockMergeRequestWith(detailMergeStatus: DetailMergeStatus.brokenStatus, conflicts: true, shouldBeRebased: false);
    await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: MergeBlockedView(mergeRequest: mr, onMarkedAsReady: () => Future(() => {}), onRebaseFinished: () => Future(() => {})),
        ),
        localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate]));
    await tester.pumpAndSettle();
    expect(SvgFinder("assets/images/merge_blocked.svg"), findsOneWidget);
    expect(find.text("Merge blocked"), findsOneWidget);
    expect(find.text("Merge conflicts must be resolved."), findsOneWidget);
  });

  testWidgets('Should be able to display merge blocked and remind to rebase locally with BROKEN_STATUS status', (tester) async {
    MergeRequest mr = mockMergeRequestWith(detailMergeStatus: DetailMergeStatus.brokenStatus, conflicts: true, shouldBeRebased: true);
    await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: MergeBlockedView(mergeRequest: mr, onMarkedAsReady: () => Future(() => {}), onRebaseFinished: () => Future(() => {})),
        ),
        localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate]));
    await tester.pumpAndSettle();
    expect(SvgFinder("assets/images/merge_blocked.svg"), findsOneWidget);
    expect(find.text("Merge blocked"), findsOneWidget);
    expect(find.text("Fast-forward merge is not possible. To merge this request, first rebase locally."), findsOneWidget);
  });

  testWidgets('Should be able to display merge blocked and remind to rebase locally with NOT_APPROVED status', (tester) async {
    MergeRequest mr = mockMergeRequestWith(detailMergeStatus: DetailMergeStatus.notApproved, conflicts: true, shouldBeRebased: true);
    await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: MergeBlockedView(mergeRequest: mr, onMarkedAsReady: () => Future(() => {}), onRebaseFinished: () => Future(() => {})),
        ),
        localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate]));
    await tester.pumpAndSettle();
    expect(SvgFinder("assets/images/merge_blocked.svg"), findsOneWidget);
    expect(find.text("Merge blocked"), findsOneWidget);
    expect(find.text("Fast-forward merge is not possible. To merge this request, first rebase locally."), findsOneWidget);
  });

  testWidgets('Should be able to display merge blocked and remind to rebase online with NOT_APPROVED status', (tester) async {
    MergeRequest mr = mockMergeRequestWith(detailMergeStatus: DetailMergeStatus.notApproved, conflicts: false, shouldBeRebased: true);
    await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: MergeBlockedView(mergeRequest: mr, onMarkedAsReady: () => Future(() => {}), onRebaseFinished: () => Future(() => {})),
        ),
        localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate]));
    await tester.pumpAndSettle();
    expect(SvgFinder("assets/images/mergeable.svg"), findsOneWidget);
    expect(find.text("Merge blocked"), findsOneWidget);
    expect(find.text("The source branch must be rebased onto the target branch."), findsOneWidget);
  });

  testWidgets('Should be able to display merge blocked and remind to rebase online with NOT_APPROVED status', (tester) async {
    MergeRequest mr = mockMergeRequestWith(detailMergeStatus: DetailMergeStatus.discussionsNotResolved, conflicts: false, shouldBeRebased: false);
    await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: MergeBlockedView(mergeRequest: mr, onMarkedAsReady: () => Future(() => {}), onRebaseFinished: () => Future(() => {})),
        ),
        localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate]));
    await tester.pumpAndSettle();
    expect(SvgFinder("assets/images/merge_blocked.svg"), findsOneWidget);
    expect(find.text("Merge blocked"), findsOneWidget);
    expect(find.text("All threads must be resolved."), findsOneWidget);
  });

  testWidgets('Should be able to display merge blocked and remind to rebase online with MERGEABLE status', (tester) async {
    MergeRequest mr = mockMergeRequestWith(detailMergeStatus: DetailMergeStatus.mergeable, conflicts: false, shouldBeRebased: true);
    await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: MergeBlockedView(mergeRequest: mr, onMarkedAsReady: () => Future(() => {}), onRebaseFinished: () => Future(() => {})),
        ),
        localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate]));
    await tester.pumpAndSettle();
    expect(SvgFinder("assets/images/mergeable.svg"), findsOneWidget);
    expect(find.text("Merge blocked"), findsOneWidget);
    expect(find.text("The source branch must be rebased onto the target branch."), findsOneWidget);
  });
}

MergeRequest mockMergeRequestWith({required DetailMergeStatus detailMergeStatus, bool conflicts = false, bool shouldBeRebased = false}) {
  var json = {
    "mergeRequest": {
      "shouldBeRebased": shouldBeRebased,
      "conflicts": conflicts,
      "mergeable": false,
      "detailedMergeStatus": detailMergeStatus.label,
      "fullPath": "demo",
      "iid": "28",
      "commits": {"nodes": []}
    }
  };
  var mr = MergeRequest.fromJson(json);
  mr.paidDataFromJson(json);
  return mr;
}
