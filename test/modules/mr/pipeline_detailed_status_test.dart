import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/mr/models/pipeline_detailed_status.dart';

void main() {
  var params = ['passed with warnings', 'running', 'canceled', 'failed', 'passed', ''];
  for (var value in params) {
    test('Should PipelineDetailedStatus convert $value to expect language output', () {
      expect(PipelineDetailedStatus.fromJson({'label': value}).translatedLabel, value);
    });
  }
}
