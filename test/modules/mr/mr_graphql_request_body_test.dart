import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/mr/models/mr_graphql_request_body.dart';

void main() {
  test('Should getMergeRequestDetailsGraphQLRequestBody has expected params', () {
    expect(getMergeRequestDetailsGraphQLRequestBody('fullPath', 371), {
      "operationName": "getMergeRequest",
      "variables": {"fullPath": "fullPath", "mrIid": '371'},
      "query": """
        query getMergeRequest(\$fullPath: ID!, \$mrIid: String!) {
          project(fullPath: \$fullPath) {
            id
            name
            nameWithNamespace
            path
            fullPath
            webUrl
            allowMergeOnSkippedPipeline
            onlyAllowMergeIfAllDiscussionsAreResolved
            onlyAllowMergeIfPipelineSucceeds
            removeSourceBranchAfterMerge
            mergeRequestsFfOnlyEnabled
            squashReadOnly
            mergeRequest(iid: \$mrIid) {
              id
              iid
              state
              title
              description
              webUrl
              draft
              createdAt
              rebaseInProgress
              allowCollaboration
              approvedBy {
                nodes {
                  ...UserCoreFragment
                }
              }
              assignees {
                nodes {
                  id
                  name
                }
              }
              commitCount
              commits {
                nodes {
                  id
                  shortId
                  sha
                  title
                  description
                }
              }
              author {
                id
                name
                username
                avatarUrl
              }
              mergeUser {
                id
                name
                username
                avatarUrl
              }
              mergedAt
              autoMergeEnabled
              autoMergeStrategy
              availableAutoMergeStrategies
              conflicts
              mergeError
              mergeOngoing
              mergeStatusEnum
              mergeUser {
                ...UserCoreFragment
              }
              mergeWhenPipelineSucceeds
              mergeable
              mergeableDiscussionsState
              mergedAt
              rebaseCommitSha
              rebaseInProgress
              shouldBeRebased
              shouldRemoveSourceBranch
              sourceBranch
              sourceBranchExists
              sourceBranchProtected
              targetBranch
              targetBranchExists
              squash
              squashOnMerge
              forceRemoveSourceBranch
              defaultMergeCommitMessage
              defaultSquashCommitMessage
              headPipeline {
                id
                iid
                active
                cancelable
                complete
                coverage
                status
                ref
                detailedStatus {
                  icon
                  label
                }
                jobs {
                  nodes {
                    id
                    name
                    active
                    status
                    allowFailure
                    duration
                    startedAt
                    finishedAt
                    stage {
                      id
                      name
                      status
                      jobs {
                        nodes {
                          id
                          name
                          status
                          allowFailure
                        }
                      }
                    }
                  }
                }
                warnings
                warningMessages {
                  content
                }
                createdAt
                finishedAt
                committedAt
                startedAt
              }
              userPermissions {
                adminMergeRequest
                canMerge
                pushToSourceBranch
                readMergeRequest
                removeSourceBranch
              }
            }
          }
        }
        
        fragment UserCoreFragment on UserCore {
          id
          username
          name
          state
          avatarUrl
          webUrl
          publicEmail
        }
    """
    });
  });

  test('Should getPaidMergeRequestDetailsGraphQLRequestBody has expected params', () {
    expect(getPaidMergeRequestDetailsGraphQLRequestBody('fullPath', 371), {
      "query": """
       {
          project(fullPath: "fullPath") {
            id
            onlyAllowMergeIfAllStatusChecksPassed
            mergeRequest(iid: "371") {
              id
              iid
              approved
              approvalsLeft
              approvalsRequired
              approvalState {
                approvalRulesOverwritten
                rules {
                  id
                  name
                  type
                  approvalsRequired
                  approvedBy {
                    nodes {
                      ...UserCoreFragment
                    }
                  }
                  eligibleApprovers {
                    ...UserCoreFragment
                  }
                }
              }
              detailedMergeStatus
              headPipeline {
                id
                iid
                codeQualityReportSummary {
                  count
                }
                codeQualityReports {
                  nodes {
                    line
                    path
                    description
                    severity
                  }
                }
              }
            }
          }
       }
       fragment UserCoreFragment on UserCore {
          id
          username
          name
          state
          avatarUrl
          webUrl
          publicEmail
       }
       """
    });
  });

  test('Should queryRebaseInProgressGraphQLRequestBody has expected params', () {
    expect(queryRebaseInProgressGraphQLRequestBody('fullPath', 371), {
      "query": """
      {
        project(fullPath: "fullPath") {
          mergeRequest(iid: "371") {
            rebaseInProgress
          }
        }
      }
    """
    });
  });

  test('Should queryCommitsGraphQLRequestBody has expected params', () {
    expect(queryCommitsGraphQLRequestBody('fullPath', 371, 'cursor'), {
      "query": """
      query getCommits{
        project(fullPath: "fullPath") {
          mergeRequest(iid: "371") {
            commits(after: "cursor"){
              pageInfo{
                hasNextPage
                endCursor
              }
              nodes{
                id
                shortId
                sha
                title
                description
                authoredDate
                author{
                  name
                  avatarUrl
                }
              }
            }
          }
        }
      }
    """
    });
  });

  test('Should queryJobsGraphQLRequestBody has expected params', () {
    expect(queryJobsGraphQLRequestBody('fullPath', 371, 'cursor'), {
      "query": """
      query getCommits{
        project(fullPath: "fullPath") {
          mergeRequest(iid: "371") {
            headPipeline {
              jobs(first: 100, after: "cursor") {
                pageInfo {
                  hasNextPage
                  endCursor
                }
                nodes {
                  id
                  name
                  active
                  status
                  allowFailure
                  duration
                  startedAt
                  finishedAt
                  stage {
                    id
                    name
                    status
                    jobs {
                      nodes {
                        id
                        name
                        status
                        allowFailure
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    """
    });
  });
}
