import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/mr/changes/changed_file.dart';
import 'package:jihu_gitlab_app/modules/mr/models/diff.dart';

void main() {
  test('Should create ChangedFile object with given diff data', () {
    Diff diff = Diff.fromJson({
      "diff":
          "@@ -15,13 +15,15 @@ PODS:\n++Abc\n--asd     - Flutter\n   - package_info_plus (0.4.5):\n     - Flutter\n-  - path_provider_ios (0.0.1):\n+  - path_provider_foundation (0.0.1):\n     - Flutter\n+    - FlutterMacOS\n   - permission_handler_apple (9.0.4):\n     - Flutter\n   - ReachabilitySwift (5.0.0)\n-  - shared_preferences_ios (0.0.1):\n+  - shared_preferences_foundation (0.0.1):\n     - Flutter\n+    - FlutterMacOS\n   - sqflite (0.0.2):\n     - Flutter\n     - FMDB (>= 2.7.5)\n@@ -44,9 +46,9 @@ DEPENDENCIES:\n   - image_picker_ios (from `.symlinks/plugins/image_picker_ios/ios`)\n   - open_filex (from `.symlinks/plugins/open_filex/ios`)\n   - package_info_plus (from `.symlinks/plugins/package_info_plus/ios`)\n-  - path_provider_ios (from `.symlinks/plugins/path_provider_ios/ios`)\n+  - path_provider_foundation (from `.symlinks/plugins/path_provider_foundation/ios`)\n   - permission_handler_apple (from `.symlinks/plugins/permission_handler_apple/ios`)\n-  - shared_preferences_ios (from `.symlinks/plugins/shared_preferences_ios/ios`)\n+  - shared_preferences_foundation (from `.symlinks/plugins/shared_preferences_foundation/ios`)\n   - sqflite (from `.symlinks/plugins/sqflite/ios`)\n   - updater (from `.symlinks/plugins/updater/ios`)\n   - url_launcher_ios (from `.symlinks/plugins/url_launcher_ios/ios`)\n@@ -73,12 +75,12 @@ EXTERNAL SOURCES:\n     :path: \".symlinks/plugins/open_filex/ios\"\n   package_info_plus:\n     :path: \".symlinks/plugins/package_info_plus/ios\"\n-  path_provider_ios:\n-    :path: \".symlinks/plugins/path_provider_ios/ios\"\n+  path_provider_foundation:\n+    :path: \".symlinks/plugins/path_provider_foundation/ios\"\n   permission_handler_apple:\n     :path: \".symlinks/plugins/permission_handler_apple/ios\"\n-  shared_preferences_ios:\n-    :path: \".symlinks/plugins/shared_preferences_ios/ios\"\n+  shared_preferences_foundation:\n+    :path: \".symlinks/plugins/shared_preferences_foundation/ios\"\n   sqflite:\n     :path: \".symlinks/plugins/sqflite/ios\"\n   updater:\n@@ -100,14 +102,14 @@ SPEC CHECKSUMS:\n   image_picker_ios: b786a5dcf033a8336a657191401bfdf12017dabb\n   open_filex: 6e26e659846ec990262224a12ef1c528bb4edbe4\n   package_info_plus: 6c92f08e1f853dc01228d6f553146438dafcd14e\n-  path_provider_ios: 14f3d2fd28c4fdb42f44e0f751d12861c43cee02\n+  path_provider_foundation: 37748e03f12783f9de2cb2c4eadfaa25fe6d4852\n   permission_handler_apple: 44366e37eaf29454a1e7b1b7d736c2cceaeb17ce\n   ReachabilitySwift: 985039c6f7b23a1da463388634119492ff86c825\n-  shared_preferences_ios: 548a61f8053b9b8a49ac19c1ffbc8b92c50d68ad\n+  shared_preferences_foundation: 297b3ebca31b34ec92be11acd7fb0ba932c822ca\n   sqflite: 6d358c025f5b867b29ed92fc697fd34924e11904\n   Toast: 91b396c56ee72a5790816f40d3a94dd357abc196\n   updater: d6c70e66a13a3704329f41f5653dd6e503753fa5\n-  url_launcher_ios: 839c58cdb4279282219f5e248c3321761ff3c4de\n+  url_launcher_ios: fb12c43172927bb5cf75aeebd073f883801f1993\n   video_player_avfoundation: e489aac24ef5cf7af82702979ed16f2a5ef84cff\n   wakelock: d0fc7c864128eac40eba1617cb5264d9c940b46f\n   webview_flutter_wkwebview: b7e70ef1ddded7e69c796c7390ee74180182971f\n",
      "new_path": "ios/Podfile.lock",
      "old_path": "ios/Podfile.lock",
      "a_mode": "100644",
      "b_mode": "100644",
      "new_file": false,
      "renamed_file": false,
      "deleted_file": false
    });

    ChangedFile changedFile = ChangedFile.parse(diff);
    expect(changedFile.name, 'ios/Podfile.lock');
    expect(changedFile.fileLines.length, 66);
    expect(changedFile.fileLines.first.delLineNumber, '...');
    expect(changedFile.fileLines.first.addLineNumber, '...');
    expect(changedFile.fileLines.first.type, FileLineType.unchanged);
    expect(changedFile.fileLines.first.codeContent, '@@ -15,13 +15,15 @@ PODS:');
  });

  test('Should generate code lines from given data', () {
    String sourceDiff =
        "@@ -15,13 +15,15 @@ PODS:\n     - Flutter\n   - package_info_plus (0.4.5):\n     - Flutter\n-  - path_provider_ios (0.0.1):\n+  - path_provider_foundation (0.0.1):\n     - Flutter\n+    - FlutterMacOS\n   - permission_handler_apple (9.0.4):\n     - Flutter\n   - ReachabilitySwift (5.0.0)\n-  - shared_preferences_ios (0.0.1):\n+  - shared_preferences_foundation (0.0.1):\n     - Flutter\n+    - FlutterMacOS\n   - sqflite (0.0.2):\n     - Flutter\n     - FMDB (>= 2.7.5)\n@@ -44,9 +46,9 @@ DEPENDENCIES:\n   - image_picker_ios (from `.symlinks/plugins/image_picker_ios/ios`)\n   - open_filex (from `.symlinks/plugins/open_filex/ios`)\n   - package_info_plus (from `.symlinks/plugins/package_info_plus/ios`)\n-  - path_provider_ios (from `.symlinks/plugins/path_provider_ios/ios`)\n+  - path_provider_foundation (from `.symlinks/plugins/path_provider_foundation/ios`)\n   - permission_handler_apple (from `.symlinks/plugins/permission_handler_apple/ios`)\n-  - shared_preferences_ios (from `.symlinks/plugins/shared_preferences_ios/ios`)\n+  - shared_preferences_foundation (from `.symlinks/plugins/shared_preferences_foundation/ios`)\n   - sqflite (from `.symlinks/plugins/sqflite/ios`)\n   - updater (from `.symlinks/plugins/updater/ios`)\n   - url_launcher_ios (from `.symlinks/plugins/url_launcher_ios/ios`)\n@@ -73,12 +75,12 @@ EXTERNAL SOURCES:\n     :path: \".symlinks/plugins/open_filex/ios\"\n   package_info_plus:\n     :path: \".symlinks/plugins/package_info_plus/ios\"\n-  path_provider_ios:\n-    :path: \".symlinks/plugins/path_provider_ios/ios\"\n+  path_provider_foundation:\n+    :path: \".symlinks/plugins/path_provider_foundation/ios\"\n   permission_handler_apple:\n     :path: \".symlinks/plugins/permission_handler_apple/ios\"\n-  shared_preferences_ios:\n-    :path: \".symlinks/plugins/shared_preferences_ios/ios\"\n+  shared_preferences_foundation:\n+    :path: \".symlinks/plugins/shared_preferences_foundation/ios\"\n   sqflite:\n     :path: \".symlinks/plugins/sqflite/ios\"\n   updater:\n@@ -100,14 +102,14 @@ SPEC CHECKSUMS:\n   image_picker_ios: b786a5dcf033a8336a657191401bfdf12017dabb\n   open_filex: 6e26e659846ec990262224a12ef1c528bb4edbe4\n   package_info_plus: 6c92f08e1f853dc01228d6f553146438dafcd14e\n-  path_provider_ios: 14f3d2fd28c4fdb42f44e0f751d12861c43cee02\n+  path_provider_foundation: 37748e03f12783f9de2cb2c4eadfaa25fe6d4852\n   permission_handler_apple: 44366e37eaf29454a1e7b1b7d736c2cceaeb17ce\n   ReachabilitySwift: 985039c6f7b23a1da463388634119492ff86c825\n-  shared_preferences_ios: 548a61f8053b9b8a49ac19c1ffbc8b92c50d68ad\n+  shared_preferences_foundation: 297b3ebca31b34ec92be11acd7fb0ba932c822ca\n   sqflite: 6d358c025f5b867b29ed92fc697fd34924e11904\n   Toast: 91b396c56ee72a5790816f40d3a94dd357abc196\n   updater: d6c70e66a13a3704329f41f5653dd6e503753fa5\n-  url_launcher_ios: 839c58cdb4279282219f5e248c3321761ff3c4de\n+  url_launcher_ios: fb12c43172927bb5cf75aeebd073f883801f1993\n   video_player_avfoundation: e489aac24ef5cf7af82702979ed16f2a5ef84cff\n   wakelock: d0fc7c864128eac40eba1617cb5264d9c940b46f\n   webview_flutter_wkwebview: b7e70ef1ddded7e69c796c7390ee74180182971f\n\\ No newline at end of file\n";
    List<FileLine> lines = ChangedFile.generateCodeLines(sourceDiff);
    expect(lines.length, 66);
    expect(lines[64].delLineNumber, '113');
    expect(lines[64].addLineNumber, '115');
    expect(lines[64].type, FileLineType.unchanged);
    expect(lines[64].codeContent, '   webview_flutter_wkwebview: b7e70ef1ddded7e69c796c7390ee74180182971f');
  });

  test('Should pares code block from given data success', () {
    String sourceDiff = "@@ -15,13 +15,15 @@ PODS:\n     - Flutter\n@@ -44,9 +46,9 @@ DEPENDENCIES:\n@@ -73,12 +75,12 @@ EXTERNAL SOURCES:\n@@ -100,14 +102,14 @@ SPEC CHECKSUMS:\n";
    List<String> codeBlocks = ChangedFile.parseCodeSource([], sourceDiff);
    expect(codeBlocks.length, 4);
    expect(codeBlocks, [
      '@@ -15,13 +15,15 @@ PODS:\n     - Flutter',
      '\n@@ -44,9 +46,9 @@ DEPENDENCIES:',
      '\n@@ -73,12 +75,12 @@ EXTERNAL SOURCES:',
      '\n@@ -100,14 +102,14 @@ SPEC CHECKSUMS:\n',
    ]);
  });

  test('Should pares code block from given special data with @@', () {
    String sourceDiff =
        "@@ -4,30 +4,30 @@ Make some conficts\n+\n @@23423432\n  @@3435345\n @@ 34345345\n @@ wefewgwege @@ efwe\n @@ -8,3 +8,12 @@ Make some conficts\n 213123\n\\ No newline at end of file";
    String newLine = '\n@@ -100,14 +102,14 @@ SPEC CHECKSUMS:\n';
    List<String> codeBlocks = ChangedFile.parseCodeSource([], sourceDiff + newLine);
    expect(codeBlocks.length, 2);
    expect(codeBlocks, [sourceDiff, newLine]);
  });

  test('Should create ChangedFile object with given diff data2', () {
    Diff diff =
        Diff.fromJson({"diff": "@@ -0,0 +1 @@\n+ffggg\n", "new_path": "test", "old_path": "test", "a_mode": "0", "b_mode": "100644", "new_file": true, "renamed_file": false, "deleted_file": false});
    ChangedFile changedFile = ChangedFile.parse(diff);
    expect(changedFile.name, 'test');
    expect(changedFile.fileLines.length, 2);
    expect(changedFile.fileLines.first.delLineNumber, '...');
    expect(changedFile.fileLines.first.addLineNumber, '...');
    expect(changedFile.fileLines.first.type, FileLineType.unchanged);
    expect(changedFile.fileLines.first.codeContent, '@@ -0,0 +1 @@');

    expect(changedFile.fileLines.last.delLineNumber, '');
    expect(changedFile.fileLines.last.addLineNumber, '1');
    expect(changedFile.fileLines.last.type, FileLineType.added);
    expect(changedFile.fileLines.last.codeContent, '+ffggg');
  });
  test('Should create ChangedFile object with given diff data3', () {
    Diff diff =
        Diff.fromJson({"diff": "@@ -1 +0,0 @@\n-ffggg\n", "new_path": "test", "old_path": "test", "a_mode": "0", "b_mode": "100644", "new_file": true, "renamed_file": false, "deleted_file": false});
    ChangedFile changedFile = ChangedFile.parse(diff);
    expect(changedFile.name, 'test');
    expect(changedFile.fileLines.length, 2);
    expect(changedFile.fileLines.first.delLineNumber, '...');
    expect(changedFile.fileLines.first.addLineNumber, '...');
    expect(changedFile.fileLines.first.type, FileLineType.unchanged);
    expect(changedFile.fileLines.first.codeContent, '@@ -1 +0,0 @@');

    expect(changedFile.fileLines.last.delLineNumber, '1');
    expect(changedFile.fileLines.last.addLineNumber, '');
    expect(changedFile.fileLines.last.type, FileLineType.deleted);
    expect(changedFile.fileLines.last.codeContent, '-ffggg');
  });
}
