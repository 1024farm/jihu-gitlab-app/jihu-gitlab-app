import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/mr/models/jobs.dart';

void main() {
  var params = [
    {
      'name': 'Should get created when jobs all created',
      'stage': Stage('', [
        Job.fromJson({'status': 'CREATED'}),
        Job.fromJson({'status': 'CANCELED'}),
        Job.fromJson({'status': 'FAILED', 'allowFailure': true}),
        Job.fromJson({'status': 'SUCCESS'}),
        Job.fromJson({'status': 'CREATED'})
      ]),
      'expect': 'assets/images/pipeline_created.svg'
    },
    {
      'name': 'Should get success when jobs all success',
      'stage': Stage('', [
        Job.fromJson({'status': 'SUCCESS'}),
        Job.fromJson({'status': 'SUCCESS'}),
        Job.fromJson({'status': 'SUCCESS'}),
        Job.fromJson({'status': 'SUCCESS'})
      ]),
      'expect': 'assets/images/pipeline_success.svg'
    },
    {
      'name': 'Should get warning when any job doesn\'t allow failure',
      'stage': Stage('', [
        Job.fromJson({'status': 'FAILED', 'allowFailure': true}),
        Job.fromJson({'status': 'SUCCESS'}),
        Job.fromJson({'status': 'SUCCESS'}),
        Job.fromJson({'status': 'SUCCESS'})
      ]),
      'expect': 'assets/images/warning.svg'
    },
    {
      'name': 'Should get failed when any job failed',
      'stage': Stage('', [
        Job.fromJson({'status': 'FAILED'}),
        Job.fromJson({'status': 'CANCELED'}),
        Job.fromJson({'status': 'SUCCESS'}),
        Job.fromJson({'status': 'SUCCESS'}),
        Job.fromJson({'status': 'SUCCESS'})
      ]),
      'expect': 'assets/images/pipeline_failed.svg'
    },
    {
      'name': 'Should get unknown when jobs all unknown',
      'stage': Stage('', [
        Job.fromJson({'status': ''}),
        Job.fromJson({'status': 'CREATED'})
      ]),
      'expect': 'assets/images/pipeline_unknown.svg'
    },
    {
      'name': 'Should get pending when jobs is pending',
      'stage': Stage('', [
        Job.fromJson({'status': 'CREATED'}),
        Job.fromJson({'status': 'PENDING'})
      ]),
      'expect': 'assets/images/pipeline_pending.svg'
    },
    {
      'name': 'Should get pending when jobs is pending 2',
      'stage': Stage('', [
        Job.fromJson({'status': 'PENDING'}),
        Job.fromJson({'status': 'PENDING'})
      ]),
      'expect': 'assets/images/pipeline_pending.svg'
    },
    {
      'name': 'Should get pending when jobs is pending 3',
      'stage': Stage('', [
        Job.fromJson({'status': 'PENDING'}),
        Job.fromJson({'status': 'CANCELED'}),
        Job.fromJson({'status': 'SUCCESS'}),
        Job.fromJson({'status': 'FAILED', 'allowFailure': true})
      ]),
      'expect': 'assets/images/pipeline_pending.svg'
    },
    {
      'name': 'Should get running when jobs is running',
      'stage': Stage('', [
        Job.fromJson({'status': 'RUNNING'}),
        Job.fromJson({'status': 'PENDING'}),
        Job.fromJson({'status': 'CREATED'}),
        Job.fromJson({'status': 'SUCCESS'}),
        Job.fromJson({'status': 'FAILED', 'allowFailure': true})
      ]),
      'expect': 'assets/images/pipeline_running.svg'
    },
    {
      'name': 'Should not get canceled when jobs is canceled but running',
      'stage': Stage('', [
        Job.fromJson({'status': 'RUNNING'}),
        Job.fromJson({'status': 'CANCELED'}),
        Job.fromJson({'status': 'CREATED'}),
        Job.fromJson({'status': 'SUCCESS'}),
        Job.fromJson({'status': 'FAILED', 'allowFailure': true})
      ]),
      'expect': 'assets/images/pipeline_running.svg'
    },
    {
      'name': 'Should get warning when jobs is canceled',
      'stage': Stage('', [
        Job.fromJson({'status': 'CANCELED'}),
        Job.fromJson({'status': 'SUCCESS'}),
        Job.fromJson({'status': 'SUCCESS'}),
      ]),
      'expect': 'assets/images/warning.svg'
    },
    {
      'name': 'Should get canceled when jobs are all canceled',
      'stage': Stage('', [
        Job.fromJson({'status': 'CANCELED'}),
        Job.fromJson({'status': 'CANCELED'}),
        Job.fromJson({'status': 'CANCELED'}),
      ]),
      'expect': 'assets/images/pipeline_canceled.svg'
    }
  ];
  for (var testCase in params) {
    test(testCase['name'] as String, () {
      expect((testCase['stage'] as Stage).stateIconAssetName, testCase['expect'] as String);
    });
  }
}
