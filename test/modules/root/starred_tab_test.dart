import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/stars_provider.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/projects.dart';
import 'package:jihu_gitlab_app/modules/projects/starred/projects_starred_provider.dart';
import 'package:jihu_gitlab_app/modules/root/model/root_store.dart';
import 'package:jihu_gitlab_app/modules/root/starred_tab.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../core/net/http_request_test.mocks.dart';
import '../../mocker/tester.dart';
import '../projects/starred/projects_starred_page_test.mocks.dart';

final client = MockHttpClient();

@GenerateNiceMocks([MockSpec<ProjectsStarredProvider>()])
void main() {
  late ProjectsStarredModel model;
  late MockProjectsStarredProvider provider;

  setUp(() async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    await ConnectionProvider().restore();

    model = ProjectsStarredModel();
    locator.registerSingleton(model);
    provider = MockProjectsStarredProvider();
    model.injectDataProviderForTesting(provider);
  });

  tearDown(() {
    locator.unregister<ProjectsStarredModel>();
  });

  testWidgets('Should display Empty View when logged in with empty data', (tester) async {
    ConnectionProvider().reset(Tester.jihuLabUser());

    when(provider.loadFromLocal()).thenAnswer((_) => Future(() => []));
    when(client.get('/users/9527/starred_projects?page=1&per_page=50')).thenAnswer((_) => Future(() => Response.of([])));
    HttpClient.injectInstanceForTesting(client);

    await tester.pumpWidget(
      MultiProvider(providers: [
        ChangeNotifierProvider(create: (context) => ConnectionProvider()),
        ChangeNotifierProvider(create: (context) => StarsProvider()),
      ], child: const MaterialApp(home: Scaffold(body: StarredTab()))),
    );
    await tester.pumpAndSettle();

    expect(find.text('No star'), findsOneWidget);

    ConnectionProvider().fullReset();

    StarsProvider().fullReset();
  });

  testWidgets('Should display List View when logged in with data', (tester) async {
    ConnectionProvider().reset(Tester.jihuLabUser());

    List<GroupAndProject> starredProjects = [
      GroupAndProject(1, 120181, "上线审批组", SubgroupItemType.subgroup, "ultimate-plan/jihu-gitlab-app/deployment-approvers", 0, starred: true),
      GroupAndProject(2, 75468, "API", SubgroupItemType.project, "ultimate-plan/jihu-gitlab-app/api", 0, starred: true)
    ];
    when(provider.loadFromLocal()).thenAnswer((_) => Future(() => starredProjects));
    when(client.get<List<dynamic>>('/users/9527/starred_projects?page=1&per_page=50')).thenAnswer((_) => Future(() => Response.of<List<dynamic>>(starredProjects)));
    HttpClient.injectInstanceForTesting(client);

    await tester.pumpWidget(
      MultiProvider(providers: [
        ChangeNotifierProvider(create: (context) => RootStore()),
        ChangeNotifierProvider(create: (context) => ConnectionProvider()),
        ChangeNotifierProvider(create: (context) => StarsProvider()),
      ], child: const MaterialApp(home: Scaffold(body: StarredTab()))),
    );
    await tester.pumpAndSettle();

    expect(find.byType(StarredTab), findsOneWidget);
    expect(find.byType(ListView), findsOneWidget);
    expect(find.text("上线审批组"), findsOneWidget);
    expect(find.text("API"), findsOneWidget);
    ConnectionProvider().fullReset();

    StarsProvider().fullReset();
  });
}
