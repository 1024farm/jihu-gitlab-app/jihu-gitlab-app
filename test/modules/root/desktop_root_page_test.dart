import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/main.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/root/desktop_root_page.dart';
import 'package:mockito/mockito.dart';

import '../../core/net/http_request_test.mocks.dart';
import '../../finder/svg_finder.dart';
import '../../test_binding_setter.dart';
import '../../test_data/issue.dart';

void main() {
  setUp(() async {
    WidgetsFlutterBinding.ensureInitialized();
    setupLocator();

    final client = MockHttpClient();
    when(client.postWithConnection<dynamic>("/api/graphql", getCommunityTypesGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/faq"), any))
        .thenAnswer((_) => Future(() => Response.of(faqTypesResponse)));
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets('Should be able to toggle sidebar in landscape screen', (tester) async {
    await setUpDesktopBinding(tester);
    await tester.pumpWidget(const App());
    await tester.pumpAndSettle();

    await tester.ensureVisible(find.byType(DesktopRootPage));
    expect(find.text(AppLocalizations.dictionary().appName), findsOneWidget);
    expect(find.byType(Drawer), findsNothing);
    expect(SvgFinder('assets/images/toggle_sidebar.svg'), findsOneWidget);
    expect(find.byType(AnimatedSize), findsOneWidget);
    assertSidebarWidth(tester, 300);

    await tester.tap(SvgFinder('assets/images/toggle_sidebar.svg'));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    assertSidebarWidth(tester, 0);
    await tester.tap(SvgFinder('assets/images/toggle_sidebar.svg'));
    await tester.pumpAndSettle();
    assertSidebarWidth(tester, 300);
  });
}

void assertSidebarWidth(tester, double expectWidth) {
  var finder = find.byKey(const Key("sidebar")).first;
  SizedBox sizedBox = tester.firstWidget(finder);
  expect(sizedBox.width, expectWidth);
}
