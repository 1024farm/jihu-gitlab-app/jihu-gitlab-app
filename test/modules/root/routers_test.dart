import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/l10n/current_locale.dart';
import 'package:jihu_gitlab_app/modules/home/language_settings_page.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_request_page.dart';
import 'package:jihu_gitlab_app/modules/mr/models/mr_graphql_request_body.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/project_repository_model.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../core/net/http_request_test.mocks.dart';
import '../../test_binding_setter.dart';
import '../../test_data/discussion.dart';
import '../../test_data/issue.dart';
import '../../test_data/merge_request.dart';
import '../../test_data/project.dart';
import '../issues/details/issue_details_page_test.mocks.dart';
import '../projects/repository/response_data.dart';

void main() {
  late MockHttpClient client;

  setUp(() async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    client = MockHttpClient();
    when(client.post<dynamic>('/api/graphql', argThat(predicate((arg) => !arg.toString().contains("getRepository"))))).thenAnswer((_) => Future(() => Response.of<dynamic>({
          'data': {
            'project': {
              "issues": {
                "pageInfo": {
                  "hasNextPage": true,
                  "startCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMi0xMi0yMiAxMjoyODozMS4yMzk4MzYwMDAgKzA4MDAiLCJpZCI6IjI5NTMyOSJ9",
                  "endCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMi0xMi0wMSAxOTo0MzoxNi44MjEzODIwMDAgKzA4MDAiLCJpZCI6IjI3NjA0OSJ9"
                },
                "nodes": []
              }
            }
          }
        })));
    when(client.get<List<dynamic>>("/api/v4/groups?page=1&per_page=100")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
    when(client.get<Map<String, dynamic>>("/api/v4/projects/0/merge_requests/0/approvals")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(approvalResponse)));
    when(client.get("/api/v4/projects/0/merge_requests/0/diffs?page=1&per_page=20")).thenAnswer((_) => Future(() => Response.of([])));
    when(client.get('/api/v4/projects/59893')).thenAnswer((_) => Future(() => Response.of<Map>({"path_with_namespace": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app"})));
    when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 0)))
        .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(mrGraphQLResponse)));
    when(client.post('/api/graphql', getPaidMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 0)))
        .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(paidMrGraphQLResponse)));
    when(client.post('/api/graphql', queryJobsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 0, ''))).thenAnswer((_) => Future(() => Response.of(jobsGraphQLResponse)));
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfCheckRepositoryIsEmpty("relativePath")))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(checkRepositoryIsEmptyResponse(exists: false))));
    HttpClient.injectInstanceForTesting(client);
  });

  Widget buildWidget(String routeName, params) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (context) => ConnectionProvider()),
          ChangeNotifierProvider(create: (context) => LocaleProvider()),
          ChangeNotifierProvider(create: (context) => ProjectProvider())
        ],
        child: MaterialApp(
          onGenerateRoute: onGenerateRoute,
          home: Scaffold(body: Builder(builder: (BuildContext context) {
            return Center(
                child: InkWell(
                    onTap: () {
                      Navigator.of(context).pushNamed(routeName, arguments: params);
                    },
                    child: const Text('TestButton')));
          })),
          localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
        ));
  }

  Future<void> toFindPage(tester, Type type) async {
    await tester.pumpAndSettle();
    await tester.tap(find.text('TestButton'));
    await tester.pumpAndSettle();
    expect(find.byType(type), findsOneWidget);

    ConnectionProvider().fullReset();
    ProjectProvider().fullReset();
  }

  var params = [
    {"name": "Should route to AI page", "routeName": AIPage.routeName, "params": {}, "find": AIPage},
    {
      "name": "Should route to Login page",
      "routeName": LoginPage.routeName,
      "params": {'host': 'test.host', 'clientId': 'client_id'},
      "find": LoginPage
    },
    {
      "name": "Should route to Project Issues page",
      "routeName": ProjectPage.routeName,
      "params": {"name": "极狐 GitLab APP 代码", "projectId": 59893, "relativePath": "relativePath"},
      "find": ProjectPage
    },
    {
      "name": "Should route to DescriptionTemplateSelector page",
      "routeName": DescriptionTemplateSelector.routeName,
      "params": {"projectId": 78632, 'free': false},
      "find": DescriptionTemplateSelector
    },
    {
      "name": 'Should route to SelfManagedLoginPage',
      "routeName": SelfManagedLoginPage.routeName,
      "params": {},
      "find": SelfManagedLoginPage,
    },
    {
      "name": 'Should route to PostSalesServicePage',
      "routeName": PostSalesServicePage.routeName,
      "params": {},
      "find": PostSalesServicePage,
    },
    {
      "name": 'Should route to PrivacyPolicyPage',
      "routeName": PrivacyPolicyPage.routeName,
      "params": {},
      "find": PrivacyPolicyPage,
    },
    {
      "name": 'Should route to LanguageSettingsPage',
      "routeName": LanguageSettingsPage.routeName,
      "params": {},
      "find": LanguageSettingsPage,
    },
    {
      "name": 'Should route to MergeRequestPage',
      "routeName": MergeRequestPage.routeName,
      "params": {'projectId': 0, 'projectName': '', 'mergeRequestIid': 0, 'fullPath': 'ultimate-plan/jihu-gitlab-app/jihu-gitlab-app'},
      "find": MergeRequestPage,
    },
  ];

  for (var element in params) {
    testWidgets(element['name'] as String, (tester) async {
      setUpCompatibleBinding(tester);
      String routeName = element['routeName'] as String;
      await tester.pumpWidget(buildWidget(routeName, element['params']));
      await toFindPage(tester, element['find'] as Type);
    });
  }

  testWidgets('Should route to IssueDetailsPage', (tester) async {
    var issueDetailsModel = IssueDetailsModel();
    locator.registerSingleton(issueDetailsModel);
    var provider = MockDiscussionProvider();
    issueDetailsModel.injectDataProviderForTesting(provider);

    when(client.get<List<dynamic>>("/api/v4/projects/72936/issues/265567/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>(discussionsData)));
    when(client.get<Map<String, dynamic>>("/api/v4/projects/72936/issues/265567")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(issueData)));
    when(client.get<Map<String, dynamic>>("/api/v4/projects/72936")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(projectData)));
    when(client.get<List<dynamic>>("/api/v4/projects/72936/labels?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
    when(client.get("/api/v4/projects/0/merge_requests/0")).thenAnswer((_) => Future(() => Response.of({})));

    String routeName = IssueDetailsPage.routeName;
    Map<String, dynamic> params = {'projectId': 72936, 'issueId': 3242, 'issueIid': 6, 'targetId': 45345, 'targetIid': 265567, 'pathWithNamespace': 'ultimate-plan/jihu-gitlab-app/demo-mr-test'};

    await tester.pumpWidget(buildWidget(routeName, params));

    await tester.pumpAndSettle();
    await tester.tap(find.text('TestButton'));
    for (int i = 0; i < 5; i++) {
      await tester.pump(const Duration(seconds: 1));
    }
    expect(find.byType(IssueDetailsPage), findsOneWidget);
  });
}
