var mergeRequestsResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "name": "极狐 GitLab App",
      "mergeRequests": {
        "nodes": [
          {
            "iid": "427",
            "title": "Draft: Resolve \"Hotfix: 待办列表翻页灰屏\"",
            "author": {"id": "gid://gitlab/User/29758", "name": "ling zhang", "username": "zhangling", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png"}
          },
          {
            "iid": "413",
            "title": "Draft: Resolve \"Feature: 用户未登录 jihulab 时浏览帖子评论看到登录提示\"",
            "author": {"id": "gid://gitlab/User/30192", "name": "Raymond Liao", "username": "raymond-liao", "avatarUrl": "/uploads/-/system/user/avatar/30192/avatar.png"}
          },
          {
            "iid": "412",
            "title": "Draft: Resolve \"Feature: 手机用户查看项目 MR 列表（open 状态）\"",
            "author": {"id": "gid://gitlab/User/29064", "name": "Neil Wang", "username": "NeilWang", "avatarUrl": "/uploads/-/system/user/avatar/29064/avatar.png"}
          }
        ],
        "pageInfo": {"hasNextPage": false, "endCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMy0wMy0wMiAxNTo1MjoxMC4zMTYzOTAwMDAgKzA4MDAiLCJpZCI6IjIzOTQzMCJ9"}
      }
    }
  }
};
