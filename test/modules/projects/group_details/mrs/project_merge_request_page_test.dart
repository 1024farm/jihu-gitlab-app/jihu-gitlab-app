import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_request_page.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/mrs/project_merge_request_model.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/mrs/project_merge_request_page.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

import '../../../../core/net/http_request_test.mocks.dart';
import '../../../../mocker/tester.dart';
import 'mr_page_test_data.dart';

void main() {
  HttpClient client = MockHttpClient();
  setUp(() async {
    ConnectionProvider().reset(Tester.jihuLabUser());
    when(client.post('/api/graphql', getMRListGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 20, ''))).thenAnswer((_) => Future(() => Response.of(mergeRequestsResponse)));
    when(client.get('/api/v4/projects/59893/merge_requests/427/approvals')).thenThrow(Exception());
    when(client.get('/api/v4/projects/59893/merge_requests/427/diffs?page=1&per_page=20')).thenThrow(Exception());
    when(client.post('/api/graphql', predicate((arg) => arg.toString().contains("getMergeRequest")))).thenThrow(Exception());
    when(client.post('/api/graphql', predicate((arg) => arg.toString().contains("onlyAllowMergeIfAllStatusChecksPassed")))).thenThrow(Exception());
    HttpClient.injectInstanceForTesting(client);
  });

  test('Should getMRListGraphQLRequestBody has expected params', () {
    expect(getMRListGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 20, ''), {
      "query": '''
      {
        project(fullPath: "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app") {
          id
          name
          mergeRequests(state: opened, first: 20, after: "") {
            nodes {
              iid
              title
              author {
                id
                name
                username
                avatarUrl
              }
            }
            pageInfo {
              hasNextPage
              endCursor
            }
          }
        }
      }
      '''
    });
  });

  testWidgets('Should view mr list by user and tapped into view', (tester) async {
    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(
        onGenerateRoute: onGenerateRoute,
        home: const Scaffold(
          body: Center(
            child: ProjectMergeRequestPage(arguments: {'relativePath': "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app"}),
          ),
        ),
      ),
    ));
    await tester.pumpAndSettle();
    expect(find.textContaining('待办列表翻页灰屏'), findsOneWidget);
    await tester.tap(find.textContaining('待办列表翻页灰屏'));
    await tester.pumpAndSettle();
    expect(find.byType(MergeRequestPage), findsOneWidget);
    ConnectionProvider().clearActive();
  });

  testWidgets('Should display no data view when response empty list', (tester) async {
    when(client.post('/api/graphql', getMRListGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 20, ''))).thenAnswer((_) => Future(() => Response.of({
          "data": {
            "project": {
              "id": "gid://gitlab/Project/59893",
              "name": "App",
              "mergeRequests": {
                "nodes": [],
                "pageInfo": {"hasNextPage": false, "endCursor": null}
              }
            }
          }
        })));
    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(
        onGenerateRoute: onGenerateRoute,
        home: const Scaffold(
          body: Center(
            child: ProjectMergeRequestPage(arguments: {'relativePath': "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app"}),
          ),
        ),
      ),
    ));
    await tester.pumpAndSettle();
    expect(find.widgetWithText(TipsView, 'No data'), findsOneWidget);
  });

  tearDown(() {
    ConnectionProvider().fullReset();
  });
}
