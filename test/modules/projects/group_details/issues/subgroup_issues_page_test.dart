import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/modules/issues/list/group_issues_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/list/group_issues_remaining_information_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/list/group_projects_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_page.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_view.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/issues/subgroup_issues_page.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../core/net/http_request_test.mocks.dart';
import '../../../../mocker/tester.dart';
import '../../../../test_binding_setter.dart';
import '../../../../test_data/issue.dart';

void main() {
  final client = MockHttpClient();

  setUp(() async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();

    when(client.post<dynamic>("/api/graphql", getGroupIssuesGraphQLRequestBody('path', '', 20))).thenAnswer((_) => Future(() => Response.of(groupIssuesGraphQLResponseData)));
    when(client.post<dynamic>("/api/graphql", getGroupProjectsGraphQLRequestBody('path', ['gid://gitlab/Project/59893', 'gid://gitlab/Project/72936'])))
        .thenAnswer((_) => Future(() => Response.of(groupProjectsGraphQLResponseData)));
    when(client.post<dynamic>("/api/graphql", getGroupIssuesGraphQLRequestBody('path', 'a', 20))).thenAnswer((_) => Future(() => Response.of(groupIssuesGraphQLResponseData)));
    HttpClient.injectInstanceForTesting(client);

    ConnectionProvider().reset(Tester.jihuLabUser());
  });

  group("Subgroup issues page", () {
    testWidgets('Should display page', (tester) async {
      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
          child: const MaterialApp(
            home: Scaffold(
              body: SubgroupIssuesPage(groupId: 88966, relativePath: 'path'),
            ),
          )));
      await tester.pumpAndSettle(const Duration(seconds: 1));
      await tester.pumpAndSettle(const Duration(seconds: 1));
      expect(find.text('Feature: 用户可以为评论点赞/反对/取消'), findsOneWidget);
      expect(find.text('Bugfix:议题详情视频与图片显示问题'), findsOneWidget);
    });

    testWidgets('Should display page with ce', (tester) async {
      reset(client);
      when(client.post<dynamic>("/api/graphql", getGroupIssuesGraphQLRequestBody('path', '', 20))).thenAnswer((_) => Future(() => Response.of(groupIssuesCeGraphQLResponseData)));
      when(client.post<dynamic>("/api/graphql", getGroupProjectsGraphQLRequestBody('path', ['gid://gitlab/Project/59893', 'gid://gitlab/Project/72936'])))
          .thenAnswer((_) => Future(() => Response.of(groupProjectsGraphQLResponseData)));
      when(client.post<dynamic>("/api/graphql", getGroupIssuesGraphQLRequestBody('path', 'a', 20))).thenAnswer((_) => Future(() => Response.of(groupIssuesCeGraphQLResponseData)));
      when(client.get(
              '/api/v4/groups/88966/issues?iids[]=477&iids[]=476&iids[]=475&iids[]=474&iids[]=473&iids[]=471&iids[]=39&iids[]=470&iids[]=469&iids[]=466&iids[]=464&iids[]=463&iids[]=462&iids[]=461&iids[]=457&iids[]=453&iids[]=452&iids[]=451&iids[]=450&iids[]=449'))
          .thenAnswer((_) => Future(() => Response.of(groupIssuesCeGraphQLProjectResponseData)));
      when(client.post<dynamic>("/api/graphql", getGroupIssuesRemainingInformationGraphQLRequestBody('path', '', 20))).thenAnswer((_) => Future(() => Response.of(groupIssuesCeGraphQLResponseData)));
      when(client.post<dynamic>("/api/graphql", getGroupIssuesRemainingInformationGraphQLRequestBody('path', 'a', 20))).thenAnswer((_) => Future(() => Response.of(groupIssuesCeGraphQLResponseData)));
      HttpClient.injectInstanceForTesting(client);
      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
          child: const MaterialApp(
            home: Scaffold(
              body: SubgroupIssuesPage(groupId: 88966, relativePath: 'path'),
            ),
          )));
      await tester.pumpAndSettle(const Duration(seconds: 1));
      await tester.pumpAndSettle(const Duration(seconds: 1));
      expect(find.text('Feature: 用户可以为评论点赞/反对/取消'), findsOneWidget);
      expect(find.text('Bugfix:议题详情视频与图片显示问题'), findsOneWidget);
    });

    testWidgets('Should show issue state select view', (tester) async {
      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
          child: const MaterialApp(
            home: Scaffold(
              body: SubgroupIssuesPage(groupId: 88966, relativePath: 'path'),
            ),
          )));
      await tester.pumpAndSettle();
      expect(find.byKey(const Key('filter')), findsOneWidget);
    });

    testWidgets('Should search something', (tester) async {
      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
          child: const MaterialApp(
            home: Scaffold(
              body: SubgroupIssuesPage(groupId: 88966, relativePath: 'path'),
            ),
          )));
      await tester.pumpAndSettle();
      await tester.enterText(find.byType(TextField), 'a');
      await tester.pumpAndSettle(const Duration(seconds: 1));
      await tester.tap(find.byKey(const Key('close')));
      await tester.pumpAndSettle();
      expect(find.text('Search'), findsOneWidget);
      await tester.enterText(find.byType(TextField), 'a');
      await tester.testTextInput.receiveAction(TextInputAction.done);
      await tester.pumpAndSettle();
      expect(find.text('Feature: 用户可以为评论点赞/反对/取消'), findsOneWidget);
      expect(find.text('Bugfix:议题详情视频与图片显示问题'), findsOneWidget);
    });

    testWidgets('Should search something but empty', (tester) async {
      when(client.post<dynamic>("/api/graphql", getGroupIssuesGraphQLRequestBody('path', '', 20))).thenAnswer((_) => Future(() => Response.of(groupIssuesGraphQLEmptyResponseData)));
      when(client.post<dynamic>("/api/graphql", getGroupIssuesGraphQLRequestBody('path', 'a', 20))).thenAnswer((_) => Future(() => Response.of(groupIssuesGraphQLEmptyResponseData)));

      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
          child: const MaterialApp(
            home: Scaffold(
              body: SubgroupIssuesPage(groupId: 88966, relativePath: 'path'),
            ),
          )));
      await tester.pumpAndSettle();
      await tester.enterText(find.byType(TextField), 'a');
      await tester.pumpAndSettle(const Duration(seconds: 1));
      await tester.tap(find.byKey(const Key('close')));
      await tester.pumpAndSettle();
      expect(find.text('Search'), findsOneWidget);
      await tester.enterText(find.byType(TextField), 'a');
      await tester.testTextInput.receiveAction(TextInputAction.done);
      await tester.pumpAndSettle();
      expect(find.text('No matches for \'a\''), findsOneWidget);
    });
  });

  testWidgets('Should load more subgroup issues', (tester) async {
    await setUpMobileBinding(tester);
    when(client.post<dynamic>("/api/graphql", getGroupIssuesGraphQLRequestBody('path', '', 20))).thenAnswer((_) => Future(() => Response.of(groupIssuesGraphQLResponseData)));
    when(client.post<dynamic>("/api/graphql", getGroupIssuesGraphQLRequestBody('path', '', 20, afterCursor: 'end_cursor')))
        .thenAnswer((_) => Future(() => Response.of(groupIssuesGraphQLNextPageResponseData)));

    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
      child: MaterialApp(
          home: Scaffold(
              body: SubgroupIssuesPage(
        refreshKey: GlobalKey(),
        groupId: 88966,
        relativePath: 'path',
        onSelectedIssueChange: (index, param) => {},
      ))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();

    expect(find.byType(IssuesPage), findsOneWidget);
    expect(find.byType(IssuesView), findsOneWidget);
    expect(find.byType(TipsView), findsNothing);
    expect(find.byType(ListView), findsOneWidget);
    expect(find.text('Feature: 用户可以为评论点赞/反对/取消'), findsOneWidget);

    var listFinder = find.byType(Scrollable).last;
    await tester.scrollUntilVisible(find.text('Spike: 墓碑调研'), 500.0, scrollable: listFinder);
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.text('Spike: 墓碑调研'), findsOneWidget);

    await tester.scrollUntilVisible(find.text('Feature: 用户可以为评论点赞/反对/取消2'), 500.0, scrollable: listFinder);
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.text('Feature: 用户可以为评论点赞/反对/取消2'), findsOneWidget);
  });

  tearDown(() {
    ConnectionProvider().fullReset();
    reset(client);
  });
}
