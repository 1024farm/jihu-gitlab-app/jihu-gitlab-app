import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/widgets/permission_low_view.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/core/widgets/warning_view.dart';
import 'package:jihu_gitlab_app/modules/issues/list/group_projects_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/iteration/details/iteration_model.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/iterations/subgroup_cadence_iteration_model.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/iterations/subgroup_cadence_model.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/iterations/subgroup_iterations_page.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/milestone_and_iteration_duration_view.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

import '../../../../core/net/http_request_test.mocks.dart';
import '../../../../finder/svg_finder.dart';
import '../../../../integration_tests/modules/projects/project_page_test.dart';
import '../../../../mocker/tester.dart';
import '../../../../test_data/issue.dart';

final client = MockHttpClient();

void main() {
  setUp(() async {
    ConnectionProvider().reset(Tester.jihuLabUser());
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets('Should display page with No Data View with no Cadences', (tester) async {
    var cadencesRequestResponseNoData = {
      "data": {
        "workspace": {
          "id": "gid://gitlab/Group/88966",
          "iterationCadences": {
            "nodes": [],
            "pageInfo": {"hasNextPage": false, "hasPreviousPage": true, "startCursor": null, "endCursor": null, "__typename": "PageInfo"},
            "__typename": "IterationCadenceConnection"
          },
          "__typename": "Group"
        }
      }
    };
    when(client.post('/api/graphql', groupIterationCadenceGraphql("ultimate-plan/jihu-gitlab-app", 20, ""))).thenAnswer((_) => Future(() => Response.of(cadencesRequestResponseNoData)));

    await tester.pumpWidget(const MaterialApp(
      home: Scaffold(
        body: SubgroupIterationsPage(fullPath: 'ultimate-plan/jihu-gitlab-app'),
      ),
    ));
    for (int i = 0; i < 5; i++) {
      await tester.pump(const Duration(seconds: 1));
    }
    expect(find.byType(SubgroupIterationsPage), findsOneWidget);
    expect(find.byType(TipsView), findsOneWidget);
    expect(find.text('No iteration items'), findsOneWidget);
  });

  testWidgets('Should display page with No Data View with no Iterations', (tester) async {
    var cadencesRequestResponseData = {
      "data": {
        "workspace": {
          "id": "gid://gitlab/Group/88966",
          "iterationCadences": {
            "nodes": [
              {"id": "gid://gitlab/Iterations::Cadence/316", "title": "单周迭代", "durationInWeeks": 1, "automatic": true, "__typename": "IterationCadence"}
            ],
            "pageInfo": {"hasNextPage": false, "hasPreviousPage": false, "startCursor": "MQ", "endCursor": "MQ", "__typename": "PageInfo"},
            "__typename": "IterationCadenceConnection"
          },
          "__typename": "Group"
        }
      }
    };
    when(client.post('/api/graphql', groupIterationCadenceGraphql("ultimate-plan/jihu-gitlab-app", 20, ""))).thenAnswer((_) => Future(() => Response.of(cadencesRequestResponseData)));
    var cadenceIterationsResponseNoData = {
      "data": {
        "group": {
          "id": "gid://gitlab/Group/88966",
          "iterations": {
            "nodes": [],
            "pageInfo": {"hasNextPage": false, "hasPreviousPage": false, "startCursor": null, "endCursor": null}
          }
        }
      }
    };
    when(client.post('/api/graphql', subgroupCadenceIterationGraphql("ultimate-plan/jihu-gitlab-app", "gid://gitlab/Iterations::Cadence/316", 20, "")))
        .thenAnswer((_) => Future(() => Response.of(cadenceIterationsResponseNoData)));

    await tester.pumpWidget(const MaterialApp(
      home: Scaffold(
        body: SubgroupIterationsPage(fullPath: 'ultimate-plan/jihu-gitlab-app'),
      ),
    ));
    for (int i = 0; i < 5; i++) {
      await tester.pump(const Duration(seconds: 1));
    }
    expect(find.byType(SubgroupIterationsPage), findsOneWidget);
    expect(find.byType(TipsView), findsOneWidget);
    expect(find.text('No iteration items'), findsOneWidget);
  });

  testWidgets('Should display page with List View', (tester) async {
    when(client.post('/api/graphql', requestBodyOfIterationInfo('ultimate-plan/jihu-gitlab-app', 'gid://gitlab/Iteration/1608')))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(iterationInfoResp)));
    when(client.post('/api/graphql', groupIterationCadenceGraphql("ultimate-plan/jihu-gitlab-app", 20, ""))).thenAnswer((_) => Future(() => Response.of(cadencesRequestResponseData)));
    when(client.post('/api/graphql', subgroupCadenceIterationGraphql("ultimate-plan/jihu-gitlab-app", "gid://gitlab/Iterations::Cadence/316", 20, "")))
        .thenAnswer((_) => Future(() => Response.of(iterationsRequestResponseData)));
    when(client.post('/api/graphql', subgroupIterationIssuesRequestBody("ultimate-plan/jihu-gitlab-app", 'gid://gitlab/Iteration/1608')))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(groupIssuesGraphQLResponseData)));
    when(client.post<dynamic>("/api/graphql", getGroupProjectsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app', ['gid://gitlab/Project/59893', 'gid://gitlab/Project/72936'])))
        .thenAnswer((_) => Future(() => Response.of(groupProjectsGraphQLResponseData)));

    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(
        onGenerateRoute: onGenerateRoute,
        home: const Scaffold(
          body: SubgroupIterationsPage(fullPath: 'ultimate-plan/jihu-gitlab-app'),
        ),
      ),
    ));
    for (int i = 0; i < 5; i++) {
      await tester.pump(const Duration(seconds: 1));
    }
    expect(find.byType(ListView), findsOneWidget);
    expect(find.text("单周迭代"), findsOneWidget);
    expect(find.byType(MilestoneAndIterationDurationView), findsNWidgets(7));
    expect(find.textContaining("Apr 3, 2023 - Apr 9, 2023"), findsOneWidget);
    await tester.tap(find.textContaining("Apr 3, 2023 - Apr 9, 2023"));
    await tester.pumpAndSettle();
    var finder = find.byType(HtmlWidget);
    expect(finder, findsOneWidget);
    expect(tester.widget<HtmlWidget>(finder).html, "迭代目标");
    await tester.scrollUntilVisible(find.text('Feature: 用户可以为评论点赞/反对/取消'), 500);
    expect(find.text("No Epic"), findsOneWidget);
    expect(find.text('Feature: 用户可以为评论点赞/反对/取消'), findsOneWidget);
    ConnectionProvider().fullReset();
  });

  testWidgets('Should display page with Authorized Denied View', (tester) async {
    var cadencesRequestResponseNoData = {
      "data": {
        "workspace": {"id": "gid://gitlab/Group/88966", "iterationCadences": null, "__typename": "Group"}
      }
    };
    when(client.post('/api/graphql', groupIterationCadenceGraphql("ultimate-plan/jihu-gitlab-app", 20, ""))).thenAnswer((_) => Future(() => Response.of(cadencesRequestResponseNoData)));

    await tester.pumpWidget(const MaterialApp(
      home: Scaffold(
        body: SubgroupIterationsPage(fullPath: 'ultimate-plan/jihu-gitlab-app'),
      ),
    ));
    for (int i = 0; i < 5; i++) {
      await tester.pump(const Duration(seconds: 1));
    }
    expect(find.byType(SubgroupIterationsPage), findsOneWidget);
    expect(find.byType(WarningView), findsOneWidget);
    ConnectionProvider().clearActive();
  });

  testWidgets('Should display PermissionLowView when response no fields error', (tester) async {
    when(client.post('/api/graphql', groupIterationCadenceGraphql('ultimate-plan/jihu-gitlab-app/demo', 20, "")))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(cadencesRequestResponseErrorsData)));
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: const Scaffold(body: SubgroupIterationsPage(fullPath: 'ultimate-plan/jihu-gitlab-app/demo'))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(PermissionLowView), findsOneWidget);
    expect(SvgFinder('assets/images/warning.svg'), findsOneWidget);
    expect(find.text('Feature Not Supported'), findsOneWidget);
    expect(find.textContaining('contact@gitlab.cn'), findsOneWidget);
  });

  testWidgets('Should display PermissionLowView when response no permission error', (tester) async {
    when(client.post('/api/graphql', groupIterationCadenceGraphql('ultimate-plan/jihu-gitlab-app/demo', 20, "")))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(cadencesRequestResponseDataWithErrorsData)));
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: const Scaffold(body: SubgroupIterationsPage(fullPath: 'ultimate-plan/jihu-gitlab-app/demo'))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(PermissionLowView), findsOneWidget);
    expect(SvgFinder('assets/images/warning.svg'), findsOneWidget);
    expect(find.text('Feature Not Supported'), findsOneWidget);
    expect(find.textContaining('contact@gitlab.cn'), findsOneWidget);
  });

  testWidgets('Should display PermissionLowView when response iterationCadences is null', (tester) async {
    when(client.post('/api/graphql', groupIterationCadenceGraphql('ultimate-plan/jihu-gitlab-app/demo', 20, "")))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(cadencesRequestResponseNullData)));
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: const Scaffold(body: SubgroupIterationsPage(fullPath: 'ultimate-plan/jihu-gitlab-app/demo'))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(PermissionLowView), findsOneWidget);
    expect(SvgFinder('assets/images/warning.svg'), findsOneWidget);
    expect(find.text('Feature Not Supported'), findsOneWidget);
    expect(find.textContaining('contact@gitlab.cn'), findsOneWidget);
  });

  tearDown(() {
    reset(client);
  });
}

var cadencesRequestResponseData = {
  "data": {
    "workspace": {
      "id": "gid://gitlab/Group/88966",
      "iterationCadences": {
        "nodes": [
          {"id": "gid://gitlab/Iterations::Cadence/316", "title": "单周迭代", "durationInWeeks": 1, "automatic": true, "__typename": "IterationCadence"}
        ],
        "pageInfo": {"hasNextPage": false, "hasPreviousPage": false, "startCursor": "MQ", "endCursor": "MQ", "__typename": "PageInfo"},
        "__typename": "IterationCadenceConnection"
      },
      "__typename": "Group"
    }
  }
};

var cadencesRequestResponseNullData = {
  "data": {
    "workspace": {"id": "gid://gitlab/Group/88966", "iterationCadences": null}
  }
};

var cadencesRequestResponseErrorsData = {
  "errors": [
    {
      "message": "Field 'iterationCadences' doesn't exist on type 'Group'",
      "locations": [
        {"line": 4, "column": 5}
      ],
      "path": ["query groupIterationCadences", "workspace", "iterationCadences"],
      "extensions": {"code": "undefinedField", "typeName": "Group", "fieldName": "iterationCadences"}
    },
    {
      "message": "Variable \$beforeCursor is declared by groupIterationCadences but not used",
      "locations": [
        {"line": 1, "column": 1}
      ],
      "path": ["query groupIterationCadences"],
      "extensions": {"code": "variableNotUsed", "variableName": "beforeCursor"}
    },
    {
      "message": "Variable \$afterCursor is declared by groupIterationCadences but not used",
      "locations": [
        {"line": 1, "column": 1}
      ],
      "path": ["query groupIterationCadences"],
      "extensions": {"code": "variableNotUsed", "variableName": "afterCursor"}
    },
    {
      "message": "Variable \$firstPageSize is declared by groupIterationCadences but not used",
      "locations": [
        {"line": 1, "column": 1}
      ],
      "path": ["query groupIterationCadences"],
      "extensions": {"code": "variableNotUsed", "variableName": "firstPageSize"}
    },
    {
      "message": "Variable \$lastPageSize is declared by groupIterationCadences but not used",
      "locations": [
        {"line": 1, "column": 1}
      ],
      "path": ["query groupIterationCadences"],
      "extensions": {"code": "variableNotUsed", "variableName": "lastPageSize"}
    }
  ]
};

var cadencesRequestResponseDataWithErrorsData = {
  "data": {
    "workspace": {"id": "gid://gitlab/Group/123454", "iterationCadences": null}
  },
  "errors": [
    {
      "message": "The resource that you are attempting to access does not exist or you don't have permission to perform this action",
      "locations": [
        {"line": 4, "column": 5}
      ],
      "path": ["workspace", "iterationCadences"]
    }
  ]
};

var iterationsRequestResponseData = {
  "data": {
    "group": {
      "id": "gid://gitlab/Group/88966",
      "iterations": {
        "nodes": [
          {
            "id": "gid://gitlab/Iteration/1608",
            "dueDate": "2023-04-09",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1608",
            "startDate": "2023-04-03",
            "state": "upcoming",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1608"
          },
          {
            "id": "gid://gitlab/Iteration/1582",
            "dueDate": "2023-04-02",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1582",
            "startDate": "2023-03-27",
            "state": "upcoming",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1582"
          },
          {
            "id": "gid://gitlab/Iteration/1551",
            "dueDate": "2023-03-26",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1551",
            "startDate": "2023-03-20",
            "state": "current",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1551"
          },
          {
            "id": "gid://gitlab/Iteration/1496",
            "dueDate": "2023-03-19",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1496",
            "startDate": "2023-03-13",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1496"
          },
          {
            "id": "gid://gitlab/Iteration/1435",
            "dueDate": "2023-03-12",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1435",
            "startDate": "2023-03-06",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1435"
          },
          {
            "id": "gid://gitlab/Iteration/1385",
            "dueDate": "2023-03-05",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1385",
            "startDate": "2023-02-27",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1385"
          },
          {
            "id": "gid://gitlab/Iteration/1361",
            "dueDate": "2023-02-26",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1361",
            "startDate": "2023-02-20",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1361"
          },
          {
            "id": "gid://gitlab/Iteration/1323",
            "dueDate": "2023-02-19",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1323",
            "startDate": "2023-02-13",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1323"
          },
          {
            "id": "gid://gitlab/Iteration/1298",
            "dueDate": "2023-02-12",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1298",
            "startDate": "2023-02-06",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1298"
          },
          {
            "id": "gid://gitlab/Iteration/1282",
            "dueDate": "2023-02-05",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1282",
            "startDate": "2023-01-30",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1282"
          },
          {
            "id": "gid://gitlab/Iteration/1281",
            "dueDate": "2023-01-29",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1281",
            "startDate": "2023-01-23",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1281"
          },
          {
            "id": "gid://gitlab/Iteration/1280",
            "dueDate": "2023-01-22",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1280",
            "startDate": "2023-01-16",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1280"
          },
          {
            "id": "gid://gitlab/Iteration/1279",
            "dueDate": "2023-01-15",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1279",
            "startDate": "2023-01-09",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1279"
          },
          {
            "id": "gid://gitlab/Iteration/1278",
            "dueDate": "2023-01-08",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1278",
            "startDate": "2023-01-02",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1278"
          },
          {
            "id": "gid://gitlab/Iteration/1277",
            "dueDate": "2023-01-01",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1277",
            "startDate": "2022-12-26",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1277"
          },
          {
            "id": "gid://gitlab/Iteration/1276",
            "dueDate": "2022-12-25",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1276",
            "startDate": "2022-12-19",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1276"
          },
          {
            "id": "gid://gitlab/Iteration/1275",
            "dueDate": "2022-12-18",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1275",
            "startDate": "2022-12-12",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1275"
          },
          {
            "id": "gid://gitlab/Iteration/918",
            "dueDate": "2022-12-11",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/918",
            "startDate": "2022-12-05",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/918"
          },
          {
            "id": "gid://gitlab/Iteration/917",
            "dueDate": "2022-12-04",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/917",
            "startDate": "2022-11-28",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/917"
          },
        ],
        "pageInfo": {"hasNextPage": false, "hasPreviousPage": false, "startCursor": "MQ", "endCursor": "MjA"}
      }
    }
  }
};
