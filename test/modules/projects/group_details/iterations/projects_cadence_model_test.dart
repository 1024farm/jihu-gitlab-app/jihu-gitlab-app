import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/iterations/projects_cadence_model.dart';

void main() {
  test('Should get cadenceGraphql as expect', () {
    expect(cadenceGraphql("fullPath", 20, ""), {
      "operationName": "projectIterationCadences",
      "variables": {"beforeCursor": "", "fullPath": "fullPath", "firstPageSize": 20, "afterCursor": ""},
      "query": """
query projectIterationCadences(\$fullPath: ID!, \$beforeCursor: String = "", \$afterCursor: String = "", \$firstPageSize: Int, \$lastPageSize: Int) {
workspace: project(fullPath: \$fullPath) {
  id
  iterationCadences(
    includeAncestorGroups: true
    before: \$beforeCursor
    after: \$afterCursor
    first: \$firstPageSize
    last: \$lastPageSize
  ) {
    nodes {
      id
      title
      durationInWeeks
      automatic
    }
    pageInfo {
      ...PageInfo
    }
  }
}
}

fragment PageInfo on PageInfo {
hasNextPage
hasPreviousPage
startCursor
endCursor
}

      """
    });
  });
}
