import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/iterations/subgroup_cadence_iteration_model.dart';
import 'package:mockito/mockito.dart';

import '../../../../core/net/http_request_test.mocks.dart';

void main() {
  final client = MockHttpClient();

  setUp(() {
    HttpClient.injectInstanceForTesting(client);
  });

  test('Should get cadenceIterationGraphql as expect', () {
    expect(subgroupCadenceIterationGraphql("fullPath", "id", 20, ""), {
      "operationName": "groupIterations",
      "variables": {"fullPath": "fullPath", "iterationCadenceId": "id", "firstPageSize": 20, "state": "all", "sort": "CADENCE_AND_DUE_DATE_DESC", "afterCursor": ""},
      "query": """
      query groupIterations(\$fullPath: ID!, \$iterationCadenceId: IterationsCadenceID!, \$state: IterationState!, \$sort: IterationSort, \$afterCursor: String, \$firstPageSize: Int) {
        group(fullPath: \$fullPath) {
          id
          iterations(
            iterationCadenceIds: [\$iterationCadenceId]
            state: \$state
            sort: \$sort
            after: \$afterCursor
            first: \$firstPageSize
          ) {
            nodes {
              ...IterationListItem
            }
            pageInfo {
              ...PageInfo
            }
          }
        }
      }
      
      fragment PageInfo on PageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
      
      fragment IterationListItem on Iteration {
        id
        dueDate
        scopedPath
        startDate
        state
        title
        webPath
      }
    """
    });
  });

  group('Subgroup iteration model', () {
    test('Should refresh data success', () async {
      when(client.post('/api/graphql', subgroupCadenceIterationGraphql("ultimate-plan/jihu-gitlab-app", "gid://gitlab/Iterations::Cadence/316", 20, "")))
          .thenAnswer((_) => Future(() => Response.of(response1)));

      SubgroupCadenceIterationModel model = SubgroupCadenceIterationModel();
      var result = await model.refresh(fullPath: 'ultimate-plan/jihu-gitlab-app', iterationCadenceId: 'gid://gitlab/Iterations::Cadence/316');
      expect(result, true);
      expect(model.loadState, LoadState.successState);
      Map<String, dynamic> iterations = response1['data']!['group']!['iterations'] as Map<String, dynamic>;
      List<Map<String, dynamic>> nodes = iterations['nodes'];
      expect(model.iterations.length, nodes.length);
      expect(model.iterations.first.id, nodes.first['id']);
      expect(model.hasNextPage, false);

      expect(model.iterationsPageInfo?.endCursor, "MjA");
    });

    test('Should refresh data fail', () async {
      when(client.post<List<dynamic>>('/api/graphql', subgroupCadenceIterationGraphql("ultimate-plan/jihu-gitlab-app", "gid://gitlab/Iterations::Cadence/316", 20, "")))
          .thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));

      SubgroupCadenceIterationModel model = SubgroupCadenceIterationModel();
      var result = await model.refresh(fullPath: 'ultimate-plan/jihu-gitlab-app', iterationCadenceId: 'gid://gitlab/Iterations::Cadence/316');
      expect(result, false);
      expect(model.loadState, LoadState.errorState);
    });

    test('Should load more data success', () async {
      when(client.post('/api/graphql', subgroupCadenceIterationGraphql("ultimate-plan/jihu-gitlab-app", "gid://gitlab/Iterations::Cadence/316", 20, "")))
          .thenAnswer((_) => Future(() => Response.of(response1)));
      when(client.post('/api/graphql', subgroupCadenceIterationGraphql("ultimate-plan/jihu-gitlab-app", "gid://gitlab/Iterations::Cadence/316", 20, "MjA")))
          .thenAnswer((_) => Future(() => Response.of(response2)));

      SubgroupCadenceIterationModel model = SubgroupCadenceIterationModel();
      var result1 = await model.refresh(fullPath: 'ultimate-plan/jihu-gitlab-app', iterationCadenceId: 'gid://gitlab/Iterations::Cadence/316');
      expect(result1, true);
      expect(model.loadState, LoadState.successState);

      var result2 = await model.loadMore();
      expect(result2, true);
      expect(model.hasNextPage, false);
      expect(model.iterationsPageInfo?.endCursor, '');
    });

    test('Should load more data fail', () async {
      when(client.post('/api/graphql', subgroupCadenceIterationGraphql("ultimate-plan/jihu-gitlab-app", "gid://gitlab/Iterations::Cadence/316", 20, "")))
          .thenAnswer((_) => Future(() => Response.of(response1)));
      when(client.post('/api/graphql', subgroupCadenceIterationGraphql("ultimate-plan/jihu-gitlab-app", "gid://gitlab/Iterations::Cadence/316", 20, "MjA")))
          .thenAnswer((_) => Future(() => Response.of([])));

      SubgroupCadenceIterationModel model = SubgroupCadenceIterationModel();
      var result1 = await model.refresh(fullPath: 'ultimate-plan/jihu-gitlab-app', iterationCadenceId: 'gid://gitlab/Iterations::Cadence/316');
      expect(result1, true);
      expect(model.loadState, LoadState.successState);
      var result2 = await model.loadMore();
      expect(result2, false);
    });
  });

  tearDown(() {
    reset(client);
  });
}

var response1 = {
  "data": {
    "group": {
      "id": "gid://gitlab/Group/88966",
      "iterations": {
        "nodes": [
          {
            "id": "gid://gitlab/Iteration/1608",
            "dueDate": "2023-04-09",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1608",
            "startDate": "2023-04-03",
            "state": "upcoming",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1608"
          },
          {
            "id": "gid://gitlab/Iteration/1582",
            "dueDate": "2023-04-02",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1582",
            "startDate": "2023-03-27",
            "state": "upcoming",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1582"
          },
          {
            "id": "gid://gitlab/Iteration/1551",
            "dueDate": "2023-03-26",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1551",
            "startDate": "2023-03-20",
            "state": "current",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1551"
          },
          {
            "id": "gid://gitlab/Iteration/1496",
            "dueDate": "2023-03-19",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1496",
            "startDate": "2023-03-13",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1496"
          },
          {
            "id": "gid://gitlab/Iteration/1435",
            "dueDate": "2023-03-12",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1435",
            "startDate": "2023-03-06",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1435"
          },
          {
            "id": "gid://gitlab/Iteration/1385",
            "dueDate": "2023-03-05",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1385",
            "startDate": "2023-02-27",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1385"
          },
          {
            "id": "gid://gitlab/Iteration/1361",
            "dueDate": "2023-02-26",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1361",
            "startDate": "2023-02-20",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1361"
          },
          {
            "id": "gid://gitlab/Iteration/1323",
            "dueDate": "2023-02-19",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1323",
            "startDate": "2023-02-13",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1323"
          },
          {
            "id": "gid://gitlab/Iteration/1298",
            "dueDate": "2023-02-12",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1298",
            "startDate": "2023-02-06",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1298"
          },
          {
            "id": "gid://gitlab/Iteration/1282",
            "dueDate": "2023-02-05",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1282",
            "startDate": "2023-01-30",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1282"
          },
          {
            "id": "gid://gitlab/Iteration/1281",
            "dueDate": "2023-01-29",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1281",
            "startDate": "2023-01-23",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1281"
          },
          {
            "id": "gid://gitlab/Iteration/1280",
            "dueDate": "2023-01-22",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1280",
            "startDate": "2023-01-16",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1280"
          },
          {
            "id": "gid://gitlab/Iteration/1279",
            "dueDate": "2023-01-15",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1279",
            "startDate": "2023-01-09",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1279"
          },
          {
            "id": "gid://gitlab/Iteration/1278",
            "dueDate": "2023-01-08",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1278",
            "startDate": "2023-01-02",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1278"
          },
          {
            "id": "gid://gitlab/Iteration/1277",
            "dueDate": "2023-01-01",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1277",
            "startDate": "2022-12-26",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1277"
          },
          {
            "id": "gid://gitlab/Iteration/1276",
            "dueDate": "2022-12-25",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1276",
            "startDate": "2022-12-19",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1276"
          },
          {
            "id": "gid://gitlab/Iteration/1275",
            "dueDate": "2022-12-18",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1275",
            "startDate": "2022-12-12",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1275"
          },
          {
            "id": "gid://gitlab/Iteration/918",
            "dueDate": "2022-12-11",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/918",
            "startDate": "2022-12-05",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/918"
          },
          {
            "id": "gid://gitlab/Iteration/917",
            "dueDate": "2022-12-04",
            "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/917",
            "startDate": "2022-11-28",
            "state": "closed",
            "title": null,
            "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/917"
          },
        ],
        "pageInfo": {"hasNextPage": false, "hasPreviousPage": false, "startCursor": "MQ", "endCursor": "MjA"}
      }
    }
  }
};

var response2 = {
  "data": {
    "group": {
      "id": "gid://gitlab/Group/88966",
      "iterations": {
        "nodes": [],
        "pageInfo": {"hasNextPage": false, "hasPreviousPage": false, "startCursor": null, "endCursor": null}
      }
    }
  }
};
