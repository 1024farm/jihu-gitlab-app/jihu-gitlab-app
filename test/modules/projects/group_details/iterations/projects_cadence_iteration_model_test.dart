import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/iterations/projects_cadence_iteration_model.dart';

void main() {
  test('Should get cadenceIterationGraphql as expect', () {
    expect(cadenceIterationGraphql("fullPath", "id", 20, ""), {
      "operationName": "projectIterations",
      "variables": {"fullPath": "fullPath", "iterationCadenceId": "id", "firstPageSize": 20, "state": "all", "sort": "CADENCE_AND_DUE_DATE_DESC", "afterCursor": ""},
      "query": """
      query projectIterations(\$fullPath: ID!, \$iterationCadenceId: IterationsCadenceID!, \$state: IterationState!, \$sort: IterationSort, \$beforeCursor: String,\$afterCursor: String, \$firstPageSize: Int, \$lastPageSize: Int) {
        workspace: project(fullPath: \$fullPath) {
          id
          iterations(
            iterationCadenceIds: [\$iterationCadenceId]
            state: \$state
            sort: \$sort
            before: \$beforeCursor
            after: \$afterCursor
            first: \$firstPageSize
            last: \$lastPageSize
          ) {
            nodes {
              ...IterationListItem
            }
            pageInfo {
              ...PageInfo
            }
          }
        }
      }
      
      fragment PageInfo on PageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
      
      fragment IterationListItem on Iteration {
        dueDate
        id
        scopedPath
        startDate
        state
        title
        webPath
      }
    """
    });
  });
}
