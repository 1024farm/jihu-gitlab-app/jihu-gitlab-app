import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/iterations/subgroup_cadence_model.dart';
import 'package:mockito/mockito.dart';

import '../../../../core/net/http_request_test.mocks.dart';

void main() {
  test('Should get groupIterationCadenceGraphql as expect', () {
    expect(groupIterationCadenceGraphql("fullPath", 20, ""), {
      "operationName": "groupIterationCadences",
      "variables": {"beforeCursor": "", "fullPath": "fullPath", "firstPageSize": 20, "afterCursor": ""},
      "query": """
      query groupIterationCadences(\$fullPath: ID!, \$beforeCursor: String = "", \$afterCursor: String = "", \$firstPageSize: Int, \$lastPageSize: Int) {
        workspace: group(fullPath: \$fullPath) {
          id
          iterationCadences(
            includeAncestorGroups: true
            before: \$beforeCursor
            after: \$afterCursor
            first: \$firstPageSize
            last: \$lastPageSize
          ) {
            nodes {
              id
              title
              durationInWeeks
              automatic
            }
            pageInfo {
              ...PageInfo
            }
          }
        }
      }
      
      fragment PageInfo on PageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
      """
    });
  });

  final client = MockHttpClient();
  setUp(() {
    HttpClient.injectInstanceForTesting(client);
  });

  var cadencesRequestParams = groupIterationCadenceGraphql("ultimate-plan/jihu-gitlab-app", 20, "");
  group('Subgroup iteration model', () {
    test('Should refresh data success', () async {
      var cadencesRequestResponseData = {
        "data": {
          "workspace": {
            "id": "gid://gitlab/Group/88966",
            "iterationCadences": {
              "nodes": [
                {"id": "gid://gitlab/Iterations::Cadence/316", "title": "单周迭代", "durationInWeeks": 1, "automatic": true, "__typename": "IterationCadence"}
              ],
              "pageInfo": {"hasNextPage": false, "hasPreviousPage": false, "startCursor": "MQ", "endCursor": "MQ", "__typename": "PageInfo"},
              "__typename": "IterationCadenceConnection"
            },
            "__typename": "Group"
          }
        }
      };
      when(client.post('/api/graphql', cadencesRequestParams)).thenAnswer((_) => Future(() => Response.of(cadencesRequestResponseData)));

      SubgroupCadenceModel model = SubgroupCadenceModel(fullPath: "ultimate-plan/jihu-gitlab-app");

      var result = await model.refresh();
      expect(result, true);
      expect(model.loadState, LoadState.successState);
      Map<String, dynamic> iterations = cadencesRequestResponseData['data']!['workspace']!['iterationCadences'] as Map<String, dynamic>;
      List<Map<String, dynamic>> nodes = iterations['nodes'];
      expect(model.cadences.length, nodes.length);
      expect(model.cadences.first.id, nodes.first['id']);
      expect(model.hasNextPage, false);

      expect(model.cadencesPageInfo?.endCursor, "MQ");
    });

    test('Should refresh data fail with empty response', () async {
      when(client.post('/api/graphql', cadencesRequestParams)).thenAnswer((_) => Future(() => Response.of({
            "data": {
              "workspace": {"id": "gid://gitlab/Group/88966", "iterationCadences": {}, "__typename": "Group"}
            }
          })));

      SubgroupCadenceModel model = SubgroupCadenceModel(fullPath: "ultimate-plan/jihu-gitlab-app");
      var result = await model.refresh();
      expect(result, false);
      expect(model.loadState, LoadState.errorState);
    });

    test('Should refresh data fail with authorizedDeniedState', () async {
      var cadencesRequestResponseNoData = {
        "data": {
          "workspace": {"id": "gid://gitlab/Group/88966", "iterationCadences": null}
        }
      };
      when(client.post('/api/graphql', cadencesRequestParams)).thenAnswer((_) => Future(() => Response.of(cadencesRequestResponseNoData)));
      SubgroupCadenceModel model = SubgroupCadenceModel(fullPath: "ultimate-plan/jihu-gitlab-app");
      var result = await model.refresh();
      expect(result, false);
      expect(model.loadState, LoadState.authorizedDeniedState);
    });

    test('Should load more data success', () async {
      var cadencesRequestResponseData = {
        "data": {
          "workspace": {
            "id": "gid://gitlab/Group/88966",
            "iterationCadences": {
              "nodes": [
                {"id": "gid://gitlab/Iterations::Cadence/316", "title": "单周迭代", "durationInWeeks": 1, "automatic": true, "__typename": "IterationCadence"}
              ],
              "pageInfo": {"hasNextPage": false, "hasPreviousPage": false, "startCursor": "MQ", "endCursor": "MQ", "__typename": "PageInfo"},
              "__typename": "IterationCadenceConnection"
            },
            "__typename": "Group"
          }
        }
      };
      when(client.post('/api/graphql', cadencesRequestParams)).thenAnswer((_) => Future(() => Response.of(cadencesRequestResponseData)));
      var cadencesNextPageRequestParams = groupIterationCadenceGraphql("ultimate-plan/jihu-gitlab-app", 20, "MQ");
      var cadencesNextPageRequestResponseNoData = {
        "data": {
          "workspace": {
            "id": "gid://gitlab/Group/88966",
            "iterationCadences": {
              "nodes": [],
              "pageInfo": {"hasNextPage": false, "hasPreviousPage": true, "startCursor": null, "endCursor": null, "__typename": "PageInfo"},
              "__typename": "IterationCadenceConnection"
            },
            "__typename": "Group"
          }
        }
      };
      when(client.post('/api/graphql', cadencesNextPageRequestParams)).thenAnswer((_) => Future(() => Response.of(cadencesNextPageRequestResponseNoData)));

      SubgroupCadenceModel model = SubgroupCadenceModel(fullPath: "ultimate-plan/jihu-gitlab-app");
      var result1 = await model.refresh();
      expect(result1, true);
      expect(model.loadState, LoadState.successState);
      var result2 = await model.loadMore();
      expect(result2, true);
      expect(model.hasNextPage, false);
      expect(model.cadencesPageInfo?.endCursor, '');
    });

    test('Should load more data fail', () async {
      var cadencesRequestResponseData = {
        "data": {
          "workspace": {
            "id": "gid://gitlab/Group/88966",
            "iterationCadences": {
              "nodes": [
                {"id": "gid://gitlab/Iterations::Cadence/316", "title": "单周迭代", "durationInWeeks": 1, "automatic": true, "__typename": "IterationCadence"}
              ],
              "pageInfo": {"hasNextPage": false, "hasPreviousPage": false, "startCursor": "MQ", "endCursor": "MQ", "__typename": "PageInfo"},
              "__typename": "IterationCadenceConnection"
            },
            "__typename": "Group"
          }
        }
      };
      when(client.post('/api/graphql', cadencesRequestParams)).thenAnswer((_) => Future(() => Response.of(cadencesRequestResponseData)));
      var cadencesNextPageRequestParams = groupIterationCadenceGraphql("ultimate-plan/jihu-gitlab-app", 20, "MQ");
      when(client.post('/api/graphql', cadencesNextPageRequestParams)).thenAnswer((_) => Future(() => Response.of({})));

      SubgroupCadenceModel model = SubgroupCadenceModel(fullPath: "ultimate-plan/jihu-gitlab-app");

      var result1 = await model.refresh();
      expect(result1, true);
      expect(model.loadState, LoadState.successState);

      var result2 = await model.loadMore();
      expect(result2, false);
    });
  });

  tearDown(() {
    reset(client);
  });
}
