import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_model.dart';
import 'package:jihu_gitlab_app/modules/issues/list/project_issues_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/projects.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/project_repository_model.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../core/net/http_request_test.mocks.dart';
import '../../../../integration_tests/go_into_sub_group_page_test.mocks.dart';
import '../../../../mocker/tester.dart';
import '../../../../test_data/issue.dart';
import '../../../../test_data/note.dart';
import '../../../../test_data/project.dart';
import '../../../issues/details/issue_details_page_test.mocks.dart';
import '../../repository/response_data.dart';
import '../../starred/projects_starred_page_test.mocks.dart';

List<dynamic> subgroupItems = [
  {
    "id": 120181,
    "name": "上线审批组",
    "description": "",
    "visibility": "public",
    "full_name": "旗舰版演示 / 极狐 GitLab App 产品线 / 上线审批组",
    "created_at": "2022-11-14T08:09:54.048+08:00",
    "updated_at": "2022-11-14T08:09:54.048+08:00",
    "avatar_url": null,
    "type": "group",
    "can_edit": false,
    "edit_path": "/groups/ultimate-plan/jihu-gitlab-app/deployment-approvers/-/edit",
    "full_path": "/ultimate-plan/jihu-gitlab-app/deployment-approvers",
    "permission": null,
    "children_count": 0,
    "parent_id": 88966,
    "subgroup_count": 0,
    "project_count": 0,
    "leave_path": "/groups/ultimate-plan/jihu-gitlab-app/deployment-approvers/-/group_members/leave",
    "can_leave": false,
    "can_remove": false,
    "number_users_with_delimiter": "1",
    "markdown_description": "",
    "marked_for_deletion": false
  }
];

final client = MockHttpClient();

void main() {
  var subgroupListModel = SubgroupListModel();
  locator.registerSingleton(subgroupListModel);
  late MockProjectsStarredProvider starredProvider;
  var projectsStarredModel = ProjectsStarredModel();
  locator.registerSingleton(projectsStarredModel);
  starredProvider = MockProjectsStarredProvider();
  projectsStarredModel.injectDataProviderForTesting(starredProvider);
  var provider = MockSubgroupAndProjectProvider();
  subgroupListModel.injectDataProviderForTesting(provider);

  var issueDetailsModel = IssueDetailsModel();
  locator.registerSingleton(issueDetailsModel);
  var discussionProvider = MockDiscussionProvider();
  issueDetailsModel.injectDataProviderForTesting(discussionProvider);
  when(discussionProvider.loadFromLocal()).thenAnswer((_) => Future(() => [Discussion('abc123', noteList, individualNote: true)]));

  group('Sub Groups List Page', () {
    testWidgets('Should display No Data View', (tester) async {
      ConnectionProvider().reset(Tester.jihuLabUser());

      when(provider.loadFromLocal()).thenAnswer((_) => Future(() => []));
      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
          child: const MaterialApp(
            home: MaterialApp(
              home: Scaffold(
                body: SubgroupListPage(relativePath: 'relativePath', groupId: 0),
              ),
            ),
          )));

      await tester.pumpAndSettle();
      expect(find.byType(TipsView), findsOneWidget);
      ConnectionProvider().fullReset();
    });

    testWidgets('Should display List View', (tester) async {
      when(provider.loadFromLocal())
          .thenAnswer((_) => Future(() => [GroupAndProject(1, 120181, "上线审批组", SubgroupItemType.subgroup, "ultimate-plan/jihu-gitlab-app/deployment-approvers", 0, starred: false)]));
      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
          child: MaterialApp(
            routes: {
              ProjectPage.routeName: (context) => const ProjectPage(arguments: {"name": "name1", "projectId": 1, "relativePath": "relativePath"})
            },
            home: const Scaffold(
              body: SubgroupListPage(relativePath: 'relativePath', groupId: 0),
            ),
          )));

      await tester.pumpAndSettle();
      expect(find.byType(ListView), findsOneWidget);
      ConnectionProvider().fullReset();
    });

    testWidgets('Should able to go in to sub group page', (tester) async {
      when(provider.loadFromLocal()).thenAnswer((_) => Future(() => [
            GroupAndProject(1, 120181, "上线审批组", SubgroupItemType.subgroup, "ultimate-plan/jihu-gitlab-app/deployment-approvers", 0, starred: false),
            GroupAndProject(2, 75468, "API", SubgroupItemType.project, "ultimate-plan/jihu-gitlab-app/api", 0, starred: false)
          ]));
      await tester.pumpWidget(MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
        child: MaterialApp(
          routes: {
            ProjectPage.routeName: (context) => const ProjectPage(arguments: {"name": "name1", "projectId": 1, "relativePath": "relativePath"}),
            SubgroupPage.routeName: (context) => const SubgroupPage(arguments: {"name": "上线审批组", "groupId": 120181, "relativePath": "aaa"})
          },
          home: const Scaffold(
            body: SubgroupListPage(relativePath: 'relativePath', groupId: 0),
          ),
        ),
      ));
      await tester.pumpAndSettle();
      await tester.tap(find.text('上线审批组'));
      await tester.pumpAndSettle(const Duration(seconds: 1));
      expect(find.text('上线审批组'), findsNWidgets(2));
      ConnectionProvider().fullReset();
    });

    testWidgets('Should go in to project page empty view when no data response', (tester) async {
      SharedPreferences.setMockInitialValues(<String, Object>{});
      await LocalStorage.init();
      ConnectionProvider().reset(Tester.jihuLabUser());
      ConnectionProvider().reset(Tester.jihuLabUser());

      when(provider.loadFromLocal())
          .thenAnswer((_) => Future(() => [GroupAndProject(1, 59893, "极狐 GitLab APP 代码", SubgroupItemType.project, "uultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 0, starred: false)]));

      when(client.post<dynamic>('/api/graphql', buildRequestBodyOfCheckRepositoryIsEmpty("relativePath")))
          .thenAnswer((_) => Future(() => Response.of<dynamic>(checkRepositoryIsEmptyResponse(exists: false))));
      when(client.post<dynamic>('/api/graphql', argThat(predicate((arg) => !arg.toString().contains("getRepository"))))).thenAnswer((_) => Future(() => Response.of<dynamic>({
            'data': {
              'project': {
                "issues": {
                  "pageInfo": {
                    "hasNextPage": true,
                    "startCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMi0xMi0yMiAxMjoyODozMS4yMzk4MzYwMDAgKzA4MDAiLCJpZCI6IjI5NTMyOSJ9",
                    "endCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMi0xMi0wMSAxOTo0MzoxNi44MjEzODIwMDAgKzA4MDAiLCJpZCI6IjI3NjA0OSJ9"
                  },
                  "nodes": []
                }
              }
            }
          })));
      when(client.get<List<dynamic>>("/api/v4/projects/0/issues/0/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
      HttpClient.injectInstanceForTesting(client);
      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
          child: MaterialApp(
            routes: {
              ProjectPage.routeName: (context) => const ProjectPage(arguments: {"name": "极狐 GitLab APP 代码", "projectId": 59893, "relativePath": "relativePath"})
            },
            home: const Scaffold(
              body: SubgroupListPage(relativePath: 'relativePath', groupId: 0),
            ),
          )));

      await tester.pumpAndSettle();
      expect(find.text('极狐 GitLab APP 代码'), findsOneWidget);

      await tester.tap(find.text('极狐 GitLab APP 代码'));
      await tester.pumpAndSettle(const Duration(seconds: 1));
      expect(find.widgetWithText(Tab, "Issues"), findsOneWidget);
      await tester.tap(find.widgetWithText(Tab, "Issues"));
      await tester.pumpAndSettle();
      expect(find.text('No data'), findsOneWidget);
      ConnectionProvider().fullReset();
    });

    testWidgets('Should go in to project page view', (tester) async {
      SharedPreferences.setMockInitialValues(<String, Object>{});
      await LocalStorage.init();
      ConnectionProvider().reset(Tester.jihuLabUser());
      ConnectionProvider().reset(Tester.jihuLabUser());

      when(client.get("/api/v4/projects/59893")).thenAnswer((_) => Future(() => Response.of(jiHuProjectData)));
      when(client.post<dynamic>('/api/graphql', getProjectIssuesGraphQLRequestBody('relativePath', '', 20))).thenAnswer((_) => Future(() => Response.of<dynamic>(projectIssuesGraphQLResponseData)));
      when(client.post<dynamic>('/api/graphql', getProjectIssuesGraphQLRequestBody('relativePath', 'a', 20))).thenAnswer((_) => Future(() => Response.of<dynamic>(projectIssuesGraphQLResponseData)));
      when(client.post<dynamic>('/api/graphql', buildRequestBodyOfCheckRepositoryIsEmpty("relativePath")))
          .thenAnswer((_) => Future(() => Response.of<dynamic>(checkRepositoryIsEmptyResponse(empty: true))));
      when(provider.loadFromLocal())
          .thenAnswer((_) => Future(() => [GroupAndProject(1, 59893, "极狐 GitLab APP 代码", SubgroupItemType.project, "uultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 0, starred: false)]));
      when(client.get<List<dynamic>>("/api/v4/projects/0/issues/0/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));

      HttpClient.injectInstanceForTesting(client);

      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
          child: MaterialApp(
            routes: {
              ProjectPage.routeName: (context) => const ProjectPage(arguments: {"name": "极狐 GitLab APP 代码", "projectId": 59893, "relativePath": "relativePath"})
            },
            home: const Scaffold(
              body: SubgroupListPage(relativePath: 'relativePath', groupId: 0),
            ),
          )));

      await tester.pumpAndSettle();
      expect(find.text('极狐 GitLab APP 代码'), findsOneWidget);

      await tester.tap(find.text('极狐 GitLab APP 代码'));
      await tester.pumpAndSettle(const Duration(seconds: 1));
      expect(find.widgetWithText(Tab, "Issues"), findsOneWidget);
      await tester.tap(find.widgetWithText(Tab, "Issues"));
      await tester.pumpAndSettle();
      expect(find.byType(TextField), findsOneWidget);
      await tester.enterText(find.byType(TextField), 'a');
      await tester.pumpAndSettle();
      await tester.tap(find.byKey(const Key('close')));
      await tester.pumpAndSettle(const Duration(seconds: 1));
      expect(find.text('Search'), findsOneWidget);
      await tester.enterText(find.byType(TextField), 'a');
      await tester.testTextInput.receiveAction(TextInputAction.done);
      await tester.pumpAndSettle(const Duration(seconds: 1));
      expect(find.text('Feature：交互优化：创建议题时，如果只有一个项目，则跳过「选择项目」'), findsOneWidget);
      ConnectionProvider().fullReset();
    });

    testWidgets('Should display empty project page view', (tester) async {
      SharedPreferences.setMockInitialValues(<String, Object>{});
      await LocalStorage.init();
      ConnectionProvider().reset(Tester.jihuLabUser());
      ConnectionProvider().reset(Tester.jihuLabUser());
      when(client.post<dynamic>('/api/graphql', getProjectIssuesGraphQLRequestBody('relativePath', '', 20))).thenAnswer((_) => Future(() => Response.of<dynamic>({
            'data': {
              'project': {
                "issues": {
                  "pageInfo": {
                    "hasNextPage": true,
                    "startCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMi0xMi0yMiAxMjoyODozMS4yMzk4MzYwMDAgKzA4MDAiLCJpZCI6IjI5NTMyOSJ9",
                    "endCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMi0xMi0wMSAxOTo0MzoxNi44MjEzODIwMDAgKzA4MDAiLCJpZCI6IjI3NjA0OSJ9"
                  },
                  "nodes": []
                }
              }
            }
          })));
      when(client.post<dynamic>('/api/graphql', getProjectIssuesGraphQLRequestBody('relativePath', 'a', 20))).thenAnswer((_) => Future(() => Response.of<dynamic>({
            'data': {
              'project': {
                "issues": {
                  "pageInfo": {
                    "hasNextPage": true,
                    "startCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMi0xMi0yMiAxMjoyODozMS4yMzk4MzYwMDAgKzA4MDAiLCJpZCI6IjI5NTMyOSJ9",
                    "endCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMi0xMi0wMSAxOTo0MzoxNi44MjEzODIwMDAgKzA4MDAiLCJpZCI6IjI3NjA0OSJ9"
                  },
                  "nodes": []
                }
              }
            }
          })));

      when(provider.loadFromLocal())
          .thenAnswer((_) => Future(() => [GroupAndProject(1, 59893, "极狐 GitLab APP 代码", SubgroupItemType.project, "uultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 0, starred: false)]));
      when(client.post<dynamic>('/api/graphql', buildRequestBodyOfCheckRepositoryIsEmpty("relativePath")))
          .thenAnswer((_) => Future(() => Response.of<dynamic>(checkRepositoryIsEmptyResponse(empty: true))));
      when(client.get<List<dynamic>>("/api/v4/projects/0/issues/0/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));

      HttpClient.injectInstanceForTesting(client);
      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
          child: MaterialApp(
            routes: {
              ProjectPage.routeName: (context) => const ProjectPage(arguments: {"name": "极狐 GitLab APP 代码", "projectId": 59893, "relativePath": "relativePath"})
            },
            home: const Scaffold(
              body: SubgroupListPage(relativePath: 'relativePath', groupId: 0),
            ),
          )));

      await tester.pumpAndSettle();
      expect(find.text('极狐 GitLab APP 代码'), findsOneWidget);

      await tester.tap(find.text('极狐 GitLab APP 代码'));
      await tester.pumpAndSettle(const Duration(seconds: 1));
      expect(find.widgetWithText(Tab, "Issues"), findsOneWidget);
      await tester.tap(find.widgetWithText(Tab, "Issues"));
      await tester.pumpAndSettle();
      expect(find.byType(TextField), findsOneWidget);
      await tester.enterText(find.byType(TextField), 'a');
      await tester.pumpAndSettle();
      await tester.tap(find.byKey(const Key('close')));
      await tester.pumpAndSettle(const Duration(seconds: 1));
      expect(find.text('Search'), findsOneWidget);
      await tester.enterText(find.byType(TextField), 'a');
      await tester.testTextInput.receiveAction(TextInputAction.done);
      await tester.pumpAndSettle(const Duration(seconds: 1));
      expect(find.text('No matches for \'a\''), findsOneWidget);
      ConnectionProvider().fullReset();
    });

    tearDown(() => ProjectProvider().fullReset());
  });

  tearDown(() {
    reset(client);
  });
}

var projectList = [
  {
    "id": 287556,
    "iid": 227,
    "project_id": 59893,
    "title": "Feature: 用户在创建议题时选择描述模版",
    "description": null,
    "state": "opened",
    "created_at": "2022-12-09T09:22:44.147+08:00",
    "updated_at": "2022-12-09T10:21:40.450+08:00",
    "closed_at": null,
    "closed_by": null,
    "labels": ["S::ITERATION BACKLOG"],
    "milestone": null,
    "assignees": [],
    "author": {
      "id": 23836,
      "username": "jojo0",
      "name": "yajie xue",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/23836/avatar.png",
      "web_url": "https://jihulab.com/jojo0"
    },
    "type": "ISSUE",
    "assignee": null,
    "user_notes_count": 0,
    "merge_requests_count": 0,
    "upvotes": 0,
    "downvotes": 0,
    "due_date": null,
    "confidential": false,
    "discussion_locked": null,
    "issue_type": "issue",
    "web_url": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/issues/227",
    "time_stats": {"time_estimate": 0, "total_time_spent": 0, "human_time_estimate": null, "human_total_time_spent": null},
    "task_completion_status": {"count": 0, "completed_count": 0},
    "weight": null,
    "blocking_issues_count": 2,
    "has_tasks": false,
    "_links": {
      "self": "https://jihulab.com/api/v4/projects/59893/issues/227",
      "notes": "https://jihulab.com/api/v4/projects/59893/issues/227/notes",
      "award_emoji": "https://jihulab.com/api/v4/projects/59893/issues/227/award_emoji",
      "project": "https://jihulab.com/api/v4/projects/59893",
      "closed_as_duplicate_of": null
    },
    "references": {"short": "#227", "relative": "#227", "full": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app#227"},
    "severity": "UNKNOWN",
    "moved_to_id": null,
    "service_desk_reply_to": null,
    "epic_iid": null,
    "epic": null,
    "iteration": {
      "id": 919,
      "iid": 10,
      "sequence": 5,
      "group_id": 88966,
      "title": null,
      "description": null,
      "state": 1,
      "created_at": "2022-11-15T14:38:41.939+08:00",
      "updated_at": "2022-11-15T14:38:41.939+08:00",
      "start_date": "2022-12-12",
      "due_date": "2022-12-18",
      "web_url": "https://jihulab.com/groups/ultimate-plan/jihu-gitlab-app/-/iterations/919"
    },
    "health_status": null
  }
];
