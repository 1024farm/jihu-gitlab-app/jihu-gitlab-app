import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/widgets/photo_picker.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_model.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/iterations/subgroup_cadence_iteration_model.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/iterations/subgroup_cadence_model.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/iterations/subgroup_iterations_page.dart';
import 'package:jihu_gitlab_app/modules/projects/projects.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';

import '../../../core/net/http_request_test.mocks.dart';
import '../../../mocker/tester.dart';

class _ParentPageForTestSubGroupPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        final params = <String, dynamic>{'groupId': 88966, 'name': "极狐 GitLab App 产品线", 'relativePath': 'relativePath'};
        Navigator.of(context).pushNamed(SubgroupPage.routeName, arguments: params);
      },
      child: const Text('PushToSubPage'),
    );
  }
}

void main() {
  final client = MockHttpClient();

  setUp(() async {
    setupLocator();
    ConnectionProvider().reset(Tester.jihuLabUser());
    HttpClient.injectInstanceForTesting(client);
  });

  group('Subgroup page', () {
    testWidgets('Should to Sub Groups page when click Children button', (tester) async {
      when(client.get<List<dynamic>>('/api/v4/groups/88966/subgroups?all_available=true&page=1&per_page=50')).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
      when(client.get<dynamic>('/api/v4/groups/88966?all_available=true&page=1&per_page=50')).thenAnswer((_) => Future(() => Response.of<dynamic>({'projects': []})));

      await tester.pumpWidget(MaterialApp(
        onGenerateRoute: onGenerateRoute,
        home: Scaffold(
          body: _ParentPageForTestSubGroupPage(),
        ),
      ));

      await tester.tap(find.text('PushToSubPage'));
      await tester.pump();
      await tester.pumpAndSettle();
      expect(find.byType(SubgroupPage), findsOneWidget);
      expect(find.text('Children'), findsOneWidget);

      await tester.tap(find.text('Children'));
      await tester.pumpAndSettle();
      expect(find.byType(SubgroupListPage), findsOneWidget);
    });

    testWidgets('Should to iterations page when click Iterations button', (tester) async {
      when(client.get<List<dynamic>>('/api/v4/groups/88966/subgroups?all_available=true&page=1&per_page=50')).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
      when(client.get<dynamic>('/api/v4/groups/88966?all_available=true&page=1&per_page=50')).thenAnswer((_) => Future(() => Response.of<dynamic>({'projects': []})));
      var subgroupCadenceIterationsResponse = {
        "data": {
          "group": {
            "id": "gid://gitlab/Group/88966",
            "iterations": {
              "nodes": [],
              "pageInfo": {"hasNextPage": false, "hasPreviousPage": false, "startCursor": null, "endCursor": null}
            }
          }
        }
      };
      when(client.post('/api/graphql', subgroupCadenceIterationGraphql("relativePath", "gid://gitlab/Iterations::Cadence/316", 20, "")))
          .thenAnswer((_) => Future(() => Response.of(subgroupCadenceIterationsResponse)));

      var groupIterationCadencesResponse = {
        "data": {
          "workspace": {
            "id": "gid://gitlab/Group/88966",
            "iterationCadences": {
              "nodes": [
                {"id": "gid://gitlab/Iterations::Cadence/316", "title": "单周迭代", "durationInWeeks": 1, "automatic": true, "__typename": "IterationCadence"}
              ],
              "pageInfo": {"hasNextPage": false, "hasPreviousPage": false, "startCursor": "MQ", "endCursor": "MQ", "__typename": "PageInfo"},
              "__typename": "IterationCadenceConnection"
            },
            "__typename": "Group"
          }
        }
      };
      when(client.post('/api/graphql', groupIterationCadenceGraphql('relativePath', 20, ""))).thenAnswer((_) => Future(() => Response.of(groupIterationCadencesResponse)));

      await tester.pumpWidget(MaterialApp(
        routes: {
          SubgroupPage.routeName: (context) => const SubgroupPage(arguments: {"name": "name1", "groupId": 88966, "relativePath": "relativePath"})
        },
        home: Scaffold(
          body: _ParentPageForTestSubGroupPage(),
        ),
      ));

      await tester.tap(find.text('PushToSubPage'));
      for (int i = 0; i < 5; i++) {
        await tester.pump(const Duration(seconds: 1));
      }
      expect(find.byType(SubgroupPage), findsOneWidget);
      expect(find.text('Iterations'), findsOneWidget);

      await tester.tap(find.text('Iterations'));
      for (int i = 0; i < 5; i++) {
        await tester.pump(const Duration(seconds: 1));
      }
      expect(find.byType(SubgroupIterationsPage), findsOneWidget);
    });
  });

  tearDown(() {
    reset(client);

    ConnectionProvider().fullReset();
    locator.unregister<SubgroupListModel>();
    locator.unregister<ProjectsGroupsModel>();
    locator.unregister<ProjectsStarredModel>();
    locator.unregister<IssueDetailsModel>();
    locator.unregister<PhotoPicker>();
  });
}

List<dynamic> subgroupItems = [
  {
    "id": 120181,
    "name": "上线审批组",
    "description": "",
    "visibility": "public",
    "full_name": "旗舰版演示 / 极狐 GitLab App 产品线 / 上线审批组",
    "created_at": "2022-11-14T08:09:54.048+08:00",
    "updated_at": "2022-11-14T08:09:54.048+08:00",
    "avatar_url": null,
    "type": "group",
    "can_edit": false,
    "edit_path": "/groups/ultimate-plan/jihu-gitlab-app/deployment-approvers/-/edit",
    "relative_path": "/ultimate-plan/jihu-gitlab-app/deployment-approvers",
    "permission": null,
    "children_count": 0,
    "parent_id": 88966,
    "subgroup_count": 0,
    "project_count": 0,
    "leave_path": "/groups/ultimate-plan/jihu-gitlab-app/deployment-approvers/-/group_members/leave",
    "can_leave": false,
    "can_remove": false,
    "number_users_with_delimiter": "1",
    "markdown_description": "",
    "marked_for_deletion": false
  },
  {
    "id": 75468,
    "name": "API",
    "description": null,
    "visibility": "public",
    "full_name": "旗舰版演示 / 极狐 GitLab App 产品线 / API",
    "created_at": "2022-11-29T18:56:34.526+08:00",
    "updated_at": "2022-11-29T18:56:35.390+08:00",
    "avatar_url": null,
    "type": "project",
    "can_edit": false,
    "edit_path": "/ultimate-plan/jihu-gitlab-app/api/edit",
    "relative_path": "/ultimate-plan/jihu-gitlab-app/api",
    "permission": null,
    "last_activity_at": "2022-11-29T18:56:34.526+08:00",
    "star_count": 0,
    "archived": false,
    "markdown_description": "",
    "marked_for_deletion": false,
    "compliance_management_framework": null
  },
  {
    "id": 59924,
    "name": "app demo vendor 1",
    "description": "",
    "visibility": "public",
    "full_name": "旗舰版演示 / 极狐 GitLab App 产品线 / app demo vendor 1",
    "created_at": "2022-10-10T16:46:32.305+08:00",
    "updated_at": "2022-11-29T15:23:35.389+08:00",
    "avatar_url": null,
    "type": "project",
    "can_edit": false,
    "edit_path": "/ultimate-plan/jihu-gitlab-app/demo/edit",
    "relative_path": "/ultimate-plan/jihu-gitlab-app/demo",
    "permission": null,
    "last_activity_at": "2022-11-29T15:23:35.389+08:00",
    "star_count": 0,
    "archived": false,
    "markdown_description": "",
    "marked_for_deletion": false,
    "compliance_management_framework": null
  },
  {
    "id": 69905,
    "name": "CI templates",
    "description": null,
    "visibility": "public",
    "full_name": "旗舰版演示 / 极狐 GitLab App 产品线 / CI templates",
    "created_at": "2022-11-11T14:40:09.227+08:00",
    "updated_at": "2022-11-30T22:23:18.846+08:00",
    "avatar_url": null,
    "type": "project",
    "can_edit": false,
    "edit_path": "/ultimate-plan/jihu-gitlab-app/ci-templates/edit",
    "relative_path": "/ultimate-plan/jihu-gitlab-app/ci-templates",
    "permission": null,
    "last_activity_at": "2022-11-30T22:23:18.846+08:00",
    "star_count": 0,
    "archived": false,
    "markdown_description": "",
    "marked_for_deletion": false,
    "compliance_management_framework": null
  },
  {
    "id": 69395,
    "name": "demo vendor 1",
    "description": "",
    "visibility": "public",
    "full_name": "旗舰版演示 / 极狐 GitLab App 产品线 / demo vendor 1",
    "created_at": "2022-11-10T12:32:40.141+08:00",
    "updated_at": "2022-11-17T18:16:13.352+08:00",
    "avatar_url": null,
    "type": "project",
    "can_edit": false,
    "edit_path": "/ultimate-plan/jihu-gitlab-app/demo-vendor-1/edit",
    "relative_path": "/ultimate-plan/jihu-gitlab-app/demo-vendor-1",
    "permission": null,
    "last_activity_at": "2022-11-17T18:16:13.352+08:00",
    "star_count": 0,
    "archived": false,
    "markdown_description": "",
    "marked_for_deletion": false,
    "compliance_management_framework": {
      "id": 142,
      "name": "Java 17 Maven",
      "description": "所有 Java 17 Maven 项目都用这个流水线",
      "color": "#00b140",
      "namespace_id": 14276,
      "pipeline_configuration_full_path": "Java-17-Maven.gitlab-ci.yml@ultimate-plan/jihu-gitlab-app/ci-templates",
      "created_at": "2022-10-25T13:37:33.206+08:00",
      "updated_at": "2022-11-11T15:08:44.627+08:00"
    }
  },
  {
    "id": 69920,
    "name": "demo vendor 2",
    "description": null,
    "visibility": "public",
    "full_name": "旗舰版演示 / 极狐 GitLab App 产品线 / demo vendor 2",
    "created_at": "2022-11-11T14:46:01.267+08:00",
    "updated_at": "2022-11-11T19:49:11.733+08:00",
    "avatar_url": null,
    "type": "project",
    "can_edit": false,
    "edit_path": "/ultimate-plan/jihu-gitlab-app/demo-vendor-2/edit",
    "relative_path": "/ultimate-plan/jihu-gitlab-app/demo-vendor-2",
    "permission": null,
    "last_activity_at": "2022-11-11T19:49:09.489+08:00",
    "star_count": 0,
    "archived": false,
    "markdown_description": "",
    "marked_for_deletion": false,
    "compliance_management_framework": {
      "id": 142,
      "name": "Java 17 Maven",
      "description": "所有 Java 17 Maven 项目都用这个流水线",
      "color": "#00b140",
      "namespace_id": 14276,
      "pipeline_configuration_full_path": "Java-17-Maven.gitlab-ci.yml@ultimate-plan/jihu-gitlab-app/ci-templates",
      "created_at": "2022-10-25T13:37:33.206+08:00",
      "updated_at": "2022-11-11T15:08:44.627+08:00"
    }
  },
  {
    "id": 70080,
    "name": "demo vendor 3",
    "description": null,
    "visibility": "public",
    "full_name": "旗舰版演示 / 极狐 GitLab App 产品线 / demo vendor 3",
    "created_at": "2022-11-11T15:53:21.336+08:00",
    "updated_at": "2022-11-27T15:32:21.631+08:00",
    "avatar_url": null,
    "type": "project",
    "can_edit": false,
    "edit_path": "/ultimate-plan/jihu-gitlab-app/demo-vendor-3/edit",
    "relative_path": "/ultimate-plan/jihu-gitlab-app/demo-vendor-3",
    "permission": null,
    "last_activity_at": "2022-11-17T18:34:50.513+08:00",
    "star_count": 1,
    "archived": false,
    "markdown_description": "",
    "marked_for_deletion": false,
    "compliance_management_framework": {
      "id": 143,
      "name": "Java 8 Maven",
      "description": "所有 Java 8 Maven 项目都用这个流水线",
      "color": "#ff0000",
      "namespace_id": 14276,
      "pipeline_configuration_full_path": "Java-8-Maven.gitlab-ci.yml@ultimate-plan/jihu-gitlab-app/ci-templates",
      "created_at": "2022-11-11T15:08:26.969+08:00",
      "updated_at": "2022-11-11T20:48:37.152+08:00"
    }
  },
  {
    "id": 59893,
    "name": "极狐 GitLab APP 代码",
    "description": "",
    "visibility": "public",
    "full_name": "旗舰版演示 / 极狐 GitLab App 产品线 / 极狐 GitLab APP 代码",
    "created_at": "2022-10-10T15:44:13.763+08:00",
    "updated_at": "2022-12-01T17:22:40.513+08:00",
    "avatar_url": null,
    "type": "project",
    "can_edit": false,
    "edit_path": "/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/edit",
    "relative_path": "/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app",
    "permission": null,
    "last_activity_at": "2022-12-01T17:22:40.513+08:00",
    "star_count": 2,
    "archived": false,
    "markdown_description": "",
    "marked_for_deletion": false,
    "compliance_management_framework": null
  }
];
