import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/widgets/charts/burn_chart_view.dart';
import 'package:jihu_gitlab_app/core/widgets/http_fail_view.dart';
import 'package:jihu_gitlab_app/core/widgets/round_rect_container.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/modules/epic/epic_issues_view.dart';
import 'package:jihu_gitlab_app/modules/epic/epic_progress_view.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_view.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/milestone_and_iteration_state_view.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestone_model.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestone_page.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestones_fetcher.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestones_page.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

import '../../../integration_tests/helper/core.dart';
import '../../../mocker/tester.dart';
import '../../../test_binding_setter.dart';

void main() {
  const String fullPath = "ultimate-plan/jihu-gitlab-app/demo";

  setUp(() async {
    TestWidgetsFlutterBinding.ensureInitialized();
    ConnectionProvider().reset(Tester.jihuLabUser());
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets('Should be able to display the empty data view', (WidgetTester tester) async {
    await setUpMobileBinding(tester);
    when(client.post<dynamic>('/api/graphql', requestBodyOfGetFirst100IssueByMilestoneTitle('ultimate-plan/jihu-gitlab-app/demo', "和家家户户口咯")))
        .thenAnswer((_) => Future(() => Response.of<dynamic>({})));
    Milestone milestone = Milestone.fromJson({"id": "gid://gitlab/Milestone/6706", "title": "和家家户户口咯", "startDate": null, "dueDate": "2023-03-31", "state": "active"});
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: Scaffold(body: ProjectMilestonePage(fullPath: fullPath, milestone: milestone))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.widgetWithText(MilestoneAndIterationStateView, "Open"), findsNothing);
    expect(find.text("expires on Mar 31, 2023"), findsNothing);
    expect(find.byType(TipsView), findsOneWidget);
  });

  testWidgets('Should be able to display the http failure view', (WidgetTester tester) async {
    await setUpMobileBinding(tester);
    when(client.post<dynamic>('/api/graphql', requestBodyOfGetFirst100IssueByMilestoneTitle('ultimate-plan/jihu-gitlab-app/demo', "和家家户户口咯"))).thenThrow(Exception());
    Milestone milestone = Milestone.fromJson({"id": "gid://gitlab/Milestone/6706", "title": "和家家户户口咯", "startDate": null, "dueDate": "2023-03-31", "state": "active"});
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: Scaffold(body: ProjectMilestonePage(fullPath: fullPath, milestone: milestone))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.widgetWithText(MilestoneAndIterationStateView, "Open"), findsNothing);
    expect(find.text("expires on Mar 31, 2023"), findsNothing);
    expect(find.byType(HttpFailView), findsOneWidget);
  });

  testWidgets('Should be able to display the issues and group by epic', (WidgetTester tester) async {
    await setUpMobileBinding(tester);
    when(client.get("/api/v4/groups/88966/iterations")).thenAnswer((_) => Future(() => Response.of({})));
    when(client.post<dynamic>('/api/graphql', projectMilestoneRequestBody('ultimate-plan/jihu-gitlab-app/demo', 20, "")))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(projectMilestonesResponse)));
    when(client.post<dynamic>('/api/graphql', requestBodyOfGetFirst100IssueByMilestoneTitle('ultimate-plan/jihu-gitlab-app/demo', "和家家户户口咯")))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(milestoneIssuesResponse)));
    when(client.post<dynamic>('/api/graphql', requestBodyOfGetFirst100IssueExtraByMilestoneTitle('ultimate-plan/jihu-gitlab-app/demo', "和家家户户口咯")))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(milestoneIssuesExtraResponse)));
    when(client.post<dynamic>('/api/graphql', requestBodyOfGetMilestoneTimeBoxReport('ultimate-plan/jihu-gitlab-app/demo', "gid://gitlab/Milestone/6706")))
        .thenAnswer((_) => Future(() => Response.of<dynamic>({
              "data": {
                "milestone": {
                  "report": {"burnupTimeSeries": []}
                }
              }
            })));
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: const Scaffold(body: ProjectMilestonesPage(fullPath: fullPath))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(RoundRectContainer), findsNWidgets(6));
    expect(find.text("和家家户户口咯"), findsOneWidget);
    expect(find.text("expires on Mar 31, 2023"), findsOneWidget);
    expect(find.text("Open"), findsNWidgets(5));
    expect(find.text("Closed"), findsOneWidget);
    expect(find.text("个h ju io jo i"), findsOneWidget);
    expect(find.text("Mar 24, 2023 - Mar 31, 2023"), findsOneWidget);
    expect(find.text("started on Mar 23, 2023"), findsOneWidget);

    await tester.tap(find.text("和家家户户口咯"));
    await tester.pumpAndSettle();
    expect(find.byType(ProjectMilestonePage), findsOneWidget);
    expect(find.widgetWithText(MilestoneAndIterationStateView, "Open"), findsOneWidget);
    expect(find.text("expires on Mar 31, 2023"), findsOneWidget);
    expect(find.widgetWithText(Tab, "Issues"), findsOneWidget);
    expect(find.byType(IssuesView), findsOneWidget);
    expect(find.text("Test merge mr"), findsOneWidget);
    expect(find.text("mr test"), findsOneWidget);
    expect(find.text("Test issue warnings"), findsOneWidget);
    expect(find.text("提交信息不重复"), findsOneWidget);
    expect(find.text("Group by epic"), findsOneWidget);
    expect(find.byType(CupertinoSwitch), findsOneWidget);
    expect(find.byType(EpicIssuesView), findsNWidgets(0));

    await tester.tap(find.byType(CupertinoSwitch));
    await tester.pumpAndSettle();
    expect(find.byType(EpicIssuesView), findsNWidgets(2));
    expect(find.widgetWithText(EpicIssuesView, "项目设置"), findsOneWidget);
    expect(find.widgetWithText(EpicIssuesView, "No Epic"), findsOneWidget);

    expect(find.byType(EpicProgressView), findsNWidgets(4));
    List<EpicProgressView> widgetList = tester.widgetList<EpicProgressView>(find.byType(EpicProgressView)).toList();
    expect(widgetList.length, 4);
    expect(widgetList[0].progress, 0.5);
    expect(widgetList[1].progress, 0.5);
    expect(widgetList[2].progress, 0);
    expect(widgetList[3].progress, 0);
  });

  testWidgets('Should be able to display the issues and cannot group by epic', (WidgetTester tester) async {
    await setUpMobileBinding(tester);
    when(client.get("/api/v4/groups/88966/iterations")).thenThrow(Exception());
    when(client.post<dynamic>('/api/graphql', projectMilestoneRequestBody('ultimate-plan/jihu-gitlab-app/demo', 20, "")))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(projectMilestonesResponse)));
    when(client.post<dynamic>('/api/graphql', requestBodyOfGetFirst100IssueByMilestoneTitle('ultimate-plan/jihu-gitlab-app/demo', "和家家户户口咯")))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(milestoneIssuesResponse)));
    when(client.post<dynamic>('/api/graphql', requestBodyOfGetFirst100IssueExtraByMilestoneTitle('ultimate-plan/jihu-gitlab-app/demo', "和家家户户口咯")))
        .thenAnswer((_) => Future(() => Response.of<dynamic>({
              "errors": [
                {"message": "Field 'epic' doesn't exist on type 'Issue'"}
              ]
            })));
    when(client.post<dynamic>('/api/graphql', requestBodyOfGetMilestoneTimeBoxReport('ultimate-plan/jihu-gitlab-app/demo', "gid://gitlab/Milestone/6706")))
        .thenAnswer((_) => Future(() => Response.of<dynamic>({
              "data": {
                "milestone": {
                  "report": {"burnupTimeSeries": []}
                }
              }
            })));
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: const Scaffold(body: ProjectMilestonesPage(fullPath: fullPath))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(RoundRectContainer), findsNWidgets(6));
    expect(find.text("和家家户户口咯"), findsOneWidget);
    expect(find.text("expires on Mar 31, 2023"), findsOneWidget);
    expect(find.text("Open"), findsNWidgets(5));
    expect(find.text("Closed"), findsOneWidget);
    expect(find.text("个h ju io jo i"), findsOneWidget);
    expect(find.text("Mar 24, 2023 - Mar 31, 2023"), findsOneWidget);
    expect(find.text("started on Mar 23, 2023"), findsOneWidget);

    await tester.tap(find.text("和家家户户口咯"));
    await tester.pumpAndSettle();
    expect(find.byType(ProjectMilestonePage), findsOneWidget);
    expect(find.widgetWithText(MilestoneAndIterationStateView, "Open"), findsOneWidget);
    expect(find.text("expires on Mar 31, 2023"), findsOneWidget);
    expect(find.byType(BurnChartView), findsNothing);
    expect(find.widgetWithText(Tab, "Issues"), findsOneWidget);
    expect(find.byType(IssuesView), findsOneWidget);
    expect(find.text("Test merge mr"), findsOneWidget);
    expect(find.text("mr test"), findsOneWidget);
    expect(find.text("Test issue warnings"), findsOneWidget);
    expect(find.text("提交信息不重复"), findsOneWidget);
    expect(find.text("Group by epic"), findsOneWidget);
    expect(find.byType(CupertinoSwitch), findsOneWidget);
    expect(find.byType(EpicIssuesView), findsNWidgets(0));

    await tester.tap(find.byType(CupertinoSwitch));
    await tester.pumpAndSettle();
    expect(find.byType(EpicIssuesView), findsNWidgets(0));
    expect(find.byType(CupertinoAlertDialog), findsOneWidget);
    expect(find.text("Feature Not Supported"), findsOneWidget);
  });

  tearDown(() {
    reset(client);
    ConnectionProvider().fullReset();
  });
}

Map<String, dynamic> projectMilestonesResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/72936",
      "name": "private demo",
      "milestones": {
        "nodes": [
          {"id": "gid://gitlab/Milestone/6706", "title": "和家家户户口咯", "startDate": null, "dueDate": "2023-03-31", "state": "active"},
          {"id": "gid://gitlab/Milestone/6704", "title": "个h ju io jo i", "startDate": "2023-03-24", "dueDate": "2023-03-31", "state": "active"},
          {"id": "gid://gitlab/Milestone/6674", "title": "里程碑1", "startDate": "2023-03-01", "dueDate": "2023-03-31", "state": "active"},
          {"id": "gid://gitlab/Milestone/6705", "title": "韩国胡静韩剧i", "startDate": "2023-03-23", "dueDate": null, "state": "active"},
          {"id": "gid://gitlab/Milestone/6679", "title": "ertyu", "startDate": null, "dueDate": null, "state": "active"},
          {"id": "gid://gitlab/Milestone/6675", "title": "里程碑2", "startDate": null, "dueDate": null, "state": "closed"}
        ],
        "pageInfo": {"hasNextPage": false, "hasPreviousPage": false, "startCursor": "eyJkdWVfZGF0ZSI6IjIwMjMtMDMtMzEiLCJpZCI6IjY3MDYifQ", "endCursor": "eyJkdWVfZGF0ZSI6bnVsbCwiaWQiOiI2Njc1In0"}
      },
    }
  }
};

Map<String, dynamic> milestoneIssuesResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/72936",
      "name": "private demo",
      "fullPath": "ultimate-plan/jihu-gitlab-app/demo",
      "issues": {
        "nodes": [
          {
            "id": "gid://gitlab/Issue/363456",
            "iid": "84",
            "title": "提交信息不重复",
            "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/-/issues/84",
            "state": "opened",
            "projectId": 72936,
            "epic": null,
            "weight": 6,
            "author": {"id": "gid://gitlab/User/23836", "name": "yajie xue", "username": "jojo0", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png"},
            "assignees": {"nodes": []}
          },
          {
            "id": "gid://gitlab/Issue/332646",
            "iid": "70",
            "title": "Test issue warnings",
            "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/-/issues/70",
            "state": "opened",
            "projectId": 72936,
            "epic": null,
            "weight": null,
            "author": {"id": "gid://gitlab/User/30192", "name": "Raymond Liao", "username": "raymond-liao", "avatarUrl": "/uploads/-/system/user/avatar/30192/avatar.png"},
            "assignees": {
              "nodes": [
                {"id": "gid://gitlab/User/30192", "name": "Raymond Liao", "username": "raymond-liao", "avatarUrl": "/uploads/-/system/user/avatar/30192/avatar.png"}
              ]
            }
          },
          {
            "id": "gid://gitlab/Issue/323402",
            "iid": "65",
            "title": "mr test",
            "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/-/issues/65",
            "state": "opened",
            "projectId": 72936,
            "epic": {"title": "项目设置"},
            "weight": 2,
            "author": {"id": "gid://gitlab/User/23836", "name": "yajie xue", "username": "jojo0", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png"},
            "assignees": {
              "nodes": [
                {"id": "gid://gitlab/User/23836", "name": "yajie xue", "username": "jojo0", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png"}
              ]
            }
          },
          {
            "id": "gid://gitlab/Issue/323362",
            "iid": "63",
            "title": "Test merge mr",
            "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/-/issues/63",
            "state": "closed",
            "projectId": 72936,
            "epic": {"title": "项目设置"},
            "weight": 2,
            "author": {"id": "gid://gitlab/User/23837", "name": "万友 朱", "username": "wanyouzhu", "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png"},
            "assignees": {
              "nodes": [
                {"id": "gid://gitlab/User/23837", "name": "万友 朱", "username": "wanyouzhu", "avatarUrl": "/uploads/-/system/user/avatar/23837/avatar.png"}
              ]
            }
          }
        ]
      }
    }
  }
};
Map<String, dynamic> milestoneIssuesExtraResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/72936",
      "name": "private demo",
      "fullPath": "ultimate-plan/jihu-gitlab-app/demo",
      "issues": {
        "nodes": [
          {
            "iid": "84",
            "epic": null,
            "weight": 6,
          },
          {
            "iid": "70",
            "epic": null,
            "weight": null,
          },
          {
            "iid": "65",
            "epic": {"title": "项目设置"},
            "weight": 2,
          },
          {
            "iid": "63",
            "epic": {"title": "项目设置"},
            "weight": 2,
          }
        ]
      }
    }
  }
};

Map<String, dynamic> milestoneTimeBoxReportResp = {
  "data": {
    "milestone": {
      "report": {
        "burnupTimeSeries": [
          {"date": "2023-04-03", "scopeCount": 4, "scopeWeight": 10, "completedCount": 1, "completedWeight": 2},
          {"date": "2023-04-06", "scopeCount": 4, "scopeWeight": 10, "completedCount": 2, "completedWeight": 8}
        ]
      }
    }
  }
};
