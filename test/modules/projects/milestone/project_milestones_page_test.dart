import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/widgets/http_fail_view.dart';
import 'package:jihu_gitlab_app/core/widgets/round_rect_container.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestones_fetcher.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestones_page.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

import '../../../integration_tests/helper/core.dart';
import '../../../mocker/tester.dart';
import '../../../test_binding_setter.dart';

void main() {
  const String fullPath = "ultimate-plan/jihu-gitlab-app/demo";

  setUp(() async {
    TestWidgetsFlutterBinding.ensureInitialized();
    ConnectionProvider().reset(Tester.jihuLabUser());
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets('Should be able to display the empty data view', (tester) async {
    await setUpMobileBinding(tester);
    when(client.post<dynamic>('/api/graphql', projectMilestoneRequestBody('ultimate-plan/jihu-gitlab-app/demo', 20, ""))).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: const Scaffold(body: ProjectMilestonesPage(fullPath: fullPath))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(TipsView), findsOneWidget);
  });

  testWidgets('Should be able to display the http failure view', (tester) async {
    await setUpMobileBinding(tester);
    when(client.post<dynamic>('/api/graphql', projectMilestoneRequestBody('ultimate-plan/jihu-gitlab-app/demo', 20, ""))).thenThrow(Exception());
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: const Scaffold(body: ProjectMilestonesPage(fullPath: fullPath))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(HttpFailView), findsOneWidget);
  });

  testWidgets('Should be able to display the milestone list', (tester) async {
    await setUpMobileBinding(tester);
    when(client.post<dynamic>('/api/graphql', projectMilestoneRequestBody('ultimate-plan/jihu-gitlab-app/demo', 20, "")))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(projectMilestonesResponse)));
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: const Scaffold(body: ProjectMilestonesPage(fullPath: fullPath))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(RoundRectContainer), findsNWidgets(6));
    expect(find.text("和家家户户口咯"), findsOneWidget);
    expect(find.text("expires on Mar 31, 2023"), findsOneWidget);
    expect(find.text("Open"), findsNWidgets(5));
    expect(find.text("Closed"), findsOneWidget);

    expect(find.text("个h ju io jo i"), findsOneWidget);
    expect(find.text("Mar 24, 2023 - Mar 31, 2023"), findsOneWidget);
    expect(find.text("started on Mar 23, 2023"), findsOneWidget);
  });

  tearDown(() {
    reset(client);
    ConnectionProvider().fullReset();
  });
}

Map<String, dynamic> projectMilestonesResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/72936",
      "name": "private demo",
      "milestones": {
        "nodes": [
          {"id": "gid://gitlab/Milestone/6706", "title": "和家家户户口咯", "startDate": null, "dueDate": "2023-03-31", "state": "active"},
          {"id": "gid://gitlab/Milestone/6704", "title": "个h ju io jo i", "startDate": "2023-03-24", "dueDate": "2023-03-31", "state": "active"},
          {"id": "gid://gitlab/Milestone/6674", "title": "里程碑1", "startDate": "2023-03-01", "dueDate": "2023-03-31", "state": "active"},
          {"id": "gid://gitlab/Milestone/6705", "title": "韩国胡静韩剧i", "startDate": "2023-03-23", "dueDate": null, "state": "active"},
          {"id": "gid://gitlab/Milestone/6679", "title": "ertyu", "startDate": null, "dueDate": null, "state": "active"},
          {"id": "gid://gitlab/Milestone/6675", "title": "里程碑2", "startDate": null, "dueDate": null, "state": "closed"}
        ],
        "pageInfo": {"hasNextPage": false, "hasPreviousPage": false, "startCursor": "eyJkdWVfZGF0ZSI6IjIwMjMtMDMtMzEiLCJpZCI6IjY3MDYifQ", "endCursor": "eyJkdWVfZGF0ZSI6bnVsbCwiaWQiOiI2Njc1In0"}
      },
    }
  }
};
