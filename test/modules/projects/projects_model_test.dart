import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/modules/projects/projects_groups_model.dart';

void main() {
  test('Should create projects model object', () {
    ProjectsGroupsModel model = ProjectsGroupsModel();
    expect(model.loadState, LoadState.noItemState);
  });
}
