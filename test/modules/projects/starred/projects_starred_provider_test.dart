import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/projects.dart';
import 'package:jihu_gitlab_app/modules/projects/starred/projects_starred_provider.dart';
import 'package:mockito/mockito.dart';

import '../../../core/net/http_request_test.mocks.dart';
import '../../../mocker/tester.dart';
import '../../../test_data/starred_projects.dart';
import '../group_and_project_provider_test.mocks.dart';

void main() {
  late ProjectsStarredProvider projectsStarredProvider;
  late MockGroupAndProjectRepository repository;

  setUp(() {
    repository = MockGroupAndProjectRepository();
    projectsStarredProvider = ProjectsStarredProvider(userId: 9527);
    projectsStarredProvider.injectRepositoryForTesting(repository);
    ConnectionProvider().reset(Tester.jihuLabUser());
  });

  test("Should load groups and projects data from local", () async {
    var dbData = {
      "id": 1,
      "iid": 11,
      "user_id": 9527,
      "name": "test_project",
      "type": SubgroupItemType.project.name,
      "relative_path": "path_with_namespace_test",
      "parent_id": 59883,
      "starred": 1,
      "version": 1,
      "last_activity_at": 0,
      "starred_at": 0
    };
    when(repository.findStarredList(userId: 9527)).thenAnswer((realInvocation) async => Future.value([GroupAndProjectEntity.restore(dbData)]));
    var list = await projectsStarredProvider.loadFromLocal();
    expect(list.length, 1);
    expect(list[0].iid, 11);
    expect(list[0].name, "test_project");
    expect(list[0].relativePath, "path_with_namespace_test");
  });

  test('Should sync starred projects data from remote', () async {
    var client = MockHttpClient();
    HttpClient.injectInstanceForTesting(client);
    when(client.get<List<dynamic>>('/api/v4/users/9527/starred_projects?page=1&per_page=50')).thenAnswer((_) => Future(() => Response.of<List<dynamic>>(starredProjects)));
    GroupAndProjectEntity existsProject = GroupAndProjectEntity.restore({
      "id": 44,
      "iid": 78632,
      "user_id": 9527,
      "parent_id": 32052,
      "name": "tester-44",
      "relative_path": "path_with_namespace_44",
      "starred": 1,
      "version": 4,
      "last_activity_at": 0,
      "starred_at": 0
    });
    when(repository.findStarredList(userId: 9527)).thenAnswer((realInvocation) async => Future.value([existsProject]));
    when(repository.findStarredList(userId: 9527, iidList: [78632, 59893])).thenAnswer((realInvocation) async => Future.value([existsProject]));

    var result = await projectsStarredProvider.syncFromRemote();
    expect(result, true);
  });
}
