import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/stars_provider.dart';
import 'package:jihu_gitlab_app/main.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_model.dart';
import 'package:jihu_gitlab_app/modules/issues/list/project_issues_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/mrs/project_merge_request_model.dart';
import 'package:jihu_gitlab_app/modules/projects/projects.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/project_repository_model.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_file_content_fetcher.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_tree_data_fetcher.dart';
import 'package:jihu_gitlab_app/modules/projects/starred/projects_starred_provider.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../core/net/http_request_test.mocks.dart';
import '../../../finder/svg_finder.dart';
import '../../../integration_tests/query_issues_by_state_test.dart';
import '../../../mocker/tester.dart';
import '../../../test_binding_setter.dart';
import '../../../test_data/issue.dart';
import '../group_details/mrs/mr_page_test_data.dart';
import 'projects_starred_page_test.mocks.dart';

final client = MockHttpClient();

@GenerateNiceMocks([MockSpec<ProjectsStarredProvider>()])
void main() {
  late ProjectsStarredModel model;
  late MockProjectsStarredProvider provider;

  setUp(() async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    await ConnectionProvider().restore();

    model = ProjectsStarredModel();
    locator.registerSingleton(model);
    provider = MockProjectsStarredProvider();
    model.injectDataProviderForTesting(provider);
    var issueDetailsModel = IssueDetailsModel();
    locator.registerSingleton(issueDetailsModel);
  });

  tearDown(() {
    locator.unregister<ProjectsStarredModel>();
    locator.unregister<IssueDetailsModel>();
    reset(client);
    ConnectionProvider().fullReset();
    StarsProvider().fullReset();
    ProjectProvider().fullReset();
  });

  testWidgets('Should display Default starred View when not logged in', (tester) async {
    when(client.post<dynamic>("https://gitlab.com/api/graphql", getProjectIssuesGraphQLRequestBody('gitlab-org/gitlab', '', 20), connection: null, useActiveConnection: false))
        .thenAnswer((_) => Future(() => Response.of(projectIssuesGraphQLResponseData)));
    when(client.post('https://gitlab.com/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 276), connection: null, useActiveConnection: false))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(issueDetailsGraphQLResponse)));
    when(client.post('https://gitlab.com/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 276), connection: null, useActiveConnection: false))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(issuePostVotesGraphQLResponse)));
    when(client.get("https://gitlab.com/api/v4/projects/59893/issues/276/award_emoji?per_page=100&page=1", connection: null, useActiveConnection: false))
        .thenAnswer((_) => Future(() => Response.of([])));

    when(client.post('https://gitlab.com/api/graphql', getMRListGraphQLRequestBody('gitlab-org/gitlab', 20, ''), connection: null, useActiveConnection: false))
        .thenAnswer((_) => Future(() => Response.of(mergeRequestsResponse)));
    when(client.get('https://gitlab.com/api/v4/projects/59893/merge_requests/427/approvals', connection: null, useActiveConnection: false)).thenThrow(Exception());
    when(client.get('https://gitlab.com/api/v4/projects/59893/merge_requests/427/diffs?page=1&per_page=20', connection: null, useActiveConnection: false)).thenThrow(Exception());
    when(client.post('https://gitlab.com/api/graphql', predicate((arg) => arg.toString().contains("getMergeRequest")), connection: null, useActiveConnection: false)).thenThrow(Exception());
    when(client.post('https://gitlab.com/api/graphql', predicate((arg) => arg.toString().contains("onlyAllowMergeIfAllStatusChecksPassed")), connection: null, useActiveConnection: false))
        .thenThrow(Exception());
    when(client.post<dynamic>('https://gitlab.com/api/graphql', buildRequestBodyOfRepositoryTree("gitlab-org/gitlab", '', ref: 'main'), connection: null, useActiveConnection: false))
        .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>({})));
    when(client.post<dynamic>('https://gitlab.com/api/graphql', buildRequestBodyOfCheckRepositoryIsEmpty('gitlab-org/gitlab'), connection: null, useActiveConnection: false))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(checkRepositoryIsEmptyResponse)));
    when(client.post<dynamic>('https://gitlab.com/api/graphql', buildRequestBodyOfFetchFileContent('gitlab-org/gitlab', "README.md", "main"), connection: null, useActiveConnection: false))
        .thenAnswer((_) => Future(() => Response.of<dynamic>({})));
    HttpClient.injectInstanceForTesting(client);
    when(provider.loadFromLocal()).thenAnswer((_) => Future(() => []));
    await tester.pumpWidget(
      MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
        child: MaterialApp(
          onGenerateRoute: onGenerateRoute,
          navigatorKey: navigatorKey,
          home: const Scaffold(
            body: ProjectsStarredPage(),
          ),
        ),
      ),
    );
    await tester.pumpAndSettle();
    expect(find.text('GitLab'), findsOneWidget);
    await tester.tap(find.text('GitLab'));
    await tester.pumpAndSettle();
    expect(find.text('Issues'), findsOneWidget);
    expect(find.text('MR'), findsOneWidget);
    expect(find.text('Repository'), findsOneWidget);
    expect(find.widgetWithText(Tab, "Issues"), findsOneWidget);
    await tester.tap(find.widgetWithText(Tab, "Issues"));
    await tester.pumpAndSettle();
    expect(find.text("Feature：交互优化：创建议题时，如果只有一个项目，则跳过「选择项目」"), findsOneWidget);
    await tester.tap(find.text("Feature：交互优化：创建议题时，如果只有一个项目，则跳过「选择项目」"));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.text("回复评论"), findsOneWidget);
    expect(SvgFinder('assets/images/comment.svg'), findsNothing);

    await tester.tap(find.text('MR'));
    await tester.pumpAndSettle();
    expect(find.text("Draft: Resolve \"Hotfix: 待办列表翻页灰屏\""), findsOneWidget);
    await tester.tap(find.text("Draft: Resolve \"Hotfix: 待办列表翻页灰屏\""));
    await tester.pumpAndSettle();
  });

  testWidgets('Should display Default Starred View when not logged in 2', (tester) async {
    setUpDesktopBinding(tester);
    when(client.post<dynamic>("https://gitlab.com/api/graphql", getProjectIssuesGraphQLRequestBody('gitlab-org/gitlab', '', 20), connection: null, useActiveConnection: false))
        .thenAnswer((_) => Future(() => Response.of(projectIssuesGraphQLResponseData)));
    when(client.post('https://gitlab.com/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 276), connection: null, useActiveConnection: false))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(issueDetailsGraphQLResponse)));
    when(client.post('https://gitlab.com/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 276), connection: null, useActiveConnection: false))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(issuePostVotesGraphQLResponse)));
    when(client.get("https://gitlab.com/api/v4/projects/59893/issues/276/award_emoji?per_page=100&page=1", connection: null, useActiveConnection: false))
        .thenAnswer((_) => Future(() => Response.of([])));
    when(client.post('https://gitlab.com/api/graphql', getMRListGraphQLRequestBody('gitlab-org/gitlab', 20, ''), connection: null, useActiveConnection: false))
        .thenAnswer((_) => Future(() => Response.of(mergeRequestsResponse)));
    when(client.get('https://gitlab.com/api/v4/projects/59893/merge_requests/427/approvals', connection: null, useActiveConnection: false)).thenThrow(Exception());
    when(client.get('https://gitlab.com/api/v4/projects/59893/merge_requests/427/diffs?page=1&per_page=20', connection: null, useActiveConnection: false)).thenThrow(Exception());
    when(client.post('https://gitlab.com/api/graphql', predicate((arg) => arg.toString().contains("getMergeRequest")), connection: null, useActiveConnection: false)).thenThrow(Exception());
    when(client.post('https://gitlab.com/api/graphql', predicate((arg) => arg.toString().contains("onlyAllowMergeIfAllStatusChecksPassed")), connection: null, useActiveConnection: false))
        .thenThrow(Exception());
    when(client.post<dynamic>('https://gitlab.com/api/graphql', buildRequestBodyOfRepositoryTree("gitlab-org/gitlab", '', ref: 'main'), connection: null, useActiveConnection: false))
        .thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>({})));
    when(client.post<dynamic>('https://gitlab.com/api/graphql', buildRequestBodyOfCheckRepositoryIsEmpty('gitlab-org/gitlab'), connection: null, useActiveConnection: false))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(checkRepositoryIsEmptyResponse)));
    when(client.post<dynamic>('https://gitlab.com/api/graphql', buildRequestBodyOfFetchFileContent('gitlab-org/gitlab', "README.md", "main"), connection: null, useActiveConnection: false))
        .thenAnswer((_) => Future(() => Response.of<dynamic>({})));
    HttpClient.injectInstanceForTesting(client);
    when(provider.loadFromLocal()).thenAnswer((_) => Future(() => []));
    await tester.pumpWidget(
      MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
        child: MaterialApp(onGenerateRoute: onGenerateRoute, navigatorKey: navigatorKey, home: const Scaffold(body: ProjectsStarredPage())),
      ),
    );
    await tester.pumpAndSettle();
    expect(find.text('GitLab'), findsOneWidget);
    await tester.tap(find.text('GitLab'));
    await tester.pumpAndSettle();
    expect(find.text('Issues'), findsOneWidget);
    expect(find.text('MR'), findsOneWidget);
    expect(find.text('Repository'), findsOneWidget);
    expect(find.widgetWithText(Tab, "Issues"), findsOneWidget);
    await tester.tap(find.widgetWithText(Tab, "Issues"));
    await tester.pumpAndSettle();
    expect(find.text("Feature：交互优化：创建议题时，如果只有一个项目，则跳过「选择项目」"), findsOneWidget);
    await tester.tap(find.text("Feature：交互优化：创建议题时，如果只有一个项目，则跳过「选择项目」"));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.text("回复评论"), findsOneWidget);
    expect(SvgFinder('assets/images/comment.svg'), findsNothing);

    await tester.tap(find.text('MR'));
    await tester.pumpAndSettle();
    expect(find.text("Draft: Resolve \"Hotfix: 待办列表翻页灰屏\""), findsOneWidget);
    await tester.tap(find.text("Draft: Resolve \"Hotfix: 待办列表翻页灰屏\""));
    await tester.pumpAndSettle();
  });

  testWidgets('Should display Empty View when logged in with empty data', (tester) async {
    ConnectionProvider().reset(Tester.jihuLabUser());

    when(provider.loadFromLocal()).thenAnswer((_) => Future(() => []));
    when(client.get('/users/9527/starred_projects?page=1&per_page=50')).thenAnswer((_) => Future(() => Response.of([])));
    HttpClient.injectInstanceForTesting(client);

    await tester.pumpWidget(
      MultiProvider(providers: [
        ChangeNotifierProvider(create: (context) => ConnectionProvider()),
        ChangeNotifierProvider(create: (context) => StarsProvider()),
        ChangeNotifierProvider(create: (context) => ProjectProvider())
      ], child: MaterialApp(navigatorKey: navigatorKey, home: const Scaffold(body: ProjectsStarredPage()))),
    );
    await tester.pumpAndSettle();

    expect(find.byType(ProjectsStarredPage), findsOneWidget);
    expect(find.text('GitLab'), findsOneWidget);
  });

  testWidgets('Should display List View when logged in with data', (tester) async {
    ConnectionProvider().reset(Tester.jihuLabUser());

    List<GroupAndProject> starredProjects = [
      GroupAndProject(1, 120181, "上线审批组", SubgroupItemType.subgroup, "ultimate-plan/jihu-gitlab-app/deployment-approvers", 0, starred: true),
      GroupAndProject(2, 75468, "API", SubgroupItemType.project, "ultimate-plan/jihu-gitlab-app/api", 0, starred: true)
    ];
    when(provider.loadFromLocal()).thenAnswer((_) => Future(() => starredProjects));
    when(client.get<List<dynamic>>('/users/9527/starred_projects?page=1&per_page=50')).thenAnswer((_) => Future(() => Response.of<List<dynamic>>(starredProjects)));
    HttpClient.injectInstanceForTesting(client);
    await tester.pumpWidget(
      MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
          child: MaterialApp(navigatorKey: navigatorKey, home: const Scaffold(body: ProjectsStarredPage()))),
    );
    await tester.pumpAndSettle();
    expect(find.byType(ProjectsStarredPage), findsOneWidget);
    expect(find.byType(ListView), findsOneWidget);
    expect(find.byIcon(Icons.star_rounded), findsNWidgets(2));
    expect(find.text("上线审批组"), findsOneWidget);
    expect(find.text("API"), findsOneWidget);
    await tester.tap(find.byIcon(Icons.star_rounded).first);
    starredProjects.removeAt(0);
    await tester.pumpAndSettle();
    expect(find.byIcon(Icons.star_rounded), findsNWidgets(1));
    expect(find.text('GitLab'), findsOneWidget);
  });
}
