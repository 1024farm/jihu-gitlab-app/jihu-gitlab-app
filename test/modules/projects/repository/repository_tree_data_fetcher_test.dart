import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_tree_data_fetcher.dart';

void main() {
  test("Should be able to build the body of request repository tree", () {
    expect(buildRequestBodyOfRepositoryTree("DEMO", "CURSOR", path: "/", ref: "main"), {
      "variables": {"projectPath": "DEMO", "path": "/", "ref": "main", "nextPageCursor": "CURSOR"},
      "query": """
              fragment PageInfo on PageInfo {
                hasNextPage
                hasPreviousPage
                startCursor
                endCursor
              }

              fragment TreeEntry on Entry {
                id
                sha
                name
                flatPath
                type
                path
              }

              query getPaginatedTree(\$projectPath: ID!, \$path: String, \$ref: String, \$nextPageCursor:String) {
                project(fullPath: \$projectPath) {
                  id
                  name
                  repository {
                    paginatedTree(path: \$path, ref: \$ref, after: \$nextPageCursor){
                      nodes{
                        trees{
                          nodes{
                            ...TreeEntry
                            webUrl
                          }
                        }
                        blobs{
                          nodes{
                            ...TreeEntry
                            webUrl
                          }
                        }
                      }
                      pageInfo{
                        ...PageInfo
                      }
                    }
                  }
                }
              }
              """
    });
  });
}
