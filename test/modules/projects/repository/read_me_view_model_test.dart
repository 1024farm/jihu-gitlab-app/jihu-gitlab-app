import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/project_repository_model.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/project_repository_page.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/readme_view.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_file_content_fetcher.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_tree_data_fetcher.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../../../core/user_provider_test.dart';
import '../../../mocker/tester.dart';
import '../../../test_binding_setter.dart';
import 'response_data.dart';

void main() {
  const String fullPath = "ultimate-plan/jihu-gitlab-app/demo";

  setUp(() {
    TestWidgetsFlutterBinding.ensureInitialized();
    ConnectionProvider().reset(Tester.jihuLabUser());
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfCheckRepositoryIsEmpty(fullPath))).thenAnswer((_) => Future(() => Response.of<dynamic>(checkRepositoryIsEmptyResponse())));
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfRepositoryTree(fullPath, '', ref: "main"))).thenAnswer((_) => Future(() => Response.of<dynamic>(repositoryTreeResponse)));
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets("Should display empty box when the readme file does not exists", (tester) async {
    WidgetsFlutterBinding.ensureInitialized();
    await setUpMobileBinding(tester);
    when(client.post<dynamic>("/api/graphql", buildRequestBodyOfFetchFileContent(fullPath, "README.md", "main")))
        .thenAnswer((_) => Future(() => Response.of(buildReadmeResponse("", "raw-path-test"))));
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: const Scaffold(body: ProjectRepositoryPage(fullPath: fullPath, projectName: "demo"))),
    );
    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(ReadmeView), findsOneWidget);
    await tester.pumpAndSettle();
    expect(find.byType(WebViewWidget), findsNothing);
    expect(find.byKey(const Key("empty-readme")), findsOneWidget);
  });

  testWidgets("Should be able to fill the relative path of the images", (tester) async {
    WidgetsFlutterBinding.ensureInitialized();
    await setUpMobileBinding(tester);
    when(client.post<dynamic>("/api/graphql", buildRequestBodyOfFetchFileContent(fullPath, "README.md", "main"))).thenAnswer((_) => Future(() => Response.of(buildReadmeResponse(
        "# 极狐 GitLab App\n\n<a href='https://apps.apple.com/cn/app/jihu-gitlab/id6444783184' style=\"display: inline-block; overflow: hidden; border-radius: 13px; height: 60px;\"><img alt='下载应用，请到 Apple App Store' src=\"public/assets/ios-badge.svg\" style=\"border-radius: 13px; height: 60px;\"/></a>\n\n<a href='https://play.google.com/store/apps/details?id=cn.gitlab.app&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1' style=\"display: inline-block; overflow: hidden; border-radius: 13px; height: 60px;\"><img alt='下载应用，请到 Google Play' src='https://play.google.com/intl/en_us/badges/static/images/badges/zh-cn_badge_web_generic.png' style=\"border-radius: 13px; height: 60px;\"/></a>\n\n<a href='https://jihulab.com/api/v4/projects/59893/packages/generic/jihu-gitlab-app/latest/app-debug.apk' style=\"display: inline-block; overflow: hidden; border-radius: 13px; height: 60px;\"><img alt='本地下载安卓安装包' src='public/assets/apk-badge.png' style=\"border-radius: 13px; height: 60px;\"/></a>",
        "/raw-path-test/README.md"))));

    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: const Scaffold(body: ProjectRepositoryPage(fullPath: fullPath, projectName: "demo"))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(ReadmeView), findsOneWidget);
    await tester.pumpAndSettle();
    expect(find.byType(WebViewWidget), findsOneWidget);
    expect(find.byKey(const Key("empty-readme")), findsNothing);
  });

  test("Should be able build the body of request readme", () {
    expect(buildRequestBodyOfFetchFileContent("demo", "README.md", "main"), {
      "variables": {"projectPath": 'demo', "path": "README.md", "ref": "main"},
      "query": """
                query getFileContent(\$projectPath: ID!, \$path: String!, \$ref: String) {
                  project(fullPath: \$projectPath) {
                    id
                    repository {
                      blobs(paths: [\$path], ref: \$ref) {
                        edges {
                          node {
                            rawTextBlob
                            rawPath
                            name
                            language
                            simpleViewer{
                              fileType
                              tooLarge
                            }
                          }
                        }
                      }
                    }
                  }
                }
              """
    });
  });

  tearDown(() {
    reset(client);
    ConnectionProvider().fullReset();
  });
}

Map<String, dynamic> buildReadmeResponse(String rawTextBlob, String rawPath) {
  return {
    "data": {
      "project": {
        "repository": {
          "blobs": {
            "edges": [
              {
                "node": {"rawTextBlob": rawTextBlob, "rawPath": rawPath, "name": "README.md"}
              }
            ]
          }
        }
      }
    }
  };
}
