import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/share_view.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/project_repository_model.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/project_repository_page.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/readme_view.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_file_content_fetcher.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_file_content_page.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_tree_data_fetcher.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_tree_item_view.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_tree_page.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../../../finder/svg_finder.dart';
import '../../../integration_tests/helper/core.dart';
import '../../../mocker/tester.dart';
import '../../../test_binding_setter.dart';
import 'response_data.dart';

void main() {
  const String fullPath = "ultimate-plan/jihu-gitlab-app/demo";

  setUp(() async {
    TestWidgetsFlutterBinding.ensureInitialized();
    ConnectionProvider().reset(Tester.jihuLabUser());
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets('Should be able to display the repository tree', (tester) async {
    await setUpMobileBinding(tester);
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfRepositoryTree(fullPath, '', ref: "main"))).thenAnswer((_) => Future(() => Response.of<dynamic>(repositoryTreeResponse)));
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfRepositoryTree(fullPath, '', path: "android", ref: "main")))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(repositoryTreeInAndroidDirResponse)));
    when(client.post<dynamic>("/api/graphql", buildRequestBodyOfFetchFileContent(fullPath, "README.md", "main"))).thenAnswer((_) => Future(() => Response.of(readmeResponse)));
    when(client.post<dynamic>("/api/graphql", buildRequestBodyOfFetchFileContent(fullPath, "android/settings.gradle", "main"))).thenAnswer((_) => Future(() => Response.of(gradleFileResponse)));
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfCheckRepositoryIsEmpty(fullPath))).thenAnswer((_) => Future(() => Response.of<dynamic>(checkRepositoryIsEmptyResponse())));
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: const Scaffold(body: ProjectRepositoryPage(fullPath: fullPath, projectName: "demo"))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(ProjectRepositoryPage), findsOneWidget);
    expect(find.byType(RepositoryTreeItemView), findsNWidgets(5));
    expect(find.text(".gitlab/issue_templates"), findsOneWidget);
    expect(find.text("android"), findsOneWidget);
    expect(find.text("assets"), findsOneWidget);
    expect(find.text("ios"), findsOneWidget);
    expect(find.text("lib"), findsOneWidget);
    expect(find.text("View All"), findsOneWidget);
    expect(SvgFinder("assets/images/readme.svg"), findsOneWidget);
    expect(find.text("README.md"), findsOneWidget);
    expect(find.byType(WebViewWidget), findsOneWidget);

    await tester.tap((find.text("android")));
    await tester.pumpAndSettle();
    expect(find.byType(RepositoryTreePage), findsOneWidget);
    expect(find.widgetWithText(CommonAppBar, "android"), findsOneWidget);
    expect(find.byType(BackButton), findsOneWidget);
    expect(find.byType(RepositoryTreeItemView), findsNWidgets(7));
    expect(find.text("settings.gradle"), findsOneWidget);
    await tester.tap((find.text("settings.gradle")));
    await tester.pumpAndSettle();
    expect(find.byType(RepositoryFileContentPage), findsOneWidget);
    expect(find.byType(BackButton), findsOneWidget);
    expect(find.widgetWithText(CommonAppBar, "settings.gradle"), findsOneWidget);
    expect(find.byType(ListView), findsNWidgets(2));
    expect(SvgFinder('assets/images/external-link.svg'), findsOneWidget);
    expect(find.text("1"), findsOneWidget);
    expect(find.text("2"), findsOneWidget);
    expect(find.text("3"), findsOneWidget);
    expect(find.text("1 in settings.gradle"), findsOneWidget);
    expect(find.text("2 in settings.gradle"), findsOneWidget);
    expect(find.text("3 in settings.gradle"), findsOneWidget);
    await tester.tap(SvgFinder('assets/images/external-link.svg'));
    await tester.pumpAndSettle();
    expect(find.byType(ShareView), findsOneWidget);
    expect(SvgFinder('assets/images/copy_filename_and_link.svg'), findsOneWidget);
    await tester.tap(SvgFinder('assets/images/copy_filename_and_link.svg'));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    await tester.tap(find.byType(BackButton));
    await tester.pumpAndSettle();
    await tester.tap(find.byType(BackButton));
    await tester.pumpAndSettle();
    expect(find.byType(RepositoryTreePage), findsNothing);

    await tester.tap((find.text("View All")));
    await tester.pumpAndSettle();
    expect(find.byType(RepositoryTreePage), findsOneWidget);
    expect(find.widgetWithText(CommonAppBar, "demo"), findsOneWidget);
    expect(find.byType(BackButton), findsOneWidget);
    expect(find.byType(RepositoryTreeItemView), findsNWidgets(19));
  });

  testWidgets("Should be able to hide the 'View All' button when the number of files is less than 6", (tester) async {
    await setUpMobileBinding(tester);
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfRepositoryTree(fullPath, '', ref: 'main'))).thenAnswer((_) => Future(() => Response.of<dynamic>(repositoryTreeLessThan6Response)));
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfRepositoryTree(fullPath, '', path: "android", ref: 'main')))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(repositoryTreeInAndroidDirResponse)));
    when(client.post<dynamic>("/api/graphql", buildRequestBodyOfFetchFileContent(fullPath, "README.md", "main"))).thenAnswer((_) => Future(() => Response.of(readmeResponse)));
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfCheckRepositoryIsEmpty(fullPath))).thenAnswer((_) => Future(() => Response.of<dynamic>(checkRepositoryIsEmptyResponse())));
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: const Scaffold(body: ProjectRepositoryPage(fullPath: fullPath, projectName: "demo"))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(ProjectRepositoryPage), findsOneWidget);
    expect(find.byType(RepositoryTreeItemView), findsNWidgets(5));
    expect(find.text(".gitlab/issue_templates"), findsOneWidget);
    expect(find.text("android"), findsOneWidget);
    expect(find.text("assets"), findsOneWidget);
    expect(find.text("ios"), findsOneWidget);
    expect(find.text("lib"), findsOneWidget);
    expect(find.text("View All"), findsNothing);
  });

  testWidgets("Should be able to display the empty view when the repository does not exists", (tester) async {
    await setUpMobileBinding(tester);
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfCheckRepositoryIsEmpty(fullPath))).thenAnswer((_) => Future(() => Response.of<dynamic>(checkRepositoryIsEmptyResponse(empty: true))));
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: const Scaffold(body: ProjectRepositoryPage(fullPath: fullPath, projectName: "demo"))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(ProjectRepositoryPage), findsOneWidget);
    expect(find.byType(RepositoryTreeItemView), findsNothing);
    expect(find.byType(WebViewWidget), findsNothing);
    expect(find.widgetWithText(TipsView, "No data"), findsOneWidget);
  });

  testWidgets('Should be able to switch the branch', (tester) async {
    await setUpMobileBinding(tester);
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfRepositoryTree(fullPath, '', ref: "main"))).thenAnswer((_) => Future(() => Response.of<dynamic>(repositoryTreeOnMainBranchResponse)));
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfRepositoryTree(fullPath, '', ref: "test"))).thenAnswer((_) => Future(() => Response.of<dynamic>(repositoryTreeOnTestBranchResponse)));
    when(client.post<dynamic>("/api/graphql", buildRequestBodyOfFetchFileContent(fullPath, "README.md", "main"))).thenAnswer((_) => Future(() => Response.of(readmeResponse)));
    when(client.post<dynamic>("/api/graphql", buildRequestBodyOfFetchFileContent(fullPath, "README.md", "test"))).thenAnswer((_) => Future(() => Response.of(readmeResponse)));
    when(client.post<dynamic>("/api/graphql", buildRequestBodyOfFetchFileContent(fullPath, ".gitignore", "main"))).thenAnswer((_) => Future(() => Response.of(gitIgnoreFileOnMainBranchResponse)));
    when(client.post<dynamic>("/api/graphql", buildRequestBodyOfFetchFileContent(fullPath, ".gitignore", "test"))).thenAnswer((_) => Future(() => Response.of(gitIgnoreFileOnTestBranchResponse)));
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfCheckRepositoryIsEmpty(fullPath))).thenAnswer((_) => Future(() => Response.of<dynamic>(checkRepositoryIsEmptyResponse())));
    when(client.get("/api/v4/projects/59893/repository/branches?per_page=100")).thenAnswer((_) => Future(() => Response.of(branchesResponse())));

    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: const Scaffold(body: ProjectRepositoryPage(fullPath: fullPath, projectName: "demo"))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(ProjectRepositoryPage), findsOneWidget);
    expect(find.text("main"), findsOneWidget);
    expect(find.byIcon(Icons.arrow_drop_down), findsOneWidget);
    expect(find.byType(RepositoryTreeItemView), findsNWidgets(4));
    expect(find.text(".gitlab/issue_templates"), findsOneWidget);
    expect(find.text("android"), findsOneWidget);
    expect(find.text(".gitignore"), findsOneWidget);
    expect(find.text(".gitlab-ci.pages.yml"), findsOneWidget);
    expect(find.byType(ReadmeView), findsOneWidget);

    await tester.tap((find.text(".gitignore")));
    await tester.pumpAndSettle();
    expect(find.byType(RepositoryFileContentPage), findsOneWidget);
    expect(find.widgetWithText(CommonAppBar, ".gitignore"), findsOneWidget);
    expect(find.byType(BackButton), findsOneWidget);
    expect(find.byType(ListView), findsNWidgets(2));
    expect(find.text("1"), findsOneWidget);
    expect(find.text("2"), findsOneWidget);
    expect(find.text("3"), findsOneWidget);
    expect(find.text("1 in gitignore"), findsOneWidget);
    expect(find.text("2 in gitignore"), findsOneWidget);
    expect(find.text("3 in gitignore"), findsOneWidget);
    await tester.tap(find.byType(BackButton));
    await tester.pumpAndSettle();

    await tester.tap(find.text("main"));
    await tester.pumpAndSettle();
    expect(find.text("main"), findsNWidgets(2));
    expect(find.text("test"), findsOneWidget);
    expect(find.text("dev"), findsOneWidget);

    await tester.tap(find.text("test"));
    await tester.pumpAndSettle();
    expect(find.text("main"), findsNothing);
    expect(find.text("test"), findsOneWidget);
    expect(find.byType(RepositoryTreeItemView), findsNWidgets(2));
    await tester.tap((find.text(".gitignore")));
    await tester.pumpAndSettle();
    expect(find.byType(RepositoryFileContentPage), findsOneWidget);
    expect(find.widgetWithText(CommonAppBar, ".gitignore"), findsOneWidget);
    expect(find.byType(BackButton), findsOneWidget);
    expect(find.byType(ListView), findsNWidgets(2));
    expect(find.text("1"), findsOneWidget);
    expect(find.text("2"), findsOneWidget);
    expect(find.text("3"), findsOneWidget);
    expect(find.text("4"), findsOneWidget);
    expect(find.text("1 in gitignore"), findsOneWidget);
    expect(find.text("2 in gitignore"), findsOneWidget);
    expect(find.text("commit on the test branch"), findsOneWidget);
  });
}
