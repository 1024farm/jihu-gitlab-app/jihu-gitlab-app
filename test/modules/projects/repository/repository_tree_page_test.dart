import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/widgets/http_fail_view.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_tree_data_fetcher.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_tree_page.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

import '../../../integration_tests/helper/core.dart';
import '../../../mocker/tester.dart';
import '../../../test_binding_setter.dart';

void main() {
  const String fullPath = "ultimate-plan/jihu-gitlab-app/demo";

  setUp(() async {
    TestWidgetsFlutterBinding.ensureInitialized();
    ConnectionProvider().reset(Tester.jihuLabUser());
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets('Should be able to display the http failure view ', (tester) async {
    await setUpMobileBinding(tester);
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfRepositoryTree(fullPath, ''))).thenThrow(Exception());
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: const Scaffold(body: RepositoryTreePage(fullPath: fullPath, pageTitle: "demo"))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(RepositoryTreePage), findsOneWidget);
    expect(find.byType(HttpFailView), findsOneWidget);
  });

  testWidgets('Should be able to display the empty data view ', (tester) async {
    await setUpMobileBinding(tester);
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfRepositoryTree(fullPath, ''))).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: const Scaffold(body: RepositoryTreePage(fullPath: fullPath, pageTitle: "demo"))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(RepositoryTreePage), findsOneWidget);
    expect(find.byType(TipsView), findsOneWidget);
  });
}
