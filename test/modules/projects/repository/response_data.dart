Map<String, dynamic> readmeResponse = {
  "data": {
    "project": {
      "repository": {
        "blobs": {
          "edges": [
            {
              "node": {"rawTextBlob": "The is the content of readme"}
            }
          ]
        }
      }
    }
  }
};

Map<String, dynamic> readmeOnTestBranchResponse = {
  "data": {
    "project": {
      "repository": {
        "blobs": {
          "edges": [
            {
              "node": {"rawTextBlob": "The is the content of readme on the test branch"}
            }
          ]
        }
      }
    }
  }
};

Map<String, dynamic> gradleFileResponse = {
  "data": {
    "project": {
      "repository": {
        "blobs": {
          "edges": [
            {
              "node": {
                "name": "settings.gradle",
                "rawTextBlob": "1 in settings.gradle\n2 in settings.gradle\n3 in settings.gradle",
                "simpleViewer": {"fileType": "text", "tooLarge": false}
              }
            }
          ]
        }
      }
    }
  }
};
Map<String, dynamic> gitIgnoreFileOnMainBranchResponse = {
  "data": {
    "project": {
      "repository": {
        "blobs": {
          "edges": [
            {
              "node": {
                "name": "settings.gradle",
                "rawTextBlob": "1 in gitignore\n2 in gitignore\n3 in gitignore",
                "simpleViewer": {"fileType": "text", "tooLarge": false}
              }
            }
          ]
        }
      }
    }
  }
};
Map<String, dynamic> gitIgnoreFileOnTestBranchResponse = {
  "data": {
    "project": {
      "repository": {
        "blobs": {
          "edges": [
            {
              "node": {
                "name": "settings.gradle",
                "rawTextBlob": "1 in gitignore\n2 in gitignore\n3 in gitignore\ncommit on the test branch",
                "simpleViewer": {"fileType": "text", "tooLarge": false}
              }
            }
          ]
        }
      }
    }
  }
};

Map<String, dynamic> nonTextFileResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "name": "极狐 GitLab App",
      "repository": {
        "exists": true,
        "empty": false,
        "rootRef": "main",
        "blobs": {
          "edges": [
            {
              "node": {
                "rawTextBlob": null,
                "rawPath": "/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/raw/main/assets/images/browser_icon.png",
                "name": "browser_icon.png",
                "language": null,
                "simpleViewer": {"fileType": "download", "tooLarge": false}
              }
            }
          ]
        }
      }
    }
  }
};

Map<String, dynamic> repositoryTreeResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "name": "极狐 GitLab App",
      "repository": {
        "paginatedTree": {
          "nodes": [
            {
              "trees": {
                "nodes": [
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/98d598257faec96a120c6d2dc29d2cc584bc4215",
                    "name": ".gitlab",
                    "flatPath": ".gitlab/issue_templates",
                    "type": "tree",
                    "path": ".gitlab",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/tree/eb6a42e258966bed09b5edbf95fa68c683c84d2c/.gitlab"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/0ef6c1088ddb7636a6ae2dc432bf48f11b071d76",
                    "name": "android",
                    "flatPath": "android",
                    "type": "tree",
                    "path": "android",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/tree/eb6a42e258966bed09b5edbf95fa68c683c84d2c/android"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/1e161c23368ae1aab0775e663805808b03a0238e",
                    "name": "assets",
                    "flatPath": "assets",
                    "type": "tree",
                    "path": "assets",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/tree/eb6a42e258966bed09b5edbf95fa68c683c84d2c/assets"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/63c379f59cef7a0447a215f99a9f744237106ce8",
                    "name": "ios",
                    "flatPath": "ios",
                    "type": "tree",
                    "path": "ios",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/tree/eb6a42e258966bed09b5edbf95fa68c683c84d2c/ios"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/57f3750d0000288f02a961e765ca58d916ada2cc",
                    "name": "lib",
                    "flatPath": "lib",
                    "type": "tree",
                    "path": "lib",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/tree/eb6a42e258966bed09b5edbf95fa68c683c84d2c/lib"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/52ad1b7b6dcc382d855c62cbf1de1039d041411e",
                    "name": "public",
                    "flatPath": "public",
                    "type": "tree",
                    "path": "public",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/tree/eb6a42e258966bed09b5edbf95fa68c683c84d2c/public"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/c80746080c82c5033c3f6e08da822f2f51755999",
                    "name": "scripts",
                    "flatPath": "scripts",
                    "type": "tree",
                    "path": "scripts",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/tree/eb6a42e258966bed09b5edbf95fa68c683c84d2c/scripts"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/573d443ecb67dc527c62e656c72c9e833b60ddfe",
                    "name": "test",
                    "flatPath": "test",
                    "type": "tree",
                    "path": "test",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/tree/eb6a42e258966bed09b5edbf95fa68c683c84d2c/test"
                  }
                ]
              },
              "blobs": {
                "nodes": [
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/b42f0026514e28a0017cac21f714b518c95a36e2",
                    "name": ".gitignore",
                    "flatPath": ".gitignore",
                    "type": "blob",
                    "path": ".gitignore",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/eb6a42e258966bed09b5edbf95fa68c683c84d2c/.gitignore"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/fb7dd36adedec85c384a62bd2a34c6c34d661b20",
                    "name": ".gitlab-ci.pages.yml",
                    "flatPath": ".gitlab-ci.pages.yml",
                    "type": "blob",
                    "path": ".gitlab-ci.pages.yml",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/eb6a42e258966bed09b5edbf95fa68c683c84d2c/.gitlab-ci.pages.yml"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/ae762851b6881100cba1c31fbb6fe4b54c5d4426",
                    "name": ".gitlab-ci.yml",
                    "flatPath": ".gitlab-ci.yml",
                    "type": "blob",
                    "path": ".gitlab-ci.yml",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/eb6a42e258966bed09b5edbf95fa68c683c84d2c/.gitlab-ci.yml"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/9e243a57458017222cfd6d4c17aa9dedd8a5c043",
                    "name": ".metadata",
                    "flatPath": ".metadata",
                    "type": "blob",
                    "path": ".metadata",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/eb6a42e258966bed09b5edbf95fa68c683c84d2c/.metadata"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/2d22890a66cbeaa44aaf133affe8c6ae93b19172",
                    "name": "Dockerfile",
                    "flatPath": "Dockerfile",
                    "type": "blob",
                    "path": "Dockerfile",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/eb6a42e258966bed09b5edbf95fa68c683c84d2c/Dockerfile"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/7a118b49be750543e59f7b9c55123e11322b00c6",
                    "name": "Gemfile",
                    "flatPath": "Gemfile",
                    "type": "blob",
                    "path": "Gemfile",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/eb6a42e258966bed09b5edbf95fa68c683c84d2c/Gemfile"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/9c7e62c0c9bef81f2914ad9e1f94e351710376a3",
                    "name": "Gemfile.lock",
                    "flatPath": "Gemfile.lock",
                    "type": "blob",
                    "path": "Gemfile.lock",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/eb6a42e258966bed09b5edbf95fa68c683c84d2c/Gemfile.lock"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/349b11c6c7e1b7e89b5d7afa67f4b184a070a5d9",
                    "name": "LICENSE",
                    "flatPath": "LICENSE",
                    "type": "blob",
                    "path": "LICENSE",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/eb6a42e258966bed09b5edbf95fa68c683c84d2c/LICENSE"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/a0f33a1dd71a55aef90707f748fc319a4e01dbc3",
                    "name": "README.en.md",
                    "flatPath": "README.en.md",
                    "type": "blob",
                    "path": "README.en.md",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/eb6a42e258966bed09b5edbf95fa68c683c84d2c/README.en.md"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/9e0f3cc2cd1b54cf3f83bfd60551680f0cbceac0",
                    "name": "README.md",
                    "flatPath": "README.md",
                    "type": "blob",
                    "path": "README.md",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/eb6a42e258966bed09b5edbf95fa68c683c84d2c/README.md"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/61b6c4de17c96863d24279f06b85e01b6ebbdb34",
                    "name": "analysis_options.yaml",
                    "flatPath": "analysis_options.yaml",
                    "type": "blob",
                    "path": "analysis_options.yaml",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/eb6a42e258966bed09b5edbf95fa68c683c84d2c/analysis_options.yaml"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/ee209b05909e265e1d04e2bc5948183c164f7346",
                    "name": "init.gradle",
                    "flatPath": "init.gradle",
                    "type": "blob",
                    "path": "init.gradle",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/eb6a42e258966bed09b5edbf95fa68c683c84d2c/init.gradle"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/6e5ea375ee9668f5a4a0fbe99cac85d7617f1afa",
                    "name": "jihu_gitlab_app.iml",
                    "flatPath": "jihu_gitlab_app.iml",
                    "type": "blob",
                    "path": "jihu_gitlab_app.iml",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/eb6a42e258966bed09b5edbf95fa68c683c84d2c/jihu_gitlab_app.iml"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/f2695d907be85c2b31608109fc5d677215848689",
                    "name": "pubspec.lock",
                    "flatPath": "pubspec.lock",
                    "type": "blob",
                    "path": "pubspec.lock",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/eb6a42e258966bed09b5edbf95fa68c683c84d2c/pubspec.lock"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/a86617fe74895e8fb8d3c50976056815ae7b4bc9",
                    "name": "pubspec.yaml",
                    "flatPath": "pubspec.yaml",
                    "type": "blob",
                    "path": "pubspec.yaml",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/eb6a42e258966bed09b5edbf95fa68c683c84d2c/pubspec.yaml"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/e38787b387a8e0fb72662e6b8da1723f60f8b1bc",
                    "name": "updater.json",
                    "flatPath": "updater.json",
                    "type": "blob",
                    "path": "updater.json",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/eb6a42e258966bed09b5edbf95fa68c683c84d2c/updater.json"
                  }
                ]
              }
            }
          ],
          "pageInfo": {"hasNextPage": false, "hasPreviousPage": false, "startCursor": null, "endCursor": ""}
        }
      }
    }
  }
};

Map<String, dynamic> repositoryTreeOnMainBranchResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "name": "极狐 GitLab App",
      "repository": {
        "paginatedTree": {
          "nodes": [
            {
              "trees": {
                "nodes": [
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/98d598257faec96a120c6d2dc29d2cc584bc4215",
                    "name": ".gitlab",
                    "flatPath": ".gitlab/issue_templates",
                    "type": "tree",
                    "path": ".gitlab",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/tree/eb6a42e258966bed09b5edbf95fa68c683c84d2c/.gitlab"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/0ef6c1088ddb7636a6ae2dc432bf48f11b071d76",
                    "name": "android",
                    "flatPath": "android",
                    "type": "tree",
                    "path": "android",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/tree/eb6a42e258966bed09b5edbf95fa68c683c84d2c/android"
                  }
                ]
              },
              "blobs": {
                "nodes": [
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/b42f0026514e28a0017cac21f714b518c95a36e2",
                    "name": ".gitignore",
                    "flatPath": ".gitignore",
                    "type": "blob",
                    "path": ".gitignore",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/eb6a42e258966bed09b5edbf95fa68c683c84d2c/.gitignore"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/fb7dd36adedec85c384a62bd2a34c6c34d661b20",
                    "name": ".gitlab-ci.pages.yml",
                    "flatPath": ".gitlab-ci.pages.yml",
                    "type": "blob",
                    "path": ".gitlab-ci.pages.yml",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/eb6a42e258966bed09b5edbf95fa68c683c84d2c/.gitlab-ci.pages.yml"
                  }
                ]
              }
            }
          ],
          "pageInfo": {"hasNextPage": false, "hasPreviousPage": false, "startCursor": null, "endCursor": ""}
        }
      }
    }
  }
};

Map<String, dynamic> repositoryTreeOnTestBranchResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "name": "极狐 GitLab App",
      "repository": {
        "paginatedTree": {
          "nodes": [
            {
              "trees": {
                "nodes": [
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/98d598257faec96a120c6d2dc29d2cc584bc4215",
                    "name": ".gitlab",
                    "flatPath": ".gitlab/issue_templates",
                    "type": "tree",
                    "path": ".gitlab",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/tree/eb6a42e258966bed09b5edbf95fa68c683c84d2c/.gitlab"
                  },
                ]
              },
              "blobs": {
                "nodes": [
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/b42f0026514e28a0017cac21f714b518c95a36e2",
                    "name": ".gitignore",
                    "flatPath": ".gitignore",
                    "type": "blob",
                    "path": ".gitignore",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/eb6a42e258966bed09b5edbf95fa68c683c84d2c/.gitignore"
                  },
                ]
              }
            }
          ],
          "pageInfo": {"hasNextPage": false, "hasPreviousPage": false, "startCursor": null, "endCursor": ""}
        }
      }
    }
  }
};

Map<String, dynamic> repositoryTreeInAndroidDirResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "name": "极狐 GitLab App",
      "repository": {
        "paginatedTree": {
          "nodes": [
            {
              "trees": {
                "nodes": [
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/4a0f1ff661fed3dea9ab2f1b8e732c9a35c193e6",
                    "name": "app",
                    "flatPath": "android/app",
                    "type": "tree",
                    "path": "android/app",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/tree/eb6a42e258966bed09b5edbf95fa68c683c84d2c/android/app"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/7a7a6534bc9f38a062417a6cb90527169185e852",
                    "name": "fastlane",
                    "flatPath": "android/fastlane",
                    "type": "tree",
                    "path": "android/fastlane",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/tree/eb6a42e258966bed09b5edbf95fa68c683c84d2c/android/fastlane"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/afb0322811cb86d5be969c8c8b29438856aaa008",
                    "name": "gradle",
                    "flatPath": "android/gradle/wrapper",
                    "type": "tree",
                    "path": "android/gradle",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/tree/eb6a42e258966bed09b5edbf95fa68c683c84d2c/android/gradle"
                  }
                ]
              },
              "blobs": {
                "nodes": [
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/a7ddf3f4bb78622c47b30d2a7832e7d0ee2094d7",
                    "name": ".gitignore",
                    "flatPath": "android/.gitignore",
                    "type": "blob",
                    "path": "android/.gitignore",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/eb6a42e258966bed09b5edbf95fa68c683c84d2c/android/.gitignore"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/eb0f5c3f9687e79377805567919c24ca38edd96e",
                    "name": "build.gradle",
                    "flatPath": "android/build.gradle",
                    "type": "blob",
                    "path": "android/build.gradle",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/eb6a42e258966bed09b5edbf95fa68c683c84d2c/android/build.gradle"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/94adc3a3f97aa8ae37ba567d080f94f95ee8f9b7",
                    "name": "gradle.properties",
                    "flatPath": "android/gradle.properties",
                    "type": "blob",
                    "path": "android/gradle.properties",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/eb6a42e258966bed09b5edbf95fa68c683c84d2c/android/gradle.properties"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/44e62bcf06ae649ea809590f8a861059886502e8",
                    "name": "settings.gradle",
                    "flatPath": "android/settings.gradle",
                    "type": "blob",
                    "path": "android/settings.gradle",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/eb6a42e258966bed09b5edbf95fa68c683c84d2c/android/settings.gradle"
                  }
                ]
              }
            }
          ],
          "pageInfo": {"hasNextPage": false, "hasPreviousPage": false, "startCursor": null, "endCursor": ""}
        }
      }
    }
  }
};

Map<String, dynamic> repositoryTreeLessThan6Response = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/59893",
      "name": "极狐 GitLab App",
      "repository": {
        "paginatedTree": {
          "nodes": [
            {
              "trees": {
                "nodes": [
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/98d598257faec96a120c6d2dc29d2cc584bc4215",
                    "name": ".gitlab",
                    "flatPath": ".gitlab/issue_templates",
                    "type": "tree",
                    "path": ".gitlab",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/tree/eb6a42e258966bed09b5edbf95fa68c683c84d2c/.gitlab"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/0ef6c1088ddb7636a6ae2dc432bf48f11b071d76",
                    "name": "android",
                    "flatPath": "android",
                    "type": "tree",
                    "path": "android",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/tree/eb6a42e258966bed09b5edbf95fa68c683c84d2c/android"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/1e161c23368ae1aab0775e663805808b03a0238e",
                    "name": "assets",
                    "flatPath": "assets",
                    "type": "tree",
                    "path": "assets",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/tree/eb6a42e258966bed09b5edbf95fa68c683c84d2c/assets"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/63c379f59cef7a0447a215f99a9f744237106ce8",
                    "name": "ios",
                    "flatPath": "ios",
                    "type": "tree",
                    "path": "ios",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/tree/eb6a42e258966bed09b5edbf95fa68c683c84d2c/ios"
                  },
                  {
                    "id": "gid://gitlab/Gitlab::Graphql::Representation::TreeEntry/57f3750d0000288f02a961e765ca58d916ada2cc",
                    "name": "lib",
                    "flatPath": "lib",
                    "type": "tree",
                    "path": "lib",
                    "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/tree/eb6a42e258966bed09b5edbf95fa68c683c84d2c/lib"
                  },
                ]
              },
            }
          ],
          "pageInfo": {"hasNextPage": false, "hasPreviousPage": false, "startCursor": null, "endCursor": ""}
        }
      }
    }
  }
};

Map<String, dynamic> checkRepositoryIsEmptyResponse({bool empty = false, bool exists = true, String rootRef = "main"}) {
  return {
    "data": {
      "project": {
        "id": "gid://gitlab/Project/59893",
        "name": "极狐 GitLab App",
        "repository": {"exists": exists, "empty": empty, "rootRef": rootRef}
      }
    }
  };
}

List<Map<String, dynamic>> branchesResponse() {
  return [
    {"name": "main"},
    {"name": "test"},
    {"name": "dev"},
  ];
}
