import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/readme_view_model.dart';

void main() {
  test("Should be able to fill the relative path of the images", () {
    String rawText =
        "# 极狐 GitLab App\n\n<a href='https://apps.apple.com/cn/app/jihu-gitlab/id6444783184' style=\"display: inline-block; overflow: hidden; border-radius: 13px; height: 60px;\"><img alt='下载应用，请到 Apple App Store' src=\"public/assets/ios-badge.svg\" style=\"border-radius: 13px; height: 60px;\"/></a>\n\n<a href='https://play.google.com/store/apps/details?id=cn.gitlab.app&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1' style=\"display: inline-block; overflow: hidden; border-radius: 13px; height: 60px;\"><img alt='下载应用，请到 Google Play' src='https://play.google.com/intl/en_us/badges/static/images/badges/zh-cn_badge_web_generic.png' style=\"border-radius: 13px; height: 60px;\"/></a>\n\n<a href='https://jihulab.com/api/v4/projects/59893/packages/generic/jihu-gitlab-app/latest/app-debug.apk' style=\"display: inline-block; overflow: hidden; border-radius: 13px; height: 60px;\"><img alt='本地下载安卓安装包' src='public/assets/apk-badge.png' style=\"border-radius: 13px; height: 60px;\"/></a>";
    var html = Readme(rawText: rawText, host: "https://jihulab.com", rawPath: "/raw-path-test/").value;
    expect(html, contains("""<h1>极狐 GitLab App</h1>"""));
    expect(html, contains("""src='https://jihulab.com/raw-path-test/public/assets/apk-badge.png'"""));
    expect(html, contains("src=\"https://jihulab.com/raw-path-test/public/assets/ios-badge.svg\""));
    expect(html, contains("""src='https://play.google.com/intl/en_us/badges/static/images/badges/zh-cn_badge_web_generic.png'"""));
    expect(html, contains("""<meta name="viewport" content="width=device-width, initial-scale=1.0">"""));
  });
}
