import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/uri_launcher.dart';
import 'package:jihu_gitlab_app/core/widgets/http_fail_view.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_file_content_fetcher.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_file_content_page.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../core/user_provider_test.dart';
import '../../../core/widgets/home/home_drawer_test.mocks.dart';
import '../../../finder/svg_finder.dart';
import '../../../mocker/tester.dart';
import '../../../test_binding_setter.dart';
import 'response_data.dart';

void main() {
  const String fullPath = "ultimate-plan/jihu-gitlab-app/demo";

  setUp(() async {
    TestWidgetsFlutterBinding.ensureInitialized();
    ConnectionProvider().reset(Tester.jihuLabUser());
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets('Should be able to display the http failure view ', (tester) async {
    await setUpMobileBinding(tester);
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfFetchFileContent(fullPath, 'android/settings.gradle', "main"))).thenThrow(Exception());
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(
        onGenerateRoute: onGenerateRoute,
        home: const Scaffold(
            body: RepositoryFileContentPage(
          fullPath: fullPath,
          path: "android/settings.gradle",
          name: "settings.gradle",
          webUrl: "",
          ref: "main",
        )),
      ),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(RepositoryFileContentPage), findsOneWidget);
    expect(find.byType(HttpFailView), findsOneWidget);
  });

  testWidgets('Should be able to display the hint view when the file type is not text ', (tester) async {
    var mockUriLauncher = MockUriLauncher();
    UriLauncher.instance().injectInstanceForTesting(mockUriLauncher);
    await setUpMobileBinding(tester);
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfFetchFileContent(fullPath, 'assets/images/browser_icon.png', "main")))
        .thenAnswer((_) => Future(() => Response.of(nonTextFileResponse)));
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(
        onGenerateRoute: onGenerateRoute,
        home: const Scaffold(
            body: RepositoryFileContentPage(
          fullPath: fullPath,
          path: "assets/images/browser_icon.png",
          name: "browser_icon.png",
          webUrl: "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/72f497f8ea470b7a35ec7b92492819899ab4b73a/assets/images/browser_icon.png",
          ref: "main",
        )),
      ),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(RepositoryFileContentPage), findsOneWidget);
    expect(SvgFinder("assets/images/warning.svg"), findsOneWidget);
    expect(find.text("Unable to open this file"), findsOneWidget);
    expect(find.widgetWithText(OutlinedButton, "Open in browser"), findsOneWidget);
    await tester.tap(find.widgetWithText(OutlinedButton, "Open in browser"));
    await tester.pumpAndSettle();
    verify(mockUriLauncher.launch(Uri.parse("https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/blob/72f497f8ea470b7a35ec7b92492819899ab4b73a/assets/images/browser_icon.png"),
            mode: LaunchMode.externalApplication))
        .called(1);
  });
}
