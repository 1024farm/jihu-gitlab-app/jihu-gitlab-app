import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/projects/projects_title_tab_bar.dart';

import '../../finder/text_finder.dart';

void main() {
  final tabBarTitles = ['Starred', 'Groups'];

  group('Projects Page Title Tab Bar', () {
    testWidgets('Should display tab bar with two tabs and default select index 0', (tester) async {
      TabController controller = TabController(initialIndex: 0, length: tabBarTitles.length, vsync: tester);
      final ProjectsTitleTabBarView tabBar = ProjectsTitleTabBarView(tabController: controller);

      GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();
      await tester.pumpWidget(MaterialApp(
        key: scaffoldKey,
        home: Scaffold(appBar: AppBar(flexibleSpace: SafeArea(child: tabBar))),
      ));

      final textTheme = Theme.of(scaffoldKey.currentContext!).textTheme;
      expect(find.text(tabBarTitles[0]), findsOneWidget);
      expect(find.text(tabBarTitles[1]), findsOneWidget);
      expect(getTextRenderObject(tester, tabBarTitles[0]).text.style!.fontSize, 20.0);
      expect(getTextRenderObject(tester, tabBarTitles[1]).text.style!.fontSize, textTheme.titleMedium!.fontSize);
    });

    testWidgets('Should display tab bar with default select index 1', (tester) async {
      const defaultIndex = 1;
      final firstTitle = tabBarTitles[0];
      final secondTitle = tabBarTitles[1];

      TabController controller = TabController(initialIndex: defaultIndex, length: tabBarTitles.length, vsync: tester);
      final ProjectsTitleTabBarView tabBar = ProjectsTitleTabBarView(tabController: controller);

      GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();
      await tester.pumpWidget(MaterialApp(
        key: scaffoldKey,
        home: Scaffold(appBar: AppBar(flexibleSpace: SafeArea(child: tabBar))),
      ));

      final textTheme = Theme.of(scaffoldKey.currentContext!).textTheme;
      expect(getTextRenderObject(tester, firstTitle).text.style!.fontSize, textTheme.titleMedium!.fontSize);
      expect(getTextRenderObject(tester, secondTitle).text.style!.fontSize, 20.0);
    });

    testWidgets('Should selected second tab when tap index 1', (tester) async {
      const defaultIndex = 0;
      final firstTitle = tabBarTitles[0];
      final secondTitle = tabBarTitles[1];

      final ProjectsTitleTabBarView tabBar = ProjectsTitleTabBarView(tabController: TabController(initialIndex: defaultIndex, length: tabBarTitles.length, vsync: tester));

      GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();
      await tester.pumpWidget(MaterialApp(
        key: scaffoldKey,
        home: Scaffold(appBar: AppBar(flexibleSpace: SafeArea(child: tabBar))),
      ));

      final textTheme = Theme.of(scaffoldKey.currentContext!).textTheme;

      expect(getTextRenderObject(tester, firstTitle).text.style!.fontSize, 20.0);
      expect(getTextRenderObject(tester, secondTitle).text.style!.fontSize, textTheme.titleMedium!.fontSize);

      await tester.tap(find.text(secondTitle));
      await tester.pumpAndSettle();
      expect(getTextRenderObject(tester, firstTitle).text.style!.fontSize, textTheme.titleMedium!.fontSize);
      expect(getTextRenderObject(tester, secondTitle).text.style!.fontSize, 20.0);
    });
  });
}
