import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/subgroups/subgroup_list_model.dart';

void main() {
  test("Should create project entity from map and convert to domain", () {
    var projectEntity = GroupAndProjectEntity.createProjectEntity({"id": 1, "name": "test_project", "path_with_namespace": "path_with_namespace_test"}, 9527, 59883, 1);
    expect(projectEntity.id, null);
    expect(projectEntity.iid, 1);
    expect(projectEntity.name, "test_project");
    expect(projectEntity.relativePath, "path_with_namespace_test");
    expect(projectEntity.userId, 9527);
    expect(projectEntity.parentId, 59883);
    expect(projectEntity.version, 1);
    expect(projectEntity.type, "project");

    var groupAndProject = projectEntity.asDomain();
    expect(groupAndProject.iid, 1);
    expect(groupAndProject.name, "test_project");
    expect(groupAndProject.relativePath, "path_with_namespace_test");
    expect(groupAndProject.parentId, 59883);
    expect(groupAndProject.type, SubgroupItemType.project);
  });

  test("Should create subgroup entity from map and convert to domain", () {
    var projectEntity = GroupAndProjectEntity.createSubgroupEntity({"id": 1, "name": "test_project", "full_path": "path_with_namespace_test"}, 9527, 59883, 1);
    expect(projectEntity.id, null);
    expect(projectEntity.iid, 1);
    expect(projectEntity.name, "test_project");
    expect(projectEntity.relativePath, "path_with_namespace_test");
    expect(projectEntity.userId, 9527);
    expect(projectEntity.parentId, 59883);
    expect(projectEntity.version, 1);
    expect(projectEntity.type, "subgroup");

    var groupAndProject = projectEntity.asDomain();
    expect(groupAndProject.iid, 1);
    expect(groupAndProject.name, "test_project");
    expect(groupAndProject.relativePath, "path_with_namespace_test");
    expect(groupAndProject.parentId, 59883);
    expect(groupAndProject.type, SubgroupItemType.subgroup);
  });

  test("Should create group entity from map and convert to domain", () {
    var projectEntity =
        GroupAndProjectEntity.createGroupEntity({"id": 1, "name": "test_group", "full_path": "path_with_namespace_test", "created_at": "2022-12-28T20:26:46.348+08:00"}, 9527, 59883, 1);
    expect(projectEntity.id, null);
    expect(projectEntity.iid, 1);
    expect(projectEntity.name, "test_group");
    expect(projectEntity.relativePath, "path_with_namespace_test");
    expect(projectEntity.userId, 9527);
    expect(projectEntity.parentId, 59883);
    expect(projectEntity.version, 1);
    expect(projectEntity.type, "group");
    expect(projectEntity.lastActivityAt, DateTime.parse("2022-12-28T20:26:46.348+08:00").millisecondsSinceEpoch);
    expect(projectEntity.starredAt, 0);

    var groupAndProject = projectEntity.asDomain();
    expect(groupAndProject.iid, 1);
    expect(groupAndProject.name, "test_group");
    expect(groupAndProject.relativePath, "path_with_namespace_test");
    expect(groupAndProject.parentId, 59883);
    expect(groupAndProject.type, SubgroupItemType.group);
  });

  test("Should restore from map and convert to map", () {
    var dbData = {
      "id": 1,
      "iid": 11,
      "user_id": 9527,
      "name": "test_project",
      "type": SubgroupItemType.subgroup.name,
      "relative_path": "path_with_namespace_test",
      "parent_id": 59883,
      "version": 1,
      "starred": 0,
      "last_activity_at": 0,
      "starred_at": 0
    };
    var restore = GroupAndProjectEntity.restore(dbData);
    expect(restore.id, 1);
    expect(restore.iid, 11);
    expect(restore.name, "test_project");
    expect(restore.relativePath, "path_with_namespace_test");
    expect(restore.userId, 9527);
    expect(restore.parentId, 59883);
    expect(restore.version, 1);
    expect(restore.type, "subgroup");
    expect(restore.lastActivityAt, 0);
    expect(restore.starred, 0);

    expect(restore.toMap(), dbData);
  });

  test("Should get hash code", () {
    var dbData = {
      "id": 1,
      "iid": 11,
      "user_id": 9527,
      "name": "test_project",
      "type": SubgroupItemType.subgroup.name,
      "relative_path": "path_with_namespace_test",
      "parent_id": 59883,
      "version": 1,
      "starred": 0,
      "last_activity_at": 0,
      "starred_at": 0
    };
    var restore = GroupAndProjectEntity.restore(dbData);
    expect(restore.hashCode, greaterThan(0));
  });

  group("Test sql", () {
    test("Should get the sql of create table", () {
      String expectSql = """ 
    create table groups_and_projects(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        user_id INTEGER NOT NULL,
        iid INTEGER NOT NULL,
        name TEXT ,
        type TEXT ,
        relative_path TEXT,
        parent_id int,
        version INTEGER
    );    
  """;
      expect(GroupAndProjectEntity.createTableSql, expectSql);
    });

    test("Should get the sql of add last_activity_at column", () {
      String expectSql = """ALTER TABLE groups_and_projects ADD COLUMN last_activity_at INTEGER;""";
      expect(GroupAndProjectEntity.addLastActivityAtColumn, expectSql);
    });

    test("Should get the sql of add last_activity_at column", () {
      String expectSql = """ALTER TABLE groups_and_projects ADD COLUMN starred INTEGER;""";
      expect(GroupAndProjectEntity.addStarredColumn, expectSql);
    });

    test("Should get the sql of add last_activity_at column", () {
      String expectSql = """ALTER TABLE groups_and_projects ADD COLUMN starred_at INTEGER;""";
      expect(GroupAndProjectEntity.addStarredAtColumn, expectSql);
    });
  });
}
