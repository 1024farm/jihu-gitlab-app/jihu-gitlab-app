import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/l10n/current_locale.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/subgroups/subgroup_list_model.dart';
import 'package:jihu_gitlab_app/modules/projects/projects_groups_model.dart';
import 'package:jihu_gitlab_app/modules/projects/projects_page.dart';
import 'package:jihu_gitlab_app/modules/projects/starred/projects_starred_model.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

import '../../core/net/http_request_test.mocks.dart';
import '../../integration_tests/create_issue_in_sub_group_test.mocks.dart';
import '../../mocker/tester.dart';
import '../../test_binding_setter.dart';

final client = MockHttpClient();

void main() {
  var projectsModel = ProjectsGroupsModel();
  var subgroupListModel = SubgroupListModel();
  var projectsStarredModel = ProjectsStarredModel();
  var provider = MockGroupProvider();
  projectsModel.injectDataProviderForTesting(provider);

  setUp(() {
    ConnectionProvider().reset(Tester.jihuLabUser());
    locator.registerSingleton(subgroupListModel);
    locator.registerSingleton(projectsModel);
    locator.registerSingleton(projectsStarredModel);
    when(client.get<List<dynamic>>("/api/v4/users/9527/starred_projects?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
    when(provider.loadFromLocal()).thenAnswer((_) => Future(() => [GroupAndProject(null, 118014, "highsoft", SubgroupItemType.group, "highsof-t", 0, starred: false)]));
    HttpClient.injectInstanceForTesting(client);
  });

  group('Projects Page', () {
    testWidgets('Should display project title', (tester) async {
      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
          child: const MaterialApp(
            home: Scaffold(body: ProjectsPage()),
            localizationsDelegates: [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
          )));

      await tester.pumpAndSettle();
      expect(find.text('Starred'), findsOneWidget);
      expect(find.text('Groups'), findsOneWidget);

      ConnectionProvider().fullReset();
    });

    testWidgets('Should display menu button on the top left corner', (tester) async {
      await setUpCompatibleBinding(tester);

      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
          child: const MaterialApp(
            home: Scaffold(body: ProjectsPage()),
            localizationsDelegates: [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
          )));

      await tester.pumpAndSettle();
      expect(find.byType(SvgPicture), findsOneWidget);

      ConnectionProvider().fullReset();
    });
  });

  tearDown(() {
    locator.unregister<ProjectsGroupsModel>();
    locator.unregister<SubgroupListModel>();
    locator.unregister<ProjectsStarredModel>();
  });
}
