import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project_repository.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/subgroup_and_project_provider.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/subgroups/subgroup_list_model.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../core/net/http_request_test.mocks.dart';
import '../../mocker/tester.dart';
import 'group_and_project_provider_test.mocks.dart';

@GenerateNiceMocks([MockSpec<GroupAndProjectRepository>()])
void main() {
  late SubgroupAndProjectProvider groupAndProjectProvider;
  late MockGroupAndProjectRepository repository;

  setUp(() {
    repository = MockGroupAndProjectRepository();
    groupAndProjectProvider = SubgroupAndProjectProvider(userId: 9527, parentId: 59893);
    groupAndProjectProvider.injectRepositoryForTesting(repository);
    ConnectionProvider().reset(Tester.jihuLabUser());
  });

  test("Should load groups and projects data from local", () async {
    var dbData = {
      "id": 1,
      "iid": 11,
      "user_id": 9527,
      "name": "test_project",
      "type": SubgroupItemType.subgroup.name,
      "relative_path": "path_with_namespace_test",
      "parent_id": 59883,
      "version": 1,
      "last_activity_at": 0,
      "starred_at": 0
    };
    when(repository.query(userId: 9527, parentId: 59893)).thenAnswer((realInvocation) async => Future.value([GroupAndProjectEntity.restore(dbData)]));
    var list = await groupAndProjectProvider.loadFromLocal();
    expect(list.length, 1);
    expect(list[0].iid, 11);
    expect(list[0].name, "test_project");
    expect(list[0].relativePath, "path_with_namespace_test");
  });

  test("Should sync groups and projects data from remote", () async {
    var mockHttpClient = MockHttpClient();
    HttpClient.injectInstanceForTesting(mockHttpClient);
    when(mockHttpClient.get<List<dynamic>>("/api/v4/groups/59893/subgroups?all_available=true&page=1&per_page=50")).thenAnswer((_) async => Future.value(Response.of([
          {"id": 22, "name": "tester-22-new", "full_path": "full_path_22_new"},
          {"id": 33, "name": "tester-33", "full_path": "full_path_33"}
        ])));
    when(mockHttpClient.get<Map<String, dynamic>>("/api/v4/groups/59893?all_available=true&page=1&per_page=50")).thenAnswer((_) async => Future.value(Response.of({
          "projects": [
            {"id": 44, "name": "tester-44-new", "path_with_namespace": "path_with_namespace_44_new"},
            {"id": 55, "name": "tester-55", "path_with_namespace": "path_with_namespace_55"}
          ]
        })));
    // shouldBeUpdate
    GroupAndProjectEntity existsSubgroup = GroupAndProjectEntity.restore(
        {"id": 1, "iid": 22, "user_id": 9527, "parent_id": 59893, "name": "tester-22", "relative_path": "full_path_22", "version": 4, "last_activity_at": 0, "starred_at": 0});
    GroupAndProjectEntity existsProject = GroupAndProjectEntity.restore(
        {"id": 2, "iid": 44, "user_id": 9527, "parent_id": 59893, "name": "tester-44", "relative_path": "path_with_namespace_44", "version": 4, "last_activity_at": 0, "starred_at": 0});
    when(repository.query(userId: 9527, parentId: 59893, iidList: [22, 33])).thenAnswer((realInvocation) async => Future.value([existsSubgroup]));
    when(repository.query(userId: 9527, parentId: 59893, iidList: [44, 55])).thenAnswer((realInvocation) async => Future.value([existsProject]));

    var result = await groupAndProjectProvider.syncFromRemote();
    expect(result, true);
    verify(repository.deleteLessThan(9527, 59893, SubgroupItemType.subgroup, any)).called(1);
    verify(repository.deleteLessThan(9527, 59893, SubgroupItemType.project, any)).called(1);
    verify(repository.insert(argThat(predicate((x) {
      return x is List<GroupAndProjectEntity> && x.length == 1 && x[0].iid == 33;
    }))));
    verify(repository.insert(argThat(predicate((x) {
      return x is List<GroupAndProjectEntity> && x.length == 1 && x[0].iid == 55;
    }))));

    verify(repository.update(argThat(predicate((x) {
      return x is List<GroupAndProjectEntity> && x.length == 1 && x[0].iid == 22;
    }))));
    verify(repository.update(argThat(predicate((x) {
      return x is List<GroupAndProjectEntity> && x.length == 1 && x[0].iid == 44;
    }))));
  });

  test("Should return false when sync groups and projects data from remote failure", () async {
    var mockHttpClient = MockHttpClient();
    HttpClient.injectInstanceForTesting(mockHttpClient);
    when(mockHttpClient.get<List<dynamic>>("/api/v4/groups/59893?all_available=true&page=1&per_page=50")).thenThrow(Exception());
    when(mockHttpClient.get<List<dynamic>>("/api/v4/groups/59893/subgroups?all_available=true&page=1&per_page=50")).thenThrow(Exception());
    var result = await groupAndProjectProvider.syncFromRemote();
    expect(result, false);
  });
}
