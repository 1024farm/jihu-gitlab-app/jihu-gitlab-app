import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:jihu_gitlab_app/core/clock/global_glock_setter.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/system_version_detector.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/platform_adaptive_text_field.dart';
import 'package:jihu_gitlab_app/core/widgets/single_choice_input.dart';
import 'package:jihu_gitlab_app/l10n/current_locale.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/project/project_page.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestones_fetcher.dart';
import 'package:jihu_gitlab_app/modules/projects/releases/project_releases_model.dart';
import 'package:jihu_gitlab_app/modules/projects/releases/release_asset_link_input_view.dart';
import 'package:jihu_gitlab_app/modules/projects/releases/release_creation_page.dart';
import 'package:jihu_gitlab_app/modules/projects/releases/tag_selector_page.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/project_repository_model.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

import '../../../integration_tests/helper/core.dart';
import '../../../mocker/tester.dart';
import '../../../modules/community/community_post_details_page_test.mocks.dart';
import '../../../test_binding_setter.dart';

Future<void> main() async {
  setUp(() async {
    var detector = MockSystemVersionDetector();
    when(detector.detect()).thenAnswer((_) => Future(() => SystemType.latest));
    SystemVersionDetector.injectInstanceForTesting(detector);
    TestWidgetsFlutterBinding.ensureInitialized();
    GlobalClockSetter.fixAt(DateTime(2023, 4, 28));
    initializeDateFormatting();

    ConnectionProvider().reset(Tester.jihuLabUser());
    when(client.post<dynamic>('/api/graphql', buildRequestBodyOfCheckRepositoryIsEmpty('ultimate-plan/jihu-gitlab-app/demo')))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(_checkRepositoryIsEmptyResponse(empty: true))));
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets('Should be able to create new release', (WidgetTester tester) async {
    await setUpMobileBinding(tester);
    when(client.get("/api/v4/projects/59893/repository/tags?per_page=20&page=1&search=")).thenAnswer((_) => Future(() => Response.of([
          {"name": "v1.5.0"}
        ])));

    when(client.post<dynamic>('/api/graphql', requestBodyOfQueryReleases('ultimate-plan/jihu-gitlab-app/demo', ""))).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
    when(client.post<dynamic>('/api/graphql', projectMilestoneRequestBody('ultimate-plan/jihu-gitlab-app/demo', 20, "", state: null)))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(_milestonesResponse)));
    when(client.post("/api/v4/projects/59893/releases", {
      "name": "",
      "tag_name": "v1.5.0",
      "description": "",
      "released_at": "2023-04-28T00:00:00.000",
      "milestones": [""],
      "assets": {
        "links": [
          {"url": "http://example.com", "link_type": "image", "name": "link title1"}
        ]
      }
    })).thenAnswer((_) => Future(() => Response.of({})));

    var parameters = {'projectId': 59893, 'groupId': 88966, 'showLeading': false, 'name': 'test project', 'relativePath': 'ultimate-plan/jihu-gitlab-app/demo'};
    var builder = MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => ConnectionProvider()),
        ChangeNotifierProvider(create: (context) => ProjectProvider()),
        ChangeNotifierProvider(create: (context) => LocaleProvider()),
      ],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: Scaffold(body: ProjectPage(arguments: parameters))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(ProjectPage), findsOneWidget);
    expect(find.widgetWithText(Tab, "Releases"), findsOneWidget);
    await tester.ensureVisible(find.widgetWithText(Tab, "Releases"));
    await tester.tap(find.widgetWithText(Tab, "Releases"));
    await tester.pumpAndSettle();
    expect(find.byIcon(Icons.add_box_outlined), findsOneWidget);
    await tester.tap(find.byIcon(Icons.add_box_outlined));
    await tester.pumpAndSettle();
    expect(find.text("Create issue"), findsOneWidget);
    expect(find.text("Create release"), findsOneWidget);
    await tester.tap(find.text("Create release"));
    await tester.pumpAndSettle();
    expect(find.byType(ReleaseCreationPage), findsOneWidget);
    expect(find.widgetWithText(CommonAppBar, "Create release"), findsOneWidget);
    expect(find.widgetWithText(TextButton, "Create"), findsOneWidget);
    expect(find.text("Tag name (required)"), findsOneWidget);
    expect(find.byType(SingleChoiceInput), findsNWidgets(3));
    expect(find.byIcon(Icons.add), findsNWidgets(4));
    expect(find.text("No tag selected"), findsOneWidget);
    expect(find.text("Release title"), findsOneWidget);
    expect(find.byType(PlatformAdaptiveTextField), findsOneWidget);
    await tester.tap(find.byIcon(Icons.add).first);
    await tester.pumpAndSettle();
    expect(find.byType(TagSelectorPage), findsOneWidget);
    expect(find.widgetWithText(CommonAppBar, "Select tag"), findsOneWidget);
    expect(find.widgetWithText(TextButton, "Done"), findsOneWidget);
    expect(find.text("v1.5.0"), findsOneWidget);
    expect(find.byIcon(Icons.radio_button_unchecked), findsOneWidget);
    await tester.tap(find.text("v1.5.0"));
    await tester.pumpAndSettle();
    expect(find.byIcon(Icons.radio_button_unchecked), findsNothing);
    expect(find.byIcon(Icons.check_circle), findsOneWidget);
    await tester.tap(find.widgetWithText(TextButton, "Done"));
    await tester.pumpAndSettle();
    expect(find.widgetWithText(SingleChoiceInput, "v1.5.0"), findsOneWidget);

    // select date
    expect(find.widgetWithText(SingleChoiceInput, "4/28/2023"), findsOneWidget);

    // add links
    expect(find.byKey(const Key("add-release-asset-link")), findsOneWidget);
    await tester.tap(find.byKey(const Key("add-release-asset-link")));
    await tester.pumpAndSettle();
    expect(find.byType(ReleaseAssetLinkInputView), findsOneWidget);
    expect(find.byIcon(Icons.arrow_drop_down), findsOneWidget);
    await tester.ensureVisible(find.byIcon(Icons.arrow_drop_down).last);
    await tester.tap(find.byIcon(Icons.arrow_drop_down), warnIfMissed: false);
    await tester.pumpAndSettle();
    expect(find.text("Image"), findsOneWidget);
    await tester.tap(find.text("Image"));
    await tester.pumpAndSettle();
    expect(find.text("Image"), findsOneWidget);

    await tester.tap(find.byKey(const Key("add-release-asset-link")));
    await tester.pumpAndSettle();
    expect(find.byType(ReleaseAssetLinkInputView), findsOneWidget);

    await tester.enterText(find.byKey(const Key("release-asset-link-input-url")), "http://example.com");
    await tester.enterText(find.byKey(const Key("release-asset-link-input-title")), "link title1");
    await tester.pumpAndSettle();
    await tester.tap(find.byKey(const Key("add-release-asset-link")));
    await tester.pumpAndSettle();
    expect(find.byType(ReleaseAssetLinkInputView), findsNWidgets(2));
    expect(find.byIcon(Icons.delete_outline), findsNWidgets(2));
    await tester.ensureVisible(find.byIcon(Icons.delete_outline).last);
    await tester.tap(find.byIcon(Icons.delete_outline).first);
    await tester.pumpAndSettle();
    expect(find.byType(ReleaseAssetLinkInputView), findsOneWidget);

    await tester.tap(find.widgetWithText(TextButton, "Create"));
    await tester.pumpAndSettle(const Duration(seconds: 2));
    verify(client.post("/api/v4/projects/59893/releases", {
      "name": "",
      "tag_name": "v1.5.0",
      "description": "",
      "released_at": "2023-04-28T00:00:00.000",
      "milestones": [""],
      "assets": {
        "links": [
          {"url": "http://example.com", "link_type": "image", "name": "link title1"}
        ]
      }
    })).called(1);
    verify(client.post<dynamic>('/api/graphql', requestBodyOfQueryReleases('ultimate-plan/jihu-gitlab-app/demo', ""))).called(2);
  });

  tearDown(() async {
    ConnectionProvider().fullReset();
    ProjectProvider().fullReset();
    reset(client);
    GlobalClockSetter.reset();
  });
}

Map<String, dynamic> _checkRepositoryIsEmptyResponse({bool exists = true, bool empty = false}) {
  return {
    "data": {
      "project": {
        "id": "gid://gitlab/Project/59893",
        "name": "极狐 GitLab App",
        "repository": {"exists": exists, "empty": empty, "rootRef": "main"}
      }
    }
  };
}

Map<String, dynamic> _milestonesResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/72936",
      "name": "private demo",
      "milestones": {
        "nodes": [
          {"id": "gid://gitlab/Milestone/6674", "title": "里程碑1", "startDate": "2023-03-01", "dueDate": "2023-03-31", "state": "active"},
          {"id": "gid://gitlab/Milestone/6675", "title": "里程碑2", "startDate": null, "dueDate": null, "state": "closed"}
        ],
        "pageInfo": {"hasNextPage": false, "hasPreviousPage": false, "startCursor": "eyJkdWVfZGF0ZSI6IjIwMjMtMDMtMzEiLCJpZCI6IjY3MDYifQ", "endCursor": "eyJkdWVfZGF0ZSI6bnVsbCwiaWQiOiI2Njc1In0"}
      },
    }
  }
};
