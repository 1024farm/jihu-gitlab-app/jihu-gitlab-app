import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:jihu_gitlab_app/core/clock/global_glock_setter.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/uri_launcher.dart';
import 'package:jihu_gitlab_app/core/widgets/http_fail_view.dart';
import 'package:jihu_gitlab_app/core/widgets/no_data_view.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestone_model.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestone_page.dart';
import 'package:jihu_gitlab_app/modules/projects/releases/project_release_item_view.dart';
import 'package:jihu_gitlab_app/modules/projects/releases/project_releases_model.dart';
import 'package:jihu_gitlab_app/modules/projects/releases/project_releases_page.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

import '../../../core/widgets/home/home_drawer_test.mocks.dart';
import '../../../finder/svg_finder.dart';
import '../../../integration_tests/helper/core.dart';
import '../../../mocker/tester.dart';
import '../../../test_binding_setter.dart';

void main() {
  const String fullPath = "ultimate-plan/jihu-gitlab-app/demo";

  late MockUriLauncher mockUriLauncher;
  setUp(() async {
    TestWidgetsFlutterBinding.ensureInitialized();
    ConnectionProvider().reset(Tester.jihuLabUser());
    HttpClient.injectInstanceForTesting(client);
    GlobalClockSetter.fixAt(DateTime.parse("2023-04-25 14:00:00"));
    mockUriLauncher = MockUriLauncher();
    UriLauncher.instance().injectInstanceForTesting(mockUriLauncher);
  });

  testWidgets('Should be able to display the empty data view', (tester) async {
    await setUpMobileBinding(tester);
    when(client.post<dynamic>('/api/graphql', requestBodyOfQueryReleases('ultimate-plan/jihu-gitlab-app/demo', ""))).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: const Scaffold(body: ProjectReleasesPage(fullPath: fullPath))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(NoDataView), findsOneWidget);
  });

  testWidgets('Should be able to display the http failure view', (tester) async {
    await setUpMobileBinding(tester);
    when(client.post<dynamic>('/api/graphql', requestBodyOfQueryReleases('ultimate-plan/jihu-gitlab-app/demo', ""))).thenAnswer((_) => Future(() => Response.of<dynamic>({"errors": {}})));
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(onGenerateRoute: onGenerateRoute, home: const Scaffold(body: ProjectReleasesPage(fullPath: fullPath))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(HttpFailView), findsOneWidget);
  });

  testWidgets('Should be able to display the releases', (tester) async {
    await setUpMobileBinding(tester);
    when(client.post<dynamic>('/api/graphql', requestBodyOfQueryReleases('ultimate-plan/jihu-gitlab-app/demo', ""))).thenAnswer((_) => Future(() => Response.of<dynamic>(projectReleasesResponse)));
    when(client.post<dynamic>('/api/graphql', requestBodyOfQueryReleases('ultimate-plan/jihu-gitlab-app/demo', "MQ"))).thenAnswer((_) => Future(() => Response.of<dynamic>(projectReleasesResponse)));
    when(client.post<dynamic>('/api/graphql', requestBodyOfGetFirst100IssueByMilestoneTitle('ultimate-plan/jihu-gitlab-app/demo', "里程碑1"))).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
    when(client.post<dynamic>('/api/graphql', requestBodyOfGetFirst100IssueExtraByMilestoneTitle('ultimate-plan/jihu-gitlab-app/demo', "里程碑1")))
        .thenAnswer((_) => Future(() => Response.of<dynamic>({})));
    var builder = MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(
          onGenerateRoute: onGenerateRoute,
          localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
          home: const Scaffold(body: ProjectReleasesPage(fullPath: fullPath))),
    );

    await tester.pumpWidget(builder);
    await tester.pumpAndSettle();
    expect(find.byType(ProjectReleaseItemView), findsNWidgets(2));
    expect(find.text("v1.4"), findsNWidgets(2));
    expect(find.text("Upcoming Release"), findsOneWidget);
    expect(find.text("Historical Release"), findsOneWidget);
    expect(find.text("v1.5.0"), findsOneWidget);
    expect(SvgFinder("assets/images/chevron_lg_down.svg"), findsNWidgets(1));
    expect(SvgFinder("assets/images/chevron_lg_up.svg"), findsNWidgets(1));
    expect(find.text("Milestones"), findsOneWidget);
    expect(find.text("里程碑1"), findsOneWidget);
    await tester.tap(find.text("里程碑1"));
    await tester.pumpAndSettle();
    expect(find.byType(ProjectMilestonePage), findsOneWidget);
    expect(find.byType(BackButton), findsOneWidget);
    await tester.tap(find.byType(BackButton));
    await tester.pumpAndSettle();
    expect(find.text("Assets"), findsOneWidget);
    expect(find.text("5"), findsOneWidget);
    expect(SvgFinder("assets/images/assets.svg"), findsNWidgets(4));
    expect(find.text("Source code (zip)"), findsOneWidget);
    expect(find.text("Source code (tar.gz)"), findsOneWidget);
    expect(find.text("Source code (tar.bz2)"), findsOneWidget);
    expect(find.text("Source code (tar)"), findsOneWidget);
    await tester.tap(find.text("Source code (tar)"));
    expect(SvgFinder("assets/images/link.svg"), findsNWidgets(1));
    expect(find.text("自动生成 release notes"), findsOneWidget);
    expect(SvgFinder("assets/images/commit.svg"), findsNWidgets(1));
    expect(find.text("fbd925f4"), findsOneWidget);
    expect(SvgFinder("assets/images/tag.svg"), findsNWidgets(1));
    expect(find.text("v1.4"), findsNWidgets(2));
    expect(find.textContaining("Will be released in 4 days by"), findsOneWidget);
    expect(find.byType(HtmlWidget), findsNothing);

    await tester.tap(find.text("v1.5.0"));
    await tester.pumpAndSettle();
    expect(SvgFinder("assets/images/chevron_lg_down.svg"), findsNWidgets(1));
    expect(SvgFinder("assets/images/chevron_lg_up.svg"), findsNWidgets(1));
    expect(find.text("5"), findsNothing);
    expect(find.text("4"), findsOneWidget);
    expect(find.byType(HtmlWidget), findsOneWidget);
    expect(SvgFinder("assets/images/assets.svg"), findsNWidgets(4));
    expect(SvgFinder("assets/images/chevron_up.svg"), findsOneWidget);
    await tester.tap(SvgFinder("assets/images/chevron_up.svg"));
    await tester.pumpAndSettle();
    expect(SvgFinder("assets/images/chevron_down.svg"), findsOneWidget);
    expect(SvgFinder("assets/images/assets.svg"), findsNWidgets(0));

    await tester.drag(find.byType(ProjectReleaseItemView).last, const Offset(0, -500));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    verify(client.post<dynamic>('/api/graphql', requestBodyOfQueryReleases('ultimate-plan/jihu-gitlab-app/demo', "MQ"))).called(1);
    expect(find.byType(ProjectReleaseItemView), findsNWidgets(4));
  });

  tearDown(() {
    reset(client);
    reset(mockUriLauncher);
    ConnectionProvider().fullReset();
    GlobalClockSetter.reset();
  });
}

Map<String, dynamic> projectReleasesResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/72936",
      "name": "private demo",
      "namespace": null,
      "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / private demo",
      "releases": {
        "nodes": [
          {
            "id": "gid://gitlab/Release/25371",
            "name": "v1.4",
            "tagName": "v1.4",
            "tagPath": "/ultimate-plan/jihu-gitlab-app/demo/-/tags/v1.4",
            "descriptionHtml": "",
            "releasedAt": "2023-04-30T00:00:00+08:00",
            "createdAt": "2023-04-23T19:26:30+08:00",
            "upcomingRelease": true,
            "historicalRelease": false,
            "assets": {
              "count": 5,
              "sources": {
                "nodes": [
                  {"format": "zip", "url": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/-/archive/v1.4/demo-v1.4.zip"},
                  {"format": "tar.gz", "url": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/-/archive/v1.4/demo-v1.4.tar.gz"},
                  {"format": "tar.bz2", "url": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/-/archive/v1.4/demo-v1.4.tar.bz2"},
                  {"format": "tar", "url": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/-/archive/v1.4/demo-v1.4.tar"}
                ]
              },
              "links": {
                "nodes": [
                  {
                    "id": "gid://gitlab/Releases::Link/2784",
                    "name": "自动生成 release notes",
                    "url": "https://jihulab.com/gitlab-cn/gitlab/-/issues/2872",
                    "directAssetUrl": "https://jihulab.com/gitlab-cn/gitlab/-/issues/2872",
                    "linkType": "OTHER"
                  }
                ]
              }
            },
            "evidences": {"nodes": []},
            "commit": {
              "id": "gid://gitlab/CommitPresenter/fbd925f45b9d326aec45bdc8429352854a65a5ee",
              "sha": "fbd925f45b9d326aec45bdc8429352854a65a5ee",
              "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/-/commit/fbd925f45b9d326aec45bdc8429352854a65a5ee",
              "title": "Update file test1",
              "shortId": "fbd925f4"
            },
            "author": {"id": "gid://gitlab/User/23836", "webUrl": "https://jihulab.com/jojo0", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "username": "jojo0"},
            "milestones": {
              "nodes": [
                {
                  "id": "gid://gitlab/Milestone/6704",
                  "title": "里程碑1",
                  "description": "",
                  "webPath": "/ultimate-plan/jihu-gitlab-app/demo/-/milestones/4",
                  "stats": {"totalIssuesCount": 2, "closedIssuesCount": 0}
                }
              ]
            }
          },
          {
            "id": "gid://gitlab/Release/25370",
            "name": "v1.5.0",
            "tagName": "v1.5.0",
            "tagPath": "/ultimate-plan/jihu-gitlab-app/demo/-/tags/v1.5.0",
            "descriptionHtml":
                "<h3 data-sourcepos=\"3:1-3:15\" dir=\"auto\">&#x000A;<a id=\"user-content-tag-message\" class=\"anchor\" href=\"#tag-message\" aria-hidden=\"true\"></a>Tag message</h3>&#x000A;<p data-sourcepos=\"5:1-5:13\" dir=\"auto\">version 1.5.0</p>",
            "releasedAt": "2023-04-23T19:25:50+08:00",
            "createdAt": "2023-04-23T19:25:50+08:00",
            "upcomingRelease": false,
            "historicalRelease": true,
            "assets": {
              "count": 4,
              "sources": {
                "nodes": [
                  {"format": "zip", "url": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/-/archive/v1.5.0/demo-v1.5.0.zip"},
                  {"format": "tar.gz", "url": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/-/archive/v1.5.0/demo-v1.5.0.tar.gz"},
                  {"format": "tar.bz2", "url": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/-/archive/v1.5.0/demo-v1.5.0.tar.bz2"},
                  {"format": "tar", "url": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/-/archive/v1.5.0/demo-v1.5.0.tar"}
                ]
              },
              "links": {"nodes": []}
            },
            "evidences": {
              "nodes": [
                {
                  "id": "gid://gitlab/Releases::Evidence/3706",
                  "filepath": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/-/releases/v1.5.0/evidences/3706.json",
                  "collectedAt": "2023-04-23T19:25:50+08:00",
                  "sha": "a924fdc3ef9b904cf8bdb17beba4e06fd0bc86f1962d"
                }
              ]
            },
            "commit": {
              "id": "gid://gitlab/CommitPresenter/fbd925f45b9d326aec45bdc8429352854a65a5ee",
              "sha": "fbd925f45b9d326aec45bdc8429352854a65a5ee",
              "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/-/commit/fbd925f45b9d326aec45bdc8429352854a65a5ee",
              "title": "Update file test1",
              "shortId": "fbd925f4"
            },
            "author": {"id": "gid://gitlab/User/23836", "webUrl": "https://jihulab.com/jojo0", "avatarUrl": "/uploads/-/system/user/avatar/23836/avatar.png", "username": "jojo0"},
            "milestones": {"nodes": []}
          }
        ],
        "pageInfo": {"startCursor": "eyJyZWxlYXNlZF9hdCI6IjIwMjMtMDQtMzAgMDA6MDA6MDAuMDAwMDAwMDAwICswODAwIiwiaWQiOiIyNTM3MSJ9", "hasPreviousPage": false, "hasNextPage": false, "endCursor": "MQ"}
      }
    }
  }
};
