import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connections.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/widgets/unauthorized_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/auth/auth.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/projects.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../core/net/http_request_test.mocks.dart';
import '../../../finder/text_field_hint_text_finder.dart';
import '../../../integration_tests/create_issue_in_sub_group_test.mocks.dart';
import '../../../test_data/top_groups.dart';
import '../../../test_data/user.dart';

final client = MockHttpClient();

void main() {
  setUp(() async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    await ConnectionProvider().restore();
    AppLocalizations.init();
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    // TODO: set the privacy alert not showing for some test issue: "not hit test on the specified widget"
    LocalStorage.save('privacy-policy-agreed', true);
  });

  var projectsModel = ProjectsGroupsModel();
  var subgroupListModel = SubgroupListModel();
  locator.registerSingleton(subgroupListModel);
  locator.registerSingleton(projectsModel);
  var provider = MockGroupProvider();
  group('Projects Groups Page', () {
    testWidgets('Should display Unauthorized View when not logged in', (tester) async {
      when(provider.loadFromLocal()).thenAnswer((_) => Future(() => [GroupAndProject(null, 118014, "highsoft", SubgroupItemType.group, "highsof-t", 0, starred: false)]));
      projectsModel.injectDataProviderForTesting(provider);
      when(client.get<List<dynamic>>("/api/v4/groups?top_level_only=true&page=1&per_page=20")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
      HttpClient.injectInstanceForTesting(client);

      await tester.pumpWidget(MultiProvider(providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())], child: const MaterialApp(home: Scaffold(body: ProjectsGroupsPage()))));

      await tester.pumpAndSettle();
      expect(find.byType(UnauthorizedView), findsOneWidget);

      ConnectionProvider().fullReset();
    });

    testWidgets('Should display Empty View when logged in with empty data', (tester) async {
      when(provider.loadFromLocal()).thenAnswer((_) => Future(() => []));
      projectsModel.injectDataProviderForTesting(provider);
      when(client.get<List<dynamic>>("/api/v4/groups?top_level_only=true&page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
      when(client.getWithHeader<Map<String, dynamic>>("https://example.com/api/v4/user", any)).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
      HttpClient.injectInstanceForTesting(client);

      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
          child: MaterialApp(routes: {SelfManagedLoginPage.routeName: (context) => const SelfManagedLoginPage()}, home: const Scaffold(body: ProjectsGroupsPage()))));

      await tester.pumpAndSettle();
      expect(find.byType(UnauthorizedView), findsOneWidget);

      await tester.tap(find.byType(PopupMenuButton<String>));
      await tester.pumpAndSettle();
      await tester.tap(find.byWidgetPredicate((Widget widget) => widget is PopupMenuItem<String> && widget.value == LoginMenuSet.selfManaged.name));
      await tester.pumpAndSettle();
      await tester.tap(find.text(LoginMenuSet.selfManaged.name));
      await tester.pumpAndSettle();

      expect(find.byType(SelfManagedLoginPage), findsOneWidget);
      expect(find.text('GitLab self-managed'), findsOneWidget);

      await tester.enterText(TextFieldHintTextFinder('Host: gitlab.example.com'), 'example.com');
      await tester.enterText(TextFieldHintTextFinder('Access Token'), 'access_token');
      await tester.tap(find.text('Submit'));
      projectsModel.injectDataProviderForTesting(provider);
      await tester.pumpAndSettle();
      expect(ConnectionProvider.isSelfManagedGitLab, isTrue);
      expect(find.byType(ProjectsGroupsPage), findsOneWidget);
      ConnectionProvider().fullReset();
    });

    testWidgets('Should display List View when logged in with groups data', (tester) async {
      when(provider.loadFromLocal()).thenAnswer((_) => Future(() => [GroupAndProject(null, 118014, "highsoft", SubgroupItemType.group, "highsof-t", 0, starred: false)]));
      projectsModel.injectDataProviderForTesting(provider);
      when(client.get<List<dynamic>>("/api/v4/groups?top_level_only=true&page=1&per_page=20")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>(topGroups)));
      when(client.getWithHeader<Map<String, dynamic>>("https://example.com/api/v4/user", any)).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
      HttpClient.injectInstanceForTesting(client);

      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
          child: MaterialApp(routes: {
            SelfManagedLoginPage.routeName: (context) => const SelfManagedLoginPage(),
            SubgroupPage.routeName: (context) => const SubgroupPage(arguments: {"name": "name1", "groupId": 1, "relativePath": "highsof-t"})
          }, home: const Scaffold(body: ProjectsGroupsPage()))));

      await tester.pumpAndSettle();
      expect(find.byType(UnauthorizedView), findsOneWidget);

      await tester.tap(find.byType(PopupMenuButton<String>));
      await tester.pumpAndSettle();
      await tester.tap(find.byWidgetPredicate((Widget widget) => widget is PopupMenuItem<String> && widget.value == LoginMenuSet.selfManaged.name));
      await tester.pumpAndSettle();
      await tester.tap(find.text(LoginMenuSet.selfManaged.name));
      await tester.pumpAndSettle();

      expect(find.byType(SelfManagedLoginPage), findsOneWidget);
      expect(find.text('GitLab self-managed'), findsOneWidget);

      await tester.enterText(TextFieldHintTextFinder('Host: gitlab.example.com'), 'example.com');
      await tester.enterText(TextFieldHintTextFinder('Access Token'), 'access_token');
      await tester.tap(find.text('Submit'));
      await tester.pumpAndSettle();

      expect(find.byType(ProjectsGroupsPage), findsOneWidget);
      expect(find.byType(ListView), findsOneWidget);

      ConnectionProvider().fullReset();
    });
  });
}
