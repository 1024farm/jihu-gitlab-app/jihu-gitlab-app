import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/iteration/details/iteration_model.dart';

void main() {
  test('Should get subgroupIterationIssuesRequestBody as expect', () {
    expect(subgroupIterationIssuesRequestBody('full', '1'), {
      "query": """
        query iterationIssues(\$fullPath: ID!, \$id: ID!, \$afterCursor: String = "", \$firstPageSize: Int) {
          group(fullPath: \$fullPath) {
            id
            path
            fullPath
            name
            issues(
              iterationId: [\$id]
              sort: RELATIVE_POSITION_ASC,
              after: \$afterCursor
              first: \$firstPageSize
            ) {
              ...IterationIssues
            }
          }
        }
        
        fragment IterationIssues on IssueConnection {
          count
          pageInfo {
            ...PageInfo
          }
          nodes {
            id
            iid
            title
            webUrl
            state
            projectId
            epic {
              title
            }
            weight
            author {
              id
              name
              username
              avatarUrl
            }
            assignees {
              nodes {
                id
                name
                username
                avatarUrl
              }
            }
          }
        }
        
        fragment PageInfo on PageInfo {
          hasNextPage
          endCursor
        }
""",
      "variables": {"afterCursor": '', "fullPath": 'full', "id": '1', "firstPageSize": 100}
    });
  });
}
