import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/iteration/models/cadence_iteration.dart';

var iterationData = {
  "dueDate": "2022-11-19",
  "id": "gid://gitlab/Iteration/917",
  "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/917",
  "startDate": "2022-11-12",
  "state": "closed",
  "title": null,
  "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/917",
  "__typename": "Iteration"
};

void main() {
  group('Subgroup iteration', () {
    test('Should create iteration model object', () {
      CadenceIteration iteration = CadenceIteration.init(iterationData);
      expect(iteration.id, iterationData['id']);
      expect(iteration.state, iterationData['state']);
      expect(iteration.dueDate, iterationData['dueDate']);
      expect(iteration.startDate, iterationData['startDate']);
      expect(iteration.scopedPath, iterationData['scopedPath']);
      expect(iteration.webPath, iterationData['webPath']);
    });

    test('Subgroup iteration data validate', () {
      CadenceIteration iteration = CadenceIteration.init(iterationData);
      expect(iteration.getStartDate(), "Nov 12, 2022");
      expect(iteration.getDueDate(), "Nov 19, 2022");
      expect(iteration.getState(), "Closed");
    });

    test('Subgroup iteration validate state', () {
      var iterationData2 = {
        "dueDate": "2022-12-04",
        "id": "gid://gitlab/Iteration/917",
        "scopedPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/917",
        "startDate": "2022-11-28",
        "state": "current",
        "title": null,
        "webPath": "/groups/ultimate-plan/jihu-gitlab-app/-/iterations/917",
        "__typename": "Iteration"
      };
      CadenceIteration iteration = CadenceIteration.init(iterationData);
      CadenceIteration iteration2 = CadenceIteration.init(iterationData2);
      expect(iteration.getState(), "Closed");
      expect(iteration2.getState(), "Open");
    });
  });
}
