import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/iteration/models/cadence.dart';

var response = {"id": "gid://gitlab/Iterations::Cadence/316", "title": "单周迭代", "durationInWeeks": 1, "automatic": true, "__typename": "IterationCadence"};

void main() {
  test('Should create group model object', () {
    Cadence item = Cadence.init(response);
    expect(item.id, response['id']);
    expect(item.title, response['title']);
    expect(item.automatic, response['automatic']);
  });
}
