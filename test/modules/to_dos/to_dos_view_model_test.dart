import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/modules/to_dos/to_dos_view_model.dart';
import 'package:mockito/mockito.dart';

import '../../core/net/http_request_test.mocks.dart';

void main() {
  test("Should load more in todo list model", () async {
    final client = MockHttpClient();
    when(client.get<List<dynamic>>("/api/v4/todos?page=2&per_page=20&state=pending")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([
          {'target_type': "Issue", "id": 1}
        ])));
    HttpClient.injectInstanceForTesting(client);
    var model = ToDosViewModel();
    model.init(state: ToDoState.pending);
    await model.loadMore();
    expect(model.todos.length, 1);
  });

  test("Should return false when load more exception in todo list model", () async {
    final client = MockHttpClient();
    when(client.get<List<dynamic>>("/api/v4/todos?page=2&per_page=20&state=pending")).thenThrow(Exception());
    HttpClient.injectInstanceForTesting(client);
    var model = ToDosViewModel();
    model.init(state: ToDoState.pending);
    expect(await model.loadMore(), false);
  });

  test('Should get hasNextPage in todo list model', () {
    var model = ToDosViewModel();
    model.init(state: ToDoState.pending);
    expect(model.hasNextPage, false);
  });
}
