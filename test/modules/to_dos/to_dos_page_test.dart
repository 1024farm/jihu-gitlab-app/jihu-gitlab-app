import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/global_keys.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart' as r;
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/uri_launcher.dart';
import 'package:jihu_gitlab_app/core/widgets/connection_selector.dart';
import 'package:jihu_gitlab_app/core/widgets/home/home_drawer.dart';
import 'package:jihu_gitlab_app/core/widgets/unauthorized_view.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/l10n/current_locale.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_request_page.dart';
import 'package:jihu_gitlab_app/modules/mr/models/mr_graphql_request_body.dart';
import 'package:jihu_gitlab_app/modules/to_dos/to_do_view.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../core/net/http_request_test.mocks.dart';
import '../../core/widgets/home/home_drawer_test.mocks.dart';
import '../../finder/svg_finder.dart';
import '../../finder/text_finder.dart';
import '../../mocker/tester.dart';
import '../../test_binding_setter.dart';
import '../../test_data/discussion.dart';
import '../../test_data/issue.dart';
import '../../test_data/merge_request.dart';
import '../../test_data/note.dart';
import '../../test_data/to_dos.dart';
import '../community/community_post_details_page_test.mocks.dart';

void main() {
  var params = {'projectId': 72936, 'issueId': 3242, 'issueIid': 0, 'targetId': 45345, 'targetIid': 6, 'pathWithNamespace': 'ultimate-plan/jihu-gitlab-app/demo-mr-test'};
  var mockUriLauncher = MockUriLauncher();
  late MockHttpClient client;

  setUp(() async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    await ConnectionProvider().restore();
    client = MockHttpClient();
    HttpClient.injectInstanceForTesting(client);
    when(client.get("/api/v4/projects/0/issues/0/discussions?page=1&per_page=50", useActiveConnection: true)).thenThrow(Exception());
  });

  testWidgets('Should display Unauthorized View if the user did not login', (tester) async {
    await setUpMobileBinding(tester);

    await tester.pumpWidget(MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
        child: MaterialApp(
          home: Scaffold(key: GlobalKeys.rootScaffoldKey, drawer: const HomeDrawer(), body: const ToDosPage()),
          localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
        )));

    await tester.pumpAndSettle();
    expect(find.byType(UnauthorizedView), findsOneWidget);

    ConnectionProvider().fullReset();
  });

  group('To-do item detail', () {
    testWidgets('Should display List View and show the issue details with @ message', (tester) async {
      await setUpMobileBinding(tester);

      ConnectionProvider().reset(Tester.jihuLabUser());
      when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>(todoPendingResponseData)));
      when(client.get<List<dynamic>>("/api/v4/projects/72936/issues/6/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>(discussionsData)));
      when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 6)))
          .thenAnswer((_) => Future(() => r.Response.of<dynamic>(issueDetailsGraphQLResponse)));
      when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 6), useActiveConnection: true))
          .thenAnswer((_) => Future(() => r.Response.of<dynamic>(issuePostVotesGraphQLResponse)));
      when(client.get("/api/v4/projects/72936/issues/6/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => r.Response.of([])));

      var issueDetailsModel = IssueDetailsModel();
      locator.registerSingleton(issueDetailsModel);
      var provider = MockDiscussionProvider();
      issueDetailsModel.injectDataProviderForTesting(provider);
      when(provider.loadFromLocal()).thenAnswer((_) => Future(() => [Discussion('abc123', noteList, individualNote: false)]));

      await tester.pumpWidget(MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (context) => ConnectionProvider()),
            ChangeNotifierProvider(create: (context) => ProjectProvider()),
            ChangeNotifierProvider(create: (context) => LocaleProvider())
          ],
          child: MaterialApp(
            routes: {IssueDetailsPage.routeName: (context) => IssueDetailsPage(arguments: params)},
            home: const Scaffold(body: ToDosPage()),
            localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
          )));

      await tester.pumpAndSettle();
      await tester.pump(const Duration(seconds: 1));

      expect(find.text("To Do"), findsOneWidget);
      expect(find.text("Done"), findsOneWidget);
      expect(find.text("demo mr test"), findsOneWidget);

      await tester.tap(find.text('demo mr test'));
      for (int i = 0; i < 5; i++) {
        await tester.pump(const Duration(seconds: 1));
      }

      locator.unregister(instance: issueDetailsModel);
      ConnectionProvider().fullReset();
    });

    testWidgets('Should display List View and show the issue details with assign message', (tester) async {
      await setUpMobileBinding(tester);

      ConnectionProvider().reset(Tester.jihuLabUser());
      when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>(todoPendingResponseData)));
      when(client.get<List<dynamic>>("/api/v4/projects/59893/issues/217/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>(discussionsData)));
      when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 217)))
          .thenAnswer((_) => Future(() => r.Response.of<dynamic>(issueDetailsGraphQLResponse)));
      when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 217), useActiveConnection: true))
          .thenAnswer((_) => Future(() => r.Response.of<dynamic>(issuePostVotesGraphQLResponse)));
      when(client.get("/api/v4/projects/59893/issues/217/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => r.Response.of([])));

      var issueDetailsModel = IssueDetailsModel();
      locator.registerSingleton(issueDetailsModel);
      var provider = MockDiscussionProvider();
      issueDetailsModel.injectDataProviderForTesting(provider);
      when(provider.loadFromLocal()).thenAnswer((_) => Future(() => [Discussion('abc123', noteList, individualNote: false)]));

      await tester.pumpWidget(MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (context) => ConnectionProvider()),
            ChangeNotifierProvider(create: (context) => ProjectProvider()),
            ChangeNotifierProvider(create: (context) => LocaleProvider())
          ],
          child: MaterialApp(
            routes: {
              IssueDetailsPage.routeName: (context) => const IssueDetailsPage(arguments: {
                    'projectId': 59893,
                    'issueId': 142971,
                    'issueIid': 0,
                    'targetId': 217,
                    'targetIid': 217,
                    'targetUrl': 'https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/issues/217',
                    'pathWithNamespace': 'ultimate-plan/jihu-gitlab-app/jihu-gitlab-app'
                  })
            },
            home: const Scaffold(body: ToDosPage()),
            localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
          )));

      await tester.pumpAndSettle();
      await tester.pump(const Duration(seconds: 1));

      expect(find.text("To Do"), findsOneWidget);
      expect(find.text("test project"), findsOneWidget);

      await tester.tap(find.text('test project'));
      for (int i = 0; i < 5; i++) {
        await tester.pump(const Duration(seconds: 1));
      }

      expect(find.byType(IssueDetailsPage), findsOneWidget);

      locator.unregister(instance: issueDetailsModel);
      ConnectionProvider().fullReset();
    });

    testWidgets('Should enter merge request view when tap mr in todo view', (tester) async {
      await setUpMobileBinding(tester);
      ConnectionProvider().reset(Tester.jihuLabUser());
      when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>(todoPendingResponseData)));
      when(client.post<dynamic>("/api/v4/todos/143074/mark_as_done", {})).thenAnswer((_) => Future(() => r.Response.of<dynamic>(markAsDoneResponse)));
      when(client.get<Map<String, dynamic>>('/api/v4/projects/59893/merge_requests/84/approvals')).thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>({"id": 1, "iid": 1})));

      var uri = Uri.parse('https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/merge_requests/84');
      when(client.get('/api/v4/projects/59893')).thenAnswer((_) => Future(() => r.Response.of<Map>({"path_with_namespace": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app"})));
      when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 84)))
          .thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>(mrGraphQLDraftResponse)));
      when(client.post('/api/graphql', getPaidMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 84)))
          .thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>(paidMrRebaseGraphQLResponse)));
      when(client.post('/api/graphql', queryJobsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 84, ''))).thenAnswer((_) => Future(() => r.Response.of(jobsGraphQLResponse)));
      when(client.post("/api/graphql", queryCommitsGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 84, ''))).thenAnswer((_) => Future(() => r.Response.of(commitsGraphQLResponse)));
      when(client.get<List<Map<String, dynamic>>>("/api/v4/projects/59893/merge_requests/84/diffs?page=1&per_page=20")).thenAnswer((_) => Future(() => r.Response.of<List<Map<String, dynamic>>>([])));

      when(mockUriLauncher.canLaunch(uri)).thenAnswer((realInvocation) => Future(() => true));
      when(mockUriLauncher.launch(uri, mode: LaunchMode.externalApplication)).thenAnswer((realInvocation) => Future(() => true));
      UriLauncher.instance().injectInstanceForTesting(mockUriLauncher);
      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
          child: MaterialApp(
            routes: {
              MergeRequestPage.routeName: (context) =>
                  const MergeRequestPage(arguments: {'projectId': 59893, 'mergeRequestIid': 84, 'projectName': "merge request item", 'fullPath': 'ultimate-plan/jihu-gitlab-app/jihu-gitlab-app'})
            },
            home: const Scaffold(body: ToDosPage()),
            localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
          )));

      await tester.pumpAndSettle();
      expect(find.text("To Do"), findsOneWidget);
      expect(find.text("merge request item"), findsOneWidget);

      await tester.tap(find.text('merge request item'));
      for (int i = 0; i < 10; i++) {
        await tester.pump(const Duration(seconds: 1));
      }
      expect(find.text("merge request item !84"), findsOneWidget);
      UriLauncher.instance().fullReset();
      ConnectionProvider().fullReset();
    });

    testWidgets('Should be able to show the issue detail after click the issue item', (tester) async {
      await setUpDesktopBinding(tester);
      ConnectionProvider().reset(Tester.jihuLabUser());

      when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>(todoPendingResponseData)));
      when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=done")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>([])));
      when(client.get<List<dynamic>>("/api/v4/projects/72936/issues/6/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>([])));
      when(client.get<List<dynamic>>("/api/v4/projects/59893/issues/217/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>([])));
      when(client.post<Map<String, dynamic>>("/api/v4/todos/143191/mark_as_done", {})).thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>({"id": 143191, "state": "done"})));
      when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 6)))
          .thenAnswer((_) => Future(() => r.Response.of<dynamic>(issueDetailsGraphQLResponse)));
      when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 217)))
          .thenAnswer((_) => Future(() => r.Response.of<dynamic>(issueDetailsGraphQLResponse)));
      when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 6), useActiveConnection: true))
          .thenAnswer((_) => Future(() => r.Response.of<dynamic>(issuePostVotesGraphQLResponse)));
      when(client.get("/api/v4/projects/72936/issues/6/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => r.Response.of([])));
      when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 217), useActiveConnection: true))
          .thenAnswer((_) => Future(() => r.Response.of<dynamic>(issuePostVotesGraphQLResponse)));
      when(client.get("/api/v4/projects/59893/issues/217/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => r.Response.of([])));

      var issueDetailsModel = IssueDetailsModel();
      locator.registerSingleton(issueDetailsModel);
      var provider = MockDiscussionProvider();
      issueDetailsModel.injectDataProviderForTesting(provider);
      when(provider.loadFromLocal()).thenAnswer((_) => Future(() => [Discussion('abc123', noteList, individualNote: false)]));
      await tester.pumpWidget(MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (context) => ConnectionProvider()),
            ChangeNotifierProvider(create: (context) => ProjectProvider()),
            ChangeNotifierProvider(create: (context) => LocaleProvider())
          ],
          child: MaterialApp(
            onGenerateRoute: onGenerateRoute,
            home: const Scaffold(body: ToDosPage()),
            localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
          )));
      await tester.pumpAndSettle();
      expect(find.text("To Do"), findsOneWidget);
      expect(find.text("Done"), findsOneWidget);
      await tester.tap(find.text("Done"));
      await tester.pumpAndSettle();

      await tester.tap(find.text("To Do"));
      await tester.pumpAndSettle();

      expect(find.text("demo mr test"), findsOneWidget);
      expect(find.text("test project"), findsOneWidget);
      await tester.tap(find.text("demo mr test"));
      await tester.pumpAndSettle(const Duration(milliseconds: 1001));
      expect(find.byType(IssueDetailsPage), findsOneWidget);

      await tester.tap(find.text("test project"));
      await tester.pumpAndSettle(const Duration(seconds: 1));
      expect(find.byType(IssueDetailsPage), findsOneWidget);

      ConnectionProvider().fullReset();

      locator.unregister(instance: issueDetailsModel);
    });
  });

  group('To-do item mark as done', () {
    testWidgets('Should display confirm alert of done the todo item when come back from  the issue details of @ item', (tester) async {
      await setUpCompatibleBinding(tester);

      ConnectionProvider().reset(Tester.jihuLabUser());
      when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>(todoPendingResponseData)));
      when(client.get<List<dynamic>>("/api/v4/projects/72936/issues/6/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>(discussionsData)));
      when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 6)))
          .thenAnswer((_) => Future(() => r.Response.of<dynamic>(issueDetailsGraphQLResponse)));
      when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 6), useActiveConnection: true))
          .thenAnswer((_) => Future(() => r.Response.of<dynamic>(issuePostVotesGraphQLResponse)));
      when(client.get("/api/v4/projects/72936/issues/6/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => r.Response.of([])));

      var issueDetailsModel = IssueDetailsModel();
      locator.registerSingleton(issueDetailsModel);
      var provider = MockDiscussionProvider();
      issueDetailsModel.injectDataProviderForTesting(provider);
      when(provider.loadFromLocal()).thenAnswer((_) => Future(() => [Discussion('abc123', noteList, individualNote: false)]));

      params['showLeading'] = true;
      await tester.pumpWidget(MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (context) => ConnectionProvider()),
            ChangeNotifierProvider(create: (context) => ProjectProvider()),
            ChangeNotifierProvider(create: (context) => LocaleProvider())
          ],
          child: MaterialApp(
            routes: {IssueDetailsPage.routeName: (context) => IssueDetailsPage(arguments: params)},
            home: const Scaffold(body: ToDosPage()),
            localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
          )));

      await tester.pumpAndSettle();

      expect(find.text("To Do"), findsOneWidget);
      expect(find.text("demo mr test"), findsOneWidget);

      await tester.tap(find.text('demo mr test'));
      for (int i = 0; i < 5; i++) {
        await tester.pump(const Duration(seconds: 1));
      }

      expect(find.byType(IssueDetailsPage), findsOneWidget);
      await tester.tap(find.byType(BackButton));
      await tester.pumpAndSettle();
      expect(find.byType(ToDosPage), findsOneWidget);

      expect(find.byType(CupertinoAlertDialog), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().markTodoAsDoneTitle), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().markTodoAsDoneContent), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().cancel), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().yes), findsOneWidget);

      locator.unregister(instance: issueDetailsModel);
      ConnectionProvider().fullReset();
    });

    testWidgets('Should display confirm alert of done the todo item when come back from the Merge Request message', (tester) async {
      await setUpMobileBinding(tester);

      ConnectionProvider().reset(Tester.jihuLabUser());
      when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>(todoPendingResponseData)));
      when(client.post<dynamic>("/api/v4/todos/143074/mark_as_done", {})).thenAnswer((_) => Future(() => r.Response.of<dynamic>(markAsDoneResponse)));
      when(client.get<Map<String, dynamic>>("/api/v4/projects/59893/merge_requests/371/approvals")).thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>(approvalResponse)));
      when(client.get('/api/v4/projects/59893')).thenAnswer((_) => Future(() => r.Response.of<Map>({"path_with_namespace": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app"})));
      when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>(mrRebaseGraphQLResponse)));
      when(client.post('/api/graphql', getPaidMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>(paidMrRebaseGraphQLResponse)));
      when(client.post('/api/graphql', queryJobsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371, ''))).thenAnswer((_) => Future(() => r.Response.of(jobsGraphQLResponse)));
      when(client.post("/api/graphql", queryCommitsGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 371, '')))
          .thenAnswer((_) => Future(() => r.Response.of(commitsGraphQLResponse)));
      when(client.get<List<Map<String, dynamic>>>("/api/v4/projects/59893/merge_requests/371/diffs?page=1&per_page=20")).thenAnswer((_) => Future(() => r.Response.of<List<Map<String, dynamic>>>([])));

      var parameters = {'projectId': 59893, 'projectName': "demo", 'mergeRequestIid': 371, 'fullPath': 'ultimate-plan/jihu-gitlab-app/jihu-gitlab-app'};
      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
          child: MaterialApp(
            routes: {MergeRequestPage.routeName: (context) => MergeRequestPage(arguments: parameters)},
            home: const Scaffold(body: ToDosPage()),
          )));

      await tester.pumpAndSettle();
      expect(find.text("To Do"), findsOneWidget);
      expect(find.text("merge request item"), findsOneWidget);

      await tester.tap(find.text('merge request item'));
      await tester.pumpAndSettle();
      expect(find.byType(MergeRequestPage), findsOneWidget);
      expect(find.text('demo !371'), findsOneWidget);
      expect(find.text('Draft: Resolve "Feature: 开发人员 Rebase MR"'), findsOneWidget);
      await tester.tap(find.byType(BackButton));
      await tester.pumpAndSettle();

      expect(find.byType(CupertinoAlertDialog), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().markTodoAsDoneTitle), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().markTodoAsDoneContent), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().cancel), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().yes), findsOneWidget);

      await tester.tap(find.text(AppLocalizations.dictionary().yes));
      await tester.pumpAndSettle();
      expect(find.byType(CupertinoAlertDialog), findsNothing);

      UriLauncher.instance().fullReset();
      ConnectionProvider().fullReset();
    });

    testWidgets('Should display data change nothing when cancel mark item as done for the Merge Request message', (tester) async {
      await setUpMobileBinding(tester);

      ConnectionProvider().reset(Tester.jihuLabUser());
      when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>(todoPendingResponseData)));
      when(client.post<dynamic>("/api/v4/todos/143074/mark_as_done", {})).thenAnswer((_) => Future(() => r.Response.of<dynamic>(markAsDoneResponse)));
      when(client.get<Map<String, dynamic>>("/api/v4/projects/59893/merge_requests/371/approvals")).thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>(approvalResponse)));
      when(client.get('/api/v4/projects/59893')).thenAnswer((_) => Future(() => r.Response.of<Map>({"path_with_namespace": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app"})));
      when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>(mrRebaseGraphQLResponse)));
      when(client.post('/api/graphql', getPaidMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>(paidMrRebaseGraphQLResponse)));
      when(client.post('/api/graphql', queryJobsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371, ''))).thenAnswer((_) => Future(() => r.Response.of(jobsGraphQLResponse)));
      when(client.post("/api/graphql", queryCommitsGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 371, '')))
          .thenAnswer((_) => Future(() => r.Response.of(commitsGraphQLResponse)));
      when(client.get<List<Map<String, dynamic>>>("/api/v4/projects/59893/merge_requests/371/diffs?page=1&per_page=20")).thenAnswer((_) => Future(() => r.Response.of<List<Map<String, dynamic>>>([])));

      var parameters = {'projectId': 59893, 'projectName': "demo", 'mergeRequestIid': 371, 'fullPath': 'ultimate-plan/jihu-gitlab-app/jihu-gitlab-app'};
      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
          child: MaterialApp(
            routes: {MergeRequestPage.routeName: (context) => MergeRequestPage(arguments: parameters)},
            home: const Scaffold(body: ToDosPage()),
          )));

      await tester.pumpAndSettle();
      expect(find.text("To Do"), findsOneWidget);
      expect(find.text("merge request item"), findsOneWidget);

      await tester.tap(find.text('merge request item'));
      await tester.pumpAndSettle();
      expect(find.byType(MergeRequestPage), findsOneWidget);
      expect(find.text('demo !371'), findsOneWidget);
      expect(find.text('Draft: Resolve "Feature: 开发人员 Rebase MR"'), findsOneWidget);
      await tester.tap(find.byType(BackButton));
      await tester.pumpAndSettle();

      expect(find.byType(CupertinoAlertDialog), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().markTodoAsDoneTitle), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().markTodoAsDoneContent), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().cancel), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().yes), findsOneWidget);

      await tester.tap(find.text(AppLocalizations.dictionary().cancel));
      await tester.pumpAndSettle();
      expect(find.byType(CupertinoAlertDialog), findsNothing);
      expect(find.textContaining("merge request item"), findsOneWidget);

      UriLauncher.instance().fullReset();
      ConnectionProvider().fullReset();
    });

    testWidgets('Should display mark todo item as done fail notice when mark todo item as done request error for the Merge Request message', (tester) async {
      await setUpMobileBinding(tester);

      ConnectionProvider().reset(Tester.jihuLabUser());
      when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>(todoPendingResponseData)));
      when(client.post<dynamic>("/api/v4/todos/143074/mark_as_done", {})).thenThrow(Exception());
      when(client.get<Map<String, dynamic>>("/api/v4/projects/59893/merge_requests/371/approvals")).thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>(approvalResponse)));
      when(client.get('/api/v4/projects/59893')).thenAnswer((_) => Future(() => r.Response.of<Map>({"path_with_namespace": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app"})));
      when(client.post('/api/graphql', getMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>(mrRebaseGraphQLResponse)));
      when(client.post('/api/graphql', getPaidMergeRequestDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371)))
          .thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>(paidMrRebaseGraphQLResponse)));
      when(client.post('/api/graphql', queryJobsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/jihu-gitlab-app', 371, ''))).thenAnswer((_) => Future(() => r.Response.of(jobsGraphQLResponse)));
      when(client.post("/api/graphql", queryCommitsGraphQLRequestBody("ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 371, '')))
          .thenAnswer((_) => Future(() => r.Response.of(commitsGraphQLResponse)));
      when(client.get<List<Map<String, dynamic>>>("/api/v4/projects/59893/merge_requests/371/diffs?page=1&per_page=20")).thenAnswer((_) => Future(() => r.Response.of<List<Map<String, dynamic>>>([])));

      var parameters = {'projectId': 59893, 'projectName': "demo", 'mergeRequestIid': 371, 'fullPath': 'ultimate-plan/jihu-gitlab-app/jihu-gitlab-app'};
      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
          child: MaterialApp(
            routes: {MergeRequestPage.routeName: (context) => MergeRequestPage(arguments: parameters)},
            home: const Scaffold(body: ToDosPage()),
          )));

      await tester.pumpAndSettle();
      expect(find.text("To Do"), findsOneWidget);
      expect(find.text("merge request item"), findsOneWidget);

      await tester.tap(find.text('merge request item'));
      await tester.pumpAndSettle();
      expect(find.byType(MergeRequestPage), findsOneWidget);
      expect(find.text('demo !371'), findsOneWidget);
      expect(find.text('Draft: Resolve "Feature: 开发人员 Rebase MR"'), findsOneWidget);
      await tester.tap(find.byType(BackButton));
      await tester.pumpAndSettle();

      expect(find.byType(CupertinoAlertDialog), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().markTodoAsDoneTitle), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().markTodoAsDoneContent), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().cancel), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().yes), findsOneWidget);

      await tester.tap(find.text(AppLocalizations.dictionary().yes));
      await tester.pumpAndSettle();
      expect(find.byType(CupertinoAlertDialog), findsNothing);

      UriLauncher.instance().fullReset();
      ConnectionProvider().fullReset();
    });

    testWidgets('Should be able to refresh the done list after some item is marked as done', (tester) async {
      await setUpMobileBinding(tester);
      ConnectionProvider().reset(Tester.jihuLabUser());

      when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>(todoPendingResponseData)));
      when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=done")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>([])));
      when(client.get<List<dynamic>>("/api/v4/projects/72936/issues/6/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>([])));
      when(client.post<Map<String, dynamic>>("/api/v4/todos/143191/mark_as_done", {})).thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>({"id": 143191, "state": "done"})));
      when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 6)))
          .thenAnswer((_) => Future(() => r.Response.of<dynamic>(issueDetailsGraphQLResponse)));
      when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 6), useActiveConnection: true))
          .thenAnswer((_) => Future(() => r.Response.of<dynamic>(issuePostVotesGraphQLResponse)));
      when(client.get("/api/v4/projects/72936/issues/6/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => r.Response.of([])));

      var issueDetailsModel = IssueDetailsModel();
      locator.registerSingleton(issueDetailsModel);
      var provider = MockDiscussionProvider();
      issueDetailsModel.injectDataProviderForTesting(provider);
      when(provider.loadFromLocal()).thenAnswer((_) => Future(() => [Discussion('abc123', noteList, individualNote: false)]));
      await tester.pumpWidget(MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (context) => ConnectionProvider()),
            ChangeNotifierProvider(create: (context) => ProjectProvider()),
            ChangeNotifierProvider(create: (context) => LocaleProvider())
          ],
          child: MaterialApp(
            onGenerateRoute: onGenerateRoute,
            home: const Scaffold(body: ToDosPage()),
            localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
          )));
      await tester.pumpAndSettle();
      expect(find.text("To Do"), findsOneWidget);
      expect(find.text("Done"), findsOneWidget);
      await tester.tap(find.text("Done"));
      await tester.pumpAndSettle();

      await tester.tap(find.text("To Do"));
      await tester.pumpAndSettle();
      verify(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).called(1);

      expect(find.text("demo mr test"), findsOneWidget);
      await tester.tap(find.text("demo mr test"));

      for (int i = 0; i < 5; i++) {
        await tester.pump(const Duration(seconds: 1));
      }

      expect(find.byType(IssueDetailsPage), findsOneWidget);
      await tester.tap(find.byType(BackButton));
      await tester.pumpAndSettle();
      expect(find.byType(ToDosPage), findsOneWidget);
      expect(find.byType(CupertinoAlertDialog), findsOneWidget);
      expect(find.text("Yes"), findsOneWidget);
      await tester.tap(find.text("Yes"));
      await tester.pumpAndSettle();
      // await tester.tap(find.text("Done"));
      // await tester.pumpAndSettle();

      ConnectionProvider().fullReset();

      locator.unregister(instance: issueDetailsModel);
    });

    testWidgets('Should be able to mark to do item as done', (tester) async {
      await setUpMobileBinding(tester);
      ConnectionProvider().reset(Tester.jihuLabUser());

      var pendingTodos = todoPendingResponseData.sublist(0, 1);
      when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>(pendingTodos)));
      when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=done")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>([])));
      when(client.get<List<dynamic>>("/api/v4/projects/72936/issues/6/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>([])));
      when(client.post<Map<String, dynamic>>("/api/v4/todos/143191/mark_as_done", {})).thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>({"id": 143191, "state": "done"})));
      when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 6)))
          .thenAnswer((_) => Future(() => r.Response.of<dynamic>(issueDetailsGraphQLResponse)));

      var provider = MockDiscussionProvider();
      when(provider.loadFromLocal()).thenAnswer((_) => Future(() => [Discussion('abc123', noteList, individualNote: false)]));
      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
          child: MaterialApp(
            onGenerateRoute: onGenerateRoute,
            home: const Scaffold(body: ToDosPage()),
            localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
          )));
      await tester.pumpAndSettle();
      expect(find.text("To Do"), findsOneWidget);
      expect(find.text("demo mr test"), findsOneWidget);
      var svgFinder = SvgFinder("assets/images/mark_as_done.svg");
      expect(svgFinder, findsOneWidget);
      await tester.tap(svgFinder);
      await tester.pumpAndSettle(const Duration(seconds: 1));
      verify(client.post<Map<String, dynamic>>("/api/v4/todos/143191/mark_as_done", {})).called(1);
      verify(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).called(1);
      expect(find.text("demo mr test"), findsNothing);
      expect(svgFinder, findsNothing);
      ConnectionProvider().fullReset();
    });

    testWidgets('Should be able to refresh the todo list after some item is marked as done', (tester) async {
      await setUpMobileBinding(tester);
      ConnectionProvider().reset(Tester.jihuLabUser());

      var pendingTodos = todoPendingResponseData.sublist(0, 1);
      when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>(pendingTodos)));
      when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=done")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>([])));
      when(client.get<List<dynamic>>("/api/v4/projects/72936/issues/6/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>([])));
      when(client.post<Map<String, dynamic>>("/api/v4/todos/143191/mark_as_done", {})).thenAnswer((_) => Future(() => r.Response.of<Map<String, dynamic>>({"id": 143191, "state": "done"})));
      when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 6)))
          .thenAnswer((_) => Future(() => r.Response.of<dynamic>(issueDetailsGraphQLResponse)));
      when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 6), useActiveConnection: true))
          .thenAnswer((_) => Future(() => r.Response.of<dynamic>(issuePostVotesGraphQLResponse)));
      when(client.get("/api/v4/projects/72936/issues/6/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => r.Response.of([])));

      var issueDetailsModel = IssueDetailsModel();
      locator.registerSingleton(issueDetailsModel);
      var provider = MockDiscussionProvider();
      issueDetailsModel.injectDataProviderForTesting(provider);

      when(provider.loadFromLocal()).thenAnswer((_) => Future(() => [Discussion('abc123', noteList, individualNote: false)]));
      await tester.pumpWidget(MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (context) => ConnectionProvider()),
            ChangeNotifierProvider(create: (context) => ProjectProvider()),
            ChangeNotifierProvider(create: (context) => LocaleProvider())
          ],
          child: MaterialApp(
            onGenerateRoute: onGenerateRoute,
            home: const Scaffold(body: ToDosPage()),
            localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
          )));
      await tester.pumpAndSettle();
      expect(find.text("To Do"), findsOneWidget);
      expect(find.text("Done"), findsOneWidget);

      expect(find.text("demo mr test"), findsOneWidget);
      await tester.tap(find.text("demo mr test"));
      await tester.pumpAndSettle();

      expect(find.byType(IssueDetailsPage), findsOneWidget);
      await tester.tap(find.byType(BackButton));
      await tester.pumpAndSettle();
      expect(find.byType(ToDosPage), findsOneWidget);
      expect(find.byType(CupertinoAlertDialog), findsOneWidget);
      expect(find.text("Yes"), findsOneWidget);

      await tester.tap(find.text("Yes"));
      await tester.pumpAndSettle();
      verify(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).called(1);
      expect(find.text("demo mr test"), findsNothing);
      ConnectionProvider().fullReset();
      locator.unregister(instance: issueDetailsModel);
    });
  });

  group('Drawer', () {
    testWidgets('Should display menu button on the left top corner', (tester) async {
      await setUpCompatibleBinding(tester);

      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
          child: const MaterialApp(
            home: Scaffold(body: ToDosPage()),
          )));

      await tester.pumpAndSettle();
      expect(find.byType(SvgPicture), findsOneWidget);

      ConnectionProvider().fullReset();
    });

    testWidgets('Should display drawer view with version when user tapped the menu button on the left top corner after login', (tester) async {
      await setUpCompatibleBinding(tester);

      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
          child: MaterialApp(
            home: Scaffold(key: GlobalKeys.rootScaffoldKey, drawer: const HomeDrawer(), body: const ToDosPage()),
          )));

      await tester.pumpAndSettle();
      await tester.tap(SvgFinder("assets/images/menu.svg"));
      await tester.pumpAndSettle();

      expect(find.byType(Drawer), findsOneWidget);
      expect(find.textContaining('Version'), findsOneWidget);

      ConnectionProvider().fullReset();
    });
  });

  group('Add account', () {
    testWidgets('Should display correct account after add new account', (tester) async {
      await setUpCompatibleBinding(tester);

      ConnectionProvider().reset(Tester.jihuLabUser());
      when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>([])));

      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
          child: MaterialApp(
            onGenerateRoute: onGenerateRoute,
            home: const Scaffold(body: ToDosPage()),
          )));

      await tester.pumpAndSettle();
      expect(find.byWidgetPredicate((widget) => widget is ConnectionSelector && widget.key == const Key('ToDosConnectionSelector')), findsOneWidget);
      expect(find.byType(ToDoView), findsNothing);

      await tester.tap(find.byKey(const Key('jihulab_tester')));
      await tester.pumpAndSettle();
      expect(find.text('@jihulab_tester'), findsNWidgets(1));
      expect(find.byType(PopupMenuDivider), findsNWidgets(1));
      await tester.tap(find.byKey(const Key('AddAccountButton')));
      await tester.pumpAndSettle();
      expect(find.widgetWithText(MenuItemButton, 'Self-Managed'), findsOneWidget);
      expect(find.widgetWithText(MenuItemButton, 'jihulab.com'), findsOneWidget);
      expect(find.widgetWithText(MenuItemButton, 'gitlab.com'), findsOneWidget);

      expect(getTextRenderObjectFromMenuItemButton(tester, 'jihulab.com').text.style!.color, const Color(0xFF87878C));
      expect(find.text('Already used'), findsNWidgets(1));
      expect(find.byType(PopupMenuDivider), findsNWidgets(3));

      expect(find.byType(ToDosPage), findsOneWidget);
      expect(find.byType(LoginPage), findsNothing);
      expect(find.byType(SelfManagedLoginPage), findsNothing);

      // Do not showing login page when tap the logged in account type
      await tester.tap(find.widgetWithText(MenuItemButton, 'jihulab.com'));
      await tester.pumpAndSettle(const Duration(seconds: 2));
      expect(find.byType(ToDosPage), findsOneWidget);
      expect(find.byType(LoginPage), findsNothing);
      expect(find.byType(SelfManagedLoginPage), findsNothing);

      // Go to login gitlab.com
      await tester.tap(find.byKey(const Key('AddAccountButton')));
      await tester.pumpAndSettle();
      await tester.tap(find.widgetWithText(MenuItemButton, 'gitlab.com'));
      await tester.pumpAndSettle();
      expect(find.byType(ToDosPage), findsNothing);
      expect(find.byType(LoginPage), findsOneWidget);

      when(client.get<List<dynamic>>("/api/v4/todos?page=1&per_page=20&state=pending")).thenAnswer((_) => Future(() => r.Response.of<List<dynamic>>(todoPendingResponseData)));
      ConnectionProvider().injectConnectionForTest(Tester.gitLabUser());
      await tester.tap(find.byType(BackButton));
      await tester.pumpAndSettle();
      expect(find.byType(ToDoView), findsWidgets);
      expect(find.byKey(const Key('jihulab_tester')), findsNothing);
      expect(find.byKey(const Key('gitlab_tester')), findsOneWidget);

      await tester.tap(find.byKey(const Key('gitlab_tester')));
      await tester.pumpAndSettle();
      expect(find.text('@jihulab_tester'), findsNWidgets(1));
      expect(find.text('@gitlab_tester'), findsNWidgets(1));
      expect(find.byType(PopupMenuDivider), findsNWidgets(2));
      await tester.tap(find.byKey(const Key('AddAccountButton')));
      await tester.pumpAndSettle();
      expect(getTextRenderObjectFromMenuItemButton(tester, 'jihulab.com').text.style!.color, const Color(0xFF87878C));
      expect(getTextRenderObjectFromMenuItemButton(tester, 'gitlab.com').text.style!.color, const Color(0xFF87878C));
      expect(find.text('Already used'), findsNWidgets(2));
      expect(find.byType(PopupMenuDivider), findsNWidgets(4));
    });
  });

  tearDown(() {
    ConnectionProvider().fullReset();
    ProjectProvider().fullReset();
    reset(client);
  });
}
