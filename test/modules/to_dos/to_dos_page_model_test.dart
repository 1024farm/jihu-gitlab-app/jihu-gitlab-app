import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/to_dos/to_dos_page_model.dart';

void main() {
  test("Should get the value of hasMarkedDoneItem field", () async {
    var toDosPageModel = ToDosPageModel();
    toDosPageModel.onItemMarkedDone();
    expect(toDosPageModel.hasMarkedDoneItem, true);
    toDosPageModel.onAfterRefreshDoneList();
    expect(toDosPageModel.hasMarkedDoneItem, false);
  });
}
