import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/modules/privacy_policy/privacy_policy_page.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:webview_flutter_android/webview_flutter_android.dart';

void main() {
  testWidgets("Should check privacy policy", (tester) async {
    WebViewPlatform.instance = AndroidWebViewPlatform();
    await tester.pumpWidget(const MaterialApp(
      home: PrivacyPolicyPage(),
      localizationsDelegates: [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
    ));

    await tester.pumpAndSettle();
    expect(find.byType(WebViewWidget), findsOneWidget);
    expect(find.text("Privacy Policy"), findsOneWidget);
  });
}
