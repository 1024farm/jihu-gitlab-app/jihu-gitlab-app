import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/modules/auth/login_model.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../core/net/http_request_test.mocks.dart';
import '../../test_data/user.dart';

void main() {
  setUp(() async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
  });

  test('Should create Login Model object with singleton instance', () {
    LoginModel model = LoginModel();
    expect(model.authorizeUrl, isNull);
    expect(model.callbackUrl, 'jihu-gitlab://authorization/callback');
    expect(model, LoginModel());
  });

  test('Should exchange token from server', () async {
    final client = MockHttpClient();
    when(client.getWithHeader<Map<String, dynamic>>("https://jihulab.com/api/v4/user", any)).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
    when(client.post<Map<String, dynamic>>('https://jihulab.com/oauth/token', any)).thenAnswer((_) =>
        Future(() => Response.of<Map<String, dynamic>>({'access_token': 'access_token', 'refresh_token': 'refresh_token', 'expires_in': 7200, 'token_type': 'Bearer', 'created_at': 1672736401})));
    HttpClient.injectInstanceForTesting(client);

    LoginModel model = LoginModel();
    model.configuration(host: 'jihulab.com');
    bool result = await model.exchangeToken('code');
    expect(result, isTrue);
    expect(ConnectionProvider.accessToken, 'access_token');
    expect(ConnectionProvider.refreshToken, 'refresh_token');

    LoginModel().fullReset();
  });

  test('Should exchange token be failed when server response error', () async {
    final client = MockHttpClient();
    when(client.post<Map<String, dynamic>>('https://jihulab.com/oauth/token', any)).thenThrow(Exception());
    HttpClient.injectInstanceForTesting(client);

    LoginModel model = LoginModel();
    model.configuration(host: 'jihulab.com');
    bool result = await model.exchangeToken('code');
    expect(result, isFalse);

    LoginModel().fullReset();
  });
}
