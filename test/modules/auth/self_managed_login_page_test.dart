import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../core/net/http_request_test.mocks.dart';
import '../../finder/text_field_hint_text_finder.dart';
import '../../test_data/user.dart';

void main() {
  setUp(() async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    await ConnectionProvider().restore();
  });

  testWidgets('Should login succeed when user submit the Host and Personal Access Token in GitLab self-managed page', (tester) async {
    final client = MockHttpClient();
    when(client.getWithHeader<Map<String, dynamic>>("https://example.com/api/v4/user", any)).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
    HttpClient.injectInstanceForTesting(client);

    await tester.pumpWidget(const MaterialApp(home: Scaffold(body: SelfManagedLoginPage())));

    await tester.pumpAndSettle();
    expect(find.byType(SelfManagedLoginPage), findsOneWidget);
    expect(find.text('GitLab self-managed'), findsOneWidget);

    expect(TextFieldHintTextFinder('Host: gitlab.example.com'), findsOneWidget);
    expect(find.text('HTTPS'), findsOneWidget);
    expect(find.byType(CupertinoSwitch), findsOneWidget);
    expect(tester.widget<CupertinoSwitch>(find.byType(CupertinoSwitch)).value, isTrue);
    expect(TextFieldHintTextFinder('Access Token'), findsOneWidget);
    expect(find.text('Submit'), findsOneWidget);

    await tester.enterText(TextFieldHintTextFinder('Host: gitlab.example.com'), 'example.com');
    await tester.enterText(TextFieldHintTextFinder('Access Token'), 'access_token');
    await tester.tap(find.text('Submit'));
    await tester.pumpAndSettle();
    expect(ConnectionProvider.isSelfManagedGitLab, isTrue);
    expect(ConnectionProvider.currentBaseUrl.get, "https://example.com");
    expect(ConnectionProvider.personalAccessToken, 'access_token');
    expect(ConnectionProvider.authorized, isTrue);

    ConnectionProvider().fullReset();
  });

  testWidgets('Should login failed when fetch user info error', (tester) async {
    final client = MockHttpClient();
    when(client.getWithHeader<Map<String, dynamic>>("https://example.com/api/v4/user", any)).thenThrow(Exception());
    HttpClient.injectInstanceForTesting(client);

    await tester.pumpWidget(const MaterialApp(home: Scaffold(body: SelfManagedLoginPage())));

    await tester.pumpAndSettle();
    await tester.enterText(TextFieldHintTextFinder('Host: gitlab.example.com'), 'example.com');
    await tester.enterText(TextFieldHintTextFinder('Access Token'), 'access_token');
    await tester.tap(find.text('Submit'));
    await tester.pumpAndSettle();
    expect(find.byType(CupertinoAlertDialog), findsOneWidget);
    expect(find.text(AppLocalizations.dictionary().warning), findsOneWidget);
    expect(find.text(AppLocalizations.dictionary().loginHostOrAccessTokenError), findsOneWidget);
    expect(find.text(AppLocalizations.dictionary().ok), findsOneWidget);
    expect(ConnectionProvider.isSelfManagedGitLab, isFalse);
    expect(ConnectionProvider.personalAccessToken, isNull);
    expect(ConnectionProvider.authorized, isFalse);

    await tester.tap(find.text(AppLocalizations.dictionary().ok));
    await tester.pumpAndSettle();
    expect(find.byType(CupertinoAlertDialog), findsNothing);
    await tester.tap(find.text('GitLab self-managed'));

    ConnectionProvider().fullReset();
  });

  testWidgets('Should base url start with http when user closed the https switch', (tester) async {
    final client = MockHttpClient();
    when(client.getWithHeader<Map<String, dynamic>>("http://example.com/api/v4/user", any)).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
    HttpClient.injectInstanceForTesting(client);

    await tester.pumpWidget(const MaterialApp(home: Scaffold(body: SelfManagedLoginPage())));

    await tester.pumpAndSettle();
    expect(find.byType(SelfManagedLoginPage), findsOneWidget);
    expect(find.text('GitLab self-managed'), findsOneWidget);
    expect(find.byType(CupertinoSwitch), findsOneWidget);
    expect(tester.widget<CupertinoSwitch>(find.byType(CupertinoSwitch)).value, isTrue);

    await tester.enterText(TextFieldHintTextFinder('Host: gitlab.example.com'), 'example.com');
    await tester.tap(find.byType(CupertinoSwitch));
    await tester.enterText(TextFieldHintTextFinder('Access Token'), 'access_token');
    await tester.pumpAndSettle();
    expect(tester.widget<CupertinoSwitch>(find.byType(CupertinoSwitch)).value, isFalse);

    await tester.tap(find.text('Submit'));
    await tester.pumpAndSettle();
    expect(ConnectionProvider.currentBaseUrl.get, "http://example.com");

    ConnectionProvider().fullReset();
  });

  testWidgets('Should display input Host notice alert when user input nothing for Host and tapped submit', (tester) async {
    await tester.pumpWidget(const MaterialApp(home: Scaffold(body: SelfManagedLoginPage())));

    await tester.pumpAndSettle();
    expect(find.byType(SelfManagedLoginPage), findsOneWidget);
    expect(find.text('GitLab self-managed'), findsOneWidget);

    await tester.tap(find.text('Submit'));
    await tester.pumpAndSettle();
    expect(find.byType(CupertinoAlertDialog), findsOneWidget);
    expect(find.text('Warning'), findsOneWidget);
    expect(find.text('Please input the host for Submit.'), findsOneWidget);
    expect(find.text('OK'), findsOneWidget);

    await tester.tap(find.text('OK'));
    await tester.pumpAndSettle();
    expect(find.byType(CupertinoAlertDialog), findsNothing);
  });

  testWidgets('Should display input Personal Access Token notice alert when user input nothing for Private Access Token and tapped submit', (tester) async {
    await tester.pumpWidget(const MaterialApp(home: Scaffold(body: SelfManagedLoginPage())));

    await tester.pumpAndSettle();
    expect(find.byType(SelfManagedLoginPage), findsOneWidget);
    expect(find.text('GitLab self-managed'), findsOneWidget);

    await tester.enterText(TextFieldHintTextFinder('Host: gitlab.example.com'), 'example.com');
    await tester.tap(find.text('Submit'));
    await tester.pumpAndSettle();
    expect(find.byType(CupertinoAlertDialog), findsOneWidget);
    expect(find.text('Warning'), findsOneWidget);
    expect(find.text('Please input your Access Token for Submit.'), findsOneWidget);
    expect(find.text('OK'), findsOneWidget);

    await tester.tap(find.text('OK'));
    await tester.pumpAndSettle();
    expect(find.byType(CupertinoAlertDialog), findsNothing);
  });

  testWidgets("Should display clear svg", (tester) async {
    final client = MockHttpClient();
    when(client.get<Map<String, dynamic>>("/api/v4/user")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
    HttpClient.injectInstanceForTesting(client);

    await tester.pumpWidget(const MaterialApp(home: Scaffold(body: SelfManagedLoginPage())));
    await tester.pumpAndSettle();
    await tester.enterText(TextFieldHintTextFinder('Host: gitlab.example.com'), 'example.com');
    await tester.pumpAndSettle();
    expect(find.byType(SvgPicture), findsOneWidget);
    await tester.tap(find.byType(SvgPicture));
    await tester.enterText(TextFieldHintTextFinder('Access Token'), 'access_token');
    await tester.pumpAndSettle();
    await tester.tap(find.byKey(const ValueKey<String>('clear')));
    await tester.pumpAndSettle();
    expect(find.byType(SvgPicture), findsNothing);

    ConnectionProvider().fullReset();
  });
}
