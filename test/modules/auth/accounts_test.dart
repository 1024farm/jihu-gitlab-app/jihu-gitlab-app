import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';

import '../../mocker/tester.dart';

void main() {
  test('Should get jihu connection from project provider', () {
    ConnectionProvider().reset(Tester.jihuLabUser());
    ConnectionProvider().injectConnectionForTest(Tester.gitLabUser());
    ProjectProvider().changeProject(null, 'https://jihulab.com');
    expect(ProjectProvider().isSpecifiedHostHasConnection, true);
    expect(ConnectionProvider().connectionOfSpecifiedHost!.baseUrl.get, "https://jihulab.com");
  });

  test('Should get gitlab connection from project provider', () {
    ConnectionProvider().reset(Tester.jihuLabUser());
    ConnectionProvider().injectConnectionForTest(Tester.gitLabUser());
    ProjectProvider().changeProject(null, 'https://gitlab.com');
    expect(ProjectProvider().isSpecifiedHostHasConnection, true);
    expect(ConnectionProvider().connectionOfSpecifiedHost!.baseUrl.get, "https://gitlab.com");
  });

  test('Should get gitlab connection from project provider', () {
    ConnectionProvider().reset(Tester.selfManagedUser());
    ProjectProvider().changeProject(null, 'https://gitlab.com');
    expect(ProjectProvider().isSpecifiedHostHasConnection, false);
    expect(ConnectionProvider().connectionOfSpecifiedHost, null);
  });

  tearDown(() {
    ConnectionProvider().fullReset();
    ProjectProvider().fullReset();
  });
}
