import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/ai/message/non_response_message.dart';
import 'package:jihu_gitlab_app/modules/ai/message/robot_message.dart';

void main() {
  test('Should throw when non response message request answer', () {
    expect(() async => await NonResponseMessage().requestAnswer(), throwsException);
  });

  test('Should throw when robot message request answer', () {
    expect(() async => await RobotMessage('').requestAnswer(), throwsException);
  });

  test('Should throw when non response message get a box', () {
    expect(() => NonResponseMessage().toView(), throwsException);
  });
}
