import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/db_manager.dart';
import 'package:jihu_gitlab_app/modules/ai/ai_page.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:sqflite/sqflite.dart';

import 'ai_page_test.mocks.dart';

@GenerateNiceMocks([MockSpec<Database>()])
void main() {
  MockDatabase mockDatabase = MockDatabase();

  setUp(() {
    DbManager.instance().injectDatabaseForTesting(mockDatabase);
  });

  testWidgets("Should be able to save welcome message when history is empty", (tester) async {
    when(mockDatabase.query("ai_messages", distinct: null, columns: null, where: null, whereArgs: null, groupBy: null, having: null, orderBy: "created_at desc", limit: 20, offset: 0))
        .thenAnswer((_) => Future(() => []));
    await tester.pumpWidget(const MaterialApp(
      home: Scaffold(
        body: Center(
          child: AIPage(arguments: {}),
        ),
      ),
    ));

    await tester.pumpAndSettle(const Duration(milliseconds: 500));
    verify(mockDatabase.insert("ai_messages", argThat(predicate((map) {
      return (map as Map).containsKey("type") &&
          map.containsKey("state") &&
          map.containsKey("content") &&
          map.containsKey("created_at") &&
          map.containsValue("robot") &&
          map.containsValue("done") &&
          map.containsValue("I'm Jixiaohu, your AI assistant");
    })))).called(1);
    expect(find.text("I'm Jixiaohu, your AI assistant"), findsOneWidget);
  });

  testWidgets("Should be able to load history message", (tester) async {
    when(mockDatabase.query("ai_messages", distinct: null, columns: null, where: null, whereArgs: null, groupBy: null, having: null, orderBy: "created_at desc", limit: 20, offset: 0))
        .thenAnswer((_) => Future(() {
              return [
                {"id": 1, "type": "robot", "state": "done", "content": "Welcome", "created_at": 1},
                {"id": 2, "type": "robot", "state": "done", "content": "a1", "created_at": 2},
                {"id": 3, "type": "robot", "state": "done", "content": "a2", "created_at": 3}
              ];
            }));
    await tester.pumpWidget(const MaterialApp(
      home: Scaffold(
        body: Center(
          child: AIPage(arguments: {}),
        ),
      ),
    ));

    await tester.pumpAndSettle(const Duration(milliseconds: 1000));
    verifyNever(mockDatabase.insert(any, any));
    expect(find.text("Welcome"), findsOneWidget);
    expect(find.text("a1"), findsOneWidget);
    expect(find.text("a2"), findsOneWidget);
  });
}
