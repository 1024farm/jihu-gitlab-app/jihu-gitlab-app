import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/uri_launcher.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar_and_name.dart';
import 'package:jihu_gitlab_app/core/widgets/selector/data_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/selector/selectable.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/l10n/current_locale.dart';
import 'package:jihu_gitlab_app/modules/issues/member/member.dart';
import 'package:jihu_gitlab_app/modules/issues/member/member_provider.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../core/net/http_request_test.mocks.dart';
import '../../core/widgets/home/home_drawer_test.mocks.dart';
import '../../finder/selected_check_box_finder.dart';
import '../../finder/svg_finder.dart';
import '../../mocker/tester.dart';
import 'selectable_test.mocks.dart';

final client = MockHttpClient();

@GenerateNiceMocks([MockSpec<NavigatorObserver>(), MockSpec<MemberProvider>()])
void main() {
  late DataProvider<Member> provider;
  late MockNavigatorObserver navigatorObserver;
  AppLocalizations.init();

  setUp(() async {
    ConnectionProvider().reset(Tester.jihuLabUser());
    provider = MockMemberProvider();
    navigatorObserver = MockNavigatorObserver();
  });

  testWidgets('Should select multi members', (tester) async {
    when(provider.loadFromLocal()).thenAnswer((_) => Future<List<Member>>(() => [
          Member(1, "test_1", "tester_1", "avatar_url_1"),
          Member(2, "test_2", "tester_2", "avatar_url_2"),
          Member(3, "test_11", "tester_11", "avatar_url_11"),
          Member(4, "test_22", "tester_22", "avatar_url_22"),
        ]));

    List<Member> selectedMembers = [];
    var widget = MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => LocaleProvider())],
        child: MaterialApp(
          navigatorObservers: [navigatorObserver],
          onGenerateRoute: onGenerateRoute,
          home: Scaffold(
            body: Selectable<Member>(
                onSelected: (members) {
                  selectedMembers.clear();
                  selectedMembers.addAll(members);
                },
                selectedItemBuilder: (member) => Text(member.name),
                selectorItemBuilder: (context, index, member) => AvatarAndName(username: member.username, avatarUrl: member.avatarUrl),
                dataProviderProvider: () => provider,
                keyMapper: (member) => member.id,
                title: "Assignees",
                pageTitle: "Select Assignees",
                placeHolder: "Select assignee(s)",
                multiSelection: () => Future(() => true),
                filter: (keyword, e) => e.username.toUpperCase().contains(keyword.toUpperCase())),
          ),
          localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
        ));
    await tester.pumpWidget(widget);
    expect(find.byKey(const Key("gotoSelector")), findsOneWidget);
    expect(find.text("Select assignee(s)"), findsOneWidget);
    await tester.tap(find.byKey(const Key("gotoSelector")));
    await tester.pumpAndSettle();
    verify(navigatorObserver.didPush(any, any));
    expect(find.byType(TextField), findsOneWidget);
    expect(find.text("Select Assignees"), findsOneWidget);
    expect(find.widgetWithText(TextButton, AppLocalizations.dictionary().done), findsOneWidget);
    expect(find.byType(AvatarAndName), findsNWidgets(4));

    // Should search
    await tester.enterText(find.byType(TextField), "2");
    await tester.pumpAndSettle();
    expect(find.byType(AvatarAndName), findsNWidgets(2));
    await tester.tap(find.byWidgetPredicate((widget) => widget.runtimeType == IconButton && (widget as IconButton).icon.runtimeType == SvgPicture));
    await tester.pumpAndSettle();
    expect(find.byType(AvatarAndName), findsNWidgets(4));
    expect(find.byType(Checkbox), findsNWidgets(4));

    // multi selection
    await tester.tap(find.byType(Checkbox).first);
    await tester.tap(find.text("tester_22"));

    await tester.tap(find.widgetWithText(TextButton, AppLocalizations.dictionary().done));
    await tester.pumpAndSettle();
    verify(navigatorObserver.didPop(any, any));
    expect(selectedMembers.length, 2);
    expect(selectedMembers[0].id, 1);
    expect(selectedMembers[1].id, 4);

    // reopen selector
    await tester.tap(find.byKey(const Key("gotoSelector")));
    await tester.pumpAndSettle();
    verify(navigatorObserver.didPush(any, any));
    var finder = find.byType(Checkbox);
    expect(finder, findsNWidgets(4));
    var firstCheckbox = tester.firstWidget(finder) as Checkbox;
    expect(firstCheckbox.value, true);
  });

  testWidgets('Should select single member', (tester) async {
    var provider = MockMemberProvider();
    MockNavigatorObserver navigatorObserver = MockNavigatorObserver();

    when(provider.loadFromLocal()).thenAnswer((_) => Future<List<Member>>(() => [
          Member(1, "test_1", "tester_1", "avatar_url_1"),
          Member(2, "test_2", "tester_2", "avatar_url_1"),
        ]));

    var widget = MultiProvider(providers: [ChangeNotifierProvider(create: (context) => LocaleProvider())], child: TestWidget(dataProviderProvider: provider, navigatorObserver: navigatorObserver));
    await tester.pumpWidget(widget);
    expect(find.byKey(const Key("gotoSelector")), findsOneWidget);
    expect(find.text("Select assignee(s)"), findsOneWidget);

    // await tester.ensureVisible(find.byKey(const Key("gotoSelector")));
    await tester.tap(find.byKey(const Key("gotoSelector")));
    for (int i = 0; i < 5; i++) {
      await tester.pumpAndSettle();
    }
    verify(navigatorObserver.didPush(any, any));
    expect(find.byType(TextField), findsOneWidget);
    expect(find.text("Select Assignees"), findsOneWidget);
    expect(find.byType(AvatarAndName), findsNWidgets(2));
    expect(find.text("tester_1"), findsOneWidget);
    expect(find.text("tester_2"), findsOneWidget);

    // Should search
    await tester.enterText(find.byType(TextField), "2");
    await tester.pumpAndSettle();
    expect(find.text("tester_2"), findsOneWidget);
    expect(find.text("tester_1"), findsNothing);

    await tester.tap(find.text("tester_2"));
    await tester.pumpAndSettle();
    verify(navigatorObserver.didPop(any, any));
    expect(SvgFinder("assets/images/clear.svg"), findsOneWidget);

    TestWidgetState state = tester.state(find.byType(TestWidget));
    expect(state.selected.length, 1);
    expect(state.selected[0].id, 2);
    expect(find.widgetWithText(TextButton, "Refresh"), findsOneWidget);
    await tester.tap(find.widgetWithText(TextButton, "Refresh"));
    await tester.pumpAndSettle();
    expect(state.selected.length, 1);
    expect(SvgFinder("assets/images/clear.svg"), findsOneWidget);

    // remove the selected member
    await tester.tap(SvgFinder("assets/images/clear.svg"));
    await tester.pumpAndSettle();
    expect(SvgFinder("assets/images/clear.svg"), findsNothing);
  });

  testWidgets('Should open select member', (tester) async {
    var provider = MockMemberProvider();
    MockNavigatorObserver navigatorObserver = MockNavigatorObserver();

    when(provider.loadFromLocal()).thenAnswer((_) => Future<List<Member>>(() => [
          Member(1, "test_1", "tester_1", "avatar_url_1"),
          Member(2, "test_2", "tester_2", "avatar_url_1"),
        ]));

    var widget = MultiProvider(providers: [ChangeNotifierProvider(create: (context) => LocaleProvider())], child: TestWidget(dataProviderProvider: provider, navigatorObserver: navigatorObserver));
    await tester.pumpWidget(widget);
    expect(find.byKey(const Key("gotoSelector")), findsOneWidget);
    expect(find.text("Select assignee(s)"), findsOneWidget);

    await tester.tap(find.text("Select assignee(s)"));
    await tester.pumpAndSettle();
    verify(navigatorObserver.didPush(any, any));
    expect(find.byType(TextField), findsOneWidget);
    expect(find.text("Select Assignees"), findsOneWidget);
    expect(find.byType(AvatarAndName), findsNWidgets(2));
    expect(find.text("tester_1"), findsOneWidget);
    expect(find.text("tester_2"), findsOneWidget);

    // Should search
    await tester.enterText(find.byType(TextField), "2");
    await tester.pumpAndSettle();
    expect(find.text("tester_2"), findsOneWidget);
    expect(find.text("tester_1"), findsNothing);

    await tester.tap(find.text("tester_2"));
    await tester.pumpAndSettle();
    verify(navigatorObserver.didPop(any, any));
    expect(SvgFinder("assets/images/clear.svg"), findsOneWidget);

    TestWidgetState state = tester.state(find.byType(TestWidget));
    expect(state.selected.length, 1);
    expect(state.selected[0].id, 2);
    expect(find.widgetWithText(TextButton, "Refresh"), findsOneWidget);
    await tester.tap(find.widgetWithText(TextButton, "Refresh"));
    await tester.pumpAndSettle();
    expect(state.selected.length, 1);
    expect(SvgFinder("assets/images/clear.svg"), findsOneWidget);

    // remove the selected member
    await tester.tap(SvgFinder("assets/images/clear.svg"));
    await tester.pumpAndSettle();
    expect(SvgFinder("assets/images/clear.svg"), findsNothing);
  });

  group('Should select single member and display warning message on selector page when current user is not have license', () {
    var provider = MockMemberProvider();
    MockNavigatorObserver navigatorObserver = MockNavigatorObserver();
    var mockUriLauncher = MockUriLauncher();
    when(mockUriLauncher.canLaunch(any)).thenAnswer((realInvocation) => Future(() => true));
    when(mockUriLauncher.launch(any)).thenAnswer((realInvocation) => Future(() => true));
    when(mockUriLauncher.launch(any, mode: LaunchMode.externalApplication)).thenAnswer((realInvocation) => Future(() => true));
    UriLauncher.instance().injectInstanceForTesting(mockUriLauncher);

    when(provider.loadFromLocal()).thenAnswer((_) => Future<List<Member>>(() => [
          Member(1, "test_1", "tester_1", "avatar_url_1"),
          Member(2, "test_2", "tester_2", "avatar_url_1"),
        ]));

    var params = [
      {'name': 'open email link', 'link': 'mail@test.com'},
      {'name': 'open url which include https and www ', 'link': 'https://www.example.com'}
    ];
    for (var param in params) {
      testWidgets(param["name"] as String, (tester) async {
        String beforeText = 'Warning Notice: ';
        String linkText = param['link'] as String;

        List<Member> selectedMembers = [];
        var widget = MultiProvider(
            providers: [ChangeNotifierProvider(create: (context) => LocaleProvider())],
            child: MaterialApp(
              navigatorObservers: [navigatorObserver],
              onGenerateRoute: onGenerateRoute,
              home: Scaffold(
                body: Selectable<Member>(
                    onSelected: (members) {
                      selectedMembers.clear();
                      selectedMembers.addAll(members);
                    },
                    selectedItemBuilder: (member) => Text(member.name),
                    selectorItemBuilder: (context, index, member) => AvatarAndName(username: member.username, avatarUrl: member.avatarUrl),
                    dataProviderProvider: () => provider,
                    keyMapper: (member) => member.id,
                    title: "Assignees",
                    pageTitle: "Select Assignees",
                    placeHolder: "Select assignee(s)",
                    multiSelection: () => Future(() => false),
                    warningNotice: () => Future(() => '$beforeText$linkText'),
                    filter: (keyword, e) => e.username.toUpperCase().contains(keyword.toUpperCase())),
              ),
              localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
            ));
        await tester.pumpWidget(widget);
        expect(find.byKey(const Key("gotoSelector")), findsOneWidget);
        expect(find.text("Select assignee(s)"), findsOneWidget);

        // check if route to correct page with correct widgets
        await tester.tap(find.byKey(const Key("gotoSelector")));
        for (int i = 0; i < 5; i++) {
          await tester.pumpAndSettle();
        }
        verify(navigatorObserver.didPush(any, any));
        expect(find.byType(TextField), findsOneWidget);
        expect(find.text("Select Assignees"), findsOneWidget);
        expect(find.byType(AvatarAndName), findsNWidgets(2));
        expect(find.text("tester_1"), findsOneWidget);
        expect(find.text("tester_2"), findsOneWidget);

        expect(find.widgetWithText(TextButton, AppLocalizations.dictionary().done), findsOneWidget);
        expect(find.byType(Checkbox), findsNWidgets(2));

        // check if single select
        await tester.tap(find.text("tester_2"));
        await tester.pumpAndSettle();
        expect(SelectedCheckBoxFinder(), findsNWidgets(1));
        expect(find.byType(Checkbox), findsNWidgets(2));
        // check if warning notice is showing
        expect(find.textContaining(beforeText, findRichText: true), findsOneWidget);
        expect(find.text(linkText, findRichText: true), findsOneWidget);

        // check if the link in the warning notice text can be tapped
        await tester.tap(find.textContaining(linkText, findRichText: true));
        await tester.pumpAndSettle();
        verify(mockUriLauncher.launch(any)).called(1);

        await tester.tap(find.byType(Checkbox).first);
        await tester.pumpAndSettle();
        expect(SelectedCheckBoxFinder(), findsNWidgets(1));

        // check if unselect is available
        await tester.tap(find.text("tester_2"));
        await tester.pumpAndSettle();
        expect(SelectedCheckBoxFinder(), findsNothing);

        // check if another item is available to select after the other one is unselected
        await tester.tap(find.byType(Checkbox).first);
        await tester.pumpAndSettle();
        expect(SelectedCheckBoxFinder(), findsNWidgets(1));
        await tester.tap(find.byType(Checkbox).first);
        await tester.pumpAndSettle();

        // check if the item can be selected if the other item is unselected
        expect(find.textContaining(beforeText, findRichText: true), findsNothing);
        expect(find.text(linkText, findRichText: true), findsNothing);
        await tester.tap(find.text("tester_2"));
        await tester.pumpAndSettle();
        expect(SelectedCheckBoxFinder(), findsNWidgets(1));
        expect(find.textContaining(beforeText, findRichText: true), findsOneWidget);
        expect(find.text(linkText, findRichText: true), findsOneWidget);

        // check if the value is correct after the select is done
        await tester.tap(find.widgetWithText(TextButton, AppLocalizations.dictionary().done));
        await tester.pumpAndSettle();
        verify(navigatorObserver.didPop(any, any));
        expect(selectedMembers.length, 1);
        expect(selectedMembers[0].id, 2);

        await tester.tap(find.byKey(const Key("gotoSelector")));
        for (int i = 0; i < 5; i++) {
          await tester.pumpAndSettle();
        }
        expect(find.text("Select Assignees"), findsOneWidget);
        expect(SelectedCheckBoxFinder(), findsNWidgets(1));
        expect(find.byType(Checkbox), findsNWidgets(2));
        // check if warning notice is showing
        expect(find.textContaining(beforeText, findRichText: true), findsOneWidget);
        expect(find.text(linkText, findRichText: true), findsOneWidget);
      });
    }
  });
}

class TestWidget extends StatefulWidget {
  final MockNavigatorObserver navigatorObserver;
  final DataProvider<Member> dataProviderProvider;

  const TestWidget({required this.navigatorObserver, required this.dataProviderProvider, Key? key}) : super(key: key);

  @override
  State<TestWidget> createState() => TestWidgetState();
}

class TestWidgetState extends State<TestWidget> {
  List<Member> selected = [];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorObservers: [widget.navigatorObserver],
      onGenerateRoute: onGenerateRoute,
      home: Scaffold(
        body: Column(
          children: [
            TextButton(onPressed: () => setState(() {}), child: const Text("Refresh")),
            Selectable<Member>(
                // key: const Key("container-of-selectable"),
                onSelected: (members) {
                  selected.clear();
                  selected.addAll(members);
                },
                selectedItemBuilder: (member) => Text(member.name),
                selectorItemBuilder: (context, index, member) => AvatarAndName(username: member.username, avatarUrl: member.avatarUrl),
                dataProviderProvider: () => widget.dataProviderProvider,
                keyMapper: (member) => member.id,
                title: "Assignees",
                pageTitle: "Select Assignees",
                placeHolder: "Select assignee(s)",
                multiSelection: () => Future(() => false),
                initialData: selected,
                filter: (keyword, e) => e.username.toUpperCase().contains(keyword.toUpperCase())),
          ],
        ),
      ),
      localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
    );
  }
}
