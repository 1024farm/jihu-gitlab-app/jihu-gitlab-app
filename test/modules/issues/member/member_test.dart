import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/issues/member/member.dart';

void main() {
  test("Should switch checked state", () {
    var user = Member(1, "testers", "tester", "tester-avatar");
    expect(user.checked, false);
    user.toggleCheck(check: true);
    expect(user.checked, true);
    user.toggleCheck(check: null);
    expect(user.checked, true);
  });
}
