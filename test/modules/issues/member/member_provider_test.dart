import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/modules/issues/member/member_entity.dart';
import 'package:jihu_gitlab_app/modules/issues/member/member_provider.dart';
import 'package:jihu_gitlab_app/modules/issues/member/member_repository.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../../core/net/http_request_test.mocks.dart';
import 'member_provider_test.mocks.dart';

@GenerateNiceMocks([MockSpec<MemberRepository>()])
void main() {
  late MemberProvider memberProvider;
  late MockMemberRepository repository;

  setUp(() {
    repository = MockMemberRepository();
    memberProvider = MemberProvider(projectId: 59893);
    memberProvider.setRepository(repository);
  });

  test("Should load assignees data from local", () async {
    Map<String, dynamic> dbData = {"id": 1, "member_id": 2, "project_id": 59893, "username": "tester", "avatar_url": "tester-avatar", "version": 4};
    when(repository.queryByProject(59893)).thenAnswer((realInvocation) async => Future.value([MemberEntity.restore(dbData)]));
    var list = await memberProvider.loadFromLocal();
    expect(list.length, 1);
    expect(list[0].id, 2);
    expect(list[0].username, "tester");
    expect(list[0].avatarUrl, "tester-avatar");
  });

  test("Should sync assignees data from remote", () async {
    var mockHttpClient = MockHttpClient();
    HttpClient.injectInstanceForTesting(mockHttpClient);
    when(mockHttpClient.get<List<dynamic>>("/api/v4/projects/59893/members/all?page=1&per_page=50")).thenAnswer((_) async => Future.value(Response.of([
          {"id": 22, "username": "tester-22-new", "avatar_url": "tester-avatar-22-new"},
          {"id": 33, "username": "tester-33", "avatar_url": "tester-avatar-33"}
        ])));

    // shouldBeDelete
    MemberEntity.restore({"id": 1, "member_id": 11, "project_id": 59893, "username": "tester-11", "avatar_url": "tester-avatar-11", "version": 4});
    // shouldBeUpdate
    MemberEntity shouldBeUpdate = MemberEntity.restore({"id": 2, "member_id": 22, "project_id": 59893, "username": "tester-22", "avatar_url": "tester-avatar-22", "version": 4});
    when(repository.query(59893, [22, 33])).thenAnswer((realInvocation) async => Future.value([shouldBeUpdate]));

    var result = await memberProvider.syncFromRemote();
    expect(result, true);
    verify(repository.deleteLessThan(any, 59893)).called(1);
    verify(repository.insert(argThat(predicate((x) {
      return x is List<MemberEntity> && x.length == 1 && x[0].memberId == 33 && x[0].username == "tester-33" && x[0].avatarUrl == "tester-avatar-33";
    }))));

    verify(repository.update(argThat(predicate((x) {
      return x is List<MemberEntity> && x.length == 1 && x[0].memberId == 22 && x[0].username == "tester-22-new" && x[0].avatarUrl == "tester-avatar-22-new";
    }))));
  });

  test("Should return false when sync assignees data from remote failure", () async {
    var mockHttpClient = MockHttpClient();
    HttpClient.injectInstanceForTesting(mockHttpClient);
    when(mockHttpClient.get<List<dynamic>>("/api/v4/projects/59893/members/all?page=1&per_page=50")).thenThrow(Exception());
    var result = await memberProvider.syncFromRemote();
    expect(result, false);
  });
}
