import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/issues/member/member_entity.dart';

void main() {
  test("Should construct from map", () {
    var assigneeEntity = MemberEntity.create({"id": 1, "username": "tester", "avatar_url": "tester-avatar"}, 11, 0);
    expect(assigneeEntity.id, null);
    expect(assigneeEntity.memberId, 1);
    expect(assigneeEntity.username, "tester");
    expect(assigneeEntity.avatarUrl, "tester-avatar");
    expect(assigneeEntity.projectId, 11);
    expect(assigneeEntity.version, 0);
  });

  test("Should restore from repository", () {
    Map<String, dynamic> dbData = {"id": 1, "member_id": 2, "project_id": 3, "username": "tester", "avatar_url": "tester-avatar", "version": 4};
    var assigneeEntity = MemberEntity.restore(dbData);
    expect(assigneeEntity.id, 1);
    expect(assigneeEntity.memberId, 2);
    expect(assigneeEntity.username, "tester");
    expect(assigneeEntity.avatarUrl, "tester-avatar");
    expect(assigneeEntity.projectId, 3);
    expect(assigneeEntity.version, 4);
  });

  test("Should convert to map", () {
    Map<String, dynamic> dbData = {"id": 1, "member_id": 2, "project_id": 3, "name": "name", "username": "tester", "avatar_url": "tester-avatar", "version": 4};
    var map = MemberEntity.restore(dbData).toMap();
    expect(map, dbData);
  });

  test("Should get member entity hashcode", () {
    Map<String, dynamic> dbData = {"id": 1, "member_id": 2, "project_id": 3, "name": "name", "username": "tester", "avatar_url": "tester-avatar", "version": 4};
    expect(MemberEntity.restore(dbData).hashCode, greaterThan(0));
  });

  test("Should update", () {
    Map<String, dynamic> dbData1 = {"id": 1, "member_id": 2, "project_id": 3, "username": "tester", "avatar_url": "tester-avatar", "version": 4};
    Map<String, dynamic> dbData2 = {"id": null, "member_id": 2, "project_id": 3, "username": "tester2", "avatar_url": "tester-avatar2", "version": 5};
    var entity1 = MemberEntity.restore(dbData1);
    var entity2 = MemberEntity.restore(dbData2);
    entity1.update(entity2);
    expect(entity1.id, 1);
    expect(entity1.memberId, 2);
    expect(entity1.username, "tester2");
    expect(entity1.avatarUrl, "tester-avatar2");
    expect(entity1.projectId, 3);
    expect(entity1.version, 5);
  });

  test("Should generate the sql of create table", () {
    var expectSql = """ 
    create table project_members(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        member_id INTEGER NOT NULL,
        project_id INTEGER NOT NULL,
        name TEXT ,
        username TEXT ,
        avatar_url TEXT,
        version INTEGER
    );    
    """;
    expect(MemberEntity.createTableSql(), expectSql);
  });
}
