import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/models/issue_creation_model.dart';
import 'package:jihu_gitlab_app/modules/issues/member/member.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../core/net/http_request_test.mocks.dart';
import '../../mocker/tester.dart';
import '../../test_data/iterations.dart';
import '../../test_data/user.dart';

void main() {
  final client = MockHttpClient();

  setUp(() async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();

    ConnectionProvider().reset(Tester.jihuLabUser());
  });

  test("Issue draft view", () {
    IssueCreationModel model = IssueCreationModel();
    List<Label> labels = [];
    labels.add(Label.fromJson({'id': 1, 'name': 'label-name'}));
    List<Member> members = [];
    members.add(Member(1, "name", "username", "avatar"));
    model.initLabelAndAssign(labels, members);

    expect(model.selectedLabels.length, 1);
    expect(model.selectedAssignees.length, 1);
  });

  test("Should selected labels", () {
    IssueCreationModel model = IssueCreationModel();
    List<Label> labels = [];
    labels.add(Label.fromJson({'id': 1, 'name': 'label-name'}));
    model.onLabelSelect(labels);
    expect(model.selectedLabels.length, 1);
  });

  test("Should get empty warning notice when current user has license", () async {
    when(client.get<List<dynamic>>("/api/v4/groups/0/iterations")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>(iterations)));
    HttpClient.injectInstanceForTesting(client);

    IssueCreationModel model = IssueCreationModel();
    model.init(projectId: 10000, groupId: 0);
    String notice = await model.warningNoticeWhenNoLicense();
    expect(notice.isEmpty, isTrue);
  });

  test("Should get warning notice when current user login with SaaS and has no license", () async {
    when(client.get<List<dynamic>>("/api/v4/groups/0/iterations")).thenThrow(Exception());
    HttpClient.injectInstanceForTesting(client);

    IssueCreationModel model = IssueCreationModel();
    model.init(projectId: 10000, groupId: 0);
    String notice = await model.warningNoticeWhenNoLicense();
    expect(notice.isEmpty, isFalse);
    expect(notice, 'Your group don\'t support: multi-selection, contact us: contact@gitlab.cn');
  });

  test("Should get warning notice when current user login with Self Managed and has no license", () async {
    when(client.get<List<dynamic>>("/api/v4/groups/0/iterations")).thenThrow(Exception());
    when(client.getWithHeader<Map<String, dynamic>>("https://example.com/api/v4/user", any)).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>(userInfo)));
    HttpClient.injectInstanceForTesting(client);

    await ConnectionProvider().initSelfManagedConfigs(host: 'example.com', token: 'access_token', isHttps: true);

    IssueCreationModel model = IssueCreationModel();
    model.init(projectId: 10000, groupId: 0);
    String notice = await model.warningNoticeWhenNoLicense();
    expect(notice.isEmpty, isFalse);
    expect(notice, 'Your server don\'t support: multi-selection, contact us: tlindemann@gitlab.com');
  });
}
