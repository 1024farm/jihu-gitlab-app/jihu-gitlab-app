import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/db_manager.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/widgets/label_view.dart';
import 'package:jihu_gitlab_app/core/widgets/markdown/markdown_input_box.dart';
import 'package:jihu_gitlab_app/core/widgets/search_no_result_view.dart';
import 'package:jihu_gitlab_app/core/widgets/selector/selectable.dart';
import 'package:jihu_gitlab_app/core/widgets/unauthorized_view.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/l10n/current_locale.dart';
import 'package:jihu_gitlab_app/modules/issues/index.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../core/net/http_request_test.mocks.dart';
import '../../finder/svg_finder.dart';
import '../../finder/text_field_hint_text_finder.dart';
import '../../mocker/tester.dart';
import '../../test_binding_setter.dart';
import '../../test_data/members.dart';
import '../ai/ai_page_test.mocks.dart';
import 'issue_creation_page_test.mocks.dart';

final client = MockHttpClient();

@GenerateNiceMocks([MockSpec<NavigatorObserver>()])
void main() {
  setUp(() async {
    HttpClient.injectInstanceForTesting(client);

    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    await ConnectionProvider().restore();
    await ConnectionProvider().restore();

    ConnectionProvider().reset(Tester.jihuLabUser());
  });

  testWidgets('Should view create issue page when there is no auth', (tester) async {
    ConnectionProvider().fullReset();

    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
      child: const MaterialApp(
        home: Scaffold(
          body: IssueCreationPage(projectId: 10000, from: 'group', groupId: 0),
        ),
        localizationsDelegates: [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
      ),
    ));
    await tester.pumpAndSettle();
    expect(find.byType(UnauthorizedView), findsOneWidget);
  });

  testWidgets('Should create an issue', (tester) async {
    when(client.get("/api/v4/groups/0/iterations")).thenAnswer((realInvocation) => Future(() => Response.of([])));
    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
      child: const MaterialApp(
        home: Scaffold(
          body: IssueCreationPage(projectId: 10000, from: 'group', groupId: 0),
        ),
        localizationsDelegates: [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
      ),
    ));
    await tester.pumpAndSettle();
    expect(find.text('Create'), findsOneWidget);
    expect(find.text('Title (required)'), findsOneWidget);
    expect(find.text('Write a issue title'), findsOneWidget);
    expect(find.byType(MarkdownInputBox), findsOneWidget);
  });

  testWidgets('Should create issue with title and description success', (tester) async {
    when(client.get("/api/v4/groups/0/iterations")).thenAnswer((realInvocation) => Future(() => Response.of([])));
    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
      child: const MaterialApp(
        home: Scaffold(
          body: IssueCreationPage(projectId: 10000, from: 'group', groupId: 0),
        ),
        localizationsDelegates: [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
      ),
    ));
    await tester.pumpAndSettle();

    const issueTitle = 'issue title';
    const issueDescription = 'issue description';

    await tester.enterText(TextFieldHintTextFinder('Write a issue title'), issueTitle);
    await tester.enterText(TextFieldHintTextFinder('Write a description here...'), issueDescription);

    await tester.ensureVisible(find.byType(CupertinoSwitch));
    await tester.tap(find.byType(CupertinoSwitch));
    await tester.tap(find.text('Create'));
    for (int i = 0; i < 5; i++) {
      await tester.pumpAndSettle(const Duration(seconds: 1));
    }
    verify(client.post("/api/v4/projects/10000/issues", {"title": "issue title", "description": "issue description", "labels": "", "confidential": true})).called(1);
  });

  testWidgets('Should select labels from label selector', (tester) async {
    await setUpMobileBinding(tester);

    when(client.get("/api/v4/groups/0/iterations")).thenAnswer((realInvocation) => Future(() => Response.of([])));
    when(client.get<List<dynamic>>("/api/v4/projects/59893/labels?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([
          {"id": 1, "name": "tester", "description": "tester-avatar", "text_color": "#FFFFFF", "color": "#6699cc", "is_project_label": false}
        ])));

    final mockDatabase = MockDatabase();
    when(mockDatabase.insert("project_labels", any)).thenAnswer((_) => Future(() => 1));
    when(mockDatabase.update("project_labels", any)).thenAnswer((_) => Future(() => 1));
    when(mockDatabase.transaction(any)).thenAnswer((_) => Future(() => <int>[]));
    when(mockDatabase.query("project_labels", where: " project_id = ? ", whereArgs: [59893])).thenAnswer((_) => Future(() {
          return [
            {"id": 1, "label_id": 1, "project_id": 59893, "name": "CREQ::blocker", "description": "description_1", "textColor": "#FFFFFF", "color": "#FFFFFF", "version": 1},
            {"id": 2, "label_id": 2, "project_id": 59893, "name": "P::M", "description": "description_2", "textColor": "#FFFFFF", "color": "#FFFFFF", "version": 1},
            {"id": 3, "label_id": 3, "project_id": 59893, "name": "Ready::No", "description": "description_3", "textColor": "#FFFFFF", "color": "#FFFFFF", "version": 1},
            {"id": 4, "label_id": 4, "project_id": 59893, "name": "Ready::Yes", "description": "description_4", "textColor": "#FFFFFF", "color": "#FFFFFF", "version": 1},
          ];
        }));
    DbManager.instance().injectDatabaseForTesting(mockDatabase);

    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: const MaterialApp(home: Scaffold(body: IssueCreationPage(projectId: 59893, from: 'group', groupId: 0))),
    ));
    await tester.pumpAndSettle();
    expect(find.byType(Selectable<Label>), findsOneWidget);
    expect(find.text("Select label(s)"), findsOneWidget);

    await tester.tap(find.byType(Selectable<Label>));
    await tester.pumpAndSettle();
    expect(find.byType(IssueCreationPage), findsNothing);
    expect(find.byType(Checkbox), findsNWidgets(4));
    expect(find.text("Done"), findsOneWidget);
    expect(find.text("Ready::No"), findsOneWidget);

    // Search
    expect(TextFieldHintTextFinder("Search"), findsOneWidget);
    await tester.enterText(TextFieldHintTextFinder("Search"), "aaa");
    await tester.pumpAndSettle();
    expect(find.byType(Checkbox), findsNothing);
    expect(find.text("Ready::No"), findsNothing);
    expect(find.byType(SearchNoResultView), findsOneWidget);
    expect(find.text("No matches for 'aaa'"), findsOneWidget);
    await tester.enterText(TextFieldHintTextFinder("Search"), "No");
    await tester.pumpAndSettle();
    expect(find.byType(Checkbox), findsOneWidget);
    expect(find.text("Ready::No"), findsOneWidget);

    // Select label
    await tester.tap(find.text("Ready::No"));
    await tester.tap(find.text("Done"));
    await tester.pumpAndSettle();
    expect(find.byType(IssueCreationPage), findsOneWidget);
    expect(find.byWidgetPredicate((widget) => widget is LabelView && widget.left == 'Ready' && widget.right == 'No'), findsOneWidget);
  });

  testWidgets('Should open label selector', (tester) async {
    await setUpMobileBinding(tester);
    when(client.get("/api/v4/groups/0/iterations")).thenAnswer((realInvocation) => Future(() => Response.of([])));
    when(client.get<List<dynamic>>("/api/v4/projects/59893/labels?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([
          {"id": 1, "name": "tester", "description": "tester-avatar", "text_color": "#FFFFFF", "color": "#6699cc", "is_project_label": false}
        ])));

    final mockDatabase = MockDatabase();
    when(mockDatabase.insert("project_labels", any)).thenAnswer((_) => Future(() => 1));
    when(mockDatabase.update("project_labels", any)).thenAnswer((_) => Future(() => 1));
    when(mockDatabase.transaction(any)).thenAnswer((_) => Future(() => <int>[]));
    when(mockDatabase.query("project_labels", where: " project_id = ? ", whereArgs: [59893])).thenAnswer((_) => Future(() {
          return [
            {"id": 1, "label_id": 1, "project_id": 59893, "name": "CREQ::blocker", "description": "description_1", "textColor": "#FFFFFF", "color": "#FFFFFF", "version": 1},
            {"id": 2, "label_id": 2, "project_id": 59893, "name": "P::M", "description": "description_2", "textColor": "#FFFFFF", "color": "#FFFFFF", "version": 1},
            {"id": 3, "label_id": 3, "project_id": 59893, "name": "Ready::No", "description": "description_3", "textColor": "#FFFFFF", "color": "#FFFFFF", "version": 1},
            {"id": 4, "label_id": 4, "project_id": 59893, "name": "Ready::Yes", "description": "description_4", "textColor": "#FFFFFF", "color": "#FFFFFF", "version": 1},
          ];
        }));
    DbManager.instance().injectDatabaseForTesting(mockDatabase);

    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: const MaterialApp(home: Scaffold(body: IssueCreationPage(projectId: 59893, from: 'group', groupId: 0))),
    ));
    await tester.pumpAndSettle();
    expect(find.byType(Selectable<Label>), findsOneWidget);
    expect(find.text("Select label(s)"), findsOneWidget);

    await tester.tap(find.text("Select label(s)"));
    await tester.pumpAndSettle();
    expect(find.byType(IssueCreationPage), findsNothing);
    expect(find.byType(Checkbox), findsNWidgets(4));
  });

  testWidgets('Should open assignee selector', (tester) async {
    when(client.get<Map<String, dynamic>>("/api/v4/user")).thenAnswer((_) => Future(() => Response.of<Map<String, dynamic>>({'id': 5882})));
    when(client.get<dynamic>("/api/v4/groups/0/iterations")).thenAnswer((_) async => Future(() => Response.of("")));
    when(client.get<List<dynamic>>("/api/v4/projects/10000/members/all?page=1&per_page=50")).thenAnswer((_) async => Future.value(Response.of(members)));
    MockNavigatorObserver navigatorObserver = MockNavigatorObserver();
    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: MaterialApp(
        navigatorObservers: [navigatorObserver],
        home: const Scaffold(body: IssueCreationPage(projectId: 10000, from: 'group', groupId: 0)),
      ),
    ));
    await tester.pumpAndSettle();
    expect(find.byType(Selectable<Member>), findsOneWidget);
    expect(find.text("Select assignee(s)"), findsOneWidget);

    await tester.ensureVisible(find.byType(Selectable<Member>));
    await tester.tap(find.byType(Selectable<Member>));
    await tester.pumpAndSettle();
    verify(navigatorObserver.didPush(any, any));
    expect(find.byType(TextField), findsOneWidget);
    expect(find.text("Select Assignees"), findsOneWidget);

    await tester.tap(find.text("Select Assignees"));
    await tester.pumpAndSettle();
  });

  testWidgets('Should open member selector', (tester) async {
    when(client.get<dynamic>("/api/v4/groups/0/iterations")).thenAnswer((_) async => Future(() => Response.of([])));
    when(client.get<List<dynamic>>("/api/v4/projects/10000/members/all?page=1&per_page=50")).thenAnswer((_) async => Future.value(Response.of(members)));
    MockNavigatorObserver navigatorObserver = MockNavigatorObserver();
    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
      child: MaterialApp(
        navigatorObservers: [navigatorObserver],
        onGenerateRoute: onGenerateRoute,
        home: const Scaffold(
          body: IssueCreationPage(projectId: 10000, from: 'group', groupId: 0),
        ),
        localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
      ),
    ));

    var imageFinder = SvgFinder('assets/images/alternate_email.svg');
    expect(imageFinder, findsOneWidget);
    await tester.ensureVisible(imageFinder);
    await tester.tap(imageFinder, warnIfMissed: false);

    for (int i = 0; i < 5; i++) {
      await tester.pumpAndSettle();
    }
    expect(find.text("Select contact"), findsOneWidget);

    verify(navigatorObserver.didPush(any, any));
    expect(find.byType(TextField), findsOneWidget);
  });

  tearDown(() {
    ConnectionProvider().fullReset();

    reset(client);
  });
}
