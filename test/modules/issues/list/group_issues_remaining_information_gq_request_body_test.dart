import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/issues/list/group_issues_remaining_information_gq_request_body.dart';

void main() {
  test('Should get getGroupIssuesRemainingInformationGraphQLRequestBody', () {
    expect(getGroupIssuesRemainingInformationGraphQLRequestBody('1', '1', 10), {
      "query": """
       {
          group(fullPath:"1") {
            issues(labelName: null, first:10, after: "", search: "1", state: opened){
              nodes {
                id
                iteration {
                  id
                  state
                  startDate
                  dueDate
                  scopedPath
                  webPath
                }
                epic {
                  title
                }
                weight
              }
            }
          }
       }
       """
    });
  });
}
