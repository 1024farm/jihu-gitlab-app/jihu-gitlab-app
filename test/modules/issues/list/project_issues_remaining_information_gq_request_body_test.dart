import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/issues/list/project_issues_remaining_information_gq_request_body.dart';

void main() {
  test('Should view project issues remaining information gq request body', () {
    expect(getProjectIssuesRemainingInformationGraphQLRequestBody('', '', 10), {
      "query": """
       {
          project(fullPath:""){
            id
            path
            fullPath
            name
            nameWithNamespace
            issues(labelName: null, first:10, after: "", search: "", state: opened){
              nodes{
                id
                epic {
                  title
                }
                weight
                iteration {
                  id
                  state
                  startDate
                  dueDate
                  scopedPath
                  webPath
                }
              }
            }
          }
       }
       """
    });
  });
}
