import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connections.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/widgets/unauthorized_view.dart';
import 'package:jihu_gitlab_app/modules/auth/login_page.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/widgets/description_template_selector.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../mocker/tester.dart';

void main() {
  testWidgets('Should render unauthorized view', (tester) async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    await ConnectionProvider().restore();

    late GlobalKey currentRouteKey;
    await tester.pumpWidget(MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
        child: MaterialApp(routes: {
          LoginPage.routeName: (context) => LoginPage(key: currentRouteKey = GlobalKey(), arguments: const {'host': 'test.host', 'clientId': 'client_id'})
        }, home: const Scaffold(body: DescriptionTemplateSelector(arguments: {'projectId': 0, 'free': false})))));
    await tester.pumpAndSettle();
    expect(find.byType(UnauthorizedView), findsOneWidget);

    await tester.tap(find.byType(PopupMenuButton<String>));
    await tester.pumpAndSettle();
    await tester.tap(find.byWidgetPredicate((widget) => widget is PopupMenuItem<String> && widget.value == LoginMenuSet.jihulab.name));
    await tester.pumpAndSettle();
    await tester.tap(find.text(LoginMenuSet.jihulab.name));
    await tester.pumpAndSettle();

    expect(find.byType(LoginPage), findsOneWidget);

    ConnectionProvider().reset(Tester.jihuLabUser());
    Navigator.pop(currentRouteKey.currentContext!, true);
    await tester.pumpAndSettle();
    expect(find.byType(DescriptionTemplateSelector), findsOneWidget);

    ConnectionProvider().fullReset();
  });
}
