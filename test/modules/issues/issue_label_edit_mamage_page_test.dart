import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/db_manager.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/widgets/label_manage_view.dart';
import 'package:jihu_gitlab_app/core/widgets/selector/selectable.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/l10n/current_locale.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label_manage_page.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label_update_page.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/issue_creation_page.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';

import '../../core/net/http_request_test.mocks.dart';
import '../../mocker/tester.dart';
import '../../test_binding_setter.dart';
import '../ai/ai_page_test.mocks.dart';

final client = MockHttpClient();

@GenerateNiceMocks([MockSpec<NavigatorObserver>()])
void main() {
  setUp(() async {
    HttpClient.injectInstanceForTesting(client);

    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    await ConnectionProvider().restore();

    ConnectionProvider().reset(Tester.jihuLabUser());
  });

  testWidgets('Should see manage label button', (tester) async {
    await setUpMobileBinding(tester);
    when(client.get("/api/v4/groups/0/iterations")).thenAnswer((realInvocation) => Future(() => Response.of([])));
    when(client.get<List<dynamic>>("/api/v4/projects/59893/labels?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([
          {"id": 1, "name": "tester", "description": "tester-avatar", "text_color": "#FFFFFF", "color": "#6699cc", "is_project_label": false}
        ])));

    final mockDatabase = MockDatabase();
    when(mockDatabase.insert("project_labels", any)).thenAnswer((_) => Future(() => 1));
    when(mockDatabase.update("project_labels", any)).thenAnswer((_) => Future(() => 1));
    when(mockDatabase.transaction(any)).thenAnswer((_) => Future(() => <int>[]));
    when(mockDatabase.query("project_labels", where: " project_id = ? ", whereArgs: [59893])).thenAnswer((_) => Future(() {
          return [
            {"id": 1, "label_id": 1, "project_id": 59893, "name": "CREQ::blocker", "description": "description_1", "textColor": "#FFFFFF", "color": "#FFFFFF", "version": 1},
            {"id": 2, "label_id": 2, "project_id": 59893, "name": "P::M", "description": "description_2", "textColor": "#FFFFFF", "color": "#FFFFFF", "version": 1},
            {"id": 3, "label_id": 3, "project_id": 59893, "name": "Ready::No", "description": "description_3", "textColor": "#FFFFFF", "color": "#FFFFFF", "version": 1},
            {"id": 4, "label_id": 4, "project_id": 59893, "name": "Ready::Yes", "description": "description_4", "textColor": "#FFFFFF", "color": "#FFFFFF", "version": 1},
          ];
        }));
    DbManager.instance().injectDatabaseForTesting(mockDatabase);

    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider())],
      child: const MaterialApp(home: Scaffold(body: IssueCreationPage(projectId: 59893, from: 'group', groupId: 0))),
    ));
    await tester.pumpAndSettle();
    expect(find.byType(Selectable<Label>), findsOneWidget);
    expect(find.text("Select label(s)"), findsOneWidget);

    await tester.tap(find.byType(Selectable<Label>));
    await tester.pumpAndSettle();
    expect(find.byType(IssueCreationPage), findsNothing);
    expect(find.byType(Checkbox), findsNWidgets(4));
    expect(find.text("Done"), findsOneWidget);

    expect(find.text("Manage labels"), findsOneWidget);

    await tester.tap(find.text("Manage labels"));
    expect(find.text("Manage labels"), findsOneWidget);
  });

  testWidgets('Should enter manage label page', (tester) async {
    await setUpMobileBinding(tester);
    when(client.get("/api/v4/groups/0/iterations")).thenAnswer((realInvocation) => Future(() => Response.of([])));
    when(client.get<List<dynamic>>("/api/v4/projects/10000/labels?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([
          {"id": 1, "name": "tester", "description": "tester-avatar", "text_color": "#FFFFFF", "color": "#6699cc", "is_project_label": false}
        ])));

    final mockDatabase = MockDatabase();
    when(mockDatabase.insert("project_labels", any)).thenAnswer((_) => Future(() => 1));
    when(mockDatabase.update("project_labels", any)).thenAnswer((_) => Future(() => 1));
    when(mockDatabase.transaction(any)).thenAnswer((_) => Future(() => <int>[]));
    when(mockDatabase.query("project_labels", where: " project_id = ? ", whereArgs: [10000])).thenAnswer((_) => Future(() {
          return [
            {"id": 1, "label_id": 1, "project_id": 10000, "name": "CREQ::blocker", "description": "description_1", "textColor": "#FFFFFF", "color": "#FFFFFF", "version": 1},
            {"id": 2, "label_id": 2, "project_id": 10000, "name": "P::M", "description": "description_2", "textColor": "#FFFFFF", "color": "#FFFFFF", "version": 1},
            {"id": 3, "label_id": 3, "project_id": 10000, "name": "Ready::No", "description": "description_3", "textColor": "#FFFFFF", "color": "#FFFFFF", "version": 1},
            {"id": 4, "label_id": 4, "project_id": 10000, "name": "Ready::Yes", "description": "description_4", "textColor": "#FFFFFF", "color": "#FFFFFF", "version": 1},
          ];
        }));
    DbManager.instance().injectDatabaseForTesting(mockDatabase);

    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
      child: const MaterialApp(
        home: Scaffold(
          body: LabelManagePage(projectId: 10000, projectNameSpace: "projectNameSpace"),
        ),
        localizationsDelegates: [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
      ),
    ));
    await tester.pumpAndSettle();
    expect(find.text('Manage labels'), findsOneWidget);
    expect(find.byType(LabelManageView), findsNWidgets(4));
    await tester.tap(find.text("P"));
  });

  testWidgets('Should edit label with title and description success', (tester) async {
    Map<String, dynamic> labelJson = {"id": 1, "label_id": 1, "project_id": 10000, "name": "CREQ::blocker", "description": "description_1", "textColor": "#FFFFFF", "color": "#FFFFFF", "version": 1};
    when(client.get("/api/v4/projects/10000/labels")).thenAnswer((realInvocation) => Future(() => Response.of([])));
    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => LocaleProvider())],
      child: MaterialApp(
        home: Scaffold(
          body: LabelUpdatePage(projectId: 10000, label: Label.fromJson(labelJson)),
        ),
        localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
      ),
    ));
    await tester.pumpAndSettle();
    expect(find.text('Save'), findsOneWidget);
    expect(find.text('Title (required)'), findsOneWidget);
    expect(find.text('Background color'), findsOneWidget);
    expect(find.byType(Wrap), findsOneWidget);

    const labelTitle = 'label title';
    const labelDescription = 'label description';

    await tester.enterText(find.byKey(const Key("label_title")), labelTitle);
    await tester.enterText(find.byKey(const Key("label_description")), labelDescription);

    await tester.tap(find.text('Save'));
    for (int i = 0; i < 5; i++) {
      await tester.pumpAndSettle(const Duration(seconds: 1));
    }
  });

  tearDown(() {
    ConnectionProvider().fullReset();
    reset(client);
  });
}
