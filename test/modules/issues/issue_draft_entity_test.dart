import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/models/issue_draft_entity.dart';
import 'package:jihu_gitlab_app/modules/issues/member/member.dart';

void main() {
  test("Should construct from map", () {
    List<Member> members = [];
    members.add(Member(1, "name", "username", "avatar"));
    var data = {"user_id": 1, "title": "tester", "description": "tester-description", "template": "template", "selected_assignees": members};
    var issueDraft = IssueDraftEntity.create(data, 11, 0);
    expect(issueDraft.userId, 1);
    expect(issueDraft.title, "tester");
    expect(issueDraft.description, "tester-description");
    expect(issueDraft.template, "template");
    expect(issueDraft.assignees, members);
    expect(issueDraft.projectId, 11);
    expect(issueDraft.version, 0);
  });

  test("Should restore from repository", () {
    Map<String, dynamic> data = {
      "user_id": 1,
      "title": "tester",
      "description": "tester-description",
      "template": "template",
      "selected_assignees": '[{"id": 1, "username": "username", "avatarUrl": "avatar"}]',
      "project_id": 11,
      "version": 2
    };
    var issueDraft = IssueDraftEntity.restore(data);
    expect(issueDraft.userId, 1);
    expect(issueDraft.title, "tester");
    expect(issueDraft.description, "tester-description");
    expect(issueDraft.template, "template");
    expect(issueDraft.assignees?[0].username, 'username');
    expect(issueDraft.projectId, 11);
    expect(issueDraft.version, 2);
  });

  test("Should convert to map", () {
    Map<String, dynamic> dbData = {
      "user_id": 1,
      "project_id": 11,
      "title": "tester",
      "description": "tester-description",
      "template": "template",
      "selected_assignees": '[{"id": 1, "name": "name", "username": "username", "avatarUrl": "avatar"}]',
      "selected_labels": '[{"id": 1, "name": "label-name","text_color":"#000000","color":"#000000"}]',
      "version": 2
    };
    var map = IssueDraftEntity.restore(dbData).toMap();
    expect(map, {
      'user_id': 1,
      'project_id': 11,
      'title': 'tester',
      'description': 'tester-description',
      "template": "template",
      'selected_assignees': '[{"id":1,"name":"name","username":"username","avatarUrl":"avatar"}]',
      'selected_labels': '[{"id":1,"name":"label-name","description":"","text_color":"#000000","color":"#000000"}]',
      'version': 2,
      "confidential": 0
    });
  });

  test("Should generate the sql of create table", () {
    var expectSql = """ 
    create table issue_drafts(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        user_id INTEGER NOT NULL,
        project_id INTEGER NOT NULL,
        title TEXT,
        description TEXT,
        selected_assignees TEXT,
        selected_labels TEXT,
        version INTEGER
    );    
    """;
    expect(IssueDraftEntity.createTableSql(), expectSql);
  });
}
