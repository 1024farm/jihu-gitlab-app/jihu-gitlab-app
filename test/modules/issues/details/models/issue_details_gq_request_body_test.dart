import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_gq_request_body.dart';

void main() {
  test('Should getIssueDetailsGraphQLRequestBody has expected params', () {
    expect(getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 6), {
      "query": """
       {
          project(fullPath: "ultimate-plan/jihu-gitlab-app/demo-mr-test") {
            id
            name
            nameWithNamespace
            path
            fullPath
            issue(iid: "6") {
              id
              iid
              title
              description
              webUrl
              confidential
              state
              createdAt
              author {
                id
                avatarUrl
                name
                username
              }
              assignees {
                nodes {
                  id
                  avatarUrl
                  name
                  username
                }
              }
              labels {
                nodes {
                  id
                  title
                  description
                  color
                  textColor
                }
              }
              milestone{
                id
                title
              }
            }
          }
       }
       """
    });
  });

  test('Should getIssuePostVotesGraphQLRequestBody has expected params', () {
    expect(getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 6), {
      "query": """
           {
              project(fullPath: "ultimate-plan/jihu-gitlab-app/demo-mr-test") {
                issue(iid: "6") {
                  downvotes
                  upvotes
                }
              }
           }
       """
    });
  });
}
