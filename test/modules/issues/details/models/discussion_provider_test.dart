import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion_entity.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion_provider.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion_repository.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../../../core/net/http_request_test.mocks.dart';
import '../../../../mocker/tester.dart';
import '../../../../test_data/assignee.dart';
import '../../../../test_data/discussion.dart';
import '../../../../test_data/note.dart';
import 'discussion_provider_test.mocks.dart';

@GenerateNiceMocks([MockSpec<DiscussionRepository>()])
void main() {
  late DiscussionProvider discussionProvider;
  late MockDiscussionRepository mockDiscussionRepository;

  var mockHttpClient = MockHttpClient();

  setUp(() {
    mockDiscussionRepository = MockDiscussionRepository();
    discussionProvider = DiscussionProvider(issueId: 123, issueIid: 12, projectId: 321, pathWithNamespace: 'pathWithNamespace');
    discussionProvider.injectInstanceForTesting(mockDiscussionRepository);
    ConnectionProvider().reset(Tester.jihuLabUser());
  });

  test("Should load data from local", () async {
    when(mockDiscussionRepository.queryByIssue(123)).thenAnswer((realInvocation) async => Future.value([DiscussionEntity.restore(discussionDbData)]));

    var list = await discussionProvider.loadFromLocal();
    expect(list.length, 1);
    expect(list[0].id, '123');
    expect(list[0].individualNote, true);
    expect(list[0].notes.first.id, noteDbData['note_id']);
    expect(list[0].notes.first.createdAt.time, noteDbData['created_at']);
    expect(list[0].notes.first.system, noteDbData['system'] == 1 ? true : false);
    expect(list[0].notes.first.body, noteDbData['body']);
    expect(list[0].notes.first.author.id, assigneeDbData['assignee_id']);
    expect(list[0].notes.first.author.name, assigneeDbData['name']);
    expect(list[0].notes.first.author.username, assigneeDbData['username']);
    expect(list[0].notes.first.author.avatarUrl, assigneeDbData['avatar_url']);
  });

  test("Should sync data from remote", () async {
    when(mockHttpClient.get<List<dynamic>>("/api/v4/projects/321/issues/12/discussions?page=1&per_page=50")).thenAnswer((_) async => Future.value(Response.of(discussionsData)));
    HttpClient.injectInstanceForTesting(mockHttpClient);

    DiscussionEntity existsDiscussion = DiscussionEntity.restore({
      'id': 111,
      'discussion_id': 'abc124',
      'issue_id': 123,
      'issue_iid': 12,
      'individual_note': true,
      'path_with_namespace': 'pathWithNamespace',
      'version': 4,
      'notes': [noteDbData]
    });
    when(mockDiscussionRepository.queryByIssue(123, discussionIds: ['abc123', 'abc124'])).thenAnswer((_) async => Future.value([existsDiscussion]));

    var result = await discussionProvider.syncFromRemote();
    expect(result, true);
    verify(mockDiscussionRepository.deleteLessThan(any, any)).called(1);
    verify(mockDiscussionRepository.insert(argThat(predicate((x) {
      return x is List<DiscussionEntity> && x.length == 1 && x[0].discussionId == 'abc123';
    })))).called(1);
    verify(mockDiscussionRepository.update(argThat(predicate((x) {
      return x is List<DiscussionEntity> && x.length == 1 && x[0].discussionId == 'abc124';
    })))).called(1);
  });

  test("Should return false when sync data from remote failure", () async {
    when(mockHttpClient.get<List<dynamic>>("/api/v4/projects/321/issues/12/discussions?page=1&per_page=50")).thenThrow(Exception());
    HttpClient.injectInstanceForTesting(mockHttpClient);
    var result = await discussionProvider.syncFromRemote();
    expect(result, false);
  });
}
