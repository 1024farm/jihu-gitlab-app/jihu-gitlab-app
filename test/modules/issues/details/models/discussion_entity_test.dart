import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion_entity.dart';

import '../../../../test_data/discussion.dart';

void main() {
  test("Should construct from map", () {
    var assigneeEntity = DiscussionEntity.create(discussionsData.first, pathWithNamespace: 'pathWithNamespace', issueId: 12312, issueIid: 22, version: 0);
    expect(assigneeEntity.id, null);
    expect(assigneeEntity.discussionId, discussionsData.first['id']);
    expect(assigneeEntity.issueId, 12312);
    expect(assigneeEntity.issueIid, 22);
    expect(assigneeEntity.individualNote, discussionsData.first['individual_note']);
    expect(assigneeEntity.notes!.first.noteId, discussionsData.first['notes'].first['id']);
    expect(assigneeEntity.version, 0);
  });

  test("Should construct success from empty map", () {
    var assigneeEntity = DiscussionEntity.create({}, pathWithNamespace: 'pathWithNamespace', issueId: 12312, issueIid: 22, version: 0);
    expect(assigneeEntity.id, null);
    expect(assigneeEntity.discussionId, '');
    expect(assigneeEntity.issueId, 12312);
    expect(assigneeEntity.issueIid, 22);
    expect(assigneeEntity.individualNote, false);
    expect(assigneeEntity.notes, isEmpty);
    expect(assigneeEntity.version, 0);
  });

  test("Should restore from repository", () {
    var assigneeEntity = DiscussionEntity.restore(discussionDbData);
    expect(assigneeEntity.id, 111);
    expect(assigneeEntity.discussionId, '123');
    expect(assigneeEntity.issueId, 33);
    expect(assigneeEntity.issueIid, 3);
    expect(assigneeEntity.individualNote, true);
    expect(assigneeEntity.pathWithNamespace, 'pathWithNamespace');
    expect(assigneeEntity.version, 0);
  });

  test("Should convert to map", () {
    var map = DiscussionEntity.restore(discussionDbData).toMap();
    expect(map['id'], discussionDbData['id']);
    expect(map['discussion_id'], discussionDbData['discussion_id']);
    expect(map['issue_id'], discussionDbData['issue_id']);
    expect(map['issue_iid'], discussionDbData['issue_iid']);
    expect(map['individual_note'], discussionDbData['individual_note'] == 1 ? true : false);
    expect(map['path_with_namespace'], discussionDbData['path_with_namespace']);
    expect(map['version'], discussionDbData['version']);
  });

  test("Should get discussion entity hashcode", () {
    expect(DiscussionEntity.restore(discussionDbData).hashCode, greaterThan(0));
  });

  test("Should generate the sql of create table", () {
    var expectSql = """ 
    create table discussions(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        discussion_id TEXT NOT NULL,
        issue_id INTEGER NOT NULL,
        issue_iid INTEGER NOT NULL,
        individual_note INTEGER ,
        path_with_namespace TEXT ,
        version INTEGER
    );
    """;
    expect(DiscussionEntity.createTableSql(), expectSql);
  });
}
