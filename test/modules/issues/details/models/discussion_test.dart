import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/note/note.dart';

void main() {
  test("Should create Discussion object", () {
    var note = Note.fromJson({
      'id': 1574825,
      'noteable_id': 265567,
      'noteable_iid': 6,
      'body': 'changed the description',
      'system': false,
      'author': {
        'id': 29355,
        'name': 'miaolu',
        'username': 'perity',
        "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29355/avatar.png",
      },
      'created_at': '2022-11-25T14:19:57.067+08:00',
    }, 'pathWithNamespace');
    var discussion = Discussion('abc123', [note], individualNote: true);
    expect(discussion.id, 'abc123');
    expect(discussion.individualNote, true);
    expect(discussion.notes.length, 1);
    expect(discussion.notes.first.id, note.id);
  });
}
