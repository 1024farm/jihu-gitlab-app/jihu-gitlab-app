import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/note/note_entity.dart';

import '../../../../test_data/note.dart';

void main() {
  test("Should construct from map", () {
    var assigneeEntity = NoteEntity.create(noteData, '10ff9d7a76f4ec69417b60ad11224ea7bb3fc71e', 0);
    expect(assigneeEntity.id, null);
    expect(assigneeEntity.noteId, noteData['id']);
    expect(assigneeEntity.discussionId, '10ff9d7a76f4ec69417b60ad11224ea7bb3fc71e');
    expect(assigneeEntity.system, noteData['system']);
    expect(assigneeEntity.body, noteData['body']);
    expect(assigneeEntity.assigneeId, noteData['author']['id']);
    expect(assigneeEntity.createdAt, noteData['created_at']);
    expect(assigneeEntity.version, 0);
  });

  test("Should construct success from map with null values", () {
    var assigneeEntity = NoteEntity.create(noteDataWithNullValues, '10ff9d7a76f4ec69417b60ad11224ea7bb3fc71e', 0);
    expect(assigneeEntity.id, null);
    expect(assigneeEntity.noteId, noteDataWithNullValues['id']);
    expect(assigneeEntity.discussionId, '10ff9d7a76f4ec69417b60ad11224ea7bb3fc71e');
    expect(assigneeEntity.system, noteDataWithNullValues['system']);
    expect(assigneeEntity.body, '');
    expect(assigneeEntity.assigneeId, noteDataWithNullValues['author']['id']);
    expect(assigneeEntity.createdAt, noteDataWithNullValues['created_at']);
    expect(assigneeEntity.version, 0);
  });

  test("Should restore from repository", () {
    var assigneeEntity = NoteEntity.restore(noteDbData);
    expect(assigneeEntity.id, noteDbData['id']);
    expect(assigneeEntity.noteId, noteDbData['note_id']);
    expect(assigneeEntity.noteableId, noteDbData['noteable_id']);
    expect(assigneeEntity.noteableIid, noteDbData['noteable_iid']);
    expect(assigneeEntity.discussionId, noteDbData['discussion_id']);
    expect(assigneeEntity.system, noteDbData['system'] == 1 ? true : false);
    expect(assigneeEntity.body, noteDbData['body']);
    expect(assigneeEntity.assigneeId, noteDbData['assignee_id']);
    expect(assigneeEntity.createdAt, noteDbData['created_at']);
    expect(assigneeEntity.version, 4);
  });

  test("Should convert to map", () {
    var map = NoteEntity.restore(noteDbData).toMap();
    expect(map['id'], noteDbData['id']);
    expect(map['note_id'], noteDbData['note_id']);
    expect(map['noteable_id'], noteDbData['noteable_id']);
    expect(map['noteable_iid'], noteDbData['noteable_iid']);
    expect(map['discussion_id'], noteDbData['discussion_id']);
    expect(map['system'], noteDbData['system']);
    expect(map['body'], noteDbData['body']);
    expect(map['assignee_id'], noteDbData['assignee_id']);
    expect(map['created_at'], noteDbData['created_at']);
    expect(map['version'], 4);
  });

  test("Should get discussion entity hashcode", () {
    expect(NoteEntity.restore(noteDbData).hashCode, greaterThan(0));
  });

  test("Should generate the sql of create table", () {
    var expectSql = """ 
    create table notes(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        note_id INTEGER NOT NULL,
        noteable_id INTEGER NOT NULL,
        noteable_iid INTEGER NOT NULL,
        discussion_id TEXT NOT NULL,
        system INTEGER,
        body TEXT,
        assignee_id INTEGER NOT NULL,
        created_at TEXT ,
        version INTEGER
    );
    """;
    expect(NoteEntity.createTableSql(), expectSql);
  });

  test("Should update", () {
    Map<String, dynamic> dbData2 = {
      'id': null,
      'note_id': 1574834,
      "noteable_id": 3,
      "noteable_iid": 4,
      'discussion_id': 'abc124',
      'system': 1,
      'body': 'assigned to @perity',
      'assignee_id': 29355,
      'created_at': '2022-11-28T14:21:47.711+08:00',
      'version': 7,
      'author': {
        'id': 8,
        'assignee_id': 29355,
        'name': 'name',
        'username': 'username',
        'avatar_url': 'https://jihulab.com/uploads/-/system/user/avatar/29355/avatar.png',
        'version': 7,
      }
    };

    var entity1 = NoteEntity.restore(noteDbData);
    var entity2 = NoteEntity.restore(dbData2);
    entity1.update(entity2);
    expect(entity1.id, noteDbData['id']);
    expect(entity1.noteId, noteDbData['note_id']);
    expect(entity1.noteableId, noteDbData['noteable_id']);
    expect(entity1.noteableIid, noteDbData['noteable_iid']);
    expect(entity1.discussionId, noteDbData['discussion_id']);
    expect(entity1.system, dbData2['system'] == 1 ? true : false);
    expect(entity1.body, dbData2['body']);
    expect(entity1.assigneeId, dbData2['assignee_id']);
    expect(entity1.createdAt, noteDbData['created_at']);
    expect(entity1.version, dbData2['version']);
    expect(entity1.assigneeEntity, isNotNull);
    expect(entity1.assigneeEntity!.id, 8);
    expect(entity1.assigneeEntity!.assigneeId, 29355);
    expect(entity1.assigneeEntity!.name, 'name');
    expect(entity1.assigneeEntity!.username, 'username');
    expect(entity1.assigneeEntity!.avatarUrl, 'https://jihulab.com/uploads/-/system/user/avatar/29355/avatar.png');
    expect(entity1.assigneeEntity!.version, 7);
    expect(entity1 == entity2, isTrue);
  });
}
