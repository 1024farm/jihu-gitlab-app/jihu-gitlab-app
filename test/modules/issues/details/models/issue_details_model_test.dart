import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_model.dart';

void main() {
  test("Should save reply comment", () {
    IssueDetailsModel model = IssueDetailsModel();
    model.selectedNoteId = 1;
    model.saveComment("123");

    expect(model.selectedNoteId, 1);
  });
}
