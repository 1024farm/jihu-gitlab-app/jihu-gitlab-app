import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/db_manager.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/global_keys.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar_and_name.dart';
import 'package:jihu_gitlab_app/core/widgets/award_emoji_button.dart';
import 'package:jihu_gitlab_app/core/widgets/delimited_column.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_button.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/l10n/current_locale.dart';
import 'package:jihu_gitlab_app/modules/issues/details/issue_details_page.dart';
import 'package:jihu_gitlab_app/modules/issues/details/milestone_selector_page.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion_provider.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_model.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issue_item_view.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_page.dart';
import 'package:jihu_gitlab_app/modules/issues/list/project_issues_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/issues/project_issues_page.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestone_item_view.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestones_fetcher.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../core/net/http_request_test.mocks.dart';
import '../../../finder/semantics_label_finder.dart';
import '../../../finder/svg_finder.dart';
import '../../../finder/text_field_hint_text_finder.dart';
import '../../../mocker/tester.dart';
import '../../../test_binding_setter.dart';
import '../../../test_data/discussion.dart';
import '../../../test_data/issue.dart';
import '../../../test_data/members.dart';
import '../../../test_data/note.dart';
import '../../ai/ai_page_test.mocks.dart';
import '../selectable_test.mocks.dart';
import 'issue_details_page_test.mocks.dart';

final client = MockHttpClient();

@GenerateNiceMocks([MockSpec<DiscussionProvider>()])
void main() {
  var issueDetailsModel = IssueDetailsModel();
  locator.registerSingleton(issueDetailsModel);
  var provider = MockDiscussionProvider();
  issueDetailsModel.injectDataProviderForTesting(provider);

  var params = {'projectId': 72936, 'issueId': 3242, 'issueIid': 6, 'targetId': 45345, 'targetIid': 265567, 'pathWithNamespace': 'ultimate-plan/jihu-gitlab-app/demo-mr-test'};
  setUp(() async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    await ConnectionProvider().restore();
    ConnectionProvider().reset(Tester.jihuLabUser());

    when(client.get<List<dynamic>>("/api/v4/projects/0/issues/0/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
    when(client.get<List<dynamic>>("/api/v4/projects/72936/issues/265567/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>(discussionsData)));
    when(client.post<dynamic>("/api/v4/projects/72936/issues/265567/notes", any)).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
    when(client.get<List<dynamic>>("/api/v4/projects/72936/members/all?page=1&per_page=50")).thenAnswer((_) async => Future.value(Response.of(members)));
    when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 265567)))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(issueDetailsGraphQLResponse)));
    when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 265567), useActiveConnection: true))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(issuePostVotesGraphQLResponse)));
    when(client.get("/api/v4/projects/72936/issues/265567/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => Response.of([])));
    when(client.get("/api/v4/projects/72936/issues/265567/links")).thenAnswer((_) => Future(() => Response.of([])));
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets('Should display issues data view with data', (tester) async {
    await setUpMobileBinding(tester);
    when(client.post<dynamic>("/api/v4/projects/72936/issues/6/discussions", {'body': "test-comment"})).thenAnswer((_) => Future(() => Response.of<dynamic>({
          "id": "200",
          "individual_note": false,
        })));

    await tester.pumpWidget(MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
        child: MaterialApp(
          home: Scaffold(
            body: IssueDetailsPage(arguments: params),
          ),
        )));

    await tester.pumpAndSettle();
    expect(find.textContaining("demo mr test"), findsOneWidget);
    expect(find.text(AppLocalizations.dictionary().commentPlaceholder), findsOneWidget);
    await tester.tap(find.text(AppLocalizations.dictionary().commentPlaceholder));
    await tester.pumpAndSettle();
    await tester.enterText(TextFieldHintTextFinder(AppLocalizations.dictionary().commentPlaceholder), "test-comment");
    expect(find.text("test-comment"), findsOneWidget);
    await tester.pumpAndSettle();
    expect(find.byKey(const Key('comment-reply-inkwell')), findsOneWidget);
    await tester.tap(find.byKey(const Key('comment-reply-inkwell')));
    await tester.pumpAndSettle();
    for (int i = 0; i < 5; i++) {
      await tester.pump(const Duration(seconds: 1));
    }
    expect(find.text("test-comment"), findsNothing);
  });

  testWidgets('Should display empty view with none params provide', (tester) async {
    await tester.pumpWidget(MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
        child: const MaterialApp(
          home: Scaffold(
            body: IssueDetailsPage(arguments: {}),
          ),
        )));

    await tester.pumpAndSettle();
    expect(find.textContaining(AppLocalizations.dictionary().noItemSelected("issue")), findsOneWidget);
  });

  testWidgets("Should be able to share issue", (tester) async {
    await setUpCompatibleBinding(tester);
    when(client.put<dynamic>("/api/v4/projects/72936/issues/265567", {'state_event': 'close'})).thenAnswer((_) => Future(() => Response.of<dynamic>({})));

    await tester.pumpWidget(MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
        child: MaterialApp(
          home: Scaffold(
            body: IssueDetailsPage(arguments: params),
          ),
          localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
        )));

    await tester.pumpAndSettle();
    // expect(SemanticsLabelFinder('share'), findsOneWidget);
    expect(SemanticsLabelFinder('operate'), findsOneWidget);
    await tester.tap(SemanticsLabelFinder('operate'));
    await tester.pumpAndSettle();
    expect(find.text("Close issue"), findsOneWidget);
    expect(find.text("Edit issue"), findsOneWidget);
    expect(find.text("Make public"), findsOneWidget);
    await tester.tap(find.text("Close issue"));
    await tester.pumpAndSettle();
    await tester.tap(SemanticsLabelFinder('operate'));
    await tester.pumpAndSettle();
    expect(find.text("Copy link"), findsOneWidget);
    expect(find.text("Copy title & link"), findsOneWidget);
    expect(find.text("Open in browser"), findsOneWidget);

    await tester.tap(find.text('Copy link'));
    for (int i = 0; i < 5; i++) {
      await tester.pumpAndSettle(const Duration(seconds: 1));
    }

    await tester.tap(SemanticsLabelFinder('operate'));
    await tester.pumpAndSettle();
    await tester.tap(find.text('Copy title & link'));
    for (int i = 0; i < 5; i++) {
      await tester.pumpAndSettle(const Duration(seconds: 1));
    }
    await tester.tap(find.textContaining("months ago"));
    await tester.pumpAndSettle();
    expect(find.textContaining("Nov 25, 2022"), findsWidgets);
    await tester.tap(find.textContaining("Nov 25, 2022"));
    await tester.pumpAndSettle();
    expect(find.textContaining("months ago"), findsWidgets);
  });

  group('Issue discussions', () {
    testWidgets('Should able to send comment when @ someone only', (tester) async {
      await setUpMobileBinding(tester);
      when(client.post<dynamic>("/api/v4/projects/72936/issues/6/discussions", {'body': "test-comment"})).thenAnswer((_) => Future(() => Response.of<dynamic>({
            "id": "200",
            "individual_note": false,
          })));

      MockDatabase mockDatabase = MockDatabase();
      DbManager.instance().injectDatabaseForTesting(mockDatabase);
      when(mockDatabase.query("project_members", where: " project_id = ? ", whereArgs: [72936])).thenAnswer((_) => Future(() {
            return [
              {"id": 1, "member_id": 1, "project_id": 72936, "name": "test_1", "username": "tester_1", "avatar_url": "avatar_url_1", "version": 1},
              {"id": 2, "member_id": 2, "project_id": 72936, "name": "test_2", "username": "tester_2", "avatar_url": "avatar_url_2", "version": 1},
              {"id": 3, "member_id": 3, "project_id": 72936, "name": "test_11", "username": "tester_11", "avatar_url": "avatar_url_11", "version": 1},
              {"id": 4, "member_id": 4, "project_id": 72936, "name": "test_22", "username": "tester_22", "avatar_url": "avatar_url_22", "version": 1},
            ];
          }));

      var navigatorObserver = MockNavigatorObserver();
      await tester.pumpWidget(MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (context) => LocaleProvider()),
            ChangeNotifierProvider(create: (context) => ConnectionProvider()),
            ChangeNotifierProvider(create: (context) => ProjectProvider())
          ],
          child: MaterialApp(
            navigatorObservers: [navigatorObserver],
            onGenerateRoute: onGenerateRoute,
            home: Scaffold(
              body: IssueDetailsPage(arguments: params),
            ),
            localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
          )));

      await tester.pumpAndSettle();
      expect(find.textContaining("demo mr test"), findsOneWidget);
      expect(find.text(AppLocalizations.dictionary().commentPlaceholder), findsOneWidget);
      await tester.tap(find.text(AppLocalizations.dictionary().commentPlaceholder));
      await tester.pumpAndSettle();
      expect(find.byWidgetPredicate((widget) => widget is LoadingButton && widget.disabled), findsOneWidget);
      expect(SvgFinder('assets/images/alternate_email.svg'), findsOneWidget);

      await tester.tap(SvgFinder('assets/images/alternate_email.svg'));
      await tester.pumpAndSettle();
      verify(navigatorObserver.didPush(any, any));
      expect(find.text(AppLocalizations.dictionary().selectContact), findsOneWidget);
      expect(find.byType(TextField), findsOneWidget);
      await tester.pumpAndSettle();
      await tester.pumpAndSettle();
      expect(find.byType(AvatarAndName), findsNWidgets(4));

      await tester.tap(find.text("tester_22"));
      await tester.pumpAndSettle();
      verify(navigatorObserver.didPop(any, any));
      expect(find.byWidgetPredicate((widget) => widget is LoadingButton && widget.disabled), findsNothing);
      expect(find.byWidgetPredicate((widget) => widget is LoadingButton && !widget.disabled), findsOneWidget);
    });

    testWidgets('Should display discussions when discussions response with individualNote is false', (tester) async {
      when(provider.loadFromLocal()).thenAnswer((_) => Future(() => [Discussion('abc123', noteList, individualNote: false)]));
      var parameters = {
        'projectId': 72936,
        'issueId': 3242,
        'issueIid': 6,
        'targetId': 45345,
        'targetIid': 265567,
        'targetUrl': "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo-mr-test/-/issues/6#note_11",
        'pathWithNamespace': 'ultimate-plan/jihu-gitlab-app/demo-mr-test'
      };
      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
          child: MaterialApp(
            home: Scaffold(
              body: IssueDetailsPage(arguments: parameters),
            ),
            localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
          )));

      for (int i = 0; i < 5; i++) {
        await tester.pump(const Duration(seconds: 1));
      }
      for (int i = 0; i < 5; i++) {
        await tester.pump(const Duration(seconds: 1));
      }

      // TODO: 缓存 DiscussionProvider 会导致页面数据不刷新

      await tester.tap(find.byType(IssueDetailsPage));
    });

    testWidgets('Should display discussions when discussions response with individualNote is true', (tester) async {
      when(provider.loadFromLocal()).thenAnswer((_) => Future(() => [Discussion('abc123', noteList, individualNote: true)]));

      var parameters = {
        'projectId': 72936,
        'issueId': 3242,
        'issueIid': 6,
        'targetId': 45345,
        'targetIid': 265567,
        'targetUrl': "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo-mr-test/-/issues/6#note_6",
        'pathWithNamespace': 'ultimate-plan/jihu-gitlab-app/demo-mr-test'
      };

      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
          child: MaterialApp(
            home: Scaffold(
              body: IssueDetailsPage(arguments: parameters),
            ),
            localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
          )));

      for (int i = 0; i < 5; i++) {
        await tester.pump(const Duration(seconds: 1));
      }
      for (int i = 0; i < 5; i++) {
        await tester.pump(const Duration(seconds: 1));
      }

      // TODO: 缓存 DiscussionProvider 会导致页面数据不刷新
    });
  });

  group('Should test up and down votes', () {
    testWidgets('Should display thumbs up button', (tester) async {
      await setUpMobileBinding(tester);
      Map<String, dynamic> map = Map.from(issuePostVotesGraphQLResponse);
      map['data']['project']['issue']['upvotes'] = 0;
      when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 265567), useActiveConnection: true))
          .thenAnswer((_) => Future(() => Response.of<dynamic>(map)));
      when(client.get("/api/v4/projects/72936/issues/265567/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => Response.of([])));

      await tester.pumpWidget(MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (context) => ConnectionProvider()),
            ChangeNotifierProvider(create: (context) => ProjectProvider()),
          ],
          child: MaterialApp(
            home: Scaffold(
              key: GlobalKeys.rootAppKey,
              body: IssueDetailsPage(arguments: params),
            ),
          )));

      await tester.pumpAndSettle();
      expect(find.textContaining("demo mr test"), findsOneWidget);

      // no up votes for default
      var thumbsUpButton = find.byWidgetPredicate((widget) => widget is AwardEmojiButton && widget.svgAssetPath == 'assets/images/thumb-up.svg');
      expect(thumbsUpButton, findsOneWidget);
      expect(find.byWidgetPredicate((widget) {
        return widget is AwardEmojiButton &&
            widget.svgAssetPath == 'assets/images/thumb-up.svg' &&
            widget.emojiColor == const Color(0xFF87878C) &&
            widget.title == null &&
            widget.backgroundColor == const Color(0xFFEAEAEA);
      }), findsOneWidget);

      // tap thumbsUpButton for up votes
      map['data']['project']['issue']['upvotes'] = 1;
      when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 265567), useActiveConnection: true))
          .thenAnswer((_) => Future(() => Response.of<dynamic>(map)));
      when(client.get("/api/v4/projects/72936/issues/265567/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => Response.of([
            {
              "id": 15138,
              "name": "thumbsup",
              "user": {
                "id": 9527,
                "username": "tester",
                "name": "tester",
                "state": "active",
                "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/9527/avatar1.png",
                "web_url": "https://jihulab.com/tester"
              },
              "created_at": "2023-03-16T08:58:51.589+08:00",
              "updated_at": "2023-03-16T08:58:51.589+08:00",
              "awardable_id": 360522,
              "awardable_type": "Issue",
              "url": null
            }
          ])));
      when(client.post<dynamic>('/api/v4/projects/72936/issues/265567/award_emoji?name=thumbsup', {}, useActiveConnection: true)).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
      await tester.tap(thumbsUpButton);
      await tester.pumpAndSettle();
      expect(find.byWidgetPredicate((widget) {
        return widget is AwardEmojiButton &&
            widget.svgAssetPath == 'assets/images/thumb-up.svg' &&
            widget.emojiColor == Theme.of(GlobalKeys.rootAppKey.currentContext!).primaryColor &&
            widget.title == '1' &&
            widget.backgroundColor == const Color(0xFFFFEAE0);
      }), findsOneWidget);

      // tap thumbsUpButton for remove up votes
      map['data']['project']['issue']['upvotes'] = 0;
      when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 265567), useActiveConnection: true))
          .thenAnswer((_) => Future(() => Response.of<dynamic>(map)));
      when(client.get("/api/v4/projects/72936/issues/265567/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => Response.of([])));
      when(client.delete<dynamic>('/api/v4/projects/72936/issues/265567/award_emoji/15138', connection: anyNamed('connection'))).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
      await tester.tap(thumbsUpButton);
      await tester.pumpAndSettle();
      expect(find.byWidgetPredicate((widget) {
        return widget is AwardEmojiButton &&
            widget.svgAssetPath == 'assets/images/thumb-up.svg' &&
            widget.emojiColor == const Color(0xFF87878C) &&
            widget.title == null &&
            widget.backgroundColor == const Color(0xFFEAEAEA);
      }), findsOneWidget);
    });

    testWidgets('Should display thumbs down button', (tester) async {
      await setUpMobileBinding(tester);
      Map<String, dynamic> map = Map.from(issuePostVotesGraphQLResponse);
      map['data']['project']['issue']['downvotes'] = 0;
      when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 265567), useActiveConnection: true))
          .thenAnswer((_) => Future(() => Response.of<dynamic>(map)));
      when(client.get("/api/v4/projects/72936/issues/265567/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => Response.of([])));

      await tester.pumpWidget(MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (context) => ConnectionProvider()),
            ChangeNotifierProvider(create: (context) => ProjectProvider()),
          ],
          child: MaterialApp(
            home: Scaffold(
              key: GlobalKeys.rootAppKey,
              body: IssueDetailsPage(arguments: params),
            ),
          )));

      await tester.pumpAndSettle();
      expect(find.textContaining("demo mr test"), findsOneWidget);

      // no down votes for default
      var thumbsDownButton = find.byWidgetPredicate((widget) => widget is AwardEmojiButton && widget.svgAssetPath == 'assets/images/thumb-down.svg');
      expect(thumbsDownButton, findsOneWidget);

      expect(find.byWidgetPredicate((widget) {
        return widget is AwardEmojiButton &&
            widget.svgAssetPath == 'assets/images/thumb-down.svg' &&
            widget.emojiColor == const Color(0xFF87878C) &&
            widget.title == null &&
            widget.backgroundColor == const Color(0xFFEAEAEA);
      }), findsOneWidget);

      // tap thumbsDownButton for down votes
      map['data']['project']['issue']['downvotes'] = 1;
      when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 265567), useActiveConnection: true))
          .thenAnswer((_) => Future(() => Response.of<dynamic>(map)));
      when(client.get("/api/v4/projects/72936/issues/265567/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => Response.of([
            {
              "id": 15138,
              "name": "thumbsdown",
              "user": {
                "id": 9527,
                "username": "tester",
                "name": "tester",
                "state": "active",
                "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/9527/avatar1.png",
                "web_url": "https://jihulab.com/tester"
              },
              "created_at": "2023-03-16T08:58:51.589+08:00",
              "updated_at": "2023-03-16T08:58:51.589+08:00",
              "awardable_id": 360522,
              "awardable_type": "Issue",
              "url": null
            }
          ])));
      when(client.post<dynamic>('/api/v4/projects/72936/issues/265567/award_emoji?name=thumbsdown', {}, useActiveConnection: true)).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
      await tester.tap(thumbsDownButton);
      await tester.pumpAndSettle();
      expect(find.byWidgetPredicate((widget) {
        return widget is AwardEmojiButton &&
            widget.svgAssetPath == 'assets/images/thumb-down.svg' &&
            widget.emojiColor == Theme.of(GlobalKeys.rootAppKey.currentContext!).primaryColor &&
            widget.title == '1' &&
            widget.backgroundColor == const Color(0xFFFFEAE0);
      }), findsOneWidget);

      // tap thumbsDownButton for remove down votes
      map['data']['project']['issue']['downvotes'] = 0;
      when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 265567), useActiveConnection: true))
          .thenAnswer((_) => Future(() => Response.of<dynamic>(map)));
      when(client.get("/api/v4/projects/72936/issues/265567/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => Response.of([])));
      when(client.delete<dynamic>('/api/v4/projects/72936/issues/265567/award_emoji/15138', connection: anyNamed('connection'))).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
      await tester.tap(thumbsDownButton);
      await tester.pumpAndSettle();
      expect(find.byWidgetPredicate((widget) {
        return widget is AwardEmojiButton &&
            widget.svgAssetPath == 'assets/images/thumb-down.svg' &&
            widget.emojiColor == const Color(0xFF87878C) &&
            widget.title == null &&
            widget.backgroundColor == const Color(0xFFEAEAEA);
      }), findsOneWidget);
    });
  });

  group("Should be able to edit issue`s milestone", () {
    testWidgets("cancel association", (tester) async {
      when(client.put<dynamic>("/api/v4/projects/72936/issues/265567", {"milestone_id": null})).thenAnswer((_) => Future(() => Response.of<dynamic>({"id": 265567})));
      when(client.post<dynamic>('/api/graphql', projectMilestoneRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 20, "", state: "active")))
          .thenAnswer((_) => Future(() => Response.of<dynamic>(milestonesResponse)));
      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
          child: MaterialApp(home: Scaffold(body: IssueDetailsPage(arguments: params)))));

      await tester.pumpAndSettle(const Duration(seconds: 1));
      expect(find.text("Milestone"), findsOneWidget);
      expect(find.text("里程碑1"), findsOneWidget);
      var svgFinder = SvgFinder("assets/images/edit_pencil.svg", semanticsLabel: "edit_issue_milestone");
      expect(svgFinder, findsOneWidget);
      await tester.tap(svgFinder);
      await tester.pumpAndSettle();
      expect(find.byType(MilestoneSelectorPage), findsOneWidget);
      expect(find.text("Done"), findsOneWidget);
      expect(find.widgetWithText(ProjectMilestoneItemView, "里程碑1"), findsOneWidget);
      expect(find.byIcon(Icons.radio_button_unchecked), findsOneWidget);
      expect(find.byIcon(Icons.check_circle), findsOneWidget);
      await tester.tap(find.byIcon(Icons.check_circle));
      await tester.tap(find.text("Done"));
      await tester.pumpAndSettle();
      expect(find.byType(MilestoneSelectorPage), findsNothing);
    });

    testWidgets("associate milestone", (tester) async {
      when(client.put<dynamic>("/api/v4/projects/72936/issues/265567", {"milestone_id": 6675})).thenAnswer((_) => Future(() => Response.of<dynamic>({"id": 265567})));
      when(client.post<dynamic>('/api/graphql', projectMilestoneRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 20, "", state: "active")))
          .thenAnswer((_) => Future(() => Response.of<dynamic>(milestonesResponse)));
      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
          child: MaterialApp(home: Scaffold(body: IssueDetailsPage(arguments: params)))));

      await tester.pumpAndSettle(const Duration(seconds: 1));
      expect(find.text("Milestone"), findsOneWidget);
      expect(find.text("里程碑1"), findsOneWidget);
      var svgFinder = SvgFinder("assets/images/edit_pencil.svg", semanticsLabel: "edit_issue_milestone");
      expect(svgFinder, findsOneWidget);
      await tester.tap(svgFinder);
      await tester.pumpAndSettle();
      expect(find.byType(MilestoneSelectorPage), findsOneWidget);
      expect(find.text("Done"), findsOneWidget);
      expect(find.widgetWithText(ProjectMilestoneItemView, "里程碑2"), findsOneWidget);
      expect(find.byIcon(Icons.radio_button_unchecked), findsOneWidget);
      expect(find.byIcon(Icons.check_circle), findsOneWidget);
      await tester.tap(find.byIcon(Icons.radio_button_unchecked).last);
      await tester.pumpAndSettle();
      await tester.tap(find.text("Done"));
      await tester.pumpAndSettle();
      expect(find.byType(MilestoneSelectorPage), findsNothing);
      verify(client.put<dynamic>("/api/v4/projects/72936/issues/265567", {"milestone_id": 6675})).called(1);
    });
  });

  group("Should be to able to update the issue list after editing any issue", () {
    setUp(() {
      when(client.post<dynamic>("/api/graphql", getProjectIssuesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo', '', 20))).thenAnswer((_) => Future(() => Response.of(_projectIssuesResponse)));
      when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo', 77))).thenAnswer((_) => Future(() => Response.of<dynamic>(_issueDetailResponse())));
      when(client.get<List<dynamic>>("/api/v4/projects/72936/issues/77/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
      when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo', 77), useActiveConnection: true))
          .thenAnswer((_) => Future(() => Response.of<dynamic>(issuePostVotesGraphQLResponse)));
      when(client.get("/api/v4/projects/72936/issues/77/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => Response.of([])));
      when(client.put("/api/v4/projects/72936/issues/77", {"state_event": "close"})).thenAnswer((_) => Future(() => Response.of({})));
    });

    testWidgets("close issue", (tester) async {
      when(client.get("/api/v4/projects/72936/issues/77/links")).thenAnswer((_) => Future(() => Response.of([])));
      var builder = MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
        child: MaterialApp(
            onGenerateRoute: onGenerateRoute,
            home: Scaffold(body: ProjectIssuesPage(refreshKey: GlobalKey(), projectId: 59893, relativePath: 'ultimate-plan/jihu-gitlab-app/demo', onSelectedIssueChange: null))),
      );

      await tester.pumpWidget(builder);
      await tester.pumpAndSettle();
      expect(find.byType(IssuesPage), findsOneWidget);
      expect(find.text('test label style'), findsOneWidget);

      await tester.tap(find.text('test label style'));
      await tester.pumpAndSettle(const Duration(seconds: 1));
      expect(find.byType(IssueDetailsPage), findsOneWidget);
      await tester.tap(SvgFinder("assets/images/operate.svg"));
      await tester.pumpAndSettle();
      when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo', 77)))
          .thenAnswer((_) => Future(() => Response.of<dynamic>(_issueDetailResponse(state: "closed"))));
      await tester.tap(find.text("Close issue"));
      await tester.pumpAndSettle();
      await tester.tap(find.byType(BackButton));
      await tester.pumpAndSettle();
      expect(find.text('test label style'), findsNothing);
    });
  });

  group("Should be able to show link items", () {
    testWidgets("has link items", (tester) async {
      when(client.get("/api/v4/projects/72936/issues/265567/links")).thenAnswer((_) => Future(() => Response.of(linkItemsResponse)));
      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
          child: MaterialApp(home: Scaffold(body: IssueDetailsPage(arguments: params)))));

      await tester.pumpAndSettle(const Duration(seconds: 1));
      expect(find.text("Linked items"), findsOneWidget);
      expect(find.text("Blocks"), findsOneWidget);
      expect(find.widgetWithText(IssueItemView, "Test"), findsOneWidget);
      expect(find.text("Is blocked by"), findsOneWidget);
      expect(find.widgetWithText(IssueItemView, "保密"), findsOneWidget);
      expect(find.text("Relates to"), findsOneWidget);
      expect(find.widgetWithText(IssueItemView, "发个合集"), findsOneWidget);
      expect(find.widgetWithText(IssueItemView, "Bugfix: 无法选择已关闭的里程碑"), findsOneWidget);
    });

    testWidgets("no link items", (tester) async {
      when(client.get("/api/v4/projects/72936/issues/265567/links")).thenAnswer((_) => Future(() => Response.of([])));
      await tester.pumpWidget(MultiProvider(
          providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
          child: MaterialApp(home: Scaffold(body: IssueDetailsPage(arguments: params)))));

      await tester.pumpAndSettle(const Duration(seconds: 1));
      expect(find.text("Linked items"), findsOneWidget);
      expect(find.byType(DelimitedColumn), findsNothing);
      expect(find.widgetWithText(IssueLinksView, "None"), findsOneWidget);
    });
  });
  tearDown(() {
    reset(client);
    ConnectionProvider().fullReset();
    ProjectProvider().fullReset();
  });
}

Map<String, dynamic> milestonesResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/72936",
      "name": "private demo",
      "milestones": {
        "nodes": [
          {"id": "gid://gitlab/Milestone/6674", "title": "里程碑1", "startDate": "2023-03-01", "dueDate": "2023-03-31", "state": "active"},
          {"id": "gid://gitlab/Milestone/6675", "title": "里程碑2", "startDate": null, "dueDate": null, "state": "closed"}
        ],
        "pageInfo": {"hasNextPage": false, "hasPreviousPage": false, "startCursor": "eyJkdWVfZGF0ZSI6IjIwMjMtMDMtMzEiLCJpZCI6IjY3MDYifQ", "endCursor": "eyJkdWVfZGF0ZSI6bnVsbCwiaWQiOiI2Njc1In0"}
      },
    }
  }
};

Map<String, dynamic> _projectIssuesResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/72936",
      "path": "demo",
      "fullPath": "ultimate-plan/jihu-gitlab-app/demo",
      "name": "private demo",
      "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / private demo",
      "issues": {
        "nodes": [
          {
            "id": "gid://gitlab/Issue/359636",
            "iid": "77",
            "state": "opened",
            "title": "test label style",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/29758", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png", "name": "ling zhang", "username": "zhangling"},
            "assignees": {
              "nodes": [
                {"id": "gid://gitlab/User/29355", "name": "miaolu", "username": "perity", "avatarUrl": "/uploads/-/system/user/avatar/29355/avatar.png"},
                {"id": "gid://gitlab/User/29758", "name": "ling zhang", "username": "zhangling", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png"}
              ]
            },
            "labels": {
              "nodes": [
                {"id": "gid://gitlab/ProjectLabel/79283", "title": "A:B:CC", "description": null, "color": "#ff0000", "textColor": "#FFFFFF"},
                {"id": "gid://gitlab/ProjectLabel/73257", "title": "Test::Title", "description": "", "color": "#6699cc", "textColor": "#FFFFFF"}
              ]
            },
            "confidential": false,
            "milestone": null,
            "createdAt": "2023-03-09T17:16:28+08:00",
            "updatedAt": "2023-03-30T18:12:22+08:00"
          },
        ],
        "pageInfo": {"hasNextPage": true, "endCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMy0wMS0wNiAxMzowMzoxNy40OTY2NjEwMDAgKzA4MDAiLCJpZCI6IjMwMTYwMyJ9"}
      }
    }
  }
};

Map<String, dynamic> _issueDetailResponse({String state = "opened"}) => {
      "data": {
        "project": {
          "id": "gid://gitlab/Project/72936",
          "name": "private demo",
          "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / private demo",
          "path": "demo",
          "fullPath": "ultimate-plan/jihu-gitlab-app/demo",
          "issue": {
            "id": "gid://gitlab/Issue/359636",
            "iid": "77",
            "title": "test label style",
            "description": "",
            "webUrl": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/-/issues/77",
            "confidential": false,
            "state": state,
            "createdAt": "2023-03-09T17:16:28+08:00",
            "author": {"id": "gid://gitlab/User/29758", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png", "name": "ling zhang", "username": "zhangling"},
            "assignees": {
              "nodes": [
                {"id": "gid://gitlab/User/29758", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png", "name": "ling zhang", "username": "zhangling"},
                {"id": "gid://gitlab/User/29355", "avatarUrl": "/uploads/-/system/user/avatar/29355/avatar.png", "name": "miaolu", "username": "perity"}
              ]
            },
            "labels": {
              "nodes": [
                {"id": "gid://gitlab/ProjectLabel/79283", "title": "A:B:CC", "description": null, "color": "#ff0000", "textColor": "#FFFFFF"},
                {"id": "gid://gitlab/ProjectLabel/73257", "title": "Test::Title", "description": "", "color": "#6699cc", "textColor": "#FFFFFF"}
              ]
            }
          }
        }
      }
    };

List<Map<String, dynamic>> linkItemsResponse = [
  {
    "id": 366337,
    "iid": 88,
    "project_id": 72936,
    "title": "保密",
    "description": "",
    "state": "opened",
    "created_at": "2023-04-16T09:47:59.780+08:00",
    "updated_at": "2023-04-17T13:47:08.117+08:00",
    "closed_at": null,
    "closed_by": null,
    "labels": [],
    "milestone": null,
    "assignees": [],
    "author": {
      "id": 23836,
      "username": "jojo0",
      "name": "yajie xue",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/23836/avatar.png",
      "web_url": "https://jihulab.com/jojo0"
    },
    "type": "ISSUE",
    "assignee": null,
    "user_notes_count": 0,
    "merge_requests_count": 0,
    "upvotes": 0,
    "downvotes": 0,
    "due_date": null,
    "confidential": false,
    "discussion_locked": null,
    "issue_type": "issue",
    "web_url": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/-/issues/88",
    "time_stats": {"time_estimate": 0, "total_time_spent": 0, "human_time_estimate": null, "human_total_time_spent": null},
    "task_completion_status": {"count": 0, "completed_count": 0},
    "weight": null,
    "blocking_issues_count": 1,
    "has_tasks": true,
    "task_status": "",
    "_links": {
      "self": "https://jihulab.com/api/v4/projects/72936/issues/88",
      "notes": "https://jihulab.com/api/v4/projects/72936/issues/88/notes",
      "award_emoji": "https://jihulab.com/api/v4/projects/72936/issues/88/award_emoji",
      "project": "https://jihulab.com/api/v4/projects/72936",
      "closed_as_duplicate_of": null
    },
    "references": {"short": "#88", "relative": "#88", "full": "ultimate-plan/jihu-gitlab-app/demo#88"},
    "severity": "UNKNOWN",
    "moved_to_id": null,
    "service_desk_reply_to": null,
    "epic_iid": null,
    "epic": null,
    "iteration": null,
    "health_status": null,
    "issue_link_id": 9662,
    "link_type": "is_blocked_by",
    "link_created_at": "2023-04-17T05:47:08.055Z",
    "link_updated_at": "2023-04-17T05:47:08.055Z"
  },
  {
    "id": 366208,
    "iid": 86,
    "project_id": 72936,
    "title": "Test",
    "description": "",
    "state": "opened",
    "created_at": "2023-04-14T15:16:38.741+08:00",
    "updated_at": "2023-04-17T13:47:56.895+08:00",
    "closed_at": null,
    "closed_by": null,
    "labels": ["Label"],
    "milestone": {
      "id": 6705,
      "iid": 5,
      "project_id": 72936,
      "title": "韩国胡静韩剧i",
      "description": "",
      "state": "active",
      "created_at": "2023-03-30T10:01:18.933+08:00",
      "updated_at": "2023-03-30T10:01:18.933+08:00",
      "due_date": null,
      "start_date": "2023-03-23",
      "expired": false,
      "web_url": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/-/milestones/5"
    },
    "assignees": [],
    "author": {
      "id": 23836,
      "username": "jojo0",
      "name": "yajie xue",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/23836/avatar.png",
      "web_url": "https://jihulab.com/jojo0"
    },
    "type": "ISSUE",
    "assignee": null,
    "user_notes_count": 0,
    "merge_requests_count": 0,
    "upvotes": 0,
    "downvotes": 0,
    "due_date": null,
    "confidential": false,
    "discussion_locked": null,
    "issue_type": "issue",
    "web_url": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/-/issues/86",
    "time_stats": {"time_estimate": 0, "total_time_spent": 0, "human_time_estimate": null, "human_total_time_spent": null},
    "task_completion_status": {"count": 0, "completed_count": 0},
    "weight": null,
    "blocking_issues_count": 0,
    "has_tasks": true,
    "task_status": "",
    "_links": {
      "self": "https://jihulab.com/api/v4/projects/72936/issues/86",
      "notes": "https://jihulab.com/api/v4/projects/72936/issues/86/notes",
      "award_emoji": "https://jihulab.com/api/v4/projects/72936/issues/86/award_emoji",
      "project": "https://jihulab.com/api/v4/projects/72936",
      "closed_as_duplicate_of": null
    },
    "references": {"short": "#86", "relative": "#86", "full": "ultimate-plan/jihu-gitlab-app/demo#86"},
    "severity": "UNKNOWN",
    "moved_to_id": null,
    "service_desk_reply_to": null,
    "epic_iid": null,
    "epic": null,
    "iteration": null,
    "health_status": null,
    "issue_link_id": 9663,
    "link_type": "blocks",
    "link_created_at": "2023-04-17T05:47:56.468Z",
    "link_updated_at": "2023-04-17T05:47:56.468Z"
  },
  {
    "id": 325598,
    "iid": 68,
    "project_id": 72936,
    "title": "发个合集",
    "description": "## bhjlkaaa",
    "state": "opened",
    "created_at": "2023-02-20T10:18:11.920+08:00",
    "updated_at": "2023-04-17T17:33:13.951+08:00",
    "closed_at": null,
    "closed_by": null,
    "labels": ["Test::Title::Subtitle"],
    "milestone": {
      "id": 6790,
      "iid": 9,
      "project_id": 72936,
      "title": "state test1",
      "description": "",
      "state": "active",
      "created_at": "2023-04-12T00:33:26.498+08:00",
      "updated_at": "2023-04-12T00:33:26.498+08:00",
      "due_date": "2023-04-30",
      "start_date": "2023-04-20",
      "expired": false,
      "web_url": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/-/milestones/9"
    },
    "assignees": [],
    "author": {
      "id": 23836,
      "username": "jojo0",
      "name": "yajie xue",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/23836/avatar.png",
      "web_url": "https://jihulab.com/jojo0"
    },
    "type": "ISSUE",
    "assignee": null,
    "user_notes_count": 0,
    "merge_requests_count": 0,
    "upvotes": 0,
    "downvotes": 0,
    "due_date": null,
    "confidential": false,
    "discussion_locked": null,
    "issue_type": "issue",
    "web_url": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/demo/-/issues/68",
    "time_stats": {"time_estimate": 0, "total_time_spent": 0, "human_time_estimate": null, "human_total_time_spent": null},
    "task_completion_status": {"count": 0, "completed_count": 0},
    "weight": null,
    "blocking_issues_count": 0,
    "has_tasks": true,
    "task_status": "0 of 0 checklist items completed",
    "_links": {
      "self": "https://jihulab.com/api/v4/projects/72936/issues/68",
      "notes": "https://jihulab.com/api/v4/projects/72936/issues/68/notes",
      "award_emoji": "https://jihulab.com/api/v4/projects/72936/issues/68/award_emoji",
      "project": "https://jihulab.com/api/v4/projects/72936",
      "closed_as_duplicate_of": null
    },
    "references": {"short": "#68", "relative": "#68", "full": "ultimate-plan/jihu-gitlab-app/demo#68"},
    "severity": "UNKNOWN",
    "moved_to_id": null,
    "service_desk_reply_to": null,
    "epic_iid": null,
    "epic": null,
    "iteration": {
      "id": 1496,
      "iid": 43,
      "sequence": 18,
      "group_id": 88966,
      "title": null,
      "description": "1. 项目管理：查看项目迭代\n1. 代码管理：启动，至少要看到 readme\n1. 开发者社区：更新并查看帖子",
      "state": 3,
      "created_at": "2023-02-28T00:05:01.778+08:00",
      "updated_at": "2023-03-20T00:05:16.363+08:00",
      "start_date": "2023-03-13",
      "due_date": "2023-03-19",
      "web_url": "https://jihulab.com/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1496"
    },
    "health_status": null,
    "issue_link_id": 9665,
    "link_type": "relates_to",
    "link_created_at": "2023-04-17T06:33:20.037Z",
    "link_updated_at": "2023-04-17T06:33:20.037Z"
  },
  {
    "id": 366401,
    "iid": 1030,
    "project_id": 59893,
    "title": "Bugfix: 无法选择已关闭的里程碑",
    "description":
        "## App 版本:\n1.25.0（448）\n## 测试设备:\n\niPhone\n\n[//]: # (Android)\n\n[//]: # (iPad)\n\n## 重现步骤：\n\n1. 进入议题详情\n2. 为议题添加里程碑\n\n\n## 重现概率：\n\n[//]: # (偶现)\n\n必现\n\n## 实际结果：\n\n1.出现状态为已关闭的里程碑\n\n![image](/uploads/a7161238a08ece4579631abb500536e3/image.png)\n\n## 期望结果：\n\n1. 过滤状态为已关闭的里程碑",
    "state": "opened",
    "created_at": "2023-04-17T09:50:26.223+08:00",
    "updated_at": "2023-04-17T16:11:09.898+08:00",
    "closed_at": null,
    "closed_by": null,
    "labels": ["status::ready for test", "type::bug"],
    "milestone": null,
    "assignees": [
      {
        "id": 29758,
        "username": "zhangling",
        "name": "ling zhang",
        "state": "active",
        "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29758/avatar.png",
        "web_url": "https://jihulab.com/zhangling"
      }
    ],
    "author": {
      "id": 23836,
      "username": "jojo0",
      "name": "yajie xue",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/23836/avatar.png",
      "web_url": "https://jihulab.com/jojo0"
    },
    "type": "ISSUE",
    "assignee": {
      "id": 29758,
      "username": "zhangling",
      "name": "ling zhang",
      "state": "active",
      "avatar_url": "https://jihulab.com/uploads/-/system/user/avatar/29758/avatar.png",
      "web_url": "https://jihulab.com/zhangling"
    },
    "user_notes_count": 0,
    "merge_requests_count": 1,
    "upvotes": 0,
    "downvotes": 0,
    "due_date": null,
    "confidential": false,
    "discussion_locked": null,
    "issue_type": "issue",
    "web_url": "https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/issues/1030",
    "time_stats": {"time_estimate": 0, "total_time_spent": 0, "human_time_estimate": null, "human_total_time_spent": null},
    "task_completion_status": {"count": 0, "completed_count": 0},
    "weight": null,
    "blocking_issues_count": 0,
    "has_tasks": true,
    "task_status": "0 of 0 checklist items completed",
    "_links": {
      "self": "https://jihulab.com/api/v4/projects/59893/issues/1030",
      "notes": "https://jihulab.com/api/v4/projects/59893/issues/1030/notes",
      "award_emoji": "https://jihulab.com/api/v4/projects/59893/issues/1030/award_emoji",
      "project": "https://jihulab.com/api/v4/projects/59893",
      "closed_as_duplicate_of": null
    },
    "references": {"short": "#1030", "relative": "jihu-gitlab-app#1030", "full": "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app#1030"},
    "severity": "UNKNOWN",
    "moved_to_id": null,
    "service_desk_reply_to": null,
    "epic_iid": null,
    "epic": null,
    "iteration": {
      "id": 1709,
      "iid": 59,
      "sequence": 10,
      "group_id": 88966,
      "title": null,
      "description": null,
      "state": 2,
      "created_at": "2023-04-10T08:26:56.708+08:00",
      "updated_at": "2023-04-17T00:05:04.884+08:00",
      "start_date": "2023-04-17",
      "due_date": "2023-04-30",
      "web_url": "https://jihulab.com/groups/ultimate-plan/jihu-gitlab-app/-/iterations/1709"
    },
    "health_status": null,
    "issue_link_id": 9668,
    "link_type": "relates_to",
    "link_created_at": "2023-04-17T07:49:21.122Z",
    "link_updated_at": "2023-04-17T07:49:21.122Z"
  }
];
