import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/modules/issues/details/issue_details_param.dart';

void main() {
  test('Should get issue detail param hashcode', () {
    expect(IssueDetailsParam.fromJson({}).hashCode, isNonZero);
  });
}
