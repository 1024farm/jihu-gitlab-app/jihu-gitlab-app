import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/license_type_detector.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/modules/issues/details/issue_details_page.dart';
import 'package:jihu_gitlab_app/modules/issues/details/link_issues_page.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion_provider.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_model.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_link.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_page.dart';
import 'package:jihu_gitlab_app/modules/issues/list/project_issues_gq_request_body.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../core/net/http_request_test.mocks.dart';
import '../../../finder/svg_finder.dart';
import '../../../mocker/tester.dart';
import '../../../test_data/discussion.dart';
import '../../../test_data/issue.dart';
import '../../../test_data/members.dart';
import 'issue_details_page_test.mocks.dart';

final client = MockHttpClient();

@GenerateNiceMocks([MockSpec<DiscussionProvider>()])
void main() {
  var issueDetailsModel = IssueDetailsModel();
  locator.registerSingleton(issueDetailsModel);
  var provider = MockDiscussionProvider();
  issueDetailsModel.injectDataProviderForTesting(provider);

  var params = {'projectId': 72936, 'issueId': 3242, 'issueIid': 6, 'targetId': 45345, 'targetIid': 265567, 'pathWithNamespace': 'ultimate-plan/jihu-gitlab-app/demo-mr-test'};
  setUp(() async {
    SharedPreferences.setMockInitialValues(<String, Object>{});
    await LocalStorage.init();
    await ConnectionProvider().restore();
    ConnectionProvider().reset(Tester.jihuLabUser());

    when(client.get<List<dynamic>>("/api/v4/projects/0/issues/0/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>([])));
    when(client.get<List<dynamic>>("/api/v4/projects/72936/issues/265567/discussions?page=1&per_page=50")).thenAnswer((_) => Future(() => Response.of<List<dynamic>>(discussionsData)));
    when(client.post<dynamic>("/api/v4/projects/72936/issues/265567/notes", any)).thenAnswer((_) => Future(() => Response.of<dynamic>({})));
    when(client.get<List<dynamic>>("/api/v4/projects/72936/members/all?page=1&per_page=50")).thenAnswer((_) async => Future.value(Response.of(members)));
    when(client.post('/api/graphql', getIssueDetailsGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 265567)))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(issueDetailsGraphQLResponse)));
    when(client.post('/api/graphql', getIssuePostVotesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', 265567), useActiveConnection: true))
        .thenAnswer((_) => Future(() => Response.of<dynamic>(issuePostVotesGraphQLResponse)));
    when(client.get("/api/v4/projects/72936/issues/265567/award_emoji?per_page=100&page=1", useActiveConnection: true)).thenAnswer((_) => Future(() => Response.of([])));
    HttpClient.injectInstanceForTesting(client);
  });

  testWidgets("Should be able to link issues with any type in paid version", (tester) async {
    when(client.post<dynamic>("/api/graphql", getProjectIssuesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', '', 20)))
        .thenAnswer((_) => Future(() => Response.of(_projectIssuesResponse)));
    when(client.post<dynamic>("/api/graphql", requestBodyOfLicenseType('ultimate-plan/jihu-gitlab-app/demo-mr-test'))).thenAnswer((_) => Future(() => Response.of(_ultimateLicenseResponse)));
    when(client.get("/api/v4/projects/72936/issues/265567/links")).thenAnswer((_) => Future(() => Response.of([])));
    when(client.post("/api/v4/projects/72936/issues/265567/links?target_project_id=72936&target_issue_iid=77&link_type=blocks", {})).thenAnswer((_) => Future(() => Response.of({})));
    await tester.pumpWidget(MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
        child: MaterialApp(home: Scaffold(body: IssueDetailsPage(arguments: params)))));

    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.text("Linked items"), findsOneWidget);
    expect(SvgFinder("assets/images/plus.svg"), findsOneWidget);
    await tester.tap(SvgFinder("assets/images/plus.svg"));
    await tester.pumpAndSettle();
    expect(find.widgetWithText(CommonAppBar, "Linked items"), findsOneWidget);
    expect(find.widgetWithText(Opacity, "Done"), findsOneWidget);
    expect(find.text("The current issue"), findsOneWidget);
    expect(find.text("The following issue"), findsOneWidget);
    expect(find.byType(IssuesPage), findsOneWidget);
    expect(find.text("test label style"), findsOneWidget);
    expect(find.byIcon(Icons.check_circle), findsNothing);
    expect(find.byIcon(Icons.radio_button_unchecked), findsOneWidget);
    expect(find.byType(Checkbox), findsNWidgets(3));
    expect(tester.widget<Checkbox>(find.byKey(Key(IssueLinkType.relatesTo.name))).value, true);
    expect(tester.widget<Checkbox>(find.byKey(Key(IssueLinkType.blocks.name))).value, false);
    expect(tester.widget<Checkbox>(find.byKey(Key(IssueLinkType.isBlockedBy.name))).value, false);
    expect(find.text("Blocks"), findsOneWidget);
    await tester.tap(find.text("Blocks"));
    await tester.pumpAndSettle();
    expect(tester.widget<Checkbox>(find.byKey(Key(IssueLinkType.relatesTo.name))).value, false);
    expect(tester.widget<Checkbox>(find.byKey(Key(IssueLinkType.blocks.name))).value, true);
    expect(find.byType(CupertinoAlertDialog), findsNothing);
    await tester.tap(find.text("test label style"));
    await tester.pumpAndSettle();
    expect(find.byIcon(Icons.check_circle), findsOneWidget);
    expect(find.byIcon(Icons.radio_button_unchecked), findsNothing);
    await tester.tap(find.text("Done"));
    await tester.pumpAndSettle();
    expect(find.byType(LinkIssuesPage), findsNothing);
    verify(client.get("/api/v4/projects/72936/issues/265567/links")).called(2);
  });

  testWidgets("Should be able to link issues with relates_to in free version", (tester) async {
    when(client.post<dynamic>("/api/graphql", getProjectIssuesGraphQLRequestBody('ultimate-plan/jihu-gitlab-app/demo-mr-test', '', 20)))
        .thenAnswer((_) => Future(() => Response.of(_projectIssuesResponse)));
    when(client.post<dynamic>("/api/graphql", requestBodyOfLicenseType('ultimate-plan/jihu-gitlab-app/demo-mr-test'))).thenAnswer((_) => Future(() => Response.of(_freeLicenseResponse)));
    when(client.get("/api/v4/projects/72936/issues/265567/links")).thenAnswer((_) => Future(() => Response.of([])));
    await tester.pumpWidget(MultiProvider(
        providers: [ChangeNotifierProvider(create: (context) => ConnectionProvider()), ChangeNotifierProvider(create: (context) => ProjectProvider())],
        child: MaterialApp(home: Scaffold(body: IssueDetailsPage(arguments: params)))));

    await tester.pumpAndSettle(const Duration(seconds: 1));
    expect(find.text("Linked items"), findsOneWidget);
    expect(SvgFinder("assets/images/plus.svg"), findsOneWidget);
    await tester.tap(SvgFinder("assets/images/plus.svg"));
    await tester.pumpAndSettle();
    expect(find.widgetWithText(CommonAppBar, "Linked items"), findsOneWidget);
    expect(find.widgetWithText(Opacity, "Done"), findsOneWidget);
    expect(find.text("The current issue"), findsOneWidget);
    expect(find.text("The following issue"), findsOneWidget);
    expect(find.byType(IssuesPage), findsOneWidget);
    expect(find.text("test label style"), findsOneWidget);
    expect(find.byIcon(Icons.check_circle), findsNothing);
    expect(find.byIcon(Icons.radio_button_unchecked), findsOneWidget);
    expect(find.byType(Checkbox), findsNWidgets(3));
    expect(tester.widget<Checkbox>(find.byKey(Key(IssueLinkType.relatesTo.name))).value, true);
    expect(tester.widget<Checkbox>(find.byKey(Key(IssueLinkType.blocks.name))).value, false);
    expect(tester.widget<Checkbox>(find.byKey(Key(IssueLinkType.isBlockedBy.name))).value, false);
    expect(find.text("Blocks"), findsOneWidget);
    await tester.tap(find.text("Blocks"));
    await tester.pumpAndSettle();
    expect(find.byType(CupertinoAlertDialog), findsOneWidget);
    expect(find.text("Feature Not Supported"), findsOneWidget);
    expect(tester.widget<Checkbox>(find.byKey(Key(IssueLinkType.relatesTo.name))).value, true);
    expect(tester.widget<Checkbox>(find.byKey(Key(IssueLinkType.blocks.name))).value, false);
  });

  tearDown(() {
    reset(client);
    ConnectionProvider().fullReset();
    ProjectProvider().fullReset();
  });
}

Map<String, dynamic> _projectIssuesResponse = {
  "data": {
    "project": {
      "id": "gid://gitlab/Project/72936",
      "path": "demo",
      "fullPath": "ultimate-plan/jihu-gitlab-app/demo",
      "name": "private demo",
      "nameWithNamespace": "旗舰版演示 / 极狐 GitLab App 产品线 / private demo",
      "issues": {
        "nodes": [
          {
            "id": "gid://gitlab/Issue/359636",
            "iid": "77",
            "state": "opened",
            "title": "test label style",
            "type": "ISSUE",
            "author": {"id": "gid://gitlab/User/29758", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png", "name": "ling zhang", "username": "zhangling"},
            "assignees": {
              "nodes": [
                {"id": "gid://gitlab/User/29355", "name": "miaolu", "username": "perity", "avatarUrl": "/uploads/-/system/user/avatar/29355/avatar.png"},
                {"id": "gid://gitlab/User/29758", "name": "ling zhang", "username": "zhangling", "avatarUrl": "/uploads/-/system/user/avatar/29758/avatar.png"}
              ]
            },
            "labels": {
              "nodes": [
                {"id": "gid://gitlab/ProjectLabel/79283", "title": "A:B:CC", "description": null, "color": "#ff0000", "textColor": "#FFFFFF"},
                {"id": "gid://gitlab/ProjectLabel/73257", "title": "Test::Title", "description": "", "color": "#6699cc", "textColor": "#FFFFFF"}
              ]
            },
            "confidential": false,
            "milestone": null,
            "createdAt": "2023-03-09T17:16:28+08:00",
            "updatedAt": "2023-03-30T18:12:22+08:00"
          },
        ],
        "pageInfo": {"hasNextPage": true, "endCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMy0wMS0wNiAxMzowMzoxNy40OTY2NjEwMDAgKzA4MDAiLCJpZCI6IjMwMTYwMyJ9"}
      }
    }
  }
};

Map<String, dynamic> _ultimateLicenseResponse = {
  "data": {
    "project": {
      "group": {
        "iterations": {"__typename": "IterationConnection"}
      }
    }
  }
};

Map<String, dynamic> _freeLicenseResponse = {
  "errors": [
    {
      "message": "Field 'iterations' doesn't exist on type 'Group'",
      "locations": [
        {"line": 18, "column": 9}
      ],
      "path": ["query", "project", "group", "iterations"],
      "extensions": {"code": "undefinedField", "typeName": "Group", "fieldName": "iterations"}
    }
  ]
};
