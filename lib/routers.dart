import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/widgets/account/account_page.dart';
import 'package:jihu_gitlab_app/core/widgets/video_player_page.dart';
import 'package:jihu_gitlab_app/modules/community/community_post_creation_page.dart';
import 'package:jihu_gitlab_app/modules/community/community_post_details_page.dart';
import 'package:jihu_gitlab_app/modules/home/language_settings_page.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issue_selector.dart';
import 'package:jihu_gitlab_app/modules/iteration/details/iteration_page.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_request_page.dart';

Map<String, WidgetBuilder> routes = {
  AIPage.routeName: (context, {arguments}) => AIPage(arguments: arguments),
  LoginPage.routeName: (context, {arguments}) => LoginPage(arguments: arguments),
  SubgroupPage.routeName: (context, {arguments}) => SubgroupPage(arguments: arguments),
  ProjectPage.routeName: (context, {arguments}) => ProjectPage(arguments: arguments),
  DescriptionTemplateSelector.routeName: (context, {arguments}) => DescriptionTemplateSelector(arguments: arguments),
  SelfManagedLoginPage.routeName: (context, {arguments}) => const SelfManagedLoginPage(),
  PostSalesServicePage.routeName: (context, {arguments}) => const PostSalesServicePage(),
  PrivacyPolicyPage.routeName: (context, {arguments}) => const PrivacyPolicyPage(),
  IssueDetailsPage.routeName: (context, {arguments}) => IssueDetailsPage(arguments: arguments),
  LanguageSettingsPage.routeName: (context, {arguments}) => const LanguageSettingsPage(),
  VideoPlayerPage.routeName: (context, {arguments}) => VideoPlayerPage(arguments: arguments),
  IssueSelector.routeName: (context, {arguments}) => IssueSelector(arguments: arguments),
  MergeRequestPage.routeName: (context, {arguments}) => MergeRequestPage(arguments: arguments),
  AccountPage.routeName: (context, {arguments}) => const AccountPage(),
  CommunityPostCreationPage.routeName: (context, {arguments}) => CommunityPostCreationPage(arguments: arguments),
  CommunityPostDetailsPage.routeName: (context, {arguments}) => CommunityPostDetailsPage(arguments: arguments),
  ResourcesPage.routeName: (context, {arguments}) => const ResourcesPage(),
  IterationPage.routeName: (context, {arguments}) => IterationPage(arguments: arguments)
};

Route<dynamic>? Function(RouteSettings settings) onGenerateRoute = (RouteSettings settings) {
  debugPrint("Router settings.name: ${settings.name}");
  debugPrint("Router settings.arguments: ${settings.arguments.toString()}");
  String? name = settings.name;
  Function? widgetBuilder = routes[name];
  if (name == null || widgetBuilder == null) {
    return null;
  }
  return MaterialPageRoute(builder: (BuildContext context) => settings.arguments != null ? widgetBuilder(context, arguments: settings.arguments) : widgetBuilder(context));
};
