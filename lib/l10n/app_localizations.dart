import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/l10n/current_locale.dart';

const Locale zhLocale = Locale("zh");

class AppLocalizations {
  static S dictionary() {
    return S.current;
  }

  @visibleForTesting
  static void init() {
    S.load(LocaleProvider().value);
  }
}
