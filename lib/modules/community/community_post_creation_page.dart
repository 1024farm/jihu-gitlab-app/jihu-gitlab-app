import 'dart:async';

import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/loader.dart';
import 'package:jihu_gitlab_app/core/system_version_detector.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/connect_jihu_page.dart';
import 'package:jihu_gitlab_app/core/widgets/input_field_with_title.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_view.dart';
import 'package:jihu_gitlab_app/core/widgets/low_system_version_view.dart';
import 'package:jihu_gitlab_app/core/widgets/markdown/markdown_input_box.dart';
import 'package:jihu_gitlab_app/core/widgets/streaming_container.dart';
import 'package:jihu_gitlab_app/core/widgets/toast.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_post_creation_model.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_project.dart';
import 'package:jihu_gitlab_app/modules/community/request_access_page.dart';
import 'package:provider/provider.dart';

class CommunityPostCreationPage extends StatefulWidget {
  static const routeName = "CommunityPostCreationPage";

  const CommunityPostCreationPage({required this.arguments, super.key});

  final Map arguments;

  @override
  State<CommunityPostCreationPage> createState() => _CommunityPostCreationPageState();
}

class _CommunityPostCreationPageState extends State<CommunityPostCreationPage> {
  final CommunityPostCreationModel _model = CommunityPostCreationModel();
  final TextEditingController _issueTitleController = TextEditingController();
  final TextEditingController _issueDescriptionController = TextEditingController();
  late Timer _timer;

  @override
  void initState() {
    super.initState();
    SystemVersionDetector.instance().detect().then((value) => setState(() => _model.systemType = value));
    _model.init(widget.arguments['projectId'], widget.arguments['fullPath']);
    _timer = Timer.periodic(const Duration(milliseconds: 5000), (timer) {
      if (ConnectionProvider().communityConnection.authorized) _model.saveAsDraft(_issueTitleController.text, _issueDescriptionController.text);
    });
    if (ConnectionProvider().communityConnection.authorized) {
      _loadDraftData();
    }
    _model.checkUserProjectPermission().then((_) => setState(() {}));
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ConnectionProvider>(builder: (context, _, child) {
      if (!ConnectionProvider().communityConnection.authorized) {
        return Scaffold(appBar: CommonAppBar(showLeading: true), body: const ConnectJiHuPage());
      }
      if (_model.permissionCheckingState == LoadState.loadingState) return Scaffold(appBar: CommonAppBar(showLeading: true), body: const LoadingView());
      if (_model.permissionCheckingState == LoadState.successState && _model.requestAccessEnabled) {
        return Scaffold(
            appBar: CommonAppBar(
              showLeading: true,
              title: _model.systemType == SystemType.unmaintainable ? const Text("") : Text(AppLocalizations.dictionary().requestAccess),
            ),
            body: RequestAccessPage(projectId: _model.projectId));
      }
      if (_model.systemType == SystemType.unmaintainable) {
        return Scaffold(appBar: CommonAppBar(showLeading: true), body: const LowSystemVersionView(systemType: SystemType.unmaintainable, isForGeek: false));
      }
      return GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Scaffold(
            appBar: CommonAppBar(
              title: Text(AppLocalizations.dictionary().createIssue(AppLocalizations.dictionary().post)),
              showLeading: true,
              leading: BackButton(
                  color: Colors.black,
                  onPressed: () {
                    Navigator.pop(context);
                  }),
              actions: [
                Container(
                  transform: Matrix4.translationValues(-12, 2, 0),
                  child: TextButton(
                      onPressed: _model.submittable
                          ? () {
                              var title = _issueTitleController.text;
                              var description = _issueDescriptionController.text;
                              Loader.showProgress(context);
                              _model.create(widget.arguments['projectId'], title, description: description, Localizations.localeOf(context)).then((value) {
                                Loader.hideProgress(context);
                                if (value != null) {
                                  Toast.success(context, AppLocalizations.dictionary().createSuccessful);
                                  Navigator.pop(context, value);
                                } else {
                                  Toast.error(context, AppLocalizations.dictionary().createFailed);
                                }
                              });
                            }
                          : null,
                      child: Text(AppLocalizations.dictionary().create, style: TextStyle(color: Theme.of(context).primaryColor.withOpacity(_model.submittable ? 1 : 0.5)))),
                ),
              ],
            ),
            body: SingleChildScrollView(
                physics: const ScrollPhysics(),
                child: SafeArea(
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                    InputFieldWithTitle(
                      title: AppLocalizations.dictionary().createIssueTitle,
                      placeHolder: AppLocalizations.dictionary().issueTitlePlaceholder(AppLocalizations.dictionary().post),
                      controller: _issueTitleController,
                      onChanged: (text) {
                        _model.changeTitle(text);
                        setState(() {});
                      },
                    ),
                    _buildTitle(AppLocalizations.dictionary().faqType),
                    _buildFaqTypeSelector(),
                    _buildTitle(AppLocalizations.dictionary().description),
                    Container(
                        margin: const EdgeInsets.symmetric(horizontal: 16),
                        child: MarkdownInputBox(
                          projectId: widget.arguments['projectId'],
                          controller: _issueDescriptionController,
                          issueSelectable: false,
                          memberSelectable: false,
                          placeholder: AppLocalizations.dictionary().issueDescriptionPlaceholder,
                          onChanged: () {},
                          videoSelectable: false,
                        )),
                  ]),
                )),
          ));
    });
  }

  Padding _buildTitle(String title, {EdgeInsetsGeometry padding = const EdgeInsets.all(16.0)}) {
    return Padding(
      padding: padding,
      child: Text(title, style: const TextStyle(color: Color(0XFF03162F), fontSize: 16, fontWeight: FontWeight.w600)),
    );
  }

  Future<void> _loadDraftData() async {
    var loadDraft = await _model.loadDraft();
    if (loadDraft.isNotEmpty) {
      _issueTitleController.text = loadDraft[0].title!;
      _model.changeTitle(loadDraft[0].title!);
      _issueDescriptionController.text = loadDraft[0].description!;
      // _model
      var labels = loadDraft[0].labels;
      if (labels!.isNotEmpty) {
        var type = CommunityType.fromJson(labels.first.toJson());
        _model.selectFaqType(type);
      }
      setState(() {});
    }
  }

  Widget _buildFaqTypeSelector() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: ValueListenableBuilder<List<CommunityType>>(
        valueListenable: _model.notifier,
        builder: (context, data, child) => StreamingContainer(children: data.map((e) => _buildFaqTypeRadio(e)).toList()),
      ),
    );
  }

  Widget _buildFaqTypeRadio(CommunityType faqType) {
    return InkWell(
      onTap: () => setState(() => _model.selectFaqType(faqType)),
      child: Stack(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
            decoration: BoxDecoration(color: _model.isSelected(faqType) ? const Color(0xFFFFEBE2) : Colors.white, borderRadius: const BorderRadius.all(Radius.circular(4))),
            child: Text(
              faqType.typeName(Localizations.localeOf(context)),
              style: TextStyle(color: _model.isSelected(faqType) ? AppThemeData.primaryColor : const Color(0xFF03162F), fontSize: 13, fontWeight: FontWeight.w400),
            ),
          )
        ],
      ),
    );
  }
}
