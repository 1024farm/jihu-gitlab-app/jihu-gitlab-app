import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/layout/adaptive.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/widgets/app_footer.dart';
import 'package:jihu_gitlab_app/core/widgets/app_header.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar_and_name.dart';
import 'package:jihu_gitlab_app/core/widgets/http_fail_view.dart';
import 'package:jihu_gitlab_app/core/widgets/label_view.dart';
import 'package:jihu_gitlab_app/core/widgets/search_box.dart';
import 'package:jihu_gitlab_app/core/widgets/search_no_result_view.dart';
import 'package:jihu_gitlab_app/core/widgets/search_result.dart';
import 'package:jihu_gitlab_app/core/widgets/streaming_container.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/ai/ai_page.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_post.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_post_list_model.dart';

class CommunityPostListView extends StatefulWidget {
  final String? labelName;
  final OnCommunityPostSelected? onFaqSelected;

  const CommunityPostListView({this.labelName, this.onFaqSelected, Key? key}) : super(key: key);

  @override
  State<CommunityPostListView> createState() => CommunityPostListViewState();
}

class CommunityPostListViewState extends State<CommunityPostListView> with AutomaticKeepAliveClientMixin {
  final CommunityPostListModel _model = CommunityPostListModel();

  @override
  void initState() {
    _model.init(widget.labelName, widget.onFaqSelected);
    super.initState();
    Future.delayed(Duration.zero, () {
      _model.refreshController.callRefresh(force: true);
    });
  }

  Future<void> onRefresh() {
    return _model.refresh(Localizations.localeOf(context));
  }

  Future<void> onLoadMore() {
    return _model.loadMore(Localizations.localeOf(context));
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Column(
      children: [ValueListenableBuilder<LoadState>(valueListenable: _model.stateNotifier, builder: (context, state, child) => _buildListBody(state)), _buildSearchBox()],
    );
  }

  Widget _buildFaqItem(int index, CommunityPost faq) {
    String avatar = faq.author.avatarUrl;
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        InkWell(
          onTap: () {
            if (isDesktopLayout(context)) {
              _model.selectPost(index, faq);
            }
            widget.onFaqSelected?.call(faq);
          },
          child: ValueListenableBuilder<int?>(
            valueListenable: _model.selectedIndexNotifier,
            builder: (context, selectedIndex, child) {
              Color? itemBackgroundColor = isDesktopLayout(context) && index == selectedIndex ? Theme.of(context).colorScheme.primary : null;
              return Container(
                  padding: const EdgeInsets.all(12),
                  margin: const EdgeInsets.fromLTRB(12, 12, 12, 0),
                  constraints: const BoxConstraints(minHeight: 50),
                  decoration:
                      BoxDecoration(color: Colors.white, borderRadius: const BorderRadius.all(Radius.circular(4)), border: itemBackgroundColor != null ? Border.all(color: itemBackgroundColor) : null),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                        AvatarAndName(
                          avatarUrl: avatar.isNotEmpty ? Api.communityHost + avatar : '',
                          username: faq.author.name,
                          avatarSize: 20,
                          gap: 4,
                          nameStyle: const TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Color(0xFF03162F)),
                        ),
                        const SizedBox(width: 8),
                        Text("·${faq.createdAt.value}", style: const TextStyle(color: Color(0xFF87878C), fontSize: 12, fontWeight: FontWeight.w400))
                      ]),
                      const SizedBox(height: 8),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [SearchResult(keyword: _model.searchController.text, text: faq.title)],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8),
                        child: StreamingContainer(
                          spacing: 4,
                          children: faq.listLabels.map((label) => LabelView(labelName: label.displayName(Localizations.localeOf(context)), textColor: label.textColor, color: label.color)).toList(),
                        ),
                      )
                    ],
                  ));
            },
          ),
        )
      ],
    );
  }

  Widget _buildSearchBox() {
    return Row(
      children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12),
            child: SearchBox(
              searchController: _model.searchController,
              onSubmitted: (text) {
                _model.submitSearch(text);
                onRefresh();
              },
              onClear: () {
                _model.clearSearch();
                onRefresh();
              },
            ),
          ),
        ),
        InkWell(
          onTap: () => Navigator.of(context).pushNamed(AIPage.routeName, arguments: {'search': _model.searchController.text}),
          child: Container(
            width: 42,
            height: 42,
            padding: const EdgeInsets.only(right: 12),
            child: ClipOval(child: Image.asset("assets/images/jixiaohu.png", height: 32, width: 32)),
          ),
        ),
      ],
    );
  }

  Widget _buildListBody(LoadState state) {
    if (state == LoadState.errorState) return Expanded(child: HttpFailView(onRefresh: onRefresh));
    if (_model.noSearchingResult) {
      return Expanded(child: SearchNoResultView(message: '${AppLocalizations.dictionary().noMatch}\'${_model.search}\'', onRefresh: onRefresh));
    }
    if (_model.isDataEmpty) return Expanded(child: TipsView(onRefresh: onRefresh));
    return Expanded(
      child: EasyRefresh(
          controller: _model.refreshController,
          onRefresh: onRefresh,
          onLoad: onLoadMore,
          header: AppHeader(),
          footer: AppFooter(),
          child: ValueListenableBuilder(
              valueListenable: _model.dataNotifier, builder: (context, data, child) => ListView.builder(itemBuilder: (context, index) => _buildFaqItem(index, data[index]), itemCount: data.length))),
    );
  }

  @override
  void dispose() {
    _model.dispose();
    super.dispose();
  }

  void clearSelection() {
    setState(() => _model.clearSelected(callback: false));
  }

  @override
  bool get wantKeepAlive => true;
}
