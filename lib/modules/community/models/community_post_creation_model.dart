import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/domain/global_time.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/system_version_detector.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_project.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/models/issue_draft_entity.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/models/issue_draft_repository.dart';

class CommunityPostCreationModel {
  late int _projectId;
  late String _fullPath;
  late ValueNotifier<List<CommunityType>> _notifier;

  LoadState _permissionCheckingState = LoadState.loadingState;

  final IssueDraftRepository _repository = IssueDraftRepository();
  final List<CommunityType> _faqTypes = [];

  bool _requestAccessEnabled = false;
  CommunityType? _selectedFaqType;
  String? _title;
  SystemType systemType = SystemType.latest;

  void init(int projectId, String fullPath) {
    _projectId = projectId;
    _fullPath = fullPath;
    _faqTypes.addAll(CommunityProject.instance().types);
    _notifier = ValueNotifier(_faqTypes);
  }

  Future<void> checkUserProjectPermission() async {
    try {
      var userInfo = ConnectionProvider().communityConnection.userInfo;
      List<User> results = await searchProjectMembers(userInfo.username);
      var index = results.indexWhere((element) => element.id == userInfo.id);
      _requestAccessEnabled = index < 0;
      _permissionCheckingState = LoadState.successState;
    } catch (error) {
      LogHelper.err("Check User Project Permission failed", error);
      _permissionCheckingState = LoadState.errorState;
    }
  }

  Future<List<User>> searchProjectMembers(String search) async {
    try {
      bool hasNextPage = true;
      String endCursor = '';
      List<User> members = [];
      do {
        var resp = await HttpClient.instance().postWithConnection(Api.graphql(), getProjectMembersGraphQLRequestBody(_fullPath, search, endCursor), ConnectionProvider().communityConnection);
        var projectMembersData = resp.body()['data']?['project']?['projectMembers'];
        hasNextPage = projectMembersData?['pageInfo']?['hasNextPage'] ?? false;
        endCursor = projectMembersData?['pageInfo']?['endCursor'] ?? '';
        List<dynamic> list = projectMembersData?['nodes'] ?? [];
        members.addAll(list.map((e) => User.fromGraphQLJson(e['user'] ?? {})).toList());
      } while (hasNextPage);
      return members;
    } catch (e) {
      LogHelper.err('Search project members for "$search" error', e);
      return [];
    }
  }

  Future<CommunityType?> create(int projectId, String title, Locale locale, {String? description, String? labels}) async {
    try {
      String uri = Api.join('/projects/$projectId/issues');
      var body = <String, dynamic>{'title': title};
      if (description != null) {
        body['description'] = description;
      }
      body['labels'] = [_selectedFaqType!.name, locale == const Locale("zh") ? "lang::zh" : "lang::en"].join(",");
      await HttpClient.instance().postWithConnection<Map<String, dynamic>>(uri, body, ConnectionProvider().communityConnection);
      _repository.delete(projectId, ConnectionProvider().communityConnection.userInfo.id);
      CommunityType type = _selectedFaqType!;
      _reset();
      return type;
    } catch (error) {
      LogHelper.err("Create issue failed", error);
      return null;
    }
  }

  void saveAsDraft(String title, String description) async {
    var userId = ConnectionProvider().communityConnection.userInfo.id;
    var faqDraft = {
      "user_id": userId,
      "title": title,
      "description": description,
    };
    if (_selectedFaqType != null) {
      faqDraft['selected_labels'] = [
        Label.fromJson({'id': _selectedFaqType!.id.id, 'name': _selectedFaqType!.name, 'description': _selectedFaqType!.description})
      ];
    }

    int version = GlobalTime.now().millisecondsSinceEpoch;
    var faqDraftEntity = IssueDraftEntity.create(faqDraft, projectId, version);
    List<IssueDraftEntity> issueDrafts = await _repository.query(projectId, userId);
    if (issueDrafts.isNotEmpty) {
      _repository.update(faqDraftEntity);
    } else {
      _repository.insert(faqDraftEntity);
    }
  }

  Future<List<IssueDraftEntity>> loadDraft() async {
    List<IssueDraftEntity> issueDrafts = await _repository.query(projectId, ConnectionProvider().communityConnection.userInfo.id);
    return issueDrafts;
  }

  void selectFaqType(CommunityType faqType) {
    _selectedFaqType = faqType;
  }

  bool isSelected(CommunityType faqType) {
    return _selectedFaqType == faqType;
  }

  void _reset() {
    _selectedFaqType = null;
    _title = null;
  }

  void changeTitle(String text) => _title = text;

  int get projectId => _projectId;

  ValueNotifier<List<CommunityType>> get notifier => _notifier;

  bool get submittable => _selectedFaqType != null && _title != null && _title!.isNotEmpty;

  bool get requestAccessEnabled => _requestAccessEnabled;

  LoadState get permissionCheckingState => _permissionCheckingState;
}
