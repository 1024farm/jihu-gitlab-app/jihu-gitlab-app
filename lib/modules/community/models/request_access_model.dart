import 'package:dio/dio.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/global_keys.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/system_version_detector.dart';
import 'package:jihu_gitlab_app/core/widgets/toast.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class RequestAccessModel {
  late int _faqProjectId;
  SystemType systemType = SystemType.latest;

  bool _requestInProgress = false;

  Future<void> init(int projectId) async {
    _faqProjectId = projectId;
  }

  Future<bool> requestAccess() async {
    _requestInProgress = true;
    try {
      await HttpClient.instance().postWithConnection(Api.join("projects/$_faqProjectId/access_requests"), {}, ConnectionProvider().communityConnection);
      _requestInProgress = false;
      return true;
    } on DioError catch (error) {
      _requestInProgress = false;
      if (error.response != null && error.response!.statusCode == 400) return true;
      LogHelper.err("Check User Project Permission failed", error);
      Toast.error(GlobalKeys.rootScaffoldKey.currentContext!, AppLocalizations.dictionary().failedToRequest);
      return false;
    }
  }

  bool get requestInProgress => _requestInProgress;
}
