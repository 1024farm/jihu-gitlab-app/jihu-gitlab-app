import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/id.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label.dart';

class CommunityProject {
  static const int id = 89335;
  static const String relativePath = 'ultimate-plan/jihu-gitlab-app/faq';
  static const String typeLabelPrefix = 'type';
  static const String languageLabelPrefix = 'lang';
  static const String enLabelName = "lang::en";
  static const String zhLabelName = "lang::zh";
  static const String geekLabelName = "for::geek";

  static final CommunityProject _instance = CommunityProject._internal();

  CommunityProject._internal();

  factory CommunityProject.instance() => _instance;

  List<CommunityType> _types = [];
  bool _inMaintenance = false;

  Future<bool> refresh() async {
    try {
      var response = await HttpClient.instance().postWithConnection(Api.graphql(), getCommunityTypesGraphQLRequestBody(relativePath), ConnectionProvider().communityConnection);
      var project = response.body()['data']?['project'];
      _types =
          ((project?['labels']?['nodes'] ?? []) as List).map((e) => CommunityType.fromGraphqlJson(e)).where((e) => e.name.isNotEmpty).where((e) => e.name.startsWith(typeLabelPrefix)).toSet().toList();
      _inMaintenance = project?['archived'] ?? false;
      return true;
    } catch (error) {
      LogHelper.err('FaqModel fetch labels error', error);
      return false;
    }
  }

  bool get inMaintenance => _inMaintenance;

  List<CommunityType> get types => _types;
}

class CommunityType extends Label {
  static CommunityType all = CommunityType(Id.fromGid("/0"), "All", "所有");

  CommunityType(Id id, String name, String description) : super(id, name, description, "#FFFFFF", "#FFFFFF", isProjectLabel: true);

  factory CommunityType.fromGraphqlJson(Map<String, dynamic> json) {
    return CommunityType(Id.fromGid(json['id']), json['title'], json['description'] ?? "");
  }

  factory CommunityType.fromJson(Map<String, dynamic> json) {
    return CommunityType(Id.from(json['id']), json['name'], json['description'] ?? "");
  }

  String typeName(Locale locale) {
    if (locale == zhLocale && description.isNotEmpty) {
      return description;
    }
    return name.contains(":") ? name.replaceAll("::", ":").split(":")[1] : name;
  }

  bool get isAll => name == all.name;

  @override
  bool operator ==(Object other) => identical(this, other) || other is CommunityType && id == other.id && name == other.name && description == other.description;

  @override
  int get hashCode => id.hashCode;
}
