import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/global_keys.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/modules/community/community_post_list_view.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_post.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_project.dart';
import 'package:jihu_gitlab_app/modules/root/model/root_store.dart';
import 'package:provider/provider.dart';

class CommunityModel {
  static const int _communityPageIndex = 2;
  late ValueNotifier<List<CommunityType>> _notifier;
  late TabController _tabController;
  final List<CommunityType> _communityTypes = [CommunityType.all];
  final Map<CommunityType, GlobalKey<CommunityPostListViewState>> _globalKeys = {};
  LoadState _fetchTypesState = LoadState.loadingState;
  CommunityPost? _selectedCommunityPost;
  String _selectedCommunityPostTypeName = '';
  int currentIndex = 0;

  void init(TickerProvider tickerProvider) {
    _notifier = ValueNotifier(_communityTypes);
    _tabController = TabController(length: _communityTypes.length, vsync: tickerProvider);
    RootStore().addListener(() {
      int index = Provider.of<RootStore>(GlobalKeys.rootAppKey.currentContext!, listen: false).selectedIndex;
      if (index == _communityPageIndex) refresh(tickerProvider);
    });
  }

  Future<void> refresh(TickerProvider tickerProvider) async {
    try {
      bool state = await CommunityProject.instance().refresh();
      if (!state) throw Exception('Refresh project info failed');
      _communityTypes.clear();
      _communityTypes.add(CommunityType.all);
      var types = CommunityProject.instance().types;
      _communityTypes.addAll(CommunityProject.instance().types);
      for (var e in types) {
        _globalKeys[e] = GlobalKey<CommunityPostListViewState>();
      }
      _tabController = TabController(length: _communityTypes.length, vsync: tickerProvider);
      _notifier.value = List.of(_communityTypes);
      _fetchTypesState = LoadState.successState;
    } catch (error) {
      LogHelper.err('FaqModel fetch labels error', error);
      _fetchTypesState = LoadState.errorState;
    }
  }

  void changeSelectedCommunityPost(CommunityPost? post, Locale locale) {
    _selectedCommunityPost = post;
    _selectedCommunityPostTypeName = '';
    if (post == null) return;
    var labels = post.labels;
    CommunityType type = labels.map((e) => CommunityType.fromJson(e.toJson())).where((e) => e.name.isNotEmpty).where((e) => e.name.startsWith(CommunityProject.typeLabelPrefix)).toSet().toList().first;
    _selectedCommunityPostTypeName = type.typeName(locale);
  }

  void clearSelectedCommunityPost() {
    _selectedCommunityPost = null;
    _selectedCommunityPostTypeName = '';
    _globalKeys[_communityTypes[currentIndex]]?.currentState?.clearSelection();
  }

  TabController get tabController => _tabController;

  ValueNotifier<List<CommunityType>> get notifier => _notifier;

  LoadState get fetchTypesState => _fetchTypesState;

  GlobalKey<CommunityPostListViewState> globalKey(CommunityType faqType) {
    return _globalKeys.putIfAbsent(faqType, () => GlobalKey<CommunityPostListViewState>());
  }

  Map<CommunityType, GlobalKey<CommunityPostListViewState>> get globalKeys => _globalKeys;

  Map<String, dynamic> get selectedCommunityPostParams =>
      _selectedCommunityPost == null ? {} : {'issueId': _selectedCommunityPost!.id.id, 'issueIid': _selectedCommunityPost!.iid, 'communityTypeName': _selectedCommunityPostTypeName};
}
