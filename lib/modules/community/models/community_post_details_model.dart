import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/widgets/selector/data_provider.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_post.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_project.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion_provider.dart';

class CommunityPostDetailsModel {
  CommunityPost? _communityPostInfo;

  LoadState _loadState = LoadState.loadingState;
  final List<Discussion> _comments = [];
  DataProvider<Discussion>? _dataProvider;

  int? selectedNoteId;
  String selectedDiscussionId = "";
  bool isInternal = false;

  late int _communityProjectId;
  late String _fullPath;
  late ValueNotifier<List<Discussion>> _notifier;

  late int _postId;
  late int _postIid;

  void config(int postId, int postIid) {
    _postId = postId;
    _postIid = postIid;
    _communityProjectId = CommunityProject.id;
    _fullPath = CommunityProject.relativePath;
    _dataProvider = DiscussionProvider(
      issueId: _postId,
      issueIid: _postIid,
      projectId: _communityProjectId,
      pathWithNamespace: _fullPath,
      isFaqDiscussion: true,
    );
    _comments.clear();
    _notifier = ValueNotifier(_comments);
  }

  Future<void> loadDiscussions() async {
    _dataProvider?.syncFromRemote().then((value) {
      _loadLocalDataAndNotify();
    });
  }

  Future<void> _loadLocalDataAndNotify() async {
    List<Discussion> data = await _dataProvider!.loadFromLocal();
    _comments.clear();
    _comments.addAll(data);
    _notifier.value = data;
    _loadState = LoadState.successState;
  }

  Future getPostInfo() async {
    try {
      var response = await HttpClient.instance().postWithConnection(Api.graphql(), getCommunityDetailsGraphQLRequestBody(_fullPath, _postIid), ConnectionProvider().communityConnection);
      var errors = response.body()["errors"];
      if (errors != null) throw Exception(errors);
      var communityInfo = response.body()?['data']?['project']?['issue'] ?? {};
      _communityPostInfo = CommunityPost.fromCommunityGraphQLJson(communityInfo, _fullPath);
      _loadState = LoadState.successState;
    } catch (e) {
      LogHelper.err('CommunityPostDetailsModel getIssueInfo error', e);
      if (e.runtimeType == DioError && (e as DioError).response?.statusCode == 404) {
        _loadState = LoadState.noItemState;
      } else {
        _loadState = LoadState.errorState;
      }
    }
  }

  Future _saveComment(String comments) async {
    Map<String, dynamic> data = {};
    data['body'] = comments;
    data['confidential'] = isInternal;
    data['internal'] = isInternal;
    try {
      await HttpClient.instance().post<dynamic>(Api.join("projects/$_communityProjectId/issues/$_postIid/notes"), data);
    } catch (e) {
      LogHelper.err('CommunityPostDetailsModel _saveComment error', e);
      _loadState = LoadState.errorState;
    }
  }

  Future _saveReplyComment(int noteId, String comments) async {
    try {
      await HttpClient.instance().post<dynamic>("/api/v4/projects/$_communityProjectId/issues/$_postIid/discussions/$selectedDiscussionId/notes", {'body': comments, 'note_id': noteId});
    } catch (e) {
      LogHelper.err('CommunityPostDetailsModel _saveReplyComment error', e);
      _loadState = LoadState.errorState;
    }
  }

  Future<void> saveComment(String commentContent) async {
    if (selectedNoteId != null) {
      await _saveReplyComment(selectedNoteId!, commentContent);
    } else {
      await _saveComment(commentContent);
    }
  }

  void resetForReplyToComment() {
    selectedNoteId = null;
    isInternal = false;
  }

  void loadingPage() {
    _loadState = LoadState.loadingState;
  }

  CommunityPost? get communityPostInfo => _communityPostInfo;

  int get projectId => _communityProjectId;

  LoadState get loadState => _loadState;

  List<Discussion> get comments => _comments;

  ValueNotifier<List<Discussion>> get notifier => _notifier;

  bool get shouldShowInternalSwitch => selectedNoteId == null;

  bool get isForGeek => (_communityPostInfo?.labels ?? []).any((element) => element.name == CommunityProject.geekLabelName);

  int get postId => _postId;

  String get fullPath => _fullPath;

  int get postIid => _postIid;
}
