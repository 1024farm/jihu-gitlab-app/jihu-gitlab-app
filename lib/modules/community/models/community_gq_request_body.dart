import 'dart:convert';

Map<String, String> getCommunityTypesGraphQLRequestBody(String fullPath) {
  return {
    "query": """
       {
          project(fullPath:"$fullPath"){
            archived
            labels{
              nodes{
                id
                title
                description
              }
            }
          }
       } 
    """
  };
}

Map<String, String> getCommunityPostListGraphQLRequestBody(String fullPath, List<String> labelNames, String afterCursor, String search, int pageSize) {
  return {
    "query": """
       {
        project(fullPath:"$fullPath"){
          issues(labelName: ${jsonEncode(labelNames)}, first:$pageSize, after:"$afterCursor", search: "$search", state: opened){
            nodes{
              id
              iid
              title
              webUrl
              confidential
              state
              createdAt
              author{
                name
                avatarUrl
              }
              labels {
                nodes {
                  id
                  title
                  description
                  color
                  textColor
                }
              }
            }
            pageInfo{
              hasNextPage
              endCursor
            }
          }
        }
       }
      """
  };
}

Map<String, String> getProjectMembersGraphQLRequestBody(String fullPath, String search, String endCursor) {
  return {
    "query": """
     {
        project(fullPath: "$fullPath") {
          projectMembers(search: "$search", first: 50, after: "$endCursor") {
            pageInfo {
              hasNextPage
              endCursor
            }
            nodes {
              id
              accessLevel {
                integerValue
                stringValue
              }
              user {
                ...UserCoreFragment
              }
            }
          }
        }
     }
     fragment UserCoreFragment on UserCore {
        id
        username
        name
        state
        avatarUrl
        webUrl
        publicEmail
      }
    """
  };
}

Map<String, dynamic> getCommunityDetailsGraphQLRequestBody(String fullPath, int issueIid) {
  return {
    "query": """
       {
          project(fullPath: "$fullPath") {
            id
            name
            nameWithNamespace
            path
            fullPath
            issue(iid: "$issueIid") {
              id
              iid
              projectId
              title
              description
              webUrl
              confidential
              state
              createdAt
              author {
                id
                avatarUrl
                name
                username
              }
              assignees {
                nodes {
                  id
                  avatarUrl
                  name
                  username
                }
              }
              userPermissions {
                updateIssue
              }
              labels {
                nodes {
                  id
                  title
                  description
                  color
                  textColor
                }
              }
            }
          }
       }
       """
  };
}

Map<String, dynamic> getCommunityPostVotesGraphQLRequestBody(String fullPath, int postIid) {
  return {
    "query": """
           {
              project(fullPath: "$fullPath") {
                issue(iid: "$postIid") {
                  downvotes
                  upvotes
                }
              }
           }
       """
  };
}
