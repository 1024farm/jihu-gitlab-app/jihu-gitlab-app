import 'package:jihu_gitlab_app/core/domain/assignee/assignee.dart';
import 'package:jihu_gitlab_app/core/id.dart';
import 'package:jihu_gitlab_app/core/time.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_project.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/description_content.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label.dart';

class CommunityPost {
  Id id;
  int iid;
  String title;
  String description;
  Time createdAt;
  bool confidential;
  String state;
  String webUrl;
  Assignee author;
  List<String>? labelNames;
  List<Label> labels;
  bool hasEditPermission;
  int upVotes;
  int downVotes;

  CommunityPost._(this.id, this.iid, this.title, this.description, this.createdAt, this.confidential, this.state, this.webUrl, this.author, this.labelNames, this.labels, this.hasEditPermission,
      this.upVotes, this.downVotes);

  factory CommunityPost.fromGraphQLJson(Map<String, dynamic> json) {
    return CommunityPost._(
      Id.fromGid(json['id'] ?? '/0'),
      int.parse(json['iid']),
      json['title'] ?? '',
      '',
      Time.init(json['createdAt'] ?? ''),
      json['confidential'] ?? false,
      json['state'] ?? '',
      json['webUrl'] ?? '',
      Assignee.fromGraphQLJson(json['author'] ?? {}),
      [],
      ((json['labels']?['nodes'] ?? []) as List).map((e) => Label.fromGraphQLJson(e)).toList(),
      json['userPermissions']?['updateIssue'] ?? false,
      json['upvotes'] ?? 0,
      json['downvotes'] ?? 0,
    );
  }

  factory CommunityPost.fromCommunityGraphQLJson(Map<String, dynamic> json, String pathWithNameSpace) {
    String description = DescriptionContent.init(json['description'] ?? '', pathWithNameSpace).value;
    return CommunityPost._(
      Id.fromGid(json['id'] ?? '/0'),
      int.parse(json['iid'] ?? '0'),
      json['title'] ?? '',
      description,
      Time.init(json['createdAt'] ?? ''),
      json['confidential'] ?? false,
      json['state'] ?? '',
      json['webUrl'] ?? '',
      Assignee.fromGraphQLJson(json['author'] ?? {}),
      [],
      ((json['labels']?['nodes'] ?? []) as List).map((e) => Label.fromGraphQLJson(e)).toList(),
      json['userPermissions']?['updateIssue'] ?? false,
      json['upvotes'] ?? 0,
      json['downvotes'] ?? 0,
    );
  }

  List<Label> get listLabels {
    return labels.where((e) => !e.name.startsWith(CommunityProject.typeLabelPrefix) && !e.name.startsWith(CommunityProject.languageLabelPrefix)).toList();
  }
}
