import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/graphql_param.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/l10n/current_locale.dart';
import 'package:jihu_gitlab_app/main.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_post.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_project.dart';
import 'package:provider/provider.dart';

typedef OnCommunityPostSelected = void Function(CommunityPost? post);

class CommunityPostListModel {
  static const _pageSize = 20;
  final TextEditingController _searchController = TextEditingController();
  final EasyRefreshController _refreshController = EasyRefreshController(controlFinishLoad: true, controlFinishRefresh: true);
  List<CommunityPost> _posts = [];
  String _afterCursor = "";
  bool _hasNextPage = true;
  LoadState _loadState = LoadState.successState;
  String _search = '';
  late ValueNotifier<LoadState> _stateNotifier;
  late ValueNotifier<List<CommunityPost>> _dataNotifier;
  late ValueNotifier<int?> _selectedIndexNotifier;
  late VoidCallback listener;
  String? _labelName;
  int? _selectedIndex;
  CommunityPost? _selectedPost;
  OnCommunityPostSelected? _onPostSelected;

  void init(String? labelName, OnCommunityPostSelected? onPostSelected) {
    _labelName = labelName;
    _onPostSelected = onPostSelected;
    _stateNotifier = ValueNotifier(_loadState);
    _dataNotifier = ValueNotifier(_posts);
    _selectedIndexNotifier = ValueNotifier(_selectedIndex);
    var currentContext = navigatorKey.currentContext;
    if (currentContext != null) {
      listener = () {
        clearSearch();
        refresh(Provider.of<LocaleProvider>(currentContext, listen: false).value);
      };
      Provider.of<LocaleProvider>(currentContext, listen: false).addListener(listener);
    }
  }

  Future<bool> refresh(Locale locale) async {
    try {
      _hasNextPage = true;
      _afterCursor = "";
      _posts = await _getCommunityPostList(locale);
      _loadState = _posts.isEmpty ? LoadState.noItemState : LoadState.successState;
      _refreshController.finishRefresh();
      _refreshController.resetFooter();
      return Future.value(true);
    } catch (e) {
      _loadState = LoadState.errorState;
      _refreshController.finishRefresh(IndicatorResult.fail);
      return Future.value(false);
    } finally {
      notifyDataChanged();
      clearSelected();
    }
  }

  Future<bool> loadMore(Locale locale) async {
    try {
      List<CommunityPost> list = await _getCommunityPostList(locale);
      _posts.addAll(list);
      _refreshController.finishLoad(_hasNextPage ? IndicatorResult.success : IndicatorResult.noMore);
      return Future.value(true);
    } catch (e) {
      _refreshController.finishLoad(IndicatorResult.fail);
      return Future.value(false);
    } finally {
      notifyDataChanged();
    }
  }

  void notifyDataChanged() {
    _stateNotifier.value = _loadState;
    _dataNotifier.value = _posts;
  }

  Future<List<CommunityPost>> _getCommunityPostList(Locale locale) async {
    List<String> labelNames = [locale.languageCode == "zh" ? CommunityProject.zhLabelName : CommunityProject.enLabelName];
    if (_labelName != null) labelNames.add(_labelName!);
    _search = GraphqlParam(_search).get();
    var response = await HttpClient.instance().postWithConnection(
        Api.graphql(), getCommunityPostListGraphQLRequestBody(CommunityProject.relativePath, labelNames, _afterCursor, _search, _pageSize), ConnectionProvider().communityConnection);
    var errors = response.body()["errors"];
    if (errors != null) throw Exception(errors);
    var nodes = response.body()['data']?['project']?['issues']?['nodes'] ?? [];
    var pageInfo = response.body()['data']?['project']?['issues']?['pageInfo'] ?? {};
    _hasNextPage = pageInfo['hasNextPage'] ?? false;
    _afterCursor = pageInfo['endCursor'] ?? "";
    return Future(() => nodes.map<CommunityPost>((e) => CommunityPost.fromGraphQLJson(e)).toList());
  }

  void selectPost(int? index, CommunityPost? selectedPost) {
    _selectedIndex = index;
    _selectedPost = selectedPost;
    _selectedIndexNotifier.value = _selectedIndex;
    _onPostSelected?.call(_selectedPost);
  }

  void clearSelected({bool callback = true}) {
    _selectedIndex = null;
    _selectedPost = null;
    _selectedIndexNotifier.value = _selectedIndex;
    if (callback) {
      _onPostSelected?.call(null);
    }
  }

  void submitSearch(String? search) {
    _search = search ?? '';
  }

  void clearSearch() {
    _search = '';
    _searchController.clear();
  }

  void dispose() {
    _refreshController.dispose();
    _searchController.dispose();
  }

  TextEditingController get searchController => _searchController;

  EasyRefreshController get refreshController => _refreshController;

  bool get isDataEmpty => _loadState == LoadState.noItemState;

  bool get noSearchingResult => isDataEmpty && _search.isNotEmpty;

  String get search => _search;

  ValueNotifier<LoadState> get stateNotifier => _stateNotifier;

  ValueNotifier<List<CommunityPost>> get dataNotifier => _dataNotifier;

  ValueNotifier<int?> get selectedIndexNotifier => _selectedIndexNotifier;
}
