import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/layout/adaptive.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/app_tab_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/home/home_scaffold.dart';
import 'package:jihu_gitlab_app/core/widgets/http_fail_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/community/community_maintenance_notice_view.dart';
import 'package:jihu_gitlab_app/modules/community/community_post_creation_page.dart';
import 'package:jihu_gitlab_app/modules/community/community_post_details_page.dart';
import 'package:jihu_gitlab_app/modules/community/community_post_list_view.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_model.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_post.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_project.dart';
import 'package:provider/provider.dart';

class CommunityPage extends StatefulWidget {
  const CommunityPage({super.key});

  @override
  State<CommunityPage> createState() => CommunityPageState();
}

class CommunityPageState extends State<CommunityPage> with TickerProviderStateMixin {
  final CommunityModel _model = CommunityModel();

  @override
  void initState() {
    _model.init(this);
    onRefresh();
    super.initState();
  }

  Future<void> onRefresh() async {
    await _model.refresh(this);
    _model.tabController.addListener(() {
      if (_model.tabController.index.toDouble() == _model.tabController.animation?.value) {
        setState(() => _model.clearSelectedCommunityPost());
        _model.currentIndex = _model.tabController.index.toInt();
        FocusScope.of(context).requestFocus(FocusNode());
      }
    });
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ConnectionProvider>(
      builder: (context, _, child) => GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: isDesktopLayout(context) ? Row(children: [_buildDesktopLeftView(), _buildDesktopDetailsView()]) : _buildBody(),
      ),
    );
  }

  Widget _buildDesktopLeftView() {
    return Expanded(child: _buildBody());
  }

  Widget _buildDesktopDetailsView() {
    return Expanded(child: CommunityPostDetailsPage(arguments: _model.selectedCommunityPostParams));
  }

  Widget _buildBody() {
    if (_model.fetchTypesState == LoadState.loadingState) return Scaffold(appBar: CommonAppBar(title: Text(AppLocalizations.dictionary().community)), body: Container());
    if (_model.fetchTypesState == LoadState.errorState) {
      return HomeScaffold.withAppBar(
        params: HomeAppBarRequiredParams(title: AppLocalizations.dictionary().community, context: context),
        body: HttpFailView(onRefresh: onRefresh),
      );
    }
    return _buildListPage();
  }

  Widget _buildListPage() {
    return HomeScaffold.withAppBar(
        params: HomeAppBarRequiredParams(title: AppLocalizations.dictionary().community, context: context, actions: [
          Consumer<ConnectionProvider>(
            builder: (context, _, child) {
              return ConnectionProvider().communityConnection.authorized
                  ? IconButton(
                      icon: const Icon(Icons.add_box_outlined),
                      color: const Color(0xFF87878C),
                      onPressed: () async {
                        Navigator.of(context).pushNamed(CommunityPostCreationPage.routeName, arguments: {"projectId": CommunityProject.id, "fullPath": CommunityProject.relativePath}).then((value) {
                          if (value != null && value is CommunityType) {
                            _model.globalKeys[value]?.currentState?.onRefresh().then((value) => setState(() {}));
                            _model.globalKeys[CommunityType.all]?.currentState?.onRefresh().then((value) => setState(() {}));
                          }
                        });
                      },
                    )
                  : const SizedBox();
            },
          )
        ]),
        backgroundColor: Theme.of(context).colorScheme.background,
        body: SafeArea(
          child: ValueListenableBuilder<List<CommunityType>>(
            valueListenable: _model.notifier,
            builder: (_, data, child) {
              return Column(
                children: [
                  if (CommunityProject.instance().inMaintenance) const CommunityMaintenanceNoticeView(),
                  Container(
                    margin: const EdgeInsets.fromLTRB(4, 0, 12, 0),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: AppTabBar(
                        controller: _model.tabController,
                        labelColor: const Color(0xFF171321),
                        labelStyle: const TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                        unselectedLabelColor: AppThemeData.secondaryColor,
                        unselectedLabelStyle: const TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                        isScrollable: true,
                        tabs: data.map((e) => Tab(text: e.typeName(Localizations.localeOf(context)))).toList(),
                      ),
                    ),
                  ),
                  Expanded(
                    child: TabBarView(controller: _model.tabController, children: data.map((e) => _buildFaqs(e)).toList()),
                  )
                ],
              );
            },
          ),
        ));
  }

  Widget _buildFaqs(CommunityType faqType) {
    return CommunityPostListView(
      key: _model.globalKey(faqType),
      labelName: faqType.isAll ? null : faqType.name,
      onFaqSelected: (CommunityPost? faq) {
        _model.changeSelectedCommunityPost(faq, Localizations.localeOf(context));
        if (isDesktopLayout(context)) {
          setState(() {});
        } else {
          if (faq != null) {
            Map<String, dynamic> params = _model.selectedCommunityPostParams;
            params['showLeading'] = true;
            Navigator.of(context).pushNamed(CommunityPostDetailsPage.routeName, arguments: params);
          }
        }
      },
    );
  }
}
