import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/layout/adaptive.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/settings/settings.dart';
import 'package:jihu_gitlab_app/core/system_version_detector.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/connect_server_tips.dart';
import 'package:jihu_gitlab_app/core/widgets/issue_state_and_creation_info_view.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_button.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_view.dart';
import 'package:jihu_gitlab_app/core/widgets/low_system_version_view.dart';
import 'package:jihu_gitlab_app/core/widgets/markdown/markdown_input_box.dart';
import 'package:jihu_gitlab_app/core/widgets/share_view.dart';
import 'package:jihu_gitlab_app/core/widgets/toast.dart';
import 'package:jihu_gitlab_app/core/widgets/votes/issue_votes_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/community/community_maintenance_notice_view.dart';
import 'package:jihu_gitlab_app/modules/community/community_post_edit_page.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_post.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_post_details_model.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_project.dart';
import 'package:jihu_gitlab_app/modules/issues/details/discussion_view.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_description_view.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label.dart';
import 'package:provider/provider.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

typedef EditLabelsCallback = void Function(List<Label> selectedLabels);
typedef OnReplayTappedCallBack = void Function(Discussion comment);

class CommunityPostDetailsPage extends StatefulWidget {
  static const String routeName = 'FaqDetailsPage';
  final Map arguments;

  const CommunityPostDetailsPage({required this.arguments, super.key});

  @override
  State<StatefulWidget> createState() => _CommunityPostDetailsPageState();
}

class _CommunityPostDetailsPageState extends State<CommunityPostDetailsPage> {
  final TextEditingController _commentController = TextEditingController();
  final CommunityPostDetailsModel _model = CommunityPostDetailsModel();
  final double _bottomActionBoxHeight = 45;

  FocusNode commentFocusNode = FocusNode();
  String placeholder = AppLocalizations.dictionary().commentPlaceholder;
  bool _submittable = false;
  bool _inCommentSubmitting = false;
  bool _inPageInitializing = false;
  bool _showLeading = false;

  String _title = '';
  SystemType _systemType = SystemType.latest;
  late AutoScrollController _controller;

  @override
  void initState() {
    SystemVersionDetector.instance().detect().then((value) => setState(() {
          _systemType = value;
        }));
    _pageInit();
    super.initState();
  }

  void _pageInit() {
    if (_inPageInitializing) return;
    _inPageInitializing = true;
    _model.loadingPage();
    _showLeading = widget.arguments['showLeading'] ?? false;
    _controller = AutoScrollController(viewportBoundaryGetter: () => Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom), axis: Axis.vertical);
    _model.config(widget.arguments['issueId'] ?? 0, widget.arguments['issueIid'] ?? 0);
    _title = widget.arguments['communityTypeName'] ?? '';
    if (_model.postId != 0) {
      _refreshPostInfo();
    }
    _inPageInitializing = false;
  }

  Future<void> _refreshPostInfo() => _model.getPostInfo().then((value) => setState(() {}));

  @override
  void dispose() {
    _commentController.dispose();
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (_model.postId != widget.arguments["issueId"]) _pageInit();
    return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          placeholder = AppLocalizations.dictionary().writePlaceholder;
          _model.selectedNoteId = 0;
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Scaffold(appBar: _buildAppBar(), backgroundColor: Colors.white, body: _buildBody()));
  }

  Widget _buildBody() {
    if (_systemType != SystemType.latest && _model.isForGeek) return LowSystemVersionView(systemType: _systemType, isForGeek: _model.isForGeek);
    if (_model.postId == 0) {
      return Center(child: Text(AppLocalizations.dictionary().noItemSelected(AppLocalizations.dictionary().post.toLowerCase()), style: const TextStyle(color: Colors.black54)));
    }
    if (_model.loadState == LoadState.loadingState) return const LoadingView();
    if (_model.loadState == LoadState.noItemState || _model.loadState == LoadState.errorState) return _build404View();
    return Consumer<ConnectionProvider>(builder: (context, _, child) {
      if (_model.loadState == LoadState.successState && ConnectionProvider().communityConnection.authorized && _model.comments.isEmpty) {
        _loadComments();
      }
      return SafeArea(
        top: !isDesktopLayout(context),
        bottom: (_showCommentBox() || CommunityProject.instance().inMaintenance),
        child: Stack(children: [
          Container(
              padding: EdgeInsets.only(bottom: (_showCommentBox() || CommunityProject.instance().inMaintenance) ? _bottomActionBoxHeight : 0),
              child: SingleChildScrollView(controller: _controller, physics: const ScrollPhysics(), child: _buildIssueDetail())),
          if (_showCommentBox()) _buildSendCommentMarkdownBox(),
          if (CommunityProject.instance().inMaintenance) const Align(alignment: Alignment.bottomCenter, child: CommunityMaintenanceNoticeView())
        ]),
      );
    });
  }

  bool _showCommentBox() => ConnectionProvider().communityConnection.authorized && !CommunityProject.instance().inMaintenance;

  void _loadComments() async {
    await _model.loadDiscussions();
  }

  void _saveCommentAndRefresh(StateSetter setBottomState) async {
    var commentContent = _commentController.text;
    if (commentContent.isEmpty) {
      return;
    }
    _toggleSubmitting(true, setBottomState);
    try {
      await _model.saveComment(commentContent).then((value) {
        Toast.success(context, AppLocalizations.dictionary().successful);
        Navigator.pop(context);
        _model.resetForReplyToComment();
      }).catchError((e) {
        Toast.error(context, AppLocalizations.dictionary().failed);
        Navigator.pop(context);
      });
      _loadComments();
    } finally {
      _toggleSubmitting(false, setBottomState);
    }
    _commentController.clear();
    commentFocusNode.unfocus();
    _updateButtonState("", setBottomState);
  }

  void _updateButtonState(String text, StateSetter setBottomState) {
    setBottomState(() {
      _submittable = text.isNotEmpty;
    });
  }

  void _toggleSubmitting(bool on, StateSetter setBottomState) {
    setBottomState(() {
      _inCommentSubmitting = on;
    });
  }

  CommonAppBar _buildAppBar() {
    if (_systemType != SystemType.latest && _model.isForGeek) {
      _title = "";
    }
    return CommonAppBar(
      title: Text(_title),
      showLeading: _showLeading,
      automaticallyImplyLeading: false,
      backgroundColor: Colors.white,
      actions: [
        if (!(_systemType != SystemType.latest && _model.isForGeek) && _model.communityPostInfo != null)
          Container(
              margin: const EdgeInsets.only(right: 20),
              child: PopupMenuButton<String>(
                padding: EdgeInsets.zero,
                position: PopupMenuPosition.under,
                itemBuilder: (context) => [PopupMenuItem<String>(padding: EdgeInsets.zero, child: ShareView(title: _model.communityPostInfo!.title, link: _model.communityPostInfo!.webUrl))],
                child: Container(
                  margin: const EdgeInsets.only(right: 20),
                  child: SvgPicture.asset("assets/images/external-link.svg",
                      semanticsLabel: "share", colorFilter: const ColorFilter.mode(AppThemeData.secondaryColor, BlendMode.srcIn), width: 18, height: 18),
                ),
              )),
      ],
    );
  }

  Widget _buildIssueDetail() {
    if (_model.communityPostInfo == null) return Container();
    var issueInfo = _model.communityPostInfo!;
    return Container(
      margin: const EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(issueInfo.title, style: const TextStyle(fontSize: 18, color: Color(0xFF1A1B36), fontWeight: FontWeight.w600)),
          _buildGap(),
          _buildStatusAndCreationInfo(issueInfo, () => setState(() {})),
          _buildGap(),
          _buildDescription(context, issueInfo),
          _buildGap(gap: 14),
          _buildComments(context, _model, _controller, (comment) {
            setState(() {
              placeholder = "${AppLocalizations.dictionary().reply} ${comment.notes[0].author.name}:";
              _model.selectedNoteId = comment.notes[0].id;
              _model.selectedDiscussionId = comment.id;
              renderSheet();
            });
          }),
        ],
      ),
    );
  }

  Widget _buildSendCommentMarkdownBox() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        width: double.infinity,
        height: _bottomActionBoxHeight,
        padding: const EdgeInsets.symmetric(horizontal: 16),
        decoration: const BoxDecoration(color: Colors.white, border: Border(top: BorderSide(width: 0.2, color: Color(0xFFCECECE)))),
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, crossAxisAlignment: CrossAxisAlignment.center, children: [
          Expanded(
            child: InkWell(
              onTap: () {
                setState(() {
                  placeholder = AppLocalizations.dictionary().commentPlaceholder;
                  _model.resetForReplyToComment();
                });
                renderSheet();
              },
              child: Row(
                children: [
                  SvgPicture.asset('assets/images/comment.svg', width: 18, height: 18),
                  Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 12),
                      child: Text(AppLocalizations.dictionary().commentPlaceholder,
                          textAlign: TextAlign.center, style: const TextStyle(color: Color(0xFF03162F), fontSize: 12, fontWeight: FontWeight.w400)))
                ],
              ),
            ),
          ),
          if (_model.communityPostInfo != null && _model.communityPostInfo!.hasEditPermission)
            InkWell(
                child: SizedBox(
                  width: 44,
                  height: 44,
                  child: Center(
                    child: SvgPicture.asset("assets/images/operate.svg",
                        semanticsLabel: "operate", colorFilter: const ColorFilter.mode(AppThemeData.secondaryColor, BlendMode.srcIn), width: 18, height: 18),
                  ),
                ),
                onTap: () => renderBottomSheet(_buildEditButton()))
        ]),
      ),
    );
  }

  InkWell _buildEditButton() {
    return InkWell(
      onTap: () {
        Navigator.of(context).pop();
        Navigator.of(context)
            .push(MaterialPageRoute(
                builder: (context) => CommunityPostEditPage(
                      postIid: _model.communityPostInfo?.iid ?? 0,
                      title: _model.communityPostInfo?.title ?? '',
                      description: _model.communityPostInfo?.description ?? '',
                    )))
            .then((value) => _refreshPostInfo());
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.symmetric(vertical: 12),
        child: Center(child: Text(AppLocalizations.dictionary().edit)),
      ),
    );
  }

  void renderBottomSheet(Widget widget) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context1, setBottomState) {
              return GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                },
                child: SafeArea(
                  child: AnimatedPadding(
                    padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
                    duration: const Duration(milliseconds: 100),
                    child: SingleChildScrollView(child: widget),
                  ),
                ),
              );
            },
          );
        });
  }

  void renderSheet() {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context1, setBottomState) {
              return GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                },
                child: SafeArea(
                  child: AnimatedPadding(
                    padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
                    duration: const Duration(milliseconds: 100),
                    child: SingleChildScrollView(
                      child: Container(
                          padding: const EdgeInsets.all(12),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              MarkdownInputBox(
                                controller: _commentController,
                                projectId: _model.projectId,
                                onChanged: () {
                                  _updateButtonState(_commentController.text, setBottomState);
                                },
                                placeholder: placeholder,
                                scrollable: true,
                              ),
                              const SizedBox(
                                height: 12,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    flex: 1,
                                    child: Offstage(
                                      offstage: !_model.shouldShowInternalSwitch,
                                      child: Row(
                                        children: [
                                          Transform.scale(
                                              scale: 0.7,
                                              child: CupertinoSwitch(
                                                  activeColor: Theme.of(context).primaryColor, value: _model.isInternal, onChanged: (value) => setBottomState(() => _model.isInternal = value))),
                                          Text(AppLocalizations.dictionary().internal, style: const TextStyle(color: AppThemeData.secondaryColor, fontSize: 13, fontWeight: FontWeight.w400)),
                                        ],
                                      ),
                                    ),
                                  ),
                                  LoadingButton.asElevatedButton(
                                    key: const Key('comment-reply-inkwell'),
                                    isLoading: _inCommentSubmitting,
                                    disabled: !_submittable,
                                    text: Text(AppLocalizations.dictionary().comment, style: const TextStyle(color: Colors.white, fontSize: 12)),
                                    onPressed: () {
                                      _saveCommentAndRefresh(setBottomState);
                                    },
                                  ),
                                ],
                              ),
                            ],
                          )),
                    ),
                  ),
                ),
              );
            },
          );
        });
  }

  Widget _buildDescription(BuildContext context, CommunityPost faq) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        faq.description.isEmpty
            ? const SizedBox()
            : IssueDescriptionView(
                projectId: _model.projectId,
                padding: const EdgeInsets.only(left: 0, top: 12, right: 16, bottom: 16),
                data: faq.description,
                context: context,
              ),
        IssueVotesView(issueIid: _model.postIid, projectIid: _model.projectId, fullPath: _model.fullPath, connection: ConnectionProvider().communityConnection),
        _buildDivider()
      ],
    );
  }

  Widget _build404View() {
    return Align(
      alignment: const FractionalOffset(0.5, 0.2),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SvgPicture.asset("assets/images/not_found.svg", width: 60, height: 60),
          const SizedBox(height: 8),
          Text(AppLocalizations.dictionary().pageNotFound, style: const TextStyle(color: Color(0xFF171321), fontSize: 14, fontWeight: FontWeight.w400)),
          const SizedBox(height: 8),
          SizedBox(
            width: 230,
            child: Text(AppLocalizations.dictionary().pageNotFoundHint, style: const TextStyle(color: Color(0xFF171321), fontSize: 14, fontWeight: FontWeight.w400), textAlign: TextAlign.center),
          )
        ],
      ),
    );
  }

  Widget _buildComments(BuildContext context, CommunityPostDetailsModel model, AutoScrollController controller, OnReplayTappedCallBack onReplayTappedCallBack) {
    return ValueListenableBuilder<List<Discussion>>(
        valueListenable: model.notifier,
        builder: (BuildContext context, List<Discussion> data, Widget? child) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(AppLocalizations.dictionary().comments, style: const TextStyle(color: Color(0xFF1A1B36), fontSize: 15, fontWeight: FontWeight.w600)),
              _buildGap(gap: 14),
              buildDiscussionsView(data, context, model, controller, onReplayTappedCallBack)
            ],
          );
        });
  }

  Widget buildDiscussionsView(List<Discussion> data, BuildContext context, CommunityPostDetailsModel model, AutoScrollController controller, OnReplayTappedCallBack onReplayTappedCallBack) {
    if (ConnectionProvider().communityConnection.authorized) {
      return data.isEmpty
          ? Center(child: Text(AppLocalizations.dictionary().noData, style: Theme.of(context).textTheme.bodySmall))
          : _buildDiscussionList(context, model, controller, onReplayTappedCallBack);
    }

    return const ConnectServerTips(targetUrl: 'jihulab.com');
  }

  Widget _buildDiscussionList(BuildContext context, CommunityPostDetailsModel model, AutoScrollController controller, OnReplayTappedCallBack onReplayTappedCallBack) {
    final colorScheme = Theme.of(context).colorScheme;
    return ValueListenableBuilder<List<Discussion>>(
      valueListenable: model.notifier,
      builder: (BuildContext context, List<Discussion> data, Widget? child) {
        return Column(
          key: const Key('DiscussionListView'),
          children: model.comments.map((comment) {
            int index = model.comments.indexOf(comment);
            return Column(
              children: [
                if (index > 0) const Divider(thickness: 8, height: 8, color: Colors.white),
                AutoScrollTag(
                  key: ValueKey(comment.notes[0].noteableIid),
                  controller: controller,
                  index: comment.notes[0].noteableIid,
                  highlightColor: colorScheme.primary.withOpacity(0.1),
                  child: DiscussionView(
                    projectId: model.projectId,
                    discussion: comment,
                    onTap: () => onReplayTappedCallBack(comment),
                    autoScrollController: controller,
                  ),
                )
              ],
            );
          }).toList(),
        );
      },
    );
  }

  Widget _buildDivider() {
    return const Divider(thickness: 1, color: Color(0xFFEAEAEA));
  }

  Widget _buildGap({double gap = 8}) {
    return SizedBox(height: gap);
  }

  Widget _buildStatusAndCreationInfo(CommunityPost issueInfo, VoidCallback changeTimeCallBack) {
    return IssueStateAndCreationInfoView(
      state: issueInfo.state,
      confidential: issueInfo.confidential,
      authorName: issueInfo.author.name,
      authorAvatarUrl: issueInfo.author.avatarUrl,
      recentActivities: " ${Settings.instance().precisionTime().isEnabled() ? AppLocalizations.dictionary().createdAtPreciseFormat : AppLocalizations.dictionary().created}",
      recentActivityTimes: issueInfo.createdAt,
      onTimeFormatChanged: changeTimeCallBack,
    );
  }
}
