import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/system_version_detector.dart';
import 'package:jihu_gitlab_app/core/themes/project_style.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_button.dart';
import 'package:jihu_gitlab_app/core/widgets/low_system_version_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/community/models/request_access_model.dart';

class RequestAccessPage extends StatefulWidget {
  const RequestAccessPage({required this.projectId, super.key});

  final int projectId;

  @override
  State<RequestAccessPage> createState() => _RequestAccessPageState();
}

class _RequestAccessPageState extends State<RequestAccessPage> {
  final RequestAccessModel _model = RequestAccessModel();
  bool _didRequestSucceed = false;

  @override
  void initState() {
    SystemVersionDetector.instance().detect().then((value) => setState(() {
          _model.systemType = value;
        }));
    _model.init(widget.projectId).then((value) => setState(() {}));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (_model.systemType == SystemType.unmaintainable) {
      return const LowSystemVersionView(systemType: SystemType.unmaintainable, isForGeek: false);
    }
    return Center(
      heightFactor: 3,
      child: SizedBox(
        height: 200,
        child: Column(
          children: _didRequestSucceed ? _buildRequestSucceedBox(context) : _buildRequestBox(context),
        ),
      ),
    );
  }

  List<Widget> _buildRequestSucceedBox(BuildContext context) {
    return [
      SvgPicture.asset('assets/images/succeed.svg', width: 40, height: 40),
      const SizedBox(height: 20),
      SizedBox(
        width: MediaQuery.of(context).size.width * 0.7,
        child: Text(
          AppLocalizations.dictionary().requestAccessQueued,
          textAlign: TextAlign.center,
          style: ProjectStyle.build(Theme.of(context).textTheme.bodySmall, Colors.black87),
        ),
      ),
    ];
  }

  List<Widget> _buildRequestBox(BuildContext context) {
    return [
      Text(
        AppLocalizations.dictionary().joinCommunityToPost,
        style: ProjectStyle.build(Theme.of(context).textTheme.bodySmall, Colors.black87),
      ),
      const SizedBox(height: 20),
      LoadingButton.asElevatedButton(
        isLoading: _model.requestInProgress,
        padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
        text: Text(
          AppLocalizations.dictionary().requestAccess,
          style: ProjectStyle.build(Theme.of(context).textTheme.bodySmall, Colors.white),
        ),
        onPressed: () {
          setState(() {
            _model.requestAccess().then((value) => setState(() => _didRequestSucceed = value));
          });
        },
      ),
    ];
  }
}
