import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/loader.dart';
import 'package:jihu_gitlab_app/core/widgets/alerter.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/input_field_with_title.dart';
import 'package:jihu_gitlab_app/core/widgets/markdown/markdown_input_box.dart';
import 'package:jihu_gitlab_app/core/widgets/toast.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_project.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/models/issue_update_model.dart';

class CommunityPostEditPage extends StatefulWidget {
  const CommunityPostEditPage({required this.postIid, required this.title, required this.description, super.key});

  final int postIid;
  final String title;
  final String description;

  @override
  State<CommunityPostEditPage> createState() => _CommunityPostEditPageState();
}

class _CommunityPostEditPageState extends State<CommunityPostEditPage> {
  final IssueUpdateModel _model = IssueUpdateModel();
  final TextEditingController _issueTitleController = TextEditingController();
  final TextEditingController _issueDescriptionController = TextEditingController();

  bool _enableUpdate = false;
  String _title = '';
  String _description = '';

  @override
  void initState() {
    _issueTitleController.text = widget.title;
    _issueDescriptionController.text = widget.description;
    super.initState();
  }

  void onValueChanged() {
    _title = _issueTitleController.text;
    _description = _issueDescriptionController.text;
    _enableUpdate = (_title != widget.title || _description != widget.description);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Scaffold(
          appBar: CommonAppBar(
              title: Text(AppLocalizations.dictionary().edit),
              showLeading: true,
              leading: BackButton(
                  color: Colors.black,
                  onPressed: () {
                    if (!_enableUpdate) {
                      Navigator.pop(context);
                    } else {
                      Alerter.showCupertinoAlert(context,
                          title: AppLocalizations.dictionary().warning,
                          content: AppLocalizations.dictionary().confirmToLeavePageContent,
                          okTitle: AppLocalizations.dictionary().confirm,
                          onCancelPressed: () => Navigator.pop(context),
                          onOkPressed: () {
                            Navigator.pop(context);
                            Navigator.pop(context);
                          });
                    }
                  }),
              actions: [
                Container(
                  transform: Matrix4.translationValues(-12, 2, 0),
                  child: TextButton(
                      onPressed: _enableUpdate
                          ? () async {
                              Loader.showProgress(context, text: AppLocalizations.dictionary().inProgress);
                              _model
                                  .update(CommunityProject.id, widget.postIid,
                                      title: _title != widget.title ? _title : null,
                                      description: _description != widget.description ? _description : null,
                                      connection: ConnectionProvider().communityConnection)
                                  .then((value) {
                                Loader.hideProgress(context);
                                if (value) {
                                  Toast.success(context, AppLocalizations.dictionary().updateSuccessful);
                                  Navigator.pop(context);
                                } else {
                                  Toast.error(context, AppLocalizations.dictionary().updateFailed);
                                }
                              });
                            }
                          : null,
                      child: Text(AppLocalizations.dictionary().update, style: TextStyle(color: Theme.of(context).primaryColor.withOpacity(_enableUpdate ? 1 : 0.5)))),
                ),
              ]),
          body: SingleChildScrollView(
              physics: const ScrollPhysics(),
              child: SafeArea(
                child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  InputFieldWithTitle(
                    title: AppLocalizations.dictionary().createIssueTitle,
                    placeHolder: AppLocalizations.dictionary().issueTitlePlaceholder(AppLocalizations.dictionary().post),
                    controller: _issueTitleController,
                    onChanged: (text) {
                      onValueChanged();
                      setState(() {});
                    },
                  ),
                  _buildTitle(AppLocalizations.dictionary().description),
                  Container(
                      margin: const EdgeInsets.symmetric(horizontal: 16),
                      child: MarkdownInputBox(
                        projectId: CommunityProject.id,
                        controller: _issueDescriptionController,
                        issueSelectable: false,
                        memberSelectable: false,
                        placeholder: AppLocalizations.dictionary().issueDescriptionPlaceholder,
                        onChanged: () {
                          onValueChanged();
                          setState(() {});
                        },
                        videoSelectable: false,
                      )),
                ]),
              )),
        ));
  }

  Padding _buildTitle(String title, {EdgeInsetsGeometry padding = const EdgeInsets.all(16.0)}) {
    return Padding(
      padding: padding,
      child: Text(title, style: const TextStyle(color: Color(0XFF03162F), fontSize: 16, fontWeight: FontWeight.w600)),
    );
  }
}
