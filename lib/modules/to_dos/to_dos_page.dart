import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/layout/adaptive.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/connection_selector.dart';
import 'package:jihu_gitlab_app/core/widgets/home/home_scaffold.dart';
import 'package:jihu_gitlab_app/modules/issues/details/issue_details_page.dart';
import 'package:jihu_gitlab_app/modules/to_dos/to_do.dart';
import 'package:jihu_gitlab_app/modules/to_dos/to_dos_page_model.dart';
import 'package:jihu_gitlab_app/modules/to_dos/to_dos_tab_bar.dart';
import 'package:jihu_gitlab_app/modules/to_dos/to_dos_view.dart';
import 'package:jihu_gitlab_app/modules/to_dos/to_dos_view_model.dart';
import 'package:provider/provider.dart';

class ToDosPage extends StatefulWidget {
  const ToDosPage({Key? key}) : super(key: key);

  @override
  State<ToDosPage> createState() => _ToDosPageState();
}

class _ToDosPageState extends State<ToDosPage> with TickerProviderStateMixin {
  final ToDosPageModel _model = ToDosPageModel();

  @override
  void initState() {
    super.initState();
    _model.init(this);
  }

  @override
  void dispose() {
    _model.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ConnectionProvider>(
      builder: (context, _, child) {
        return isDesktopLayout(context) && ConnectionProvider.authorized ? Row(children: [_buildDesktopLeftView(), _buildDesktopDetailsView()]) : _buildPage();
      },
    );
  }

  Widget _buildDesktopLeftView() {
    return Expanded(child: _buildPage());
  }

  Widget _buildDesktopDetailsView() {
    return Expanded(child: IssueDetailsPage(arguments: _model.selectedTodoAsParams));
  }

  Widget _buildToDosPage() {
    return HomeScaffold.withAppBar(
      params: HomeAppBarRequiredParams(
        context: context,
        flexibleSpace: SafeArea(
          child: ToDosTabBar(tabController: _model.tabController),
        ),
        actions: [const ConnectionSelector(key: Key('ToDosConnectionSelector'))],
      ),
      body: TabBarView(
        key: const Key('ToDosPageTabBarView'),
        controller: _model.tabController,
        children: [
          ToDosView(
            state: ToDoState.pending,
            onIssueTypeTodoTappedInDesktop: (todo) => _onIssueTypeTodoTappedInDesktop(todo),
            onItemMarkedDone: _model.onItemMarkedDone,
          ),
          ToDosView(
            key: ToDosPageModel.doneListKey,
            state: ToDoState.done,
            onIssueTypeTodoTappedInDesktop: (todo) => _onIssueTypeTodoTappedInDesktop(todo),
          )
        ],
      ),
      backgroundColor: Theme.of(context).colorScheme.background,
    );
  }

  void _onIssueTypeTodoTappedInDesktop(ToDo todo) {
    ProjectProvider().clear();
    setState(() => _model.onSelectedItemChanged(todo));
  }

  Widget _buildPage() {
    return SafeArea(top: !isDesktopLayout(context), bottom: !isDesktopLayout(context), child: _buildToDosPage());
  }
}
