import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/modules/to_dos/to_do.dart';
import 'package:jihu_gitlab_app/modules/to_dos/to_dos_view.dart';

class ToDosPageModel {
  static GlobalKey<ToDosViewState> doneListKey = GlobalKey();

  ToDo? _selectedItem;
  late TabController _tabController;
  bool _hasMarkedDoneItem = false;

  void init(TickerProvider tickerProvider) {
    _tabController = TabController(initialIndex: 0, length: 2, vsync: tickerProvider);
    _tabController.addListener(() {
      if (_hasMarkedDoneItem && _tabController.index.toDouble() == _tabController.animation?.value && _tabController.index == 1) {
        doneListKey.currentState?.refreshAfterItemMarkedDone();
        _hasMarkedDoneItem = false;
      }
    });
  }

  void dispose() {
    _tabController.dispose();
  }

  void onSelectedItemChanged(ToDo toDo) {
    _selectedItem = toDo;
  }

  void onItemMarkedDone() {
    _hasMarkedDoneItem = true;
  }

  void onAfterRefreshDoneList() {
    _hasMarkedDoneItem = false;
  }

  bool get hasMarkedDoneItem => _hasMarkedDoneItem;

  TabController get tabController => _tabController;

  Map<String, dynamic> get selectedTodoAsParams => _selectedItem?.asDetailPageParams ?? {};
}
