import 'package:jihu_gitlab_app/core/domain/action_name.dart';
import 'package:jihu_gitlab_app/core/domain/assignee/assignee.dart';
import 'package:jihu_gitlab_app/core/domain/project.dart';
import 'package:jihu_gitlab_app/core/domain/target.dart';
import 'package:jihu_gitlab_app/core/time.dart';

class ToDo {
  int id;
  int projectId;
  int iid;
  String title;
  String targetUrl;
  String createdAt;
  Time updatedAt;
  ActionName actionName;
  String targetType;
  Target target;
  Project project;
  Assignee author;
  String state;

  ToDo._(this.id, this.projectId, this.iid, this.title, this.targetUrl, this.createdAt, this.updatedAt, this.actionName, this.target, this.project, this.author, this.targetType, this.state);

  factory ToDo.fromJson(Map<String, dynamic> json) {
    return ToDo._(
      json['id'],
      json['project_id'] ?? 0,
      json['iid'] ?? 0,
      json['title'] ?? '',
      json['target_url'] ?? '',
      json['created_at'] ?? '',
      Time.init(json['updated_at'] ?? ''),
      ActionName.init(json['action_name'] ?? ""),
      Target.fromJson(json['target'] ?? {}),
      Project.fromJson(json['project'] ?? {}),
      Assignee.fromJson(json['author'] ?? {}),
      json['target_type'] ?? '',
      json['state'] ?? '',
    );
  }

  Map<String, dynamic> get asDetailPageParams {
    return {'projectId': project.id, 'issueId': id, 'issueIid': iid, 'targetId': target.iid, 'targetIid': target.iid, 'targetUrl': targetUrl, 'pathWithNamespace': project.pathWithNamespace};
  }

  Map<String, dynamic> get toMergeRequestParams {
    return {'projectId': project.id, 'mergeRequestIid': target.iid, 'projectName': project.name, 'fullPath': project.pathWithNamespace};
  }

  bool get isNotIssue => "Issue" != targetType;
}
