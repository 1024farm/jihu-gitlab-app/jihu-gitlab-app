import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ToDoTypePainter extends CustomPainter {
  final double _width = 28;
  final double _topLineLength = 10.0;
  final bool showTopLine;
  final bool showBottomLine;
  late final Paint _paint;
  PictureInfo? pictureInfo;

  ToDoTypePainter({required this.showTopLine, required this.showBottomLine, required this.pictureInfo})
      : _paint = Paint()
          ..color = const Color(0xFFEAEAEA)
          ..strokeWidth = 1.0
          ..isAntiAlias = true;

  @override
  void paint(Canvas canvas, Size size) {
    final double radius = _width / 2;
    if (showTopLine) canvas.drawLine(Offset(radius, 0), Offset(radius, _topLineLength), _paint);
    if (showBottomLine) canvas.drawLine(Offset(radius, _width + _topLineLength), Offset(radius, size.height), _paint);
    if (pictureInfo?.picture != null) {
      canvas.translate(0, _topLineLength);
      canvas.drawPicture(pictureInfo!.picture);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return (oldDelegate as ToDoTypePainter).pictureInfo != pictureInfo || (oldDelegate).showTopLine != showTopLine || (oldDelegate).showBottomLine != showBottomLine;
  }
}
