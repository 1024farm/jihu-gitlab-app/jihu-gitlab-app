import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/widgets/app_tab_bar.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class ToDosTabBar extends StatelessWidget {
  ToDosTabBar({required this.tabController, super.key});

  final TabController tabController;
  final _tabBarTitles = [AppLocalizations.dictionary().todo, AppLocalizations.dictionary().done];

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return SizedBox(
      height: 64.0,
      child: Container(
          alignment: Alignment.center,
          child: AppTabBar(
              controller: tabController,
              isScrollable: true,
              labelStyle: textTheme.titleLarge,
              unselectedLabelStyle: textTheme.titleMedium,
              tabs: _tabBarTitles.map((item) => Tab(text: item)).toList())),
    );
  }
}
