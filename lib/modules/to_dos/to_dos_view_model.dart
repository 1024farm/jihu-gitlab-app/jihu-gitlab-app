import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter/cupertino.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/modules/to_dos/to_do.dart';

enum ToDoState { pending, done }

class ToDosViewModel {
  static const int _size = 20;
  int _page = 1;
  bool _hasNextPage = false;
  LoadState _loadState = LoadState.noItemState;
  List<ToDo> todos = [];
  EasyRefreshController? _refreshController;
  ToDo? _selectedItem;
  late ToDoState _state;
  late ValueNotifier<List<ToDo>> notifier;

  void init({required ToDoState state}) {
    _state = state;
    notifier = ValueNotifier(todos);
    _refreshController ??= EasyRefreshController(controlFinishRefresh: true, controlFinishLoad: true);
    ConnectionProvider().addListener(listener);
  }

  void listener() {
    _refresh().then((value) {
      notifier.value = List.of(todos);
    });
  }

  Future<List<ToDo>> _getTodos() async {
    return HttpClient.instance().get<List<dynamic>>(Api.join("/todos?page=$_page&per_page=$_size&state=${_state.name}")).then((response) {
      return response.body().where((element) => element['target_type'] == "Issue" || element['target_type'] == "MergeRequest").map((e) {
        return ToDo.fromJson(e);
      }).toList();
    });
  }

  Future<ToDo> _markItemAsDone(int itemId) async {
    return HttpClient.instance().post<dynamic>(Api.join("/todos/$itemId/mark_as_done"), {}).then((response) {
      return ToDo.fromJson(response.body());
    });
  }

  Future<bool> _refresh() async {
    try {
      _page = 1;
      todos = await _getTodos();
      _loadState = LoadState.successState;
      _hasNextPage = todos.length >= _size;
      return Future.value(true);
    } catch (e) {
      LogHelper.err("ToDosModel refresh todos error", e);
      _loadState = LoadState.errorState;
      return Future.value(false);
    }
  }

  Future<bool> loadMore() async {
    try {
      _page++;
      List<ToDo> moreTodos = await _getTodos();
      todos.addAll(moreTodos);
      _hasNextPage = moreTodos.length >= _size;
      return Future.value(true);
    } catch (e) {
      LogHelper.err("ToDosModel load more todos error", e);
      _page--;
      return Future.value(false);
    }
  }

  void clear() {
    _page = 1;
    _hasNextPage = false;
    _loadState = LoadState.noItemState;
    todos = [];
  }

  Future<bool> markItemAsDoneAt(ToDo todo) async {
    try {
      ToDo newItem = await _markItemAsDone(todo.id);
      var result = todo.id == newItem.id && newItem.state == 'done';
      if (result) todos.remove(todo);
      notifier.value = List.of(todos);
      return result;
    } catch (e) {
      LogHelper.err("ToDosModel _markItemAsDone error", e);
      return false;
    }
  }

  void dispose() {
    _refreshController?.dispose();
    ConnectionProvider().removeListener(listener);
  }

  Future<void> onRefresh() async {
    bool success = await _refresh();
    notifier.value = List.of(todos);
    if (success) {
      _refreshController?.finishRefresh();
      _refreshController?.resetFooter();
    } else {
      _refreshController?.finishRefresh(IndicatorResult.fail);
    }
  }

  Future<void> onLoadMore() async {
    bool success = await loadMore();
    notifier.value = List.of(todos);
    if (success) {
      _refreshController?.finishLoad(hasNextPage ? IndicatorResult.success : IndicatorResult.noMore);
    } else {
      _refreshController?.finishLoad(IndicatorResult.fail);
    }
  }

  void refreshAfterItemMarkedDone() {
    _refreshController?.callRefresh();
  }

  void onItemSelected(ToDo? selectedItem) {
    _selectedItem = selectedItem;
  }

  LoadState get loadState {
    return _loadState;
  }

  bool get hasNextPage {
    return _hasNextPage;
  }

  EasyRefreshController get refreshController => _refreshController!;

  int get selectedItemId => _selectedItem == null ? -1 : _selectedItem!.id;

  ToDoState get todoState => _state;
}
