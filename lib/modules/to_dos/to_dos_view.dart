import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/layout/adaptive.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/widgets/alerter.dart';
import 'package:jihu_gitlab_app/core/widgets/http_fail_view.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_view.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/core/widgets/toast.dart';
import 'package:jihu_gitlab_app/core/widgets/unauthorized_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/details/issue_details_page.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_request_page.dart';
import 'package:jihu_gitlab_app/modules/to_dos/to_do.dart';
import 'package:jihu_gitlab_app/modules/to_dos/to_do_view.dart';
import 'package:jihu_gitlab_app/modules/to_dos/to_dos_view_model.dart';
import 'package:provider/provider.dart';

typedef OnItemTapped = void Function(ToDo todo);

class ToDosView extends StatefulWidget {
  const ToDosView({required this.state, this.onIssueTypeTodoTappedInDesktop, this.onItemMarkedDone, Key? key}) : super(key: key);
  final ToDoState state;
  final OnItemTapped? onIssueTypeTodoTappedInDesktop;
  final VoidCallback? onItemMarkedDone;

  @override
  State<ToDosView> createState() => ToDosViewState();
}

class ToDosViewState extends State<ToDosView> with AutomaticKeepAliveClientMixin {
  final ToDosViewModel _model = ToDosViewModel();
  PictureInfo? _issuePictureInfo;
  PictureInfo? _mrPictureInfo;

  @override
  void initState() {
    super.initState();
    _loadSvgPicture("assets/images/todos_issue.svg").then((value) {
      if (mounted) setState(() => _issuePictureInfo = value);
    });
    _loadSvgPicture("assets/images/todos_mr.svg").then((value) {
      if (mounted) setState(() => _mrPictureInfo = value);
    });
    _model.init(state: widget.state);
    if (ConnectionProvider.authorized) _doRefresh();
  }

  @override
  void dispose() {
    _model.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Consumer<ConnectionProvider>(
      builder: (context, _, child) {
        if (!ConnectionProvider.authorized) {
          _model.clear();
          return const UnauthorizedView();
        }
        return _buildToDosBody();
      },
    );
  }

  Future<void> _doRefresh() async {
    _model.onRefresh();
  }

  void refreshAfterItemMarkedDone() {
    _model.refreshAfterItemMarkedDone();
  }

  Widget _buildToDosBody() {
    return ValueListenableBuilder<List<ToDo>>(
      valueListenable: _model.notifier,
      builder: (BuildContext context, List<ToDo> data, Widget? child) {
        if (_model.loadState == LoadState.noItemState) return const LoadingView();
        if (_model.loadState == LoadState.errorState) return HttpFailView(onRefresh: () => _doRefresh());
        if (_model.loadState == LoadState.successState && data.isEmpty) {
          return TipsView(
              icon: 'assets/images/no_item.svg',
              message: _model.todoState == ToDoState.pending ? AppLocalizations.dictionary().noTodoItems : AppLocalizations.dictionary().noDoneItems,
              onRefresh: () => _doRefresh());
        }
        return EasyRefresh(
          controller: _model.refreshController,
          refreshOnStart: false,
          header: const CupertinoHeader(),
          footer: const CupertinoFooter(),
          onRefresh: () => _doRefresh(),
          onLoad: _model.onLoadMore,
          child: _buildListView(data),
        );
      },
    );
  }

  Widget _buildListView(List<ToDo> data) {
    return ListView.builder(
      key: const Key('ToDosView'),
      itemBuilder: (context, index) {
        var toDo = data[index];
        return ToDoView(
          toDo: toDo,
          state: widget.state,
          pictureInfo: toDo.isNotIssue ? _mrPictureInfo : _issuePictureInfo,
          isFirst: index == 0,
          isLast: index == data.length - 1,
          isSelected: _model.selectedItemId == toDo.id,
          onItemTapped: () => onItemTaped(toDo, index),
          onMarkDoneButtonTapped: () async {
            return _model.markItemAsDoneAt(toDo).then((value) => widget.onItemMarkedDone?.call());
          },
        );
      },
      itemCount: data.length,
    );
  }

  Future<PictureInfo> _loadSvgPicture(String svgString) {
    return vg.loadPicture(SvgAssetLoader(svgString), null);
  }

  void showConfirmToMarkTodoItemDoneAlert(ToDo todo) {
    if (_model.todoState == ToDoState.done) {
      return;
    }
    Alerter.showCupertinoAlert(context,
        title: AppLocalizations.dictionary().markTodoAsDoneTitle,
        content: AppLocalizations.dictionary().markTodoAsDoneContent,
        onCancelPressed: () => Navigator.pop(context),
        onOkPressed: () async {
          bool result = await _model.markItemAsDoneAt(todo);
          setState(() {
            Navigator.pop(context);
          });
          if (result) {
            widget.onItemMarkedDone?.call();
          } else {
            Toast.show(AppLocalizations.dictionary().markTodoAsDoneFailNotice);
          }
        });
  }

  void onItemTaped(ToDo todo, int index) {
    setState(() => _model.onItemSelected(todo.isNotIssue ? null : todo));
    if (todo.targetType == 'MergeRequest') {
      Navigator.of(context).pushNamed(MergeRequestPage.routeName, arguments: todo.toMergeRequestParams).then((value) {
        if (!isDesktopLayout(context) && ToDoState.pending == _model.todoState) showConfirmToMarkTodoItemDoneAlert(todo);
      });
      return;
    }
    if (isDesktopLayout(context)) {
      widget.onIssueTypeTodoTappedInDesktop?.call(todo);
    } else {
      var params = todo.asDetailPageParams;
      params['showLeading'] = true;
      Navigator.of(context).pushNamed(IssueDetailsPage.routeName, arguments: params).then((value) {
        if (ToDoState.pending == _model.todoState) {
          showConfirmToMarkTodoItemDoneAlert(todo);
        }
      });
    }
  }

  @override
  bool get wantKeepAlive => true;
}
