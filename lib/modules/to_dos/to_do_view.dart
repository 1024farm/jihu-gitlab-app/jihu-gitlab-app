import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:jihu_gitlab_app/core/domain/assignee/assignee.dart';
import 'package:jihu_gitlab_app/core/layout/adaptive.dart';
import 'package:jihu_gitlab_app/core/time.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar/avatar.dart';
import 'package:jihu_gitlab_app/core/widgets/changeable_time.dart';
import 'package:jihu_gitlab_app/modules/to_dos/to_do.dart';
import 'package:jihu_gitlab_app/modules/to_dos/to_do_type_painter.dart';
import 'package:jihu_gitlab_app/modules/to_dos/to_dos_view_model.dart';

typedef VoidFutureCallBack = Future<void> Function();

class ToDoView extends StatefulWidget {
  const ToDoView(
      {required this.toDo,
      required this.state,
      required this.pictureInfo,
      required this.isFirst,
      required this.isLast,
      this.isSelected = false,
      this.onItemTapped,
      this.onMarkDoneButtonTapped,
      Key? key})
      : super(key: key);
  final ToDo toDo;
  final ToDoState state;
  final PictureInfo? pictureInfo;
  final bool isFirst;
  final bool isLast;
  final bool isSelected;
  final VoidCallback? onItemTapped;
  final VoidFutureCallBack? onMarkDoneButtonTapped;

  @override
  State<ToDoView> createState() => _ToDoViewState();
}

class _ToDoViewState extends State<ToDoView> {
  bool _isMarkingDone = false;

  @override
  Widget build(BuildContext context) {
    ToDo todoItem = widget.toDo;
    return Container(
        padding: const EdgeInsets.symmetric(horizontal: 14),
        child: CustomPaint(
            painter: ToDoTypePainter(showTopLine: !widget.isFirst, showBottomLine: !widget.isLast, pictureInfo: widget.pictureInfo),
            child: Container(
              margin: const EdgeInsets.only(left: 32),
              child: InkWell(
                key: const Key('ToDosViewItem'),
                onTap: widget.onItemTapped,
                child: Container(
                  constraints: const BoxConstraints(minHeight: 50),
                  margin: const EdgeInsets.only(bottom: 12),
                  padding: const EdgeInsets.only(left: 12, top: 10),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: const BorderRadius.all(Radius.circular(4)),
                      border: isDesktopLayout(context) && widget.isSelected ? Border.all(color: Theme.of(context).primaryColor) : null),
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                    _buildAuthorInfo(todoItem.author, todoItem.updatedAt),
                    const SizedBox(height: 4),
                    Padding(
                      padding: const EdgeInsets.only(right: 12),
                      child: Text.rich(TextSpan(text: todoItem.target.title, style: const TextStyle(fontSize: 14, color: Color(0xFF03162F), fontWeight: FontWeight.w500))),
                    ),
                    const SizedBox(height: 8),
                    const Divider(color: Color(0xFFEAEAEA), thickness: 1.0, height: 1, endIndent: 12),
                    _buildProjectInfo(todoItem.project.name)
                  ]),
                ),
              ),
            )));
  }

  Widget _buildAuthorInfo(Assignee author, Time updatedAt) {
    return Container(
      padding: const EdgeInsets.only(right: 12),
      child: Row(
        children: [
          Avatar(avatarUrl: author.avatarUrl, size: 28),
          const SizedBox(width: 6),
          Expanded(
            child: Text(
              author.name,
              style: const TextStyle(fontSize: 14, color: Color(0xFF03162F), fontWeight: FontWeight.w400),
              overflow: TextOverflow.ellipsis,
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: ChangeableTime.show(() => setState(() {}), updatedAt, const Color(0xFF95979A), editable: false),
          )
        ],
      ),
    );
  }

  Widget _buildProjectInfo(String projectName) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SvgPicture.asset("assets/images/project_icon.svg", height: 16, width: 16),
        const SizedBox(width: 2),
        Expanded(child: Text(projectName, style: const TextStyle(fontSize: 12, color: Color(0xFF95979A)))),
        _buildMarkDoneButton()
      ],
    );
  }

  Widget _buildMarkDoneButton() {
    if (_isMarkingDone) {
      return Padding(padding: const EdgeInsets.all(12), child: SizedBox(width: 16, height: 16, child: CircularProgressIndicator(color: Theme.of(context).primaryColor, strokeWidth: 2)));
    }
    if (pending) {
      return InkWell(onTap: _onMarkDoneButtonTapped, child: Padding(padding: const EdgeInsets.all(12), child: SvgPicture.asset("assets/images/mark_as_done.svg", height: 16, width: 16)));
    }
    return const Padding(padding: EdgeInsets.all(12), child: SizedBox(height: 16, width: 16));
  }

  void _onMarkDoneButtonTapped() async {
    if (pending) {
      setState(() => _isMarkingDone = true);
      await widget.onMarkDoneButtonTapped?.call();
      setState(() => _isMarkingDone = false);
    }
  }

  bool get pending => widget.state == ToDoState.pending;
}
