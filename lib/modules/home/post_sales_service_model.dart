import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/license_type_detector.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';

class PostSalesServiceModel {
  int _page = 1;
  int _size = 100;

  Future<List<int>> _fetchGroups() async {
    try {
      var response = await HttpClient.instance().get<List<dynamic>>(Api.join('/groups?page=$_page&per_page=$_size'));
      var resp = response.body();
      return resp.map((e) => e['id'] as int).toList();
    } catch (error) {
      debugPrint("PostSalesServiceModel _fetchGroups: $error");
      return [];
    }
  }

  Future<List<int>> _allGroupIds() async {
    List<int> list = [];
    _page = 0;
    await Future.doWhile(() async {
      _page++;
      List<int> ids = await _fetchGroups();
      list.addAll(ids);
      return _size <= ids.length;
    });
    return list;
  }

  Future<bool> _checkSelfManagedUserIsFree() async {
    _page = 1;
    _size = 1;
    List<int> ids = await _fetchGroups();
    if (ids.isEmpty) return true;
    return await LicenseTypeDetector.instance().isFree(ids.first);
  }

  Future<bool> isFree() async {
    bool isFreeCheckDomain = ConnectionProvider.currentBaseUrl.isFree();
    debugPrint("UserProvider.isSelfManagedGitLab: ${ConnectionProvider.isSelfManagedGitLab}, isFreeCheckDomain: $isFreeCheckDomain");
    if (ConnectionProvider.isSelfManagedGitLab && !isFreeCheckDomain) {
      return _checkSelfManagedUserIsFree();
    }

    List<int> groupIds = await _allGroupIds();
    try {
      await Future.forEach(groupIds, (groupId) async {
        bool result = await LicenseTypeDetector.instance().isFree(groupId);
        if (!result) {
          throw Exception('Paying User');
        }
      });
      return true;
    } catch (error) {
      debugPrint("PostSalesServiceModel isFree error: $error");
      return false;
    }
  }
}
