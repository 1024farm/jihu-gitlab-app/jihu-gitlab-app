class Language {
  String name;
  bool isSelected;
  String key;

  Language(this.name, this.key, {required this.isSelected});
}
