import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/home/language.dart';

class LanguageSettingsModel {
  List<Language> languages = [Language(AppLocalizations.dictionary().automatic, "auto", isSelected: true), Language("简体中文", "zh", isSelected: false), Language("English", "en", isSelected: false)];

  void update(String key) {
    for (var element in languages) {
      element.isSelected = element.key == key;
    }
  }
}
