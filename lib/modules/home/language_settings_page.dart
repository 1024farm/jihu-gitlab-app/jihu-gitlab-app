import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/l10n/current_locale.dart';
import 'package:jihu_gitlab_app/modules/home/language_settings_model.dart';
import 'package:provider/provider.dart';

class LanguageSettingsPage extends StatefulWidget {
  static const routeName = "LanguageSettingPage";

  const LanguageSettingsPage({super.key});

  @override
  State<StatefulWidget> createState() => _LanguageSettingsPageState();
}

class _LanguageSettingsPageState extends State<LanguageSettingsPage> {
  final LanguageSettingsModel _model = LanguageSettingsModel();

  @override
  void initState() {
    super.initState();
    setState(() {
      _model.update(LocalStorage.get("language", "auto"));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CommonAppBar(
          title: Text(AppLocalizations.dictionary().settings),
          showLeading: true,
          actions: [
            TextButton(
                onPressed: () {
                  setState(() {
                    var languageCode = _model.languages.where((element) => element.isSelected == true).toList()[0].key;
                    if (languageCode == "auto") {
                      Provider.of<LocaleProvider>(context, listen: false).setLocale(Locale(Platform.localeName.split("_")[0]));
                      LocalStorage.save("language", "auto");
                    } else {
                      Provider.of<LocaleProvider>(context, listen: false).setLocale(Locale(languageCode));
                      LocalStorage.save("language", languageCode);
                    }
                  });
                  ConnectionProvider().notifyChanged();
                  Navigator.pop(context);
                },
                child: Text(AppLocalizations.dictionary().done))
          ],
        ),
        body: Container(
          margin: const EdgeInsets.only(left: 16, right: 16),
          child: ListView.separated(
            itemBuilder: (context, index) {
              return GestureDetector(
                child: Container(
                  padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 12),
                  decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(4)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [Text(_model.languages[index].name), _model.languages[index].isSelected ? SvgPicture.asset("assets/images/tick.svg", semanticsLabel: "tick") : const Text("")],
                  ),
                ),
                onTap: () {
                  updateSelected(index);
                },
              );
            },
            separatorBuilder: (BuildContext context, int index) => const Divider(height: 1, color: Color(0xFFEAEAEA)),
            itemCount: _model.languages.length,
          ),
        ));
  }

  void updateSelected(int index) {
    setState(() {
      _model.languages[index].isSelected = true;
      var list = _model.languages.where((element) => element.name != _model.languages[index].name).toList();
      for (int i = 0; i < list.length; i++) {
        list[i].isSelected = false;
      }
    });
  }
}
