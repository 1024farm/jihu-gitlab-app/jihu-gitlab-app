import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/browser_launcher.dart';
import 'package:jihu_gitlab_app/core/themes/project_style.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/home/post_sales_service_model.dart';

class PostSalesServicePage extends StatefulWidget {
  static const routeName = "PostSalesServicePage";

  const PostSalesServicePage({super.key});

  @override
  State<PostSalesServicePage> createState() => _PostSalesServicePageState();
}

class _PostSalesServicePageState extends State<PostSalesServicePage> {
  final PostSalesServiceModel _model = PostSalesServiceModel();
  bool? _isFreeUser;

  @override
  void initState() {
    checkFreeUser();
    super.initState();
  }

  Future<void> checkFreeUser() async {
    _isFreeUser = await _model.isFree();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final colorScheme = Theme.of(context).colorScheme;
    return Scaffold(
      appBar: CommonAppBar(title: Text(AppLocalizations.dictionary().postSalesService), showLeading: true),
      backgroundColor: colorScheme.background,
      body: _isFreeUser == null
          ? Center(
              heightFactor: 5,
              child: SizedBox(
                height: 74,
                child: Column(
                  children: [
                    Image.asset("assets/images/loading.png", height: 40, width: 40),
                    Container(margin: const EdgeInsets.only(top: 12), child: Text(AppLocalizations.dictionary().loading)),
                  ],
                ),
              ))
          : SafeArea(
              child: Column(
              children: [
                _buildCard(
                    context, _CardModel(title: AppLocalizations.dictionary().freeUser, subTitle: AppLocalizations.dictionary().supportForum, url: 'https://forum.gitlab.cn/', isCurrent: _isFreeUser!)),
                _buildCard(
                  context,
                  _CardModel(
                    title: AppLocalizations.dictionary().payingUser,
                    subTitle: AppLocalizations.dictionary().engineersSupport,
                    url: 'https://support.gitlab.cn/#/login',
                    isCurrent: !_isFreeUser!,
                    shouldShowPhoneCall: _isFreeUser!,
                  ),
                )
              ],
            )),
    );
  }

  Widget _buildCard(BuildContext context, _CardModel cardModel) {
    final colorScheme = Theme.of(context).colorScheme;
    final textTheme = Theme.of(context).textTheme;

    double browserIconWidth = 62.0;

    return Container(
      padding: const EdgeInsets.only(left: 16, right: 16, top: 16),
      height: cardModel.shouldShowPhoneCall ? 220 : 160,
      child: Card(
          clipBehavior: Clip.antiAlias,
          elevation: 0,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0), side: BorderSide(color: cardModel.isCurrent ? colorScheme.primary : Colors.white, width: 1)),
          child: Stack(
            children: [
              Container(
                padding: EdgeInsets.fromLTRB(18, 0, browserIconWidth + 8, 0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(cardModel.title, style: textTheme.titleLarge),
                    const SizedBox(height: 10),
                    InkWell(
                      onTap: () {
                        if (!cardModel.shouldShowPhoneCall) {
                          UrlLauncher.open(context, cardModel.url, shouldLaunchBrowserAlert: false);
                        }
                      },
                      splashFactory: NoSplash.splashFactory,
                      child: Row(children: [
                        Text(cardModel.subTitle, style: textTheme.bodySmall),
                        const SizedBox(width: 4),
                        if (!cardModel.shouldShowPhoneCall) const Icon(Icons.keyboard_arrow_right, size: 18, color: AppThemeData.secondaryColor)
                      ]),
                    ),
                    if (cardModel.shouldShowPhoneCall)
                      Container(
                          padding: const EdgeInsets.only(top: 20), child: Text(AppLocalizations.dictionary().noPostSalesService, style: ProjectStyle.build(textTheme.labelMedium, Colors.black87))),
                    if (cardModel.shouldShowPhoneCall)
                      Container(
                        padding: const EdgeInsets.only(top: 8),
                        child: InkWell(
                            onTap: () {
                              UrlLauncher.open(context, 'tel://4000-888-738', shouldLaunchBrowserAlert: false);
                            },
                            child: Text('4000-888-738', style: ProjectStyle.build(textTheme.titleSmall, colorScheme.primary))),
                      ),
                  ],
                ),
              ),
              if (cardModel.isCurrent)
                Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    padding: const EdgeInsets.fromLTRB(6, 4, 6, 4),
                    decoration: BoxDecoration(borderRadius: const BorderRadius.all(Radius.circular(4)), color: colorScheme.primary),
                    child: Text(
                      AppLocalizations.dictionary().currentWord,
                      style: ProjectStyle.build(textTheme.labelSmall, Colors.white),
                    ),
                  ),
                ),
              Align(
                alignment: Alignment.bottomRight,
                child: Image.asset(
                  'assets/images/browser_icon.png',
                  width: browserIconWidth,
                  height: 68,
                ),
              )
            ],
          )),
    );
  }
}

class _CardModel {
  String title;
  String subTitle;
  String url;
  bool isCurrent;
  bool shouldShowPhoneCall;

  _CardModel({required this.title, required this.subTitle, required this.url, this.isCurrent = true, this.shouldShowPhoneCall = false});
}
