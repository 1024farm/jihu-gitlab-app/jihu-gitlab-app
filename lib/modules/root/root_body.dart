import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/modules/root/destination_page.dart';
import 'package:jihu_gitlab_app/modules/root/model/root_model.dart';
import 'package:jihu_gitlab_app/modules/root/model/root_store.dart';
import 'package:provider/provider.dart';

class RootBody extends StatelessWidget {
  const RootBody({super.key, this.uniqueKey});

  final UniqueKey? uniqueKey;

  @override
  Widget build(BuildContext context) {
    return Consumer<RootStore>(builder: (context, store, child) {
      return store.type == DestinationType.tab
          ? IndexedStack(
              key: uniqueKey,
              index: store.selectedPageIndex,
              children: RootModel.pages(context),
            )
          : DestinationDispatcher(
              destination: store.destination!,
            );
    });
  }
}
