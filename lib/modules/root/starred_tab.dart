import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/starred/projects_starred_model.dart';
import 'package:jihu_gitlab_app/modules/root/pad_group_and_project_list_tile.dart';

class StarredTab extends StatefulWidget {
  const StarredTab({super.key});

  @override
  State<StarredTab> createState() => _StarredTabState();
}

class _StarredTabState extends State<StarredTab> {
  final ProjectsStarredModel _model = locator<ProjectsStarredModel>();

  @override
  void dispose() {
    _model.clear();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisSize: MainAxisSize.max, children: [const Divider(), _buildTitleView(), _buildStarredListView()]);
  }

  Widget _buildTitleView() {
    return Padding(
        padding: const EdgeInsets.only(left: 0, right: 12),
        child: Row(
          children: [
            Expanded(child: ListTile(title: Text(AppLocalizations.dictionary().projectsStarred, style: const TextStyle(fontSize: 20)))),
            Align(
                alignment: Alignment.centerRight,
                child: InkWell(
                  child: SvgPicture.asset('assets/images/refresh_exception.svg', colorFilter: const ColorFilter.mode(AppThemeData.secondaryColor, BlendMode.srcIn), width: 24, height: 24),
                  onTap: () => _model.loadData(),
                )),
          ],
        ));
  }

  Widget _buildStarredListView() {
    if (_model.loadState == LoadState.noItemState) {
      _model.config();
      _model.loadData();
    }
    return Padding(
        padding: const EdgeInsets.only(left: 12, right: 0, bottom: 16, top: 0),
        child: ValueListenableBuilder<List<GroupAndProject>>(
            valueListenable: _model.notifier,
            builder: (BuildContext context, List<GroupAndProject> data, Widget? child) {
              if (_model.loadState == LoadState.successState && data.isEmpty) {
                return Container(
                  alignment: Alignment.center,
                  padding: const EdgeInsets.only(top: 100),
                  child: Text(AppLocalizations.dictionary().noStar, style: const TextStyle(color: Color(0xFF87878C), fontSize: 14)),
                );
              }
              return ClipRRect(
                borderRadius: BorderRadius.circular(4.0),
                child: ListView.builder(
                  padding: EdgeInsets.zero,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: data.length,
                  shrinkWrap: true,
                  itemBuilder: (context, index) => PadGroupAndProjectListTile(
                    itemDataProvider: () => data[index],
                    starChanged: () {
                      _model.loadLocalDataAndNotify();
                    },
                  ),
                ),
              );
            }));
  }
}
