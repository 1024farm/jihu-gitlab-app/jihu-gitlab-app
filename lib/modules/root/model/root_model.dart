import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/layout/adaptive.dart';
import 'package:jihu_gitlab_app/core/themes/custom_icons.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';

enum PageType { todo, projects, faq }

enum DestinationType { tab, starred }

class Destination {
  const Destination({
    required this.type,
    required this.title,
    required this.icon,
  });

  final PageType type;
  final String title;
  final Icon icon;
}

class StarredDestination {
  final DestinationType type;
  final Object data;

  const StarredDestination({required this.type, required this.data});
}

class RootModel {
  static List<Destination> navigationDestinations(S dictionary, {BuildContext? context}) => <Destination>[
        Destination(type: PageType.todo, title: dictionary.toDos, icon: const Icon(CustomIcons.toDos)),
        Destination(type: PageType.projects, title: context != null && isDesktopLayout(context) ? dictionary.projectsGroups : dictionary.projects, icon: const Icon(CustomIcons.projects)),
        Destination(type: PageType.faq, title: dictionary.community, icon: const Icon(Icons.textsms_outlined))
      ];

  static List<Widget> pages(BuildContext context) => <Widget>[
        const ToDosPage(),
        isDesktopLayout(context) ? const ProjectsGroupsPage() : const ProjectsPage(),
        const CommunityPage(),
        const ResourcesPage(),
      ];
}
