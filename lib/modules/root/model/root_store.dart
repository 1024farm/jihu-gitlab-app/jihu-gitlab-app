import 'package:flutter/widgets.dart';
import 'package:jihu_gitlab_app/core/global_keys.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/modules/root/model/root_model.dart';

class RootStore with ChangeNotifier {
  static const _initialPageIndex = 1;
  static final RootStore _instance = RootStore._internal();

  RootStore._internal();

  factory RootStore() => _instance;

  int _selectedIndex = 0;

  Future<void> restore() async {
    _selectedIndex = await LocalStorage.get(LocalStorageKeys.navigationBarCurrentIndexKey, _initialPageIndex);
  }

  set selectedIndex(int index) {
    _selectedIndex = index;
    _selectedDestination2 = null;
    notifyListeners();
  }

  int get selectedIndex => _selectedIndex;

  StarredDestination? _selectedDestination2;

  int get selectedPageIndex => _selectedDestination2 != null ? 4 : _selectedIndex;

  set selectedDestination2(StarredDestination destination) {
    _selectedDestination2 = destination;
    notifyListeners();
  }

  StarredDestination? get destination => _selectedDestination2;

  DestinationType get type => _selectedDestination2 != null ? DestinationType.starred : DestinationType.tab;
}
