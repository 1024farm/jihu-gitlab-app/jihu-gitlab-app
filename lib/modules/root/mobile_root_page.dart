import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/global_keys.dart';
import 'package:jihu_gitlab_app/core/widgets/home/home_drawer.dart';
import 'package:jihu_gitlab_app/modules/root/model/root_model.dart';
import 'package:jihu_gitlab_app/modules/root/model/root_store.dart';
import 'package:jihu_gitlab_app/modules/root/root_body.dart';
import 'package:provider/provider.dart';

class MobileRootPage extends StatefulWidget {
  const MobileRootPage({
    required this.destinations,
    required this.onItemTapped,
    this.uniqueKey,
    super.key,
  });

  final UniqueKey? uniqueKey;
  final List<Destination> destinations;
  final void Function(int, PageType) onItemTapped;

  @override
  State<MobileRootPage> createState() => _MobileRootPageState();
}

class _MobileRootPageState extends State<MobileRootPage> {
  @override
  Widget build(BuildContext context) {
    return Consumer<RootStore>(builder: (context, store, child) {
      return Scaffold(
        key: GlobalKeys.rootScaffoldKey,
        drawer: const HomeDrawer(),
        body: RootBody(uniqueKey: widget.uniqueKey),
        bottomNavigationBar: BottomNavigationBar(
          key: GlobalKeys.rootBottomNavigationBarKey,
          items: widget.destinations.map((item) => BottomNavigationBarItem(icon: item.icon, label: item.title)).toList(),
          type: BottomNavigationBarType.fixed,
          fixedColor: Theme.of(context).primaryColor,
          currentIndex: store.selectedIndex,
          onTap: (index) {
            setState(() {
              widget.onItemTapped(index, widget.destinations[index].type);
            });
          },
        ),
      );
    });
  }
}
