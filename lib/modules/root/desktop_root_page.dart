import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/global_keys.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar/avatar.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/paste_type_url_navigation.dart';
import 'package:jihu_gitlab_app/core/widgets/user/user_info_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/root/model/root_model.dart';
import 'package:jihu_gitlab_app/modules/root/model/root_store.dart';
import 'package:jihu_gitlab_app/modules/root/root_body.dart';
import 'package:jihu_gitlab_app/modules/root/starred_tab.dart';
import 'package:provider/provider.dart';

class DesktopRootPage extends StatefulWidget {
  const DesktopRootPage({
    required this.extended,
    required this.destinations,
    required this.onItemTapped,
    this.uniqueKey,
    super.key,
  });

  final bool extended;
  final UniqueKey? uniqueKey;
  final List<Destination> destinations;
  final void Function(int, PageType) onItemTapped;

  @override
  State<DesktopRootPage> createState() => _DesktopRootPageState();
}

class _DesktopRootPageState extends State<DesktopRootPage> {
  static const double _sidebarMaxWidth = 300;
  double _sidebarCurrentWidth = 0;
  late Orientation _orientation = Orientation.portrait;
  final double _kAppbarHeight = 56;

  @override
  Widget build(BuildContext context) {
    return Consumer<RootStore>(builder: (context, store, child) {
      return OrientationBuilder(builder: (BuildContext context, Orientation orientation) {
        _onOrientationChanged(orientation);
        return _buildPage(orientation, store, context);
      });
    });
  }

  Scaffold _buildPage(Orientation orientation, RootStore store, BuildContext context) {
    return Scaffold(
      key: GlobalKeys.rootScaffoldKey,
      resizeToAvoidBottomInset: false,
      drawer: orientation == Orientation.portrait ? _buildPortraitSideBar(store) : null,
      body: Stack(
        children: [
          Row(
            children: [
              orientation == Orientation.landscape ? _buildLandscapeSidebar(store) : const SizedBox(),
              const VerticalDivider(thickness: 1, width: 1),
              Expanded(child: RootBody(uniqueKey: widget.uniqueKey)),
            ],
          ),
          Container(
            width: _sidebarMaxWidth,
            margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                InkWell(
                    onTap: _toggleSidebar,
                    child: Container(padding: const EdgeInsets.symmetric(horizontal: 16), height: _kAppbarHeight, child: SvgPicture.asset("assets/images/toggle_sidebar.svg", width: 28, height: 28))),
                if (_orientation == Orientation.landscape && _sidebarCurrentWidth == _sidebarMaxWidth) Expanded(child: Text(AppLocalizations.dictionary().appName)),
                if (_orientation == Orientation.landscape && _sidebarCurrentWidth == _sidebarMaxWidth && ConnectionProvider.authorized) _pasteAndGoButton()
              ],
            ),
          ),
        ],
      ),
    );
  }

  PasteTypeUrlNavigation _pasteAndGoButton() {
    return PasteTypeUrlNavigation(
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: SvgPicture.asset('assets/images/paste_and_go.svg', width: 20, height: 20, colorFilter: const ColorFilter.mode(AppThemeData.secondaryColor, BlendMode.srcIn)),
      ),
    );
  }

  void _onOrientationChanged(Orientation orientation) {
    if (_orientation != orientation) {
      _sidebarCurrentWidth = orientation == Orientation.landscape ? _sidebarMaxWidth : 0;
    }
    _orientation = orientation;
  }

  void _toggleSidebar() {
    if (_orientation == Orientation.portrait) {
      GlobalKeys.rootScaffoldKey.currentState?.openDrawer();
    } else {
      setState(() {
        _sidebarCurrentWidth = _sidebarCurrentWidth == 0 ? _sidebarMaxWidth : 0;
      });
    }
  }

  Widget _buildLandscapeSidebar(RootStore store) {
    return AnimatedSize(duration: const Duration(milliseconds: 500), child: _buildSidebar(store));
  }

  Widget _buildPortraitSideBar(RootStore store) {
    return Drawer(child: _buildSidebar(store));
  }

  // TODO Refactor
  Widget _buildSidebar(RootStore store) {
    return SizedBox(
      key: const Key("sidebar"),
      height: MediaQuery.of(context).size.height,
      width: _sidebarCurrentWidth,
      child: Stack(children: [_buildNavigationItemsView(store), _buildBottomUserInfoBox(context)]),
    );
  }

  Widget _buildNavigationItemsView(RootStore store) {
    return Container(
      height: MediaQuery.of(context).size.height,
      padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top + (_orientation == Orientation.landscape ? _kAppbarHeight : 0), bottom: 70),
      child: SingleChildScrollView(
          physics: const ScrollPhysics(),
          child: Column(
            children: [
              if (_orientation == Orientation.portrait)
                Row(children: [Expanded(child: ListTile(title: Text(AppLocalizations.dictionary().appName, style: const TextStyle(fontSize: 20)))), _pasteAndGoButton()]),
              const Divider(),
              ...widget.destinations.asMap().entries.map((entry) => ListTile(
                    visualDensity: const VisualDensity(horizontal: -2, vertical: -2),
                    minLeadingWidth: 0,
                    key: Key(entry.value.title),
                    enabled: true,
                    selected: store.selectedIndex == entry.key && store.destination == null,
                    leading: entry.value.icon,
                    title: Text(entry.value.title),
                    onTap: () {
                      setState(() {
                        widget.onItemTapped(entry.key, entry.value.type);
                      });
                    },
                  )),
              const StarredTab(),
            ],
          )),
    );
  }

  Widget _buildBottomUserInfoBox(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        height: 64,
        color: Colors.white,
        child: Row(
          children: [
            const SizedBox(width: 16),
            if (ConnectionProvider.authorized)
              SizedBox(
                height: 32,
                child: Avatar(
                  avatarUrl: ConnectionProvider.connection?.userInfo.avatarUrl ?? '',
                  size: 32,
                ),
              ),
            if (ConnectionProvider.authorized) const SizedBox(width: 10),
            ConnectionProvider.authorized
                ? Expanded(child: _buildNameWithAccountType())
                : Expanded(
                    child: Text(AppLocalizations.dictionary().connectServerFirstNoPoint,
                        style: const TextStyle(fontWeight: FontWeight.w400, fontSize: 14.0, color: Color(0xFF9E9E9E)), overflow: TextOverflow.ellipsis)),
            InkWell(
              onTap: () {
                double horizontalPadding = MediaQuery.of(context).size.width * 0.1;
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => Scaffold(
                      appBar: CommonAppBar(showLeading: true),
                      body: Container(
                        padding: EdgeInsets.fromLTRB(horizontalPadding, 0, horizontalPadding, 40.0),
                        child: const Center(child: UserInfoView()),
                      )),
                ));
              },
              child: Container(
                height: 64,
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: SvgPicture.asset('assets/images/settings.svg', height: 20, width: 20),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Column _buildNameWithAccountType() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('@${ConnectionProvider.connection!.userInfo.username}', overflow: TextOverflow.ellipsis, style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w600, color: Color(0xFF03162F))),
        const SizedBox(height: 4),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
          decoration: const BoxDecoration(color: Color(0xFFEAEAEA), borderRadius: BorderRadius.all(Radius.circular(4))),
          child: Text(ConnectionProvider.connection!.serverType, style: const TextStyle(color: AppThemeData.secondaryColor, fontSize: 14)),
        ),
      ],
    );
  }
}
