import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/star.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/root/model/root_model.dart';
import 'package:jihu_gitlab_app/modules/root/model/root_store.dart';
import 'package:provider/provider.dart';

class PadGroupAndProjectListTile extends StatefulWidget {
  final GroupAndProject? data;
  final ItemDataProvider? itemDataProvider;
  final VoidCallback? starChanged;

  const PadGroupAndProjectListTile({this.data, this.itemDataProvider, this.starChanged, Key? key}) : super(key: key);

  @override
  State<PadGroupAndProjectListTile> createState() => _PadGroupAndProjectListTileState();
}

class _PadGroupAndProjectListTileState extends State<PadGroupAndProjectListTile> {
  @override
  Widget build(BuildContext context) {
    var rootStore = Provider.of<RootStore>(context, listen: true);
    var data = widget.itemDataProvider!();
    return InkWell(
        onTap: () => _onTap(data, rootStore),
        child: Container(
          padding: const EdgeInsets.only(right: 10, top: 10, bottom: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SvgPicture.asset(data.iconWithPad, width: 28, height: 28),
              Expanded(
                flex: 1,
                child: Padding(
                    padding: const EdgeInsets.only(left: 8),
                    child: Text(
                      data.name,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(color: rootStore.destination != null && (rootStore.destination!.data as GroupAndProject).id == data.id ? Theme.of(context).colorScheme.primary : Colors.black),
                    )),
              )
            ],
          ),
        ));
  }

  void _onTap(GroupAndProject item, RootStore store) async {
    if (item.recommend) {
      ProjectProvider().changeProject(item.asProject, item.destinationHost).then((_) => store.selectedDestination2 = StarredDestination(type: DestinationType.starred, data: item));
    } else {
      ProjectProvider().changeProject(item.asProject, null).then((_) => store.selectedDestination2 = StarredDestination(type: DestinationType.starred, data: item));
    }
  }
}
