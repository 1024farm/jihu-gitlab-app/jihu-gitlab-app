import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';

class DestinationDispatcher extends StatefulWidget {
  final StarredDestination destination;

  const DestinationDispatcher({required this.destination, Key? key}) : super(key: key);

  @override
  State<DestinationDispatcher> createState() => _DestinationDispatcherState();
}

class _DestinationDispatcherState extends State<DestinationDispatcher> {
  int _currentTabIndex = 0;
  Map<String, dynamic>? selectedIssueParams;

  @override
  Widget build(BuildContext context) {
    var item = widget.destination.data as GroupAndProject;
    if (item.type == SubgroupItemType.group || item.type == SubgroupItemType.subgroup) {
      ProjectProvider().clear();
      return Row(
        children: [
          Expanded(
            child: SubgroupPage(
              arguments: <String, dynamic>{'groupId': item.iid, 'name': item.name, 'relativePath': item.relativePath, 'showLeading': false},
              onTabChange: (index) {
                setState(() => _currentTabIndex = index);
              },
              onSelectedIssueChange: (index, params) {
                setState(() => selectedIssueParams = params);
              },
            ),
          ),
          _currentTabIndex == 1 ? Expanded(child: IssueDetailsPage(arguments: selectedIssueParams ?? {})) : const SizedBox()
        ],
      );
    }
    final params = <String, dynamic>{
      'projectId': item.iid,
      'name': item.name,
      'relativePath': item.relativePath,
      "groupId": item.parentId,
      'showLeading': false,
    };
    return ProjectPage(key: Key(params.toString()), arguments: params);
  }
}
