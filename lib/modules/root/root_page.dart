import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/global_keys.dart';
import 'package:jihu_gitlab_app/core/layout/adaptive.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/plugins/install_apk_plugin.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/paste_type_url_navigation.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/privacy_policy/privacy_policy_page.dart';
import 'package:jihu_gitlab_app/modules/root/desktop_root_page.dart';
import 'package:jihu_gitlab_app/modules/root/mobile_root_page.dart';
import 'package:jihu_gitlab_app/modules/root/model/root_model.dart';
import 'package:jihu_gitlab_app/modules/root/model/root_store.dart';
import 'package:provider/provider.dart';
import 'package:updater/updater.dart';

class RootPage extends StatefulWidget {
  const RootPage({super.key});

  @override
  State<RootPage> createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  final UniqueKey _uniqueKey = UniqueKey();

  static const _privacyPolicyKey = "privacy-policy-agreed";
  dynamic version;
  late UpdaterController controller;
  late Updater updater;
  late String _androidBuildType;

  @override
  void initState() {
    super.initState();
    _androidBuildType = const String.fromEnvironment("ANDROID_BUILD_TYPE");
    if (_androidBuildType == 'debug') {
// coverage:ignore-start
      _initializeUpdater();
      _checkUpdate();
// coverage:ignore-end
    }
    bool agreedPrivacyPolicy = LocalStorage.get(_privacyPolicyKey, false);
    if (!agreedPrivacyPolicy) {
      Future.delayed(Duration.zero, _showPrivacyPolicyDialog);
    }
    parseMergeRequest();
  }

  Future<void> parseMergeRequest() async {
    var url = await ConnectionProvider.mergeRequestWebUrl;
    if (url.isEmpty) {
      return;
    }
    Future.delayed(Duration.zero, () {
      PasteTypeUrlNavigationModel().parseAndToward(url, context);
    });
  }

  @override
  void dispose() {
    if (_androidBuildType == 'debug') {
      controller.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(key: GlobalKeys.rootAppKey, resizeToAvoidBottomInset: false, body: Consumer<ConnectionProvider>(builder: (context, _, child) => _buildPage(context)));
  }

  Widget _buildPage(BuildContext context) {
    var navigationDestinations = RootModel.navigationDestinations(AppLocalizations.dictionary(), context: context);
    if (isDesktopLayout(context)) {
      return DesktopRootPage(
        uniqueKey: _uniqueKey,
        extended: true,
        destinations: navigationDestinations,
        onItemTapped: _onDestinationSelected,
      );
    }
    return MobileRootPage(
      uniqueKey: _uniqueKey,
      destinations: navigationDestinations,
      onItemTapped: _onDestinationSelected,
    );
  }

  void _onDestinationSelected(int index, PageType destination) {
    LocalStorage.save(LocalStorageKeys.navigationBarCurrentIndexKey, index);
    Provider.of<RootStore>(context, listen: false).selectedIndex = index;
  }

// coverage:ignore-start
  void _initializeUpdater() {
    controller = UpdaterController(
      listener: (UpdateStatus status) async {
        if (status == UpdateStatus.Completed) {
          await InstallApkPlugin().install();
        }
        debugPrint('Listener: $status');
      },
      onChecked: (bool isAvailable) {
        debugPrint('initializeUpdater onChecked: $isAvailable');
      },
      progress: (current, total) {
        // debugPrint('Progress: $current -- $total');
      },
      onError: (status) {
        debugPrint('Error: $status');
      },
    );

    updater = Updater(
      context: context,
      delay: const Duration(milliseconds: 300),
      url: 'https://jihulab.com/api/v4/projects/59893/packages/generic/jihu-gitlab-app/updater/updater.json',
      titleText: 'Stay with time',
      // backgroundDownload: false,
      allowSkip: true,
      contentText: 'Update your app to the latest version to enjoy new feature.',
      callBack: (UpdateModel model) {
        debugPrint(model.versionName);
        debugPrint(model.versionCode.toString());
        debugPrint(model.contentText);
        debugPrint(model.downloadUrl);
      },
      enableResume: true,
      controller: controller,
    );
  }

  void _checkUpdate() async {
    bool isAvailable = await updater.check();
    debugPrint('checkUpdate: $isAvailable');
  }

  void _showPrivacyPolicyDialog() {
    showCupertinoDialog(
        context: context,
        builder: (context) {
          return WillPopScope(
            onWillPop: () async => false,
            child: CupertinoAlertDialog(
              title: Text(AppLocalizations.dictionary().privacyTitle),
              content: Container(
                margin: const EdgeInsets.only(top: 12),
                child: Text.rich(
                  TextSpan(children: [
                    TextSpan(text: AppLocalizations.dictionary().privacyTop),
                    TextSpan(
                        text: AppLocalizations.dictionary().privacyLink,
                        style: TextStyle(color: Theme.of(context).primaryColor),
                        recognizer: TapGestureRecognizer()..onTap = () => Navigator.pushNamed(context, PrivacyPolicyPage.routeName, arguments: {})),
                    TextSpan(text: AppLocalizations.dictionary().privacyContent)
                  ]),
                  textAlign: TextAlign.left,
                ),
              ),
              actions: [
                TextButton(
                  onPressed: () => exit(0),
                  child: Text(AppLocalizations.dictionary().disagree, style: const TextStyle(color: AppThemeData.secondaryColor, fontSize: 14)),
                ),
                TextButton(
                  onPressed: () {
                    LocalStorage.save(_privacyPolicyKey, true);
                    Navigator.pop(context);
                  },
                  child: Text(AppLocalizations.dictionary().agree, style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 14)),
                )
              ],
            ),
          );
        });
  }
// coverage:ignore-end
}
