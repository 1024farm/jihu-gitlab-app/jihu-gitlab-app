import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/domain/selector_project.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/paged_state.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/index.dart';
import 'package:jihu_gitlab_app/modules/projects/projects_selector/projects_selector_model.dart';

class ProjectsSelector extends StatefulWidget {
  final String fullPath;
  final int groupId;

  const ProjectsSelector({required this.fullPath, required this.groupId, super.key});

  @override
  State<StatefulWidget> createState() => _ProjectsSelectorState();
}

class _ProjectsSelectorState extends PagedState<ProjectsSelector> {
  final ProjectsSelectorModel _model = ProjectsSelectorModel();

  @override
  Future<void> onRefresh() async {
    super.onRefresh().then((value) {
      if (_model.onlyOneProject) {
        _gotoIssueCreationPage(_model.projects[0]);
      }
    });
  }

  @override
  AppBar? buildAppBar() {
    return CommonAppBar(showLeading: true, title: Text(AppLocalizations.dictionary().projectsIssueCreationSelectProject));
  }

  @override
  Widget? buildListView() {
    return SafeArea(
        child: Padding(
            padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(4.0),
              child: ListView.separated(
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () => _gotoIssueCreationPage(_model.projects[index]),
                    child: Container(
                      decoration: const BoxDecoration(
                        color: Colors.white,
                      ),
                      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 16),
                      child: Row(
                        children: <Widget>[
                          SvgPicture.asset("assets/images/projects.svg", width: 16, height: 16),
                          const SizedBox(width: 12),
                          Expanded(
                              child: Text(
                            _model.projects[index].name,
                            style: const TextStyle(fontWeight: FontWeight.w400, fontSize: 16, color: Color(0XFF03162F)),
                          )),
                        ],
                      ),
                    ),
                  );
                },
                separatorBuilder: (context, index) => const Divider(thickness: 0.5, height: 0, indent: 12.0, endIndent: 12.0, color: Color(0xFFDDDDDD)),
                itemCount: _model.projects.length,
              ),
            )));
  }

  @override
  Function hasNextPage() {
    return () => false;
  }

  @override
  Future<bool> Function() modelLoadMore() {
    return () => Future.value(true);
  }

  @override
  Future<bool> Function() modelRefresh() {
    return () async => await _model.refresh(widget.fullPath);
  }

  @override
  Function isErrorState() {
    return () => _model.loadState == LoadState.errorState;
  }

  void _gotoIssueCreationPage(SelectorProject project) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => IssueCreationPage(projectId: project.id, from: 'group', groupId: widget.groupId)));
  }
}
