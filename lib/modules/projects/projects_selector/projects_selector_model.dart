import 'package:jihu_gitlab_app/core/domain/selector_project.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';

class ProjectsSelectorModel {
  LoadState _loadState = LoadState.noItemState;
  List<SelectorProject> _projects = [];

  Future<bool> refresh(String fullPath) async {
    try {
      _projects = await _get(fullPath);
      _loadState = LoadState.successState;
      return Future.value(true);
    } catch (e) {
      _loadState = LoadState.errorState;
      return Future.value(false);
    }
  }

  Future<List<SelectorProject>> _get(String fullPath) async {
    var params = [
      {
        "operationName": "searchProjects",
        "variables": {"fullPath": fullPath, "search": ""},
        "query":
            "query searchProjects(\$fullPath: ID!, \$search: String) {\n  group(fullPath: \$fullPath) {\n    id\n    projects(search: \$search, includeSubgroups: true) {\n      nodes {\n        id\n        issuesEnabled\n        name\n        nameWithNamespace\n        webUrl\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n}\n"
      }
    ];
    var response = await HttpClient.instance().post<List<dynamic>>(Api.graphql(), params);
    var resp = response.body().first;
    List<dynamic> items = resp['data']['group']['projects']['nodes'];
    return items.map((e) => SelectorProject.fromJson(e)).toList();
  }

  List<SelectorProject> get projects => _projects;

  LoadState get loadState {
    return _loadState;
  }

  bool get onlyOneProject => _projects.length == 1;
}
