import 'package:jihu_gitlab_app/core/domain/project.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/subgroups/subgroup_list_model.dart';

class GroupAndProject {
  int? id;
  int iid;
  String name;
  SubgroupItemType? type;
  String? relativePath;
  int? parentId;
  bool starred;
  String? destinationHost;
  bool recommend;

  GroupAndProject(this.id, this.iid, this.name, this.type, this.relativePath, this.parentId, {required this.starred, this.recommend = false});

  String get icon => type == SubgroupItemType.project ? 'assets/images/projects.svg' : 'assets/images/groups.svg';

  String get iconWithPad => type == SubgroupItemType.project ? 'assets/images/projects_star.svg' : 'assets/images/groups_star.svg';

  Project get asProject => Project(iid, name, relativePath ?? '', '', '');
}

class GroupAndProjectEntity {
  static const tableName = "groups_and_projects";

  int? id;
  final int userId;
  final int iid;
  String name;
  final String? type;
  String? relativePath;
  final int? parentId;
  int? starred;
  int version;
  int lastActivityAt;
  int starredAt;

  GroupAndProjectEntity._internal(this.id, this.userId, this.iid, this.name, this.type, this.relativePath, this.parentId, this.version, this.starred, this.lastActivityAt, this.starredAt);

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "user_id": userId,
      "iid": iid,
      "name": name,
      "type": type,
      "relative_path": relativePath,
      "parent_id": parentId,
      "version": version,
      "starred": starred,
      "last_activity_at": lastActivityAt,
      "starred_at": starredAt
    };
  }

  static GroupAndProjectEntity createGroupEntity(Map<String, dynamic> json, int userId, int parentId, int version) {
    return GroupAndProjectEntity._internal(null, userId, json['id'], json['name'], SubgroupItemType.group.name, json['full_path'] ?? '', parentId, version, null, parseDateTime(json['created_at']), 0);
  }

  static GroupAndProjectEntity createSubgroupEntity(Map<String, dynamic> json, int userId, int parentId, int version) {
    return GroupAndProjectEntity._internal(
        null, userId, json['id'], json['name'], SubgroupItemType.subgroup.name, json['full_path'] ?? '', parentId, version, null, parseDateTime(json['created_at']), 0);
  }

  static GroupAndProjectEntity createProjectEntity(Map<String, dynamic> json, int userId, int parentId, int version) {
    return GroupAndProjectEntity._internal(
        null, userId, json['id'], json['name'], SubgroupItemType.project.name, json['path_with_namespace'] ?? '', parentId, version, null, parseDateTime(json['last_activity_at']), 0);
  }

  static GroupAndProjectEntity createStarredProjectEntity(Map<String, dynamic> json, int userId, int version) {
    return GroupAndProjectEntity._internal(null, userId, json['id'], json['name'], SubgroupItemType.project.name, json['path_with_namespace'] ?? '', json['namespace']['id'], version, 1,
        parseDateTime(json['last_activity_at']), parseDateTime(json['last_activity_at']));
  }

  static GroupAndProjectEntity restore(Map<String, dynamic> map) {
    return GroupAndProjectEntity._internal(map['id'], map['user_id'], map['iid'], map['name'], map['type'], map['relative_path'], map['parent_id'], map['version'], map['starred'] ?? 0,
        map['last_activity_at'] ?? 0, map['starred_at'] ?? 0);
  }

  static int parseDateTime(String? dateTimeString) {
    if (dateTimeString == null || dateTimeString.isEmpty) {
      return 0;
    }
    return DateTime.parse(dateTimeString).millisecondsSinceEpoch;
  }

  GroupAndProject asDomain() {
    return GroupAndProject(id, iid, name, SubgroupItemType.values.where((e) => e.name == type).first, relativePath, parentId, starred: starred == 1);
  }

  void update(GroupAndProjectEntity newValue) {
    name = newValue.name;
    relativePath = newValue.relativePath;
    version = newValue.version;
    lastActivityAt = newValue.lastActivityAt;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is GroupAndProjectEntity && runtimeType == other.runtimeType && userId == other.userId && iid == other.iid && parentId == other.parentId;

  @override
  int get hashCode => userId.hashCode ^ iid.hashCode ^ parentId.hashCode;

  static String get createTableSql => """ 
    create table $tableName(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        user_id INTEGER NOT NULL,
        iid INTEGER NOT NULL,
        name TEXT ,
        type TEXT ,
        relative_path TEXT,
        parent_id int,
        version INTEGER
    );    
  """;

  static String get addStarredColumn => """ALTER TABLE $tableName ADD COLUMN starred INTEGER;""";

  static String get addLastActivityAtColumn => """ALTER TABLE $tableName ADD COLUMN last_activity_at INTEGER;""";

  static String get addStarredAtColumn => """ALTER TABLE $tableName ADD COLUMN starred_at INTEGER;""";
}
