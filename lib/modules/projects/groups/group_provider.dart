import 'package:jihu_gitlab_app/core/domain/global_time.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/synchronizer.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/subgroup_and_project_provider.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/subgroups/subgroup_list_model.dart';

class GroupProvider extends SubgroupAndProjectProvider {
  GroupProvider({required super.userId, required super.parentId});

  @override
  Future<bool> syncFromRemote() async {
    int version = GlobalTime.now().millisecondsSinceEpoch;
    List<bool> results = await Future.wait([_syncGroups(version)]);
    return Future(() => results.every((e) => e));
  }

  Future<bool> _syncGroups(int version) async {
    return Synchronizer<dynamic>(
        url: Api.join('/groups?top_level_only=true'),
        dataProcessor: (data, page, pageSize) async {
          List<GroupAndProjectEntity> subgroupEntities = (data as List).map((e) => GroupAndProjectEntity.createGroupEntity(e, ConnectionProvider.connectionId!, 0, version)).toList(growable: false);
          await save(subgroupEntities, SubgroupItemType.group, version);
          return subgroupEntities.length >= pageSize;
        }).run();
  }
}
