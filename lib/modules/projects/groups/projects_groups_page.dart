import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/layout/adaptive.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/widgets/home/home_scaffold.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/core/widgets/unauthorized_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project_list_tile.dart';
import 'package:jihu_gitlab_app/modules/projects/projects.dart';
import 'package:jihu_gitlab_app/modules/projects/projects_groups_model.dart';
import 'package:provider/provider.dart';

class ProjectsGroupsPage extends StatefulWidget {
  static const routeName = "ProjectsGroupsPage";

  const ProjectsGroupsPage({super.key});

  @override
  State<ProjectsGroupsPage> createState() => _ProjectsGroupsPageState();
}

class _ProjectsGroupsPageState extends State<ProjectsGroupsPage> {
  final ProjectsGroupsModel _model = locator<ProjectsGroupsModel>();

  @override
  void initState() {
    super.initState();
    _model.initListener();
  }

  @override
  void dispose() {
    _model.clear();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return isDesktopLayout(context) ? _buildPadPage(context) : _buildBody();
  }

  HomeScaffold _buildPadPage(BuildContext context) {
    return HomeScaffold.withAppBar(
        params: HomeAppBarRequiredParams(
          context: context,
          title: AppLocalizations.dictionary().projectsGroups,
        ),
        body: _buildBody());
  }

  Widget _buildBody() {
    return SafeArea(
      child: Consumer<ConnectionProvider>(builder: (context, userProvider, child) {
        if (!ConnectionProvider.authorized) {
          _model.clear();
          return const UnauthorizedView();
        } else if (_model.loadState == LoadState.noItemState) {
          _model.init();
          _model.loadData();
        }
        return Padding(padding: const EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 0), child: groupList());
      }),
    );
  }

  Widget groupList() {
    return ValueListenableBuilder<List<GroupAndProject>>(
      valueListenable: _model.notifier,
      builder: (BuildContext context, List<GroupAndProject> data, Widget? child) {
        if (_model.loadState == LoadState.successState && data.isEmpty) {
          return TipsView(icon: 'assets/images/no_groups.png', onRefresh: () => _model.loadData());
        }
        return ClipRRect(
          borderRadius: BorderRadius.circular(4.0),
          child: ListView.separated(
              itemCount: data.length,
              separatorBuilder: (context, index) => const Divider(height: .5, indent: 12.0, endIndent: 12.0, color: Color(0xFFDDDDDD)),
              itemBuilder: (context, index) => GroupAndProjectListTile(itemDataProvider: () => data[index])),
        );
      },
    );
  }
}
