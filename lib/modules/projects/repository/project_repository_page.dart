import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_view.dart';
import 'package:jihu_gitlab_app/core/widgets/round_rect_container.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/branch.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/project_repository_model.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/readme_view.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_tree_item.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_tree_item_view.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_tree_page.dart';

class ProjectRepositoryPage extends StatefulWidget {
  const ProjectRepositoryPage({required this.fullPath, required this.projectName, Key? key}) : super(key: key);
  final String fullPath;
  final String projectName;

  @override
  State<ProjectRepositoryPage> createState() => _ProjectRepositoryPageState();
}

class _ProjectRepositoryPageState extends State<ProjectRepositoryPage> with AutomaticKeepAliveClientMixin {
  static const double _bodyHorizontalPadding = 16;
  final ProjectRepositoryModel _model = ProjectRepositoryModel();
  final GlobalKey _branchSelectorKey = GlobalKey();
  bool _isLoadingBySwitchBranch = false;
  bool _isLoadingRepository = false;

  Future<void> _load() async {
    toggleRepositoryLoading(loading: true);
    _model.checkRepositoryIsEmpty().then((value) {
      if (_model.repositoryIsEmpty) {
        toggleRepositoryLoading(loading: false);
      } else {
        _getTreesAndReadme().then((value) => toggleRepositoryLoading(loading: false));
      }
    });
  }

  Future<void> _getTreesAndReadme() async {
    await Future.wait([_model.getFirst5TreeItems(), _model.loadReadme()]);
  }

  void toggleRepositoryLoading({required bool loading}) {
    if (_isLoadingRepository != loading) {
      setState(() => _isLoadingRepository = loading);
    }
  }

  @override
  void initState() {
    _model.init(widget.fullPath, widget.projectName);
    _load();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    if (_isLoadingRepository) return const LoadingView();
    if (_model.repositoryIsEmpty) return TipsView(onRefresh: _load);
    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: _bodyHorizontalPadding),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildBranchSelector(),
            _isLoadingBySwitchBranch
                ? const LoadingView()
                : Column(children: [
                    _buildRepositoryTree(),
                    const SizedBox(height: 24),
                    ReadmeView(model: _model.readMeModel),
                  ]),
          ],
        ),
      ),
    );
  }

  Widget _buildBranchSelector() {
    return InkWell(
      key: _branchSelectorKey,
      onTap: _onBranchSelectorTapped,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
        decoration: const BoxDecoration(color: Color(0xFFEAEAEA), borderRadius: BorderRadius.all(Radius.circular(4))),
        margin: const EdgeInsets.only(bottom: 8),
        constraints: const BoxConstraints(minHeight: 32),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(
                child:
                    Text(_model.selectedBranchName ?? '', maxLines: 1, overflow: TextOverflow.ellipsis, style: const TextStyle(color: Color(0xFF03162F), fontSize: 14, fontWeight: FontWeight.w500))),
            ValueListenableBuilder<bool>(
                valueListenable: _model.branchLoadingStateNotifier,
                builder: (context, isLoadingBranches, child) =>
                    isLoadingBranches ? const SizedBox(height: 16, width: 16, child: CircularProgressIndicator(strokeWidth: 2)) : const Icon(Icons.arrow_drop_down, size: 16))
          ],
        ),
      ),
    );
  }

  void _onBranchSelectorTapped() async {
    double maxWidth = MediaQuery.of(context).size.width - _bodyHorizontalPadding * 2;
    double maxHeight = MediaQuery.of(context).size.height / 2;
    RenderBox? renderObject = _branchSelectorKey.currentContext?.findRenderObject() as RenderBox?;
    Offset offset = renderObject?.localToGlobal(Offset.zero) ?? const Offset(_bodyHorizontalPadding, 0);
    Size size = renderObject?.size ?? Size(maxWidth, 0);
    RelativeRect position = RelativeRect.fromLTRB(offset.dx, offset.dy + size.height, offset.dx, 0);
    debugPrint(offset.toString());
    _model.getBranches().then((branches) async {
      var result = await showMenu<Branch>(
        context: context,
        constraints: BoxConstraints(maxWidth: size.width, maxHeight: maxHeight),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
        position: position,
        items: _buildPopupMenuEntries(branches, maxWidth),
      );
      if (result != null && result.name != _model.selectedBranchName) {
        setState(() {
          _model.changeSelectedBranch(result);
          _isLoadingBySwitchBranch = true;
        });
        await _getTreesAndReadme();
        setState(() => _isLoadingBySwitchBranch = false);
      }
    });
  }

  List<PopupMenuEntry<Branch>> _buildPopupMenuEntries(List<Branch> branches, double width) {
    List<PopupMenuEntry<Branch>> items = [];
    for (int i = 0; i < branches.length; i++) {
      var branch = branches[i];
      items.add(PopupMenuItem(
          value: branch,
          padding: const EdgeInsets.all(0),
          height: 0,
          textStyle: const TextStyle(color: Colors.blue),
          labelTextStyle: MaterialStateProperty.resolveWith<TextStyle?>((states) => const TextStyle(color: Colors.purple)),
          child: Container(
            width: width,
            padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
            color: _model.selectedBranchName == branch.name ? const Color(0xFFFFF7F3) : Colors.white,
            child: Text(branch.name, style: const TextStyle(color: Color(0xFF03162F), fontSize: 14)),
          )));
      items.add(const PopupMenuDivider(height: 1));
    }
    if (items.isNotEmpty) items.removeLast();
    return items;
  }

  Widget _buildRepositoryTree() {
    return RoundRectContainer(
        padding: const EdgeInsets.symmetric(horizontal: 12),
        child: _model.items.isEmpty
            ? const SizedBox()
            : Column(children: [
                ..._buildTreeItemViews(_model.items),
                if (_model.showTheViewAllButton)
                  InkWell(
                      onTap: _viewAll,
                      child: Container(
                          padding: const EdgeInsets.all(16),
                          child: Text(AppLocalizations.dictionary().viewAll, style: TextStyle(fontSize: 14, color: Theme.of(context).primaryColor, fontWeight: FontWeight.normal))))
              ]));
  }

  List<Widget> _buildTreeItemViews(List<RepositoryTreeItem> items) {
    List<Widget> views = [];
    for (var item in items) {
      views.add(RepositoryTreeItemView(fullPath: _model.fullPath, data: item, ref: _model.selectedBranchName));
      views.add(const Divider(color: Color(0xFFEAEAEA), height: 1, thickness: 1));
    }
    if (!_model.showTheViewAllButton) views.removeLast();
    return views;
  }

  void _viewAll() {
    Navigator.push(context, MaterialPageRoute(builder: (context) => RepositoryTreePage(fullPath: _model.fullPath, ref: _model.selectedBranchName, pageTitle: _model.projectName)));
  }

  @override
  bool get wantKeepAlive => true;
}
