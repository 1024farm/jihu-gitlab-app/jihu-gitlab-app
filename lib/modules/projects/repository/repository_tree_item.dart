class RepositoryTreeItem {
  final String id;
  final String sha;
  final String name;
  final String flatPath;
  final String type;
  final String webUrl;
  final String path;

  RepositoryTreeItem._(this.id, this.sha, this.name, this.flatPath, this.type, this.webUrl, this.path);

  factory RepositoryTreeItem.fromGraphQLJson(Map<String, dynamic> json) {
    return RepositoryTreeItem._(json["id"] ?? '', json["sha"] ?? '', json["name"] ?? '', json["flatPath"] ?? '', json["type"] ?? '', json["webUrl"] ?? '', json["path"] ?? '');
  }

  bool get isTree => type == "tree";

  String get displayName => flatPath.replaceFirst(path, name);
}
