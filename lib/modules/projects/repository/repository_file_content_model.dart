import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_file_content_fetcher.dart';

class RepositoryFileContentModel {
  Lines? _lines;
  late String _path;
  late String _name;
  late String _webUrl;
  String? _ref;
  late RepositoryFileContentFetcher _fileContentFetcher;
  late ValueNotifier<LoadState> _notifier;
  LoadState _loadState = LoadState.loadingState;
  final TextPainter _textPainter = TextPainter(textDirection: TextDirection.ltr, maxLines: 1, textScaleFactor: 1);
  final TextStyle contentTextStyle = const TextStyle(color: Color(0xFF1A1B36), fontSize: 14, fontWeight: FontWeight.normal);

  void init(String fullPath, String path, String name, String webUrl, String? ref) {
    _path = path;
    _name = name;
    _ref = ref;
    _webUrl = webUrl;
    _fileContentFetcher = RepositoryFileContentFetcher(fullPath);
    _notifier = ValueNotifier(_loadState);
    getFileContent();
  }

  Future<void> getFileContent() async {
    _loadState = LoadState.loadingState;
    try {
      FileContent? content = await _fileContentFetcher.fetch(path: _path, ref: _ref);
      _lines = content != null ? Lines.from(content, _textPainter, contentTextStyle) : null;
      _loadState = LoadState.successState;
    } catch (e) {
      _loadState = LoadState.errorState;
    }
    _notifier.value = _loadState;
  }

  ValueNotifier<LoadState> get notifier => _notifier;

  String get name => _name;

  Lines? get lines => _lines;

  String get webUrl => _webUrl;
}

class Line {
  int lineNumber;
  String text;
  Size lineSize;

  Line(this.lineNumber, this.text, this.lineSize);
}

class Lines {
  List<Line> data;
  FileContent content;
  double maxTextWidth;
  double maxLineHeight;
  double lineNumberWidth;

  Lines(this.content, this.data, this.lineNumberWidth)
      : maxTextWidth = data.map((e) => e.lineSize.width).reduce(max),
        maxLineHeight = data.map((e) => e.lineSize.height).reduce(max);

  factory Lines.from(FileContent content, TextPainter textPainter, TextStyle textStyle) {
    List<Line> lines = [];
    var texts = content.rawText.split('\n');
    for (int i = 0; i < texts.length; i++) {
      var text = texts[i];
      Size lineSize = Size.zero;
      if (text.isNotEmpty) {
        textPainter.text = TextSpan(text: text, style: textStyle);
        textPainter.layout();
        lineSize = textPainter.size;
      }
      lines.add(Line(i + 1, text, lineSize));
    }
    textPainter.text = TextSpan(text: lines.length.toString(), style: textStyle);
    textPainter.layout();
    double lineNumberWidth = textPainter.size.width;
    return Lines(content, lines, lineNumberWidth);
  }

  bool get isTooLarge => content.tooLarge;

  bool get isTextFile => content.fileType == "text";
}
