import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter/cupertino.dart';
import 'package:jihu_gitlab_app/core/graph_ql_page_info.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_tree_data_fetcher.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_tree_item.dart';

class RepositoryTreeModel {
  late String _fullPath;
  final List<RepositoryTreeItem> _repositoryTreeItems = [];
  LoadState _loadState = LoadState.successState;
  GraphQLPageInfo _pageInfo = GraphQLPageInfo.init();
  late ValueNotifier<List<RepositoryTreeItem>> _notifier;
  String? _flatPath;
  String? _ref;
  String? _pageTitle;
  late RepositoryTreeDataFetcher _dataFetcher;
  final EasyRefreshController _refreshController = EasyRefreshController(controlFinishLoad: true, controlFinishRefresh: true);
  bool isLoading = true;

  void init(String fullPath, String? flatPath, String? ref, String? pageTitle) {
    _fullPath = fullPath;
    _flatPath = flatPath;
    _ref = ref;
    _pageTitle = pageTitle;
    _notifier = ValueNotifier(_repositoryTreeItems);
    _dataFetcher = RepositoryTreeDataFetcher(_fullPath);
    refresh();
  }

  Future<void> refresh() async {
    try {
      _pageInfo.reset();
      _repositoryTreeItems.clear();
      var repositoryPaginatedTree = await _dataFetcher.fetch(nextPageCursor: _pageInfo.endCursor, flatPath: _flatPath, ref: _ref);
      _pageInfo = repositoryPaginatedTree.pageInfo;
      _repositoryTreeItems.addAll(repositoryPaginatedTree.items);
      _loadState = LoadState.successState;
      _refreshController.finishRefresh(IndicatorResult.success);
    } catch (e) {
      debugPrint(e.toString());
      _loadState = LoadState.errorState;
      _refreshController.finishRefresh(IndicatorResult.fail);
    } finally {
      isLoading = false;
      _notifier.value = List.of(_repositoryTreeItems);
    }
  }

  Future<void> loadMore() async {
    if (_pageInfo.hasNextPage) {
      try {
        RepositoryPaginatedTree repositoryPaginatedTree = await _dataFetcher.fetch(nextPageCursor: _pageInfo.endCursor, flatPath: _flatPath, ref: _ref);
        _pageInfo = repositoryPaginatedTree.pageInfo;
        _repositoryTreeItems.addAll(repositoryPaginatedTree.items);
        _loadState = LoadState.successState;
        _notifier.value = List.of(_repositoryTreeItems);
        _refreshController.finishLoad(_pageInfo.hasNextPage ? IndicatorResult.success : IndicatorResult.noMore);
      } catch (_) {}
    } else {
      _refreshController.finishLoad(IndicatorResult.noMore);
    }
  }

  ValueNotifier<List<RepositoryTreeItem>> get notifier => _notifier;

  String get fullPath => _fullPath;

  String get pageTitle => _pageTitle ?? "";

  EasyRefreshController get refreshController => _refreshController;

  LoadState get loadState => _loadState;

  String? get ref => _ref;
}
