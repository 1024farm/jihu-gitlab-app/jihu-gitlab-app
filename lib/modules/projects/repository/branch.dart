class Branch {
  final String name;

  Branch._(this.name);

  factory Branch.fromJson(Map<String, dynamic> json) {
    return Branch._(json['name'] ?? '');
  }
}
