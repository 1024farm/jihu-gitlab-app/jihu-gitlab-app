import 'package:jihu_gitlab_app/core/graph_ql_page_info.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_tree_item.dart';

class RepositoryPaginatedTree {
  final GraphQLPageInfo pageInfo;
  final List<RepositoryTreeItem> items;

  RepositoryPaginatedTree(this.pageInfo, this.items);

  factory RepositoryPaginatedTree.fromJson(Map<String, dynamic> json) {
    var paginatedTree = json['data']?['project']?['repository']?['paginatedTree'] ?? {};
    Map<String, dynamic> pageInfo = paginatedTree?['pageInfo'] ?? {};
    List<RepositoryTreeItem> items = [];
    List<dynamic> nodes = paginatedTree?['nodes'] ?? [];
    if (nodes.isNotEmpty) {
      List<RepositoryTreeItem> trees = ((nodes.first["trees"]?["nodes"] ?? []) as List).map((e) => RepositoryTreeItem.fromGraphQLJson(e)).toList();
      List<RepositoryTreeItem> blobs = ((nodes.first["blobs"]?["nodes"] ?? []) as List).map((e) => RepositoryTreeItem.fromGraphQLJson(e)).toList();
      items.addAll(trees);
      items.addAll(blobs);
    }
    return RepositoryPaginatedTree(GraphQLPageInfo.fromJson(pageInfo), items);
  }
}

class RepositoryTreeDataFetcher {
  final String fullPath;

  RepositoryTreeDataFetcher(this.fullPath);

  Future<RepositoryPaginatedTree> fetch({String nextPageCursor = '', String? flatPath, String? ref}) async {
    var body = (await ProjectRequestSender.instance().post(Api.graphql(), buildRequestBodyOfRepositoryTree(fullPath, nextPageCursor, path: flatPath, ref: ref))).body();
    if (body["errors"] != null) throw Exception(body["errors"].toString());
    return RepositoryPaginatedTree.fromJson(body);
  }
}

Map<String, dynamic> buildRequestBodyOfRepositoryTree(String fullPath, String nextPageCursor, {String? path, String? ref}) {
  return {
    "variables": {"projectPath": fullPath, "path": path, "ref": ref, "nextPageCursor": nextPageCursor},
    "query": """
              fragment PageInfo on PageInfo {
                hasNextPage
                hasPreviousPage
                startCursor
                endCursor
              }

              fragment TreeEntry on Entry {
                id
                sha
                name
                flatPath
                type
                path
              }

              query getPaginatedTree(\$projectPath: ID!, \$path: String, \$ref: String, \$nextPageCursor:String) {
                project(fullPath: \$projectPath) {
                  id
                  name
                  repository {
                    paginatedTree(path: \$path, ref: \$ref, after: \$nextPageCursor){
                      nodes{
                        trees{
                          nodes{
                            ...TreeEntry
                            webUrl
                          }
                        }
                        blobs{
                          nodes{
                            ...TreeEntry
                            webUrl
                          }
                        }
                      }
                      pageInfo{
                        ...PageInfo
                      }
                    }
                  }
                }
              }
              """
  };
}
