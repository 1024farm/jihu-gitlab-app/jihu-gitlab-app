import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/browser_launcher.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/widgets/app_footer.dart';
import 'package:jihu_gitlab_app/core/widgets/app_header.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/http_fail_view.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_view.dart';
import 'package:jihu_gitlab_app/core/widgets/round_rect_container.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_tree_item.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_tree_item_view.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_tree_model.dart';

class RepositoryTreePage extends StatefulWidget {
  const RepositoryTreePage({required this.fullPath, this.flatPath, this.ref, this.pageTitle, Key? key}) : super(key: key);
  final String fullPath;
  final String? flatPath;
  final String? ref;
  final String? pageTitle;

  @override
  State<RepositoryTreePage> createState() => _RepositoryTreePageState();
}

class _RepositoryTreePageState extends State<RepositoryTreePage> {
  final RepositoryTreeModel _model = RepositoryTreeModel();

  @override
  void initState() {
    _model.init(widget.fullPath, widget.flatPath, widget.ref, widget.pageTitle);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CommonAppBar(showLeading: true, title: Text(_model.pageTitle)),
      body: SafeArea(
        child: EasyRefresh(
          controller: _model.refreshController,
          onRefresh: _model.refresh,
          onLoad: _model.loadMore,
          footer: AppFooter(),
          header: AppHeader(),
          child: ValueListenableBuilder<List<RepositoryTreeItem>>(
            valueListenable: _model.notifier,
            builder: (context, items, child) {
              if (_model.isLoading) return const LoadingView();
              if (_model.loadState == LoadState.errorState) return HttpFailView(onRefresh: _model.refresh);
              if (_model.loadState == LoadState.successState && items.isEmpty) return TipsView(onRefresh: _model.refresh);
              return RoundRectContainer(
                margin: const EdgeInsets.symmetric(horizontal: 16),
                padding: const EdgeInsets.symmetric(horizontal: 12),
                child: ListView.separated(
                    itemBuilder: (context, index) {
                      return InkWell(onTap: () => _onTreesItemTapped(items[index]), child: RepositoryTreeItemView(fullPath: _model.fullPath, data: items[index], ref: _model.ref));
                    },
                    separatorBuilder: (context, index) => const Divider(color: Color(0xFFEAEAEA), height: 1, thickness: 1),
                    itemCount: items.length),
              );
            },
          ),
        ),
      ),
    );
  }

  void _onTreesItemTapped(RepositoryTreeItem tree) {
    if (tree.isTree) {
      Navigator.push(context, MaterialPageRoute(builder: (context) => RepositoryTreePage(fullPath: _model.fullPath, pageTitle: _model.pageTitle, flatPath: tree.flatPath)));
    } else {
      UrlLauncher.open(context, tree.webUrl);
    }
  }
}
