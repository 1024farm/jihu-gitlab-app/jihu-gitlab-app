import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/readme_view_model.dart';
import 'package:webview_flutter/webview_flutter.dart';

class ReadmeView extends StatefulWidget {
  const ReadmeView({required this.model, Key? key}) : super(key: key);
  final ReadmeViewModel model;

  @override
  State<ReadmeView> createState() => _ReadmeViewState();
}

class _ReadmeViewState extends State<ReadmeView> {
  late ReadmeViewModel _model;

  @override
  void initState() {
    _model = widget.model;
    super.initState();
  }

  @override
  void dispose() {
    _model.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<bool>(
      valueListenable: _model.getReadmeResultNotifier,
      builder: (context, hasReadme, child) {
        if (!_model.hasReadme) {
          return const SizedBox(key: Key("empty-readme"));
        }
        return Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                SvgPicture.asset("assets/images/readme.svg", width: 18, height: 18),
                const Text('README.md', style: TextStyle(fontSize: 15, color: Color(0xFF03162F), fontWeight: FontWeight.w500)),
                ValueListenableBuilder<bool>(
                  valueListenable: _model.loadingWebViewNotifier,
                  builder: (context, isLoading, child) {
                    return isLoading
                        ? Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: SizedBox(height: 14, width: 14, child: CircularProgressIndicator(color: Theme.of(context).primaryColor, strokeWidth: 1)),
                          )
                        : const SizedBox();
                  },
                )
              ],
            ),
            const SizedBox(height: 14),
            Container(
              padding: const EdgeInsets.all(12),
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(4), color: Colors.white),
              child: ValueListenableBuilder<double>(
                valueListenable: _model.webViewHeightNotifier,
                builder: (context, height, child) {
                  debugPrint('Received notifier: $height');
                  return SizedBox(
                    height: height,
                    child: WebViewWidget(controller: _model.webViewController),
                  );
                },
              ),
            )
          ],
        );
      },
    );
  }
}
