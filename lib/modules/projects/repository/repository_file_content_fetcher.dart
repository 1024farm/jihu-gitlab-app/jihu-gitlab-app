import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';

class FileContent {
  String name;
  String rawText;
  String rawPath;
  String? language;
  String? fileType;
  bool tooLarge;

  FileContent(this.name, this.rawText, this.rawPath, this.language, this.fileType, this.tooLarge);

  String get projectRawPath => rawPath.substring(0, rawPath.length - name.length);
}

class RepositoryFileContentFetcher {
  final String fullPath;

  RepositoryFileContentFetcher(this.fullPath);

  Future<FileContent?> fetch({required String path, String? ref}) async {
    var body = (await ProjectRequestSender.instance().post(Api.graphql(), buildRequestBodyOfFetchFileContent(fullPath, path, ref))).body();
    if (body["errors"] != null) throw Exception(body["errors"].toString());
    List<dynamic> nodes = body['data']?['project']?['repository']?['blobs']?['edges'] ?? [];
    if (nodes.isEmpty) {
      return null;
    }
    String? rawTextBlob = nodes.first['node']?['rawTextBlob'];
    if (rawTextBlob == null) {
      return null;
    }
    String? language = nodes.first['node']?['language'];
    String name = nodes.first['node']?['name'] ?? '';
    String rawPath = nodes.first['node']?['rawPath'] ?? '';
    String? fileType = nodes.first['node']?['simpleViewer']?['fileType'];
    bool tooLarge = nodes.first['node']?['simpleViewer']?['tooLarge'] ?? false;
    return FileContent(name, rawTextBlob, rawPath, language, fileType, tooLarge);
  }
}

Map<String, dynamic> buildRequestBodyOfFetchFileContent(String fullPath, String path, String? ref) {
  return {
    "variables": {"projectPath": fullPath, "path": path, "ref": ref},
    "query": """ 
                query getFileContent(\$projectPath: ID!, \$path: String!, \$ref: String) {
                  project(fullPath: \$projectPath) {
                    id
                    repository {
                      blobs(paths: [\$path], ref: \$ref) {
                        edges {
                          node {
                            rawTextBlob
                            rawPath
                            name
                            language
                            simpleViewer{
                              fileType
                              tooLarge
                            }
                          }
                        }
                      }
                    }
                  }
                }
              """
  };
}
