import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/uri_launcher.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_file_content_fetcher.dart';
import 'package:markdown/markdown.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';

class Readme {
  late String _htmlText;

  Readme({required String rawText, required String host, required String rawPath}) {
    var html = markdownToHtml(rawText);
    if (rawPath.isNotEmpty) {
      html = _fillPath(html, host, rawPath);
    }
    _htmlText = """
                 <!DOCTYPE html>
                      <html>
                      <head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head>
                        <body>$html</body>
                      </html>
                      <script>
                        const resizeObserver = new ResizeObserver(entries =>
                        Resize.postMessage(document.documentElement.offsetHeight.toString()) )
                        resizeObserver.observe(document.body)
                      </script>
                  """;
  }

  String _fillPath(String readmeText, String host, String rawPath) {
    return readmeText.replaceAllMapped(RegExp("(<img.*src=['\"])(.*)(['\"])\\s+"), (match) {
      String? relativePath = match[2];
      if (relativePath != null && !relativePath.startsWith("http")) {
        return "${match[1]}$host$rawPath${match[2]}${match[3]} ";
      }
      return match[0] ?? '';
    });
  }

  String? get value => _htmlText;
}

class ReadmeViewModel {
  static const double _maxWebViewHeight = 5000;
  static const String _jsChanelName = "Resize";
  bool isLoading = false;
  bool _isLoadingWebView = false;
  bool _hasReadme = false;
  double _webViewHeight = 0;
  late String _fullPath;
  late ValueNotifier<bool> _loadingWebViewNotifier;
  late ValueNotifier<bool> _getReadmeResultNotifier;
  late ValueNotifier<double> _webViewHeightNotifier;
  late WebViewController _webViewController;
  late RepositoryFileContentFetcher _fileContentFetcher;

  ReadmeViewModel({required String fullPath, required String? ref}) {
    _fullPath = fullPath;
    _fileContentFetcher = RepositoryFileContentFetcher(_fullPath);
    _loadingWebViewNotifier = ValueNotifier(_isLoadingWebView);
    _getReadmeResultNotifier = ValueNotifier(_hasReadme);
    _webViewHeightNotifier = ValueNotifier(_webViewHeight);
    _webViewController = _createWebViewController();
  }

  Future<String?> _getReadme(String? ref) async {
    FileContent? content = await _fileContentFetcher.fetch(path: "README.md", ref: ref);
    if (content == null || content.rawText.isEmpty || content.tooLarge) {
      return Future(() => null);
    }
    return Readme(rawText: content.rawText, host: _host, rawPath: content.projectRawPath).value;
  }

  Future<bool> load({String? ref}) async {
    try {
      String? readmeText = await _getReadme(ref);
      _hasReadme = readmeText != null;
      _getReadmeResultNotifier.value = _hasReadme;
      if (_hasReadme) {
        _webViewController.loadHtmlString(readmeText!);
      }
    } catch (e) {
      debugPrint(e.toString());
      return false;
    }
    return true;
  }

  WebViewController _createWebViewController() {
    late final PlatformWebViewControllerCreationParams params;
    if (WebViewPlatform.instance is WebKitWebViewPlatform) {
      params = WebKitWebViewControllerCreationParams(allowsInlineMediaPlayback: true, mediaTypesRequiringUserAction: const <PlaybackMediaTypes>{});
    } else {
      params = const PlatformWebViewControllerCreationParams();
    }
    return WebViewController.fromPlatformCreationParams(params)
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..addJavaScriptChannel(_jsChanelName, onMessageReceived: (message) {
        double height = double.parse(message.message);
        _webViewHeight = min(height, _maxWebViewHeight);
        if (height < _webViewHeight) {
          _webViewHeight = height;
        }
        _webViewHeightNotifier.value = _webViewHeight;
      })
      ..setNavigationDelegate(NavigationDelegate(
        onProgress: (progress) {
          _isLoadingWebView = progress < 100;
          _loadingWebViewNotifier.value = _isLoadingWebView;
        },
        onNavigationRequest: (NavigationRequest request) {
          if (request.url.startsWith("http")) {
            UriLauncher.instance().launch(Uri.parse(request.url), mode: LaunchMode.externalApplication);
            return NavigationDecision.prevent;
          }
          return NavigationDecision.navigate;
        },
      ));
  }

  void dispose() {
    _webViewController.removeJavaScriptChannel(_jsChanelName);
  }

  String get _host => ProjectProvider().specifiedHost == null ? ConnectionProvider.currentBaseUrl.get : ProjectProvider().specifiedHost!;

  String get fullPath => _fullPath;

  bool get hasReadme => _hasReadme;

  WebViewController get webViewController => _webViewController;

  ValueNotifier<bool> get loadingWebViewNotifier => _loadingWebViewNotifier;

  ValueNotifier<bool> get getReadmeResultNotifier => _getReadmeResultNotifier;

  ValueNotifier<double> get webViewHeightNotifier => _webViewHeightNotifier;
}
