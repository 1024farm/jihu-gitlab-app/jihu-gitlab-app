import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_file_content_page.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_tree_item.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_tree_page.dart';

class RepositoryTreeItemView extends StatefulWidget {
  const RepositoryTreeItemView({required this.fullPath, required this.data, this.ref, Key? key}) : super(key: key);
  final String fullPath;
  final RepositoryTreeItem data;
  final String? ref;

  @override
  State<RepositoryTreeItemView> createState() => _RepositoryTreeItemViewState();
}

class _RepositoryTreeItemViewState extends State<RepositoryTreeItemView> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: _onItemTapped,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16),
        child: Row(
          children: [
            SvgPicture.asset(widget.data.isTree ? "assets/images/folder.svg" : "assets/images/file.svg", height: 16, width: 16),
            const SizedBox(width: 8),
            Expanded(child: Text(widget.data.displayName, maxLines: 1, overflow: TextOverflow.ellipsis)),
          ],
        ),
      ),
    );
  }

  void _onItemTapped() {
    Widget destination;
    if (widget.data.isTree) {
      destination = RepositoryTreePage(fullPath: widget.fullPath, pageTitle: widget.data.displayName, flatPath: widget.data.flatPath, ref: widget.ref);
    } else {
      destination = RepositoryFileContentPage(fullPath: widget.fullPath, path: widget.data.flatPath, ref: widget.ref, name: widget.data.name, webUrl: widget.data.webUrl);
    }
    Navigator.push(context, MaterialPageRoute(builder: (context) => destination));
  }
}
