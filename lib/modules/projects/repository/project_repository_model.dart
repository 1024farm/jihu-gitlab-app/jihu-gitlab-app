import 'package:flutter/cupertino.dart';
import 'package:jihu_gitlab_app/core/id.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/branch.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/readme_view_model.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_tree_data_fetcher.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_tree_item.dart';

class ProjectRepositoryModel {
  static const _itemLimit = 5;
  List<RepositoryTreeItem> _repositoryTreeItems = [];
  int _itemLength = 0;
  bool _repositoryIsEmpty = true;
  bool _isLoadingBranches = false;
  late String _fullPath;
  late String _projectName;
  late Id _projectId;
  late ValueNotifier<bool> _branchLoadingStateNotifier;
  late RepositoryTreeDataFetcher _treeDataFetcher;
  late ReadmeViewModel _readMeModel;
  List<Branch>? _branches;
  String? _ref;
  Branch? _selectedBranch;

  void init(String fullPath, String projectName) {
    _fullPath = fullPath;
    _projectName = projectName;
    _treeDataFetcher = RepositoryTreeDataFetcher(_fullPath);
    _branchLoadingStateNotifier = ValueNotifier(_isLoadingBranches);
    _readMeModel = ReadmeViewModel(fullPath: _fullPath, ref: selectedBranchName);
  }

  Future<void> getFirst5TreeItems() async {
    List<RepositoryTreeItem> repositoryTreeItems = (await _treeDataFetcher.fetch(ref: selectedBranchName)).items;
    _itemLength = repositoryTreeItems.length;
    _repositoryTreeItems = repositoryTreeItems.length > _itemLimit ? repositoryTreeItems.sublist(0, _itemLimit) : repositoryTreeItems;
  }

  Future<void> checkRepositoryIsEmpty() async {
    var body = (await ProjectRequestSender.instance().post(Api.graphql(), buildRequestBodyOfCheckRepositoryIsEmpty(_fullPath))).body();
    bool exists = body['data']?['project']?['repository']?['exists'] ?? true;
    bool empty = body['data']?['project']?['repository']?['empty'] ?? true;
    _ref = body['data']?['project']?['repository']?['rootRef'];
    _projectId = Id.fromGid(body['data']?['project']?['id']);
    _repositoryIsEmpty = !exists || empty;
  }

  Future<void> loadReadme() async {
    _readMeModel.load(ref: selectedBranchName);
  }

  Future<List<Branch>> getBranches() async {
    if (_branches == null) {
      _notifyBranchLoadingState(true);
      var response = await ProjectRequestSender.instance().get(Api.join("/projects/${_projectId.id}/repository/branches?per_page=100"));
      _branches = (response.body() as List).map((e) => Branch.fromJson(e)).toList();
      _notifyBranchLoadingState(false);
    }
    return Future(() => _branches!);
  }

  void _notifyBranchLoadingState(bool loading) {
    _isLoadingBranches = loading;
    _branchLoadingStateNotifier.value = _isLoadingBranches;
  }

  void changeSelectedBranch(Branch selected) {
    _selectedBranch = selected;
  }

  String get projectName => _projectName;

  String get fullPath => _fullPath;

  bool get showTheViewAllButton => _itemLength > _itemLimit;

  bool get repositoryIsEmpty => _repositoryIsEmpty;

  List<RepositoryTreeItem> get items => _repositoryTreeItems;

  ValueNotifier<bool> get branchLoadingStateNotifier => _branchLoadingStateNotifier;

  String? get selectedBranchName => _selectedBranch?.name ?? _ref;

  ReadmeViewModel get readMeModel => _readMeModel;
}

Map<String, dynamic> buildRequestBodyOfCheckRepositoryIsEmpty(String fullPath) {
  return {
    "variables": {"projectPath": fullPath},
    "query": """
            query getRepository(\$projectPath: ID!) {
              project(fullPath: \$projectPath) {
                id
                name
                repository {
                  exists
                  empty
                  rootRef
                }
              }
            }
            """
  };
}
