import 'dart:math';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/syntax_highlighter.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/uri_launcher.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/http_fail_view.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_view.dart';
import 'package:jihu_gitlab_app/core/widgets/share_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/repository_file_content_model.dart';
import 'package:linked_scroll_controller/linked_scroll_controller.dart';
import 'package:url_launcher/url_launcher.dart';

class RepositoryFileContentPage extends StatefulWidget {
  const RepositoryFileContentPage({required this.fullPath, required this.path, required this.name, required this.webUrl, this.ref, Key? key}) : super(key: key);
  final String fullPath;
  final String path;
  final String name;
  final String webUrl;
  final String? ref;

  @override
  State<RepositoryFileContentPage> createState() => _RepositoryFileContentPageState();
}

class _RepositoryFileContentPageState extends State<RepositoryFileContentPage> {
  static const double _contentHorizontalPadding = 8.0;
  static const double _numberBorderWidth = 1.0;
  final RepositoryFileContentModel _model = RepositoryFileContentModel();
  late LinkedScrollControllerGroup _controllers;
  late ScrollController _numbers;
  late ScrollController _content;
  double _textScaleFactor = 1.0;

  @override
  void initState() {
    _model.init(widget.fullPath, widget.path, widget.name, widget.webUrl, widget.ref);
    _controllers = LinkedScrollControllerGroup();
    _content = _controllers.addAndGet();
    _numbers = _controllers.addAndGet();
    Future.delayed(Duration.zero, () {
      _textScaleFactor = MediaQuery.of(context).textScaleFactor;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CommonAppBar(
        showLeading: true,
        title: Text(_model.name),
        actions: [
          PopupMenuButton<String>(
            padding: EdgeInsets.zero,
            position: PopupMenuPosition.under,
            itemBuilder: (context) => [PopupMenuItem<String>(padding: EdgeInsets.zero, child: ShareView(title: widget.name, link: widget.webUrl, isFile: true))],
            child: Container(
              margin: const EdgeInsets.only(right: 20),
              child: SvgPicture.asset("assets/images/external-link.svg",
                  semanticsLabel: "share", colorFilter: const ColorFilter.mode(AppThemeData.secondaryColor, BlendMode.srcIn), width: 18, height: 18),
            ),
          )
        ],
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 16),
        child: ValueListenableBuilder<LoadState>(
            valueListenable: _model.notifier,
            builder: (context, loadState, child) {
              return _buildBody(loadState);
            }),
      ),
    );
  }

  Widget _buildBody(LoadState loadState) {
    if (loadState == LoadState.loadingState) return const LoadingView();
    if (loadState == LoadState.errorState) return HttpFailView(onRefresh: _model.getFileContent);
    var lines = _model.lines;
    if (lines == null || !lines.isTextFile || lines.isTooLarge) return _buildUnsupportedFileTypeHint();
    var leftWidth = lines.lineNumberWidth * _textScaleFactor + _contentHorizontalPadding * 2 + _numberBorderWidth * 2;
    var rightWidth = max(lines.maxTextWidth * _textScaleFactor + _contentHorizontalPadding * 2, MediaQuery.of(context).size.width - leftWidth);
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(width: leftWidth, child: _buildLineNumbers(lines)),
        Expanded(
            flex: 1,
            child: ScrollConfiguration(
                behavior: const CupertinoScrollBehavior(),
                child: SingleChildScrollView(physics: const ClampingScrollPhysics(), scrollDirection: Axis.horizontal, child: SizedBox(width: rightWidth, child: _buildFileContent(lines))))),
      ],
    );
  }

  Widget _buildUnsupportedFileTypeHint() {
    return Align(
      alignment: const FractionalOffset(0.5, 0.3),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SvgPicture.asset("assets/images/warning.svg", height: 24, width: 24),
          const SizedBox(height: 12),
          Text(AppLocalizations.dictionary().unsupportedFileType, style: const TextStyle(color: Color(0xFF1A1B36), fontSize: 16, fontWeight: FontWeight.w600)),
          const SizedBox(height: 20),
          OutlinedButton(
              onPressed: () => UriLauncher.instance().launch(Uri.parse(_model.webUrl), mode: LaunchMode.externalApplication),
              child: Text(AppLocalizations.dictionary().openInBrowser, style: const TextStyle(color: AppThemeData.secondaryColor, fontSize: 14, fontWeight: FontWeight.w500)))
        ],
      ),
    );
  }

  Widget _buildFileContent(Lines lines) {
    final SyntaxHighlighterStyle style = Theme.of(context).brightness == Brightness.dark ? SyntaxHighlighterStyle.darkThemeStyle() : SyntaxHighlighterStyle.lightThemeStyle();
    return SelectionArea(
      child: ScrollConfiguration(
        behavior: const CupertinoScrollBehavior(),
        child: ListView.builder(
          controller: _content,
          shrinkWrap: true,
          itemBuilder: (context, index) {
            Line line = lines.data[index];
            return Container(
              color: Colors.white,
              height: lines.maxLineHeight * _textScaleFactor,
              padding: const EdgeInsets.symmetric(horizontal: _contentHorizontalPadding),
              child: Text.rich(
                TextSpan(
                  style: _model.contentTextStyle,
                  children: [DartSyntaxHighlighter(style).format(line.text)],
                ),
              ),
            );
          },
          itemCount: lines.data.length,
        ),
      ),
    );
  }

  Widget _buildLineNumbers(Lines lines) {
    return ScrollConfiguration(
      behavior: const CupertinoScrollBehavior(),
      child: ListView.builder(
        controller: _numbers,
        itemBuilder: (context, index) {
          Line line = lines.data[index];
          return Container(
            height: lines.maxLineHeight * _textScaleFactor,
            padding: const EdgeInsets.only(right: _contentHorizontalPadding),
            decoration: const BoxDecoration(border: Border(right: BorderSide(width: _numberBorderWidth, color: Color(0xFF87878C)))),
            child: Text(
              line.lineNumber.toString(),
              maxLines: 1,
              style: _model.contentTextStyle.copyWith(color: const Color(0xFF87878C), fontFeatures: [const FontFeature.tabularFigures()]),
              textAlign: TextAlign.end,
            ),
          );
        },
        itemCount: lines.data.length,
      ),
    );
  }
}
