import 'package:jihu_gitlab_app/core/db_manager.dart';
import 'package:jihu_gitlab_app/core/domain/global_time.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/subgroups/subgroup_list_model.dart';

class GroupAndProjectRepository {
  static final GroupAndProjectRepository _instance = GroupAndProjectRepository._internal();

  GroupAndProjectRepository._internal();

  factory GroupAndProjectRepository.instance() => _instance;

  Future<List<int>> insert(List<GroupAndProjectEntity> subgroupEntities) async {
    return await DbManager.instance().batchInsert(GroupAndProjectEntity.tableName, subgroupEntities.map((e) => e.toMap()).toList());
  }

  Future<int> update(List<GroupAndProjectEntity> subgroupEntities) async {
    return await DbManager.instance().batchUpdate(GroupAndProjectEntity.tableName, subgroupEntities.map((e) => e.toMap()).toList());
  }

  Future<int> deleteLessThan(int userId, int parentId, SubgroupItemType type, int version) async {
    return await DbManager.instance().delete(GroupAndProjectEntity.tableName, where: ' user_id = ? and parent_id = ? and type = ? and version < ? ', whereArgs: [userId, parentId, type.name, version]);
  }

  Future<int> deleteLessThanForStarred(int userId, int version) async {
    return await DbManager.instance().delete(GroupAndProjectEntity.tableName, where: ' user_id = ? and starred = 1 and type = "project" and version < ? ', whereArgs: [userId, version]);
  }

  Future<List<GroupAndProjectEntity>> query({required int userId, required int parentId, List<int>? iidList}) async {
    String sql = "select * from ${GroupAndProjectEntity.tableName} where user_id = $userId and parent_id = $parentId";
    if (iidList != null) {
      sql += " and iid in (${iidList.join(',')})";
    }
    sql += " order by type desc, last_activity_at desc";
    var list = await DbManager.instance().rawFind(sql);
    return list.map((e) => GroupAndProjectEntity.restore(e)).toList();
  }

  Future<List<GroupAndProjectEntity>> findStarredList({required int userId, List<int>? iidList}) async {
    String sql = "select * from ${GroupAndProjectEntity.tableName} where starred = 1 and user_id = $userId";
    if (iidList != null) {
      sql += " and iid in (${iidList.join(',')})";
    }
    sql += " order by starred_at desc";
    var list = await DbManager.instance().rawFind(sql);
    return list.map((e) => GroupAndProjectEntity.restore(e)).toList();
  }

  Future<bool> toggleStar(int id, {required bool starred}) async {
    var rows = await DbManager.instance()
        .update(GroupAndProjectEntity.tableName, {"starred": starred ? 1 : 0, "starred_at": starred ? GlobalTime.now().millisecondsSinceEpoch : 0}, where: " id = ? ", whereArgs: [id]);
    return Future(() => rows > 0);
  }

  Future<int> countStarred(int userId) async {
    return await DbManager.instance().count(GroupAndProjectEntity.tableName, where: " user_id = ? and starred = 1 ", whereArgs: [userId]);
  }

  Future<int>? cancelStarredProjectsLessThan(int userId, int version) async {
    return DbManager.instance().update(GroupAndProjectEntity.tableName, {"starred": 0, "starred_at": 0, "version": version},
        where: " user_id = ? and version < ? and type = ? ", whereArgs: [userId, version, SubgroupItemType.project.name]);
  }
}
