import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/domain/global_time.dart';
import 'package:jihu_gitlab_app/core/list_comparator.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/synchronizer.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/selector/data_provider.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project_repository.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/subgroups/subgroup_list_model.dart';

class SubgroupAndProjectProvider extends DataProvider<GroupAndProject> {
  late int _userId;
  late int _parentId;
  late GroupAndProjectRepository _repository;

  SubgroupAndProjectProvider({required int userId, required int parentId}) {
    _userId = userId;
    _parentId = parentId;
    _repository = GroupAndProjectRepository.instance();
  }

  @override
  Future<List<GroupAndProject>> loadFromLocal() async {
    List<GroupAndProjectEntity> groupAndProjectEntities = await _repository.query(userId: _userId, parentId: _parentId);
    return groupAndProjectEntities.map((e) => e.asDomain()).toList();
  }

  @override
  Future<bool> syncFromRemote() async {
    int version = GlobalTime.now().millisecondsSinceEpoch;
    List<bool> results = await Future.wait([_syncProjects(version), _syncSubgroups(version)]);
    return Future(() => results.every((e) => e));
  }

  Future<bool> _syncSubgroups(int version) async {
    return Synchronizer<dynamic>(
        url: Api.join('/groups/$_parentId/subgroups?all_available=true'),
        dataProcessor: (data, page, pageSize) async {
          List<GroupAndProjectEntity> subgroupEntities =
              (data as List).map((e) => GroupAndProjectEntity.createSubgroupEntity(e, ConnectionProvider.connectionId!, _parentId, version)).toList(growable: false);
          await save(subgroupEntities, SubgroupItemType.subgroup, version);
          return subgroupEntities.length >= pageSize;
        }).run();
  }

  Future<bool> _syncProjects(int version) async {
    return Synchronizer<dynamic>(
        url: Api.join('/groups/$_parentId?all_available=true'),
        dataProcessor: (data, page, pageSize) async {
          List<GroupAndProjectEntity> subgroupEntities =
              ((data['projects'] ?? []) as List).map((e) => GroupAndProjectEntity.createProjectEntity(e, ConnectionProvider.connectionId!, _parentId, version)).toList(growable: false);
          await save(subgroupEntities, SubgroupItemType.project, version);
          return subgroupEntities.length >= pageSize;
        }).run();
  }

  Future<void> save(List<GroupAndProjectEntity> subgroupEntities, SubgroupItemType type, int version) async {
    List<int> iidList = subgroupEntities.map((e) => e.iid).toList();
    List<GroupAndProjectEntity> exists = await _repository.query(userId: _userId, parentId: _parentId, iidList: iidList);
    ListCompareResult<GroupAndProjectEntity> result = ListComparator.compare<GroupAndProjectEntity>(exists, subgroupEntities);
    if (result.hasAdd) {
      await _repository.insert(result.add);
    }
    if (result.hasUpdate) {
      var list = result.update.map((e) {
        e.left.update(e.right);
        return e.left;
      }).toList();
      await _repository.update(list);
    }
    await _repository.deleteLessThan(_userId, _parentId, type, version);
  }

  @visibleForTesting
  void injectRepositoryForTesting(GroupAndProjectRepository groupAndProjectRepository) {
    _repository = groupAndProjectRepository;
  }
}
