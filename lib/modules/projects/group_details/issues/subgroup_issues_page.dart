import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/domain/project.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/modules/issues/details/issue_details_page.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_page.dart';

class SubgroupIssuesPage extends StatefulWidget {
  final int groupId;
  final String relativePath;
  final OnSelectedIssueChange? onSelectedIssueChange;
  final Color selectedItemBackground;
  final GlobalKey<IssuesPageState>? refreshKey;

  const SubgroupIssuesPage({
    required this.groupId,
    required this.relativePath,
    this.onSelectedIssueChange,
    this.selectedItemBackground = Colors.white,
    this.refreshKey,
    super.key,
  });

  @override
  State<StatefulWidget> createState() => SubgroupIssuesPageState();
}

class SubgroupIssuesPageState extends State<SubgroupIssuesPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IssuesPage(
        key: widget.refreshKey,
        relativePath: widget.relativePath,
        selectedColor: widget.selectedItemBackground,
        issueType: IssueType.group,
        onSelectedIssueChange: (index, params) {
          Project.getById(params['projectId']).then((project) {
            ProjectProvider().changeProject(project, null);
            if (widget.onSelectedIssueChange == null) {
              params['showLeading'] = true;
              Navigator.of(context).pushNamed(IssueDetailsPage.routeName, arguments: params);
            } else {
              widget.onSelectedIssueChange!.call(index, params);
            }
          });
        },
      ),
    );
  }
}
