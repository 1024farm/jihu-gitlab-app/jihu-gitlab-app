import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/layout/adaptive.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_page.dart';

class ProjectIssuesPage extends StatefulWidget {
  const ProjectIssuesPage({
    required this.projectId,
    required this.relativePath,
    this.onSelectedIssueChange,
    this.refreshKey,
    super.key,
  });

  final int? projectId;
  final String relativePath;
  final OnSelectedIssueChange? onSelectedIssueChange;
  final GlobalKey<IssuesPageState>? refreshKey;

  @override
  State<ProjectIssuesPage> createState() => _ProjectIssuesPageState();
}

class _ProjectIssuesPageState extends State<ProjectIssuesPage> {
  final _selectedItemIndex = IndexSource();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IssuesPage(
        key: widget.refreshKey,
        relativePath: widget.relativePath,
        isProject: true,
        projectId: widget.projectId,
        indexSource: _selectedItemIndex,
        selectedColor: isDesktopLayout(context) ? Theme.of(context).colorScheme.primary : Colors.white,
        onSelectedIssueChange: widget.onSelectedIssueChange,
      ),
    );
  }
}
