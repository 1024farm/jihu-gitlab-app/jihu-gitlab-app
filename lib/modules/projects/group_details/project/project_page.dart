import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/layout/adaptive.dart';
import 'package:jihu_gitlab_app/core/project_identity.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/project_archived_notice_view.dart';
import 'package:jihu_gitlab_app/core/widgets/svg_text.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/auth/login_page.dart';
import 'package:jihu_gitlab_app/modules/issues/index.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_page.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/issues/project_issues_page.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/iterations/projects_iterations_page.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/mrs/project_merge_request_page.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/project/project_title_tab_bar_view.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestones_page.dart';
import 'package:jihu_gitlab_app/modules/projects/releases/project_releases_page.dart';
import 'package:jihu_gitlab_app/modules/projects/releases/release_creation_page.dart';
import 'package:jihu_gitlab_app/modules/projects/repository/project_repository_page.dart';
import 'package:provider/provider.dart';

class ProjectPage extends StatefulWidget {
  static const String routeName = 'ProjectPage';
  final Map arguments;

  const ProjectPage({required this.arguments, super.key});

  @override
  State<StatefulWidget> createState() => _ProjectPageState();
}

class _ProjectPageState extends State<ProjectPage> with TickerProviderStateMixin {
  static const int _tabLength = 6;
  late TabController _tabController = TabController(initialIndex: 0, length: _tabLength, vsync: this);
  int? _projectId;
  bool _showLeading = true;
  bool _isFeedback = false;
  Map<String, dynamic> _selectedItemParams = {};
  final GlobalKey<IssuesPageState> _issuesRefreshKey = GlobalKey();
  final GlobalKey<ProjectReleasesPageState> _releasesRefreshKey = GlobalKey();

  @override
  void initState() {
    if (widget.arguments.containsKey('showLeading')) {
      _showLeading = widget.arguments['showLeading'];
    }
    _projectId ??= widget.arguments['projectId'];
    _isFeedback = widget.arguments.containsKey('feedback') && widget.arguments['feedback'];
    if (_isFeedback) {
      _tabController = TabController(initialIndex: 1, length: _tabLength, vsync: this);
    }
    super.initState();
  }

  @override
  void didUpdateWidget(covariant ProjectPage oldWidget) {
    if (_projectId != widget.arguments['projectId']) {
      _projectId = widget.arguments['projectId'];
      _selectedItemParams = {};
    }
    _isFeedback = widget.arguments.containsKey('feedback') && widget.arguments['feedback'];
    if (_isFeedback) {
      _tabController = TabController(initialIndex: 1, length: _tabLength, vsync: this);
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return isDesktopLayout(context) ? Row(children: [_buildDesktopLeftView(), _buildDesktopDetailsView()]) : _buildBody();
  }

  Widget _buildDesktopLeftView() {
    return Expanded(child: _buildBody());
  }

  Widget _buildDesktopDetailsView() {
    return Expanded(child: IssueDetailsPage(arguments: _selectedItemParams));
  }

  Widget _buildBody() {
    return Consumer<ProjectProvider>(builder: (context, _, child) {
      return Consumer<ConnectionProvider>(builder: (context, _, child) {
        return Scaffold(
            appBar: _buildAppBar(),
            body: SafeArea(
              child: Scaffold(
                appBar: AppBar(
                  toolbarHeight: ProjectProvider().archived ? 105 : null,
                  automaticallyImplyLeading: false,
                  flexibleSpace: ProjectProvider().archived
                      ? Column(
                          children: [
                            if (ProjectProvider().archived) const ProjectArchivedNoticeView(),
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: ProjectTitleTabBarView(tabController: _tabController),
                            ),
                          ],
                        )
                      : Padding(padding: const EdgeInsets.only(left: 10), child: ProjectTitleTabBarView(tabController: _tabController)),
                ),
                body: TabBarView(
                  controller: _tabController,
                  children: [
                    ProjectRepositoryPage(fullPath: widget.arguments['relativePath'], projectName: widget.arguments["name"]),
                    ProjectIssuesPage(
                      refreshKey: _issuesRefreshKey,
                      projectId: _projectId,
                      relativePath: widget.arguments['relativePath'],
                      onSelectedIssueChange: isDesktopLayout(context) ? (index, params) => setState(() => _selectedItemParams = params) : null,
                    ),
                    ProjectMergeRequestPage(arguments: {'relativePath': widget.arguments['relativePath']}),
                    ProjectsIterationsPage(fullPath: widget.arguments['relativePath']),
                    ProjectMilestonesPage(fullPath: widget.arguments['relativePath']),
                    ProjectReleasesPage(fullPath: widget.arguments['relativePath'], key: _releasesRefreshKey)
                  ],
                ),
              ),
            ));
      });
    });
  }

  AppBar _buildAppBar() {
    return CommonAppBar(
        title: Text(widget.arguments['name']),
        showLeading: _showLeading,
        actions: [if ((ProjectProvider().isSpecifiedHostHasConnection || _isFeedback) && !ProjectProvider().archived) Padding(padding: const EdgeInsets.all(10), child: _buildPopupMenuButton())]);
  }

  Widget _buildPopupMenuButton() {
    return PopupMenuButton<String>(
      padding: EdgeInsets.zero,
      position: PopupMenuPosition.under,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
      itemBuilder: (context) => [
        PopupMenuItem<String>(
          value: "issue",
          child: SvgText(
            svgAssetPath: 'assets/images/issue_new.svg',
            svgColor: const Color(0xFF66696D),
            svgSize: const Size(20, 20),
            text: AppLocalizations.dictionary().createIssue(AppLocalizations.dictionary().issue),
            spacing: 8,
          ),
        ),
        if (!_isFeedback) const PopupMenuDivider(height: 8),
        if (!_isFeedback)
          PopupMenuItem(
            value: "release",
            child: SvgText(
              svgAssetPath: 'assets/images/release_new.svg',
              svgColor: const Color(0xFF66696D),
              svgSize: const Size(20, 20),
              text: AppLocalizations.dictionary().createIssue(AppLocalizations.dictionary().release),
              spacing: 8,
            ),
          ),
      ],
      onSelected: (value) {
        if (value == 'issue') {
          if (!ProjectProvider().isSpecifiedHostHasConnection && _isFeedback) {
            Navigator.of(context).pushNamed(LoginPage.routeName, arguments: {'host': 'jihulab.com'}).then((value) {
              if (ProjectProvider().isSpecifiedHostHasConnection) _navigateToCreateIssuePage();
            });
          } else {
            _navigateToCreateIssuePage();
          }
        } else {
          _navigateToReleaseCreationPage();
        }
      },
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        margin: const EdgeInsets.only(right: 4),
        child: const Icon(Icons.add_box_outlined, color: Color(0xFF66696D)),
      ),
    );
  }

  void _navigateToCreateIssuePage() {
    Navigator.of(context)
        .push(MaterialPageRoute(
      builder: (context) => IssueCreationPage(projectId: widget.arguments['projectId'], from: 'project', groupId: widget.arguments['groupId']),
    ))
        .then((value) {
      if (value != null && value is bool && value == true) {
        _issuesRefreshKey.currentState?.onRefresh();
        setState(() {});
      }
    });
  }

  void _navigateToReleaseCreationPage() {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) {
        return ReleaseCreationPage(projectIdentity: ProjectIdentity(widget.arguments['projectId'], widget.arguments['relativePath']));
      },
    )).then((value) {
      if (value != null && value is bool && value && _tabController.index == 5) {
        _releasesRefreshKey.currentState?.refresh();
      }
    });
  }
}
