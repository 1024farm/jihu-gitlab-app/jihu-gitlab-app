import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/widgets/app_tab_bar.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class ProjectTitleTabBarView extends StatelessWidget {
  final TabController tabController;
  final _tabBarTitles = [
    AppLocalizations.dictionary().repository,
    AppLocalizations.dictionary().projectsIssues,
    AppLocalizations.dictionary().projectsMr,
    AppLocalizations.dictionary().iterations,
    AppLocalizations.dictionary().milestones,
    AppLocalizations.dictionary().releases
  ];

  ProjectTitleTabBarView({required this.tabController, super.key});

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return SizedBox(
      height: 64.0,
      child: Container(
          alignment: Alignment.topLeft,
          child: AppTabBar(
              controller: tabController,
              isScrollable: true,
              labelStyle: textTheme.titleLarge,
              unselectedLabelStyle: textTheme.titleMedium,
              tabs: _tabBarTitles.map((item) => Tab(text: item)).toList())),
    );
  }
}
