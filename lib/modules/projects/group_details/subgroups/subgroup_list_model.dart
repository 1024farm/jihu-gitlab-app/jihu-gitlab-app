import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/widgets/selector/data_provider.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/subgroup_and_project_provider.dart';

enum SubgroupItemType { group, project, subgroup }

class SubgroupListModel {
  LoadState _loadState = LoadState.loadingState;

  late int _groupId;
  List<GroupAndProject> _data = [];
  late ValueNotifier<List<GroupAndProject>> _notifier;
  DataProvider<GroupAndProject>? _dataProvider;

  void init({required int groupId}) {
    _groupId = groupId;
    _dataProvider ??= SubgroupAndProjectProvider(userId: ConnectionProvider.connectionId!, parentId: _groupId);
    _notifier = ValueNotifier(_data);
  }

  Future<void> loadData() async {
    _loadLocalDataAndNotify();
    _dataProvider?.syncFromRemote().then((value) {
      if (value) {
        _loadLocalDataAndNotify();
      }
    });
  }

  Future<void> _loadLocalDataAndNotify() async {
    List<GroupAndProject> data = await _dataProvider!.loadFromLocal();
    _data.clear();
    _data.addAll(data);
    _notifier.value = data;
    _loadState = LoadState.successState;
  }

  void clear() {
    _loadState = LoadState.loadingState;
    _data = [];
    _dataProvider = null;
  }

  LoadState get loadState => _loadState;

  ValueNotifier<List<GroupAndProject>> get notifier => _notifier;

  int get groupId => _groupId;

  @visibleForTesting
  void injectDataProviderForTesting(DataProvider<GroupAndProject> dataProvider) => _dataProvider = dataProvider;
}
