import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project_list_tile.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/subgroups/subgroup_list_model.dart';

class SubgroupListPage extends StatefulWidget {
  final String relativePath;
  final int groupId;

  const SubgroupListPage({required this.relativePath, required this.groupId, super.key});

  @override
  State<SubgroupListPage> createState() => _SubgroupListPageState();
}

class _SubgroupListPageState extends State<SubgroupListPage> {
  final SubgroupListModel _model = locator<SubgroupListModel>();

  bool _inPageInitializing = false;

  @override
  void initState() {
    super.initState();
    _pageInit(false);
  }

  void _pageInit(bool shouldClearModel) {
    if (_inPageInitializing) return;
    _inPageInitializing = true;
    if (shouldClearModel) _model.clear();
    _model.init(groupId: widget.groupId);
    _model.loadData();
    _inPageInitializing = false;
  }

  @override
  Widget build(BuildContext context) {
    if (widget.groupId != _model.groupId) _pageInit(true);
    return Padding(padding: const EdgeInsets.symmetric(horizontal: 16), child: subgroupListView());
  }

  Widget subgroupListView() {
    return ValueListenableBuilder<List<GroupAndProject>>(
        valueListenable: _model.notifier,
        builder: (BuildContext context, List<GroupAndProject> data, Widget? child) {
          if (_model.loadState == LoadState.successState && data.isEmpty) {
            return TipsView(icon: 'assets/images/no_groups.png', onRefresh: () => _model.loadData());
          }
          return ClipRRect(
              borderRadius: BorderRadius.circular(4.0),
              child: ListView.separated(
                  itemCount: data.length,
                  separatorBuilder: (context, index) => const Divider(height: 0, thickness: 0.5, indent: 12.0, endIndent: 12.0, color: Color(0xFFDDDDDD)),
                  itemBuilder: (context, index) => GroupAndProjectListTile(itemDataProvider: () => data[index])));
        });
  }
}
