import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/domain/avatar_url.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/paged_state.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar/avatar.dart';
import 'package:jihu_gitlab_app/core/widgets/http_fail_view.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_view.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/core/widgets/unauthorized_view.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_request_page.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/mrs/project_merge_request_model.dart';
import 'package:provider/provider.dart';

class ProjectMergeRequestPage extends StatefulWidget {
  final Map arguments;

  const ProjectMergeRequestPage({required this.arguments, super.key});

  @override
  State<ProjectMergeRequestPage> createState() => _ProjectMergeRequestPageState();
}

class _ProjectMergeRequestPageState extends PagedState<ProjectMergeRequestPage> {
  final ProjectMergeRequestModel _model = ProjectMergeRequestModel();

  @override
  void initState() {
    _model.init(widget.arguments['relativePath']);
    super.initState();
  }

  @override
  AppBar? buildAppBar() {
    return null;
  }

  @override
  Widget buildBody() {
    return Scaffold(
        appBar: buildAppBar(),
        body: Consumer<ConnectionProvider>(builder: (context, _, child) {
          if (!ConnectionProvider.authorized && ProjectProvider().isSpecifiedHostHasConnection) {
            return const UnauthorizedView();
          }
          if (isErrorState()()) {
            return HttpFailView(onRefresh: () => onRefresh());
          }
          return buildRefreshView();
        }));
  }

  @override
  Widget? buildListView() {
    if (_model.loadState == LoadState.loadingState) return const LoadingView();
    if (_model.mergeRequests.isEmpty) return TipsView(onRefresh: () => onRefresh());
    return SafeArea(
      child: Padding(
          padding: const EdgeInsets.all(16),
          child: ListView.separated(
            itemBuilder: (context, index) {
              return InkWell(
                onTap: () {
                  final params = <String, dynamic>{
                    'projectId': _model.projectId.id,
                    'projectName': _model.projectName,
                    'mergeRequestIid': int.parse(_model.mergeRequests[index].iid),
                    'fullPath': widget.arguments['relativePath'],
                  };
                  Navigator.of(context).pushNamed(MergeRequestPage.routeName, arguments: params);
                },
                child: Container(
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(6.0)),
                  ),
                  padding: const EdgeInsets.all(12),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        _model.mergeRequests[index].title,
                        style: const TextStyle(fontSize: 14, color: Color(0xFF03162F), fontWeight: FontWeight.w500),
                      ),
                      const SizedBox(height: 8),
                      Row(
                        children: [
                          SvgPicture.asset('assets/images/mr_icon.svg', width: 10, height: 10),
                          Expanded(
                            child: Text(
                              ' !${_model.mergeRequests[index].iid}',
                              style: const TextStyle(fontSize: 12, color: Color(0xFF95979A), fontWeight: FontWeight.w400),
                            ),
                          ),
                          Avatar(avatarUrl: AvatarUrl(_model.mergeRequests[index].author.avatarUrl).realUrl(ProjectProvider().specifiedHost), size: 16)
                        ],
                      )
                    ],
                  ),
                ),
              );
            },
            separatorBuilder: (BuildContext context, int index) => const Divider(thickness: 8, height: 8, color: Colors.transparent),
            itemCount: _model.mergeRequests.length,
          )),
    );
  }

  @override
  Function hasNextPage() => () => _model.hasNextPage;

  @override
  Function isErrorState() => () => _model.loadState == LoadState.errorState;

  @override
  Future<bool> Function() modelLoadMore() => () => _model.loadMore();

  @override
  Future<bool> Function() modelRefresh() => () => _model.onRefresh();
}
