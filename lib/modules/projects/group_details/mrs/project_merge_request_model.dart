import 'package:jihu_gitlab_app/core/domain/assignee/assignee.dart';
import 'package:jihu_gitlab_app/core/id.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';

class ProjectMergeRequestModel {
  static const int _size = 20;
  LoadState loadState = LoadState.loadingState;
  String _afterCursor = '';
  bool hasNextPage = true;
  late final String _relativePath;
  late Id projectId;
  late String projectName;
  List<ProjectMergeRequest> mergeRequests = [];

  void init(String relativePath) {
    _relativePath = relativePath;
  }

  Future<bool> onRefresh() async {
    try {
      loadState = LoadState.loadingState;
      hasNextPage = true;
      _afterCursor = '';
      mergeRequests = await _getMrs(_size, _afterCursor);
      loadState = LoadState.successState;
      return Future.value(true);
    } catch (e) {
      LogHelper.err('MergeRequestPage refresh error', e);
      loadState = LoadState.errorState;
      return Future.value(true);
    }
  }

  Future<bool> loadMore() async {
    var stashedHasNextPage = hasNextPage;
    var afterCursor = _afterCursor;
    try {
      var list = await _getMrs(_size, afterCursor);
      mergeRequests.addAll(list);
      return Future.value(true);
    } catch (e) {
      LogHelper.err('ProjectMergeRequestModel loadMore error', e);
      hasNextPage = stashedHasNextPage;
      _afterCursor = afterCursor;
      return Future.value(false);
    }
  }

  Future<List<ProjectMergeRequest>> _getMrs(int size, String cursor) async {
    var response = await ProjectRequestSender.instance().post(Api.graphql(), getMRListGraphQLRequestBody(_relativePath, size, cursor));
    var errors = response.body()["errors"];
    if (errors != null) throw Exception(errors);
    var project = response.body()['data']?['project'];
    var mergeRequest = project?['mergeRequests'];
    hasNextPage = mergeRequest?['pageInfo']?['hasNextPage'] ?? false;
    _afterCursor = mergeRequest?['pageInfo']?['endCursor'] ?? '';
    projectId = Id.fromGid(project?['id'] ?? '/0');
    projectName = project?['name'] ?? '';
    return ((mergeRequest?['nodes'] ?? []) as List).map((o) => ProjectMergeRequest.fromJson(o)).toList();
  }
}

class ProjectMergeRequest {
  String iid;
  String title;
  Assignee author;

  ProjectMergeRequest(this.iid, this.title, this.author);

  factory ProjectMergeRequest.fromJson(json) {
    return ProjectMergeRequest(json['iid'] ?? '', json['title'] ?? '', Assignee.fromGraphQLJson(json['author'] ?? {}));
  }
}

Map<String, String> getMRListGraphQLRequestBody(String fullPath, size, cursor) => {
      "query": '''
      {
        project(fullPath: "$fullPath") {
          id
          name
          mergeRequests(state: opened, first: $size, after: "$cursor") {
            nodes {
              iid
              title
              author {
                id
                name
                username
                avatarUrl
              }
            }
            pageInfo {
              hasNextPage
              endCursor
            }
          }
        }
      }
      '''
    };
