import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/widgets/app_tab_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_page.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/iterations/subgroup_iterations_page.dart';
import 'package:jihu_gitlab_app/modules/projects/projects.dart';
import 'package:jihu_gitlab_app/modules/projects/projects_selector/projects_selector.dart';

GlobalKey<IssuesPageState> mineGlobalKey = GlobalKey();

typedef OnTabChange = void Function(int index);

class SubgroupPage extends StatefulWidget {
  static const String routeName = 'SubgroupPage';
  final Map arguments;
  final OnTabChange? onTabChange;
  final OnSelectedIssueChange? onSelectedIssueChange;

  const SubgroupPage({required this.arguments, this.onTabChange, this.onSelectedIssueChange, super.key});

  @override
  State<StatefulWidget> createState() => _SubgroupPageState();
}

class _SubgroupPageState extends State<SubgroupPage> with TickerProviderStateMixin {
  late TabController _tabController;
  int? _groupId;
  String? _name;
  String? _relativePath;
  bool _showLeading = true;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(initialIndex: 0, length: 3, vsync: this);
    _pageInit();
  }

  void _pageInit() {
    _groupId = widget.arguments['groupId'];
    _name = widget.arguments['name'];
    _relativePath = widget.arguments['relativePath'];
    if (widget.arguments.containsKey('showLeading')) {
      _showLeading = widget.arguments['showLeading'];
    }
    _tabController = TabController(initialIndex: 0, length: 3, vsync: this);
    if (widget.onTabChange != null) {
      _tabController.addListener(() {
        if (_tabController.index.toDouble() == _tabController.animation?.value) {
          widget.onTabChange!.call(_tabController.index);
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_groupId != widget.arguments['groupId']) setState(() => _pageInit());
    final colorScheme = Theme.of(context).colorScheme;
    return Scaffold(
      appBar: CommonAppBar(title: Text(_name!), showLeading: _showLeading, actions: [
        Padding(
          padding: const EdgeInsets.all(10),
          child: IconButton(
            icon: const Icon(Icons.add_box_outlined),
            color: colorScheme.secondary,
            onPressed: () async {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => ProjectsSelector(fullPath: _relativePath!, groupId: _groupId!))).then((value) {
                if (value != null && value is bool && value) {
                  _tabController.index = 1;
                  mineGlobalKey.currentState?.onRefresh();
                }
              });
            },
          ),
        )
      ]),
      body: _buildBodyView(),
    );
  }

  Scaffold _buildBodyView() {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: SafeArea(child: _buildTabBar()),
        ),
        body: TabBarView(
          controller: _tabController,
          children: [
            Center(child: SubgroupListPage(relativePath: _relativePath!, groupId: _groupId!)),
            Center(
                child: SubgroupIssuesPage(
                    groupId: _groupId!,
                    relativePath: _relativePath!,
                    refreshKey: mineGlobalKey,
                    selectedItemBackground: _showLeading ? Colors.white : Theme.of(context).colorScheme.primary,
                    onSelectedIssueChange: widget.onSelectedIssueChange)),
            Center(child: SubgroupIterationsPage(fullPath: _relativePath!)),
          ],
        ));
  }

  AppTabBar _buildTabBar() {
    return AppTabBar(
      controller: _tabController,
      tabs: [Tab(text: AppLocalizations.dictionary().projectsChildren), Tab(text: AppLocalizations.dictionary().projectsIssues), Tab(text: AppLocalizations.dictionary().iterations)],
    );
  }
}
