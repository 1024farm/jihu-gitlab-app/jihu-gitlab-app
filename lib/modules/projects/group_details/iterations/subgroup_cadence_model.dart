import 'package:jihu_gitlab_app/core/graph_ql_page_info.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/authorized_denied_exception.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';
import 'package:jihu_gitlab_app/modules/iteration/models/cadence.dart';

class SubgroupCadenceModel {
  SubgroupCadenceModel({required this.fullPath});

  String fullPath;

  int pageSize = 20;
  List<Cadence> cadences = [];
  LoadState _loadState = LoadState.loadingState;
  GraphQLPageInfo? cadencesPageInfo;

  Future<Map<String, dynamic>> _fetchCadences() async {
    var params = groupIterationCadenceGraphql(fullPath, pageSize, cadencesPageInfo?.endCursor ?? "");
    var response = await ProjectRequestSender.instance().post(Api.graphql(), params);
    var errors = response.body()["errors"];
    if (errors != null) {
      var errs = errors as List<dynamic>;
      bool isNoFieldsError = errs.any((element) => '${element['message']}'.contains("iterationCadences' doesn't exist"));
      bool isNoPermissionError = errs.any((element) => '${element['message']}'.contains("you don't have permission"));
      if (isNoFieldsError || isNoPermissionError) throw AuthorizedDeniedException();
      throw Exception(errors);
    }
    var iterationCadences = response.body()['data']?['workspace']?['iterationCadences'];
    if (iterationCadences == null) {
      throw AuthorizedDeniedException();
    }
    var items = iterationCadences['nodes'];
    var pageInfo = GraphQLPageInfo.fromJson(iterationCadences['pageInfo']);
    List<Cadence> list = [];
    items.forEach((item) {
      list.add(Cadence.init(item));
    });
    return {'list': list, 'pageInfo': pageInfo};
  }

  Future<bool> refresh() async {
    try {
      cadencesPageInfo = null;
      var result = await _fetchCadences();
      cadences = result['list'];
      cadencesPageInfo = result['pageInfo'];

      _loadState = LoadState.successState;
      return Future.value(true);
    } on AuthorizedDeniedException {
      _loadState = LoadState.authorizedDeniedState;
      return Future.value(false);
    } catch (error) {
      LogHelper.err('SubgroupCadenceModel _fetchCadences refresh error', error);
      _loadState = LoadState.errorState;
      return Future.value(false);
    }
  }

  Future<bool> loadMore() async {
    try {
      var result = await _fetchCadences();
      List<Cadence> moreCadences = result['list'];
      cadences.addAll(moreCadences);
      cadencesPageInfo = result['pageInfo'];
      return Future.value(true);
    } catch (error) {
      LogHelper.err('SubgroupCadenceModel _fetchCadences loadMore error', error);
      return Future.value(false);
    }
  }

  LoadState get loadState {
    return _loadState;
  }

  bool get hasNextPage {
    return cadencesPageInfo?.hasNextPage ?? false;
  }
}

Map<String, Object> groupIterationCadenceGraphql(String fullPath, int pageSize, String afterCursor) {
  return {
    "operationName": "groupIterationCadences",
    "variables": {"beforeCursor": "", "fullPath": fullPath, "firstPageSize": pageSize, "afterCursor": afterCursor},
    "query": """
      query groupIterationCadences(\$fullPath: ID!, \$beforeCursor: String = "", \$afterCursor: String = "", \$firstPageSize: Int, \$lastPageSize: Int) {
        workspace: group(fullPath: \$fullPath) {
          id
          iterationCadences(
            includeAncestorGroups: true
            before: \$beforeCursor
            after: \$afterCursor
            first: \$firstPageSize
            last: \$lastPageSize
          ) {
            nodes {
              id
              title
              durationInWeeks
              automatic
            }
            pageInfo {
              ...PageInfo
            }
          }
        }
      }
      
      fragment PageInfo on PageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
      """
  };
}
