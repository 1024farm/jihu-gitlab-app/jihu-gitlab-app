import 'package:jihu_gitlab_app/core/graph_ql_page_info.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';
import 'package:jihu_gitlab_app/modules/iteration/models/cadence_iteration.dart';

class SubgroupCadenceIterationModel {
  String? fullPath;
  String? iterationCadenceId;

  int pageSize = 20;
  List<CadenceIteration> iterations = [];
  LoadState _loadState = LoadState.noItemState;
  GraphQLPageInfo? iterationsPageInfo;

  Future<Map<String, dynamic>> fetchCadenceIterations() async {
    var response = await ProjectRequestSender.instance().post(Api.graphql(), subgroupCadenceIterationGraphql(fullPath!, iterationCadenceId!, pageSize, iterationsPageInfo?.endCursor ?? ""));
    var items = response.body()['data']['group']['iterations']['nodes'];
    var pageInfo = GraphQLPageInfo.fromJson(response.body()['data']['group']['iterations']['pageInfo']);
    List<CadenceIteration> list = [];
    items.forEach((item) {
      list.add(CadenceIteration.init(item));
    });
    return {'list': list, 'pageInfo': pageInfo};
  }

  Future<bool> refresh({required String fullPath, required String iterationCadenceId}) async {
    try {
      this.fullPath = fullPath;
      this.iterationCadenceId = iterationCadenceId;

      iterationsPageInfo = null;
      var result = await fetchCadenceIterations();
      iterations = result['list'];
      iterationsPageInfo = result['pageInfo'];

      _loadState = LoadState.successState;
      return Future.value(true);
    } catch (error) {
      LogHelper.err("SubgroupCadenceIterationModel refresh error", error);
      _loadState = LoadState.errorState;
      return Future.value(false);
    }
  }

  Future<bool> loadMore() async {
    try {
      var result = await fetchCadenceIterations();
      List<CadenceIteration> moreIterations = result['list'];
      iterations.addAll(moreIterations);
      iterationsPageInfo = result['pageInfo'];
      return Future.value(true);
    } catch (error) {
      LogHelper.err("SubgroupCadenceIterationModel loadMore error", error);
      return Future.value(false);
    }
  }

  LoadState get loadState {
    return _loadState;
  }

  bool get hasNextPage {
    return iterationsPageInfo?.hasNextPage ?? false;
  }
}

Map<String, Object> subgroupCadenceIterationGraphql(String fullPath, String iterationCadenceId, int pageSize, String afterCursor) {
  return {
    "operationName": "groupIterations",
    "variables": {"fullPath": fullPath, "iterationCadenceId": iterationCadenceId, "firstPageSize": pageSize, "state": "all", "sort": "CADENCE_AND_DUE_DATE_DESC", "afterCursor": afterCursor},
    "query": """
      query groupIterations(\$fullPath: ID!, \$iterationCadenceId: IterationsCadenceID!, \$state: IterationState!, \$sort: IterationSort, \$afterCursor: String, \$firstPageSize: Int) {
        group(fullPath: \$fullPath) {
          id
          iterations(
            iterationCadenceIds: [\$iterationCadenceId]
            state: \$state
            sort: \$sort
            after: \$afterCursor
            first: \$firstPageSize
          ) {
            nodes {
              ...IterationListItem
            }
            pageInfo {
              ...PageInfo
            }
          }
        }
      }
      
      fragment PageInfo on PageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
      
      fragment IterationListItem on Iteration {
        id
        dueDate
        scopedPath
        startDate
        state
        title
        webPath
      }
    """
  };
}
