import 'package:jihu_gitlab_app/core/graph_ql_page_info.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';
import 'package:jihu_gitlab_app/modules/iteration/models/cadence_iteration.dart';

class ProjectsCadenceIterationModel {
  String? fullPath;
  String? iterationCadenceId;

  int pageSize = 20;
  List<CadenceIteration> iterations = [];
  LoadState _loadState = LoadState.noItemState;
  GraphQLPageInfo? iterationsPageInfo;

  Future<Map<String, dynamic>> fetchCadenceIterations() async {
    var params = cadenceIterationGraphql(fullPath!, iterationCadenceId!, pageSize, iterationsPageInfo?.endCursor ?? "");
    var response = await ProjectRequestSender.instance().post(Api.graphql(), params);
    var resp = response.body();
    var items = resp['data']['workspace']['iterations']['nodes'];
    var pageInfo = GraphQLPageInfo.fromJson(resp['data']['workspace']['iterations']['pageInfo']);
    List<CadenceIteration> list = [];
    items.forEach((item) {
      list.add(CadenceIteration.init(item));
    });
    return {'list': list, 'pageInfo': pageInfo};
  }

  Future<bool> refresh({required String fullPath, required String iterationCadenceId}) async {
    try {
      this.fullPath = fullPath;
      this.iterationCadenceId = iterationCadenceId;

      iterationsPageInfo = null;
      var result = await fetchCadenceIterations();
      iterations = result['list'];
      iterationsPageInfo = result['pageInfo'];

      _loadState = LoadState.successState;
      return Future.value(true);
    } catch (error) {
      _loadState = LoadState.errorState;
      return Future.value(false);
    }
  }

  Future<bool> loadMore() async {
    try {
      var result = await fetchCadenceIterations();
      List<CadenceIteration> moreIterations = result['list'];
      iterations.addAll(moreIterations);
      iterationsPageInfo = result['pageInfo'];
      return Future.value(true);
    } catch (error) {
      return Future.value(false);
    }
  }

  LoadState get loadState {
    return _loadState;
  }

  bool get hasNextPage {
    return iterationsPageInfo?.hasNextPage ?? false;
  }
}

Map<String, Object> cadenceIterationGraphql(String fullPath, String iterationCadenceId, int pageSize, String afterCursor) {
  return {
    "operationName": "projectIterations",
    "variables": {"fullPath": fullPath, "iterationCadenceId": iterationCadenceId, "firstPageSize": pageSize, "state": "all", "sort": "CADENCE_AND_DUE_DATE_DESC", "afterCursor": afterCursor},
    "query": """
      query projectIterations(\$fullPath: ID!, \$iterationCadenceId: IterationsCadenceID!, \$state: IterationState!, \$sort: IterationSort, \$beforeCursor: String,\$afterCursor: String, \$firstPageSize: Int, \$lastPageSize: Int) {
        workspace: project(fullPath: \$fullPath) {
          id
          iterations(
            iterationCadenceIds: [\$iterationCadenceId]
            state: \$state
            sort: \$sort
            before: \$beforeCursor
            after: \$afterCursor
            first: \$firstPageSize
            last: \$lastPageSize
          ) {
            nodes {
              ...IterationListItem
            }
            pageInfo {
              ...PageInfo
            }
          }
        }
      }
      
      fragment PageInfo on PageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
      
      fragment IterationListItem on Iteration {
        dueDate
        id
        scopedPath
        startDate
        state
        title
        webPath
      }
    """
  };
}
