import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/widgets/app_footer.dart';
import 'package:jihu_gitlab_app/core/widgets/app_header.dart';
import 'package:jihu_gitlab_app/core/widgets/http_fail_view.dart';
import 'package:jihu_gitlab_app/core/widgets/permission_low_view.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/iteration/list/iterations_view.dart';
import 'package:jihu_gitlab_app/modules/iteration/widgets/iteration_tab_item.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/iterations/projects_cadence_iteration_model.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/iterations/projects_cadence_model.dart';

class ProjectsIterationsPage extends StatefulWidget {
  final String fullPath;

  const ProjectsIterationsPage({required this.fullPath, Key? key}) : super(key: key);

  @override
  State<ProjectsIterationsPage> createState() => _ProjectsIterationsPageState();
}

class _ProjectsIterationsPageState extends State<ProjectsIterationsPage> with TickerProviderStateMixin, RestorationMixin {
  ProjectsCadenceModel? _cadenceModel;
  TabController? _tabController;
  final RestorableInt tabIndex = RestorableInt(0);

  final ProjectsCadenceIterationModel _model = ProjectsCadenceIterationModel();
  EasyRefreshController? _refreshController;
  String? iterationCadenceId;
  List<String> tabs = [];

  @override
  String get restorationId => 'iteration_iterations_page_tab';

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {
    registerForRestoration(tabIndex, 'tab_index');
    _tabController!.index = tabIndex.value;
  }

  @override
  void initState() {
    _refreshController = EasyRefreshController(
      controlFinishRefresh: true,
      controlFinishLoad: true,
    );
    _cadenceModel = ProjectsCadenceModel(fullPath: widget.fullPath);
    _refreshTabControllerWithTabLength();
    _requestCadences();
    super.initState();
  }

  @override
  void dispose() {
    _refreshController?.dispose();
    _tabController?.dispose();
    tabIndex.dispose();
    super.dispose();
  }

  void _requestCadences() async {
    bool result = await _cadenceModel!.refresh();
    setState(() {});
    if (result && _cadenceModel!.cadences.isNotEmpty) {
      iterationCadenceId = _cadenceModel!.cadences.first.id;
      _refreshTabControllerWithTabLength();
      _refreshTabs();
      _onRefresh();
    }
  }

  Future<void> _onRefresh() async {
    bool result = await _model.refresh(fullPath: widget.fullPath, iterationCadenceId: iterationCadenceId!);
    setState(() {});
    if (result) {
      _refreshController?.finishRefresh();
      _refreshController?.resetFooter();
    } else {
      _refreshController?.finishRefresh(IndicatorResult.fail);
    }
  }

  void _loadMore() async {
    bool result = await _model.loadMore();
    setState(() {});
    if (result) {
      _refreshController?.finishLoad(_model.hasNextPage ? IndicatorResult.success : IndicatorResult.noMore);
    } else {
      _refreshController?.finishLoad(IndicatorResult.fail);
    }
  }

  void _refreshTabs() {
    tabs = _cadenceModel!.cadences.map((e) => e.title).toList();
  }

  void _refreshTabControllerWithTabLength() {
    _tabController = TabController(initialIndex: 0, length: _cadenceModel!.cadences.isEmpty ? 0 : _cadenceModel!.cadences.length, vsync: this);

    _tabController!.addListener(() {
      setState(() {
        iterationCadenceId = _cadenceModel!.cadences[_tabController!.index].id;
        tabIndex.value = _tabController!.index;
        _onRefresh();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_cadenceModel!.loadState == LoadState.loadingState) return Container();
    if (_cadenceModel!.loadState == LoadState.authorizedDeniedState) return PermissionLowView(featureName: AppLocalizations.dictionary().iterations);
    if (_cadenceModel!.loadState == LoadState.successState && _cadenceModel!.cadences.isEmpty) {
      return TipsView(icon: 'assets/images/no_item.svg', message: AppLocalizations.dictionary().projectsIterationNoItems);
    }
    return NestedScrollView(
        physics: const ClampingScrollPhysics(),
        headerSliverBuilder: (context, value) {
          return [
            SliverToBoxAdapter(
              child: TabBar(
                controller: _tabController,
                isScrollable: true,
                indicatorWeight: 0.01,
                labelColor: Theme.of(context).primaryColor,
                unselectedLabelColor: const Color(0xFF03162F),
                splashFactory: NoSplash.splashFactory,
                padding: const EdgeInsets.only(bottom: 10, left: 16, right: 16),
                labelPadding: const EdgeInsets.only(right: 10, left: 0),
                tabs: [
                  for (int index = 0; index < tabs.length; index++) tabItem(title: tabs[index], isSelected: index == _tabController!.index),
                ],
              ),
            ),
          ];
        },
        body: TabBarView(
          physics: const NeverScrollableScrollPhysics(),
          controller: _tabController,
          children: <Widget>[
            for (final _ in tabs)
              EasyRefresh(
                controller: _refreshController!,
                header: AppHeader(),
                footer: AppFooter(),
                onRefresh: _onRefresh,
                onLoad: _loadMore,
                child: iterationListView(),
              ),
          ],
        ));
  }

  Widget tabItem({required String title, bool isSelected = false}) {
    return IterationTabItem(title: title, isSelected: isSelected);
  }

  Widget iterationListView() {
    if (_model.loadState == LoadState.errorState) return const HttpFailView();
    if (_model.loadState == LoadState.successState && _model.iterations.isEmpty) {
      return TipsView(
        icon: 'assets/images/no_item.svg',
        message: AppLocalizations.dictionary().projectsIterationNoItems,
      );
    }
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: IterationsView(
        iterations: _model.iterations,
        source: IterationListViewSource.project,
        fullPath: widget.fullPath,
        title: tabs[_tabController!.index],
      ),
    );
  }
}
