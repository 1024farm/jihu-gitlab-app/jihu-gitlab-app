import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/connection_selector.dart';
import 'package:jihu_gitlab_app/core/widgets/home/home_scaffold.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project_repository.dart';
import 'package:jihu_gitlab_app/modules/projects/groups/projects_groups_page.dart';
import 'package:jihu_gitlab_app/modules/projects/projects_title_tab_bar.dart';
import 'package:jihu_gitlab_app/modules/projects/starred/projects_starred_page.dart';
import 'package:provider/provider.dart';

class ProjectsPage extends StatefulWidget {
  const ProjectsPage({Key? key}) : super(key: key);

  @override
  State<ProjectsPage> createState() => _ProjectsPageState();
}

class _ProjectsPageState extends State<ProjectsPage> with TickerProviderStateMixin {
  late TabController _tabController;
  final GroupAndProjectRepository _repository = GroupAndProjectRepository.instance();

  @override
  void initState() {
    super.initState();
    _tabController = TabController(initialIndex: 0, length: 2, vsync: this);
    if (ConnectionProvider.authorized) {
      _repository.countStarred(ConnectionProvider.connectionId!).then((value) {
        _tabController.index = value > 0 ? 0 : 1;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final colorScheme = Theme.of(context).colorScheme;
    return Consumer<ConnectionProvider>(builder: (context, _, child) {
      return HomeScaffold.withAppBar(
        params: HomeAppBarRequiredParams(
          context: context,
          flexibleSpace: SafeArea(
            child: ProjectsTitleTabBarView(tabController: _tabController),
          ),
          actions: [const ConnectionSelector(key: Key('ProjectsConnectionSelector'))],
        ),
        body: TabBarView(
          key: const Key('ProjectsPageTabBarView'),
          controller: _tabController,
          children: const [
            Center(child: ProjectsStarredPage()),
            Center(child: ProjectsGroupsPage()),
          ],
        ),
        backgroundColor: colorScheme.background,
      );
    });
  }
}
