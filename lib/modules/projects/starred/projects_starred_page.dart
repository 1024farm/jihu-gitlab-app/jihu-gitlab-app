import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project_list_tile.dart';
import 'package:jihu_gitlab_app/modules/projects/starred/projects_starred_model.dart';
import 'package:provider/provider.dart';

class ProjectsStarredPage extends StatefulWidget {
  const ProjectsStarredPage({super.key});

  @override
  State<ProjectsStarredPage> createState() => _ProjectsStarredPageState();
}

class _ProjectsStarredPageState extends State<ProjectsStarredPage> {
  final ProjectsStarredModel _model = locator<ProjectsStarredModel>();

  @override
  void initState() {
    super.initState();
    _model.config();
    _model.loadData();
  }

  @override
  void dispose() {
    _model.clear();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Consumer<ConnectionProvider>(builder: (context, _, child) {
        return Padding(padding: const EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 0), child: _starredListView());
      }),
    );
  }

  Widget _starredListView() {
    return ValueListenableBuilder<List<GroupAndProject>>(
        valueListenable: _model.notifier,
        builder: (BuildContext context, List<GroupAndProject> data, Widget? child) {
          return ClipRRect(
            borderRadius: BorderRadius.circular(4.0),
            child: ListView.separated(
                itemCount: data.length,
                separatorBuilder: (context, index) => const Divider(height: .5, indent: 12.0, endIndent: 12.0, color: Color(0xFFDDDDDD)),
                itemBuilder: (context, index) => GroupAndProjectListTile(
                      itemDataProvider: () => data[index],
                      starChanged: () => _model.loadLocalDataAndNotify(),
                    )),
          );
        });
  }
}
