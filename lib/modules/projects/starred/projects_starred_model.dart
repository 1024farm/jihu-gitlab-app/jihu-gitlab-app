import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/recommend_projects.dart';
import 'package:jihu_gitlab_app/core/stars_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/selector/data_provider.dart';
import 'package:jihu_gitlab_app/main.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/starred/projects_starred_provider.dart';

class ProjectsStarredModel {
  LoadState _loadState = LoadState.noItemState;
  late final ValueNotifier<List<GroupAndProject>> _notifier = ValueNotifier(_data);
  DataProvider<GroupAndProject>? _dataProvider;
  final List<GroupAndProject> _data = [];

  void config() {
    StarsProvider().addListener(listener);
    ConnectionProvider().addListener(listener);
  }

  void listener() {
    _dataProvider = null;
    loadData();
  }

  Future<void> loadData() async {
    _initializeRecommendProjects();
    if (ConnectionProvider.connection == null) {
      _notifier.value = _data;
      _loadState = LoadState.successState;
      return;
    }
    _dataProvider ??= ProjectsStarredProvider(userId: ConnectionProvider.connectionId!);
    loadLocalDataAndNotify();
    _dataProvider?.syncFromRemote().then((value) {
      if (value) {
        loadLocalDataAndNotify();
      }
    });
  }

  void loadLocalDataAndNotify() {
    _dataProvider?.loadFromLocal().then((value) {
      _initializeRecommendProjects();
      _data.addAll(value);
      _notifier.value = List.of(_data);
      _loadState = LoadState.successState;
    });
  }

  void clear() {
    _data.clear();
    _loadState = LoadState.noItemState;
    _dataProvider = null;
    ConnectionProvider().removeListener(listener);
    StarsProvider().removeListener(listener);
  }

  void refresh() {
    loadLocalDataAndNotify();
  }

  void _initializeRecommendProjects() {
    _data.clear();
    var context = navigatorKey.currentContext;
    if (context != null) {
      _data.insertAll(0, RecommendProjects().get(Localizations.localeOf(context)).map((e) => e.toGroupAndProject()).toList());
    }
  }

  LoadState get loadState => _loadState;

  ValueNotifier<List<GroupAndProject>> get notifier => _notifier;

  @visibleForTesting
  void injectDataProviderForTesting(DataProvider<GroupAndProject> dataProvider) => _dataProvider = dataProvider;
}
