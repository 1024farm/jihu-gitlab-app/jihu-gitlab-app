import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/domain/global_time.dart';
import 'package:jihu_gitlab_app/core/list_comparator.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/synchronizer.dart';
import 'package:jihu_gitlab_app/core/widgets/selector/data_provider.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project_repository.dart';

class ProjectsStarredProvider extends DataProvider<GroupAndProject> {
  late int _userId;
  late GroupAndProjectRepository _repository;

  ProjectsStarredProvider({required int userId}) {
    _userId = userId;
    _repository = GroupAndProjectRepository.instance();
  }

  @override
  Future<List<GroupAndProject>> loadFromLocal() async {
    List<GroupAndProjectEntity> groupAndProjectEntities = await _repository.findStarredList(userId: _userId);
    return groupAndProjectEntities.map((e) => e.asDomain()).toList();
  }

  @override
  Future<bool> syncFromRemote() async {
    int version = GlobalTime.now().millisecondsSinceEpoch;
    return await _syncStarredProjects(version);
  }

  Future<bool> _syncStarredProjects(int version) async {
    return Synchronizer<dynamic>(
        url: Api.join('users/$_userId/starred_projects'),
        dataProcessor: (data, page, pageSize) async {
          List<GroupAndProjectEntity> starredProjectIds = (data as List).map((e) => GroupAndProjectEntity.createStarredProjectEntity(e, _userId, version)).toList();
          await _save(starredProjectIds, version);
          return starredProjectIds.length >= pageSize;
        }).run();
  }

  Future<void> _save(List<GroupAndProjectEntity> starredProjects, int version) async {
    List<int> iidList = starredProjects.map((e) => e.iid).toList();
    List<GroupAndProjectEntity> exists = await _repository.findStarredList(userId: _userId, iidList: iidList);
    ListCompareResult<GroupAndProjectEntity> result = ListComparator.compare<GroupAndProjectEntity>(exists, starredProjects);
    if (result.hasAdd) {
      await _repository.insert(result.add);
    }
    if (result.hasUpdate) {
      var list = result.update.map((e) {
        e.left.name = e.right.name;
        e.left.relativePath = e.right.relativePath;
        e.left.starred = 1;
        e.left.version = version;
        return e.left;
      }).toList();
      await _repository.update(list);
    }
    await _repository.cancelStarredProjectsLessThan(_userId, version);
  }

  @visibleForTesting
  void injectRepositoryForTesting(GroupAndProjectRepository groupAndProjectRepository) {
    _repository = groupAndProjectRepository;
  }
}
