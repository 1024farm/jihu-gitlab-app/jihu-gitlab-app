import 'package:flutter/foundation.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/widgets/selector/data_provider.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/groups/group_provider.dart';

class ProjectsGroupsModel {
  LoadState _loadState = LoadState.noItemState;
  final List<GroupAndProject> _data = [];
  late ValueNotifier<List<GroupAndProject>> _notifier;
  DataProvider<GroupAndProject>? _dataProvider;

  void init() {
    _dataProvider ??= GroupProvider(userId: ConnectionProvider.connectionId!, parentId: 0);
  }

  void initListener() {
    _notifier = ValueNotifier(_data);
    ConnectionProvider().addListener(listener);
  }

  void listener() {
    if (ConnectionProvider.connection == null) return;
    _dataProvider = GroupProvider(userId: ConnectionProvider.connectionId!, parentId: 0);
    loadData();
  }

  Future<void> loadData() async {
    _loadLocalDataAndNotify();
    _dataProvider?.syncFromRemote().then((value) {
      if (value) {
        _loadLocalDataAndNotify();
      }
    });
  }

  Future<void> _loadLocalDataAndNotify() async {
    List<GroupAndProject> data = await _dataProvider!.loadFromLocal();
    _data.clear();
    _data.addAll(data);
    _notifier.value = data;
    _loadState = LoadState.successState;
  }

  ValueNotifier<List<GroupAndProject>> get notifier => _notifier;

  @visibleForTesting
  void injectDataProviderForTesting(DataProvider<GroupAndProject> dataProvider) => _dataProvider = dataProvider;

  void clear() {
    _loadState = LoadState.noItemState;
    _dataProvider = null;
    ConnectionProvider().removeListener(listener);
  }

  LoadState get loadState => _loadState;
}
