import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

enum MilestoneAndIterationState {
  active(Color(0xFF16A926), Color(0x1400E94B)),
  closed(Color(0xFFE24329), Color(0xFFFFF6F5)),
  upcoming(AppThemeData.secondaryColor, Color(0x1F66696D)),
  current(Color(0xFF16A926), Color(0x1400E94B));

  final Color textColor;
  final Color backgroundColor;

  const MilestoneAndIterationState(this.textColor, this.backgroundColor);

  static MilestoneAndIterationState? valueOf(String name) {
    var values = MilestoneAndIterationState.values.where((element) => element.name == name);
    return values.isNotEmpty ? values.first : null;
  }

  String get label {
    if (this == MilestoneAndIterationState.active) {
      return AppLocalizations.dictionary().issueOpenState;
    }
    if (this == MilestoneAndIterationState.current) {
      return AppLocalizations.dictionary().projectsIterationCurrent;
    }
    if (this == MilestoneAndIterationState.upcoming) {
      return AppLocalizations.dictionary().projectsIterationUpcoming;
    }
    return AppLocalizations.dictionary().issueClosedState;
  }
}

class MilestoneAndIterationStateView extends StatelessWidget {
  const MilestoneAndIterationStateView({required this.state, Key? key}) : super(key: key);
  final String? state;

  @override
  Widget build(BuildContext context) {
    var s = MilestoneAndIterationState.valueOf(state ?? '');
    if (s == null) {
      return const SizedBox();
    }
    return Container(
        padding: const EdgeInsets.symmetric(vertical: 2, horizontal: 6),
        decoration: BoxDecoration(color: s.backgroundColor, borderRadius: const BorderRadius.all(Radius.circular(2))),
        child: Text(s.label, style: TextStyle(color: s.textColor, fontSize: 13, fontWeight: FontWeight.normal)));
  }
}
