import 'package:flutter/cupertino.dart';
import 'package:jihu_gitlab_app/core/domain/epic.dart';
import 'package:jihu_gitlab_app/core/domain/issue.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';
import 'package:jihu_gitlab_app/core/widgets/charts/time_box_report.dart';
import 'package:jihu_gitlab_app/modules/issues/grouped_issues/grouped_issues_model.dart';
import 'package:jihu_gitlab_app/modules/issues/list/project_issues_fetcher.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestones_fetcher.dart';

class ProjectMilestoneModel {
  final String _fullPath;
  GroupedIssues _groupedIssues = GroupedIssues.init();
  late ValueNotifier<GroupedIssues> _notifier;
  late final Milestone _milestone;

  ProjectMilestoneModel(this._fullPath, this._milestone) {
    _notifier = ValueNotifier(_groupedIssues);
  }

  ValueNotifier<GroupedIssues> get notifier => _notifier;

  Milestone get milestone => _milestone;

  Future<void> getProjectMilestoneInfo() async {
    try {
      var first100Issues = await _getIssues();
      if (first100Issues.isEmpty) {
        _groupedIssues = GroupedIssues.empty();
      } else {
        var timeBoxReport = await _getTimeBoxReport();
        _groupedIssues = GroupedIssues.success(description: null, timeBoxReport: timeBoxReport, first100Issues: first100Issues);
      }
    } catch (e) {
      debugPrint(e.toString());
      _groupedIssues = GroupedIssues.fail();
    }
    _notifier.value = _groupedIssues;
  }

  Future<TimeBoxReport?> _getTimeBoxReport() async {
    var body = (await ProjectRequestSender.instance().post(Api.graphql(), requestBodyOfGetMilestoneTimeBoxReport(_fullPath, _milestone.id))).body();
    var report = body['data']?['milestone']?['report'];
    return report != null ? TimeBoxReport.fromJson(report, _milestone.startDate, _milestone.dueDate) : null;
  }

  Future<First100Issues> _getIssues() async {
    List<Issue> issues = (await ProjectIssuesFetcher().fetchIssues(() => requestBodyOfGetFirst100IssueByMilestoneTitle(_fullPath, _milestone.title))).list;
    var response = await ProjectRequestSender.instance().post(Api.graphql(), requestBodyOfGetFirst100IssueExtraByMilestoneTitle(_fullPath, _milestone.title));
    bool canGroupByEpic = false;
    if (response.body()['errors'] == null) {
      canGroupByEpic = true;
      List<dynamic> resp = response.body()['data']?['project']?['issues']?['nodes'] ?? [];
      var map = {for (var item in resp) item['iid']: item};
      for (var issue in issues) {
        var key = issue.iid.toString();
        var epicTitle = map[key]?["epic"]?['title'];
        if (epicTitle != null) issue.epic = Epic(epicTitle);
        issue.weight = map[key]?['weight'];
      }
    }
    return Future(() => First100Issues(issues, canGroupByEpic: canGroupByEpic));
  }
}

Map<String, Object> requestBodyOfGetFirst100IssueByMilestoneTitle(String fullPath, String milestoneTitle) {
  return {
    "variables": {"fullPath": fullPath, "milestoneTitle": milestoneTitle},
    "query": """
      query getFirst100IssuesByTitle(\$fullPath: ID!, \$milestoneTitle: String) {
        project(fullPath: \$fullPath) {
          id
          name
          fullPath
          issues(milestoneTitle:[\$milestoneTitle], first: 100, sort: RELATIVE_POSITION_ASC){
            nodes{
              ...Issue
            }
          }
        }
      }
      
      fragment Issue on Issue {
          id
          iid
          title
          webUrl
          state
          author {
            id
            name
            username
            avatarUrl
          }
          assignees {
            nodes {
              id
              name
              username
              avatarUrl
            }
          }
        }
    """
  };
}

Map<String, Object> requestBodyOfGetFirst100IssueExtraByMilestoneTitle(String fullPath, String milestoneTitle) {
  return {
    "variables": {"fullPath": fullPath, "milestoneTitle": milestoneTitle},
    "query": """
      query getFirst100IssuesExtraByTitle(\$fullPath: ID!, \$milestoneTitle: String) {
        project(fullPath: \$fullPath) {
          id
          name
          fullPath
          issues(milestoneTitle:[\$milestoneTitle], first: 100, sort: RELATIVE_POSITION_ASC){
            nodes{
              iid
              epic {
                title
              }
              weight
            }
          }
        }
      }
    """
  };
}

Map<String, Object> requestBodyOfGetMilestoneTimeBoxReport(String fullPath, String milestoneId) {
  return {
    "variables": {"fullPath": fullPath, "id": milestoneId},
    "query": """
      query getMilestoneTimeBoxReport(\$fullPath: String!,\$id: MilestoneID!) {
        milestone(id: \$id) {
          startDate
          dueDate
          report(fullPath:\$fullPath){
            burnupTimeSeries{
              date
              scopeCount
              scopeWeight
              completedCount
              completedWeight
            }
          }
        }
      }
    """
  };
}
