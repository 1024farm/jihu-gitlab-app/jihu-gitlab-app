import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/milestone_and_iteration_duration_view.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/milestone_and_iteration_state_view.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestones_fetcher.dart';

class ProjectMilestoneItemView extends StatefulWidget {
  const ProjectMilestoneItemView({required this.milestone, Key? key}) : super(key: key);
  final Milestone milestone;

  @override
  State<ProjectMilestoneItemView> createState() => _ProjectMilestoneItemViewState();
}

class _ProjectMilestoneItemViewState extends State<ProjectMilestoneItemView> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(widget.milestone.title, style: const TextStyle(color: Color(0xFF03162F), fontSize: 15, fontWeight: FontWeight.w500)),
        if (widget.milestone.startDate != null || widget.milestone.dueDate != null) ...[
          const SizedBox(height: 8),
          MilestoneAndIterationDurationView(
            widget.milestone.startDate,
            widget.milestone.dueDate,
            style: const TextStyle(color: AppThemeData.secondaryColor, fontSize: 13, fontWeight: FontWeight.normal),
          )
        ],
        if (widget.milestone.state != null) ...[const SizedBox(height: 8), MilestoneAndIterationStateView(state: widget.milestone.state!)]
      ],
    );
  }
}
