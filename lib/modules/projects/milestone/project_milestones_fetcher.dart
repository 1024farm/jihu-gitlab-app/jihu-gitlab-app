import 'package:jihu_gitlab_app/core/graph_ql_page_info.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';
import 'package:jihu_gitlab_app/core/paginated_graphql_response.dart';

class Milestone {
  final String id;
  final String title;
  final String? startDate;
  final String? dueDate;
  final String? state;

  Milestone(this.id, this.title, this.startDate, this.dueDate, this.state);

  factory Milestone.fromJson(Map<String, dynamic> json) {
    return Milestone(json['id'] ?? '', json['title'] ?? '', json['startDate'], json['dueDate'], json['state']);
  }
}

class ProjectMilestoneFetcher {
  static const int pageSize = 20;
  bool _isOldVersionApi = false;
  final String _fullPath;
  final String? _expectState;

  ProjectMilestoneFetcher(this._fullPath, [this._expectState]);

  Future<PaginatedGraphQLResponse<Milestone>> fetch([String endCursor = '']) async {
    Map<String, dynamic> body;
    if (_isOldVersionApi) {
      body = (await ProjectRequestSender.instance().post(Api.graphql(), projectMilestoneRequestBody(_fullPath, pageSize, endCursor, state: _expectState, withSort: false))).body();
    } else {
      body = (await ProjectRequestSender.instance().post(Api.graphql(), projectMilestoneRequestBody(_fullPath, pageSize, endCursor, state: _expectState))).body();
      if (body['errors']?[0]?['message'] == "Field 'milestones' doesn't accept argument 'sort'") {
        body = (await ProjectRequestSender.instance().post(Api.graphql(), projectMilestoneRequestBody(_fullPath, pageSize, endCursor, state: _expectState, withSort: false))).body();
        _isOldVersionApi = true;
      }
    }
    var milestonesBody = body['data']?['project']?['milestones'];
    var pageInfo = GraphQLPageInfo.fromJson(milestonesBody['pageInfo'] ?? {});
    List<Milestone> milestones = ((milestonesBody["nodes"] ?? []) as List).map((e) => Milestone.fromJson(e)).toList();
    return PaginatedGraphQLResponse<Milestone>(pageInfo, milestones);
  }

  bool get isOldVersionApi => _isOldVersionApi;
}

Map<String, Object> projectMilestoneRequestBody(String fullPath, int pageSize, String afterCursor, {String? state, bool withSort = true}) {
  return {
    "variables": {"fullPath": fullPath, "includeAncestors": false, "firstPageSize": pageSize, "afterCursor": afterCursor, "state": state},
    "query": """
      query getProjectMilestones(\$fullPath: ID!, \$includeAncestors: Boolean, \$afterCursor: String, \$firstPageSize: Int,\$state: MilestoneStateEnum
) {
        project(fullPath: \$fullPath) {
          id
          milestones(
            includeAncestors: \$includeAncestors
            ${withSort ? "sort: DUE_DATE_DESC" : ""}
            after: \$afterCursor
            first: \$firstPageSize
            state: \$state
          ) {
            nodes {
              ...Milestone
            }
            pageInfo {
              ...PageInfo
            }
          }
        }
      }
      
      fragment PageInfo on PageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
      
      fragment Milestone on Milestone{
        id
        title
        startDate
        dueDate
        state
      }
    """
  };
}
