import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/widgets/round_rect_container.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/details/paginable_list_view.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestone_item_view.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestone_page.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestones_fetcher.dart';

class ProjectMilestonesPage extends StatefulWidget {
  final String fullPath;

  const ProjectMilestonesPage({required this.fullPath, Key? key}) : super(key: key);

  @override
  State<ProjectMilestonesPage> createState() => _ProjectMilestonesPageState();
}

class _ProjectMilestonesPageState extends State<ProjectMilestonesPage> {
  late ProjectMilestoneFetcher _projectMilestoneFetcher;
  late PaginableListViewModel<Milestone> _paginableListViewModel;

  @override
  void initState() {
    _projectMilestoneFetcher = ProjectMilestoneFetcher(widget.fullPath);
    _paginableListViewModel = PaginableListViewModel(
      onRefresh: (_) => _projectMilestoneFetcher.fetch(),
      onLoadMore: (endCursor, _) => _projectMilestoneFetcher.fetch(endCursor),
      featureName: AppLocalizations.dictionary().milestones,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return PaginableListView<Milestone>(
      model: _paginableListViewModel,
      itemBuilder: <Milestone>(context, index, data) {
        var milestone = data[index];
        return InkWell(
          onTap: () => _onMilestoneTapped(milestone),
          child: RoundRectContainer(
            margin: const EdgeInsets.symmetric(horizontal: 16),
            padding: const EdgeInsets.all(12),
            child: ProjectMilestoneItemView(milestone: milestone),
          ),
        );
      },
      separatorBuilder: (context, index) => const Divider(height: 12, thickness: 12, color: Colors.transparent),
    );
  }

  void _onMilestoneTapped(milestone) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => ProjectMilestonePage(fullPath: widget.fullPath, milestone: milestone)));
  }
}
