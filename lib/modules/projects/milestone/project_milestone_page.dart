import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/modules/issues/grouped_issues/grouped_issues_model.dart';
import 'package:jihu_gitlab_app/modules/issues/grouped_issues/grouped_issues_view.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestone_model.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestones_fetcher.dart';

class ProjectMilestonePage extends StatefulWidget {
  final String fullPath;
  final Milestone milestone;

  const ProjectMilestonePage({required this.fullPath, required this.milestone, Key? key}) : super(key: key);

  @override
  State<ProjectMilestonePage> createState() => _ProjectMilestonePageState();
}

class _ProjectMilestonePageState extends State<ProjectMilestonePage> {
  late final ProjectMilestoneModel _model;

  @override
  void initState() {
    _model = ProjectMilestoneModel(widget.fullPath, widget.milestone)..getProjectMilestoneInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<GroupedIssues>(
        valueListenable: _model.notifier,
        builder: (context, data, child) {
          return GroupedIssuesView(
            onRetry: _model.getProjectMilestoneInfo,
            state: _model.milestone.state,
            startDate: _model.milestone.startDate,
            dueDate: _model.milestone.dueDate,
            fromGroup: false,
            groupingDefault: false,
            groupedIssues: data,
            title: _model.milestone.title,
          );
        });
  }
}
