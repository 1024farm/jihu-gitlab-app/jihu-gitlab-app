import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/time.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class MilestoneAndIterationDurationView extends StatelessWidget {
  const MilestoneAndIterationDurationView(this.startDate, this.dueDate, {this.style = const TextStyle(color: Color(0xFF171321), fontSize: 14, fontWeight: FontWeight.w600), super.key});

  final String? startDate;
  final String? dueDate;
  final TextStyle? style;

  @override
  Widget build(BuildContext context) {
    var date = _displayDate();
    return date == null ? const SizedBox() : Text(date, style: style);
  }

  String? _displayDate() {
    if (startDate == null && dueDate == null) {
      return null;
    }
    var start = Time.init(startDate ?? '').formatAsDate();
    var due = Time.init(dueDate ?? '').formatAsDate();
    if (start == null) {
      return AppLocalizations.dictionary().expiresOn(due!);
    }
    if (due == null) {
      return AppLocalizations.dictionary().startedOn(start);
    }
    return "$start - $due";
  }
}
