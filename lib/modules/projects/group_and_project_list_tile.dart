import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/star.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';

class GroupAndProjectListTile extends StatefulWidget {
  final GroupAndProject? data;
  final ItemDataProvider? itemDataProvider;
  final VoidCallback? starChanged;

  const GroupAndProjectListTile({this.data, this.itemDataProvider, this.starChanged, Key? key}) : super(key: key);

  @override
  State<GroupAndProjectListTile> createState() => _GroupAndProjectListTileState();
}

class _GroupAndProjectListTileState extends State<GroupAndProjectListTile> {
  @override
  Widget build(BuildContext context) {
    var data = widget.itemDataProvider!();
    return InkWell(
      onTap: () => _onTap(data),
      child: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
        ),
        padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 12),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SvgPicture.asset(data.icon, width: 16, height: 16),
            const SizedBox(width: 12),
            Expanded(
              flex: 1,
              child: Text(data.name, maxLines: 1, overflow: TextOverflow.ellipsis, style: const TextStyle(color: Color(0xFF1A1B36), fontSize: 14, fontWeight: FontWeight.w500)),
            ),
            if (!data.recommend) Star(key: Key('${data.iid}_${data.name}'), itemDataProvider: widget.itemDataProvider!, starChanged: widget.starChanged)
          ],
        ),
      ),
    );
  }

  void _onTap(GroupAndProject item) async {
    if (item.type == SubgroupItemType.group || item.type == SubgroupItemType.subgroup) {
      ProjectProvider().clear();
      final params = <String, dynamic>{'groupId': item.iid, 'name': item.name, 'relativePath': item.relativePath};
      await Navigator.of(context).pushNamed(SubgroupPage.routeName, arguments: params);
      widget.starChanged?.call();
    } else {
      if (item.recommend) {
        ProjectProvider().changeProject(item.asProject, item.destinationHost).then((value) {
          final params = <String, dynamic>{'projectId': item.iid, 'name': item.name, 'relativePath': item.relativePath, "groupId": item.parentId};
          Navigator.of(context).pushNamed(ProjectPage.routeName, arguments: params);
        });
      } else {
        ProjectProvider().changeProject(item.asProject, null).then((value) {
          final params = <String, dynamic>{'projectId': item.iid, 'name': item.name, 'relativePath': item.relativePath, "groupId": item.parentId};
          Navigator.of(context).pushNamed(ProjectPage.routeName, arguments: params);
        });
      }
    }
  }
}
