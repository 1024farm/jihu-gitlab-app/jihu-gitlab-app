import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/modules/issues/details/paginable_list_view.dart';
import 'package:jihu_gitlab_app/modules/projects/releases/project_release_item_view.dart';
import 'package:jihu_gitlab_app/modules/projects/releases/project_releases_model.dart';
import 'package:jihu_gitlab_app/modules/projects/releases/release.dart';

class ProjectReleasesPage extends StatefulWidget {
  const ProjectReleasesPage({required this.fullPath, Key? key}) : super(key: key);
  final String fullPath;

  @override
  State<ProjectReleasesPage> createState() => ProjectReleasesPageState();
}

class ProjectReleasesPageState extends State<ProjectReleasesPage> {
  late ProjectReleasesModel _model;
  String? _expandedReleaseId;
  bool _expandChanged = false;

  @override
  void initState() {
    _model = ProjectReleasesModel(widget.fullPath);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return PaginableListView<Release>(
      model: _model.paginableListViewModel,
      itemBuilder: (context, index, data) {
        Release release = data[index];
        return ProjectReleaseItemView(
          release: release,
          fullPath: _model.fullPath,
          expanded: (!_expandChanged && _expandedReleaseId == null && index == 0) || _expandedReleaseId == release.id,
          onExpandedChange: (expand) {
            setState(() {
              _expandChanged = true;
              if (expand) {
                _expandedReleaseId = release.id;
              } else {
                _expandedReleaseId = null;
              }
            });
          },
        );
      },
      separatorBuilder: (context, index) => const Divider(height: 12, thickness: 12, color: Colors.transparent),
    );
  }

  Future<void> refresh() => _model.paginableListViewModel.refresh();
}
