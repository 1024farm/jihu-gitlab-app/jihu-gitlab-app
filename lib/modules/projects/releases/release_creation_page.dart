import 'dart:core';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/clock/global_glock.dart';
import 'package:jihu_gitlab_app/core/project_identity.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/delimited_column.dart';
import 'package:jihu_gitlab_app/core/widgets/markdown/markdown_input_box.dart';
import 'package:jihu_gitlab_app/core/widgets/platform_adaptive_text_field.dart';
import 'package:jihu_gitlab_app/core/widgets/single_choice_input.dart';
import 'package:jihu_gitlab_app/core/widgets/toast.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/details/milestone_selector_page.dart';
import 'package:jihu_gitlab_app/modules/projects/releases/release_asset_link_input_view.dart';
import 'package:jihu_gitlab_app/modules/projects/releases/release_creation_model.dart';
import 'package:jihu_gitlab_app/modules/projects/releases/tag_selector_page.dart';

class ReleaseCreationPage extends StatefulWidget {
  final ProjectIdentity projectIdentity;

  const ReleaseCreationPage({required this.projectIdentity, Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ReleaseCreationPageState();
}

class _ReleaseCreationPageState extends State<ReleaseCreationPage> {
  late ReleaseCreationModel _model;
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    _model = ReleaseCreationModel(widget.projectIdentity);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CommonAppBar(
        showLeading: true,
        title: Text(AppLocalizations.dictionary().createIssue(AppLocalizations.dictionary().release)),
        actions: [
          Opacity(
              opacity: _model.submittable ? 1 : 0.5,
              child: TextButton(
                  onPressed: _model.submittable
                      ? () {
                          _model.createRelease().then((value) => Navigator.of(context).pop(true)).catchError((error) => Toast.error(context, error.message));
                        }
                      : null,
                  child: Text(AppLocalizations.dictionary().create, style: const TextStyle(color: AppThemeData.primaryColor)))),
        ],
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          controller: _scrollController,
          child: Container(
            margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildTagChoiceInputView(),
                const SizedBox(height: 16),
                _buildReleaseTitleView(),
                const SizedBox(height: 16),
                _buildMilestoneChoiceInputView(),
                const SizedBox(height: 16),
                _buildReleaseDateView(),
                const SizedBox(height: 16),
                _buildReleaseNotesView(),
                const SizedBox(height: 16),
                _buildReleaseAssetsView(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildReleaseTitleView() {
    return InputWidthTitle(title: AppLocalizations.dictionary().releaseTitle, child: PlatformAdaptiveTextField(controller: _model.titleController));
  }

  Widget _buildTagChoiceInputView() {
    return InputWidthTitle(
        title: AppLocalizations.dictionary().tagName,
        child: SingleChoiceInput(
          placeHolder: AppLocalizations.dictionary().noTagSelected,
          value: _model.selectedTagName,
          onClear: () => setState(() => _model.changeSelectedTag(null)),
          onAdd: () async {
            var selectedTagName = await Navigator.of(context).push(MaterialPageRoute(builder: (context) {
              return TagSelectorPage(projectId: widget.projectIdentity.id, selectedName: _model.selectedTagName);
            }));
            setState(() => _model.changeSelectedTag(selectedTagName));
          },
        ));
  }

  Widget _buildMilestoneChoiceInputView() {
    return InputWidthTitle(
        title: AppLocalizations.dictionary().milestones,
        child: SingleChoiceInput(
          placeHolder: AppLocalizations.dictionary().noMilestone,
          value: _model.selectedMilestone?.title,
          onAdd: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => MilestoneSelectorPage(fullPath: _model.projectIdentity.fullPath))).then((value) {
              setState(() => _model.changeSelectedMilestone(value));
            });
          },
          onClear: () => setState(() => _model.changeSelectedMilestone(null)),
        ));
  }

  Widget _buildReleaseDateView() {
    return InputWidthTitle(
        title: AppLocalizations.dictionary().releaseDate,
        child: SingleChoiceInput(
          value: _model.formattedReleaseDate,
          clearable: false,
          onAdd: () async {
            var datetime = await _showDatePickerDialog(_model.releaseDate);
            setState(() => _model.changeReleaseDate(datetime));
          },
          onClear: () => setState(() => _model.changeReleaseDate(GlobalClock.now())),
        ));
  }

  Future<DateTime> _showDatePickerDialog(DateTime initValue) async {
    return await showModalBottomSheet(
        context: context,
        builder: (context) {
          DateTime datetime = initValue;
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TextButton(onPressed: () => Navigator.of(context).pop(), child: Text(AppLocalizations.dictionary().cancel)),
                  TextButton(onPressed: () => Navigator.of(context).pop(datetime), child: Text(AppLocalizations.dictionary().confirm))
                ],
              ),
              SizedBox(
                  height: 260,
                  child: CupertinoDatePicker(
                    onDateTimeChanged: (value) => datetime = value,
                    initialDateTime: initValue,
                    mode: CupertinoDatePickerMode.date,
                  )),
            ],
          );
        });
  }

  Widget _buildReleaseNotesView() {
    return InputWidthTitle(
        title: AppLocalizations.dictionary().releaseNotes,
        child: MarkdownInputBox(
          controller: _model.descriptionController,
          projectId: _model.projectIdentity.id,
          onChanged: () {},
        ));
  }

  Widget _buildReleaseAssetsView() {
    return InputWidthTitle(
        title: AppLocalizations.dictionary().releaseAssets,
        child: _ReleaseAssetLinksView(
          controller: _model.releaseAssetLinksController,
          onLinksCountChange: () {
            setState(() {
              _scrollController.jumpTo(_scrollController.position.minScrollExtent);
            });
          },
        ));
  }
}

class ReleaseAssetLinksController {
  final Map<Key, ReleaseAssetLink> _assetLinks = {};

  List<ReleaseAssetLink> get assetLinks => List.from(_assetLinks.values);

  set assetLinks(List<ReleaseAssetLink> value) {}

  void remove(Key key) {
    _assetLinks.removeWhere((k, value) => k == key);
  }

  void put(Key key, ReleaseAssetLink releaseAssetLink) {
    _assetLinks[key] = releaseAssetLink;
  }

  bool validate(context) {
    for (var value in _assetLinks.values) {
      var message = value.validate();
      if (message != null) {
        Toast.error(context, message);
        return false;
      }
    }
    return true;
  }
}

class _ReleaseAssetLinksView extends StatefulWidget {
  const _ReleaseAssetLinksView({required this.onLinksCountChange, required this.controller, Key? key}) : super(key: key);
  final VoidCallback onLinksCountChange;
  final ReleaseAssetLinksController controller;

  @override
  State<_ReleaseAssetLinksView> createState() => _ReleaseAssetLinksViewState();
}

class _ReleaseAssetLinksViewState extends State<_ReleaseAssetLinksView> {
  final Map<GlobalKey<FormState>, ReleaseAssetLinkInputView> _children = {};

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Padding(
        padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(AppLocalizations.dictionary().links, style: Theme.of(context).textTheme.titleLarge),
            IconButton(
                key: const Key("add-release-asset-link"),
                onPressed: () {
                  var validate = widget.controller.validate(context);
                  if (!validate) {
                    return;
                  }
                  var key = GlobalKey<FormState>(debugLabel: _children.length.toString());
                  setState(() {
                    widget.controller.put(key, ReleaseAssetLink.empty());
                    _children[key] = ReleaseAssetLinkInputView(
                      key: key,
                      onDelete: () {
                        setState(() {
                          _children.removeWhere((k, value) => key == k);
                          widget.controller.remove(key);
                        });
                      },
                      onInputChange: (releaseAssetLink) {
                        widget.controller.put(key, releaseAssetLink);
                      },
                    );
                  });
                  widget.onLinksCountChange();
                },
                icon: const Icon(Icons.add, size: 22, color: Color(0xFF87878C))),
          ],
        ),
      ),
      DelimitedColumn(divider: const Divider(height: 8, thickness: 8, color: Colors.transparent), children: List.of(_children.values).reversed.toList())
    ]);
  }
}

class InputWidthTitle extends StatelessWidget {
  const InputWidthTitle({
    required this.title,
    required this.child,
    this.textStyle,
    this.titleAction,
    Key? key,
  }) : super(key: key);
  final String title;
  final Widget child;
  final TextStyle? textStyle;
  final Widget? titleAction;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(title, style: textStyle ?? Theme.of(context).textTheme.titleLarge),
            if (titleAction != null) titleAction!,
          ],
        ),
        const SizedBox(height: 8),
        child,
      ],
    );
  }
}
