import 'package:jihu_gitlab_app/core/graph_ql_page_info.dart';
import 'package:jihu_gitlab_app/core/graphql/fragment.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';
import 'package:jihu_gitlab_app/core/paginated_graphql_response.dart';
import 'package:jihu_gitlab_app/modules/issues/details/paginable_list_view.dart';
import 'package:jihu_gitlab_app/modules/projects/releases/release.dart';

class ProjectReleasesModel {
  late String _fullPath;
  late PaginableListViewModel<Release> _paginableListViewModel;

  ProjectReleasesModel(String fullPath) {
    _fullPath = fullPath;
    _paginableListViewModel = PaginableListViewModel(
      onRefresh: (_) => refresh(),
      onLoadMore: (endCursor, _) => loadMore(endCursor),
    );
  }

  PaginableListViewModel<Release> get paginableListViewModel => _paginableListViewModel;

  Future<PaginatedGraphQLResponse<Release>> refresh() async {
    var body = (await ProjectRequestSender.instance().post(Api.graphql(), requestBodyOfQueryReleases(_fullPath, ""))).body();
    if (body['errors'] != null) {
      throw Exception(body['errors']);
    }
    var releasesJson = body['data']?['project']?['releases'];
    var list = ((releasesJson?['nodes'] ?? []) as List).map((e) => Release.fromGraphQLJson(e)).toList();
    return PaginatedGraphQLResponse(GraphQLPageInfo.fromJson(releasesJson?['pageInfo'] ?? {}), list);
  }

  Future<PaginatedGraphQLResponse<Release>> loadMore(String endCursor) async {
    var body = (await ProjectRequestSender.instance().post(Api.graphql(), requestBodyOfQueryReleases(_fullPath, endCursor))).body();
    var releasesJson = body['data']?['project']?['releases'];
    var list = ((releasesJson?['nodes'] ?? []) as List).map((e) => Release.fromGraphQLJson(e)).toList();
    return PaginatedGraphQLResponse(GraphQLPageInfo.fromJson(releasesJson?['pageInfo'] ?? {}), list);
  }

  String get fullPath => _fullPath;
}

Map<String, dynamic> requestBodyOfQueryReleases(String fullPath, String endCursor) {
  return {
    "variables": {"fullPath": fullPath, "first": 20, "sort": "RELEASED_AT_DESC", "after": endCursor},
    "query": """
            query allReleases(\$fullPath: ID!, \$first: Int, \$last: Int, \$before: String, \$after: String, \$sort: ReleaseSort) {
              project(fullPath: \$fullPath) {
                id
                releases(
                  first: \$first
                  last: \$last
                  before: \$before
                  after: \$after
                  sort: \$sort
                ) {
                  nodes {
                    ...Release
                  }
                  pageInfo {
                    ...PageInfo
                  }
                }
              }
            }
            
            $pageInfoFragment

            fragment Release on Release {
              id
              name
              tagName
              tagPath
              descriptionHtml
              releasedAt
              createdAt
              upcomingRelease
              historicalRelease
              assets {
                count
                sources {
                  nodes {
                    format
                    url
                  }
                }
                links {
                  nodes {
                    id
                    name
                    url
                    directAssetUrl
                    linkType
                    external
                  }
                }
              }
              evidences {
                nodes {
                  id
                  filepath
                  collectedAt
                  sha
                }
              }
              links {
                editUrl
                selfUrl
                openedIssuesUrl
                closedIssuesUrl
                openedMergeRequestsUrl
                mergedMergeRequestsUrl
                closedMergeRequestsUrl
              }
              commit {
                id
                sha
                webUrl
                title
                shortId
              }
              author {
                id
                webUrl
                avatarUrl
                username
              }
              milestones {
                nodes {
                  id
                  title
                  state
                  startDate
                  dueDate
                }
              }
            }
            """
  };
}
