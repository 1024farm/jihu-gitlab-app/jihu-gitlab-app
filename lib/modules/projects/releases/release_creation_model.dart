import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:jihu_gitlab_app/core/clock/global_glock.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';
import 'package:jihu_gitlab_app/core/project_identity.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestones_fetcher.dart';
import 'package:jihu_gitlab_app/modules/projects/releases/release_creation_page.dart';

class Tag {
  final String name;

  Tag(this.name);

  factory Tag.fromJson(Map<String, dynamic> json) {
    return Tag(json['name']);
  }
}

class ReleaseCreationModel {
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final ReleaseAssetLinksController _releaseAssetLinksController = ReleaseAssetLinksController();
  late ProjectIdentity _projectIdentity;
  String? _selectedTagName;
  Milestone? _selectedMilestone;
  DateTime _releaseDate = GlobalClock.now();

  TextEditingController get titleController => _titleController;

  ProjectIdentity get projectIdentity => _projectIdentity;

  ReleaseCreationModel(ProjectIdentity projectIdentity) {
    _projectIdentity = projectIdentity;
  }

  void changeSelectedTag(String? tagName) {
    _selectedTagName = tagName;
  }

  void changeSelectedMilestone(Milestone? milestone) {
    _selectedMilestone = milestone;
  }

  void changeReleaseDate(DateTime dateTime) {
    _releaseDate = dateTime;
  }

  String? get selectedTagName => _selectedTagName;

  Milestone? get selectedMilestone => _selectedMilestone;

  String get formattedReleaseDate => DateFormat(DateFormat.YEAR_NUM_MONTH_DAY).format(_releaseDate);

  DateTime get releaseDate => _releaseDate;

  bool get submittable => _selectedTagName != null;

  TextEditingController get descriptionController => _descriptionController;

  ReleaseAssetLinksController get releaseAssetLinksController => _releaseAssetLinksController;

  Future<void> createRelease() async {
    try {
      Map<String, dynamic> body = {
        'name': _titleController.text,
        "tag_name": _selectedTagName,
        "description": _descriptionController.value.text,
        "released_at": _releaseDate.toIso8601String(),
        "milestones": [_selectedMilestone?.title ?? ''],
        "assets": {"links": _releaseAssetLinksController.assetLinks.map((e) => e.toMap()).toList()},
      };
      await ProjectRequestSender.instance().post(Api.join('/projects/${projectIdentity.id}/releases'), body);
    } catch (error) {
      var message = AppLocalizations.dictionary().failed;
      if (error is DioError) {
        message = error.response?.data['message'] ?? error.message;
      }
      throw Exception(message);
    }
  }
}
