import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:jihu_gitlab_app/core/clock/global_glock.dart';
import 'package:jihu_gitlab_app/core/domain/avatar_url.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/time.dart';
import 'package:jihu_gitlab_app/core/uri_launcher.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar/avatar.dart';
import 'package:jihu_gitlab_app/core/widgets/delimited_column.dart';
import 'package:jihu_gitlab_app/core/widgets/round_rect_container.dart';
import 'package:jihu_gitlab_app/core/widgets/svg_text.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestone_page.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestones_fetcher.dart';
import 'package:jihu_gitlab_app/modules/projects/releases/release.dart';
import 'package:url_launcher/url_launcher.dart';

typedef ExpandedChangeListener = void Function(bool expand);

class ProjectReleaseItemView extends StatefulWidget {
  const ProjectReleaseItemView({required this.release, required this.expanded, required this.fullPath, required this.onExpandedChange, Key? key}) : super(key: key);
  final Release release;
  final bool expanded;
  final String fullPath;
  final ExpandedChangeListener onExpandedChange;

  @override
  State<ProjectReleaseItemView> createState() => _ProjectReleaseItemViewState();
}

class _ProjectReleaseItemViewState extends State<ProjectReleaseItemView> {
  bool _expanded = false;

  @override
  void initState() {
    _expanded = widget.expanded;
    super.initState();
  }

  @override
  void didUpdateWidget(covariant ProjectReleaseItemView oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.expanded != widget.expanded) {
      _expanded = widget.expanded;
    }
  }

  @override
  Widget build(BuildContext context) {
    return RoundRectContainer(
      margin: const EdgeInsets.symmetric(horizontal: 16),
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _ReleaseItemHeaderView(
            name: widget.release.name.isNotEmpty ? widget.release.name : widget.release.tagName,
            historicalRelease: widget.release.historicalRelease,
            upcomingRelease: widget.release.upcomingRelease,
            expanded: widget.expanded,
            onPressed: () => widget.onExpandedChange(!_expanded),
          ),
          Offstage(offstage: !_expanded, child: _ReleaseItemBodyView(fullPath: widget.fullPath, release: widget.release))
        ],
      ),
    );
  }
}

class _ReleaseItemHeaderView extends StatelessWidget {
  const _ReleaseItemHeaderView({required this.name, required this.historicalRelease, required this.upcomingRelease, required this.expanded, required this.onPressed});

  final String name;
  final bool historicalRelease;
  final bool upcomingRelease;
  final bool expanded;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        constraints: const BoxConstraints(minHeight: 44),
        child: Row(
          children: [
            Text(name, style: const TextStyle(color: Color(0xFF0063E9), fontSize: 17, fontWeight: FontWeight.w600)),
            const SizedBox(width: 8),
            if (historicalRelease) const _HistoricalReleaseView(),
            if (upcomingRelease) const _UpcomingReleaseView(),
            const Spacer(),
            SvgPicture.asset(expanded ? "assets/images/chevron_lg_up.svg" : "assets/images/chevron_lg_down.svg")
          ],
        ),
      ),
    );
  }
}

class _HistoricalReleaseView extends StatelessWidget {
  const _HistoricalReleaseView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
        decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(15)), color: Color(0xFFEAEAEA)),
        child: Text(AppLocalizations.dictionary().historicalRelease, style: const TextStyle(color: Color(0xFF66696D), fontSize: 13, fontWeight: FontWeight.w600)));
  }
}

class _UpcomingReleaseView extends StatelessWidget {
  const _UpcomingReleaseView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
        decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(15)), color: Color(0xFFF5D9A8)),
        child: Text(AppLocalizations.dictionary().upcomingRelease, style: const TextStyle(color: Color(0xFF66696D), fontSize: 13, fontWeight: FontWeight.normal)));
  }
}

class _ReleaseItemBodyView extends StatelessWidget {
  const _ReleaseItemBodyView({required this.fullPath, required this.release, Key? key}) : super(key: key);
  final String fullPath;
  final Release release;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _ReleaseMilestonesView(fullPath: fullPath, milestones: release.milestones),
        _ReleaseAssetsView(releaseAssets: release.assets),
        if (release.descriptionHtml.isNotEmpty)
          HtmlWidget(
            release.descriptionHtml,
            onTapUrl: (url) {
              UriLauncher.instance().launch(Uri.parse(url), mode: LaunchMode.externalApplication);
              return true;
            },
            customStylesBuilder: (element) => element.localName == 'a' ? {'color': '#0063E9'} : null,
          ),
        const Divider(color: Color(0xFFEAEAEA), thickness: 1),
        Row(
          children: [
            if (release.hasCommit)
              Container(
                  margin: const EdgeInsets.only(right: 12),
                  child: SvgText(
                      svgAssetPath: "assets/images/commit.svg", text: release.commit!.shortId, textStyle: const TextStyle(color: Color(0xFF03162F), fontSize: 13, fontWeight: FontWeight.normal))),
            if (release.tagName.isNotEmpty)
              SvgText(svgAssetPath: "assets/images/tag.svg", text: release.tagName, textStyle: const TextStyle(color: Color(0xFF03162F), fontSize: 13, fontWeight: FontWeight.normal))
          ],
        ),
        _ReleaseCreationInfoView(releasedAt: release.releasedAt, avatarUrl: release.author.avatarUrl)
      ],
    );
  }
}

class _ReleaseMilestonesView extends StatelessWidget {
  const _ReleaseMilestonesView({required this.fullPath, required this.milestones, Key? key}) : super(key: key);
  final String fullPath;
  final List<Milestone> milestones;

  @override
  Widget build(BuildContext context) {
    return milestones.isEmpty
        ? const SizedBox()
        : DelimitedColumn(
            crossAxisAlignment: CrossAxisAlignment.start,
            divider: const Divider(height: 4, thickness: 4, color: Colors.transparent),
            children: [
              Text(AppLocalizations.dictionary().milestones, style: const TextStyle(color: Color(0xFF03162F), fontSize: 13, fontWeight: FontWeight.normal)),
              ...milestones
                  .map((e) => InkWell(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) {
                          return ProjectMilestonePage(fullPath: fullPath, milestone: e);
                        }));
                      },
                      child: Text(e.title, style: const TextStyle(color: Color(0xFF0063E9), fontSize: 13, fontWeight: FontWeight.w600))))
                  .toList(),
              const Divider(color: Color(0xFFEAEAEA), height: 16, thickness: 1)
            ],
          );
  }
}

class _ReleaseCreationInfoView extends StatelessWidget {
  const _ReleaseCreationInfoView({required this.releasedAt, required this.avatarUrl});

  final String? releasedAt;
  final String? avatarUrl;

  @override
  Widget build(BuildContext context) {
    var dateTime = DateTime.tryParse(releasedAt ?? "");
    String releaseTime = "by";
    if (dateTime != null) {
      Time time = Time.init(releasedAt!);
      var duration = dateTime.difference(GlobalClock.now()).abs();
      var formatted = time.calculateDisplayFormat(duration.inSeconds, duration.inMinutes, duration.inHours, duration.inDays).trim();
      releaseTime = dateTime.isAfter(GlobalClock.now())
          ? AppLocalizations.dictionary().willBeReleased(formatted.replaceFirst("ago", "").replaceFirst("前", "")).trim()
          : AppLocalizations.dictionary().releasedAt(formatted).trim();
    }
    return Row(
      children: [
        Text("$releaseTime ${AppLocalizations.dictionary().by}", style: const TextStyle(color: Color(0xFF03162F), fontSize: 13, fontWeight: FontWeight.normal)),
        const SizedBox(width: 8),
        Avatar(avatarUrl: AvatarUrl(avatarUrl ?? '').realUrl(ProjectProvider().specifiedHost), size: 28),
      ],
    );
  }
}

class _ReleaseAssetsView extends StatefulWidget {
  const _ReleaseAssetsView({required this.releaseAssets, Key? key}) : super(key: key);
  final ReleaseAssets releaseAssets;

  @override
  State<_ReleaseAssetsView> createState() => _ReleaseAssetsViewState();
}

class _ReleaseAssetsViewState extends State<_ReleaseAssetsView> {
  bool _expanded = true;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InkWell(
            onTap: () => setState(() => _expanded = !_expanded),
            child: Row(children: [
              SvgText(
                  svgAssetPath: _expanded ? "assets/images/chevron_up.svg" : "assets/images/chevron_down.svg",
                  svgColor: const Color(0xFF0063E9),
                  text: AppLocalizations.dictionary().assets,
                  textStyle: const TextStyle(color: Color(0xFF0063E9), fontSize: 13, fontWeight: FontWeight.normal)),
              Container(
                  padding: const EdgeInsets.symmetric(horizontal: 6),
                  margin: const EdgeInsets.only(left: 4),
                  decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(15)), color: Color(0xFFEAEAEA)),
                  child: Text(widget.releaseAssets.count.toString(), style: const TextStyle(color: Color(0xFF66696D), fontSize: 13, fontWeight: FontWeight.normal))),
            ])),
        Visibility(
          visible: _expanded,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
            child: DelimitedColumn(divider: const Divider(height: 8, thickness: 8, color: Colors.transparent), crossAxisAlignment: CrossAxisAlignment.start, children: [
              ...widget.releaseAssets.sources.map((e) => _ReleaseAssetView(e.url, "${AppLocalizations.dictionary().sourceCode}(${e.format})", "assets.svg")).toList(),
              if (widget.releaseAssets.links.isNotEmpty)
                Padding(
                    padding: const EdgeInsets.only(top: 8),
                    child: Text(AppLocalizations.dictionary().other, style: const TextStyle(color: Color(0xFF03162F), fontSize: 13, fontWeight: FontWeight.w600))),
              ...widget.releaseAssets.links.map((e) => _ReleaseAssetView(e.url, e.name, "link.svg")).toList(),
            ]),
          ),
        ),
      ],
    );
  }
}

class _ReleaseAssetView extends StatelessWidget {
  final String url;
  final String name;
  final String iconName;

  const _ReleaseAssetView(this.url, this.name, this.iconName);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => UriLauncher.instance().launch(Uri.parse(url), mode: LaunchMode.externalApplication),
      child: SvgText(
          svgAssetPath: "assets/images/$iconName", svgColor: const Color(0xFF0063E9), text: name, textStyle: const TextStyle(color: Color(0xFF0063E9), fontSize: 13, fontWeight: FontWeight.normal)),
    );
  }
}
