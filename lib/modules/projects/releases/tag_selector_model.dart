import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/graph_ql_page_info.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';
import 'package:jihu_gitlab_app/core/paginated_graphql_response.dart';
import 'package:jihu_gitlab_app/modules/issues/details/paginable_list_view.dart';
import 'package:jihu_gitlab_app/modules/projects/releases/release_creation_model.dart';

class TagSelectorModel {
  late final int _projectId;
  final TextEditingController _searchController = TextEditingController();
  late PaginableListViewModel<Tag> _paginableListViewModel;

  TagSelectorModel(this._projectId) {
    _paginableListViewModel = PaginableListViewModel(
      onRefresh: (keyword) => refresh(keyword),
      onLoadMore: (endCursor, keyword) => loadMore(endCursor, keyword),
      searchable: true,
    );
  }

  Future<PaginatedGraphQLResponse<Tag>> refresh(String? keyword) async {
    int page = 1;
    List<Tag> tags = await _getTags(1, keyword);
    return PaginatedGraphQLResponse(GraphQLPageInfo.fromJson({"endCursor": page.toString()}), tags);
  }

  Future<PaginatedGraphQLResponse<Tag>> loadMore(String endCursor, String? keyword) async {
    int nextPage = int.parse(endCursor);
    List<Tag> tags = await _getTags(nextPage, keyword);
    return PaginatedGraphQLResponse(GraphQLPageInfo.fromJson({"endCursor": nextPage.toString()}), tags);
  }

  Future<List<Tag>> _getTags(int page, String? keyword) async {
    var response = await ProjectRequestSender.instance().get(Api.join("projects/$_projectId/repository/tags?per_page=20&page=$page&search=${keyword ?? ''}"));
    List<Tag> tags = (response.body() as List).map((e) => Tag.fromJson(e)).toList();
    return tags;
  }

  TextEditingController get searchController => _searchController;

  PaginableListViewModel<Tag> get paginableListViewModel => _paginableListViewModel;
}
