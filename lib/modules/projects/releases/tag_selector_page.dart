import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/details/paginable_list_view.dart';
import 'package:jihu_gitlab_app/modules/projects/releases/release_creation_model.dart';
import 'package:jihu_gitlab_app/modules/projects/releases/tag_selector_model.dart';

class TagSelectorPage extends StatefulWidget {
  const TagSelectorPage({required this.projectId, this.selectedName, Key? key}) : super(key: key);
  final int projectId;
  final String? selectedName;

  @override
  State<TagSelectorPage> createState() => _TagSelectorPageState();
}

class _TagSelectorPageState extends State<TagSelectorPage> {
  String? _selectedName;
  late TagSelectorModel _model;

  @override
  void initState() {
    _selectedName = widget.selectedName;
    _model = TagSelectorModel(widget.projectId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CommonAppBar(
          showLeading: true,
          title: Text(AppLocalizations.dictionary().selectTag),
          actions: [
            Container(
                transform: Matrix4.translationValues(-12, 2, 0),
                child: TextButton(onPressed: () => Navigator.pop(context, _selectedName), child: Text(AppLocalizations.dictionary().done, style: TextStyle(color: Theme.of(context).primaryColor))))
          ],
        ),
        body: Container(
          margin: const EdgeInsets.symmetric(horizontal: 16),
          child: PaginableListView<Tag>(
            model: _model.paginableListViewModel,
            listBoxDecoration: const BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(4))),
            itemBuilder: <Tag>(context, index, data) {
              var item = data[index];
              return InkWell(
                  onTap: () => _onSelectedChanged(item.name),
                  child: Container(
                    padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        _selectedName == item.name
                            ? Icon(Icons.check_circle, color: Theme.of(context).primaryColor, size: 18)
                            : const Icon(Icons.radio_button_unchecked, color: Color(0XFFCECECE), size: 18),
                        const SizedBox(width: 8),
                        Text(
                          item.name,
                          style: const TextStyle(color: Color(0xFF03162F), fontSize: 14, fontWeight: FontWeight.normal),
                        ),
                      ],
                    ),
                  ));
            },
          ),
        ));
  }

  void _onSelectedChanged(String tagName) {
    setState(() => _selectedName = _selectedName == tagName ? null : tagName);
  }
}
