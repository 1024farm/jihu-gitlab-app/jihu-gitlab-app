import 'package:jihu_gitlab_app/core/connection_provider/connection.dart';
import 'package:jihu_gitlab_app/modules/mr/models/commits.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestones_fetcher.dart';

class Release {
  String id;
  String name;
  String tagName;
  String tagPath;
  String descriptionHtml;
  String releasedAt;
  String createdAt;
  bool upcomingRelease;
  bool historicalRelease;
  ReleaseAssets assets;
  Commit? commit;
  User author;
  List<Milestone> milestones;

  Release(this.id, this.name, this.tagName, this.tagPath, this.descriptionHtml, this.releasedAt, this.createdAt, this.upcomingRelease, this.historicalRelease, this.assets, this.commit, this.author,
      this.milestones);

  factory Release.fromGraphQLJson(Map<String, dynamic> json) {
    return Release(
        json['id'],
        json['name'],
        json['tagName'],
        json['tagPath'],
        json['descriptionHtml'],
        json['releasedAt'],
        json['createdAt'],
        json['upcomingRelease'],
        json['historicalRelease'],
        ReleaseAssets.fromJson(json['assets']),
        json['commit'] != null ? Commit.fromJson(json['commit']) : null,
        User.fromGraphQLJson(json['author'] ?? {}),
        ((json['milestones']?['nodes'] ?? []) as List).map((e) => Milestone.fromJson(e)).toList());
  }

  bool get hasCommit => commit != null;
}

class ReleaseLinkAsset {
  String id;
  String name;
  String url;
  String directAssetUrl;
  String linkType;

  ReleaseLinkAsset._(this.id, this.name, this.url, this.directAssetUrl, this.linkType);

  factory ReleaseLinkAsset.fromJson(Map<String, dynamic> json) {
    return ReleaseLinkAsset._(json['id'], json['name'] ?? "", json['url'] ?? "", json['directAssetUrl'] ?? "", json['linkType']);
  }
}

class ReleaseSourceAsset {
  String format;
  String url;

  ReleaseSourceAsset._(this.format, this.url);

  factory ReleaseSourceAsset.fromJson(Map<String, dynamic> json) {
    return ReleaseSourceAsset._(json['format'], json['url']);
  }
}

class ReleaseAssets {
  int count;
  List<ReleaseSourceAsset> sources;
  List<ReleaseLinkAsset> links;

  ReleaseAssets._(this.count, this.sources, this.links);

  factory ReleaseAssets.fromJson(Map<String, dynamic> json) {
    return ReleaseAssets._(
      json['count'] ?? 0,
      ((json['sources']?['nodes'] ?? []) as List).map((e) => ReleaseSourceAsset.fromJson(e)).toList(),
      ((json['links']?['nodes'] ?? []) as List).map((e) => ReleaseLinkAsset.fromJson(e)).toList(),
    );
  }
}
