import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/widgets/platform_adaptive_text_field.dart';
import 'package:jihu_gitlab_app/core/widgets/round_rect_container.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/projects/releases/release_creation_page.dart';

typedef OnDeleteListener = void Function();

typedef OnInputChangeListener = void Function(ReleaseAssetLink releaseAssetLink);

class ReleaseAssetLinkInputView extends StatefulWidget {
  const ReleaseAssetLinkInputView({required this.onDelete, required this.onInputChange, Key? key}) : super(key: key);
  final OnDeleteListener onDelete;
  final OnInputChangeListener onInputChange;

  @override
  State<ReleaseAssetLinkInputView> createState() => _ReleaseAssetLinkInputViewState();
}

class _ReleaseAssetLinkInputViewState extends State<ReleaseAssetLinkInputView> {
  final ReleaseAssetLink _assetLink = ReleaseAssetLink.empty();

  @override
  Widget build(BuildContext context) {
    return RoundRectContainer(
      padding: const EdgeInsets.fromLTRB(12, 0, 0, 12),
      child: Column(
        children: [
          Align(
            alignment: Alignment.centerRight,
            child: IconButton(
              icon: const Icon(Icons.delete_outline, color: Color(0xFF66696D)),
              onPressed: () => widget.onDelete(),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(right: 12),
            child: Column(
              children: [
                InputWidthTitle(
                    title: AppLocalizations.dictionary().url,
                    child: PlatformAdaptiveTextField(
                      key: const Key("release-asset-link-input-url"),
                      onChanged: (value) {
                        _assetLink.url = value;
                        widget.onInputChange(_assetLink);
                      },
                    )),
                const SizedBox(height: 16),
                InputWidthTitle(
                    title: AppLocalizations.dictionary().linkTitle,
                    child: PlatformAdaptiveTextField(
                      key: const Key("release-asset-link-input-title"),
                      onChanged: (value) {
                        _assetLink.linkTitle = value;
                        widget.onInputChange(_assetLink);
                      },
                    )),
                const SizedBox(height: 16),
                InputWidthTitle(
                  title: AppLocalizations.dictionary().type,
                  child: _buildSelector(),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildSelector() {
    return InkWell(
      onTap: _onBranchSelectorTapped,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
        decoration: BoxDecoration(color: Colors.white, borderRadius: const BorderRadius.all(Radius.circular(4)), border: Border.all(color: const Color(0xFFEAEAEA))),
        margin: const EdgeInsets.only(bottom: 8),
        constraints: const BoxConstraints(minHeight: 48),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(child: Text(_assetLink.type.label, maxLines: 1, overflow: TextOverflow.ellipsis, style: const TextStyle(color: Color(0xFF03162F), fontSize: 14, fontWeight: FontWeight.w500))),
            const Icon(Icons.arrow_drop_down)
          ],
        ),
      ),
    );
  }

  void _onBranchSelectorTapped() async {
    double maxWidth = MediaQuery.of(context).size.width - 32;
    double maxHeight = MediaQuery.of(context).size.height / 2;
    RenderBox? renderObject = context.findRenderObject() as RenderBox?;
    Offset offset = renderObject?.localToGlobal(Offset.zero) ?? const Offset(0, 0);
    Size size = renderObject?.size ?? Size(maxWidth, 0);
    RelativeRect position = RelativeRect.fromLTRB(offset.dx, offset.dy + size.height, offset.dx, 0);
    var result = await showMenu<ReleaseAssetLinkType>(
      context: context,
      constraints: BoxConstraints(maxWidth: size.width, maxHeight: maxHeight),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
      position: position,
      items: _buildPopupMenuEntries(ReleaseAssetLinkType.values, maxWidth),
    );
    if (result != null) {
      setState(() {
        _assetLink.type = result;
      });
    }
  }

  List<PopupMenuEntry<ReleaseAssetLinkType>> _buildPopupMenuEntries(List<ReleaseAssetLinkType> types, double width) {
    List<PopupMenuEntry<ReleaseAssetLinkType>> items = [];
    for (int i = 0; i < types.length; i++) {
      var type = types[i];
      items.add(PopupMenuItem(
          value: type,
          padding: const EdgeInsets.all(0),
          height: 0,
          textStyle: const TextStyle(color: Colors.blue),
          labelTextStyle: MaterialStateProperty.resolveWith<TextStyle?>((states) => const TextStyle(color: Colors.purple)),
          child: Container(
            width: width,
            padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 12),
            color: Colors.white,
            child: Text(type.label, style: const TextStyle(color: Color(0xFF03162F), fontSize: 14)),
          )));
      items.add(const PopupMenuDivider(height: 1));
    }
    if (items.isNotEmpty) items.removeLast();
    return items;
  }
}

enum ReleaseAssetLinkType {
  image,
  package,
  runbook,
  other;

  String get label {
    if (this == ReleaseAssetLinkType.image) {
      return AppLocalizations.dictionary().image;
    }
    if (this == ReleaseAssetLinkType.package) {
      return AppLocalizations.dictionary().package;
    }
    if (this == ReleaseAssetLinkType.runbook) {
      return AppLocalizations.dictionary().runbook;
    }
    return AppLocalizations.dictionary().other;
  }
}

class ReleaseAssetLink {
  String? url;
  String? linkTitle;
  ReleaseAssetLinkType type = ReleaseAssetLinkType.other;

  ReleaseAssetLink._();

  factory ReleaseAssetLink.empty() {
    return ReleaseAssetLink._();
  }

  Map<String, dynamic> toMap() {
    return {"url": url, "link_type": type.name, "name": linkTitle};
  }

  String? validate() {
    if (url == null || (!(url!.startsWith("http")) && !(url!.startsWith("ftp")))) {
      return AppLocalizations.dictionary().releaseAssetLinkUrlValidateMessage;
    }
    if (linkTitle == null) {
      return AppLocalizations.dictionary().releaseAssetLinkTitleValidateMessage;
    }
    return null;
  }
}
