import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/app_tab_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/charts/burn_chart_view.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/feature_not_supported_hint_dialog.dart';
import 'package:jihu_gitlab_app/core/widgets/multi_state_view.dart';
import 'package:jihu_gitlab_app/core/widgets/project_archived_notice_view.dart';
import 'package:jihu_gitlab_app/core/widgets/round_rect_container.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/epic/epic_issues_view.dart';
import 'package:jihu_gitlab_app/modules/issues/details/issue_details_page.dart';
import 'package:jihu_gitlab_app/modules/issues/grouped_issues/grouped_issues_model.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_view.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_blocked_view.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/milestone_and_iteration_duration_view.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/milestone_and_iteration_state_view.dart';

class GroupedIssuesView extends StatefulWidget {
  const GroupedIssuesView(
      {required this.fromGroup,
      required this.groupingDefault,
      required this.groupedIssues,
      required this.state,
      required this.title,
      required this.startDate,
      required this.dueDate,
      required this.onRetry,
      super.key});

  final bool fromGroup;
  final bool groupingDefault;
  final GroupedIssues groupedIssues;
  final VoidFutureCallBack onRetry;
  final String? state;
  final String? title;
  final String? startDate;
  final String? dueDate;

  @override
  State<GroupedIssuesView> createState() => _GroupedIssuesViewState();
}

class _GroupedIssuesViewState extends State<GroupedIssuesView> with SingleTickerProviderStateMixin {
  late TabController _tabController;
  bool _groupedByEpic = false;

  @override
  void initState() {
    _groupedByEpic = widget.groupingDefault;
    _tabController = TabController(length: 1, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void _toggleEpicGroup() => _groupedByEpic = !_groupedByEpic;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CommonAppBar(showLeading: true, backgroundColor: Colors.white, title: Text(widget.title ?? '')),
      body: SafeArea(
        child: Column(
          children: [
            Flexible(flex: 1, child: Container(color: Colors.white, padding: const EdgeInsets.symmetric(horizontal: 16), child: _buildPageBody())),
            if (ProjectProvider().archived) const Align(alignment: Alignment.bottomCenter, child: ProjectArchivedNoticeView()),
          ],
        ),
      ),
    );
  }

  Widget _buildPageBody() {
    return MultiStateView(
      loadState: widget.groupedIssues.loadState,
      onRetry: widget.onRetry,
      child: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) => [SliverToBoxAdapter(child: _buildNestedScrollViewHeader())],
        body: _buildNestedScrollViewBody(),
      ),
    );
  }

  Widget _buildNestedScrollViewHeader() {
    bool shouldShowDescription = widget.groupedIssues.description != null && widget.groupedIssues.description!.isNotEmpty;
    bool shouldShowBurnChart = widget.groupedIssues.timeBoxReport != null && widget.groupedIssues.timeBoxReport!.isNotEmpty;
    return Column(
      children: [
        _StateAndDateView(widget.state, widget.startDate, widget.dueDate),
        const SizedBox(height: 12),
        if (shouldShowDescription) HtmlWidget(widget.groupedIssues.description!),
        if (shouldShowDescription && shouldShowBurnChart) const Divider(color: Color(0xFFEAEAEA), thickness: 1, height: 24),
        if (shouldShowBurnChart) BurnChartView(timeBoxReport: widget.groupedIssues.timeBoxReport!),
      ],
    );
  }

  Widget _buildNestedScrollViewBody() {
    return Column(
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: AppTabBar(
            isScrollable: true,
            tabs: [Tab(child: Text(AppLocalizations.dictionary().projectsIssues))],
            controller: _tabController,
          ),
        ),
        Expanded(
          child: TabBarView(controller: _tabController, children: [
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(AppLocalizations.dictionary().groupByEpic),
                    Transform.scale(
                      scale: 0.7,
                      child: CupertinoSwitch(
                        activeColor: Theme.of(context).primaryColor,
                        value: _groupedByEpic,
                        onChanged: (value) async {
                          if (!widget.groupedIssues.canGroupByEpic) {
                            await FeatureNotSupportedHintDialog.show(context, AppLocalizations.dictionary().epic);
                          } else {
                            setState(() => _toggleEpicGroup());
                          }
                        },
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 12),
                Flexible(child: _buildIssues(widget.groupedIssues.first100issues)),
              ],
            )
          ]),
        ),
      ],
    );
  }

  Widget _buildIssues(First100Issues? first100issues) {
    if (first100issues == null || first100issues.isEmpty) {
      return const SizedBox();
    }
    if (_groupedByEpic) return _buildGroupedIssues(first100issues);
    return RoundRectContainer(
      decoration: const BoxDecoration(color: Color(0xFFF8F8FA), borderRadius: BorderRadius.all(Radius.circular(4))),
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 12),
      child: IssuesView(
          issuesFetcher: () => widget.groupedIssues.issues,
          isLoading: () => false,
          isEmpty: () => false,
          onRefresh: () async => {},
          onListItemTap: (index) {
            var params = widget.groupedIssues.issues[index].asDetailParams;
            params['showLeading'] = true;
            Navigator.of(context).pushNamed(IssueDetailsPage.routeName, arguments: params);
          },
          selectedIndex: 0,
          selectedColor: Colors.transparent,
          scrollable: true,
          shrinkWrap: true,
          displayOptions: IssuesViewDisplayOptions(),
          displayProject: widget.fromGroup),
    );
  }

  Widget _buildGroupedIssues(First100Issues first100issues) {
    return ListView.separated(
      shrinkWrap: true,
      itemBuilder: (context, index) => EpicIssuesView(epicIssues: first100issues.epicIssuesList[index], formGroup: widget.fromGroup),
      separatorBuilder: (context, index) => const Divider(color: Colors.transparent, height: 12, thickness: 12),
      itemCount: first100issues.epicIssuesList.length,
    );
  }
}

class _StateAndDateView extends StatelessWidget {
  final String? state;
  final String? startDate;
  final String? dueDate;

  const _StateAndDateView(this.state, this.startDate, this.dueDate);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [MilestoneAndIterationStateView(state: state), const SizedBox(width: 8), MilestoneAndIterationDurationView(startDate, dueDate)],
    );
  }
}
