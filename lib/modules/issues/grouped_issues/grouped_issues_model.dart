import 'package:collection/collection.dart';
import 'package:jihu_gitlab_app/core/domain/issue.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/widgets/charts/time_box_report.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/epic/epic_issues_view.dart';

class First100Issues {
  final List<Issue> _issues;
  bool _canGroupByEpic = false;
  List<EpicIssues>? _epicIssuesList;

  First100Issues(this._issues, {required bool canGroupByEpic}) {
    _canGroupByEpic = canGroupByEpic;
  }

  List<Issue> get issues => _issues;

  bool get canGroupByEpic => _canGroupByEpic;

  List<EpicIssues> get epicIssuesList {
    _epicIssuesList ??= _groupIssuesByEpic(_issues);
    return _epicIssuesList!;
  }

  List<EpicIssues> _groupIssuesByEpic(List<Issue> issues) {
    var map = groupBy<Issue, String>(issues.where((e) => e.epic?.title != null).toList(), (e) => e.epic!.title);
    List<EpicIssues> epicIssuesList = map.entries.map((e) => EpicIssues(e.key, e.value)).toList();
    var noEpicIssues = issues.where((e) => e.epic?.title == null).toList();
    if (noEpicIssues.isNotEmpty) {
      epicIssuesList.add(EpicIssues(AppLocalizations.dictionary().projectsIterationUnallocated, noEpicIssues));
    }
    return epicIssuesList;
  }

  bool get isEmpty => _issues.isEmpty;
}

class GroupedIssues {
  final LoadState loadState;
  final String? description;
  final TimeBoxReport? timeBoxReport;
  final First100Issues? first100issues;

  GroupedIssues._({required this.loadState, this.description, this.timeBoxReport, this.first100issues});

  factory GroupedIssues.init() {
    return GroupedIssues._(loadState: LoadState.loadingState);
  }

  factory GroupedIssues.empty() {
    return GroupedIssues._(loadState: LoadState.noItemState);
  }

  factory GroupedIssues.fail() {
    return GroupedIssues._(loadState: LoadState.errorState);
  }

  factory GroupedIssues.success({String? description, TimeBoxReport? timeBoxReport, First100Issues? first100Issues}) {
    return GroupedIssues._(loadState: LoadState.successState, description: description, timeBoxReport: timeBoxReport, first100issues: first100Issues);
  }

  bool get isEmpty => first100issues == null || first100issues!.issues.isEmpty;

  bool get canGroupByEpic => first100issues?.canGroupByEpic ?? false;

  List<Issue> get issues => first100issues?.issues ?? [];

  List<EpicIssues> get epicIssuesList => first100issues?.epicIssuesList ?? [];
}
