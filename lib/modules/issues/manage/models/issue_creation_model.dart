import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/domain/global_time.dart';
import 'package:jihu_gitlab_app/core/domain/low_permission_information.dart';
import 'package:jihu_gitlab_app/core/license_type_detector.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/models/issue_draft_entity.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/models/issue_draft_repository.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/models/issue_update_model.dart';
import 'package:jihu_gitlab_app/modules/issues/member/member.dart';

class IssueCreationModel {
  late int _projectId;
  late int _groupId;
  late IssueUpdateModel _issueUpdateModel;

  bool? _isMultiAssignees;
  String? templateName;
  List<Label> selectedLabels = [];
  final List<Member> _selectedAssignees = [];
  final IssueDraftRepository _repository = IssueDraftRepository();
  bool _confidential = false;

  LoadState _createState = LoadState.noItemState;
  Map<String, dynamic>? _createdResp;

  void init({required int projectId, int? groupId}) {
    _projectId = projectId;
    _groupId = groupId ?? 0;
    if (groupId != null) {
      isFree().then((value) => _isMultiAssignees = !value);
      return;
    }
    loadGroupId().then((value) {
      _groupId = value;
      isFree().then((free) => _isMultiAssignees = !free);
    });
    _issueUpdateModel = IssueUpdateModel();
  }

  Future<int> loadGroupId() async {
    try {
      return (await ProjectRequestSender.instance().get(Api.join('/projects/$projectId'))).body()['namespace']['id'];
    } catch (e) {
      return 0;
    }
  }

  LoadState get createState => _createState;

  void onAssigneesSelect(List<Member> selectedAssignees) {
    _selectedAssignees.clear();
    _selectedAssignees.addAll(selectedAssignees);
  }

  void deleteSelectedUser(Member user) {
    _selectedAssignees.remove(user);
  }

  Future<bool> create(int projectId, String title, {String? description, String? labels}) async {
    _createState = LoadState.loadingState;
    try {
      String uri = Api.join('/projects/$projectId/issues');
      var body = <String, dynamic>{'title': title};
      if (description != null) {
        body['description'] = description;
      }
      if (labels != null) {
        body['labels'] = labels;
      }
      if (_selectedAssignees.isNotEmpty) {
        if (_isMultiAssignees!) {
          body['assignee_ids'] = _selectedAssignees.map((e) => e.id).toList();
        } else {
          body['assignee_id'] = _selectedAssignees[0].id;
        }
      }
      body['confidential'] = _confidential;
      var response = await ProjectRequestSender.instance().post<Map<String, dynamic>>(uri, body);
      _createdResp = response.body();
      _createState = LoadState.successState;
      _repository.delete(projectId, ConnectionProvider.connectionId!);
      return Future.value(true);
    } catch (error) {
      _createState = LoadState.errorState;
      return Future.value(false);
    }
  }

  Future<bool> update(int projectId, int issueIid, String title, {String? description}) async {
    _createState = LoadState.loadingState;
    try {
      bool result = await _issueUpdateModel.update(projectId, issueIid, title: title, description: description);
      _createdResp = _issueUpdateModel.updatedResp;
      _createState = _issueUpdateModel.updateState;
      return Future.value(result);
    } catch (error) {
      _createState = LoadState.errorState;
      return Future.value(false);
    }
  }

  void onLabelSelect(List<Label> selectedLabels) {
    this.selectedLabels.clear();
    this.selectedLabels.addAll(selectedLabels);
  }

  void deleteSelectedLabel(Label label) {
    selectedLabels.remove(label);
  }

  bool edited() {
    return selectedLabels.isNotEmpty || _selectedAssignees.isNotEmpty;
  }

  Future<bool> isMultiAssignees() async {
    _isMultiAssignees ??= !(await isFree());
    return Future.value(_isMultiAssignees);
  }

  Future<bool> isFree() async {
    return await LicenseTypeDetector.instance().isFree(_groupId);
  }

  Future<String> warningNoticeWhenNoLicense() async {
    if (!await isFree()) return '';
    return '${LowPermissionInformation.warningNotice(AppLocalizations.dictionary().multiSelection)}, ${LowPermissionInformation.warningNoticeSupportLine()}';
  }

  int get groupId => _groupId;

  int get projectId => _projectId;

  List<Member> get selectedAssignees => _selectedAssignees;

  void saveAsDraft(String title, String description) async {
    var userId = ConnectionProvider.connectionId!;
    var issueDraft = {
      "user_id": userId,
      "title": title,
      "description": description,
      "template": templateName,
      "selected_assignees": selectedAssignees,
      "selected_labels": selectedLabels,
      "confidential": _confidential ? 1 : 0
    };
    int version = GlobalTime.now().millisecondsSinceEpoch;
    var issueDraftEntity = IssueDraftEntity.create(issueDraft, projectId, version);
    List<IssueDraftEntity> issueDrafts = await _repository.query(projectId, userId);
    if (issueDrafts.isNotEmpty) {
      _repository.update(issueDraftEntity);
    } else {
      _repository.insert(issueDraftEntity);
    }
  }

  Future<List<IssueDraftEntity>> loadDraft() async {
    List<IssueDraftEntity> issueDrafts = await _repository.query(projectId, ConnectionProvider.connectionId!);
    return issueDrafts;
  }

  void initLabelAndAssign(List<Label> labels, List<Member> assigns) {
    selectedLabels.clear();
    _selectedAssignees.clear();
    selectedLabels.addAll(labels);
    _selectedAssignees.addAll(assigns);
  }

  void toggleConfidential() {
    _confidential = !_confidential;
  }

  void restoreConfidential({required bool confidential}) {
    _confidential = confidential;
  }

  bool get confidential => _confidential;

  Map<String, dynamic>? get createdResp => _createdResp;
}
