import 'dart:convert';

import 'package:jihu_gitlab_app/modules/issues/label/label.dart';
import 'package:jihu_gitlab_app/modules/issues/member/member.dart';

class IssueDraftEntity {
  static const tableName = "issue_drafts";

  int userId;
  int projectId;
  String? title;
  String? description;
  String? template;
  List<Member>? assignees;
  List<Label>? labels;
  int confidential;
  int version;

  IssueDraftEntity._internal(this.userId, this.projectId, this.title, this.description, this.template, this.assignees, this.labels, this.confidential, this.version);

  static IssueDraftEntity restore(Map<String, dynamic> map) {
    List<Member> members = !map.containsKey('selected_assignees') ? [] : ((jsonDecode(map['selected_assignees']) ?? []) as List).map((e) => Member.fromJson(e)).toList();
    List<Label> labels = !map.containsKey('selected_labels') ? [] : ((jsonDecode(map['selected_labels']) ?? []) as List).map((e) => Label.fromJson(e)).toList();
    return IssueDraftEntity._internal(map['user_id'], map['project_id'], map['title'], map['description'], map['template'], members, labels, map['confidential'] ?? 0, map['version']);
  }

  static IssueDraftEntity create(Map<String, dynamic> map, int projectId, int version) {
    return IssueDraftEntity._internal(
        map['user_id'], projectId, map['title'], map['description'], map['template'], map['selected_assignees'], map['selected_labels'], map['confidential'] ?? 0, version);
  }

  Map<String, dynamic> toMap() {
    return {
      'user_id': userId,
      'project_id': projectId,
      'title': title,
      'description': description,
      'template': template,
      'selected_assignees': jsonEncode(assignees?.map((e) => e.toJson()).toList()),
      'selected_labels': jsonEncode(labels?.map((e) => e.toJson()).toList()),
      'version': version,
      "confidential": confidential
    };
  }

  static String createTableSql() {
    return """ 
    create table $tableName(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        user_id INTEGER NOT NULL,
        project_id INTEGER NOT NULL,
        title TEXT,
        description TEXT,
        selected_assignees TEXT,
        selected_labels TEXT,
        version INTEGER
    );    
    """;
  }

  static String get addTemplateColumn => """ALTER TABLE $tableName ADD COLUMN template TEXT;""";

  static String get addConfidentialColumn => """ALTER TABLE $tableName ADD COLUMN confidential INTEGER;""";
}
