import 'package:jihu_gitlab_app/core/connection_provider/connection.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';

class IssueUpdateModel {
  LoadState _updateState = LoadState.noItemState;
  Map<String, dynamic>? _updatedResp;

  Future<bool> update(int projectId, int issueIid, {String? title, String? description, String? labels, Connection? connection, int? milestoneId, List<int>? assigneeIds}) async {
    _updateState = LoadState.loadingState;
    try {
      String uri = '/projects/$projectId/issues/$issueIid';
      var body = <String, dynamic>{};
      if (title != null) {
        body['title'] = title;
      }
      if (description != null) {
        body['description'] = description;
      }
      if (labels != null) {
        body['labels'] = labels;
      }
      if (assigneeIds != null) {
        body['assignee_ids'] = assigneeIds;
      }
      if (milestoneId != null) {
        body['milestone_id'] = milestoneId == 0 ? null : milestoneId;
      }
      var response =
          connection != null ? await HttpClient.instance().put<Map<String, dynamic>>(Api.join(uri), body, connection: connection) : await ProjectRequestSender.instance().put(Api.join(uri), body);
      _updatedResp = response.body();
      _updateState = LoadState.successState;
      return Future.value(true);
    } catch (error) {
      _updateState = LoadState.errorState;
      return Future.value(false);
    }
  }

  Map<String, dynamic>? get updatedResp => _updatedResp;

  LoadState get updateState => _updateState;
}
