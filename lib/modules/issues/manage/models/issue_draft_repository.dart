import 'package:jihu_gitlab_app/core/db_manager.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/models/issue_draft_entity.dart';
import 'package:sqflite/sqlite_api.dart';

class IssueDraftRepository {
  Future<List<IssueDraftEntity>> query(int projectId, int userId) async {
    Database database = await DbManager.instance().getDb();
    List<Map<String, Object?>> query = await database.query(IssueDraftEntity.tableName, where: " project_id = ? and user_id = ? ", whereArgs: [projectId, userId]);
    return query.map((e) => IssueDraftEntity.restore(e)).toList();
  }

  Future<int> insert(IssueDraftEntity issueDraftEntity) async {
    Database database = await DbManager.instance().getDb();
    return database.insert(IssueDraftEntity.tableName, issueDraftEntity.toMap());
  }

  Future<int> update(IssueDraftEntity issueDraftEntity) async {
    Database database = await DbManager.instance().getDb();
    return database.update(IssueDraftEntity.tableName, issueDraftEntity.toMap(), where: " user_id = ? and project_id = ?", whereArgs: [issueDraftEntity.userId, issueDraftEntity.projectId]);
  }

  Future<int> delete(int projectId, int userId) async {
    Database database = await DbManager.instance().getDb();
    return database.delete(IssueDraftEntity.tableName, where: " user_id = ? and project_id = ?", whereArgs: [userId, projectId]);
  }
}
