import 'package:jihu_gitlab_app/core/domain/low_permission_information.dart';
import 'package:jihu_gitlab_app/core/license_type_detector.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class AssigneeSelectorModel {
  late int _groupId;
  bool? _isMultiAssignees;

  void init(int groupId, int projectId) {
    if (groupId == 0) {
      loadGroupId(projectId).then((value) => _groupId = value);
      return;
    }
    _groupId = groupId;
  }

  Future<int> loadGroupId(int projectId) async {
    try {
      var body = (await ProjectRequestSender.instance().get(Api.join('/projects/$projectId'))).body();
      return body['namespace']['id'];
    } catch (e) {
      return 0;
    }
  }

  Future<bool> isMultiAssignees() async {
    _isMultiAssignees ??= !(await isFree());
    return Future.value(_isMultiAssignees);
  }

  Future<String> warningNoticeWhenNoLicense() async {
    if (!await isFree()) return '';
    return '${LowPermissionInformation.warningNotice(AppLocalizations.dictionary().multiSelection)}, ${LowPermissionInformation.warningNoticeSupportLine()}';
  }

  Future<bool> isFree() async {
    return await LicenseTypeDetector.instance().isFree(_groupId);
  }
}
