import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';

class DescriptionTemplateSelectorModel {
  List<Template> _templates = [];
  int _page = 1;
  static const int _size = 50;
  bool _hasNextPage = false;

  LoadState _loadState = LoadState.noItemState;

  Future<List<Template>> _fetch(int projectId) async {
    var response = await ProjectRequestSender.instance().get<List<dynamic>>(Api.join('/projects/$projectId/templates/issues?page=$_page&per_page=$_size'));
    return response.body().map((e) => Template.fromJson(e, projectId)).toList();
  }

  Future<bool> refresh(int projectId) async {
    try {
      _page = 1;
      _templates = await _fetch(projectId);
      _hasNextPage = _templates.length >= _size;
      _loadState = LoadState.successState;
      return Future.value(true);
    } catch (error) {
      _loadState = LoadState.errorState;
      return Future.value(false);
    }
  }

  Future<bool> loadMore(int projectId) async {
    try {
      _page++;
      List<Template> list = await _fetch(projectId);
      if (list.isNotEmpty) {
        _templates.addAll(list);
        _hasNextPage = list.length >= _size;
      } else {
        _hasNextPage = false;
      }
      return Future.value(true);
    } catch (e) {
      _page--;
      return Future.value(false);
    }
  }

  List<Template> get templates => _templates;

  bool get hasNextPage => _hasNextPage;

  LoadState get loadState => _loadState;
}

class Template {
  String key;
  String name;
  int projectId;

  Template._(this.key, this.name, this.projectId);

  factory Template.fromJson(e, int projectId) {
    return Template._(e['key'], e['name'], projectId);
  }

  Future<String> content() async {
    var response = await HttpClient.instance().get<Map<String, dynamic>>(Api.join('/projects/$projectId/templates/issues/$key'));
    return response.body()['content'];
  }
}
