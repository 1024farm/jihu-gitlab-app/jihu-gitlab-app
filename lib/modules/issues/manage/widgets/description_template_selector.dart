import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/paged_state.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/permission_low_view.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/widgets/description_template_selector_model.dart';

class DescriptionTemplateSelector extends StatefulWidget {
  static const String routeName = 'DescriptionTemplateSelector';

  final Map arguments;

  const DescriptionTemplateSelector({required this.arguments, super.key});

  @override
  State<StatefulWidget> createState() => _DescriptionTemplateSelectorState();
}

class _DescriptionTemplateSelectorState extends PagedState<DescriptionTemplateSelector> {
  final DescriptionTemplateSelectorModel _model = DescriptionTemplateSelectorModel();

  @override
  AppBar? buildAppBar() {
    return CommonAppBar(showLeading: true, title: Text(AppLocalizations.dictionary().descriptionSelectorTitle));
  }

  @override
  Widget buildRefreshView() {
    if (widget.arguments['free']) return PermissionLowView(featureName: AppLocalizations.dictionary().descriptionTemplate);
    if (_model.loadState == LoadState.successState && _model.templates.isEmpty) {
      return TipsView(icon: 'assets/images/no_item.svg', message: AppLocalizations.dictionary().descriptionSelectorNoTemplates, onRefresh: () => onRefresh());
    }
    return super.buildRefreshView();
  }

  @override
  Widget? buildListView() {
    return SafeArea(
        child: Padding(
            padding: const EdgeInsets.all(16),
            child: ListView.separated(
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: () {
                    var content = _model.templates[index].content();
                    Navigator.pop(context, {'templateName': _model.templates[index].name, 'content': content});
                  },
                  child: Container(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(6.0)),
                    ),
                    height: 80,
                    padding: const EdgeInsets.only(left: 12, right: 12),
                    child: Row(
                      children: <Widget>[
                        Text(
                          _model.templates[index].name,
                          style: const TextStyle(fontWeight: FontWeight.w400, fontSize: 16, color: Color(0XFF03162F)),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (BuildContext context, int index) => const Divider(thickness: 8, height: 8, color: Colors.white),
              itemCount: _model.templates.length,
            )));
  }

  @override
  Function hasNextPage() {
    return () => _model.hasNextPage;
  }

  @override
  Future<bool> Function() modelLoadMore() {
    return () => _model.loadMore(widget.arguments['projectId']);
  }

  @override
  Future<bool> Function() modelRefresh() {
    return () => _model.refresh(widget.arguments['projectId']);
  }

  @override
  Function isErrorState() {
    return () => _model.loadState == LoadState.errorState;
  }
}
