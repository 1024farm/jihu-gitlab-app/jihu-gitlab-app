import 'dart:core';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/loader.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/toast.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_info.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/issue_creation_page.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/issue_manage_page.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/models/issue_creation_model.dart';

class IssueUpdatePage extends StatefulWidget {
  final int projectId;
  final int issueIid;
  final IssueInfo info;

  const IssueUpdatePage({required this.projectId, required this.issueIid, required this.info, Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _IssueUpdatePageState();
}

class _IssueUpdatePageState extends State<IssueUpdatePage> {
  final IssueCreationModel _model = IssueCreationModel();
  late IssueInfo _info;
  final TextEditingController _issueTitleController = TextEditingController();
  final TextEditingController _issueDescriptionController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _info = widget.info.clone();
    _model.init(projectId: widget.projectId);
    if (ConnectionProvider.authorized) {
      loadIssue();
    }
  }

  Future<void> loadIssue() async {
    _issueTitleController.text = widget.info.title;
    _issueDescriptionController.text = widget.info.description;
    _model.templateName = null;
    _model.restoreConfidential(confidential: widget.info.confidential);
    setState(() {
      _model.initLabelAndAssign(widget.info.labels, widget.info.assignees.map((e) => e.toMember()).toList());
    });
  }

  @override
  Widget build(BuildContext context) {
    return IssueManagePage(
      onBack: () {
        var title = _issueTitleController.text;
        var description = _issueDescriptionController.text;
        if (_info.isSame(title, description)) {
          Navigator.pop(context);
          Navigator.pop(context);
        } else {
          showCupertinoDialog(
              context: context,
              builder: (context) {
                return CupertinoAlertDialog(
                    content: Text(
                      AppLocalizations.dictionary().editIssueBackContent,
                      style: const TextStyle(color: Color(0XFF03162F), fontSize: 14),
                    ),
                    actions: [
                      TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text(
                          AppLocalizations.dictionary().cancel,
                          style: const TextStyle(color: AppThemeData.secondaryColor, fontSize: 14),
                        ),
                      ),
                      TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                          Navigator.pop(context);
                          Navigator.pop(context);
                        },
                        child: Text(AppLocalizations.dictionary().ok, style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 14)),
                      )
                    ]);
              });
        }
      },
      onSuccess: () {
        var title = _issueTitleController.text;
        var description = _issueDescriptionController.text;
        Loader.showProgress(context);
        _model.update(widget.projectId, widget.issueIid, title, description: description).then((value) {
          Loader.hideProgress(context);
          if (value) {
            Toast.success(context, AppLocalizations.dictionary().updateSuccessful);
            Navigator.pop(context);
            Navigator.pop(context);
          } else {
            Toast.error(context, AppLocalizations.dictionary().updateFailed);
          }
        });
      },
      projectId: widget.projectId,
      model: _model,
      issueTitleController: _issueTitleController,
      issueDescriptionController: _issueDescriptionController,
      type: IssueManageViewType.update,
    );
  }
}
