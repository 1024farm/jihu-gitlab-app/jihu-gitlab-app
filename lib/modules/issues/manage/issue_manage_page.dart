import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/unauthorized_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/issue_creation_page.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/models/issue_creation_model.dart';
import 'package:provider/provider.dart';

class IssueManagePage extends StatefulWidget {
  final VoidCallback? onBack;
  final VoidCallback? onSuccess;
  final int projectId;
  final IssueCreationModel model;
  final TextEditingController issueTitleController;
  final TextEditingController issueDescriptionController;
  final IssueManageViewType type;
  final IssueType issueType;

  const IssueManagePage(
      {required this.projectId,
      required this.model,
      required this.issueTitleController,
      required this.issueDescriptionController,
      required this.type,
      this.onBack,
      this.onSuccess,
      this.issueType = IssueType.common,
      super.key});

  @override
  State<StatefulWidget> createState() => IssueManagePageState();
}

class IssueManagePageState extends State<IssueManagePage> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Scaffold(
            appBar: CommonAppBar(
              title: widget.type.isCreate ? Text(AppLocalizations.dictionary().createIssue(widget.issueType.label)) : Text(AppLocalizations.dictionary().editIssue),
              showLeading: true,
              leading: BackButton(color: Colors.black, onPressed: widget.onBack),
              actions: [
                if (ConnectionProvider.authorized)
                  Container(
                    transform: Matrix4.translationValues(-12, 2, 0),
                    child: TextButton(
                        onPressed: widget.onSuccess,
                        child: Text(widget.type.isCreate ? AppLocalizations.dictionary().create : AppLocalizations.dictionary().update, style: TextStyle(color: Theme.of(context).primaryColor))),
                  ),
              ],
            ),
            body: SafeArea(child: Consumer<ConnectionProvider>(
              builder: (context, _, child) {
                if (!ConnectionProvider.authorized) {
                  return const UnauthorizedView();
                }
                return IssueManageView(
                  projectId: widget.projectId,
                  model: widget.model,
                  issueTitleController: widget.issueTitleController,
                  issueDescriptionController: widget.issueDescriptionController,
                  type: widget.type,
                  issueType: widget.issueType,
                );
              },
            ))));
  }
}
