import 'dart:async';
import 'dart:core';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/loader.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar_and_name.dart';
import 'package:jihu_gitlab_app/core/widgets/input_field_with_title.dart';
import 'package:jihu_gitlab_app/core/widgets/label_color_and_name.dart';
import 'package:jihu_gitlab_app/core/widgets/label_view.dart';
import 'package:jihu_gitlab_app/core/widgets/markdown/markdown_input_box.dart';
import 'package:jihu_gitlab_app/core/widgets/selector/selectable.dart';
import 'package:jihu_gitlab_app/core/widgets/streaming_container.dart';
import 'package:jihu_gitlab_app/core/widgets/toast.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label_provider.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/issue_manage_page.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/models/issue_creation_model.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/widgets/description_template_selector.dart';
import 'package:jihu_gitlab_app/modules/issues/member/member.dart';
import 'package:jihu_gitlab_app/modules/issues/member/member_provider.dart';

part 'issue_manage_view.dart';

class IssueCreationPage extends StatefulWidget {
  final int projectId;
  final String from;
  final int? groupId;
  final IssueType issueType;

  const IssueCreationPage({required this.projectId, this.from = 'project', this.groupId, this.issueType = IssueType.common, Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _IssueCreationPageState();
}

class _IssueCreationPageState extends State<IssueCreationPage> {
  final IssueCreationModel _model = IssueCreationModel();
  final TextEditingController _issueTitleController = TextEditingController();
  final TextEditingController _issueDescriptionController = TextEditingController();
  late Timer _timer;

  @override
  void initState() {
    super.initState();
    _model.init(projectId: widget.projectId, groupId: widget.groupId);
    _timer = Timer.periodic(const Duration(milliseconds: 5000), (timer) {
      if (_issueTitleController.text.isNotEmpty || _issueDescriptionController.text.isNotEmpty) {
        _model.saveAsDraft(_issueTitleController.text, _issueDescriptionController.text);
      }
    });
    if (ConnectionProvider.authorized) {
      _loadDraftData();
    }
  }

  Future<void> _loadDraftData() async {
    var loadDraft = await _model.loadDraft();
    if (loadDraft.isNotEmpty) {
      _issueTitleController.text = loadDraft[0].title!;
      _issueDescriptionController.text = loadDraft[0].description!;
      _model.templateName = loadDraft[0].template;
      _model.restoreConfidential(confidential: loadDraft[0].confidential == 1);
      setState(() {
        _model.initLabelAndAssign(loadDraft[0].labels!, loadDraft[0].assignees!);
      });
    }
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return IssueManagePage(
      onBack: () {
        if (widget.from == 'group') {
          Navigator.pop(context);
          Navigator.pop(context, true);
        } else {
          Navigator.pop(context, true);
        }
      },
      onSuccess: () {
        var title = _issueTitleController.text;
        var description = _issueDescriptionController.text;
        var labels = _model.selectedLabels.map((e) => e.name).toList().join(",");
        Loader.showProgress(context);
        _model.create(widget.projectId, title, description: description, labels: labels).then((value) {
          Loader.hideProgress(context);
          if (value) {
            Toast.success(context, AppLocalizations.dictionary().createSuccessful);
            if (widget.from == 'group') {
              Navigator.pop(context);
              Navigator.pop(context, true);
            } else {
              Navigator.pop(context, true);
            }
          } else {
            Toast.error(context, AppLocalizations.dictionary().createFailed);
          }
        });
      },
      projectId: widget.projectId,
      model: _model,
      issueTitleController: _issueTitleController,
      issueDescriptionController: _issueDescriptionController,
      type: IssueManageViewType.create,
      issueType: widget.issueType,
    );
  }
}
