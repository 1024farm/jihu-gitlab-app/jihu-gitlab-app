part of 'issue_creation_page.dart';

typedef CreateIssueCallback = void Function({required String title, String? description, String? labels});

typedef TextProvider = String Function();

enum IssueType {
  common,
  faq;

  String get label {
    if (this == IssueType.common) {
      return AppLocalizations.dictionary().issue;
    }
    return AppLocalizations.dictionary().post;
  }
}

enum IssueManageViewType {
  create,
  update;

  bool get isCreate {
    return this == create;
  }
}

class IssueManageView extends StatefulWidget {
  const IssueManageView(
      {required this.projectId, required this.model, required this.issueTitleController, required this.issueDescriptionController, required this.type, this.issueType = IssueType.common, super.key});

  final int projectId;
  final IssueCreationModel model;
  final TextEditingController issueTitleController;
  final TextEditingController issueDescriptionController;
  final IssueManageViewType type;
  final IssueType issueType;

  @override
  State<IssueManageView> createState() => _IssueManageViewState();
}

class _IssueManageViewState extends State<IssueManageView> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        physics: const ScrollPhysics(),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          InputFieldWithTitle(
              title: AppLocalizations.dictionary().createIssueTitle, placeHolder: AppLocalizations.dictionary().issueTitlePlaceholder(widget.issueType.label), controller: widget.issueTitleController),
          if (isCommonIssue) _buildTitle(AppLocalizations.dictionary().template),
          if (isCommonIssue) _buildDescriptionTemplatesSelector(),
          _buildTitle(AppLocalizations.dictionary().description),
          Container(
              margin: const EdgeInsets.symmetric(horizontal: 16),
              child: MarkdownInputBox(
                projectId: widget.projectId,
                controller: widget.issueDescriptionController,
                issueSelectable: isCommonIssue,
                memberSelectable: isCommonIssue,
                placeholder: AppLocalizations.dictionary().issueDescriptionPlaceholder,
                onChanged: () {},
              )),
          if (widget.type.isCreate) ...onlyCreationView(),
          const SizedBox(height: 16)
        ]));
  }

  List<Widget> onlyCreationView() {
    return isCommonIssue
        ? [
            _buildAssigneeSelector(),
            _buildLabelSelector(),
            _buildConfidentialSwitcher(),
          ]
        : [];
  }

  Padding _buildTitle(String title, {EdgeInsetsGeometry padding = const EdgeInsets.all(16.0)}) {
    return Padding(
      padding: padding,
      child: Text(title, style: const TextStyle(color: Color(0XFF03162F), fontSize: 16, fontWeight: FontWeight.w600)),
    );
  }

  Widget _buildLabelSelector() {
    return Selectable<Label>(
        onSelected: (labels) => widget.model.onLabelSelect(labels),
        selectedItemBuilder: (label) => LabelView(labelName: label.name, textColor: label.textColor, color: label.color),
        selectorItemBuilder: (BuildContext context, int index, Label label) => LabelColorAndName(color: label.formattedColor, name: label.name),
        dataProviderProvider: () => LabelProvider(projectId: widget.projectId),
        keyMapper: (label) => label.id.id,
        title: AppLocalizations.dictionary().labels,
        pageTitle: AppLocalizations.dictionary().selectLabels,
        placeHolder: AppLocalizations.dictionary().labelsPlaceholder,
        multiSelection: () => Future(() => true),
        initialData: widget.model.selectedLabels,
        projectId: widget.projectId,
        filter: (keyword, e) => e.name.toUpperCase().contains(keyword.toUpperCase()));
  }

  Widget _buildAssigneeSelector() {
    return Selectable<Member>(
        onSelected: (members) => widget.model.onAssigneesSelect(members),
        selectedItemBuilder: (member) => AvatarAndName(username: member.username, avatarUrl: member.avatarUrl),
        selectorItemBuilder: (BuildContext context, int index, Member member) => AvatarAndName(username: member.username, avatarUrl: member.avatarUrl),
        dataProviderProvider: () => MemberProvider(projectId: widget.projectId),
        keyMapper: (member) => member.id,
        title: AppLocalizations.dictionary().assignees,
        pageTitle: AppLocalizations.dictionary().selectAssignees,
        placeHolder: AppLocalizations.dictionary().assigneesPlaceholder,
        multiSelection: () => widget.model.isMultiAssignees(),
        warningNotice: () => Future(() => widget.model.warningNoticeWhenNoLicense()),
        initialData: widget.model.selectedAssignees,
        filter: (keyword, e) => e.username.toUpperCase().contains(keyword.toUpperCase()));
  }

  Widget _buildDescriptionTemplatesSelector() {
    List<Widget> children = [];
    if (widget.model.templateName != null) {
      children = [Text(widget.model.templateName!, style: const TextStyle(fontWeight: FontWeight.w400, fontSize: 16, color: Color(0XFF03162F)))];
    }
    return _buildFlexibleContainer(AppLocalizations.dictionary().templatePlaceholder, children, () async {
      var result = await Navigator.of(context).pushNamed(DescriptionTemplateSelector.routeName, arguments: {
        "projectId": widget.projectId,
        "free": await widget.model.isFree(),
      }) as Map<String, dynamic>;
      var content = await result['content'];
      widget.model.templateName = result['templateName'];
      widget.issueDescriptionController.text = content;
      setState(() {});
    }, buttonKey: const Key("goto-description-selector"));
  }

  Widget _buildFlexibleContainer(String placeHolder, List<Widget> children, VoidCallback onPressed, {Key? buttonKey}) {
    bool hasNoData = children.isEmpty;
    if (hasNoData) {
      children = [Text(placeHolder, style: const TextStyle(color: AppThemeData.secondaryColor, fontSize: 14, fontWeight: FontWeight.w400))];
    }
    return InkWell(
      onTap: onPressed,
      child: Container(
        width: double.infinity,
        decoration: const BoxDecoration(border: Border.fromBorderSide(BorderSide(color: Color(0XFFEAEAEA), width: 1)), borderRadius: BorderRadius.all(Radius.circular(4)), color: Colors.white),
        padding: const EdgeInsets.only(left: 8),
        margin: const EdgeInsets.only(left: 16, right: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(flex: 1, child: StreamingContainer(children: children)),
            widget.model.templateName == null
                ? Container()
                : IconButton(
                    alignment: Alignment.centerRight,
                    onPressed: () {
                      widget.issueDescriptionController.text = '';
                      widget.model.templateName = null;
                      setState(() {});
                    },
                    icon: SvgPicture.asset('assets/images/clear.svg', colorFilter: const ColorFilter.mode(Color(0xFF95979A), BlendMode.srcIn), width: 18, key: const Key('close'))),
            IconButton(key: buttonKey, onPressed: onPressed, icon: const Icon(Icons.add, size: 22, color: Color(0xFF87878C)))
          ],
        ),
      ),
    );
  }

  Widget _buildConfidentialSwitcher() {
    return Container(
      margin: const EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(flex: 1, child: _buildTitle(AppLocalizations.dictionary().confidential, padding: EdgeInsets.zero)),
              CupertinoSwitch(value: widget.model.confidential, onChanged: (value) => setState(() => widget.model.toggleConfidential()), activeColor: Theme.of(context).primaryColor),
            ],
          ),
          Text(AppLocalizations.dictionary().confidentialHint, style: const TextStyle(color: AppThemeData.secondaryColor, fontSize: 14, fontWeight: FontWeight.w400)),
        ],
      ),
    );
  }

  bool get isCommonIssue => widget.issueType == IssueType.common;
}
