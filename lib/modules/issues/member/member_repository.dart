import 'package:jihu_gitlab_app/core/db_manager.dart';
import 'package:jihu_gitlab_app/modules/issues/member/member_entity.dart';
import 'package:sqflite/sqflite.dart';

class MemberRepository {
  Future<List<MemberEntity>> query(int projectId, Iterable<int> memberIds) async {
    String sql = "select * from ${MemberEntity.tableName} where project_id = $projectId and member_id in (${memberIds.join(", ")})";
    var list = await DbManager.instance().rawFind(sql);
    return list.map((e) => MemberEntity.restore(e)).toList();
  }

  Future<List<int>> insert(List<MemberEntity> memberEntities) async {
    // TODO: Cannot mock db.transaction, so add try catch for test
    try {
      return await DbManager.instance().batchInsert(MemberEntity.tableName, memberEntities.map((e) => e.toMap()).toList());
    } catch (e) {
      return [];
    }
  }

  Future<int> update(List<MemberEntity> memberEntities) async {
    return await DbManager.instance().batchUpdate(MemberEntity.tableName, memberEntities.map((e) => e.toMap()).toList());
  }

  Future<List<MemberEntity>> queryByProject(int projectId) async {
    Database database = await DbManager.instance().getDb();
    List<Map<String, Object?>> query = await database.query(MemberEntity.tableName, where: " project_id = ? ", whereArgs: [projectId]);
    return query.map((e) => MemberEntity.restore(e)).toList();
  }

  Future<int> deleteLessThan(int version, int projectId) async {
    Database database = await DbManager.instance().getDb();
    return await database.delete(MemberEntity.tableName, where: " project_id = ? and version < ? ", whereArgs: [projectId, version]);
  }
}
