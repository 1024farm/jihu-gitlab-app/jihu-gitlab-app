class Member {
  int id;
  String name;
  String username;
  String avatarUrl;
  bool checked = false;

  Member(this.id, this.name, this.username, this.avatarUrl);

  void toggleCheck({bool? check}) {
    if (check != null) {
      checked = check;
    }
  }

  Map<String, dynamic> toJson() => {'id': id, 'name': name, 'username': username, 'avatarUrl': avatarUrl};

  factory Member.fromJson(Map<String, dynamic> json) {
    return Member(json['id'], json['name'] ?? "", json['username'], json['avatarUrl']);
  }
}
