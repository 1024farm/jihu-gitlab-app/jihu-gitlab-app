import 'dart:async';

import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/domain/global_time.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';
import 'package:jihu_gitlab_app/core/widgets/selector/data_provider.dart';
import 'package:jihu_gitlab_app/modules/issues/member/member.dart';
import 'package:jihu_gitlab_app/modules/issues/member/member_entity.dart';
import 'package:jihu_gitlab_app/modules/issues/member/member_repository.dart';

class MemberProvider extends DataProvider<Member> {
  static const int _pageSize = 50;
  int _page = 1;
  bool _hasNextPage = false;
  late MemberRepository _memberRepository;
  int projectId;

  MemberProvider({required this.projectId}) {
    _memberRepository = MemberRepository();
  }

  @override
  Future<List<Member>> loadFromLocal() async {
    try {
      var list = await _memberRepository.queryByProject(projectId);
      return list.map((e) => Member(e.memberId, e.name ?? '', e.username ?? '', e.avatarUrl ?? '')).toList();
    } catch (e) {
      LogHelper.err("MemberProvider loadFromLocal error", e);
      return [];
    }
  }

  @override
  Future<bool> syncFromRemote() async {
    try {
      await _getAllMembers();
      return Future.value(true);
    } catch (e) {
      LogHelper.err("MemberProvider syncFromRemote error", e);
      return Future.value(false);
    }
  }

  Future<bool> _getAllMembers() async {
    int version = GlobalTime.now().millisecondsSinceEpoch;
    bool hasAnyChange = true;
    do {
      var response = await ProjectRequestSender.instance().get<List<dynamic>>(Api.join("/projects/$projectId/members/all?page=$_page&per_page=$_pageSize"));
      var entities = response.body().map((e) => MemberEntity.create(e, projectId, version)).toList();
      await _save(entities, version, projectId);
      _hasNextPage = entities.length >= _pageSize;
      _page++;
    } while (_hasNextPage);
    return Future.value(hasAnyChange);
  }

  Future<void> _save(List<MemberEntity> memberEntities, int version, int projectId) async {
    List<int> memberIds = memberEntities.map((e) => e.memberId).toList();
    List<MemberEntity> exists = await _memberRepository.query(projectId, memberIds);
    List<MemberEntity> shouldBeAdd = [];
    List<MemberEntity> shouldBeUpdate = [];
    for (var memberEntity in memberEntities) {
      var where = exists.where((e) => e == memberEntity);
      if (where.isEmpty) {
        shouldBeAdd.add(memberEntity);
      } else {
        memberEntity.id = where.first.id;
        shouldBeUpdate.add(memberEntity);
      }
    }
    if (shouldBeAdd.isNotEmpty) {
      await _memberRepository.insert(shouldBeAdd);
    }
    if (shouldBeUpdate.isNotEmpty) {
      await _memberRepository.update(shouldBeUpdate);
    }
    await _memberRepository.deleteLessThan(version, projectId);
  }

  @visibleForTesting
  void setRepository(MemberRepository memberRepository) {
    _memberRepository = memberRepository;
  }
}
