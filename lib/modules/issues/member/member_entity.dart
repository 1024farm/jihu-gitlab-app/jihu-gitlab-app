class MemberEntity {
  static const tableName = "project_members";

  int? id;
  int memberId;
  int projectId;
  String? name;
  String? username;
  String? avatarUrl;
  int version;

  MemberEntity._internal(this.id, this.memberId, this.projectId, this.name, this.username, this.avatarUrl, this.version);

  static MemberEntity restore(Map<String, dynamic> map) {
    return MemberEntity._internal(map['id'], map['member_id'], map['project_id'], map['name'], map['username'], map['avatar_url'], map['version']);
  }

  static MemberEntity create(Map<String, dynamic> map, int projectId, int version) {
    return MemberEntity._internal(null, map['id'], projectId, map['name'], map['username'], map['avatar_url'], version);
  }

  static String createTableSql() {
    return """ 
    create table $tableName(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        member_id INTEGER NOT NULL,
        project_id INTEGER NOT NULL,
        name TEXT ,
        username TEXT ,
        avatar_url TEXT,
        version INTEGER
    );    
    """;
  }

  Map<String, dynamic> toMap() {
    return {'id': id, 'member_id': memberId, 'project_id': projectId, 'name': name, 'username': username, 'avatar_url': avatarUrl, 'version': version};
  }

  void update(MemberEntity memberEntity) {
    name = memberEntity.name;
    username = memberEntity.username;
    avatarUrl = memberEntity.avatarUrl;
    version = memberEntity.version;
  }

  @override
  bool operator ==(Object other) => identical(this, other) || other is MemberEntity && runtimeType == other.runtimeType && memberId == other.memberId && projectId == other.projectId;

  @override
  int get hashCode => memberId.hashCode ^ projectId.hashCode;
}
