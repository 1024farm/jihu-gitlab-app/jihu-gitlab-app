import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/round_rect_container.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/details/paginable_list_view.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/milestone_and_iteration_state_view.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestone_item_view.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestones_fetcher.dart';

class MilestoneSelectorPage extends StatefulWidget {
  const MilestoneSelectorPage({required this.fullPath, this.selectedId, this.expectState, Key? key}) : super(key: key);
  final String fullPath;
  final String? selectedId;
  final MilestoneAndIterationState? expectState;

  @override
  State<MilestoneSelectorPage> createState() => _MilestoneSelectorPageState();
}

class _MilestoneSelectorPageState extends State<MilestoneSelectorPage> {
  Milestone? _selected;
  String? _selectedId;
  late ProjectMilestoneFetcher _projectMilestoneFetcher;
  late PaginableListViewModel<Milestone> _paginableListViewModel;

  @override
  void initState() {
    _selectedId = widget.selectedId;
    _projectMilestoneFetcher = ProjectMilestoneFetcher(widget.fullPath, widget.expectState?.name);
    _paginableListViewModel = PaginableListViewModel(onRefresh: (_) => _projectMilestoneFetcher.fetch(), onLoadMore: (endCursor, _) => _projectMilestoneFetcher.fetch(endCursor));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CommonAppBar(
          showLeading: true,
          title: Text(AppLocalizations.dictionary().selectMilestone),
          actions: [
            Container(
                transform: Matrix4.translationValues(-12, 2, 0),
                child: TextButton(onPressed: () => Navigator.pop(context, _selected ?? ""), child: Text(AppLocalizations.dictionary().done, style: TextStyle(color: Theme.of(context).primaryColor))))
          ],
        ),
        body: PaginableListView<Milestone>(
          model: _paginableListViewModel,
          itemBuilder: <Milestone>(context, index, data) {
            var item = data[index];
            return InkWell(
                onTap: () => _onSelectedChanged(item),
                child: RoundRectContainer(
                  margin: const EdgeInsets.symmetric(horizontal: 16),
                  padding: const EdgeInsets.all(12),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      _selectedId == item.id ? Icon(Icons.check_circle, color: Theme.of(context).primaryColor, size: 18) : const Icon(Icons.radio_button_unchecked, color: Color(0XFFCECECE), size: 18),
                      const SizedBox(width: 8),
                      ProjectMilestoneItemView(milestone: item),
                    ],
                  ),
                ));
          },
          separatorBuilder: (context, index) => const Divider(height: 12, thickness: 12, color: Colors.transparent),
        ));
  }

  void _onSelectedChanged(Milestone milestone) {
    setState(() {
      _selected = _selected == milestone ? null : milestone;
      _selectedId = _selected?.id;
    });
  }
}
