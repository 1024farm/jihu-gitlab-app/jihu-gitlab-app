class IssueDetailsParam {
  int projectId;
  int targetId;
  int targetIid;
  int issueId;
  int issueIid;
  String pathWithNamespace;

  IssueDetailsParam(this.projectId, this.issueId, this.issueIid, this.targetId, this.targetIid, this.pathWithNamespace);

  factory IssueDetailsParam.fromJson(Map json) {
    return IssueDetailsParam(json['projectId'] ?? 0, json['issueId'] ?? 0, json['issueIid'] ?? 0, json['targetId'] ?? 0, json['targetIid'] ?? 0, json['pathWithNamespace'] ?? '');
  }

  @override
  bool operator ==(Object other) {
    return other is IssueDetailsParam && projectId == other.projectId && targetId == other.targetId && issueId == other.issueId;
  }

  @override
  int get hashCode => Object.hash(projectId, targetId, issueId);
}
