import 'package:jihu_gitlab_app/modules/issues/details/models/issue_description_view.dart';

class IssueDiscussionContentView extends IssueDescriptionView {
  IssueDiscussionContentView({
    required super.projectId,
    required super.data,
    required super.context,
    super.padding,
    super.key,
  });
}
