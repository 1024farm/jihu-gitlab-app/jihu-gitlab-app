import 'package:jihu_gitlab_app/core/domain/assignee/assignee.dart';
import 'package:jihu_gitlab_app/core/id.dart';
import 'package:jihu_gitlab_app/core/settings/settings.dart';
import 'package:jihu_gitlab_app/core/time.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/description_content.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestones_fetcher.dart';

class IssueInfo {
  int id;
  int iid;
  Id projectId;
  String title;
  String description;
  String createdAt;
  String projectName;
  Assignee author;
  String webUrl;
  bool confidential;
  String state;
  List<String> _issueLabelNames;
  List<Label> labels;
  List<Assignee> assignees;
  Milestone? milestone;

  IssueInfo._(this.id, this.iid, this.projectId, this.title, this.description, this.createdAt, this.projectName, this.author, this.webUrl, this.confidential, this.state, this._issueLabelNames,
      this.labels, this.assignees, this.milestone);

  String recentActivities() {
    return " ${Settings.instance().precisionTime().isEnabled() ? AppLocalizations.dictionary().createdAtPreciseFormat : AppLocalizations.dictionary().created}";
  }

  Time recentActivityTimes() {
    return Time.init(createdAt);
  }

  factory IssueInfo.fromGraphQLJson(Map<String, dynamic> projectJson) {
    var issueJson = projectJson['issue'];
    String description = DescriptionContent.init(issueJson?['description'] ?? '', projectJson['fullPath']).value;
    return IssueInfo._(
        Id.fromGid(issueJson['id'] ?? '/0').id,
        int.parse(issueJson['iid'] ?? '0'),
        Id.fromGid(projectJson['id'] ?? "/0"),
        issueJson['title'] ?? '',
        description,
        issueJson['createdAt'] ?? '',
        projectJson['name'] ?? '',
        Assignee.fromGraphQLJson(issueJson['author'] ?? {}),
        issueJson['webUrl'] ?? '',
        issueJson['confidential'] ?? false,
        issueJson['state'] ?? '',
        [],
        ((issueJson['labels']?['nodes'] ?? []) as List).map((e) => Label.fromGraphQLJson(e)).toList(),
        ((issueJson['assignees']?['nodes'] ?? []) as List).map((e) => Assignee.fromGraphQLJson(e)).toList(),
        issueJson['milestone'] == null ? null : Milestone.fromJson(issueJson['milestone']));
  }

  IssueInfo clone() => IssueInfo._(id, iid, projectId, title, description, createdAt, projectName, author, webUrl, confidential, state, _issueLabelNames, labels, assignees, milestone);

  bool isSame(String title, String description) => this.title == title && this.description == description;
}
