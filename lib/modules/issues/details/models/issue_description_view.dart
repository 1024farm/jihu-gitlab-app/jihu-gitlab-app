import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:jihu_gitlab_app/core/markdown_view.dart';
import 'package:jihu_gitlab_app/core/widgets/markdown/number_sign_syntax.dart';
import 'package:jihu_gitlab_app/modules/issues/details/issue_details_page.dart';

class IssueDescriptionView extends MarkdownView {
  final int projectId;

  IssueDescriptionView({required this.projectId, required super.data, required super.context, super.padding = EdgeInsets.zero, super.key, String? specificHost})
      : super(
          builders: {'code': CodeElementBuilder(), '#': NumberSignBuilder(projectId: projectId, context: context)},
          inlineSyntaxes: [NumberSignSyntax()],
          physics: const NeverScrollableScrollPhysics(),
          specificHost: specificHost,
        );
}

class NumberSignBuilder extends MarkdownElementBuilder {
  final int projectId;
  final BuildContext context;

  NumberSignBuilder({required this.projectId, required this.context});

  @override
  Widget? visitElementAfter(element, TextStyle? preferredStyle) {
    return Text.rich(
      TextSpan(
          text: element.textContent,
          style: const TextStyle(color: Color(0xFF0D10FC)),
          recognizer: TapGestureRecognizer()
            ..onTap =
                () => Navigator.pushNamed(context, IssueDetailsPage.routeName, arguments: {"showLeading": true, "projectId": projectId, "issueIid": int.parse(element.textContent.substring(1))})),
    );
  }
}
