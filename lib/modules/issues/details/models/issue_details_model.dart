import 'package:collection/collection.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';
import 'package:jihu_gitlab_app/core/project_identity.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/selector/data_provider.dart';
import 'package:jihu_gitlab_app/modules/issues/details/issue_details_param.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion_provider.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_info.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_link.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/models/issue_update_model.dart';

class IssueDetailsModel {
  IssueInfo? _issueInfo;

  LoadState _loadState = LoadState.loadingState;
  final List<Discussion> _comments = [];
  DataProvider<Discussion>? _dataProvider;

  int? selectedNoteId;
  String selectedDiscussionId = "";
  bool isInternal = false;

  late ValueNotifier<List<Discussion>> _notifier;
  late int _projectId;
  late int _issueId;
  late int _issueIid;
  late String _fullPath;
  late IssueUpdateModel _issueUpdateModel;
  Map<IssueLinkType, List<IssueLink>> _groupedLinks = {};
  late ValueNotifier<Map<IssueLinkType, List<IssueLink>>> _linksNotifier;
  late ProjectIdentity _projectIdentity;

  void config(IssueDetailsParam param) {
    _projectId = param.projectId;
    _issueId = param.targetId == 0 ? param.issueId : param.targetId;
    _issueIid = param.targetIid == 0 ? param.issueIid : param.targetIid;
    _fullPath = param.pathWithNamespace;
    _projectIdentity = ProjectIdentity(_projectId, _fullPath);
    _dataProvider = DiscussionProvider(issueId: _issueId, issueIid: _issueIid, projectId: _projectId, pathWithNamespace: _fullPath);
    _comments.clear();
    _notifier = ValueNotifier(_comments);
    _linksNotifier = ValueNotifier(_groupedLinks);
    _issueUpdateModel = IssueUpdateModel();
    dispose();
    ConnectionProvider().addListener(loadDiscussions);
    ProjectProvider().addListener(loadDiscussions);
  }

  void dispose() {
    ConnectionProvider().removeListener(loadDiscussions);
    ProjectProvider().removeListener(loadDiscussions);
  }

  Future<void> loadDiscussions() async {
    _comments.clear();
    if (!ProjectProvider().isSpecifiedHostHasConnection || _projectId == 0) {
      _notifier.value = _comments;
      _loadState = LoadState.successState;
      return;
    }
    if (!ProjectProvider().isSpecifiedHostHasConnection) return;
    _dataProvider?.syncFromRemote().then((value) {
      _loadLocalDataAndNotify();
    });
  }

  Future<void> _loadLocalDataAndNotify() async {
    List<Discussion> data = await _dataProvider!.loadFromLocal();
    _comments.addAll(data);
    _notifier.value = data;
    _loadState = LoadState.successState;
  }

  Future getIssueInfo() async {
    try {
      await _ensureFullPathNotEmpty();
      var resp = await ProjectRequestSender.instance().post(Api.graphql(), getIssueDetailsGraphQLRequestBody(_fullPath, _issueIid));
      getLinks();
      var project = resp.body()['data']['project'];
      _issueInfo = IssueInfo.fromGraphQLJson(project);
      _loadState = LoadState.successState;
    } catch (e) {
      LogHelper.err('IssueDetailsModel getIssueInfo error', e);
      if (e.runtimeType == DioError && (e as DioError).response?.statusCode == 404) {
        _loadState = LoadState.noItemState;
      } else {
        _loadState = LoadState.errorState;
      }
    }
  }

  Future<void> getLinks() async {
    try {
      var resp = await ProjectRequestSender.instance().get(Api.join("/projects/$projectId/issues/$_issueIid/links"));
      var list = (resp.body() as List).map((e) => IssueLink.fromJson(e)).toList();
      _groupedLinks = groupBy<IssueLink, IssueLinkType>(list, (e) => e.linkType);
      _linksNotifier.value = Map.from(_groupedLinks);
    } catch (_) {}
  }

  Future<void> _ensureFullPathNotEmpty() async {
    if (_fullPath.isEmpty) {
      var response = await ProjectRequestSender.instance().get<Map<String, dynamic>>(Api.join("projects/$_projectId"));
      _fullPath = response.body()['path_with_namespace'];
    }
  }

  Future<void> updateState() async {
    Map<String, dynamic> map = {};
    map['state_event'] = _issueInfo?.state == "closed" ? "reopen" : "close";
    try {
      await HttpClient.instance().put(Api.join("/projects/$_projectId/issues/$_issueIid"), map);
    } catch (e) {
      LogHelper.err('IssueDetailsModel updateState error', e);
      _loadState = LoadState.errorState;
    } finally {
      await getIssueInfo();
    }
  }

  Future<void> updateConfidential() async {
    Map<String, dynamic> map = {};
    map['confidential'] = _issueInfo!.confidential ? false : true;
    try {
      await HttpClient.instance().put(Api.join("/projects/$_projectId/issues/$_issueIid"), map);
    } catch (e) {
      LogHelper.err('IssueDetailsModel updateConfidential error', e);
      _loadState = LoadState.errorState;
    } finally {
      await getIssueInfo();
    }
  }

  Future<bool> updateLabels(List<String> labels) async {
    try {
      var result = await _issueUpdateModel.update(_projectId, _issueIid, labels: labels.isNotEmpty ? labels.join(',') : '');
      if (result) await getIssueInfo();
      return result;
    } catch (e) {
      LogHelper.err('IssueDetailsModel updateLabels error', e);
      return false;
    }
  }

  Future<bool> updateMilestone(int milestoneId) async {
    try {
      var result = await _issueUpdateModel.update(_projectId, _issueIid, milestoneId: milestoneId);
      if (result) await getIssueInfo();
      return result;
    } catch (e) {
      LogHelper.err('IssueDetailsModel updateLabels error', e);
      return false;
    }
  }

  Future<bool> updateAssignees(List<int> assigneeIds) async {
    try {
      var result = await _issueUpdateModel.update(_projectId, _issueIid, assigneeIds: assigneeIds);
      if (result) await getIssueInfo();
      return result;
    } catch (e) {
      LogHelper.err('IssueDetailsModel updateLabels error', e);
      return false;
    }
  }

  Future _saveComment(String comments) async {
    Map<String, dynamic> data = {};
    data['body'] = comments;
    data['confidential'] = isInternal;
    data['internal'] = isInternal;
    try {
      await HttpClient.instance().post<dynamic>(Api.join("projects/$_projectId/issues/$_issueIid/notes"), data);
    } catch (e) {
      LogHelper.err('IssueDetailsModel _saveComment error', e);
      _loadState = LoadState.errorState;
    }
  }

  Future _saveReplyComment(int noteId, String comments) async {
    Map<String, dynamic> data = {};
    data['body'] = comments;
    data['note_id'] = noteId;
    try {
      await HttpClient.instance().post<dynamic>(Api.join("/projects/$_projectId/issues/$_issueIid/discussions/$selectedDiscussionId/notes"), data);
    } catch (e) {
      LogHelper.err('IssueDetailsModel _saveReplyComment error', e);
      _loadState = LoadState.errorState;
    }
  }

  Future<void> saveComment(String commentContent) async {
    if (selectedNoteId != null) {
      await _saveReplyComment(selectedNoteId!, commentContent);
    } else {
      await _saveComment(commentContent);
    }
  }

  void resetForReplyToComment() {
    selectedNoteId = null;
    isInternal = false;
  }

  void loadingPage() {
    _loadState = LoadState.loadingState;
  }

  IssueInfo? get issueInfo => _issueInfo;

  int get projectId => _projectId;

  LoadState get loadState => _loadState;

  List<Discussion> get comments => _comments;

  ValueNotifier<List<Discussion>> get notifier => _notifier;

  bool get shouldShowInternalSwitch => selectedNoteId == null;

  int get issueIid => _issueIid;

  String get fullPath => _fullPath;

  ProjectIdentity get projectIdentity => _projectIdentity;

  ValueNotifier<Map<IssueLinkType, List<IssueLink>>> get linksNotifier => _linksNotifier;

  @visibleForTesting
  void injectDataProviderForTesting(DataProvider<Discussion> dataProvider) => _dataProvider = dataProvider;
}
