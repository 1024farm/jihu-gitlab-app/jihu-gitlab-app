import 'package:jihu_gitlab_app/core/domain/issue.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

enum IssueLinkType {
  relatesTo("relates_to"),
  isBlockedBy("is_blocked_by"),
  blocks("blocks");

  final String value;

  const IssueLinkType(this.value);

  factory IssueLinkType.valueOf(String value) {
    var linkTypes = IssueLinkType.values.where((e) => e.value == value).toList();
    return linkTypes.isNotEmpty ? linkTypes[0] : IssueLinkType.relatesTo;
  }

  String get label {
    if (this == IssueLinkType.relatesTo) {
      return AppLocalizations.dictionary().relatesTo;
    }
    if (this == IssueLinkType.isBlockedBy) {
      return AppLocalizations.dictionary().isBlockedBy;
    }
    return AppLocalizations.dictionary().blocks;
  }
}

class IssueLink {
  int issueLinkId;
  IssueLinkType linkType;
  Issue issue;

  IssueLink._(this.issueLinkId, this.linkType, this.issue);

  factory IssueLink.fromJson(Map<String, dynamic> json) {
    return IssueLink._(json['issue_link_id'], IssueLinkType.valueOf(json['link_type'] ?? ""), Issue.fromJson(json));
  }
}
