import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/domain/global_time.dart';
import 'package:jihu_gitlab_app/core/list_comparator.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/project_synchronizer.dart';
import 'package:jihu_gitlab_app/core/widgets/selector/data_provider.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion_entity.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion_repository.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/note/note.dart';

class DiscussionProvider extends DataProvider<Discussion> {
  late int issueId;
  late int issueIid;
  late int projectId;
  late String pathWithNamespace;
  late DiscussionRepository _discussionRepository;
  bool isFaqDiscussion;

  DiscussionProvider({required this.issueId, required this.issueIid, required this.projectId, required this.pathWithNamespace, this.isFaqDiscussion = false}) {
    _discussionRepository = DiscussionRepository.instance();
  }

  @override
  Future<List<Discussion>> loadFromLocal() async {
    var list = await _discussionRepository.queryByIssue(issueId);
    return list
        .map((e) => Discussion(
            e.discussionId,
            e.notes!
                .map((entity) => Note.fromJson({
                      'id': entity.noteId,
                      'noteable_id': entity.noteableId,
                      'noteable_iid': entity.noteableIid,
                      'body': entity.body,
                      'system': entity.system,
                      'author': {
                        'id': entity.assigneeEntity?.assigneeId,
                        'name': entity.assigneeEntity?.name,
                        'username': entity.assigneeEntity?.username,
                        'avatar_url': entity.assigneeEntity?.avatarUrl,
                      },
                      'created_at': entity.createdAt,
                      'internal': entity.internal == 1
                    }, e.pathWithNamespace))
                .toList(),
            individualNote: e.individualNote))
        .toList();
  }

  @override
  Future<bool> syncFromRemote() async {
    int version = GlobalTime.now().millisecondsSinceEpoch;
    return _getAllDiscussions(version);
  }

  Future<bool> _getAllDiscussions(int version) async {
    Connection? connection;
    if (isFaqDiscussion) connection = ConnectionProvider().communityConnection;
    return ProjectSynchronizer<dynamic>(
        url: Api.join('projects/$projectId/issues/$issueIid/discussions'),
        connection: connection,
        dataProcessor: (data, page, pageSize) async {
          List<DiscussionEntity> entities =
              (data as List).map((e) => DiscussionEntity.create(e, pathWithNamespace: pathWithNamespace, issueId: issueId, issueIid: issueIid, version: version)).toList(growable: false);
          await _save(entities.where((element) => element.notes![0].system == false).toList(), version);
          return entities.length >= pageSize;
        }).run();
  }

  Future<void> _save(List<DiscussionEntity> entities, int version) async {
    List<String> discussionIdList = entities.map((e) => e.discussionId).toList();
    List<DiscussionEntity> exists = await _discussionRepository.queryByIssue(issueId, discussionIds: discussionIdList);
    ListCompareResult<DiscussionEntity> result = ListComparator.compare<DiscussionEntity>(exists, entities);
    if (result.hasAdd) {
      await _discussionRepository.insert(result.add);
    }
    if (result.hasUpdate) {
      var list = result.update.map((e) {
        e.left.update(e.right);
        return e.left;
      }).toList();
      await _discussionRepository.update(list);
    }
    await _discussionRepository.deleteLessThan(issueId, version);
  }

  @visibleForTesting
  void injectInstanceForTesting(DiscussionRepository mockRepository) {
    _discussionRepository = mockRepository;
  }
}
