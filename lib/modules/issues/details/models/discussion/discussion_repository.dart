import 'package:jihu_gitlab_app/core/db_manager.dart';
import 'package:jihu_gitlab_app/core/list_comparator.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion_entity.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/note/note_entity.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/note/note_repository.dart';

class DiscussionRepository {
  static final DiscussionRepository _instance = DiscussionRepository._internal();

  DiscussionRepository._internal();

  factory DiscussionRepository.instance() => _instance;

  Future<List<DiscussionEntity>> queryByIssue(int issueId, {List<String>? discussionIds}) async {
    String sql = "select * from ${DiscussionEntity.tableName} where issue_id = $issueId";
    if (discussionIds != null) {
      sql += " and discussion_id in (${discussionIds.map((e) => "'$e'").toList().join(',')})";
    }
    var list = await DbManager.instance().rawFind(sql);
    List<Map<String, dynamic>> newList = [];
    for (var item in list) {
      var noteEntityList = await NoteRepository.instance().queryByDiscussion(item['discussion_id'] as String);
      Map<String, dynamic> newItem = Map.from(item);
      newItem['notes'] = noteEntityList.map((e) => e.toMap()).toList();
      newList.add(newItem);
    }
    return newList.map((e) => DiscussionEntity.restore(e)).toList();
  }

  Future<List> insert(List<DiscussionEntity> entities) async {
    for (var entity in entities) {
      if (entity.notes != null && entity.notes!.isNotEmpty) {
        _saveNotes(entity.notes!.where((i) => i.system == false).toList(), entity.discussionId, entity.version);
      }
    }

    var discussions = entities
        .map((e) => {
              'id': e.id,
              'discussion_id': e.discussionId,
              'issue_id': e.issueId,
              'issue_iid': e.issueIid,
              'individual_note': e.individualNote ? 1 : 0,
              'path_with_namespace': e.pathWithNamespace,
              'version': e.version,
            })
        .toList();
    // TODO: Cannot mock db.transaction, so add try catch for test
    try {
      return await DbManager.instance().batchInsert(DiscussionEntity.tableName, discussions);
    } catch (e) {
      return [];
    }
  }

  Future<void> _saveNotes(List<NoteEntity> entities, String discussionId, int version) async {
    List<int> noteIdList = entities.map((e) => e.noteId).toList();
    List<NoteEntity> exists = await NoteRepository.instance().queryByDiscussion(discussionId, noteIds: noteIdList);
    ListCompareResult<NoteEntity> result = ListComparator.compare<NoteEntity>(exists, entities);
    if (result.hasAdd) {
      await NoteRepository.instance().insert(result.add, version);
    }
    if (result.hasUpdate) {
      var list = result.update.map((e) {
        e.left.update(e.right);
        return e.left;
      }).toList();
      await NoteRepository.instance().update(list, version);
    }
    await NoteRepository.instance().deleteLessThan(discussionId, version);
  }

  Future<void> update(List<DiscussionEntity> entities) async {
    var discussions = entities
        .map((e) => {
              'id': e.id,
              'discussion_id': e.discussionId,
              'issue_id': e.issueId,
              'issue_iid': e.issueIid,
              'individual_note': e.individualNote,
              'path_with_namespace': e.pathWithNamespace,
              'version': e.version,
            })
        .toList();
    await DbManager.instance().batchUpdate(DiscussionEntity.tableName, discussions);

    for (var entity in entities) {
      if (entity.notes != null && entity.notes!.isNotEmpty) {
        await _saveNotes(entity.notes!.where((i) => i.system == false).toList(), entity.discussionId, entity.version);
      }
    }
  }

  // TODO: To be confirm if needs delete notes and assignee
  Future<int> deleteLessThan(int issueId, int version) async {
    return await DbManager.instance().delete(DiscussionEntity.tableName, where: ' issue_id = ? and version < ? ', whereArgs: [issueId, version]);
  }
}
