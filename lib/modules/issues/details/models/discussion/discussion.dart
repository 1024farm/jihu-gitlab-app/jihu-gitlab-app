import 'package:jihu_gitlab_app/modules/issues/details/models/note/note.dart';

class Discussion {
  String id;
  bool individualNote;
  List<Note> notes;
  late Note discussionNote;
  List<Note> replyNotes = [];

  Discussion(this.id, this.notes, {required this.individualNote}) {
    discussionNote = notes[0];
    replyNotes = notes.sublist(1);
  }

  bool get hasNotes => !individualNote && notes.length > 1;
}
