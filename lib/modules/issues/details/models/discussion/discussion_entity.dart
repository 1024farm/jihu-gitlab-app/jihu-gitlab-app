import 'package:jihu_gitlab_app/modules/issues/details/models/note/note_entity.dart';

class DiscussionEntity {
  static const tableName = "discussions";

  int? id;
  String discussionId;
  int issueId;
  int issueIid;
  bool individualNote;
  String pathWithNamespace;
  List<NoteEntity>? notes;
  int version;

  DiscussionEntity._internal(this.id, this.discussionId, this.issueId, this.issueIid, this.individualNote, this.pathWithNamespace, this.version, {this.notes});

  static DiscussionEntity restore(Map<String, dynamic> map) {
    return DiscussionEntity._internal(map['id'], map['discussion_id'], map['issue_id'], map['issue_iid'], map['individual_note'] == 0 ? false : true, map['path_with_namespace'], map['version'],
        notes: (map['notes'] as List<Map<String, dynamic>>).map((e) => NoteEntity.restore(e)).toList());
  }

  static DiscussionEntity create(Map<String, dynamic> map, {required String pathWithNamespace, required int issueId, required int issueIid, required int version}) {
    return DiscussionEntity._internal(null, map['id'] ?? '', issueId, issueIid, map['individual_note'] ?? false, pathWithNamespace, version,
        notes: ((map['notes'] ?? []) as List<dynamic>).map((e) => NoteEntity.create(e, map['id'] ?? '', version)).toList());
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'discussion_id': discussionId,
      'issue_id': issueId,
      'issue_iid': issueIid,
      'individual_note': individualNote,
      'path_with_namespace': pathWithNamespace,
      'version': version,
      'notes': notes?.map((e) => e.toMap()).toList()
    };
  }

  void update(DiscussionEntity newValue) {
    individualNote = newValue.individualNote;
    pathWithNamespace = newValue.pathWithNamespace;
    version = newValue.version;
    notes = newValue.notes;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is DiscussionEntity && runtimeType == other.runtimeType && discussionId == other.discussionId && issueId == other.issueId && issueIid == other.issueIid;

  @override
  int get hashCode => discussionId.hashCode ^ issueId.hashCode;

  static String createTableSql() {
    return """ 
    create table $tableName(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        discussion_id TEXT NOT NULL,
        issue_id INTEGER NOT NULL,
        issue_iid INTEGER NOT NULL,
        individual_note INTEGER ,
        path_with_namespace TEXT ,
        version INTEGER
    );
    """;
  }
}
