import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';

class DescriptionContent {
  String content;
  String pathWithNamespace;

  DescriptionContent._(this.content, this.pathWithNamespace);

  factory DescriptionContent.init(String content, String pathWithNamespace) {
    return DescriptionContent._(content, pathWithNamespace);
  }

  String get value {
    return content.replaceAll("](/uploads/", "](${ConnectionProvider.currentBaseUrl.get}/$pathWithNamespace/uploads/").replaceAll('<br>', '\n').replaceAll('<br/>', '\n');
  }
}
