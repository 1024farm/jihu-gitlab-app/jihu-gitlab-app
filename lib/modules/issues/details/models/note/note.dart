import 'package:jihu_gitlab_app/core/domain/assignee/assignee.dart';
import 'package:jihu_gitlab_app/core/time.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/description_content.dart';

class Note {
  int id;
  int noteableId;
  int noteableIid;
  Time createdAt;
  bool system;
  String body;
  Assignee author;
  bool internal;

  Note._(this.id, this.noteableId, this.noteableIid, this.createdAt, this.body, this.system, this.author, this.internal);

  factory Note.fromJson(Map<String, dynamic> json, String pathWithNamespace) {
    var description = DescriptionContent.init(json['body'], pathWithNamespace).value;
    return Note._(json['id'], json['noteable_id'], json['noteable_iid'], Time.init(json['created_at']), description, json['system'], Assignee.fromJson(json['author']),
        json['internal'] ?? json['confidential'] ?? false);
  }
}
