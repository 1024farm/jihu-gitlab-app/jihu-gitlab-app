import 'package:jihu_gitlab_app/core/db_manager.dart';
import 'package:jihu_gitlab_app/core/domain/assignee/assignee_entity.dart';
import 'package:jihu_gitlab_app/core/domain/assignee/assignee_repository.dart';
import 'package:jihu_gitlab_app/core/list_comparator.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/note/note_entity.dart';

class NoteRepository {
  static final NoteRepository _instance = NoteRepository._internal();

  NoteRepository._internal();

  factory NoteRepository.instance() => _instance;

  Future<List<NoteEntity>> queryByDiscussion(String discussionId, {List<int>? noteIds}) async {
    String sql = "select * from ${NoteEntity.tableName} where discussion_id = '$discussionId'";
    if (noteIds != null) {
      sql += " and note_id in (${noteIds.join(',')})";
    }
    var list = await DbManager.instance().rawFind(sql);
    List<Map<String, dynamic>> newList = [];
    for (var item in list) {
      var assigneeEntityList = await AssigneeRepository.instance().queryById(item['assignee_id'] as int);
      Map<String, dynamic> newItem = Map.from(item);
      if (assigneeEntityList.isNotEmpty) newItem['author'] = assigneeEntityList.first.toMap();
      newList.add(newItem);
    }
    return newList.map((e) => NoteEntity.restore(e)).toList();
  }

  Future<List<int>> insert(List<NoteEntity> entities, int version) async {
    _saveAssignees(entities.map((e) => e.assigneeEntity!).toList(), version);

    var notes = entities
        .map((e) => {
              'id': e.id,
              'note_id': e.noteId,
              'noteable_id': e.noteId,
              'noteable_iid': e.noteId,
              'discussion_id': e.discussionId,
              'system': e.system ? 1 : 0,
              'body': e.body,
              'assignee_id': e.assigneeId,
              'created_at': e.createdAt,
              'version': e.version,
              'internal': e.internal
            })
        .toList();
    // TODO: Cannot mock db.transaction, so add try catch for test
    try {
      return await DbManager.instance().batchInsert(NoteEntity.tableName, notes);
    } catch (e) {
      return [];
    }
  }

  Future<void> _saveAssignees(List<AssigneeEntity> entities, int version) async {
    List<int> assigneeIdList = entities.map((e) => e.assigneeId).toList();
    List<AssigneeEntity> exists = await AssigneeRepository.instance().queryByIds(assigneeIdList);
    ListCompareResult<AssigneeEntity> result = ListComparator.compare<AssigneeEntity>(exists, entities);
    if (result.hasAdd) {
      await AssigneeRepository.instance().insert(result.add);
    }
    if (result.hasUpdate) {
      var list = result.update.map((e) {
        e.left.update(e.right);
        return e.left;
      }).toList();
      await AssigneeRepository.instance().update(list);
    }
    await AssigneeRepository.instance().deleteLessThan(assigneeIdList, version);
  }

  Future<void> update(List<NoteEntity> entities, int version) async {
    var notes = entities
        .map((e) => {
              'id': e.id,
              'note_id': e.noteId,
              'noteable_id': e.noteId,
              'noteable_iid': e.noteId,
              'discussion_id': e.discussionId,
              'system': e.system ? 1 : 0,
              'body': e.body,
              'assignee_id': e.assigneeId,
              'created_at': e.createdAt,
              'version': e.version,
              'internal': e.internal
            })
        .toList();
    await DbManager.instance().batchUpdate(NoteEntity.tableName, notes);
    _saveAssignees(entities.map((e) => e.assigneeEntity!).toList(), version);
  }

  Future<int> deleteLessThan(String discussionId, int version) async {
    return await DbManager.instance().delete(NoteEntity.tableName, where: ' discussion_id = ? and version < ? ', whereArgs: ["'$discussionId'", version]);
  }
}
