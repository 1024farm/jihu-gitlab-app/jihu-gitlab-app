import 'package:jihu_gitlab_app/core/domain/assignee/assignee_entity.dart';

class NoteEntity {
  static const tableName = "notes";

  int? id;
  int noteId;
  int noteableId;
  int noteableIid;
  String discussionId;
  bool system;
  String body;
  int assigneeId;
  String createdAt;
  AssigneeEntity? assigneeEntity;
  int version;
  int? internal;

  NoteEntity._internal(this.id, this.noteId, this.noteableId, this.noteableIid, this.discussionId, this.system, this.body, this.assigneeId, this.createdAt, this.version, this.internal,
      {this.assigneeEntity});

  static NoteEntity restore(Map<String, dynamic> map) {
    return NoteEntity._internal(map['id'], map['note_id'], map['noteable_id'], map['noteable_iid'], map['discussion_id'], map['system'] == 0 ? false : true, map['body'], map['assignee_id'],
        map['created_at'], map['version'], map['internal'],
        assigneeEntity: map['author'] != null ? AssigneeEntity.restore(map['author']) : null);
  }

  static NoteEntity create(Map<String, dynamic> map, String discussionId, int version) {
    return NoteEntity._internal(null, map['id'], map['noteable_id'] ?? 0, map['noteable_iid'] ?? 0, discussionId, map['system'] ?? false, map['body'] ?? '', map['author']?['id'] ?? 0,
        map['created_at'] ?? '', version, (map['internal'] ?? false) ? 1 : 0,
        assigneeEntity: AssigneeEntity.create(map['author'] ?? {}, version));
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'note_id': noteId,
      'noteable_id': noteableId,
      'noteable_iid': noteableIid,
      'discussion_id': discussionId,
      'system': system ? 1 : 0,
      'body': body,
      'assignee_id': assigneeId,
      'created_at': createdAt,
      'version': version,
      'internal': internal,
      'author': assigneeEntity?.toMap()
    };
  }

  static String createTableSql() {
    return """ 
    create table $tableName(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        note_id INTEGER NOT NULL,
        noteable_id INTEGER NOT NULL,
        noteable_iid INTEGER NOT NULL,
        discussion_id TEXT NOT NULL,
        system INTEGER,
        body TEXT,
        assignee_id INTEGER NOT NULL,
        created_at TEXT ,
        version INTEGER
    );
    """;
  }

  static String get addInternalColumn => """ALTER TABLE $tableName ADD COLUMN internal INTEGER; """;

  void update(NoteEntity newValue) {
    system = newValue.system;
    body = newValue.body;
    version = newValue.version;
    assigneeId = newValue.assigneeId;
    assigneeEntity = newValue.assigneeEntity;
    internal = newValue.internal;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is NoteEntity && runtimeType == other.runtimeType && noteId == other.noteId && discussionId == other.discussionId && assigneeId == other.assigneeId;

  @override
  int get hashCode => noteId.hashCode ^ discussionId.hashCode ^ assigneeId.hashCode;
}
