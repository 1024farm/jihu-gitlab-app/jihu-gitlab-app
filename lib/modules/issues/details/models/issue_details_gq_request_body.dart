Map<String, dynamic> getIssueDetailsGraphQLRequestBody(String fullPath, int issueIid) {
  return {
    "query": """
       {
          project(fullPath: "$fullPath") {
            id
            name
            nameWithNamespace
            path
            fullPath
            issue(iid: "$issueIid") {
              id
              iid
              title
              description
              webUrl
              confidential
              state
              createdAt
              author {
                id
                avatarUrl
                name
                username
              }
              assignees {
                nodes {
                  id
                  avatarUrl
                  name
                  username
                }
              }
              labels {
                nodes {
                  id
                  title
                  description
                  color
                  textColor
                }
              }
              milestone{
                id
                title
              }
            }
          }
       }
       """
  };
}

Map<String, dynamic> getIssuePostVotesGraphQLRequestBody(String fullPath, int postIid) {
  return {
    "query": """
           {
              project(fullPath: "$fullPath") {
                issue(iid: "$postIid") {
                  downvotes
                  upvotes
                }
              }
           }
       """
  };
}
