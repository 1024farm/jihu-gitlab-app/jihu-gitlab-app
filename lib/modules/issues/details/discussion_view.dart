import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar_and_name.dart';
import 'package:jihu_gitlab_app/core/widgets/changeable_time.dart';
import 'package:jihu_gitlab_app/modules/community/models/community_project.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_discussion_content_view.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/note/note.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

class DiscussionView extends StatefulWidget {
  final int projectId;
  final Discussion discussion;
  final VoidCallback? onTap;
  final AutoScrollController? autoScrollController;

  const DiscussionView({required this.projectId, required this.discussion, this.onTap, this.autoScrollController, Key? key}) : super(key: key);

  @override
  State<DiscussionView> createState() => _DiscussionViewState();
}

class _DiscussionViewState extends State<DiscussionView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Color(widget.discussion.discussionNote.internal ? 0xFFFFFBEF : 0xFFF8F8FA), borderRadius: const BorderRadius.all(Radius.circular(4))),
      padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 12),
      child: Column(
        children: <Widget>[
          _buildItemView(widget.discussion.discussionNote, true && !CommunityProject.instance().inMaintenance && !ProjectProvider().archived),
          widget.discussion.hasNotes
              ? Container(
                  margin: const EdgeInsets.only(top: 16),
                  padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 12),
                  decoration: BoxDecoration(border: Border.all(color: const Color(0xFFEAEAEA)), borderRadius: const BorderRadius.all(Radius.circular(4))),
                  child: _buildDiscussionNotesView(widget.discussion.replyNotes))
              : Container(height: 0),
        ],
      ),
    );
  }

  Widget _buildDiscussionNotesView(List<Note> replyNotes) {
    final colorScheme = Theme.of(context).colorScheme;
    List<Widget> widgets = [];
    for (int i = 0; i < replyNotes.length; i++) {
      widgets.add(AutoScrollTag(
          key: ValueKey(replyNotes[i].noteableIid),
          controller: widget.autoScrollController!,
          index: replyNotes[i].noteableIid,
          highlightColor: colorScheme.primary.withOpacity(0.1),
          child: Container(
              padding: const EdgeInsets.symmetric(vertical: 10),
              decoration: BoxDecoration(border: Border(top: BorderSide(color: i == 0 ? Colors.transparent : const Color(0xFFECE9DE)))),
              child: _buildItemView(replyNotes[i], false))));
    }
    return Column(children: widgets);
  }

  Widget _buildItemView(Note note, bool canReply) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              flex: 1,
              child: Row(
                children: [
                  AvatarAndName(
                      username: note.author.name,
                      avatarUrl: note.author.avatarUrl,
                      avatarSize: 24,
                      gap: 8,
                      nameStyle: const TextStyle(color: Color(0xFF1A1B36), fontWeight: FontWeight.w400, fontSize: 13)),
                  ChangeableTime.show(() => setState(() {}), note.createdAt, const Color(0xFF95979A)),
                ],
              ),
            ),
            note.internal ? SvgPicture.asset("assets/images/visibility_off_note.svg", height: 16, width: 16) : const SizedBox(),
            canReply
                ? Padding(
                    padding: const EdgeInsets.only(left: 12.0),
                    child: InkWell(onTap: widget.onTap, child: SvgPicture.asset("assets/images/reply.svg", height: 16, width: 16)),
                  )
                : const SizedBox(),
          ],
        ),
        IssueDiscussionContentView(
          context: context,
          padding: const EdgeInsets.only(top: 8),
          data: note.body,
          projectId: widget.projectId,
        )
      ],
    );
  }
}
