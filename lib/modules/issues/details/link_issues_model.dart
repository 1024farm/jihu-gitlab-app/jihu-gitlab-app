import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/domain/issue.dart';
import 'package:jihu_gitlab_app/core/license_type_detector.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';
import 'package:jihu_gitlab_app/core/project_identity.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_link.dart';

class LinkIssuesModel {
  IssueLinkType _linkType = IssueLinkType.relatesTo;
  Issue? _selectedIssue;
  late int issueIid;
  late ValueNotifier<bool> _notifier;
  bool? _canSwitchLinkType;
  late final ProjectIdentity _projectIdentity;

  LinkIssuesModel(this._projectIdentity, this.issueIid) {
    _notifier = ValueNotifier(false);
  }

  Future<bool> canSwitchLinkType() async {
    if (_canSwitchLinkType == null) {
      _notifier.value = true;
      _canSwitchLinkType = !(await LicenseTypeDetector.instance().isFreeProject(_projectIdentity.fullPath));
      _notifier.value = false;
    }
    return Future(() => _canSwitchLinkType!);
  }

  Future<void> createLinks() async {
    if (_selectedIssue == null) {
      return;
    }
    try {
      await ProjectRequestSender.instance().post(
          Api.join("/projects/${_projectIdentity.id}/issues/$issueIid/links?target_project_id=${_selectedIssue!.project.id}&target_issue_iid=${_selectedIssue!.iid}&link_type=${_linkType.value}"), {});
    } catch (e) {
      var message = AppLocalizations.dictionary().failed;
      if (e.runtimeType == DioError) {
        var error = e as DioError;
        message = error.response?.data['message'] ?? error.message;
      }
      throw Exception(message);
    }
  }

  void changeLinkType(IssueLinkType linkType) => _linkType = linkType;

  void changeSelectedIssue(Issue issue) => _selectedIssue = issue;

  IssueLinkType get linkType => _linkType;

  ValueNotifier<bool> get notifier => _notifier;

  bool get canCreateLinks => _selectedIssue != null;
}
