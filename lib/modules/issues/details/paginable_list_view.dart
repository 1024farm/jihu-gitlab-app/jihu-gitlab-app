import 'dart:async';

import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/graph_ql_page_info.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/paginated_graphql_response.dart';
import 'package:jihu_gitlab_app/core/widgets/app_footer.dart';
import 'package:jihu_gitlab_app/core/widgets/app_header.dart';
import 'package:jihu_gitlab_app/core/widgets/multi_state_view.dart';
import 'package:jihu_gitlab_app/core/widgets/permission_low_view.dart';
import 'package:jihu_gitlab_app/core/widgets/search_box.dart';

typedef OnRefresh<T> = Future<PaginatedGraphQLResponse<T>> Function(String? keyword);

typedef OnLoadMore<T> = Future<PaginatedGraphQLResponse<T>> Function(String endCursor, String? keyword);

class PaginableListViewModel<T> {
  List<T> _data = [];
  late ValueNotifier<List<T>> notifier;
  late ValueNotifier<LoadState> loadStateNotifier;
  LoadState _loadState = LoadState.loadingState;
  GraphQLPageInfo _pageInfo = GraphQLPageInfo.init();
  final EasyRefreshController easyRefreshController = EasyRefreshController(controlFinishRefresh: true, controlFinishLoad: true);
  TextEditingController? searchController;
  late bool searchable;
  final OnRefresh<T> onRefresh;
  final OnLoadMore<T> onLoadMore;
  String? _keyword;
  String? featureName;

  PaginableListViewModel({required this.onRefresh, required this.onLoadMore, this.searchable = false, this.featureName}) {
    notifier = ValueNotifier(_data);
    loadStateNotifier = ValueNotifier(_loadState);
    if (this.searchable) {
      searchController = TextEditingController();
    }
  }

  Future<void> refresh() async {
    try {
      var response = await onRefresh(_keyword);
      _data = response.list;
      _pageInfo = response.pageInfo;
      _loadState = _data.isNotEmpty ? LoadState.successState : LoadState.noItemState;
      notifier.value = List.of(_data);
      easyRefreshController.finishRefresh();
      easyRefreshController.resetFooter();
    } catch (e) {
      _loadState = LoadState.errorState;
    } finally {
      loadStateNotifier.value = _loadState;
    }
  }

  Future<void> loadMore() async {
    try {
      var response = await onLoadMore(_pageInfo.endCursor, _keyword);
      _data.addAll(response.list);
      _pageInfo = response.pageInfo;
      notifier.value = List.of(_data);
      easyRefreshController.finishLoad(_pageInfo.hasNextPage ? IndicatorResult.success : IndicatorResult.noMore);
    } catch (e) {
      easyRefreshController.finishLoad(IndicatorResult.fail);
      LogHelper.err('Failed to load more data[${T.runtimeType.toString()}]', e);
    }
  }

  Future<void> onSearch(String? keyword) {
    _keyword = keyword;
    return refresh();
  }

  Future<void> onClearSearch() {
    _keyword = null;
    return refresh();
  }

  void dispose() {
    easyRefreshController.dispose();
    searchController?.dispose();
  }
}

typedef PaginableListViewItemBuilder<T> = Widget Function(BuildContext context, int index, List<T> data);

class PaginableListView<T> extends StatefulWidget {
  const PaginableListView({
    required this.model,
    required this.itemBuilder,
    this.separatorBuilder,
    this.listBoxDecoration,
    super.key,
  });

  final PaginableListViewItemBuilder<T> itemBuilder;
  final IndexedWidgetBuilder? separatorBuilder;
  final BoxDecoration? listBoxDecoration;
  final PaginableListViewModel<T> model;

  @override
  State<PaginableListView<T>> createState() => _PaginableListViewState();
}

class _PaginableListViewState<T> extends State<PaginableListView<T>> {
  late PaginableListViewModel<T> _model;

  @override
  void initState() {
    _model = widget.model;
    _model.refresh();
    super.initState();
  }

  @override
  void dispose() {
    _model.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (_model.searchable)
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: SearchBox(searchController: _model.searchController!, onSubmitted: (keyword) => _model.onSearch(keyword), onClear: _model.onClearSearch),
          ),
        ValueListenableBuilder<LoadState>(
          valueListenable: _model.loadStateNotifier,
          builder: (context, loadState, child) => MultiStateView(
              loadState: loadState,
              unavailableView: PermissionLowView(featureName: _model.featureName ?? ""),
              onRetry: _model.refresh,
              child: Expanded(
                child: EasyRefresh(
                  controller: _model.easyRefreshController,
                  onRefresh: _model.refresh,
                  onLoad: _model.loadMore,
                  header: AppHeader(),
                  footer: AppFooter(),
                  child: ValueListenableBuilder<List<T>>(
                    valueListenable: _model.notifier,
                    builder: (context, data, child) {
                      return Container(
                        decoration: widget.listBoxDecoration,
                        child: ListView.separated(
                          itemBuilder: (context, index) => widget.itemBuilder(context, index, data),
                          separatorBuilder: (context, index) => widget.separatorBuilder?.call(context, index) ?? const SizedBox(),
                          itemCount: data.length,
                        ),
                      );
                    },
                  ),
                ),
              )),
        ),
      ],
    );
  }
}
