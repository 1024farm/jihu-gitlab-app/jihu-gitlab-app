import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/asset_svg.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/domain/assignee/assignee.dart';
import 'package:jihu_gitlab_app/core/domain/avatar_url.dart';
import 'package:jihu_gitlab_app/core/domain/issue.dart';
import 'package:jihu_gitlab_app/core/id.dart';
import 'package:jihu_gitlab_app/core/layout/adaptive.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/project_identity.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar_and_name.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/connect_server_tips.dart';
import 'package:jihu_gitlab_app/core/widgets/delimited_column.dart';
import 'package:jihu_gitlab_app/core/widgets/issue_state_and_creation_info_view.dart';
import 'package:jihu_gitlab_app/core/widgets/label_color_and_name.dart';
import 'package:jihu_gitlab_app/core/widgets/label_view.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_button.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_view.dart';
import 'package:jihu_gitlab_app/core/widgets/markdown/markdown_input_box.dart';
import 'package:jihu_gitlab_app/core/widgets/project_archived_notice_view.dart';
import 'package:jihu_gitlab_app/core/widgets/selector/selector.dart' as selector;
import 'package:jihu_gitlab_app/core/widgets/streaming_container.dart';
import 'package:jihu_gitlab_app/core/widgets/svg_button.dart';
import 'package:jihu_gitlab_app/core/widgets/toast.dart';
import 'package:jihu_gitlab_app/core/widgets/votes/issue_votes_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/details/discussion_view.dart';
import 'package:jihu_gitlab_app/modules/issues/details/issue_details_param.dart';
import 'package:jihu_gitlab_app/modules/issues/details/link_issues_page.dart';
import 'package:jihu_gitlab_app/modules/issues/details/milestone_selector_page.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_description_view.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_details_model.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_info.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_link.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label_provider.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issue_item_view.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/issue_update_page.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/models/assignee_selector_model.dart';
import 'package:jihu_gitlab_app/modules/issues/member/member.dart';
import 'package:jihu_gitlab_app/modules/issues/member/member_provider.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/milestone_and_iteration_state_view.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/project_milestones_fetcher.dart';
import 'package:provider/provider.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:jihu_gitlab_app/core/widgets/issue_share_view.dart';

part 'widgets.dart';

typedef EditLabelsCallback = void Function(List<Label> selectedLabels);
typedef OnReplayTappedCallBack = void Function(Discussion comment);

class IssueDetailsPage extends StatefulWidget {
  static const String routeName = 'IssueDetailsPage';
  final Map arguments;

  const IssueDetailsPage({required this.arguments, super.key});

  @override
  State<StatefulWidget> createState() => _IssueDetailsPageState();
}

class _IssueDetailsPageState extends State<IssueDetailsPage> {
  final TextEditingController _commentController = TextEditingController();
  final IssueDetailsModel _model = locator<IssueDetailsModel>();
  final AssigneeSelectorModel _assigneeSelectorModel = AssigneeSelectorModel();

  FocusNode commentFocusNode = FocusNode();
  String placeholder = AppLocalizations.dictionary().commentPlaceholder;
  bool _submittable = false;
  bool _inCommentSubmitting = false;
  bool _inPageInitializing = false;
  bool _showLeading = false;
  final double _bottomActionBoxHeight = 45;

  late IssueDetailsParam _param;
  int? _targetNodeId;
  late AutoScrollController _controller;

  @override
  void initState() {
    _pageInit();
    super.initState();
  }

  void _pageInit() {
    if (_inPageInitializing) return;
    _inPageInitializing = true;
    _model.loadingPage();
    _param = IssueDetailsParam.fromJson(widget.arguments);
    if (widget.arguments.containsKey('showLeading')) {
      _showLeading = widget.arguments['showLeading'];
    }
    if (widget.arguments.containsKey('targetUrl') && widget.arguments['targetUrl'].contains('#note_')) {
      String url = widget.arguments['targetUrl'];
      int index = url.lastIndexOf('_');
      _targetNodeId = int.parse(url.substring(index + 1));
    }
    _controller = AutoScrollController(viewportBoundaryGetter: () => Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom), axis: Axis.vertical);
    if (_param.projectId != 0) {
      _model.config(_param);
      _model.getIssueInfo().then((value) {
        setState(() {});
        if (_model.loadState == LoadState.successState) {
          _loadComments();
        }
      });
      _assigneeSelectorModel.init(0, _param.projectId);
    }
    _inPageInitializing = false;
  }

  @override
  void dispose() {
    _commentController.dispose();
    _controller.dispose();
    _model.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (IssueDetailsParam.fromJson(widget.arguments) != _param) _pageInit();
    return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          placeholder = AppLocalizations.dictionary().writePlaceholder;
          _model.selectedNoteId = 0;
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Consumer<ConnectionProvider>(builder: (context, _, child) {
          return Consumer<ProjectProvider>(builder: (context, _, child) {
            return Scaffold(appBar: _buildAppBar(), backgroundColor: Colors.white, body: _buildBody());
          });
        }));
  }

  Widget _buildBody() {
    if (_param.projectId == 0) {
      return Center(child: Text(AppLocalizations.dictionary().noItemSelected(AppLocalizations.dictionary().issue.toLowerCase()), style: const TextStyle(color: Colors.black54)));
    }
    if (_model.loadState == LoadState.loadingState) return const LoadingView();
    if (_model.loadState == LoadState.noItemState || _model.loadState == LoadState.errorState) return _build404View();
    return SafeArea(
      top: !isDesktopLayout(context),
      bottom: true,
      child: Stack(children: [
        Container(
            padding: ProjectProvider().isSpecifiedHostHasConnection && !ProjectProvider().archived ? EdgeInsets.only(bottom: _bottomActionBoxHeight) : EdgeInsets.zero,
            child: SingleChildScrollView(controller: _controller, physics: const ScrollPhysics(), child: _buildIssueDetail())),
        if (ProjectProvider().isSpecifiedHostHasConnection && !ProjectProvider().archived) _buildIssueReplyMarkdown(),
        if (ProjectProvider().archived) const Align(alignment: Alignment.bottomCenter, child: ProjectArchivedNoticeView()),
      ]),
    );
  }

  void _loadComments() async {
    await _model.loadDiscussions();
    Future.delayed(const Duration(seconds: 1)).then((value) {
      _scrollToTarget();
    });
  }

  Future<void> _scrollToTarget() async {
    if (_targetNodeId == null) return;
    await _controller.scrollToIndex(_targetNodeId!, duration: const Duration(seconds: 1), preferPosition: AutoScrollPosition.begin);
    _controller.highlight(_targetNodeId!);
  }

  void _saveCommentAndRefresh(StateSetter setBottomState) async {
    var commentContent = _commentController.text;
    if (commentContent.isEmpty) {
      return;
    }
    _toggleSubmitting(true, setBottomState);
    try {
      await _model.saveComment(commentContent).then((value) {
        Toast.success(context, AppLocalizations.dictionary().successful);
        Navigator.pop(context);
        _model.resetForReplyToComment();
      }).catchError((e) {
        Toast.error(context, AppLocalizations.dictionary().failed);
        Navigator.pop(context);
      });
      _loadComments();
    } finally {
      _toggleSubmitting(false, setBottomState);
    }
    _commentController.clear();
    commentFocusNode.unfocus();
    _updateButtonState("", setBottomState);
  }

  void _updateButtonState(String text, StateSetter setBottomState) {
    setBottomState(() {
      _submittable = text.isNotEmpty;
    });
  }

  void _updateState() async {
    await _model.updateState();
    setState(() {});
  }

  void _updateConfidential() async {
    await _model.updateConfidential();
    setState(() {});
  }

  void _toggleSubmitting(bool on, StateSetter setBottomState) {
    setBottomState(() {
      _inCommentSubmitting = on;
    });
  }

  CommonAppBar _buildAppBar() {
    Text? title;
    if (_model.issueInfo != null) {
      title = Text('${_model.issueInfo!.projectName}  #${_model.issueIid}');
    }
    return CommonAppBar(
      title: title,
      showLeading: _showLeading,
      leading: BackButton(color: Colors.black, onPressed: () => Navigator.pop(context, _model.issueInfo)),
      automaticallyImplyLeading: false,
      backgroundColor: Colors.white,
      actions: [
        if (_model.issueInfo != null)
          PopupMenuButton<String>(
            padding: EdgeInsets.zero,
            position: PopupMenuPosition.under,
            itemBuilder: (context) => [
              PopupMenuItem<String>(
                  padding: EdgeInsets.zero,
                  child: IssueShareView(
                      title: _model.issueInfo!.title,
                      link: _model.issueInfo!.webUrl,
                      issueInfo: _model.issueInfo,
                      onCloseOrReopenIssue: _updateIssueState,
                      onEditIssue: _toEditIssuePage,
                      onUpdateIssueConfidential: _updateIssueConfidential))
            ],
            child: Container(
              margin: const EdgeInsets.only(right: 20),
              child: SvgPicture.asset("assets/images/operate.svg", semanticsLabel: "operate", colorFilter: const ColorFilter.mode(AppThemeData.secondaryColor, BlendMode.srcIn), width: 18, height: 18),
            ),
          ),
      ],
    );
  }

  void _updateIssueState() async {
    _updateState();
    Navigator.pop(context);
    _model.resetForReplyToComment();
  }

  void _updateIssueConfidential() async {
    _updateConfidential();
    setState(() {});
    Navigator.pop(context);
  }

  void _toEditIssuePage() async {
    Navigator.of(context)
        .push(
          MaterialPageRoute(builder: (context) => IssueUpdatePage(projectId: widget.arguments['projectId'], issueIid: widget.arguments['issueIid'], info: _model.issueInfo!)),
        )
        .then((value) => _model.getIssueInfo().then((value) => setState(() {})));
  }

  Widget _buildIssueDetail() {
    var issueInfo = _model.issueInfo!;
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(issueInfo.title, style: const TextStyle(fontSize: 18, color: Color(0xFF1A1B36), fontWeight: FontWeight.w600)),
          _buildGap(),
          _buildStatusAndCreationInfo(issueInfo, () => setState(() {})),
          _buildGap(),
          _buildDescription(context, _model),
          _buildAssignees(context, issueInfo, (selectedAssignees) async {
            var multiSelection = await _assigneeSelectorModel.isMultiAssignees();
            var warningNotice = await _assigneeSelectorModel.warningNoticeWhenNoLicense();
            var result = await _showSelector(multiSelection, warningNotice, widget.arguments['projectId'], selectedAssignees.map((e) => e.id).toList());
            if (result != null) {
              List<Member> selectedAssignees = result;
              bool updateAssigneesResult = await _model.updateAssignees(selectedAssignees.map((e) => e.id).toList());
              if (updateAssigneesResult) {
                setState(() {});
                return;
              }
              setState(() {
                Toast.error(context, AppLocalizations.dictionary().failed);
              });
            }
          }),
          _buildLabels(context, issueInfo, (selectedLabels) async {
            var result = await _showLabelsSelector(_model.projectId, selectedLabels);
            if (result != null) {
              bool updateLabelsResult = await _model.updateLabels(result.map((e) => e.name).toList());
              if (updateLabelsResult) {
                setState(() {});
                return;
              }
              setState(() {
                Toast.error(context, AppLocalizations.dictionary().failed);
              });
            }
          }),
          _IssueMilestoneView(fullPath: _model.fullPath, milestone: issueInfo.milestone, onMilestoneSelected: (milestoneId) async => _onMilestoneSelected(issueInfo.milestone?.id, milestoneId)),
          ValueListenableBuilder<Map<IssueLinkType, List<IssueLink>>>(
              valueListenable: _model.linksNotifier,
              builder: (context, data, child) => IssueLinksView(
                    issueIid: _model.issueIid,
                    projectIdentity: _model.projectIdentity,
                    groupedIssueLinks: data,
                    onItemLinked: () => _model.getLinks(),
                  )),
          _buildGap(gap: 14),
          _buildComments(context, _model, _controller, (comment) {
            setState(() {
              placeholder = "${AppLocalizations.dictionary().reply} ${comment.notes[0].author.name}:";
              _model.selectedNoteId = comment.notes[0].id;
              _model.selectedDiscussionId = comment.id;
              renderSheet();
            });
          })
        ],
      ),
    );
  }

  Future<List<Label>?> _showLabelsSelector(int projectId, List<Label> selectedLabels) async {
    var result = await Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return selector.Selector<int, Label>(
          dataProvider: LabelProvider(projectId: projectId),
          itemBuilder: (BuildContext context, int index, Label label) => LabelColorAndName(color: label.formattedColor, name: label.name),
          filter: (keyword, e) => e.name.toUpperCase().contains(keyword.toUpperCase()),
          title: AppLocalizations.dictionary().selectLabels,
          multiSelection: true,
          keyMapper: (label) => label.id.id,
          projectId: _model.projectId,
          selectedKeysProvider: () => selectedLabels.map((t) => t.id.id).toList());
    }));
    return Future(() => result == null ? null : result as List<Label>);
  }

  Future<List<Member>?> _showSelector(bool multiSelection, String warningNotice, int projectId, List<int> ids) async {
    var result = await Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return selector.Selector<int, Member>(
          dataProvider: MemberProvider(projectId: projectId),
          itemBuilder: (BuildContext context, int index, Member member) => AvatarAndName(username: member.username, avatarUrl: member.avatarUrl),
          filter: (keyword, e) => e.username.toUpperCase().contains(keyword.toUpperCase()),
          title: AppLocalizations.dictionary().selectAssignees,
          multiSelection: multiSelection,
          warningNotice: warningNotice,
          keyMapper: (member) => member.id,
          selectedKeysProvider: () => ids);
    }));
    return Future(() => result == null ? null : result as List<Member>);
  }

  Future<void> _onMilestoneSelected(String? oldMilestoneId, String newMilestoneId) async {
    if (oldMilestoneId == newMilestoneId) {
      return;
    }
    int id = newMilestoneId.isNotEmpty ? Id.fromGid(newMilestoneId).id : 0;
    bool result = await _model.updateMilestone(id);
    setState(() {
      if (!result) {
        Toast.error(context, AppLocalizations.dictionary().failed);
      }
    });
  }

  Widget _buildIssueReplyMarkdown() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        width: double.infinity,
        height: _bottomActionBoxHeight,
        padding: const EdgeInsets.symmetric(horizontal: 16),
        decoration: const BoxDecoration(color: Colors.white, border: Border(top: BorderSide(width: 0.2, color: Color(0xFFCECECE)))),
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, crossAxisAlignment: CrossAxisAlignment.center, children: [
          Expanded(
            child: InkWell(
              onTap: () {
                setState(() {
                  placeholder = AppLocalizations.dictionary().commentPlaceholder;
                  _model.resetForReplyToComment();
                });
                renderSheet();
              },
              child: Row(
                children: [
                  SvgPicture.asset('assets/images/comment.svg', width: 18, height: 18),
                  Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 12),
                      child: Text(AppLocalizations.dictionary().commentPlaceholder,
                          textAlign: TextAlign.center, style: const TextStyle(color: Color(0xFF03162F), fontSize: 12, fontWeight: FontWeight.w400)))
                ],
              ),
            ),
          ),
        ]),
      ),
    );
  }

  void renderSheet() {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context1, setBottomState) {
              return GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                },
                child: SafeArea(
                  child: AnimatedPadding(
                    padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
                    duration: const Duration(milliseconds: 100),
                    child: SingleChildScrollView(
                      child: Container(
                          padding: const EdgeInsets.all(12),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              MarkdownInputBox(
                                controller: _commentController,
                                projectId: widget.arguments['projectId'],
                                onChanged: () {
                                  _updateButtonState(_commentController.text, setBottomState);
                                },
                                placeholder: placeholder,
                                scrollable: true,
                              ),
                              const SizedBox(
                                height: 12,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    flex: 1,
                                    child: Offstage(
                                      offstage: !_model.shouldShowInternalSwitch,
                                      child: Row(
                                        children: [
                                          Transform.scale(
                                              scale: 0.7,
                                              child: CupertinoSwitch(
                                                  activeColor: Theme.of(context).primaryColor, value: _model.isInternal, onChanged: (value) => setBottomState(() => _model.isInternal = value))),
                                          Text(AppLocalizations.dictionary().internal, style: const TextStyle(color: AppThemeData.secondaryColor, fontSize: 13, fontWeight: FontWeight.w400)),
                                        ],
                                      ),
                                    ),
                                  ),
                                  LoadingButton.asElevatedButton(
                                    key: const Key('comment-reply-inkwell'),
                                    isLoading: _inCommentSubmitting,
                                    disabled: !_submittable,
                                    text: Text(AppLocalizations.dictionary().comment, style: const TextStyle(color: Colors.white, fontSize: 12)),
                                    onPressed: () {
                                      _saveCommentAndRefresh(setBottomState);
                                    },
                                  ),
                                ],
                              ),
                            ],
                          )),
                    ),
                  ),
                ),
              );
            },
          );
        });
  }
}

class IssueLinksView extends StatelessWidget {
  const IssueLinksView({required this.issueIid, required this.projectIdentity, required this.groupedIssueLinks, required this.onItemLinked, Key? key}) : super(key: key);
  final int issueIid;
  final ProjectIdentity projectIdentity;
  final Map<IssueLinkType, List<IssueLink>> groupedIssueLinks;
  final VoidCallback onItemLinked;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _TitleLineView(
          title: AppLocalizations.dictionary().linkedItems,
          actionSvg: AssetSvg("assets/images/plus.svg", color: AppThemeData.secondaryColor),
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => LinkIssuesPage(issueIid: issueIid, projectIdentity: projectIdentity))).then((linked) {
              if (linked != null && linked) {
                onItemLinked();
              }
            });
          },
        ),
        groupedIssueLinks.isEmpty
            ? const _NoneView()
            : Container(
                padding: const EdgeInsets.all(12),
                decoration: const BoxDecoration(color: Color(0xFFF8F8FA), borderRadius: BorderRadius.all(Radius.circular(4))),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    LinkedIssuesView(linkType: IssueLinkType.blocks, issueLinks: groupedIssueLinks[IssueLinkType.blocks] ?? [], bottom: 12),
                    LinkedIssuesView(linkType: IssueLinkType.isBlockedBy, issueLinks: groupedIssueLinks[IssueLinkType.isBlockedBy] ?? [], bottom: 12),
                    LinkedIssuesView(linkType: IssueLinkType.relatesTo, issueLinks: groupedIssueLinks[IssueLinkType.relatesTo] ?? [], bottom: 0),
                  ],
                ),
              )
      ],
    );
  }
}

class LinkedIssuesView extends StatelessWidget {
  const LinkedIssuesView({required this.linkType, required this.issueLinks, required this.bottom, Key? key}) : super(key: key);
  final IssueLinkType linkType;
  final List<IssueLink> issueLinks;
  final double bottom;

  @override
  Widget build(BuildContext context) {
    return issueLinks.isEmpty
        ? const SizedBox()
        : Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(linkType.label, style: const TextStyle(color: Color(0xFF1A1B36), fontSize: 13, fontWeight: FontWeight.w500)),
              const SizedBox(height: 8),
              DelimitedColumn(
                  divider: const Divider(height: 8, thickness: 8, color: Colors.transparent),
                  children: issueLinks.map((e) => InkWell(onTap: () => _onTapped(context, e.issue), child: IssueItemView(issue: e.issue, displayProject: false))).toList()),
              SizedBox(height: bottom),
            ],
          );
  }

  void _onTapped(BuildContext context, Issue issue) {
    var params = issue.asDetailParams;
    params["showLeading"] = true;
    Navigator.pushNamed(context, IssueDetailsPage.routeName, arguments: params);
  }
}
