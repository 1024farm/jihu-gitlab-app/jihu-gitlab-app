import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/domain/issue.dart';
import 'package:jihu_gitlab_app/core/project_identity.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/feature_not_supported_hint_dialog.dart';
import 'package:jihu_gitlab_app/core/widgets/round_rect_container.dart';
import 'package:jihu_gitlab_app/core/widgets/toast.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/details/link_issues_model.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_link.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_page.dart';

class LinkIssuesPage extends StatefulWidget {
  const LinkIssuesPage({required this.issueIid, required this.projectIdentity, Key? key}) : super(key: key);
  final ProjectIdentity projectIdentity;
  final int issueIid;

  @override
  State<LinkIssuesPage> createState() => _LinkIssuesPageState();
}

class _LinkIssuesPageState extends State<LinkIssuesPage> {
  late LinkIssuesModel _model;

  @override
  void initState() {
    _model = LinkIssuesModel(widget.projectIdentity, widget.issueIid);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CommonAppBar(
        showLeading: true,
        title: Text(AppLocalizations.dictionary().linkedItems),
        actions: [
          Container(
              transform: Matrix4.translationValues(-12, 2, 0),
              child: TextButton(
                  onPressed: _model.canCreateLinks ? () => _model.createLinks().then((value) => Navigator.of(context).pop(true)).catchError((error) => Toast.error(context, error.message)) : null,
                  child: Opacity(opacity: _model.canCreateLinks ? 1 : 0.5, child: Text(AppLocalizations.dictionary().done, style: TextStyle(color: Theme.of(context).primaryColor)))))
        ],
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(AppLocalizations.dictionary().theCurrentIssue, style: const TextStyle(color: Color(0xFF171321), fontSize: 16, fontWeight: FontWeight.w600)),
                    ValueListenableBuilder(
                        valueListenable: _model.notifier,
                        builder: (context, loading, child) {
                          return loading ? Container(margin: const EdgeInsets.only(left: 8), width: 16, height: 16, child: const CircularProgressIndicator(strokeWidth: 2)) : const SizedBox();
                        })
                  ],
                ),
                const SizedBox(height: 12),
                _IssueLinkTypeSelectionView(onLinkTypeChanged: (type) => _model.changeLinkType(type), canSwitchLinkTypeFetcher: _model.canSwitchLinkType),
                const SizedBox(height: 16),
                Text(AppLocalizations.dictionary().theFollowingIssue, style: const TextStyle(color: Color(0xFF171321), fontSize: 16, fontWeight: FontWeight.w600)),
              ],
            ),
          ),
          const SizedBox(height: 8),
          Expanded(
            child: IssuesPage(
              relativePath: widget.projectIdentity.fullPath,
              isProject: true,
              selectable: true,
              projectId: widget.projectIdentity.id,
              onSelectedIssueChange: (index, params) {
                setState(() => _model.changeSelectedIssue(params['issue'] as Issue));
              },
            ),
          ),
        ],
      ),
    );
  }
}

typedef IssueLinkTypChangeListener = void Function(IssueLinkType issueLinkType);
typedef CanSwitchLinkTypeFetcher = Future<bool> Function();

class _IssueLinkTypeSelectionView extends StatefulWidget {
  const _IssueLinkTypeSelectionView({required this.onLinkTypeChanged, required this.canSwitchLinkTypeFetcher, Key? key}) : super(key: key);
  final IssueLinkTypChangeListener onLinkTypeChanged;
  final CanSwitchLinkTypeFetcher canSwitchLinkTypeFetcher;

  @override
  State<_IssueLinkTypeSelectionView> createState() => _IssueLinkTypeSelectionViewState();
}

class _IssueLinkTypeSelectionViewState extends State<_IssueLinkTypeSelectionView> {
  IssueLinkType _selected = IssueLinkType.relatesTo;

  @override
  Widget build(BuildContext context) {
    return RoundRectContainer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildOption(IssueLinkType.relatesTo),
          _buildOption(IssueLinkType.blocks),
          _buildOption(IssueLinkType.isBlockedBy),
        ],
      ),
    );
  }

  Widget _buildOption(IssueLinkType issueLinkType) {
    return InkWell(
      onTap: () => _onOptionTap(issueLinkType),
      child: SizedBox(
        width: double.infinity,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Checkbox(
              key: Key(issueLinkType.name),
              shape: const CircleBorder(),
              value: _selected == issueLinkType,
              checkColor: Colors.white,
              fillColor: MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
                return states.contains(MaterialState.selected) ? AppThemeData.primaryColor : const Color(0xFFCECECE);
              }),
              materialTapTargetSize: MaterialTapTargetSize.padded,
              onChanged: null,
            ),
            Text(issueLinkType.label, style: const TextStyle(color: Color(0xFF1A1B36), fontSize: 14, fontWeight: FontWeight.w500))
          ],
        ),
      ),
    );
  }

  void _onOptionTap(IssueLinkType issueLinkType) async {
    if (_selected == issueLinkType) {
      return;
    }
    var canSwitchLinkType = await widget.canSwitchLinkTypeFetcher();
    if (!canSwitchLinkType) {
      if (context.mounted) FeatureNotSupportedHintDialog.show(context, AppLocalizations.dictionary().blockingIssues);
      return;
    }
    setState(() => _selected = issueLinkType);
    widget.onLinkTypeChanged(_selected);
  }
}
