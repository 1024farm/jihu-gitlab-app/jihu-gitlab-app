part of 'issue_details_page.dart';

Widget _build404View() {
  return Align(
    alignment: const FractionalOffset(0.5, 0.2),
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        SvgPicture.asset("assets/images/not_found.svg", width: 60, height: 60),
        const SizedBox(height: 8),
        Text(AppLocalizations.dictionary().pageNotFound, style: const TextStyle(color: Color(0xFF171321), fontSize: 14, fontWeight: FontWeight.w400)),
        const SizedBox(height: 8),
        SizedBox(
          width: 230,
          child: Text(AppLocalizations.dictionary().pageNotFoundHint, style: const TextStyle(color: Color(0xFF171321), fontSize: 14, fontWeight: FontWeight.w400), textAlign: TextAlign.center),
        )
      ],
    ),
  );
}

Widget _buildStatusAndCreationInfo(IssueInfo issueInfo, VoidCallback changeTimeCallBack) {
  return IssueStateAndCreationInfoView(
    state: issueInfo.state,
    confidential: issueInfo.confidential,
    authorName: issueInfo.author.name,
    authorAvatarUrl: issueInfo.author.avatarUrl,
    recentActivities: issueInfo.recentActivities(),
    recentActivityTimes: issueInfo.recentActivityTimes(),
    onTimeFormatChanged: changeTimeCallBack,
  );
}

Widget _buildDescription(BuildContext context, IssueDetailsModel model) {
  var issueInfo = model.issueInfo!;
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      issueInfo.description.isEmpty
          ? const SizedBox()
          : IssueDescriptionView(
              projectId: issueInfo.projectId.id,
              padding: const EdgeInsets.only(left: 0, top: 12, right: 16, bottom: 16),
              data: issueInfo.description,
              context: context,
              specificHost: ProjectProvider().specifiedHost,
            ),
      IssueVotesView(issueIid: model.issueIid, projectIid: model.projectId, fullPath: model.fullPath),
      const SizedBox(height: 8),
      _buildDivider()
    ],
  );
}

Widget _buildAssignees(BuildContext context, IssueInfo issueInfo, void Function(List<Assignee> selectedAssignees) selectedAssignees) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Row(
        children: [
          Expanded(child: _buildParagraphHeading(AppLocalizations.dictionary().assignees)),
          if (ProjectProvider().isSpecifiedHostHasConnection && !ProjectProvider().archived)
            InkWell(
                onTap: () => selectedAssignees(issueInfo.assignees),
                radius: 0.0,
                child: Container(
                  width: 44,
                  height: 44,
                  alignment: Alignment.centerRight,
                  child: SvgPicture.asset(
                    "assets/images/edit_pencil.svg",
                    semanticsLabel: "assignees_edit",
                    height: 16,
                    width: 16,
                  ),
                )),
        ],
      ),
      issueInfo.assignees.isEmpty
          ? const _NoneView()
          : StreamingContainer(
              spacing: 12,
              children: issueInfo.assignees
                  .map((e) => AvatarAndName(
                      username: e.username,
                      avatarUrl: AvatarUrl(e.avatarUrl).realUrl(ProjectProvider().specifiedHost),
                      avatarSize: 28,
                      nameStyle: const TextStyle(fontWeight: FontWeight.w400, fontSize: 14, color: Color(0XFF03162F)),
                      gap: 8))
                  .toList(),
            ),
      _buildGap(gap: 8),
      _buildDivider(),
    ],
  );
}

Widget _buildLabels(BuildContext context, IssueInfo issueInfo, EditLabelsCallback onEditLabelsCallback) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          _buildParagraphHeading(AppLocalizations.dictionary().labels),
          if (ProjectProvider().isSpecifiedHostHasConnection && !ProjectProvider().archived)
            InkWell(
                onTap: () => onEditLabelsCallback(issueInfo.labels),
                child: Container(
                  width: 44,
                  height: 44,
                  alignment: Alignment.centerRight,
                  child: SvgPicture.asset("assets/images/edit_pencil.svg", semanticsLabel: "labels_edit", height: 16, width: 16),
                ))
        ],
      ),
      issueInfo.labels.isEmpty
          ? const _NoneView()
          : StreamingContainer(
              spacing: 12,
              children: issueInfo.labels.map((label) => LabelView(labelName: label.name, textColor: label.textColor, color: label.color)).toList(),
            ),
      _buildGap(gap: 12),
      _buildDivider()
    ],
  );
}

class _IssueMilestoneView extends StatelessWidget {
  const _IssueMilestoneView({required this.fullPath, required this.onMilestoneSelected, this.milestone});

  final String fullPath;
  final Milestone? milestone;
  final Future<void> Function(String milestoneId) onMilestoneSelected;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _TitleLineView(
          title: AppLocalizations.dictionary().milestone,
          actionSvg: AssetSvg("assets/images/edit_pencil.svg", semanticsLabel: "edit_issue_milestone", color: const Color(0xFF66696D)),
          onTap: () => _onTap(context),
          actionButtonVisible: ProjectProvider().isSpecifiedHostHasConnection && !ProjectProvider().archived,
        ),
        milestone == null ? const _NoneView() : Text(milestone!.title, style: const TextStyle(color: Color(0xFF1A1B36), fontSize: 14, fontWeight: FontWeight.normal)),
        _buildGap(gap: 12),
        _buildDivider()
      ],
    );
  }

  Future<void> _onTap(context) async {
    var result = await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => MilestoneSelectorPage(
              fullPath: fullPath,
              selectedId: milestone?.id,
              expectState: MilestoneAndIterationState.active,
            )));
    if (result != null) {
      onMilestoneSelected((result is Milestone) ? result.id : "");
    }
  }
}

class _NoneView extends StatelessWidget {
  const _NoneView();

  @override
  Widget build(BuildContext context) {
    return Text(AppLocalizations.dictionary().none, style: Theme.of(context).textTheme.bodySmall);
  }
}

Widget _buildComments(BuildContext context, IssueDetailsModel model, AutoScrollController controller, OnReplayTappedCallBack onReplayTappedCallBack) {
  if (ProjectProvider().isSpecifiedHostHasConnection) {
    return ValueListenableBuilder<List<Discussion>>(
        valueListenable: model.notifier,
        builder: (BuildContext context, List<Discussion> data, Widget? child) {
          return data.isEmpty
              ? const SizedBox()
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [_buildParagraphHeading(AppLocalizations.dictionary().comments), _buildGap(gap: 14), _buildDiscussionList(context, model, controller, onReplayTappedCallBack)],
                );
        });
  }
  if (ProjectProvider().archived) return Container();
  return _connectServerTips();
}

Widget _connectServerTips() {
  if (ProjectProvider().specifiedHost == 'https://jihulab.com') return const ConnectServerTips(targetUrl: 'jihulab.com');
  if (ProjectProvider().specifiedHost == 'https://gitlab.com') return const ConnectServerTips(targetUrl: 'gitlab.com');
  return const SizedBox();
}

Widget _buildDiscussionList(BuildContext context, IssueDetailsModel model, AutoScrollController controller, OnReplayTappedCallBack onReplayTappedCallBack) {
  final colorScheme = Theme.of(context).colorScheme;
  return ValueListenableBuilder<List<Discussion>>(
    valueListenable: model.notifier,
    builder: (BuildContext context, List<Discussion> data, Widget? child) {
      return Column(
        key: const Key('DiscussionListView'),
        children: model.comments.map((comment) {
          int index = model.comments.indexOf(comment);
          return Column(
            children: [
              if (index > 0) const Divider(thickness: 8, height: 8, color: Colors.white),
              AutoScrollTag(
                key: ValueKey(comment.notes[0].noteableIid),
                controller: controller,
                index: comment.notes[0].noteableIid,
                highlightColor: colorScheme.primary.withOpacity(0.1),
                child: DiscussionView(
                  projectId: model.projectId,
                  discussion: comment,
                  onTap: () => onReplayTappedCallBack(comment),
                  autoScrollController: controller,
                ),
              )
            ],
          );
        }).toList(),
      );
    },
  );
}

Widget _buildParagraphHeading(String heading) {
  return Text(heading, style: const TextStyle(color: Color(0xFF1A1B36), fontSize: 15, fontWeight: FontWeight.w600));
}

Widget _buildDivider() {
  return const Divider(thickness: 1, color: Color(0xFFEAEAEA), height: 1);
}

Widget _buildGap({double gap = 8}) {
  return SizedBox(height: gap);
}

class _TitleLineView extends StatelessWidget {
  const _TitleLineView({required this.title, this.actionSvg, this.actionButtonVisible = true, this.onTap, Key? key}) : super(key: key);
  final String title;
  final AssetSvg? actionSvg;
  final GestureTapCallback? onTap;
  final bool actionButtonVisible;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(title, style: const TextStyle(color: Color(0xFF1A1B36), fontSize: 15, fontWeight: FontWeight.w600)),
        if (actionSvg != null && actionButtonVisible) SvgButton(assetSvg: actionSvg!, onTap: onTap, padding: const EdgeInsets.fromLTRB(16, 16, 0, 16)),
      ],
    );
  }
}
