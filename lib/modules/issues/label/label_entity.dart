class LabelEntity {
  static const tableName = "project_labels";

  int? id;
  int labelId;
  int projectId;
  String name;
  String? description;
  String? textColor;
  String? color;
  int version;
  int? isProject;

  LabelEntity._internal(this.id, this.labelId, this.projectId, this.name, this.description, this.textColor, this.color, this.version, this.isProject);

  static LabelEntity restore(Map<String, dynamic> map) {
    return LabelEntity._internal(
      map['id'],
      map['label_id'],
      map['project_id'],
      map['name'],
      map['description'],
      map['textColor'],
      map['color'],
      map['version'],
      map['is_project_label'],
    );
  }

  static LabelEntity create(Map<String, dynamic> map, int projectId, int version) {
    return LabelEntity._internal(
      null,
      map['id'] ?? '',
      projectId,
      map['name'] ?? '',
      map['description'] ?? '',
      map['text_color'] ?? '#FFFFFF',
      map['color'] ?? '#FFFFFF',
      version,
      (map['is_project_label'] ?? true) ? 1 : 0,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'label_id': labelId,
      'project_id': projectId,
      'name': name,
      'description': description,
      'textColor': textColor,
      'color': color,
      'version': version,
      'is_project_label': isProject
    };
  }

  void update(LabelEntity entity) {
    name = entity.name;
    description = entity.description;
    textColor = entity.textColor;
    color = entity.color;
    version = entity.version;
    isProject = entity.isProject;
  }

  static String get createTableSql => """ 
    create table $tableName(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        label_id INTEGER NOT NULL,
        project_id INTEGER NOT NULL,
        name TEXT NOT NULL,
        description TEXT ,
        textColor TEXT,
        color TEXT,
        version INTEGER
    );    
    """;

  static String get addIsProjectLabelColumn => """ALTER TABLE $tableName ADD COLUMN is_project_label INTEGER; """;

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is LabelEntity && runtimeType == other.runtimeType && labelId == other.labelId && name == other.name && description == other.description;

  @override
  int get hashCode => labelId.hashCode ^ name.hashCode ^ description.hashCode;
}
