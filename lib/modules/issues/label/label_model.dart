import 'package:flutter/cupertino.dart';
import 'package:jihu_gitlab_app/core/widgets/selector/data_provider.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/selector/selector.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label.dart';
import 'package:jihu_gitlab_app/core/widgets/label_manage_view.dart';

class LabelModel {
  late DataProvider<Label> dataProvider;
  Filter<Label>? filter;
  late SelectorItemBuilder<Label> itemBuilder;
  late String projectNameSpace;

  Future<void> init({required int? projectId, required String projectNameSpace}) async {
    dataProvider = LabelProvider(projectId: projectId ?? 0);
    filter = (keyword, e) => e.name.toUpperCase().contains(keyword.toUpperCase());
    itemBuilder = (BuildContext context, int index, Label label) => LabelManageView(label: label, projectId: projectId ?? 0, projectNameSpace: projectNameSpace);
  }
}
