import 'package:jihu_gitlab_app/core/db_manager.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label_entity.dart';
import 'package:sqflite/sqflite.dart';

class LabelRepository {
  Future<List<LabelEntity>> query(int projectId, Iterable<int> labelIds) async {
    String sql = "select * from ${LabelEntity.tableName} where project_id = $projectId and label_id in (${labelIds.join(", ")})";
    var list = await DbManager.instance().rawFind(sql);
    return list.map((e) => LabelEntity.restore(e)).toList();
  }

  Future<List<int>> insert(List<LabelEntity> entities) async {
    // TODO: Cannot mock db.transaction, so add try catch for test
    try {
      return await DbManager.instance().batchInsert(LabelEntity.tableName, entities.map((e) => e.toMap()).toList());
    } catch (e) {
      return [];
    }
  }

  Future<int> update(List<LabelEntity> entities) async {
    return await DbManager.instance().batchUpdate(LabelEntity.tableName, entities.map((e) => e.toMap()).toList());
  }

  Future<List<LabelEntity>> queryByProject(int projectId) async {
    Database database = await DbManager.instance().getDb();
    List<Map<String, Object?>> query = await database.query(LabelEntity.tableName, where: " project_id = ? ", whereArgs: [projectId]);
    return query.map((e) => LabelEntity.restore(e)).toList();
  }

  Future<int> deleteLessThan(int version, int projectId) async {
    Database database = await DbManager.instance().getDb();
    return await database.delete(LabelEntity.tableName, where: " project_id = ? and version < ? ", whereArgs: [projectId, version]);
  }
}
