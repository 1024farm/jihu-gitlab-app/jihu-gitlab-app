import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label_update_model.dart';

class LabelCreationModel {
  String selectColor = "#6699cc";
  Map<String, dynamic>? _createdResp;
  LabelUpdateModel _labelUpdateModel = LabelUpdateModel();

  void init({required int projectId}) {
    _labelUpdateModel = LabelUpdateModel();
  }

  Future<Object?> create(int projectId, String name, {String? description, String? color}) async {
    try {
      String uri = Api.join('/projects/$projectId/labels');
      var body = <String, dynamic>{'name': name};
      if (description != null) {
        body['description'] = description;
      }
      if (color != null) {
        body['color'] = color;
      }

      var response = await ProjectRequestSender.instance().post<Map<String, dynamic>>(uri, body);
      _createdResp = response.body();
      return _createdResp;
    } catch (error) {
      return Future.value(false);
    }
  }

  Future<Object?> update(int projectId, int labelId, String title, {String? color, String? description}) async {
    try {
      Object? result = await _labelUpdateModel.update(projectId, labelId, title, description: description, color: color);
      return Future.value(result);
    } catch (error) {
      return Future.value(false);
    }
  }
}
