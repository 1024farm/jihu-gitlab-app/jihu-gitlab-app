import 'package:flutter/cupertino.dart';

import 'package:jihu_gitlab_app/core/loader.dart';
import 'package:jihu_gitlab_app/core/widgets/toast.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label_creation_model.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label_creation_view.dart';

class LabelCreationPage extends StatefulWidget {
  final int? projectId;
  final String from;

  const LabelCreationPage({required this.projectId, Key? key, this.from = 'project'}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _IssueCreationPageState();
}

class _IssueCreationPageState extends State<LabelCreationPage> {
  final LabelCreationModel _model = LabelCreationModel();
  final TextEditingController _labelTitleController = TextEditingController();
  final TextEditingController _labelDescriptionController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return LabelCreationView(
      onSuccess: () {
        var title = _labelTitleController.text;
        var description = _labelDescriptionController.text;
        Loader.showProgress(context);
        _model.create(widget.projectId ?? 0, title, description: description, color: _model.selectColor).then((value) {
          Loader.hideProgress(context);
          if (value != null && value is Map<String, dynamic>) {
            Toast.success(context, AppLocalizations.dictionary().createSuccessful);
            if (widget.from == 'group') {
              Navigator.pop(context);
              Navigator.pop(context, Label.fromJson(value));
            } else {
              Navigator.pop(context, Label.fromJson(value));
            }
          }
          if (value != null && value is bool && value == false) {
            Toast.error(context, AppLocalizations.dictionary().createFailed);
          }
        });
      },
      projectId: widget.projectId ?? 0,
      model: _model,
      labelTitleController: _labelTitleController,
      labelDescriptionController: _labelDescriptionController,
      title: AppLocalizations.dictionary().createLabel,
      button: AppLocalizations.dictionary().create,
    );
  }
}
