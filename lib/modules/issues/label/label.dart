import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/color_parser.dart';
import 'package:jihu_gitlab_app/core/id.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class Label {
  Id id;
  String name;
  String description;
  String textColor;
  String color;
  bool checked = false;
  bool isProjectLabel;

  Label(this.id, this.name, this.description, this.textColor, this.color, {this.isProjectLabel = true});

  factory Label.fromJson(Map<String, dynamic> json) {
    return Label(Id.from(json['id']), json['name'], json['description'] ?? "", json['text_color'] ?? "#FFFFFF", json['color'] ?? "#FFFFFF", isProjectLabel: json['is_project_label'] ?? true);
  }

  factory Label.fromGraphQLJson(Map<String, dynamic> json) {
    return Label(Id.fromGid(json['id'] ?? '/0'), json['title'], json['description'] ?? "", json['textColor'] ?? "#FFFFFF", json['color'] ?? "#FFFFFF", isProjectLabel: true);
  }

  Color? get formattedColor {
    return ColorParser(color).parse();
  }

  String displayName(Locale locale) {
    if (locale == zhLocale && description.isNotEmpty) {
      return description;
    }
    return name;
  }

  Map<String, dynamic> toJson() => {'id': id.id, 'name': name, 'description': description, "text_color": textColor, "color": color};

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Label && runtimeType == other.runtimeType && id == other.id && name == other.name && description == other.description && textColor == other.textColor && color == other.color;

  @override
  int get hashCode => id.hashCode ^ name.hashCode ^ description.hashCode ^ textColor.hashCode ^ color.hashCode;
}
