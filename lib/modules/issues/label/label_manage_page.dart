import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/search_box.dart';
import 'package:jihu_gitlab_app/core/widgets/search_no_result_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label_update_page.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label_model.dart';

class LabelManagePage extends StatefulWidget {
  final String projectNameSpace;
  final int? projectId;

  const LabelManagePage({required this.projectNameSpace, this.projectId, Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => LabelManagePageState();
}

class LabelManagePageState extends State<LabelManagePage> {
  final TextEditingController _textEditingController = TextEditingController();
  List<Label> _data = <Label>[];
  late ValueNotifier<List<Label>> _notifier;
  final LabelModel _model = LabelModel();

  @override
  void initState() {
    super.initState();
    _notifier = ValueNotifier(_data);
    _model.init(projectId: widget.projectId, projectNameSpace: widget.projectNameSpace);
    _loadDataAndRebuild();
    _model.dataProvider.syncFromRemote().then((value) => _loadDataAndRebuild());
  }

  void _loadDataAndRebuild() {
    _model.dataProvider.loadFromLocal().then((value) {
      _data = value;
      _notifier.value = _data;
      setState(() {});
    });
  }

  @override
  void dispose() {
    super.dispose();
    _textEditingController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CommonAppBar(
          showLeading: true,
          title: Text(AppLocalizations.dictionary().manageLabel),
          leading: BackButton(
              color: Colors.black,
              onPressed: () {
                Navigator.pop(context);
              }),
          actions: [
            Offstage(
              child: TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text(AppLocalizations.dictionary().done, style: TextStyle(color: Theme.of(context).primaryColor))),
            )
          ],
        ),
        body: _buildListView());
  }

  Widget _buildSearchBox() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: SearchBox(
        searchController: _textEditingController,
        onClear: () {
          _notifier.value = _data;
          _textEditingController.clear();
        },
        onChanged: (keyword) => _notifier.value = _data.where((element) => _model.filter!.call(keyword, element)).toList(),
      ),
    );
  }

  Widget _buildListView() {
    return Column(
      children: [
        _buildSearchBox(),
        Expanded(
          child: Container(
            decoration: const BoxDecoration(color: Colors.white),
            padding: const EdgeInsets.all(12.0),
            margin: const EdgeInsets.symmetric(vertical: 0, horizontal: 12.0),
            child: ValueListenableBuilder<List<Label>>(
              valueListenable: _notifier,
              builder: (BuildContext context, List<Label> data, Widget? child) {
                String search = _textEditingController.text;
                if (search.isNotEmpty && data.isEmpty) {
                  return SearchNoResultView(message: '${AppLocalizations.dictionary().noMatch}\'$search\'');
                }
                return ListView.separated(
                    itemBuilder: (context, index) => _buildListItem(index, data[index]),
                    separatorBuilder: (BuildContext context, int index) => const Divider(thickness: 12, height: 12, color: Colors.white),
                    itemCount: data.length);
              },
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildListItem(int index, Label domain) {
    void onItemTapped() => _onListTileTaped(domain);
    return InkWell(
      onTap: onItemTapped,
      child: Container(
        width: double.infinity,
        constraints: const BoxConstraints(minHeight: 44),
        margin: const EdgeInsets.only(left: 4),
        child: Row(
          children: <Widget>[Flexible(child: _model.itemBuilder.call(context, index, domain))],
        ),
      ),
    );
  }

  void _onListTileTaped(Label domain) {
    Navigator.of(context)
        .push(MaterialPageRoute(
      builder: (context) => LabelUpdatePage(projectId: widget.projectId, label: domain),
    ))
        .then((value) {
      if (value != null) {
        _loadDataAndRebuild();
        _model.dataProvider.syncFromRemote().then((value) => _loadDataAndRebuild());
      }
    });
  }
}
