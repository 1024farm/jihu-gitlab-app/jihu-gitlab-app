import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';

class LabelUpdateModel {
  LoadState _updateState = LoadState.noItemState;
  Map<String, dynamic>? _updatedResp;

  Future<Object?> update(int projectId, int labelId, String title, {String? description, String? color}) async {
    _updateState = LoadState.loadingState;
    try {
      String uri = '/projects/$projectId/labels/$labelId';
      var body = <String, dynamic>{};
      body['new_name'] = title;
      if (description != null) {
        body['description'] = description;
      }
      if (color != null) {
        body['color'] = color;
      }
      var response = await HttpClient.instance().put<Map<String, dynamic>>(Api.join(uri), body);
      _updatedResp = response.body();
      return _updatedResp;
    } catch (error) {
      _updateState = LoadState.errorState;
      return Future.value(false);
    }
  }

  Map<String, dynamic>? get updatedResp => _updatedResp;

  LoadState get updateState => _updateState;
}
