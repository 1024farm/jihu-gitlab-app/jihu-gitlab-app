import 'package:flutter/cupertino.dart';

import 'package:jihu_gitlab_app/core/loader.dart';
import 'package:jihu_gitlab_app/core/widgets/toast.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label_creation_model.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label_creation_view.dart';

class LabelUpdatePage extends StatefulWidget {
  final int? projectId;
  final Label label;

  const LabelUpdatePage({required this.projectId, required this.label, Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _LabelUpdatePageState();
}

class _LabelUpdatePageState extends State<LabelUpdatePage> {
  final LabelCreationModel _model = LabelCreationModel();
  final TextEditingController _labelTitleController = TextEditingController();
  final TextEditingController _labelDescriptionController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _labelTitleController.text = widget.label.name;
    _labelDescriptionController.text = widget.label.description;
    _model.selectColor = widget.label.color;
  }

  @override
  Widget build(BuildContext context) {
    return LabelCreationView(
      onSuccess: () {
        var title = _labelTitleController.text;
        var description = _labelDescriptionController.text;
        Loader.showProgress(context);
        _model.update(widget.projectId ?? 0, widget.label.id.id, title, description: description, color: _model.selectColor).then((value) {
          Loader.hideProgress(context);
          if (value != null && value is Map<String, dynamic>) {
            Toast.success(context, AppLocalizations.dictionary().updateSuccessful);
            Navigator.pop(context, Label.fromJson(value));
          }
          if (value != null && value is bool && value == false) {
            Toast.error(context, AppLocalizations.dictionary().updateFailed);
          }
        });
      },
      projectId: widget.projectId ?? 0,
      model: _model,
      labelTitleController: _labelTitleController,
      labelDescriptionController: _labelDescriptionController,
      title: AppLocalizations.dictionary().editLabel,
      button: AppLocalizations.dictionary().save,
    );
  }
}
