import 'package:collection/collection.dart';
import 'package:jihu_gitlab_app/core/domain/global_time.dart';
import 'package:jihu_gitlab_app/core/list_comparator.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/project_synchronizer.dart';
import 'package:jihu_gitlab_app/core/widgets/selector/data_provider.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label_entity.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label_repository.dart';

class LabelProvider extends DataProvider<Label> {
  int projectId;
  late LabelRepository _labelRepository;

  LabelProvider({required this.projectId}) {
    _labelRepository = LabelRepository();
  }

  @override
  Future<List<Label>> loadFromLocal() async {
    try {
      var list = await _labelRepository.queryByProject(projectId);
      return list
          .map((e) => Label.fromJson({
                'id': e.labelId,
                'name': e.name,
                'description': e.description,
                'text_color': e.textColor,
                'color': e.color,
                'is_project_label': e.isProject == 1,
              }))
          .sorted((left, right) => left.name.compareTo(right.name))
          .toList();
    } catch (e) {
      LogHelper.err("LabelProvider loadFromLocal error", e);
      return [];
    }
  }

  @override
  Future<bool> syncFromRemote() {
    int version = GlobalTime.now().millisecondsSinceEpoch;
    return _getAllLabels(version);
  }

  Future<bool> _getAllLabels(int version) async {
    return ProjectSynchronizer<dynamic>(
        url: Api.join('/projects/$projectId/labels'),
        dataProcessor: (data, page, pageSize) async {
          List<LabelEntity> entities = (data as List).map((e) => LabelEntity.create(e, projectId, version)).toList(growable: false);
          await _save(entities, version);
          return entities.length >= pageSize;
        }).run();
  }

  Future<void> _save(List<LabelEntity> entities, int version) async {
    List<int> labelIds = entities.map((e) => e.labelId).toList();
    List<LabelEntity> exists = await _labelRepository.query(projectId, labelIds);
    ListCompareResult<LabelEntity> result = ListComparator.compare<LabelEntity>(exists, entities);
    if (result.hasAdd) {
      await _labelRepository.insert(result.add);
    }
    if (result.hasUpdate) {
      var list = result.update.map((e) {
        e.left.update(e.right);
        return e.left;
      }).toList();
      await _labelRepository.update(list);
    }
    await _labelRepository.deleteLessThan(version, projectId);
  }
}
