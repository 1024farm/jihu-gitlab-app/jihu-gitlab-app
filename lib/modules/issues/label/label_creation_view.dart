import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';

import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label_creation_model.dart';
import 'package:provider/provider.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/input_field_with_title.dart';
import 'package:jihu_gitlab_app/core/widgets/unauthorized_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/core/color_parser.dart';

class LabelCreationView extends StatefulWidget {
  final VoidCallback? onSuccess;
  final int projectId;
  final LabelCreationModel model;
  final TextEditingController labelTitleController;
  final TextEditingController labelDescriptionController;
  final String title;
  final String button;

  const LabelCreationView(
      {required this.projectId,
      required this.title,
      required this.button,
      required this.model,
      required this.labelTitleController,
      required this.labelDescriptionController,
      this.onSuccess,
      super.key});

  @override
  State<StatefulWidget> createState() => LabelCreationViewState();
}

class LabelCreationViewState extends State<LabelCreationView> {
  bool canCreate = false;
  List<String> colors = [
    '#009966',
    '#8fbc8f',
    '#3cb371',
    '#00b140',
    '#013220',
    '#6699cc',
    '#0000ff',
    '#e6e6fa',
    '#9400d3',
    '#330066',
    '#808080',
    '#36454f',
    '#f7e7ce',
    '#c21e56',
    '#cc338b',
    '#dc143c',
    '#ff0000',
    '#cd5b45',
    '#eee600',
    '#ed9121',
    '#c39953'
  ];

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Scaffold(
            appBar: CommonAppBar(
              title: Text(widget.title),
              showLeading: true,
              actions: [
                if (ConnectionProvider.authorized)
                  Container(
                    transform: Matrix4.translationValues(-12, 2, 0),
                    child: TextButton(
                        onPressed: canCreate ? widget.onSuccess : null,
                        child: Text(widget.button, style: TextStyle(color: canCreate ? Theme.of(context).primaryColor : Theme.of(context).primaryColor.withOpacity(0.5)))),
                  ),
              ],
            ),
            body: SafeArea(child: Consumer<ConnectionProvider>(
              builder: (context, _, child) {
                if (!ConnectionProvider.authorized) {
                  return const UnauthorizedView();
                }
                return SingleChildScrollView(
                    physics: const ScrollPhysics(),
                    child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                      InputFieldWithTitle(
                        key: const Key("label_title"),
                        title: AppLocalizations.dictionary().createIssueTitle,
                        controller: widget.labelTitleController,
                        placeHolder: "",
                        onChanged: (text) {
                          _titleChanged(text);
                        },
                      ),
                      _buildTitle(AppLocalizations.dictionary().description),
                      Container(
                          margin: const EdgeInsets.symmetric(horizontal: 16),
                          decoration: BoxDecoration(color: Colors.white, border: Border.all(color: const Color(0xFFEAEAEA)), borderRadius: BorderRadius.circular(4.0)),
                          alignment: Alignment.center,
                          child: TextField(
                              key: const Key("label_description"),
                              controller: widget.labelDescriptionController,
                              maxLines: 5,
                              style: const TextStyle(color: Color(0xFF03162F), fontSize: 14, fontWeight: FontWeight.w400),
                              cursorColor: Theme.of(context).primaryColor,
                              decoration: const InputDecoration(
                                  hintStyle: TextStyle(fontSize: 14), border: OutlineInputBorder(borderSide: BorderSide.none), contentPadding: EdgeInsets.only(left: 8, top: 32)))),
                      _buildTitle(AppLocalizations.dictionary().backgroundColor),
                      InkWell(
                        onTap: _showColorPicker,
                        child: Container(
                            margin: const EdgeInsets.symmetric(horizontal: 16),
                            decoration:
                                BoxDecoration(color: ColorParser(widget.model.selectColor).parse(), border: Border.all(color: const Color(0xFFEAEAEA)), borderRadius: BorderRadius.circular(4.0)),
                            alignment: Alignment.centerRight,
                            child: SizedBox(
                              height: 45,
                              width: 100,
                              child: ElevatedButton(
                                onPressed: _showColorPicker,
                                style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(Colors.white),
                                    textStyle: MaterialStateProperty.all(const TextStyle(fontSize: 14, fontWeight: FontWeight.w400)),
                                    elevation: MaterialStateProperty.all(0),
                                    shape: MaterialStateProperty.all(const BeveledRectangleBorder(borderRadius: BorderRadius.horizontal(left: Radius.zero, right: Radius.circular(2))))),
                                child: Text(widget.model.selectColor),
                              ),
                            )),
                      ),
                      Container(
                        margin: const EdgeInsets.all(16),
                        child: Wrap(
                          children: _colorSelection(),
                        ),
                      )
                    ]));
              },
            ))));
  }

  Future _showColorPicker() {
    Color? pickColor = ColorParser(widget.model.selectColor).parse();
    return showCupertinoDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: SingleChildScrollView(
                child: Column(
              children: [
                ColorPicker(
                  enableAlpha: false,
                  labelTypes: const [ColorLabelType.rgb],
                  pickerColor: pickColor ?? Colors.cyanAccent,
                  onColorChanged: (Color value) {
                    widget.model.selectColor = '#${(value.value & 0xFFFFFF).toRadixString(16).padLeft(6, '0').toUpperCase()}';
                    setState(() {
                      canCreate = true;
                    });
                  },
                ),
                Container(
                  alignment: Alignment.centerRight,
                  child: TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      AppLocalizations.dictionary().ok,
                      style: const TextStyle(color: Color(0XFFFC6D26), fontSize: 14),
                    ),
                  ),
                )
              ],
            )),
          );
        });
  }

  void _titleChanged(String text) {
    if (widget.labelTitleController.text == '') {
      setState(() {
        canCreate = false;
      });
    } else {
      setState(() {
        canCreate = true;
      });
    }
  }

  List<Widget> _colorSelection() {
    List<Widget> colorBox = [];
    for (var color in colors) {
      colorBox.add(InkWell(
        onTap: () {
          widget.model.selectColor = color;
          setState(() {
            canCreate = true;
          });
        },
        highlightColor: Colors.transparent,
        splashColor: Colors.transparent,
        child: Container(
            width: 28, height: 28, margin: const EdgeInsets.only(right: 12, bottom: 12), decoration: BoxDecoration(color: ColorParser(color).parse(), borderRadius: BorderRadius.circular(2.0))),
      ));
    }
    return colorBox;
  }
}

Padding _buildTitle(String title, {EdgeInsetsGeometry padding = const EdgeInsets.all(16.0)}) {
  return Padding(
    padding: padding,
    child: Text(title, style: const TextStyle(color: Color(0XFF03162F), fontSize: 16, fontWeight: FontWeight.w600)),
  );
}
