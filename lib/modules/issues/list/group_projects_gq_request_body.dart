Map<String, Object> getGroupProjectsGraphQLRequestBody(String fullPath, List<dynamic> projectIds) {
  return {
    "variables": {"fullPath": fullPath, "projectIds": projectIds},
    "query": """
      query (\$fullPath: ID!, \$projectIds: [ID!]) {
        group(fullPath: \$fullPath) {
          id
          projects(ids: \$projectIds, includeSubgroups: true) {
            nodes {
              id
              path
              fullPath
              name
              nameWithNamespace
            }
          }
        }
      }
    """,
  };
}
