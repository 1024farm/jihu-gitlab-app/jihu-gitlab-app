import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/paged_state.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/http_fail_view.dart';
import 'package:jihu_gitlab_app/core/widgets/search_box.dart';
import 'package:jihu_gitlab_app/modules/issues/details/issue_details_page.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_info.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issue_filter_view.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_model.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_view.dart';
import 'package:provider/provider.dart';

typedef OnSelectedIssueChange = void Function(int index, Map<String, dynamic> params);

class IndexSource {
  int index = -1;
}

enum IssueType {
  project,
  group;

  bool get displayProject {
    return this == IssueType.group;
  }
}

class IssuesPage extends StatefulWidget {
  const IssuesPage(
      {super.key,
      this.relativePath,
      this.onSelectedIssueChange,
      this.isProject = false,
      this.showLoading = false,
      this.projectId,
      this.selectedColor = Colors.white,
      this.labelName,
      this.indexSource,
      this.selectable,
      this.issueType = IssueType.project});

  final bool isProject;
  final int? projectId;
  final bool showLoading;
  final String? relativePath;
  final Color selectedColor;
  final String? labelName;
  final OnSelectedIssueChange? onSelectedIssueChange;
  final IndexSource? indexSource;
  final IssueType issueType;
  final bool? selectable;

  @override
  State<IssuesPage> createState() => IssuesPageState();
}

class IssuesPageState extends PagedState<IssuesPage> {
  final IssuesModel _model = IssuesModel();
  late IndexSource indexSource;
  String? _relativePath;
  int? _projectId;
  Color keyBackgroundColor = const Color(0xFFEAEAEA);
  Color displayColor = AppThemeData.secondaryColor;
  final GlobalKey<IssuesViewState> _issuesViewKey = GlobalKey();

  @override
  void initState() {
    indexSource = widget.indexSource ?? IndexSource();
    _model.init(isProject: widget.isProject, projectId: widget.projectId, fullPath: widget.relativePath, labelName: widget.labelName);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _relativePath ??= widget.relativePath;
    _projectId ??= widget.projectId;
    if (_relativePath != widget.relativePath || _projectId != widget.projectId) {
      _relativePath = widget.relativePath;
      _projectId = widget.projectId;
      _model.reset();
      _model.init(isProject: widget.isProject, projectId: widget.projectId, fullPath: widget.relativePath, labelName: widget.labelName);
      indexSource.index = -1;
      _model.searchController.clear();
      onRefresh();
    }
    return Consumer<ConnectionProvider>(builder: (context, _, child) {
      if (isErrorState()()) {
        return HttpFailView(onRefresh: () => onRefresh());
      }
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12),
        child: Column(children: [_buildSearchBox(), const SizedBox(height: 12), Expanded(child: buildRefreshView())]),
      );
    });
  }

  @override
  AppBar? buildAppBar() {
    return null;
  }

  @override
  Widget? buildListView() {
    return IssuesView(
      key: _issuesViewKey,
      isProject: widget.isProject,
      search: IssuesViewSearch(_model.searchController),
      selectedIndex: indexSource.index,
      selectedColor: widget.selectedColor,
      issuesFetcher: () => _model.issues,
      isEmpty: () => _model.loadState == LoadState.successState && _model.issues.isEmpty,
      isLoading: () => _model.loadState == LoadState.loadingState,
      onRefresh: onRefresh,
      selectable: widget.selectable ?? false,
      onListItemTap: (index) async {
        final issue = _model.issues[index];
        var params = issue.asDetailParams;
        setState(() {
          indexSource.index = index;
        });
        if (widget.onSelectedIssueChange == null) {
          params['showLeading'] = true;
          Navigator.pushNamed(context, IssueDetailsPage.routeName, arguments: params).then((value) {
            var updated = _model.updateByEditedIssue(value as IssueInfo?);
            if (updated) {
              setState(() {});
            }
          });
        } else {
          widget.onSelectedIssueChange!.call(index, params);
        }
      },
      displayProject: widget.issueType.displayProject,
      scrollable: true,
    );
  }

  Widget _buildSearchBox() {
    return Row(
      children: [
        Expanded(
            child: SearchBox(
          searchController: _model.searchController,
          onSubmitted: (text) {
            onRefresh();
          },
          onClear: () {
            _model.searchController.clear();
            onRefresh();
          },
        )),
        const SizedBox(width: 12),
        _buildStateSelectView(),
      ],
    );
  }

  Widget _buildStateSelectView() {
    return InkWell(
      onTap: () {
        turnOnFilter();
        showModalBottomSheet(
          context: context,
          builder: (BuildContext context) => IssueFilterView(state: _model.state, onFilterChanged: (value) => onFilterChanged(value)),
        ).then((_) {
          turnOffFilter();
          _issuesViewKey.currentState?.refreshFieldSettings();
        });
      },
      child: Container(
        key: const Key('filter'),
        width: 42,
        height: 42,
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(color: keyBackgroundColor, borderRadius: BorderRadius.circular(4.0)),
        child: SvgPicture.asset('assets/images/filter.svg', colorFilter: ColorFilter.mode(displayColor, BlendMode.srcIn)),
      ),
    );
  }

  void onFilterChanged(String value) {
    _model.setState(IssueMenuStateOptions.values.firstWhere((element) => element.name == value));
    _model.loadingPage();
    onRefresh();
    setState(() {});
  }

  void turnOffFilter() {
    keyBackgroundColor = const Color(0xFFEAEAEA);
    displayColor = AppThemeData.secondaryColor;
    setState(() {});
  }

  void turnOnFilter() {
    keyBackgroundColor = AppThemeData.primaryColor;
    displayColor = Colors.white;
    setState(() {});
  }

  @override
  Future<bool> Function() modelRefresh() {
    return () => _model.refresh(_model.searchController.text, () => {setState(() {})});
  }

  @override
  Future<bool> Function() modelLoadMore() {
    return () => _model.loadMore(_model.searchController.text, () => setState(() {}));
  }

  @override
  Function hasNextPage() {
    return () => _model.hasNextPage;
  }

  @override
  Function isErrorState() {
    return () => _model.loadState == LoadState.errorState;
  }
}
