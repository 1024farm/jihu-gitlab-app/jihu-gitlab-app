import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/domain/assignee/assignee.dart';
import 'package:jihu_gitlab_app/core/domain/avatar_url.dart';
import 'package:jihu_gitlab_app/core/domain/issue.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar/avatar.dart';
import 'package:jihu_gitlab_app/core/widgets/issue_state_view.dart';
import 'package:jihu_gitlab_app/core/widgets/label_view.dart';
import 'package:jihu_gitlab_app/core/widgets/search_result.dart';
import 'package:jihu_gitlab_app/core/widgets/streaming_container.dart';
import 'package:jihu_gitlab_app/core/widgets/svg_text.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_view.dart';

class IssueItemView extends StatefulWidget {
  const IssueItemView({required this.issue, required this.displayProject, this.keyword, this.borderColor, this.displayOptions, this.backgroundColor, Key? key}) : super(key: key);
  final Issue issue;
  final bool displayProject;
  final String? keyword;
  final Color? borderColor;
  final Color? backgroundColor;
  final IssuesViewDisplayOptions? displayOptions;

  @override
  State<IssueItemView> createState() => _IssueItemViewState();
}

class _IssueItemViewState extends State<IssueItemView> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
            padding: const EdgeInsets.all(12),
            decoration: BoxDecoration(
                color: widget.backgroundColor ?? Colors.white,
                borderRadius: const BorderRadius.all(Radius.circular(4)),
                border: widget.borderColor != null ? Border.all(color: widget.borderColor!) : null),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    widget.issue.confidential ? Padding(padding: const EdgeInsets.only(right: 8), child: SvgPicture.asset("assets/images/visibility_off.svg")) : const SizedBox(),
                    SearchResult(keyword: widget.keyword ?? "", text: widget.issue.title)
                  ],
                ),
                _IssueLabelsRow(shouldDisplay: widget.displayOptions?.displayLabels ?? false, labels: widget.issue.labels),
                _EpicAndMilestoneAndIterationRow(issue: widget.issue, displayOptions: widget.displayOptions),
                _IidAndWeightAndAssigneeRow(issue: widget.issue, displayOptions: widget.displayOptions, shouldDisplayProjectName: widget.displayProject)
              ],
            ))
      ],
    );
  }
}

class _EpicAndMilestoneAndIterationRow extends StatelessWidget {
  const _EpicAndMilestoneAndIterationRow({required this.issue, required this.displayOptions, Key? key}) : super(key: key);
  final Issue issue;
  final IssuesViewDisplayOptions? displayOptions;

  @override
  Widget build(BuildContext context) {
    return _empty
        ? const SizedBox()
        : Padding(
            padding: const EdgeInsets.only(top: 8),
            child: StreamingContainer(
              spacing: 15,
              runSpacing: 4,
              children: [
                if (_shouldDisplayEpic) _IconAndNameFieldView(svgAssetPath: "assets/images/epic.svg", fieldValue: issue.epic!.title),
                if (_shouldDisplayMilestone) _IconAndNameFieldView(svgAssetPath: "assets/images/milestone.svg", fieldValue: issue.milestone!),
                if (_shouldDisplayIteration) _IconAndNameFieldView(svgAssetPath: "assets/images/iteration.svg", fieldValue: issue.iteration!.displayName)
              ],
            ),
          );
  }

  bool get _shouldDisplayEpic => (displayOptions?.displayEpic ?? false) && issue.epic != null;

  bool get _shouldDisplayMilestone => (displayOptions?.displayMilestone ?? false) && issue.milestone != null;

  bool get _shouldDisplayIteration => (displayOptions?.displayIteration ?? false) && issue.iteration != null;

  bool get _empty => !_shouldDisplayEpic && !_shouldDisplayMilestone && !_shouldDisplayIteration;
}

class _AssigneesAvatarView extends StatelessWidget {
  const _AssigneesAvatarView({required this.assignees});

  final List<Assignee> assignees;

  @override
  Widget build(BuildContext context) {
    var urls = assignees.map((e) => e.avatarUrl).toList();
    int index = -1;
    var widgets = urls.map<Widget>(
      (e) {
        index++;
        return Positioned(
          left: (16 * (urls.length - index)).toDouble(),
          child: Container(
            height: 20,
            width: 20,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(90), color: Colors.white),
            padding: const EdgeInsets.all(2),
            child: Avatar(
              avatarUrl: e.isNotEmpty ? AvatarUrl(e).realUrl(ProjectProvider().specifiedHost) : '',
              size: 18,
            ),
          ),
        );
      },
    ).toList();
    return Container(
      margin: const EdgeInsets.only(right: 16),
      width: urls.length * 16,
      child: Stack(alignment: Alignment.center, clipBehavior: Clip.none, children: [Container(), ...widgets]),
    );
  }
}

class _IssueLabelsRow extends StatelessWidget {
  const _IssueLabelsRow({required this.shouldDisplay, required this.labels});

  final bool shouldDisplay;
  final List<Label> labels;

  @override
  Widget build(BuildContext context) {
    return shouldDisplay && labels.isNotEmpty
        ? Padding(
            padding: const EdgeInsets.only(top: 8),
            child: StreamingContainer(
              spacing: 6,
              children: (labels).map((label) => LabelView(labelName: label.name, textColor: label.textColor, color: label.color, fontSize: 12, padding: const EdgeInsets.all(2))).toList(),
            ),
          )
        : const SizedBox();
  }
}

class _IssueIidView extends StatelessWidget {
  const _IssueIidView({required this.issueIid, required this.issueState});

  final int issueIid;
  final String issueState;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        IssueStateView(state: issueState),
        const SizedBox(width: 4),
        Text("$issueIid", style: const TextStyle(fontSize: 12, color: AppThemeData.secondaryColor)),
        const SizedBox(width: 15),
      ],
    );
  }
}

class _IconAndNameFieldView extends StatelessWidget {
  const _IconAndNameFieldView({required this.svgAssetPath, required this.fieldValue, Key? key}) : super(key: key);
  final String svgAssetPath;
  final String fieldValue;

  @override
  Widget build(BuildContext context) {
    return SvgText(svgAssetPath: svgAssetPath, svgSize: const Size(16, 16), text: fieldValue, textStyle: const TextStyle(fontSize: 12, color: AppThemeData.secondaryColor));
  }
}

class _IidAndWeightAndAssigneeRow extends StatelessWidget {
  const _IidAndWeightAndAssigneeRow({required this.issue, required this.displayOptions, required this.shouldDisplayProjectName, Key? key}) : super(key: key);
  final IssuesViewDisplayOptions? displayOptions;
  final Issue issue;
  final bool shouldDisplayProjectName;

  @override
  Widget build(BuildContext context) {
    return empty
        ? const SizedBox()
        : Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Row(
              children: [
                if (shouldDisplayIssueIid) _IssueIidView(issueIid: issue.iid, issueState: issue.state),
                if (shouldDisplayWeight) _IconAndNameFieldView(svgAssetPath: "assets/images/weight.svg", fieldValue: issue.weight.toString()),
                const SizedBox(width: 15),
                if (shouldDisplayProjectName) _IconAndNameFieldView(svgAssetPath: "assets/images/project_icon.svg", fieldValue: issue.project.name),
                Expanded(child: Container()),
                if (shouldDisplayAssignee) Padding(padding: const EdgeInsets.symmetric(vertical: 8), child: _AssigneesAvatarView(assignees: issue.assignees)),
              ],
            ),
          );
  }

  bool get shouldDisplayIssueIid => displayOptions?.displayIssueId ?? true;

  bool get shouldDisplayWeight => (displayOptions?.displayWeight ?? false) && issue.weight != null;

  bool get shouldDisplayAssignee => (displayOptions?.displayAssigneeAvatar ?? true) && issue.assignees.isNotEmpty;

  bool get empty => !shouldDisplayIssueIid && !shouldDisplayAssignee && !shouldDisplayWeight && !shouldDisplayProjectName && !shouldDisplayAssignee;
}
