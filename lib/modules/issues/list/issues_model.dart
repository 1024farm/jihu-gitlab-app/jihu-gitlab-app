import 'dart:core';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:jihu_gitlab_app/core/domain/issue.dart';
import 'package:jihu_gitlab_app/core/graph_ql_page_info.dart';
import 'package:jihu_gitlab_app/core/graphql_param.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/paginated_graphql_response.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_info.dart';
import 'package:jihu_gitlab_app/modules/issues/list/group_issues_fetcher.dart';
import 'package:jihu_gitlab_app/modules/issues/list/group_issues_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/list/group_issues_remaining_information_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/list/project_issues_fetcher.dart';
import 'package:jihu_gitlab_app/modules/issues/list/project_issues_gq_request_body.dart';
import 'package:jihu_gitlab_app/modules/issues/list/project_issues_remaining_information_gq_request_body.dart';

enum IssueMenuStateOptions {
  all('all'),
  open('opened'),
  closed('closed');

  final String name;

  const IssueMenuStateOptions(this.name);

  String title() {
    switch (this) {
      case IssueMenuStateOptions.open:
        return AppLocalizations.dictionary().issueOpenState;
      case IssueMenuStateOptions.closed:
        return AppLocalizations.dictionary().issueClosedState;
      case IssueMenuStateOptions.all:
        return AppLocalizations.dictionary().issueAllState;
    }
  }
}

class IssuesModel {
  static const int _size = 20;
  LoadState _loadState = LoadState.loadingState;
  List<Issue> _issues = [];
  IssueMenuStateOptions _state = IssueMenuStateOptions.open;
  final TextEditingController _searchController = TextEditingController();
  int? _projectId;
  String? _fullPath;
  String? _labelName;
  late bool _isProject;
  late GraphQLPageInfo _pageInfo;
  late final GroupIssuesFetcher _groupIssuesFetcher;
  late final ProjectIssuesFetcher _projectIssuesFetcher;

  void init({required bool isProject, int? projectId, String? fullPath, String? labelName}) {
    _isProject = isProject;
    _projectId = projectId;
    _labelName = labelName;
    if (_isProject) {
      assert(_projectId != null);
    }
    _fullPath = fullPath;
    _groupIssuesFetcher = GroupIssuesFetcher(fullPath ?? '');
    _projectIssuesFetcher = ProjectIssuesFetcher();
    _pageInfo = GraphQLPageInfo.init();
  }

  Future<bool> refresh(String text, Function setState) async {
    try {
      _pageInfo.reset();
      var resp = await _getIssues(_isProject, text, _fullPath, size: 20);
      _issues = resp.list;
      _pageInfo = resp.pageInfo;
      _loadState = LoadState.successState;
      return Future.value(true);
    } catch (e) {
      LogHelper.err('IssuesModel refresh error', e);
      _loadState = LoadState.errorState;
      return Future.value(true);
    }
  }

  Future<bool> loadMore(String text, Function setState) async {
    try {
      PaginatedGraphQLResponse<Issue> resp = await _getIssues(_isProject, text, _fullPath);
      _issues.addAll(resp.list);
      _pageInfo = resp.pageInfo;
      return Future.value(true);
    } catch (e) {
      LogHelper.err('IssuesModel loadMore error', e);
      return Future.value(false);
    }
  }

  Future<PaginatedGraphQLResponse<Issue>> _getIssues(bool isProject, String text, String? fullPath, {int? size}) async {
    text = GraphqlParam(text).get();
    await _ensureFullPathNotEmpty(fullPath);
    if (isProject) {
      return _getProjectIssues(text, size: size);
    }
    return await _getGroupIssues(text, size);
  }

  Future<PaginatedGraphQLResponse<Issue>> _getProjectIssues(String text, {int? size}) async {
    var issues = await _projectIssuesFetcher
        .fetchIssues(() => getProjectIssuesGraphQLRequestBody(_fullPath ?? '', text, size ?? _size, afterCursor: _pageInfo.endCursor, labelName: _labelName, state: _state.name));
    try {
      var remainingInformation = await _projectIssuesFetcher
          .fetchIssues(() => getProjectIssuesRemainingInformationGraphQLRequestBody(_fullPath ?? '', text, size ?? _size, afterCursor: _pageInfo.endCursor, labelName: _labelName, state: _state.name));
      for (var issue in issues.list) {
        issue.setRemainingInformation(remainingInformation.list.firstWhere((element) => element.id == issue.id));
      }
    } catch (_) {}
    return issues;
  }

  Future<PaginatedGraphQLResponse<Issue>> _getGroupIssues(String text, int? size) async {
    var issues = await _groupIssuesFetcher
        .fetchIssues(() => getGroupIssuesGraphQLRequestBody(_fullPath ?? '', text, size ?? _size, afterCursor: _pageInfo.endCursor, labelName: _labelName, state: _state.name));
    try {
      var remainingInformation = await _groupIssuesFetcher.fetchRemainingInformation(
          () => getGroupIssuesRemainingInformationGraphQLRequestBody(_fullPath ?? '', text, size ?? _size, afterCursor: _pageInfo.endCursor, labelName: _labelName, state: _state.name));
      for (var issue in issues.list) {
        issue.setRemainingInformation(remainingInformation.list.firstWhere((element) => element.id == issue.id));
      }
    } catch (_) {}
    return issues;
  }

  void loadingPage() {
    _loadState = LoadState.loadingState;
  }

  void reset() {
    _loadState = LoadState.loadingState;
    _issues = [];
    _state = IssueMenuStateOptions.open;
  }

  void setState(IssueMenuStateOptions value) {
    _state = value;
  }

  IssueMenuStateOptions get state => _state;

  Future<void> _ensureFullPathNotEmpty(String? fullPath) async {
    _fullPath = fullPath;
    if (_fullPath == null) {
      var response = await HttpClient.instance().get<Map<String, dynamic>>(Api.join("projects/$_projectId"));
      _fullPath = response.body()['path_with_namespace'];
    }
  }

  LoadState get loadState {
    return _loadState;
  }

  bool get hasNextPage {
    return _pageInfo.hasNextPage;
  }

  List<Issue> get issues => _issues;

  TextEditingController get searchController => _searchController;

  bool updateByEditedIssue(IssueInfo? editedIssue) {
    if (editedIssue == null) {
      return false;
    }
    var issues = _issues.where((e) => e.iid == editedIssue.iid);
    if (issues.isEmpty) {
      return false;
    }
    var issue = issues.first;
    if (_state != IssueMenuStateOptions.all && editedIssue.state != issue.state) {
      _issues.remove(issue);
      return true;
    }
    return _changed(issue, editedIssue);
  }

  bool _changed(Issue issue, IssueInfo issueInfo) {
    bool changed = false;
    if (issue.state != issueInfo.state) {
      issue.state = issueInfo.state;
      changed = true;
    }
    if (issue.milestone != issueInfo.milestone?.title) {
      issue.milestone = issueInfo.milestone?.title;
      changed = true;
    }
    if (!listEquals(issue.labels, issueInfo.labels)) {
      issue.labels = issueInfo.labels;
      changed = true;
    }
    if (!listEquals(issue.assignees, issueInfo.assignees)) {
      issue.assignees = issueInfo.assignees;
      changed = true;
    }
    if (issue.confidential != issueInfo.confidential) {
      issue.confidential = issueInfo.confidential;
      changed = true;
    }
    return changed;
  }
}
