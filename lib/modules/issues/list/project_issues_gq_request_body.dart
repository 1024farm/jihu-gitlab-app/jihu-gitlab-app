Map<String, dynamic> getProjectIssuesGraphQLRequestBody(String fullPath, String search, int size, {String? afterCursor, String? labelName, String state = "opened"}) {
  return {
    "query": """
       {
          project(fullPath:"$fullPath"){
            id
            path
            fullPath
            name
            nameWithNamespace
            issues(labelName: $labelName, first:$size, after: "${afterCursor ?? ''}", search: "$search", state: $state){
              nodes{
                id
                iid
                state
                title
                type
                author {
                  id
                  avatarUrl
                  name
                  username
                }
                assignees {
                  nodes {
                    id
                    name
                    username
                    avatarUrl
                  }
                }
                labels {
                  nodes {
                    id
                    title
                    description
                    color
                    textColor
                  }
                }
                confidential
                milestone {
                  title
                }
                createdAt
                updatedAt
              }
              pageInfo{
                hasNextPage
                endCursor
              }
            }
          }
       }
       """
  };
}
