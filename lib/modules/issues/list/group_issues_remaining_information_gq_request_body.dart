Map<String, dynamic> getGroupIssuesRemainingInformationGraphQLRequestBody(String fullPath, String search, int size, {String? afterCursor, String? labelName, String state = "opened"}) {
  return {
    "query": """
       {
          group(fullPath:"$fullPath") {
            issues(labelName: $labelName, first:$size, after: "${afterCursor ?? ''}", search: "$search", state: $state){
              nodes {
                id
                iteration {
                  id
                  state
                  startDate
                  dueDate
                  scopedPath
                  webPath
                }
                epic {
                  title
                }
                weight
              }
            }
          }
       }
       """
  };
}
