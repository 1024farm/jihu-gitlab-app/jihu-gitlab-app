import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_page.dart';

class IssueSelector extends StatelessWidget {
  static const String routeName = 'IssueSelector';
  final Map arguments;

  const IssueSelector({required this.arguments, super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CommonAppBar(title: Text(AppLocalizations.dictionary().selectIssue), showLeading: true),
        body: IssuesPage(
          relativePath: arguments['relativePath'],
          isProject: true,
          projectId: arguments['projectId'],
          onSelectedIssueChange: (index, params) {
            Navigator.of(context).pop(params['issue']);
          },
        ));
  }
}
