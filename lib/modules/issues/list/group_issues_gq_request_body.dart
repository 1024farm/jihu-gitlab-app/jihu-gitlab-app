Map<String, dynamic> getGroupIssuesGraphQLRequestBody(String fullPath, String search, int size, {String? afterCursor, String? labelName, String state = "opened"}) {
  return {
    "query": """
       {
          group(fullPath:"$fullPath") {
            id
            issues(labelName: $labelName, first:$size, after: "${afterCursor ?? ''}", search: "$search", state: $state, includeSubgroups: true){
              nodes {
                id
                iid
                state
                title
                type
                author {
                  id
                  avatarUrl
                  name
                  username
                }
                assignees {
                  nodes {
                    id
                    name
                    username
                    avatarUrl
                  }
                }
                labels {
                  nodes {
                    id
                    title
                    description
                    color
                    textColor
                  }
                }
                confidential
                milestone {
                  title
                }
                createdAt
                updatedAt
              }
              pageInfo {
                hasNextPage
                endCursor
              }
            }
          }
       }
       """
  };
}
