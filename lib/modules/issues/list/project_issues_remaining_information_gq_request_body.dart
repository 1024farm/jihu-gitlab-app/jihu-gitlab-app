Map<String, dynamic> getProjectIssuesRemainingInformationGraphQLRequestBody(String fullPath, String search, int size, {String? afterCursor, String? labelName, String state = "opened"}) {
  return {
    "query": """
       {
          project(fullPath:"$fullPath"){
            id
            path
            fullPath
            name
            nameWithNamespace
            issues(labelName: $labelName, first:$size, after: "${afterCursor ?? ''}", search: "$search", state: $state){
              nodes{
                id
                epic {
                  title
                }
                weight
                iteration {
                  id
                  state
                  startDate
                  dueDate
                  scopedPath
                  webPath
                }
              }
            }
          }
       }
       """
  };
}
