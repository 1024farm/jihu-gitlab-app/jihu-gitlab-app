import 'package:jihu_gitlab_app/core/domain/issue.dart';
import 'package:jihu_gitlab_app/core/domain/project.dart';
import 'package:jihu_gitlab_app/core/graph_ql_page_info.dart';
import 'package:jihu_gitlab_app/core/id.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/paginated_graphql_response.dart';
import 'package:jihu_gitlab_app/modules/issues/list/group_projects_gq_request_body.dart';

class GroupIssuesFetcher {
  final List<Project> _groupProjects = [];
  final String fullPath;

  GroupIssuesFetcher(this.fullPath);

  Future<PaginatedGraphQLResponse<Issue>> fetchRemainingInformation(Map Function() bodyProvider) async {
    var res = await HttpClient.instance().post(Api.graphql(), bodyProvider());
    var errors = res.body()["errors"];
    if (errors != null) throw Exception(errors);
    var issues = res.body()?['data']?['group']?['issues'];
    var list = issues?['nodes'] ?? [];
    List<Issue> issueList = list.map<Issue>((e) => Issue.fromGroupGraphQLJson(e)).toList();
    return PaginatedGraphQLResponse<Issue>(GraphQLPageInfo.fromJson({}), issueList);
  }

  Future<PaginatedGraphQLResponse<Issue>> fetchIssues(Map Function() bodyProvider) async {
    var res = await HttpClient.instance().post(Api.graphql(), bodyProvider());
    var errors = res.body()["errors"];
    if (errors != null) throw Exception(errors);
    var issues = res.body()?['data']?['group']?['issues'];
    var list = issues?['nodes'] ?? [];
    List<Issue> issueList = [];
    try {
      await _syncGroupProjects(list);
      issueList = list.map<Issue>((e) => Issue.fromGroupGraphQLJson(e, _groupProjects.firstWhere((element) => element.id == e['projectId']))).toList();
    } catch (e) {
      issueList = await _buildIssueList(list, Id.fromGid(res.body()?['data']?['group']?['id'] ?? ''));
    }
    GraphQLPageInfo pageInfo = GraphQLPageInfo.fromJson(issues?['pageInfo'] ?? {});
    return PaginatedGraphQLResponse<Issue>(pageInfo, issueList);
  }

  Future<List<Issue>> _buildIssueList(issueList, Id groupId) async {
    var nextRequestIssueIds = issueList.map((e) => int.parse(e['iid'])).toList();
    if (nextRequestIssueIds.isEmpty) return Future.value([]);
    var iidAndProjectId = await _getGroupProjectIds(nextRequestIssueIds, groupId);
    issueList.forEach((element) {
      var index = iidAndProjectId.indexWhere((e) => e.gid == element['id']);
      if (index >= 0) element['projectId'] = iidAndProjectId[index].projectId;
    });
    await _syncGroupProjects(issueList);
    var list = issueList.map<Issue>((e) {
      Project projectJson = Project.fromJson({});
      if (_groupProjects.isNotEmpty) {
        var index = _groupProjects.indexWhere((element) => element.id == e['projectId']);
        if (index >= 0) {
          projectJson = _groupProjects[index];
        }
      }
      return Issue.fromGroupGraphQLJson(e, projectJson);
    }).toList();
    return list;
  }

  Future<List<IssueGidAndProjectId>> _getGroupProjectIds(List<dynamic> iids, Id groupId) async {
    if (iids.isEmpty) return [];
    var projectsResp = await HttpClient.instance().get(Api.join('/groups/${groupId.id}/issues?${iids.map((e) => 'iids[]=$e').join('&')}'));
    var projectsData = projectsResp.body();
    if (projectsData == null) return [];
    return (projectsData as List).map((e) => IssueGidAndProjectId('gid://gitlab/Issue/${e['id']}', e['project_id'] as int)).toList();
  }

  Future<void> _syncGroupProjects(issueList) async {
    var nextRequestProjectIds = issueList
        .where((element) {
          if (element['projectId'] == null) return false;
          int pjId = element['projectId'] as int;
          var index = _groupProjects.indexWhere((element) => element.id == pjId);
          return index < 0;
        })
        .map((e) => e['projectId'] as int)
        .toSet()
        .toList();
    if (nextRequestProjectIds.isEmpty) return;
    var projectIds = nextRequestProjectIds.map((e) => 'gid://gitlab/Project/$e').toList();
    List<Project> projects = await _getGroupProjects(projectIds);
    if (projects.isNotEmpty) _groupProjects.addAll(projects);
  }

  Future<List<Project>> _getGroupProjects(List<dynamic> projectIds) async {
    if (projectIds.isEmpty) return [];
    var projectsResp = await HttpClient.instance().post(Api.graphql(), getGroupProjectsGraphQLRequestBody(fullPath, projectIds));
    var projectsData = projectsResp.body()['data']?['group'];
    if (projectsData == null) return [];
    var projectList = projectsData['projects']?['nodes'] ?? [];
    return List<Project>.from(projectList.map((e) => Project.fromGraphQLJson(e)).toList());
  }
}

class IssueGidAndProjectId {
  final String gid;
  final int projectId;

  IssueGidAndProjectId(this.gid, this.projectId);
}
