import 'package:jihu_gitlab_app/core/domain/issue.dart';
import 'package:jihu_gitlab_app/core/graph_ql_page_info.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';
import 'package:jihu_gitlab_app/core/paginated_graphql_response.dart';

class ProjectIssuesFetcher {
  Future<PaginatedGraphQLResponse<Issue>> fetchIssues(Map Function() bodyProvider) async {
    var response = await ProjectRequestSender.instance().post(Api.graphql(), bodyProvider());
    var errors = response.body()["errors"];
    if (errors != null) throw Exception(errors);
    var issues = response.body()?['data']?['project']?['issues'];
    var project = response.body()?['data']?['project'];
    List<Issue> issueList = issues?['nodes']?.map<Issue>((e) => Issue.fromProjectGraphQLJson(e, project)).toList() ?? [];
    GraphQLPageInfo pageInfo = GraphQLPageInfo.fromJson(issues?['pageInfo'] ?? {});
    return PaginatedGraphQLResponse<Issue>(pageInfo, issueList);
  }
}
