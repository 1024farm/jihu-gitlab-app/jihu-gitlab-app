import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/domain/avatar_url.dart';
import 'package:jihu_gitlab_app/core/domain/issue.dart';
import 'package:jihu_gitlab_app/core/fields_settings.dart';
import 'package:jihu_gitlab_app/core/layout/adaptive.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar/avatar.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_view.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issue_item_view.dart';

class IssuesViewSearch {
  final TextEditingController searchController;

  IssuesViewSearch(this.searchController);
}

class IssuesViewDisplayOptions {
  final bool displayIssueId;
  final bool displayWeight;
  final bool displayEpic;
  final bool displayLabels;
  final bool displayIteration;
  final bool displayAssigneeAvatar;
  final bool displayMilestone;

  IssuesViewDisplayOptions({
    this.displayIssueId = true,
    this.displayWeight = true,
    this.displayEpic = false,
    this.displayLabels = false,
    this.displayIteration = false,
    this.displayAssigneeAvatar = true,
    this.displayMilestone = false,
  });
}

typedef ListItemTapCallback = void Function(int index);

class IssuesView extends StatefulWidget {
  const IssuesView({
    required this.issuesFetcher,
    required this.isLoading,
    required this.isEmpty,
    required this.onRefresh,
    required this.onListItemTap,
    required this.selectedIndex,
    required this.selectedColor,
    required this.scrollable,
    required this.displayProject,
    this.isProject = false,
    this.search,
    this.displayOptions,
    this.shrinkWrap,
    this.selectable = false,
    super.key,
  });

  final bool isProject;
  final IssuesViewSearch? search;
  final int selectedIndex;
  final List<Issue> Function() issuesFetcher;
  final bool Function() isEmpty;
  final bool Function() isLoading;
  final Future<void> Function() onRefresh;
  final ListItemTapCallback onListItemTap;
  final Color selectedColor;
  final bool scrollable;
  final IssuesViewDisplayOptions? displayOptions;
  final bool displayProject;
  final bool? shrinkWrap;
  final bool selectable;

  @override
  State<IssuesView> createState() => IssuesViewState();
}

class IssuesViewState extends State<IssuesView> {
  IssuesViewDisplayOptions? displayOptions;
  Issue? _selectedIssue;

  @override
  void initState() {
    super.initState();
    if (widget.displayOptions != null) {
      displayOptions = widget.displayOptions!;
    } else {
      refreshFieldSettings();
    }
  }

  void refreshFieldSettings() {
    FieldsSettings.read().then((settings) {
      setState(() {
        displayOptions = settings.toDisplayOptions();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (widget.isEmpty()) {
      if (widget.search == null) {
        return TipsView(icon: 'assets/images/no_item.svg', message: AppLocalizations.dictionary().noData, onRefresh: () => widget.onRefresh());
      }
      bool isSearching = widget.search!.searchController.text.isNotEmpty;
      return TipsView(
          icon: isSearching ? 'assets/images/search.svg' : null,
          message: isSearching ? '${AppLocalizations.dictionary().noMatch}\'${widget.search!.searchController.text}\'' : null,
          onRefresh: () => widget.onRefresh());
    }
    return _buildListView();
  }

  Widget _buildListView() {
    if (widget.isLoading()) return const LoadingView();
    return ListView.separated(
      physics: widget.scrollable ? null : const NeverScrollableScrollPhysics(),
      shrinkWrap: widget.shrinkWrap ?? !widget.scrollable,
      separatorBuilder: (context, index) => const Divider(color: Colors.transparent, height: 12, thickness: 12),
      itemBuilder: (context, index) {
        Color? itemBackgroundColor = isDesktopLayout(context) && index == widget.selectedIndex ? widget.selectedColor : null;
        Issue issue = widget.issuesFetcher()[index];
        return InkWell(
          onTap: () {
            setState(() => _selectedIssue = issue);
            widget.onListItemTap(index);
          },
          child: Container(
            padding: widget.selectable ? const EdgeInsets.only(left: 12) : null,
            decoration: widget.selectable ? const BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(4))) : null,
            child: Row(
              children: [
                if (widget.selectable)
                  _selectedIssue?.iid == issue.iid
                      ? Icon(Icons.check_circle, color: Theme.of(context).primaryColor, size: 18)
                      : const Icon(Icons.radio_button_unchecked, color: Color(0XFFCECECE), size: 18),
                Flexible(
                  child: IssueItemView(
                    issue: issue,
                    displayProject: widget.displayProject,
                    keyword: widget.search == null ? "" : widget.search!.searchController.text,
                    borderColor: itemBackgroundColor,
                    displayOptions: displayOptions,
                    backgroundColor: widget.selectable ? Colors.transparent : Colors.white,
                  ),
                ),
              ],
            ),
          ),
        );
      },
      itemCount: widget.issuesFetcher().length,
    );
  }

  Widget assigneesAvatars(Issue issue) {
    var urls = issue.assignees.map((e) => e.avatarUrl).toList();
    int index = -1;
    var widgets = urls.map<Widget>(
      (e) {
        index++;
        return Positioned(
          left: (16 * (urls.length - index)).toDouble(),
          child: Container(
            height: 20,
            width: 20,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(90), color: Colors.white),
            padding: const EdgeInsets.all(2),
            child: Avatar(
              avatarUrl: e.isNotEmpty ? AvatarUrl(e).realUrl(ProjectProvider().specifiedHost) : '',
              size: 18,
            ),
          ),
        );
      },
    ).toList();
    return Container(
      margin: const EdgeInsets.only(right: 16),
      width: urls.length * 16,
      child: Stack(alignment: Alignment.center, clipBehavior: Clip.none, children: [Container(), ...widgets]),
    );
  }
}
