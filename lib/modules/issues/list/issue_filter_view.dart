import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/fields_settings.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/choice_view.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_button.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_view.dart';
import 'package:jihu_gitlab_app/core/widgets/streaming_container.dart';
import 'package:jihu_gitlab_app/core/widgets/toggle_button.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_model.dart';

class IssueFilterView extends StatefulWidget {
  const IssueFilterView({required this.onFilterChanged, required this.state, super.key});

  final void Function(String value) onFilterChanged;
  final IssueMenuStateOptions state;

  @override
  State<IssueFilterView> createState() => _IssueFilterViewState();
}

class _IssueFilterViewState extends State<IssueFilterView> {
  late final FieldsSettings fieldsSettings;
  LoadState loadState = LoadState.loadingState;
  late IssueMenuStateOptions _selectedState;

  @override
  void initState() {
    _selectedState = widget.state;
    super.initState();
    FieldsSettings.read().then((value) {
      fieldsSettings = value;
      setState(() => loadState = LoadState.successState);
    });
  }

  String get state {
    return _selectedState.name;
  }

  @override
  Widget build(BuildContext context) {
    if (loadState == LoadState.loadingState) return const LoadingView();
    return SafeArea(
      child: Container(
        padding: const EdgeInsets.all(16),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisSize: MainAxisSize.min, children: [
          Flexible(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(AppLocalizations.dictionary().state, style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: AppThemeData.secondaryColor)),
                  const SizedBox(height: 8),
                  _stateButtons(),
                  const SizedBox(height: 16),
                  Text(AppLocalizations.dictionary().fields, style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: AppThemeData.secondaryColor)),
                  const SizedBox(height: 8),
                  _fields(),
                ],
              ),
            ),
          ),
          _done(context),
        ]),
      ),
    );
  }

  Widget _fields() {
    return StreamingContainer(
      spacing: 12,
      children: [
        ToggleButton(
            svgPath: 'assets/images/avatar.svg',
            title: AppLocalizations.dictionary().assigneeAvatar,
            selectedProvider: () => fieldsSettings.assigneeAvatar,
            onChange: () => fieldsSettings.assigneeAvatar = !fieldsSettings.assigneeAvatar),
        ToggleButton(
          svgPath: 'assets/images/epic.svg',
          title: AppLocalizations.dictionary().epic,
          selectedProvider: () => fieldsSettings.epic,
          onChange: () => fieldsSettings.epic = !fieldsSettings.epic,
        ),
        ToggleButton(
            svgPath: 'assets/images/issues.svg',
            title: AppLocalizations.dictionary().issueIid,
            selectedProvider: () => fieldsSettings.issueId,
            onChange: () => fieldsSettings.issueId = !fieldsSettings.issueId),
        ToggleButton(
            svgPath: 'assets/images/iteration.svg',
            title: AppLocalizations.dictionary().iteration,
            selectedProvider: () => fieldsSettings.iteration,
            onChange: () => fieldsSettings.iteration = !fieldsSettings.iteration),
        ToggleButton(
          svgPath: 'assets/images/label.svg',
          title: AppLocalizations.dictionary().labels,
          selectedProvider: () => fieldsSettings.labels,
          onChange: () => fieldsSettings.labels = !fieldsSettings.labels,
        ),
        ToggleButton(
          svgPath: 'assets/images/milestone.svg',
          title: AppLocalizations.dictionary().milestone,
          selectedProvider: () => fieldsSettings.milestone,
          onChange: () => fieldsSettings.milestone = !fieldsSettings.milestone,
        ),
        ToggleButton(
            svgPath: 'assets/images/weight.svg',
            title: AppLocalizations.dictionary().projectsIterationWeight,
            selectedProvider: () => fieldsSettings.weight,
            onChange: () => fieldsSettings.weight = !fieldsSettings.weight),
      ],
    );
  }

  Widget _done(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.only(top: 16),
      child: LoadingButton.asElevatedButton(
        text: Text(AppLocalizations.dictionary().done, style: const TextStyle(color: Colors.white)),
        onPressed: () {
          fieldsSettings.save().then((value) => Navigator.of(context).pop());
          widget.onFilterChanged(state);
        },
      ),
    );
  }

  Widget _stateButtons() {
    return ChoiceView<IssueMenuStateOptions>(
        selected: [_selectedState],
        candidates: IssueMenuStateOptions.values,
        onChoiceChange: <IssueMenuStateOptions>(state) {
          _selectedState = state;
          return true;
        },
        itemBuilder: <Widget>(option, selected) {
          return Container(
            padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
            decoration: BoxDecoration(
              color: selected ? const Color(0xFFFFEBE2) : Colors.white,
              borderRadius: BorderRadius.circular(4),
              border: selected ? Border.all(color: const Color(0xFFFFEBE2), width: 1) : Border.all(color: const Color(0xFFEAEAEA), width: 1),
            ),
            child: Text(option.title(), style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: selected ? Theme.of(context).primaryColor : const Color(0xFF03162F))),
          );
        });
  }
}
