import 'package:easy_refresh/easy_refresh.dart';
import 'package:jihu_gitlab_app/core/graph_ql_page_info.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';
import 'package:jihu_gitlab_app/core/paginated_graphql_response.dart';

class CodeQualityReportDetail {
  String severity;
  String description;
  String path;
  int line;

  CodeQualityReportDetail._(this.severity, this.description, this.path, this.line);

  factory CodeQualityReportDetail.fromJson(body) {
    return CodeQualityReportDetail._(body['severity'] ?? '', body['description'] ?? '', body['path'] ?? '', body['line'] ?? 0);
  }
}

class CodeQualityReportModel {
  static const int _pageSize = 20;
  late final String _fullPath;
  late final int _mrIid;
  LoadState _loadState = LoadState.loadingState;
  List<CodeQualityReportDetail> _reportDetails = [];
  GraphQLPageInfo _pageInfo = GraphQLPageInfo.init();
  final EasyRefreshController _refreshController = EasyRefreshController(controlFinishLoad: true, controlFinishRefresh: true);

  CodeQualityReportModel(this._fullPath, this._mrIid);

  Future<void> refresh() async {
    try {
      _pageInfo.reset();
      var response = await _getCodeQualityReports(_pageSize, _pageInfo.endCursor);
      _pageInfo = response.pageInfo;
      _reportDetails = response.list;
      _loadState = _reportDetails.isNotEmpty ? LoadState.successState : LoadState.noItemState;
      _refreshController.finishRefresh(IndicatorResult.success);
      _refreshController.resetFooter();
    } catch (e) {
      _loadState = LoadState.errorState;
      _refreshController.finishRefresh(IndicatorResult.fail);
    }
  }

  Future<void> loadMore() async {
    try {
      var response = await _getCodeQualityReports(_pageSize, _pageInfo.endCursor);
      _pageInfo = response.pageInfo;
      _reportDetails.addAll(response.list);
      _refreshController.finishLoad(_pageInfo.hasNextPage ? IndicatorResult.success : IndicatorResult.noMore);
    } catch (e) {
      _loadState = LoadState.errorState;
    }
  }

  Future<PaginatedGraphQLResponse<CodeQualityReportDetail>> _getCodeQualityReports(int pageSize, String nextPageCursor) async {
    var body = (await ProjectRequestSender.instance().post(Api.graphql(), requestBodyOfGetCodeQualityReports(_fullPath, _mrIid, pageSize, nextPageCursor))).body();
    if (body['errors'] != null) {
      throw Exception(body['errors']);
    }
    var codeQualityReports = body['data']?['project']?['mergeRequest']?['headPipeline']?['codeQualityReports'];
    var pageInfo = GraphQLPageInfo.fromJson(codeQualityReports?['pageInfo'] ?? {});
    var nodes = codeQualityReports?['nodes'];
    List<CodeQualityReportDetail> data = ((nodes ?? []) as List).map((e) => CodeQualityReportDetail.fromJson(e)).toList();
    return PaginatedGraphQLResponse(pageInfo, data);
  }

  List<CodeQualityReportDetail> get reportDetails => _reportDetails;

  LoadState get loadState => _loadState;

  EasyRefreshController get refreshController => _refreshController;
}

Map<String, dynamic> requestBodyOfGetCodeQualityReports(String fullPath, int mrIid, int pageSize, String nextPageCursor) {
  return {
    "query": """
       {
          project(fullPath: "$fullPath") {
            mergeRequest(iid: "$mrIid") {
              headPipeline {
                codeQualityReports(first:$pageSize,after:"$nextPageCursor"){
                  nodes {
                    line
                    path
                    description
                    severity
                  }
                  pageInfo{
                    hasNextPage
                    endCursor
                  }
                }
              }
            }
          }
       }
       """
  };
}
