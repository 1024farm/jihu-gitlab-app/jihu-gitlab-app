import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/domain/avatar_url.dart';
import 'package:jihu_gitlab_app/core/global_keys.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/settings/settings.dart';
import 'package:jihu_gitlab_app/core/system_version_detector.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar_and_name.dart';
import 'package:jihu_gitlab_app/core/widgets/changeable_time.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/http_fail_view.dart';
import 'package:jihu_gitlab_app/core/widgets/low_system_version_view.dart';
import 'package:jihu_gitlab_app/core/widgets/project_archived_notice_view.dart';
import 'package:jihu_gitlab_app/core/widgets/share_view.dart';
import 'package:jihu_gitlab_app/core/widgets/skeleton.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_description_view.dart';
import 'package:jihu_gitlab_app/modules/mr/approval_view.dart';
import 'package:jihu_gitlab_app/modules/mr/changes_view.dart';
import 'package:jihu_gitlab_app/modules/mr/code_quality_report_page.dart';
import 'package:jihu_gitlab_app/modules/mr/code_quality_summary_view.dart';
import 'package:jihu_gitlab_app/modules/mr/commits/commits_view.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_blocked_view.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_ready_view.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_request_tab_bar.dart';
import 'package:jihu_gitlab_app/modules/mr/models/merge_request_model.dart';
import 'package:provider/provider.dart';

class MergeRequestPage extends StatefulWidget {
  static const String routeName = 'MergeRequestPage';
  final Map arguments;

  const MergeRequestPage({required this.arguments, super.key});

  @override
  State<MergeRequestPage> createState() => _MergeRequestPageState();
}

class _MergeRequestPageState extends State<MergeRequestPage> with TickerProviderStateMixin {
  final MergeRequestModel _model = MergeRequestModel();

  @override
  void initState() {
    SystemVersionDetector.instance().detect().then((value) => setState(() {
          _model.systemType = value;
        }));
    super.initState();
    _model.init(widget.arguments, this);
    _model.onRefresh(() => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    _saveWebUrl();
    return Consumer<ConnectionProvider>(
      builder: (context, _, child) => _buildScaffold(),
    );
  }

  Future<void> _saveWebUrl() async {
    if (_model.mr != null && (await ConnectionProvider.mergeRequestWebUrl).isEmpty) {
      LocalStorage.save(LocalStorageKeys.currentMergeRequestWebUrlKey, _model.mr!.webUrl);
    }
  }

  Widget _buildScaffold() {
    return WillPopScope(
        child: Scaffold(
          appBar: CommonAppBar(
            showLeading: true,
            backgroundColor: Colors.white,
            title: _model.systemType == SystemType.unmaintainable ? const Text("") : Text(_model.pageTitle),
            actions: [
              if (_model.mr != null)
                Container(
                  margin: const EdgeInsets.only(right: 20),
                  child: PopupMenuButton<String>(
                    padding: EdgeInsets.zero,
                    position: PopupMenuPosition.under,
                    itemBuilder: (context) => [PopupMenuItem<String>(padding: EdgeInsets.zero, child: ShareView(title: _model.mr!.title, link: _model.mr!.webUrl))],
                    child: Container(
                      margin: const EdgeInsets.only(right: 20),
                      child: SvgPicture.asset("assets/images/external-link.svg",
                          semanticsLabel: "share", colorFilter: const ColorFilter.mode(AppThemeData.secondaryColor, BlendMode.srcIn), width: 18, height: 18),
                    ),
                  ),
                ),
            ],
          ),
          body: SafeArea(
              bottom: true,
              child: Stack(children: [
                Container(color: Colors.white, child: _buildBody()),
                if (ProjectProvider().archived) const Align(alignment: Alignment.bottomCenter, child: ProjectArchivedNoticeView()),
              ])),
        ),
        onWillPop: () {
          LocalStorage.remove(LocalStorageKeys.currentMergeRequestWebUrlKey);
          return Future(() => true);
        });
  }

  Widget _buildBody() {
    if (_model.systemType == SystemType.unmaintainable) return const LowSystemVersionView(systemType: SystemType.unmaintainable, isForGeek: false);
    if (_model.loadState == LoadState.errorState) {
      return HttpFailView(
        onRefresh: () => _model.onRefresh(() => setState(() {})),
      );
    }
    return NestedScrollView(
      physics: const ClampingScrollPhysics(),
      headerSliverBuilder: (context, value) {
        return [
          SliverToBoxAdapter(
              child: Container(
            padding: const EdgeInsets.all(16),
            child: _model.mr == null
                ? Row(children: const [Expanded(child: Skeleton(height: 28))])
                : Text(_model.mr!.title, style: const TextStyle(fontSize: 18, color: Color(0xFF1A1B36), fontWeight: FontWeight.w600)),
          )),
          SliverToBoxAdapter(child: Container(padding: const EdgeInsets.symmetric(horizontal: 16), child: _buildStateBar())),
          if (_model.mr != null) SliverToBoxAdapter(child: MergeRequestTabBar(tabController: _model.tabController, tabBarTitles: _model.tabTitles)),
        ];
      },
      body: TabBarView(
        key: const Key('MergeRequestPageTabBarView'),
        controller: _model.tabController,
        children: [SingleChildScrollView(child: _buildOverviewBody()), _buildCommitsBody(), _buildChangesBody()].sublist(0, _model.tabTitles.length),
      ),
    );
  }

  Widget _buildChangesBody() {
    return ChangesView(key: MergeRequestModel.pageKey, model: _model);
  }

  Widget _buildOverviewBody() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          _buildDescription(),
          const SizedBox(height: 8),
          _buildBranchDealings(),
          const SizedBox(height: 8),
          _buildDivider(),
          const SizedBox(height: 8),
          ..._buildPipelineState(),
          if (!widget.arguments.containsKey('test')) ..._buildReports(),
          if (_model.approvalVisible) ..._buildApproval(),
          const SizedBox(height: 8),
          if (_model.mergeBlockedVisible) _buildMergeBlocked(),
          if (_model.mergeReadyVisible) _buildMergeReadyView(),
          if (_model.mergedInfoVisible) _buildMergedInfo()
        ],
      ),
    );
  }

  Widget _buildCommitsBody() {
    if (_model.mr == null) return const SizedBox();
    return CommitsView(commits: _model.mr?.commits.items ?? []);
  }

  Widget _buildDescription() {
    if (_model.mr == null) {
      return Row(children: const [Expanded(child: Skeleton(height: 40))]);
    }
    return _model.mr!.description.isEmpty
        ? Container()
        : Column(children: [
            Container(
              padding: const EdgeInsets.symmetric(vertical: 8),
              child: IssueDescriptionView(
                projectId: _model.mr!.projectId,
                data: _model.mr!.description,
                context: context,
                specificHost: ProjectProvider().specifiedHost,
              ),
            ),
            _buildDivider(),
          ]);
  }

  Widget _buildDivider() {
    return const Divider(thickness: 1, color: Color(0xFFEAEAEA));
  }

  Widget _buildStateBar() {
    return Row(
      children: [
        _model.mr == null ? const Skeleton(height: 16, width: 16) : _model.mr!.state.toSvg(),
        const SizedBox(width: 8),
        _model.mr == null
            ? const Skeleton(height: 16, width: 16)
            : AvatarAndName(
                key: const Key('MergeRequestAuthorAvatar'),
                username: _model.mr!.author.name,
                avatarUrl: _model.mr!.author.avatarUrl.isNotEmpty ? AvatarUrl(_model.mr!.author.avatarUrl).realUrl(ProjectProvider().specifiedHost) : '',
                gap: 4,
                nameStyle: const TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Color(0xFF87878C)),
                avatarSize: 16),
        if (_model.mr == null)
          const Expanded(child: Skeleton(height: 16, width: 200))
        else ...[
          Text(" ${Settings.instance().precisionTime().isEnabled() ? AppLocalizations.dictionary().createdAtPreciseFormat : AppLocalizations.dictionary().created}",
              style: const TextStyle(color: Color(0xFF87878C), fontWeight: FontWeight.w400, fontSize: 12)),
          ChangeableTime.show(() => setState(() {}), _model.mr!.createdAt, const Color(0xFF95979A))
        ],
      ],
    );
  }

  List<Widget> _buildPipelineState() {
    if (_model.mr != null && _model.mr!.headPipeline == null) return [];
    return [
      Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _model.mr == null ? const Skeleton(height: 20, width: 20) : _model.mr!.headPipeline!.statusToSvg(),
          const SizedBox(width: 8),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _model.mr == null
                    ? Row(children: const [Expanded(child: Skeleton(height: 58))])
                    : Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(text: '${AppLocalizations.dictionary().pipeline} '),
                            TextSpan(text: '#${_model.mr!.headPipeline!.id}', style: const TextStyle(fontWeight: FontWeight.w400)),
                            TextSpan(text: ' ${_model.mr!.headPipeline!.detailedStatus.translatedLabel} '),
                            if (_model.mr!.commits.isNotEmpty)
                              TextSpan(
                                  text: '${AppLocalizations.dictionary().pipelineFor} ',
                                  children: [TextSpan(text: '${_model.mr!.commits.latestShortId} ', style: const TextStyle(fontWeight: FontWeight.w400))]),
                            TextSpan(text: '${AppLocalizations.dictionary().on} '),
                            TextSpan(text: _model.mr!.headPipeline!.ref, style: const TextStyle(fontWeight: FontWeight.w400)),
                          ],
                        ),
                        style: const TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
                      ),
                const SizedBox(height: 8),
                _buildCoverage(),
                const SizedBox(height: 8),
                _buildStagesStatus(),
              ],
            ),
          ),
        ],
      ),
      const SizedBox(height: 8),
      _buildDivider(),
    ];
  }

  Widget _buildCoverage() {
    if (_model.mr == null) return Row(children: const [Expanded(child: Skeleton(height: 24, width: 200))]);
    if (_model.mr!.headPipeline == null || _model.mr!.headPipeline!.coverage == null || _model.mr!.headPipeline!.coverage == '' || double.parse(_model.mr!.headPipeline!.coverage!) <= 0) {
      return Container();
    }
    return Text('${AppLocalizations.dictionary().testCoverage} ${_model.mr!.headPipeline!.coverage!}% ${AppLocalizations.dictionary().byJob(1)}',
        style: const TextStyle(fontSize: 14, fontWeight: FontWeight.w400, color: AppThemeData.secondaryColor));
  }

  List<Widget> _buildApproval() {
    return [
      ApprovalView(
        approved: _model.approved,
        isOptional: _model.isOptional,
        canBeApproved: _model.canBeApproved,
        canBeRevokedApproval: _model.canBeRevokedApproval,
        approvalRules: _model.mr!.approvalRules,
        approvalRulesLeft: _model.approvalRulesLeft,
        approvalsLeft: _model.approvalsLeft,
        onApproved: () => _model.approve().then((value) => _model.onRefresh(() => setState(() {}))),
        onRevokedApproval: () => _model.revokeApproval().then((value) => _model.onRefresh(() => setState(() {}))),
      ),
      _buildDivider()
    ];
  }

  Widget _buildMergeBlocked() {
    return MergeBlockedView(
      mergeRequest: _model.mr!,
      onMarkedAsReady: () => _model.onRefresh(() => setState(() {})),
      rebaseStatusLooper: () => _model.isRebaseInProgress(),
      onRebaseFinished: () => _model.onRefresh(() => setState(() {})),
    );
  }

  Widget _buildMergeReadyView() {
    return MergeReadyView(
      mergeRequest: _model.mr!,
      onMergeFinished: () => _model.onRefresh(() => setState(() {})),
    );
  }

  Widget _buildMergedInfo() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: [
        SvgPicture.asset("assets/images/mr_merged.svg", width: 20, height: 20),
        const SizedBox(width: 8),
        Text(AppLocalizations.dictionary().mergedBy, style: const TextStyle(color: Color(0xFF1A1B36), fontSize: 15, fontWeight: FontWeight.w600)),
        const SizedBox(width: 20),
        if (_model.mr!.mergeUser != null)
          AvatarAndName(username: _model.mr!.mergeUser!.name, avatarUrl: AvatarUrl(_model.mr!.mergeUser!.avatarUrl).realUrl(ProjectProvider().specifiedHost), avatarSize: 24)
      ],
    );
  }

  Widget _buildBranchDealings() {
    return Column(
      children: [
        Row(
          children: [
            if (_model.mr != null) const Icon(Icons.radio_button_unchecked, size: 16, color: Color(0xFF87878C)),
            if (_model.mr != null) const SizedBox(width: 12),
            Expanded(
              child: _model.mr == null
                  ? const Skeleton(height: 20)
                  : Text(
                      '${AppLocalizations.dictionary().sourceBranch}: ${_model.mr!.sourceBranch}',
                      style: const TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
                      overflow: TextOverflow.ellipsis,
                    ),
            ),
          ],
        ),
        const SizedBox(height: 8),
        Row(
          children: [
            if (_model.mr != null) const Icon(Icons.toll, size: 16, color: Color(0xFF87878C)),
            if (_model.mr != null) const SizedBox(width: 12),
            Expanded(
              child: _model.mr == null
                  ? const Skeleton(height: 20)
                  : Text(
                      '${AppLocalizations.dictionary().targetBranch}: ${_model.mr!.targetBranch}',
                      style: const TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
                      overflow: TextOverflow.ellipsis,
                    ),
            ),
          ],
        ),
      ],
    );
  }

  List<Widget> _buildReports() {
    var codeQualityReportSummary = _model.mr?.codeQualityReportSummary;
    if (codeQualityReportSummary == null) {
      return [];
    }
    return [
      InkWell(
        onTap: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => CodeQualityReportPage(fullPath: _model.projectFullPath, mrIid: _model.mergeRequestIid, degradedCount: codeQualityReportSummary.count)));
        },
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [Flexible(flex: 1, child: CodeQualitySummaryView(count: codeQualityReportSummary.count)), const Icon(Icons.arrow_forward_ios, size: 16)],
          ),
        ),
      ),
      _buildDivider(),
    ];
  }

  Widget _buildStagesStatus() {
    if (_model.mr == null || _model.mr!.jobs.isEmpty) return const Skeleton(height: 20);
    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      Expanded(
        child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            padding: EdgeInsets.zero,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: _generateStatusIconsAndCalculateOffset(),
            )),
      ),
      Align(
        alignment: Alignment.centerRight,
        child: Padding(
          padding: const EdgeInsets.only(left: 8),
          child: _model.mr == null ? Container() : Text(_model.mr!.headPipeline!.finishedAt.value, style: const TextStyle(fontSize: 14, fontWeight: FontWeight.w600)),
        ),
      )
    ]);
  }

  List<Widget> _generateStatusIconsAndCalculateOffset() {
    var names = _model.mr!.jobs.stages.map((e) => e.stateIconAssetName).toList();
    if (names.isEmpty) return [];
    List<Widget> statuses = [];
    int index = 0;
    // Original svg have gaps. Flutter doesn't support negative margin, use transform calculation to remove these gaps.
    for (var name in names) {
      statuses.add(Container(
        transform: Matrix4.translationValues((-6 * index).toDouble(), 0, 0),
        child: SvgPicture.asset(name, width: 20, height: 20),
      ));
      index++;
      statuses.add(Container(
        transform: Matrix4.translationValues((-6 * index).toDouble(), 0, 0),
        child: const Icon(Icons.remove, size: 16, color: Colors.grey),
      ));
      index++;
    }
    statuses.removeLast();
    return statuses;
  }
}
