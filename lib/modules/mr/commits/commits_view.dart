import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/domain/avatar_url.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar/avatar.dart';
import 'package:jihu_gitlab_app/core/widgets/changeable_time.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/mr/models/commits.dart';

class CommitsView extends StatefulWidget {
  final List<Commit> commits;

  const CommitsView({required this.commits, Key? key}) : super(key: key);

  @override
  State<CommitsView> createState() => _CommitsViewState();
}

class _CommitsViewState extends State<CommitsView> with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    List<Commit> commits = widget.commits;
    if (commits.isEmpty) {
      return TipsView(icon: "assets/images/no_commits.svg", message: AppLocalizations.dictionary().noCommits);
    }
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: ListView.separated(
        itemBuilder: (context, index) => _buildItem(commits[index]),
        itemCount: commits.length,
        separatorBuilder: (BuildContext context, int index) => const Divider(color: Color(0xFFEAEAEA), thickness: 1, indent: 48, endIndent: 16, height: 20),
      ),
    );
  }

  Widget _buildItem(Commit commit) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Avatar(avatarUrl: AvatarUrl(commit.author?.avatarUrl ?? '').realUrl(ProjectProvider().specifiedHost), size: 24),
          const SizedBox(width: 8),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    height: 24, alignment: Alignment.centerLeft, child: Text(commit.author?.name ?? '', style: const TextStyle(color: Color(0xFF03162F), fontSize: 13, fontWeight: FontWeight.normal))),
                const SizedBox(height: 8),
                Text(commit.title, style: const TextStyle(color: Color(0xFF03162F), fontSize: 14, fontWeight: FontWeight.w500)),
                const SizedBox(height: 8),
                Row(
                  children: [
                    Text(AppLocalizations.dictionary().authored, style: const TextStyle(color: AppThemeData.secondaryColor, fontSize: 12, fontWeight: FontWeight.normal)),
                    ChangeableTime.show(() => setState(() {}), commit.createdAt, AppThemeData.secondaryColor)
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
