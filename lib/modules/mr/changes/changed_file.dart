import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/string_extension.dart';
import 'package:jihu_gitlab_app/modules/mr/models/diff.dart';

class ChangedFile {
  String name;
  List<FileLine> fileLines;

  ChangedFile._(this.name, this.fileLines);

  factory ChangedFile.parse(Diff diff) {
    return ChangedFile._(diff.newPath, generateCodeLines(diff.diff));
  }

  static List<FileLine> generateCodeLines(String diff) {
    diff = diff.replaceAll('\r\n', '\n');
    List<FileLine> mapLines = [];

    List<String> codes = parseCodeSource([], diff);
    for (var code in codes) {
      String blockStart = '@@ ';
      String lineNumberEnd = ' @@';
      String lineNumberBlock = code.catchSubstring(from: blockStart, to: lineNumberEnd);
      mapLines.addAll(parseCodeLines(lineNumberBlock, code, removeLastLine: codes.last == code));
    }
    return mapLines;
  }

  static List<String> parseCodeSource(List<String> codes, String source) {
    List<String> results = codes;
    String blockStart = '@@ ';
    if (results.isNotEmpty) {
      blockStart = '\n$blockStart';
    }
    if (!source.startsWith(blockStart)) return results;
    String lineNumberEnd = ' @@';
    String lineNumberBlock = source.catchSubstring(from: blockStart, to: lineNumberEnd);
    if (blockStart == '@@ ') blockStart = '\n$blockStart';
    String buffer = source.catchSubstring(from: lineNumberBlock, includeFrom: false);
    if (!buffer.contains(blockStart)) {
      results.add(source);
      return results;
    }
    String codeChangeBlock = source.catchSubstring(from: lineNumberBlock, includeFrom: true, to: blockStart, includeTo: false);
    results.add(codeChangeBlock);
    return parseCodeSource(results, source.replaceFirst(codeChangeBlock, ''));
  }

  static List<FileLine> parseCodeLines(String lineNumberSource, String codeSource, {bool removeLastLine = false}) {
    List<FileLine> result = [];
    if (!lineNumberSource.contains('@@')) return result;
    var list = lineNumberSource.split(' ');
    List<String> delNumbers = parseLineNumbers(list[1]);
    List<String> addNumbers = parseLineNumbers(list[2]);
    List<String> codeLines = codeSource.split('\n');
    if (codeLines.isNotEmpty && codeSource.startsWith('\n')) {
      codeLines.removeAt(0);
    }
    if (removeLastLine) codeLines.removeLast();
    List.generate(codeLines.length, (index) {
      String item = codeLines[index];
      FileLineType type = FileLineType.unchanged;
      if (item.startsWith('+')) {
        type = FileLineType.added;
        delNumbers.insert(index, '');
      }
      if (item.startsWith('-')) {
        type = FileLineType.deleted;
        addNumbers.insert(index, '');
      }
      if (item.startsWith('@@ ')) {
        delNumbers.insert(index, '...');
        addNumbers.insert(index, '...');
      }
      if (item.startsWith('\\ ')) {
        delNumbers.insert(index, '');
        addNumbers.insert(index, '');
      }

      var codeLine = FileLine(delNumbers[index], addNumbers[index], type, item);
      result.add(codeLine);
    });
    return result;
  }

  static List<String> parseLineNumbers(String numberSource) {
    List<String> result = [];
    var source = numberSource.split(',');
    var start = 0;
    var lineCount = 0;
    if (source.length == 1) {
      start = int.parse(source[0]).abs();
      lineCount = start;
    } else if (source.length == 2) {
      start = int.parse(source[0]).abs();
      lineCount = int.parse(source[1]);
    }
    List.generate(lineCount, (index) => result.add('${start + index}'));
    return result;
  }
}

class FileLine {
  String delLineNumber;
  String addLineNumber;
  FileLineType type;
  String codeContent;

  FileLine(this.delLineNumber, this.addLineNumber, this.type, this.codeContent);
}

enum FileLineType {
  added(Color(0xFFDBE7C6)),
  deleted(Color(0XFFFBD4C1)),
  unchanged(Color(0xFFFDF6E3));

  final Color background;

  const FileLineType(this.background);
}
