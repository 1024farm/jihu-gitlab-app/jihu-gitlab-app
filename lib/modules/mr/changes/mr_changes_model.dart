import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/modules/mr/changes/changed_file.dart';
import 'package:jihu_gitlab_app/modules/mr/changes/changes_file_section.dart';
import 'package:jihu_gitlab_app/modules/mr/models/diff.dart';

class MrChangesModel {
  late List<ChangedFile> changedFiles;
  late List<ChangesFileSection> sectionList;

  void init(List<Diff> diffs, int target) {
    changedFiles = diffs.map((e) => ChangedFile.parse(e)).toList();

    List<ChangesFileSection> list = [];
    for (var file in changedFiles) {
      var section = ChangesFileSection()
        ..header = file.name
        ..items = [Container()]
        ..expanded = false;
      if (changedFiles.indexOf(file) == target) section.expanded = true;
      list.add(section);
    }
    sectionList = list;
  }
}
