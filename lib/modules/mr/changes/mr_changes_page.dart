import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/syntax_highlighter.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/mr/changes/changed_file.dart';
import 'package:jihu_gitlab_app/modules/mr/changes/changes_file_section.dart';
import 'package:jihu_gitlab_app/modules/mr/changes/mr_changes_model.dart';
import 'package:jihu_gitlab_app/modules/mr/models/diff.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:sticky_and_expandable_list/sticky_and_expandable_list.dart';

class MrChangesPage extends StatefulWidget {
  const MrChangesPage({required this.diffs, required this.targetIndex, super.key});

  final List<Diff> diffs;
  final int targetIndex;

  @override
  State<MrChangesPage> createState() => _MrChangesPageState();
}

class _MrChangesPageState extends State<MrChangesPage> {
  final MrChangesModel _model = MrChangesModel();
  late AutoScrollController _controller;

  @override
  void initState() {
    _model.init(widget.diffs, widget.targetIndex);
    _controller = AutoScrollController(viewportBoundaryGetter: () => Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom), axis: Axis.vertical);
    super.initState();
    Future.delayed(const Duration(milliseconds: 500)).then((value) {
      _scrollToTarget();
    });
  }

  Future<void> _scrollToTarget() async {
    await _controller.scrollToIndex(widget.targetIndex, duration: const Duration(milliseconds: 50), preferPosition: AutoScrollPosition.begin);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text(AppLocalizations.dictionary().cancel, style: const TextStyle(color: AppThemeData.secondaryColor, fontSize: 14)),
          ),
          leadingWidth: 100,
        ),
        body: ExpandableListView(
          controller: _controller,
          builder: SliverExpandableChildDelegate<Widget, ChangesFileSection>(
              sectionList: _model.sectionList,
              headerBuilder: _buildHeader,
              separatorBuilder: (BuildContext context, bool isSectionSeparator, int index) => const Divider(thickness: 1, height: 1, color: Color(0xFFEAEAEA)),
              itemBuilder: (context, sectionIndex, itemIndex, index) {
                var changedFile = _model.changedFiles[sectionIndex];
                return _buildItem(changedFile);
              }),
        ));
  }

  Widget _buildHeader(BuildContext context, int sectionIndex, int index) {
    ChangesFileSection section = _model.sectionList[sectionIndex];
    return AutoScrollTag(
        key: ValueKey(sectionIndex),
        controller: _controller,
        index: sectionIndex,
        child: InkWell(
            key: GlobalObjectKey(sectionIndex),
            child: Container(
                color: Colors.white,
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                alignment: Alignment.centerLeft,
                child: Row(
                  children: [
                    Expanded(child: Text(section.header, style: const TextStyle(fontSize: 12, color: Colors.black87, fontWeight: FontWeight.w400))),
                    Icon(section.isSectionExpanded() ? Icons.keyboard_arrow_down : Icons.keyboard_arrow_right, size: 20, color: Colors.black54)
                  ],
                )),
            onTap: () {
              setState(() {
                section.setSectionExpanded(!section.isSectionExpanded());
              });
            }));
  }

  Widget _buildItem(ChangedFile changedFile) {
    return Container(
      color: const Color(0xFFFDF6E3),
      width: MediaQuery.of(context).size.width,
      child: _generateCodeView(changedFile),
    );
  }

  Widget _generateCodeView(ChangedFile changedFile) {
    List<FileLine> fileLines = changedFile.fileLines;
    final SyntaxHighlighterStyle style = Theme.of(context).brightness == Brightness.dark ? SyntaxHighlighterStyle.darkThemeStyle() : SyntaxHighlighterStyle.lightThemeStyle();
    return IntrinsicHeight(
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            Container(
              width: 60,
              padding: const EdgeInsets.only(top: 8),
              child: Column(
                children: fileLines
                    .map((e) => Container(
                          key: Key('${changedFile.name}_${e.delLineNumber}_${e.addLineNumber}_${e.codeContent}'),
                          color: e.type.background,
                          child: IntrinsicHeight(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      e.delLineNumber,
                                      style: const TextStyle(color: Colors.black54, fontSize: 12),
                                    ),
                                  ),
                                ),
                                const VerticalDivider(width: 1, thickness: 1),
                                Expanded(
                                    flex: 1,
                                    child: Container(
                                        alignment: Alignment.center,
                                        child: Text(
                                          e.addLineNumber,
                                          style: const TextStyle(color: Colors.black54, fontSize: 12),
                                        )))
                              ],
                            ),
                          ),
                        ))
                    .toList(),
              ),
            ),
            Container(
                padding: const EdgeInsets.all(8),
                child: SelectableText.rich(
                    TextSpan(
                      style: const TextStyle(color: Colors.black87, fontSize: 12),
                      children: fileLines
                          .map((e) => e.codeContent.startsWith('@@')
                              ? TextSpan(
                                  text: '${e.codeContent}\n',
                                  style: const TextStyle(color: Colors.black45, overflow: TextOverflow.clip, backgroundColor: Color(0xFFFEFAEE)),
                                )
                              : TextSpan(style: TextStyle(backgroundColor: e.type.background), children: [DartSyntaxHighlighter(style).format('${e.codeContent}\n')]))
                          .toList(),
                    ),
                    scrollPhysics: const NeverScrollableScrollPhysics(),
                    textDirection: TextDirection.ltr)),
          ],
        ),
      ),
    );
  }
}
