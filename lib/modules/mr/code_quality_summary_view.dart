import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class CodeQualitySummaryView extends StatelessWidget {
  const CodeQualitySummaryView({required this.count, super.key});

  final int count;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SvgPicture.asset('assets/images/code_quality_warnings.svg', width: 20, height: 20),
        const SizedBox(width: 8),
        Expanded(
          child: Text(AppLocalizations.dictionary().codeQualityDegraded(count), style: const TextStyle(fontWeight: FontWeight.w600, fontSize: 16)),
        ),
      ],
    );
  }
}
