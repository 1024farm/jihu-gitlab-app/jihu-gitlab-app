import 'package:flutter/material.dart';

class MergeOptionView extends StatefulWidget {
  const MergeOptionView({required this.description, required this.onCheckChanged, this.checked = false, this.disabled = false, Key? key}) : super(key: key);

  final String description;
  final ValueChanged<bool>? onCheckChanged;
  final bool checked;
  final bool disabled;

  @override
  State<MergeOptionView> createState() => _MergeOptionViewState();
}

class _MergeOptionViewState extends State<MergeOptionView> {
  bool _checked = false;

  @override
  void initState() {
    _checked = widget.checked;
    super.initState();
  }

  @override
  void didUpdateWidget(covariant MergeOptionView oldWidget) {
    _checked = widget.checked;
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.disabled
          ? null
          : () {
              setState(() => {_checked = !_checked});
              widget.onCheckChanged?.call(_checked);
            },
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Checkbox(
            key: Key(widget.description),
            value: _checked,
            onChanged: null,
          ),
          Text(widget.description)
        ],
      ),
    );
  }
}
