import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_button.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/mr/approval_rule_view.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_ready_view.dart';
import 'package:jihu_gitlab_app/modules/mr/models/approval.dart';

class ApprovalView extends StatefulWidget {
  final bool approved;
  final bool isOptional;
  final bool canBeApproved;
  final bool canBeRevokedApproval;
  final List<ApprovalRule> approvalRules;
  final List<String> approvalRulesLeft;
  final int approvalsLeft;
  final VoidFutureCallBack onApproved;
  final VoidFutureCallBack onRevokedApproval;

  const ApprovalView({
    required this.approved,
    required this.isOptional,
    required this.canBeApproved,
    required this.canBeRevokedApproval,
    required this.approvalRules,
    required this.approvalRulesLeft,
    required this.approvalsLeft,
    required this.onApproved,
    required this.onRevokedApproval,
    Key? key,
  }) : super(key: key);

  @override
  State<ApprovalView> createState() => _ApprovalViewState();
}

class _ApprovalViewState extends State<ApprovalView> {
  bool _isApproving = false;
  bool _isRevokingApproval = false;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: [
        SvgPicture.asset("assets/images/approval.svg", width: 20, height: 20),
        const SizedBox(width: 8),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(_title, style: const TextStyle(color: Color(0xFF1A1B36), fontSize: 15, fontWeight: FontWeight.w600)),
              const SizedBox(height: 12),
              ..._buildApprovalRules(),
              if (ProjectProvider().isSpecifiedHostHasConnection && !ProjectProvider().archived) _buildApprovingButton(),
              const SizedBox(height: 8),
            ],
          ),
        )
      ],
    );
  }

  List<Widget> _buildApprovalRules() {
    return widget.approvalRules
        .map((e) => Padding(
              padding: const EdgeInsets.only(bottom: 12),
              child: ApprovalRuleView(key: Key("${e.ruleType}_${e.name}"), approvalRule: e),
            ))
        .toList();
  }

  Widget _buildApprovingButton() {
    if (widget.canBeApproved) {
      return LoadingButton.asElevatedButton(
          onPressed: () async {
            setState(() => _isApproving = true);
            await widget.onApproved();
            setState(() => _isApproving = false);
          },
          isLoading: _isApproving,
          padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
          text: Text(AppLocalizations.dictionary().approve, style: const TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w500)));
    }
    if (widget.canBeRevokedApproval) {
      return LoadingButton.asOutlinedButton(
          onPressed: () async {
            setState(() => _isRevokingApproval = true);
            await widget.onRevokedApproval();
            setState(() => _isRevokingApproval = false);
          },
          isLoading: _isRevokingApproval,
          padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
          text: Text(AppLocalizations.dictionary().revokeApproval, style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 14, fontWeight: FontWeight.w500)));
    }
    return const SizedBox();
  }

  String get _approvalRuleNameSeparator => widget.approvalRulesLeft.length == 2 ? " and " : ", ";

  String get _title {
    if (widget.isOptional) {
      return AppLocalizations.dictionary().approvalIsOptional;
    }
    if (widget.approved) {
      return AppLocalizations.dictionary().approved;
    }
    return AppLocalizations.dictionary().requiredApprovals(AppLocalizations.dictionary().countOfApprovals(widget.approvalsLeft), widget.approvalRulesLeft.join(_approvalRuleNameSeparator));
  }
}
