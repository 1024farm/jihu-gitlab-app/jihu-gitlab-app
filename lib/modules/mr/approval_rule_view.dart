import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection.dart';
import 'package:jihu_gitlab_app/core/domain/avatar_url.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar/avatar.dart';
import 'package:jihu_gitlab_app/core/widgets/streaming_container.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/mr/models/approval.dart';

const String anyApproverRuleType = 'ANY_APPROVER';

class ApprovalRuleView extends StatefulWidget {
  final ApprovalRule approvalRule;

  const ApprovalRuleView({required this.approvalRule, Key? key}) : super(key: key);

  @override
  State<ApprovalRuleView> createState() => _ApprovalRuleViewState();
}

class _ApprovalRuleViewState extends State<ApprovalRuleView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(border: Border.all(color: const Color(0xFFEAEAEA)), borderRadius: BorderRadius.circular(4)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            color: const Color(0xFFF5F5F5),
            padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
            width: double.infinity,
            child: Text(_title, style: const TextStyle(color: Color(0xFF1A1B36), fontSize: 14, fontWeight: FontWeight.w400)),
          ),
          if (_allApprovers.isNotEmpty) const Divider(color: Color(0xFFEAEAEA), thickness: 1, height: 1),
          if (_allApprovers.isNotEmpty)
            Padding(
              padding: const EdgeInsets.all(12),
              child: StreamingContainer(children: _allApprovers.map((e) => _buildApproverAvatar(e, widget.approvalRule.approvedBy.where((u) => u.id == e.id).isNotEmpty)).toList()),
            )
        ],
      ),
    );
  }

  Widget _buildApproverAvatar(User user, bool approved) {
    var avatar = user.avatarUrl;
    if (approved) {
      return Stack(
        alignment: Alignment.bottomRight,
        children: [
          Avatar(
            key: Key('${widget.approvalRule.name}_${user.username}_approved'),
            avatarUrl: avatar.isNotEmpty ? AvatarUrl(avatar).realUrl(ProjectProvider().specifiedHost) : '',
            size: 24,
            boxBorder: Border.all(color: const Color(0xFF18CD08), width: 1),
          ),
          SvgPicture.asset("assets/images/approver.svg", height: 10, width: 10)
        ],
      );
    }
    return Avatar(key: Key('${widget.approvalRule.name}_${user.username}'), avatarUrl: avatar.isNotEmpty ? AvatarUrl(avatar).realUrl(ProjectProvider().specifiedHost) : '', size: 24);
  }

  List get _allApprovers => <dynamic>{...widget.approvalRule.eligibleApprovers, ...widget.approvalRule.approvedBy}.toList();

  String get _title {
    if (widget.approvalRule.ruleType == anyApproverRuleType) {
      return AppLocalizations.dictionary().allEligibleUsers;
    }
    if (widget.approvalRule.isOptional) {
      return AppLocalizations.dictionary().optionalApproveRuleTitle(AppLocalizations.dictionary().countOfApprovals(widget.approvalRule.approvedBy.length), widget.approvalRule.name);
    }
    return AppLocalizations.dictionary()
        .approveRuleTitle(widget.approvalRule.approvedBy.length, AppLocalizations.dictionary().countOfApprovals(widget.approvalRule.approvalsRequired), widget.approvalRule.name);
  }
}
