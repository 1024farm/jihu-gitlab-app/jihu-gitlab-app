import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection.dart';
import 'package:jihu_gitlab_app/core/domain/assignee/assignee.dart';
import 'package:jihu_gitlab_app/core/id.dart';
import 'package:jihu_gitlab_app/core/time.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/mr/models/approval.dart';
import 'package:jihu_gitlab_app/modules/mr/models/code_quality_report_summary.dart';
import 'package:jihu_gitlab_app/modules/mr/models/commits.dart';
import 'package:jihu_gitlab_app/modules/mr/models/jobs.dart';
import 'package:jihu_gitlab_app/modules/mr/models/pipeline.dart';

class MergeRequest {
  final int iid;
  final int projectId;
  final String fullPath;
  final String projectName;
  final String sourceBranch;
  final String targetBranch;
  final MergeRequestState state;
  final String title;
  final String description;
  final Assignee author;
  final Pipeline? headPipeline;
  final Time createdAt;
  final bool rebaseInProgress;
  DetailMergeStatus _detailedMergeStatus;
  final bool userCanMerge;
  final bool hasConflicts;
  final int commitCount;
  final bool draft;
  Commits _commits;
  CodeQualityReportSummary? _codeQualityReportSummary;
  Jobs _jobs;
  List<ApprovalRule> _approvalRules;
  final bool mergeable;
  final bool shouldBeRebased;
  final String? mergeError;
  final String webUrl;
  final bool forceRemoveSourceBranch;
  final bool squashOnMerge;
  final bool mergeRequestsFfOnlyEnabled;
  final bool squashReadOnly;
  final String defaultMergeCommitMessage;
  final String defaultSquashCommitMessage;
  final User? mergeUser;
  final Time? mergedAt;
  final bool mergeOngoing;
  List<String> availableAutoMergeStrategies;
  bool mergeWhenPipelineSucceeds;
  bool commitsHasDuplicates = false;

  MergeRequest._(
      this.iid,
      this.projectId,
      this.fullPath,
      this.projectName,
      this.sourceBranch,
      this.targetBranch,
      this.state,
      this.title,
      this.description,
      this.author,
      this.headPipeline,
      this.createdAt,
      this.rebaseInProgress,
      this._detailedMergeStatus,
      this.userCanMerge,
      this.hasConflicts,
      this.commitCount,
      this.draft,
      this._commits,
      this._codeQualityReportSummary,
      this._jobs,
      this._approvalRules,
      this.mergeable,
      this.shouldBeRebased,
      this.mergeError,
      this.webUrl,
      this.forceRemoveSourceBranch,
      this.squashOnMerge,
      this.mergeRequestsFfOnlyEnabled,
      this.squashReadOnly,
      this.defaultMergeCommitMessage,
      this.defaultSquashCommitMessage,
      this.mergeUser,
      this.mergedAt,
      this.mergeOngoing,
      this.availableAutoMergeStrategies,
      this.mergeWhenPipelineSucceeds);

  factory MergeRequest.fromJson(Map<String, dynamic> json) {
    var mergeRequest = json['mergeRequest'] ?? {};
    var headPipeline = mergeRequest['headPipeline'] ?? {};
    return MergeRequest._(
      int.parse(mergeRequest['iid'] ?? ''),
      Id.fromGid(json['id'] ?? '/0').id,
      json['fullPath'] ?? '',
      json['name'] ?? '',
      mergeRequest['sourceBranch'] ?? '',
      mergeRequest['targetBranch'] ?? '',
      MergeRequestState.valueOf(mergeRequest['state'] ?? ''),
      mergeRequest['title'] ?? '',
      mergeRequest['description'] ?? '',
      Assignee.fromGraphQLJson(mergeRequest['author'] ?? {}),
      mergeRequest['headPipeline'] == null ? null : Pipeline.fromJson(headPipeline),
      Time.init(mergeRequest['createdAt'] ?? ''),
      mergeRequest['rebaseInProgress'] ?? false,
      DetailMergeStatus.unchecked,
      mergeRequest['userPermissions']?['canMerge'] ?? false,
      mergeRequest['conflicts'] ?? false,
      mergeRequest['commitCount'] ?? 0,
      mergeRequest['draft'] ?? false,
      Commits.fromJson(mergeRequest['commits']?['nodes'] ?? []),
      null,
      Jobs.fromJson([]),
      [],
      mergeRequest['mergeable'] ?? false,
      mergeRequest['shouldBeRebased'] ?? false,
      mergeRequest['mergeError'],
      mergeRequest['webUrl'] ?? '',
      mergeRequest['forceRemoveSourceBranch'] ?? false,
      mergeRequest['squashOnMerge'] ?? false,
      json['mergeRequestsFfOnlyEnabled'] ?? false,
      json['squashReadOnly'] ?? false,
      mergeRequest['defaultMergeCommitMessage'] ?? '',
      mergeRequest['defaultSquashCommitMessage'] ?? '',
      mergeRequest['mergeUser'] == null ? null : User.fromGraphQLJson(mergeRequest['mergeUser']),
      mergeRequest['mergedAt'] == null ? null : Time.init(mergeRequest['mergedAt']),
      mergeRequest['mergeOngoing'] ?? false,
      List<String>.from(mergeRequest['availableAutoMergeStrategies'] ?? <String>[]),
      mergeRequest['mergeWhenPipelineSucceeds'] ?? false,
    );
  }

  void paidDataFromJson(Map<String, dynamic> json) {
    var mergeRequest = json['mergeRequest'] ?? {};
    List<dynamic> rules = mergeRequest['approvalState']?['rules'] ?? [];
    _detailedMergeStatus = DetailMergeStatus.valueOf(mergeRequest['detailedMergeStatus'] ?? DetailMergeStatus.unchecked.name);
    var codeQualityReportSummaryJson = mergeRequest['headPipeline']?['codeQualityReportSummary'];
    if (codeQualityReportSummaryJson != null) {
      _codeQualityReportSummary = CodeQualityReportSummary.fromJson(codeQualityReportSummaryJson!);
    }
    _approvalRules = rules.map((e) => ApprovalRule.fromGraphQLJson(e)).toList();
  }

  void jobsFromResp(List<Job> list) {
    _jobs = Jobs.fromList(list.reversed.toList());
  }

  void updateCommits(List<Commit> items) {
    _commits = Commits.fromList(items);
    commitsHasDuplicates = items.length != items.map((e) => e.title).toSet().length;
  }

  List<ApprovalRule> get approvalRules => _approvalRules;

  CodeQualityReportSummary? get codeQualityReportSummary => _codeQualityReportSummary;

  DetailMergeStatus get detailedMergeStatus => _detailedMergeStatus;

  Commits get commits => _commits;

  Jobs get jobs => _jobs;
}

enum MergeRequestState {
  opened('opened', 'assets/images/mr_opened.svg'),
  closed('closed', 'assets/images/mr_closed.svg'),
  merged('merged', 'assets/images/mr_merged.svg'),
  locked('locked', 'assets/images/mr_opened.svg');

  final String label;
  final String svgPath;

  const MergeRequestState(this.label, this.svgPath);

  static MergeRequestState valueOf(String status) {
    var matches = MergeRequestState.values.where((e) => e.label == status);
    return matches.isNotEmpty ? matches.first : MergeRequestState.opened;
  }

  SvgPicture toSvg() {
    return SvgPicture.asset(svgPath, width: 16, height: 16);
  }
}

enum DetailMergeStatus {
  blockedStatus('BLOCKED_STATUS', approvable: false, mergeBlocked: true),
  brokenStatus('BROKEN_STATUS', approvable: false, mergeBlocked: true),
  checking('CHECKING', approvable: false, mergeBlocked: true),
  ciMustPass('CI_MUST_PASS', approvable: false, mergeBlocked: true),
  ciStillRunning('CI_STILL_RUNNING', approvable: false, mergeBlocked: false),
  discussionsNotResolved('DISCUSSIONS_NOT_RESOLVED', approvable: false, mergeBlocked: true),
  draftStatus('DRAFT_STATUS', approvable: true, mergeBlocked: true),
  externalStatusChecks('EXTERNAL_STATUS_CHECKS', approvable: false, mergeBlocked: true),
  mergeable('MERGEABLE', approvable: true, mergeBlocked: true),
  notApproved('NOT_APPROVED', approvable: true, mergeBlocked: true),
  notOpen('NOT_OPEN', approvable: false, mergeBlocked: true),
  policiesDenied('POLICIES_DENIED', approvable: false, mergeBlocked: true),
  unchecked('UNCHECKED', approvable: true, mergeBlocked: true);

  final String label;
  final bool approvable;
  final bool mergeBlocked;

  const DetailMergeStatus(this.label, {required this.approvable, required this.mergeBlocked});

  static DetailMergeStatus valueOf(String status) {
    var matches = DetailMergeStatus.values.where((e) => e.label == status);
    return matches.isNotEmpty ? matches.first : DetailMergeStatus.unchecked;
  }

  static Map<DetailMergeStatus, Function> _statusAndDescriptionMap = {
    DetailMergeStatus.blockedStatus: () => AppLocalizations.dictionary().blockedStatusDescription,
    DetailMergeStatus.brokenStatus: () => AppLocalizations.dictionary().brokenStatusDescription,
    DetailMergeStatus.checking: () => AppLocalizations.dictionary().checkingDescription,
    DetailMergeStatus.ciMustPass: () => AppLocalizations.dictionary().ciMustPassDescription,
    DetailMergeStatus.ciStillRunning: () => AppLocalizations.dictionary().ciStillRunningDescription,
    DetailMergeStatus.discussionsNotResolved: () => AppLocalizations.dictionary().discussionsNotResolvedDescription,
    DetailMergeStatus.draftStatus: () => AppLocalizations.dictionary().draftStatusDescription,
    DetailMergeStatus.externalStatusChecks: () => AppLocalizations.dictionary().externalStatusChecksDescription,
    DetailMergeStatus.notApproved: () => AppLocalizations.dictionary().notApprovedDescription,
    DetailMergeStatus.notOpen: () => AppLocalizations.dictionary().notOpenDescription,
    DetailMergeStatus.policiesDenied: () => AppLocalizations.dictionary().policiesDeniedDescription,
    DetailMergeStatus.unchecked: () => AppLocalizations.dictionary().uncheckedDescription,
  };

  String get description => _statusAndDescriptionMap[this]?.call() ?? "";
}
