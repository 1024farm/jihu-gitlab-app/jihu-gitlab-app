import 'package:jihu_gitlab_app/core/connection_provider/connection.dart';
import 'package:jihu_gitlab_app/core/time.dart';

class Commits {
  final List<Commit> _items;

  Commits._(this._items);

  factory Commits.fromJson(body) {
    return Commits._((body as List<dynamic>).map((o) => Commit.fromJson(o)).toList());
  }

  factory Commits.fromList(List<Commit> list) {
    return Commits._(list);
  }

  String get latestShortId => _items.isEmpty ? ' ' : _items.first.shortId;

  bool get isNotEmpty => _items.isNotEmpty;

  List<Commit> get items => _items;
}

class Commit {
  final String id;
  final String shortId;
  final String title;
  final Time createdAt;
  final User? author;

  Commit._(this.id, this.shortId, this.title, this.createdAt, this.author);

  factory Commit.fromJson(body) {
    var author = body['author'];
    User? user = author == null ? null : User.fromGraphQLJson(author);
    return Commit._(body['id'] ?? '', body['shortId'] ?? '', body['title'] ?? '', Time.init(body['authoredDate'] ?? ''), user);
  }
}
