class CodeQualityReportSummary {
  int count;

  CodeQualityReportSummary._(this.count);

  static CodeQualityReportSummary? fromJson(body) {
    if (body == null) return null;
    return CodeQualityReportSummary._(body['count'] ?? 0);
  }
}
