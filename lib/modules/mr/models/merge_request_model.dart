import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';
import 'package:jihu_gitlab_app/core/system_version_detector.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/mr/changes_view.dart';
import 'package:jihu_gitlab_app/modules/mr/models/approval.dart';
import 'package:jihu_gitlab_app/modules/mr/models/commits.dart';
import 'package:jihu_gitlab_app/modules/mr/models/diff.dart';
import 'package:jihu_gitlab_app/modules/mr/models/jobs.dart';
import 'package:jihu_gitlab_app/modules/mr/models/merge_request.dart';
import 'package:jihu_gitlab_app/modules/mr/models/mr_graphql_request_body.dart';

class MergeRequestModel {
  static GlobalKey<ChangesViewState> pageKey = GlobalKey();

  int _tabLength = 2;

  late int projectId;
  late String projectName;
  late String projectFullPath;
  late int mergeRequestIid;

  late TabController _tabController;
  late TickerProvider _tickerProvider;
  MergeRequest? mr;
  SystemType systemType = SystemType.latest;

  Approval? approval;
  List<Diff> _diffs = [];
  LoadState _loadState = LoadState.noItemState;

  void init(Map<dynamic, dynamic> arguments, TickerProvider tickerProvider) {
    projectId = arguments['projectId'];
    projectName = arguments['projectName'];
    projectFullPath = arguments['fullPath'];
    mergeRequestIid = arguments['mergeRequestIid'];
    _tickerProvider = tickerProvider;
    _tabController = TabController(initialIndex: 0, length: _tabLength, vsync: _tickerProvider);
  }

  Future<void> onRefresh(void Function() setState) async {
    _getApproval(setState);
    await _getBasicMr(setState);
    _getJobs(setState);
    _getMrForPaid(setState);
    refreshCommits(setState);
    refreshDiffs(setState);
  }

  Future<bool> isRebaseInProgress() async {
    var resp = await ProjectRequestSender.instance().post(Api.graphql(), queryRebaseInProgressGraphQLRequestBody(projectFullPath, mergeRequestIid));
    return resp.body()['data']?['project']?['mergeRequest']?['rebaseInProgress'];
  }

  Future<void> refreshDiffs(void Function()? setState) async {
    try {
      _diffs = await _getDiffs();
      _tabLength = 3;
      _tabController = TabController(initialIndex: 0, length: _tabLength, vsync: _tickerProvider);
      setState?.call();
    } catch (e) {
      LogHelper.err("Failed to getDiffs of merge request: ", e);
    }
  }

  Future<List<Diff>> _getDiffs() async {
    int page = 1;
    int pageSize = 20;
    bool hasNextPage = true;
    List<Diff> allDiffs = [];
    try {
      do {
        var resp = await ProjectRequestSender.instance().get<List<dynamic>>(Api.join('/projects/$projectId/merge_requests/$mergeRequestIid/diffs?page=$page&per_page=$pageSize'));
        var list = resp.body().map((e) => Diff.fromJson(e)).toList();
        allDiffs.addAll(list);
        hasNextPage = list.length >= pageSize;
        page++;
      } while (hasNextPage);
      return allDiffs;
    } catch (e) {
      LogHelper.err('_getDiffs error', e);
      return [];
    }
  }

  Future<void> refreshCommits(void Function()? setState) async {
    try {
      mr?.updateCommits(await _getCommits());
      setState?.call();
    } catch (e) {
      LogHelper.err("Failed to getCommits of merge request: ", e);
    }
  }

  Future<List<Commit>> _getCommits() async {
    String endCursor = '';
    bool hasNextPage = true;
    List<Commit> allCommits = [];
    try {
      do {
        var response = await ProjectRequestSender.instance().post(Api.graphql(), queryCommitsGraphQLRequestBody(projectFullPath, mergeRequestIid, endCursor));
        var data = response.body()["data"]?['project']?['mergeRequest']?['commits'];
        hasNextPage = data?['pageInfo']?['hasNextPage'] ?? false;
        endCursor = data?['pageInfo']?['endCursor'] ?? '';
        List<Commit> commits = ((data?['nodes'] ?? []) as List).map((e) => Commit.fromJson(e)).toList();
        allCommits.addAll(commits);
      } while (hasNextPage);
      return allCommits;
    } catch (e) {
      LogHelper.err('_getCommits error', e);
      return [];
    }
  }

  Future<void> _getBasicMr(void Function()? setState) {
    return _makeRefresh(() async {
      var resp = await ProjectRequestSender.instance().post(Api.graphql(), getMergeRequestDetailsGraphQLRequestBody(projectFullPath, mergeRequestIid));
      mr = MergeRequest.fromJson(resp.body()['data']['project']);
    }, setState);
  }

  Future<void> _getMrForPaid(void Function()? setState) async {
    try {
      var resp = await ProjectRequestSender.instance().post(Api.graphql(), getPaidMergeRequestDetailsGraphQLRequestBody(projectFullPath, mergeRequestIid));
      var project = resp.body()['data']['project'];
      mr?.paidDataFromJson(project);
      setState?.call();
    } catch (e) {
      LogHelper.err("Failed to _getMrForPaid: ", e);
    }
  }

  Future<void> _getJobs(void Function()? setState) async {
    try {
      mr?.jobsFromResp(await _fetchJobs());
      setState?.call();
    } catch (e) {
      LogHelper.err("Failed to _getJobs", e);
    }
  }

  Future<List<Job>> _fetchJobs() async {
    String endCursor = '';
    bool hasNextPage = true;
    List<Job> allJobs = [];
    try {
      do {
        var response = await ProjectRequestSender.instance().post(Api.graphql(), queryJobsGraphQLRequestBody(projectFullPath, mergeRequestIid, endCursor));
        var data = response.body()["data"]?['project']?['mergeRequest']?['headPipeline']?['jobs'];
        hasNextPage = data?['pageInfo']?['hasNextPage'] ?? false;
        endCursor = data?['pageInfo']?['endCursor'] ?? '';
        List<Job> jobs = ((data?['nodes'] ?? []) as List).map((e) => Job.fromJson(e)).toList();
        allJobs.addAll(jobs);
      } while (hasNextPage);
      return allJobs;
    } catch (e) {
      LogHelper.err('_getCommits error', e);
      return [];
    }
  }

  Future<void> _makeRefresh(Future<void> Function() function, void Function()? setState) async {
    try {
      await function();
      setState?.call();
    } catch (e) {
      LogHelper.err("Failed to refresh merge request: ", e);
      _loadState = LoadState.errorState;
    }
  }

  Future<void> _getApproval(void Function() setState) async {
    _makeRefresh(() async {
      var resp = await ProjectRequestSender.instance().get<Map<String, dynamic>>(Api.join('/projects/$projectId/merge_requests/$mergeRequestIid/approvals'));
      approval = Approval.fromJson(resp.body());
    }, setState);
  }

  Future<bool> approve() async {
    try {
      await ProjectRequestSender.instance().post(Api.join('/projects/$projectId/merge_requests/$mergeRequestIid/approve'), {});
      return true;
    } catch (e) {
      LogHelper.err("Failed to approve merge request[$mergeRequestIid]", e);
      return false;
    }
  }

  Future<bool> revokeApproval() async {
    try {
      await ProjectRequestSender.instance().post(Api.join('/projects/$projectId/merge_requests/$mergeRequestIid/unapprove'), {});
      return true;
    } catch (e) {
      LogHelper.err("Failed to unapproval merge request[$mergeRequestIid]", e);
      return false;
    }
  }

  List<String> get tabTitles => <String>[AppLocalizations.dictionary().overview, AppLocalizations.dictionary().commits, AppLocalizations.dictionary().changes].sublist(0, _tabLength);

  String get pageTitle => '$projectName !$mergeRequestIid';

  bool get canBeApproved => (approval?.userCanApprove ?? false) && !(approval?.userHasApproved ?? false) && mr!.detailedMergeStatus.approvable && mr!.state == MergeRequestState.opened;

  bool get canBeRevokedApproval => (approval?.userHasApproved ?? false) && mr!.state == MergeRequestState.opened;

  bool get approved => approval?.approved ?? false;

  bool get isOptional => approval!.approvalsRequired == 0;

  bool get approvalVisible => mr != null && mr!.commitCount > 0 && approval != null && !isDraft;

  int get approvalsLeft => approval?.approvalsLeft ?? 0;

  List<String> get approvalRulesLeft => approval?.approvalRulesLeft ?? [];

  LoadState get loadState => _loadState;

  bool get isDraft => mr != null && (mr!.detailedMergeStatus == DetailMergeStatus.draftStatus || mr!.draft);

  bool get mergeBlockedVisible => mr != null && mr!.state == MergeRequestState.opened && !mr!.mergeable && mr!.detailedMergeStatus.mergeBlocked && mr!.commitCount > 0;

  bool get mergeReadyVisible => mr != null && (mr!.mergeable || mr!.detailedMergeStatus == DetailMergeStatus.ciStillRunning) && mr!.commitCount > 0;

  TabController get tabController => _tabController;

  bool get mergedInfoVisible => mr != null && mr!.state == MergeRequestState.merged;

  List<Diff> get diffs => _diffs;
}
