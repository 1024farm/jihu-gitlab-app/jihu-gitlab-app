import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/id.dart';
import 'package:jihu_gitlab_app/core/time.dart';
import 'package:jihu_gitlab_app/modules/mr/models/pipeline_detailed_status.dart';

class Pipeline {
  final int id;
  final int iid;
  final PipelineStatus status;
  final String ref;
  final String? coverage;
  final PipelineDetailedStatus detailedStatus;
  final Time finishedAt;

  Pipeline._(this.id, this.iid, this.status, this.ref, this.coverage, this.detailedStatus, this.finishedAt);

  factory Pipeline.fromJson(body) {
    return Pipeline._(Id.fromGid(body['id'] ?? '/0').id, int.parse((body['iid'] ?? '0')), PipelineStatus.valueOf(body['status'] ?? ''), body['ref'] ?? '', '${body['coverage'] ?? 0}',
        PipelineDetailedStatus.fromJson(body['detailedStatus'] ?? {}), Time.init(body['finishedAt'] ?? body['startedAt'] ?? body['committedAt'] ?? ''));
  }

  SvgPicture statusToSvg() {
    return status.toSvg(detailedStatus);
  }
}

enum PipelineStatus {
  failed('FAILED', 'assets/images/pipeline_failed.svg'),
  running('RUNNING', 'assets/images/pipeline_running.svg'),
  canceled('CANCELED', 'assets/images/pipeline_canceled.svg'),
  success('SUCCESS', 'assets/images/pipeline_success.svg');

  final String label;
  final String svgPath;

  const PipelineStatus(this.label, this.svgPath);

  static PipelineStatus valueOf(String status) {
    var matches = PipelineStatus.values.where((e) => e.label == status);
    return matches.isNotEmpty ? matches.first : PipelineStatus.success;
  }

  SvgPicture toSvg(PipelineDetailedStatus detailedStatus) => SvgPicture.asset(
        detailedStatus.label.contains('warning') ? 'assets/images/warning.svg' : svgPath,
        width: 20,
        height: 20,
      );
}
