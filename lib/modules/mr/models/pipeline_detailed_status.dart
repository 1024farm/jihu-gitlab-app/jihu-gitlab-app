import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class PipelineDetailedStatus {
  final String _label;

  PipelineDetailedStatus._(this._label);

  factory PipelineDetailedStatus.fromJson(body) {
    return PipelineDetailedStatus._(body['label'] ?? '');
  }

  String get translatedLabel => translateMap.containsKey(_label) ? translateMap[_label]() : '';

  String get label => _label;

  static Map translateMap = {
    "passed with warnings": () => AppLocalizations.dictionary().pipelinePassedWithWarnings,
    "running": () => AppLocalizations.dictionary().pipelineRunning,
    "canceled": () => AppLocalizations.dictionary().pipelineCanceled,
    "failed": () => AppLocalizations.dictionary().pipelineFailed,
    "passed": () => AppLocalizations.dictionary().pipelinePassed
  };
}
