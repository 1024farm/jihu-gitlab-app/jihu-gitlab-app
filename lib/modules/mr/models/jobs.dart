import 'package:jihu_gitlab_app/core/id.dart';

class Jobs {
  final List<Stage> stages;

  Jobs._(this.stages);

  factory Jobs.fromJson(List<Map<String, dynamic>> body) {
    var jobsInput = body.map((e) => Job.fromJson(e)).toList().reversed.toList();
    List<Stage> stages = [];
    for (var o in jobsInput) {
      if (stages.where((stage) => stage.stageName == o.stage).isEmpty) {
        stages.add(Stage(o.stage, [o]));
      } else {
        stages.where((stage) => stage.stageName == o.stage).first.jobs.add(o);
      }
    }
    return Jobs._(stages);
  }

  factory Jobs.fromList(List<Job> list) {
    List<Stage> stages = [];
    for (var o in list) {
      if (stages.where((stage) => stage.stageName == o.stage).isEmpty) {
        stages.add(Stage(o.stage, [o]));
      } else {
        stages.where((stage) => stage.stageName == o.stage).first.jobs.add(o);
      }
    }
    return Jobs._(stages);
  }

  bool get isEmpty => stages.isEmpty;
}

class Stage {
  final String stageName;
  final List<Job> jobs;

  Stage(this.stageName, this.jobs);

  String get stateIconAssetName {
    if (jobsHasAnyFailed()) return 'assets/images/pipeline_failed.svg';
    if (jobPassedWithWarnings()) return 'assets/images/warning.svg';
    if (jobsAreAllSuccess()) return 'assets/images/pipeline_success.svg';
    if (jobsMustAndOthersCan(JobState.created, [JobState.success, JobState.canceled])) return 'assets/images/pipeline_created.svg';
    if (jobsMustAndOthersCan(JobState.pending, [JobState.created, JobState.success, JobState.canceled])) return 'assets/images/pipeline_pending.svg';
    if (jobsMustAndOthersCan(JobState.running, [JobState.pending, JobState.created, JobState.success, JobState.canceled])) return 'assets/images/pipeline_running.svg';
    if (jobsAreCanceled()) return 'assets/images/pipeline_canceled.svg';
    return 'assets/images/pipeline_unknown.svg';
  }

  bool jobsAreAllSuccess() => jobs.where((job) => !(job.status == JobState.success)).isEmpty;

  bool jobPassedWithWarnings() {
    if (jobs.where((job) => (job.status == JobState.failed && job.allowFailure == true) || job.status == JobState.canceled).isEmpty) return false;
    if (jobs.where((job) => !(job.status == JobState.canceled)).isEmpty) return false;
    return jobs
        .where((job) => !(job.status == JobState.failed && job.allowFailure == true))
        .where((job) => !(job.status == JobState.success))
        .where((job) => !(job.status == JobState.canceled))
        .isEmpty;
  }

  bool jobsHasAnyFailed() => jobs.any((job) => job.status == JobState.failed && job.allowFailure == false);

  bool jobsAreCanceled() {
    return jobs.where((job) => !(job.status == JobState.canceled)).isEmpty;
  }

  bool jobsMustAndOthersCan(JobState must, List<JobState> cans) {
    if (jobs.where((job) => job.status == must).isEmpty) return false;
    return jobs.where((job) => !(job.status == must)).where((job) => !([must, ...cans].contains(job.status))).where((job) => !(job.status == JobState.failed && job.allowFailure == true)).isEmpty;
  }
}

class Job {
  final int id;
  final JobState status;
  final String stage;
  final String name;
  final bool allowFailure;

  Job._(this.id, this.status, this.stage, this.name, this.allowFailure);

  factory Job.fromJson(body) {
    return Job._(Id.fromGid(body['id'] ?? '/0').id, JobState.valueOf(body['status'] ?? ''), (body['stage'] ?? {})['name'] ?? '', body['name'] ?? '', body['allowFailure'] ?? false);
  }
}

enum JobState {
  created('CREATED'),
  success('SUCCESS'),
  pending('PENDING'),
  failed('FAILED'),
  running('RUNNING'),
  canceled('CANCELED'),
  unknown('UNKNOWN');

  final String graphQLLabel;

  const JobState(this.graphQLLabel);

  static JobState valueOf(String status) {
    var matches = JobState.values.where((e) => e.graphQLLabel == status);
    return matches.isNotEmpty ? matches.first : JobState.unknown;
  }
}
