import 'package:jihu_gitlab_app/core/id.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection.dart';

class ApprovalRule {
  int id;
  String name;
  String ruleType;
  int approvalsRequired;
  List<User> approvedBy;
  List<User> eligibleApprovers;

  ApprovalRule._internal(this.id, this.name, this.ruleType, this.approvalsRequired, this.approvedBy, this.eligibleApprovers);

  factory ApprovalRule.fromGraphQLJson(Map<String, dynamic> json) {
    List<User> eligibleApprovers = [];
    var eligibleApproversJson = json['eligibleApprovers'];
    if (eligibleApproversJson != null) {
      eligibleApprovers = (eligibleApproversJson as List).map((e) => User.fromGraphQLJson(e)).toList();
    }
    List<User> approvedBy = [];
    var approvedByJson = json['approvedBy'];
    if (approvedByJson != null) {
      approvedBy = (approvedByJson['nodes'] as List).map((e) => User.fromGraphQLJson(e)).toList();
    }
    var result = ApprovalRule._internal(Id.fromGid(json['id'] ?? '/0').id, json['name'] ?? '', json['type'] ?? '', json['approvalsRequired'] ?? 0, approvedBy, eligibleApprovers);
    return result;
  }

  bool get isOptional => approvalsRequired < 1;
}

class Approval {
  bool approved;
  final bool userCanApprove;
  final bool userHasApproved;
  final int approvalsLeft;
  final int approvalsRequired;
  List<String> approvalRulesLeft;

  Approval._internal(this.approved, this.userCanApprove, this.userHasApproved, this.approvalsLeft, this.approvalsRequired, this.approvalRulesLeft);

  factory Approval.fromJson(Map<String, dynamic> json) {
    return Approval._internal(json['approved'] ?? false, json['user_can_approve'] ?? false, json['user_has_approved'] ?? false, json['approvals_left'] ?? 0, json['approvals_required'] ?? 0,
        ((json['approval_rules_left'] ?? []) as List).map((e) => (e['name'] ?? '') as String).toList());
  }
}
