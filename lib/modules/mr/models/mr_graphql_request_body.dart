Map<String, dynamic> getMergeRequestDetailsGraphQLRequestBody(String fullPath, int mrIid) {
  return {
    "operationName": "getMergeRequest",
    "variables": {"fullPath": fullPath, "mrIid": '$mrIid'},
    "query": """
        query getMergeRequest(\$fullPath: ID!, \$mrIid: String!) {
          project(fullPath: \$fullPath) {
            id
            name
            nameWithNamespace
            path
            fullPath
            webUrl
            allowMergeOnSkippedPipeline
            onlyAllowMergeIfAllDiscussionsAreResolved
            onlyAllowMergeIfPipelineSucceeds
            removeSourceBranchAfterMerge
            mergeRequestsFfOnlyEnabled
            squashReadOnly
            mergeRequest(iid: \$mrIid) {
              id
              iid
              state
              title
              description
              webUrl
              draft
              createdAt
              rebaseInProgress
              allowCollaboration
              approvedBy {
                nodes {
                  ...UserCoreFragment
                }
              }
              assignees {
                nodes {
                  id
                  name
                }
              }
              commitCount
              commits {
                nodes {
                  id
                  shortId
                  sha
                  title
                  description
                }
              }
              author {
                id
                name
                username
                avatarUrl
              }
              mergeUser {
                id
                name
                username
                avatarUrl
              }
              mergedAt
              autoMergeEnabled
              autoMergeStrategy
              availableAutoMergeStrategies
              conflicts
              mergeError
              mergeOngoing
              mergeStatusEnum
              mergeUser {
                ...UserCoreFragment
              }
              mergeWhenPipelineSucceeds
              mergeable
              mergeableDiscussionsState
              mergedAt
              rebaseCommitSha
              rebaseInProgress
              shouldBeRebased
              shouldRemoveSourceBranch
              sourceBranch
              sourceBranchExists
              sourceBranchProtected
              targetBranch
              targetBranchExists
              squash
              squashOnMerge
              forceRemoveSourceBranch
              defaultMergeCommitMessage
              defaultSquashCommitMessage
              headPipeline {
                id
                iid
                active
                cancelable
                complete
                coverage
                status
                ref
                detailedStatus {
                  icon
                  label
                }
                jobs {
                  nodes {
                    id
                    name
                    active
                    status
                    allowFailure
                    duration
                    startedAt
                    finishedAt
                    stage {
                      id
                      name
                      status
                      jobs {
                        nodes {
                          id
                          name
                          status
                          allowFailure
                        }
                      }
                    }
                  }
                }
                warnings
                warningMessages {
                  content
                }
                createdAt
                finishedAt
                committedAt
                startedAt
              }
              userPermissions {
                adminMergeRequest
                canMerge
                pushToSourceBranch
                readMergeRequest
                removeSourceBranch
              }
            }
          }
        }
        
        fragment UserCoreFragment on UserCore {
          id
          username
          name
          state
          avatarUrl
          webUrl
          publicEmail
        }
    """
  };
}

Map<String, dynamic> getPaidMergeRequestDetailsGraphQLRequestBody(String fullPath, int mrIid) {
  return {
    "query": """
       {
          project(fullPath: "$fullPath") {
            id
            onlyAllowMergeIfAllStatusChecksPassed
            mergeRequest(iid: "$mrIid") {
              id
              iid
              approved
              approvalsLeft
              approvalsRequired
              approvalState {
                approvalRulesOverwritten
                rules {
                  id
                  name
                  type
                  approvalsRequired
                  approvedBy {
                    nodes {
                      ...UserCoreFragment
                    }
                  }
                  eligibleApprovers {
                    ...UserCoreFragment
                  }
                }
              }
              detailedMergeStatus
              headPipeline {
                id
                iid
                codeQualityReportSummary {
                  count
                }
                codeQualityReports {
                  nodes {
                    line
                    path
                    description
                    severity
                  }
                }
              }
            }
          }
       }
       fragment UserCoreFragment on UserCore {
          id
          username
          name
          state
          avatarUrl
          webUrl
          publicEmail
       }
       """
  };
}

Map<String, dynamic> queryRebaseInProgressGraphQLRequestBody(String fullPath, int mrIid) {
  return {
    "query": """
      {
        project(fullPath: "$fullPath") {
          mergeRequest(iid: "$mrIid") {
            rebaseInProgress
          }
        }
      }
    """
  };
}

Map<String, dynamic> queryCommitsGraphQLRequestBody(String fullPath, int mrIid, String endCursor) {
  return {
    "query": """
      query getCommits{
        project(fullPath: "$fullPath") {
          mergeRequest(iid: "$mrIid") {
            commits(after: "$endCursor"){
              pageInfo{
                hasNextPage
                endCursor
              }
              nodes{
                id
                shortId
                sha
                title
                description
                authoredDate
                author{
                  name
                  avatarUrl
                }
              }
            }
          }
        }
      }
    """
  };
}

Map<String, dynamic> queryJobsGraphQLRequestBody(String fullPath, int mrIid, String endCursor) {
  return {
    "query": """
      query getCommits{
        project(fullPath: "$fullPath") {
          mergeRequest(iid: "$mrIid") {
            headPipeline {
              jobs(first: 100, after: "$endCursor") {
                pageInfo {
                  hasNextPage
                  endCursor
                }
                nodes {
                  id
                  name
                  active
                  status
                  allowFailure
                  duration
                  startedAt
                  finishedAt
                  stage {
                    id
                    name
                    status
                    jobs {
                      nodes {
                        id
                        name
                        status
                        allowFailure
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    """
  };
}
