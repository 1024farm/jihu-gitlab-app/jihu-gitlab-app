class Diff {
  final String diff;
  final bool newFile;
  final bool renamedFile;
  final bool deletedFile;
  final String newPath;
  final String oldPath;

  Diff._internal(this.diff, this.newFile, this.renamedFile, this.deletedFile, this.newPath, this.oldPath);

  factory Diff.fromJson(Map<String, dynamic> json) {
    return Diff._internal(
      json['diff'] ?? '',
      json['new_file'] ?? false,
      json['renamed_file'] ?? false,
      json['deleted_file'] ?? false,
      json['new_path'] ?? '',
      json['old_path'] ?? '',
    );
  }
}
