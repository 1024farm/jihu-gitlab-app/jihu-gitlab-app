import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_button.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/mr/models/merge_request.dart';

typedef VoidFutureCallBack = Future<void> Function();
typedef BoolFutureCallBack = Future<bool> Function();

class MergeBlockedView extends StatefulWidget {
  final MergeRequest mergeRequest;
  final VoidFutureCallBack onMarkedAsReady;
  final BoolFutureCallBack? rebaseStatusLooper;
  final VoidFutureCallBack? onRebaseFinished;

  const MergeBlockedView({required this.mergeRequest, required this.onMarkedAsReady, this.onRebaseFinished, this.rebaseStatusLooper, Key? key}) : super(key: key);

  @override
  State<MergeBlockedView> createState() => _MergeBlockedViewState();
}

class _MergeBlockedViewState extends State<MergeBlockedView> {
  bool _isLoading = false;
  bool _inCheckRebaseStatusLoop = false;
  bool _showMergeError = false;
  String? _mergeError;

  @override
  Widget build(BuildContext context) {
    if (_mergeError != _mr.mergeError) {
      _mergeError = _mr.mergeError;
      _showMergeError = _mr.mergeError != null;
    }
    return Column(
      children: [
        if (_showMergeError)
          Container(
            key: const Key('MergeErrorMessage'),
            padding: const EdgeInsets.only(top: 4, bottom: 4),
            margin: const EdgeInsets.only(bottom: 14),
            decoration: BoxDecoration(color: Theme.of(context).primaryColor.withAlpha(60), borderRadius: const BorderRadius.all(Radius.circular(4))),
            child: Row(
              children: [
                Padding(padding: const EdgeInsets.all(8), child: SvgPicture.asset("assets/images/warning.svg", height: 20, width: 20)),
                Expanded(
                    child: Text(
                  '$_mergeErrorMessage Try again.',
                  style: const TextStyle(fontSize: 12, color: Colors.black87, fontWeight: FontWeight.w400),
                )),
                InkWell(
                  key: const Key('MergeErrorCloseButton'),
                  onTap: () => setState(() => _showMergeError = false),
                  child: const SizedBox(width: 44, height: 44, child: Icon(Icons.close, size: 16)),
                )
              ],
            ),
          ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            _inCheckRebaseStatusLoop
                ? SizedBox(width: 20, height: 20, child: CircularProgressIndicator(color: Theme.of(context).primaryColor, strokeWidth: 1))
                : SvgPicture.asset(_mergeBlockedIcon, width: 20, height: 20),
            const SizedBox(width: 8),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    _title,
                    style: const TextStyle(color: Color(0xFF1A1B36), fontSize: 15, fontWeight: FontWeight.w600),
                  ),
                  const SizedBox(height: 4),
                  Text(
                    _description,
                    style: const TextStyle(color: Color(0xFF03162F), fontSize: 14, fontWeight: FontWeight.w400),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  if (_isDraft && ProjectProvider().isSpecifiedHostHasConnection && !ProjectProvider().archived)
                    LoadingButton.asOutlinedButton(
                      onPressed: _markAsReady,
                      text: Text(AppLocalizations.dictionary().markAsReady, style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 14, fontWeight: FontWeight.w500)),
                      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                      isLoading: _isLoading,
                    ),
                  if (_shouldShowRebase && ProjectProvider().isSpecifiedHostHasConnection && !ProjectProvider().archived)
                    LoadingButton.asOutlinedButton(
                      onPressed: _rebase,
                      text: Text(AppLocalizations.dictionary().rebase, style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 14, fontWeight: FontWeight.w500)),
                      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                      isLoading: _inCheckRebaseStatusLoop,
                    ),
                ],
              ),
            )
          ],
        )
      ],
    );
  }

  void _markAsReady() async {
    setState(() => _isLoading = true);
    await HttpClient.instance().post(Api.graphql(), {
      "query": """
        mutation{
          mergeRequestSetDraft(input:{
            projectPath:"${_mr.fullPath}", 
            iid:"${_mr.iid}", 
            draft:false
          }){
            mergeRequest{
              draft
            }
         }
      }
      """
    });
    try {
      await widget.onMarkedAsReady();
    } catch (e) {
      LogHelper.err('Merge Request mark as ready error', e);
    } finally {
      setState(() => _isLoading = false);
    }
  }

  Future<void> _rebase({bool skipCi = false}) async {
    setState(() => _inCheckRebaseStatusLoop = true);
    try {
      await HttpClient.instance().put(Api.join("projects/${_mr.projectId}/merge_requests/${_mr.iid}/rebase?skip_ci=$skipCi"), null);
      _loopCheckRebaseStatus();
    } catch (e) {
      LogHelper.err('Merge Request rebase error', e);
      setState(() => _inCheckRebaseStatusLoop = false);
    }
  }

  Future<void> _loopCheckRebaseStatus() async {
    while (_inCheckRebaseStatusLoop) {
      _inCheckRebaseStatusLoop = await widget.rebaseStatusLooper?.call() ?? true;
      if (_inCheckRebaseStatusLoop) await Future.delayed(const Duration(seconds: 3), () {});
    }
    widget.onRebaseFinished?.call();
    setState(() {});
  }

  bool _isStatus(DetailMergeStatus status) {
    return widget.mergeRequest.detailedMergeStatus == status;
  }

  MergeRequest get _mr => widget.mergeRequest;

  String get _title => AppLocalizations.dictionary().mergeBlocked;

  String get _description {
    if (_shouldShowRebase) {
      return AppLocalizations.dictionary().shouldBeRebased;
    }
    if (_mr.hasConflicts) {
      return _mr.shouldBeRebased ? AppLocalizations.dictionary().shouldBeRebasedLocally : AppLocalizations.dictionary().shouldResolveConflicts;
    }
    if (_mr.draft) {
      return AppLocalizations.dictionary().draftStatusDescription;
    }
    return _mr.detailedMergeStatus.description;
  }

  String get _mergeErrorMessage => _mr.mergeError ?? '';

  bool get _isDraft => (_isStatus(DetailMergeStatus.draftStatus) || _mr.draft) && !_mr.hasConflicts;

  bool get _shouldShowRebase => (_isStatus(DetailMergeStatus.notApproved) || _isStatus(DetailMergeStatus.mergeable)) && _mr.shouldBeRebased && !_mr.hasConflicts;

  String get _mergeBlockedIcon => _shouldShowRebase ? "assets/images/mergeable.svg" : "assets/images/merge_blocked.svg";
}
