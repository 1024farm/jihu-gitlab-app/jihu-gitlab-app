import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/app_tab_bar.dart';

class MergeRequestTabBar extends StatelessWidget {
  const MergeRequestTabBar({required this.tabController, required this.tabBarTitles, super.key});

  final TabController tabController;
  final List<String> tabBarTitles;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.fromLTRB(6, 0, 16, 0),
        child: Column(
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: AppTabBar(
                controller: tabController,
                labelColor: const Color(0xFF171321),
                labelStyle: const TextStyle(fontSize: 15, fontWeight: FontWeight.w600),
                unselectedLabelColor: AppThemeData.secondaryColor,
                unselectedLabelStyle: const TextStyle(fontSize: 15, fontWeight: FontWeight.w300),
                isScrollable: true,
                tabs: tabBarTitles.map((item) => Tab(text: item)).toList(),
              ),
            ),
            const Divider(thickness: 1, height: 1, indent: 10, color: Color(0xFFEAEAEA)),
          ],
        ));
  }
}
