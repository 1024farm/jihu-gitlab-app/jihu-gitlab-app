import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/mr/changes/mr_changes_page.dart';
import 'package:jihu_gitlab_app/modules/mr/models/merge_request_model.dart';

class ChangesView extends StatefulWidget {
  const ChangesView({required this.model, super.key});

  final MergeRequestModel model;

  @override
  State<ChangesView> createState() => ChangesViewState();
}

class ChangesViewState extends State<ChangesView> {
  late MergeRequestModel _model;

  @override
  void initState() {
    _model = widget.model;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return (_model.diffs.isEmpty)
        ? Center(child: Text(AppLocalizations.dictionary().noChanges, style: Theme.of(context).textTheme.bodySmall))
        : ListView.separated(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _model.diffs.length,
            itemBuilder: (context, index) {
              var diff = _model.diffs[index];
              return ListTile(
                enableFeedback: false,
                dense: true,
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => MrChangesPage(diffs: _model.diffs, targetIndex: index), fullscreenDialog: true));
                },
                title: Text(diff.newPath, style: const TextStyle(fontSize: 12, color: Colors.black87, fontWeight: FontWeight.w400)),
                trailing: const Icon(Icons.keyboard_arrow_right, size: 20),
                contentPadding: const EdgeInsets.only(left: 16, right: 16),
                horizontalTitleGap: -16,
              );
            },
            separatorBuilder: (BuildContext context, int index) => const Divider(thickness: 1, height: 1, indent: 16, endIndent: 16, color: Color(0xFFEAEAEA)),
          );
  }
}
