import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/string_extension.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/multi_state_view.dart';
import 'package:jihu_gitlab_app/core/widgets/round_rect_container.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/mr/code_quality_report_model.dart';
import 'package:jihu_gitlab_app/modules/mr/code_quality_summary_view.dart';

class CodeQualityReportPage extends StatefulWidget {
  const CodeQualityReportPage({required this.fullPath, required this.mrIid, required this.degradedCount, Key? key}) : super(key: key);
  final String fullPath;
  final int mrIid;
  final int degradedCount;

  @override
  State<CodeQualityReportPage> createState() => _CodeQualityReportPageState();
}

class _CodeQualityReportPageState extends State<CodeQualityReportPage> {
  late CodeQualityReportModel _model;

  @override
  void initState() {
    _model = CodeQualityReportModel(widget.fullPath, widget.mrIid);
    super.initState();
    refresh();
  }

  Future<void> refresh() async {
    await _model.refresh();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CommonAppBar(showLeading: true, title: Text(AppLocalizations.dictionary().codeQuality)),
      body: MultiStateView(
        loadState: _model.loadState,
        onRetry: refresh,
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            children: [
              CodeQualitySummaryView(count: widget.degradedCount),
              const SizedBox(height: 16),
              Expanded(
                child: EasyRefresh(
                  controller: _model.refreshController,
                  onRefresh: () => _model.refresh().then((value) => setState(() {})),
                  onLoad: () => _model.loadMore().then((value) => setState(() {})),
                  child: ListView.separated(
                    itemBuilder: (context, index) {
                      var detail = _model.reportDetails[index];
                      return RoundRectContainer(
                        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 4, right: 8, left: 0, bottom: 4),
                              child: SvgPicture.asset('assets/images/code_quality_yellow_dot.svg', height: 8, width: 8),
                            ),
                            Expanded(child: detailContent(detail)),
                          ],
                        ),
                      );
                    },
                    separatorBuilder: (context, index) => const Divider(height: 12, color: Colors.transparent),
                    itemCount: _model.reportDetails.length,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Text detailContent(CodeQualityReportDetail detail) {
    return Text.rich(
      TextSpan(
        children: [
          TextSpan(text: '${detail.severity.capitalize} - '),
          TextSpan(text: '${detail.description} ${AppLocalizations.dictionary().inWord} '),
          TextSpan(text: '${detail.path.disableAutoWrap}:${detail.line.toString().disableAutoWrap}', style: const TextStyle(color: AppThemeData.secondaryColor)),
        ],
      ),
      style: const TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
    );
  }
}
