import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar_and_name.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_button.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_option_view.dart';
import 'package:jihu_gitlab_app/modules/mr/models/merge_request.dart';

typedef VoidFutureCallBack = Future<void> Function();

class MergeReadyView extends StatefulWidget {
  final MergeRequest mergeRequest;
  final VoidFutureCallBack onMergeFinished;

  const MergeReadyView({required this.mergeRequest, required this.onMergeFinished, Key? key}) : super(key: key);

  @override
  State<MergeReadyView> createState() => _MergeReadyViewState();
}

class _MergeReadyViewState extends State<MergeReadyView> {
  bool _isLoading = false;
  bool _deleteSourceBranch = false;
  bool _squashCommitsChecked = false;
  bool _editCommitMessageChecked = false;
  final TextEditingController _mergeCommitMessageInputController = TextEditingController();
  final TextEditingController _squashCommitMessageInputController = TextEditingController();
  bool _isLoopingMergeResult = false;
  String? _aiRecommendsSquashCommitMessage;

  @override
  void initState() {
    _initDataFromParent();
    super.initState();
  }

  @override
  void didUpdateWidget(covariant MergeReadyView oldWidget) {
    _initDataFromParent();
    super.didUpdateWidget(oldWidget);
  }

  void _initDataFromParent() {
    _squashCommitsChecked = _mr.squashOnMerge;
    if (!_mr.squashReadOnly) {
      _squashCommitsChecked = _mr.commitsHasDuplicates;
      _aiRecommendsSquashCommitMessage = _mr.commitsHasDuplicates ? AppLocalizations.dictionary().aiSquashRecommends : AppLocalizations.dictionary().aiSquashRecommendsNot;
    }
    _deleteSourceBranch = _mr.forceRemoveSourceBranch;
    _mergeCommitMessageInputController.text = widget.mergeRequest.defaultMergeCommitMessage;
    _squashCommitMessageInputController.text = widget.mergeRequest.defaultSquashCommitMessage;
    _isLoopingMergeResult = widget.mergeRequest.mergeOngoing;
  }

  @override
  Widget build(BuildContext context) {
    return _mr.mergeWhenPipelineSucceeds
        ? _buildAutoMergeInfo()
        : Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              _isLoopingMergeResult
                  ? const SizedBox(height: 18, width: 18, child: CircularProgressIndicator(color: Color(0xFFCECECE), strokeWidth: 2))
                  : SvgPicture.asset("assets/images/mergeable.svg", width: 20, height: 20),
              const SizedBox(width: 8),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(_isLoopingMergeResult ? AppLocalizations.dictionary().mergeInProgress : AppLocalizations.dictionary().readyToMerge,
                        style: const TextStyle(color: Color(0xFF1A1B36), fontSize: 15, fontWeight: FontWeight.w600)),
                    const SizedBox(height: 12),
                    if (ProjectProvider().isSpecifiedHostHasConnection && !ProjectProvider().archived)
                      Container(
                        width: double.infinity,
                        color: const Color(0xFFF5F5F5),
                        padding: const EdgeInsets.only(bottom: 14),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            MergeOptionView(
                              description: AppLocalizations.dictionary().deleteSourceBranch,
                              checked: _deleteSourceBranch,
                              disabled: _interactionDisabled,
                              onCheckChanged: (value) => {_deleteSourceBranch = value},
                            ),
                            if (_squashCommitsOptionVisible)
                              MergeOptionView(
                                  description: AppLocalizations.dictionary().squashCommits,
                                  checked: _squashCommitsChecked,
                                  disabled: _interactionDisabled || _mr.squashReadOnly,
                                  onCheckChanged: (value) => setState(() => _squashCommitsChecked = value)),
                            if (_aiRecommendsSquashCommitMessage != null)
                              Padding(
                                  padding: const EdgeInsets.only(left: 14),
                                  child: Text(_aiRecommendsSquashCommitMessage!,
                                      style: TextStyle(
                                        color: _mr.commitsHasDuplicates == _squashCommitsChecked ? AppThemeData.secondaryColor : AppThemeData.primaryColor,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                      ))),
                            if (_editCommitMessageOptionVisible)
                              MergeOptionView(
                                  description: AppLocalizations.dictionary().editCommitMessage,
                                  checked: _editCommitMessageChecked,
                                  disabled: _interactionDisabled,
                                  onCheckChanged: (value) {
                                    setState(() => _editCommitMessageChecked = value);
                                  }),
                            if (_squashCommitMessageInputVisible) _buildMessageInput(_squashCommitMessageInputController, AppLocalizations.dictionary().squashCommitMessage),
                            if (_squashCommitMessageInputVisible && _mergeCommitMessageInputVisible) const SizedBox(height: 8),
                            if (_mergeCommitMessageInputVisible) _buildMessageInput(_mergeCommitMessageInputController, AppLocalizations.dictionary().mergeCommitMessage),
                            if (hint != null)
                              Padding(
                                  padding: const EdgeInsets.only(top: 8, left: 14),
                                  child: Text(hint!, style: const TextStyle(color: AppThemeData.secondaryColor, fontSize: 13, fontWeight: FontWeight.w400)))
                          ],
                        ),
                      ),
                    const SizedBox(height: 12),
                    if (ProjectProvider().isSpecifiedHostHasConnection && !ProjectProvider().archived)
                      LoadingButton.asElevatedButton(
                        onPressed: _merge,
                        text: Text(autoMergeStrategiesAvailable ? AppLocalizations.dictionary().mergeWhenPipelineSucceeds : AppLocalizations.dictionary().merge,
                            style: const TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w500)),
                        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                        isLoading: _isLoading,
                        disabled: _interactionDisabled,
                      ),
                  ],
                ),
              )
            ],
          );
  }

  void _merge() async {
    Map<String, dynamic> body = {
      "merge_commit_message": _mergeCommitMessageInputController.text,
      "merge_when_pipeline_succeeds": autoMergeStrategiesAvailable,
      "should_remove_source_branch": _deleteSourceBranch,
      "squash_commit_message": _squashCommitMessageInputController.text,
      "squash": _squashCommitsChecked,
    };
    setState(() {
      _isLoading = true;
      _isLoopingMergeResult = true;
    });
    try {
      await HttpClient.instance().put(Api.join("/projects/${_mr.projectId}/merge_requests/${_mr.iid}/merge"), body);
      await _loopMergeResult();
      await widget.onMergeFinished();
    } catch (_) {
    } finally {
      _isLoading = false;
      _isLoopingMergeResult = false;
    }
  }

  Widget _buildMessageInput(TextEditingController controller, String title) {
    return Container(
      key: Key(title),
      padding: const EdgeInsets.symmetric(horizontal: 14),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title, style: const TextStyle(color: Color(0xFF03162F), fontSize: 14, fontWeight: FontWeight.w500)),
          const SizedBox(height: 8),
          TextField(
            enabled: !_interactionDisabled,
            minLines: 1,
            maxLines: null,
            controller: controller,
            style: const TextStyle(color: Color(0xFF03162F), fontSize: 13, fontWeight: FontWeight.w400),
            decoration: InputDecoration(
              border: OutlineInputBorder(borderSide: const BorderSide(color: Color(0xFFCECECE)), borderRadius: BorderRadius.circular(4)),
              focusedBorder: OutlineInputBorder(borderSide: const BorderSide(color: Color(0xFFCECECE)), borderRadius: BorderRadius.circular(4)),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _loopMergeResult() async {
    await Future.doWhile(() async {
      _isLoopingMergeResult = await _getMergeResult();
      if (_isLoopingMergeResult) {
        await Future.delayed(const Duration(seconds: 3));
        return true;
      }
      return false;
    });
  }

  Future<bool> _getMergeResult() async {
    Response<Map<String, dynamic>> resp = await HttpClient.instance().post(Api.graphql(), {
      "query": """
      query {
        project(fullPath: "${_mr.fullPath}") {
          mergeRequest(iid: "${_mr.iid}") {
            mergeOngoing
          }
        }
      }
    """
    });
    return resp.body()['data']?['project']?['mergeRequest']?['mergeOngoing'] ?? false;
  }

  Widget _buildAutoMergeInfo() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 3),
          child: SvgPicture.asset("assets/images/auto_merge.svg", height: 18, width: 18),
        ),
        const SizedBox(width: 8),
        Expanded(
          child: RichText(
              text: TextSpan(children: [
            TextSpan(text: "${AppLocalizations.dictionary().setBy} ", style: const TextStyle(color: Color(0xFF1A1B36), fontSize: 15, fontWeight: FontWeight.w600)),
            WidgetSpan(alignment: PlaceholderAlignment.middle, child: AvatarAndName(username: _mr.mergeUser?.name ?? '', avatarUrl: _mr.mergeUser?.avatarUrl ?? '', avatarSize: 24, gap: 4)),
            TextSpan(text: AppLocalizations.dictionary().toBeMergedAutomaticallyWhenThePipelineSucceeds, style: const TextStyle(color: Color(0xFF1A1B36), fontSize: 15, fontWeight: FontWeight.w600)),
          ])),
        )
      ],
    );
  }

  MergeRequest get _mr => widget.mergeRequest;

  bool get _squashCommitsOptionVisible => !_mr.squashReadOnly || _mr.squashOnMerge;

  bool get _editCommitMessageOptionVisible => !_mr.mergeRequestsFfOnlyEnabled || _squashCommitsChecked;

  bool get _squashCommitMessageInputVisible => (_editCommitMessageChecked) && _squashCommitsChecked;

  bool get _mergeCommitMessageInputVisible => (!_mr.mergeRequestsFfOnlyEnabled && _editCommitMessageOptionVisible) && _editCommitMessageChecked;

  bool get _interactionDisabled => _isLoading || _isLoopingMergeResult;

  String? get hint {
    if (_squashCommitMessageInputVisible && _mergeCommitMessageInputVisible) {
      return AppLocalizations.dictionary().mergeAndSquashCommitMessageHint;
    }
    if (_squashCommitMessageInputVisible) {
      return AppLocalizations.dictionary().squashCommitMessageHint;
    }
    if (_mergeCommitMessageInputVisible) {
      return AppLocalizations.dictionary().mergeCommitMessageHint;
    }
    return null;
  }

  bool get autoMergeStrategiesAvailable => _mr.availableAutoMergeStrategies.contains("merge_when_pipeline_succeeds");
}
