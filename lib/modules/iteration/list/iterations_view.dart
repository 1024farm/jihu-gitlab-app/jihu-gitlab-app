import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/modules/iteration/details/iteration_page.dart';
import 'package:jihu_gitlab_app/modules/iteration/models/cadence_iteration.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/milestone_and_iteration_duration_view.dart';
import 'package:jihu_gitlab_app/modules/projects/milestone/milestone_and_iteration_state_view.dart';

enum IterationListViewSource {
  project,
  group;
}

class IterationsView extends StatefulWidget {
  final List<CadenceIteration> iterations;
  final String fullPath;
  final IterationListViewSource source;
  final String title;

  const IterationsView({required this.iterations, required this.source, required this.fullPath, required this.title, super.key});

  @override
  State<IterationsView> createState() => _IterationsViewState();
}

class _IterationsViewState extends State<IterationsView> {
  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return ListView.builder(
      itemCount: widget.iterations.length,
      itemBuilder: (context, index) {
        var iteration = widget.iterations[index];
        return InkWell(
            onTap: () {
              Navigator.of(context).pushNamed(IterationPage.routeName, arguments: {
                'fullPath': widget.fullPath,
                'iterationId': widget.iterations[index].id,
                'iteration': widget.iterations[index],
                'title': widget.title,
                'source': widget.source,
              });
            },
            child: Container(
              padding: const EdgeInsets.all(12),
              margin: const EdgeInsets.only(bottom: 12),
              decoration: const BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(6))),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  MilestoneAndIterationDurationView(iteration.startDate, iteration.dueDate, style: textTheme.titleLarge),
                  Container(margin: const EdgeInsets.only(top: 8), child: MilestoneAndIterationStateView(state: widget.iterations[index].state)),
                ],
              ),
            ));
      },
    );
  }
}
