import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class CadenceIteration {
  String id;
  String state;
  String dueDate;
  String startDate;
  String scopedPath;
  String webPath;

  CadenceIteration({required this.id, required this.state, required this.startDate, required this.dueDate, required this.scopedPath, required this.webPath});

  factory CadenceIteration.init(Map<String, dynamic> json) {
    return CadenceIteration(id: json['id'], state: json['state'], startDate: json['startDate'], dueDate: json['dueDate'], scopedPath: json['scopedPath'], webPath: json['webPath']);
  }

  String getStartDate() {
    return "${_months[int.parse(startDate.substring(5, 7))]} ${startDate.substring(8, 10)}, ${startDate.substring(0, 4)}";
  }

  String getDueDate() {
    return "${_months[int.parse(dueDate.substring(5, 7))]} ${dueDate.substring(8, 10)}, ${dueDate.substring(0, 4)}";
  }

  String get displayName => "${getStartDate()} - ${getDueDate()}";

  String getState() {
    switch (state) {
      case 'upcoming':
        return AppLocalizations.dictionary().projectsIterationUpcoming;
      case 'current':
        return AppLocalizations.dictionary().projectsIterationCurrent;
      default:
        return AppLocalizations.dictionary().projectsIterationClosed;
    }
  }

  static final _months = {1: 'Jan', 2: 'Feb', 3: 'Mar', 4: 'Apr', 5: 'May', 6: 'Jun', 7: 'Jul', 8: 'Aug', 9: 'Sep', 10: 'Oct', 11: 'Nov', 12: 'Dec'};
}
