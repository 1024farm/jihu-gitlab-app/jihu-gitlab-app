class Cadence {
  String id;
  String title;
  bool automatic;

  Cadence({required this.id, required this.title, required this.automatic});

  factory Cadence.init(Map<String, dynamic> json) {
    return Cadence(id: json['id'], title: json['title'], automatic: json['automatic']);
  }
}
