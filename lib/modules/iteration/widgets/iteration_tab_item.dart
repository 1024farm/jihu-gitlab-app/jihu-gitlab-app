import 'package:flutter/material.dart';

class IterationTabItem extends StatelessWidget {
  final bool isSelected;
  final String title;

  const IterationTabItem({required this.title, required this.isSelected, super.key});

  @override
  Widget build(BuildContext context) {
    var defaultBackgroundColor = const Color(0xFFEAEAEA);
    var selectedBackgroundColor = const Color(0xFFFFECE2);

    return Tab(
      height: 40,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(25.0),
          color: isSelected ? selectedBackgroundColor : defaultBackgroundColor,
        ),
        padding: const EdgeInsets.fromLTRB(10, 2, 10, 2),
        child: Text(title),
      ),
    );
  }
}
