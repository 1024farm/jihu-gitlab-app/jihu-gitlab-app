import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/domain/issue.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';
import 'package:jihu_gitlab_app/core/widgets/charts/time_box_report.dart';
import 'package:jihu_gitlab_app/modules/issues/grouped_issues/grouped_issues_model.dart';
import 'package:jihu_gitlab_app/modules/issues/list/group_issues_fetcher.dart';
import 'package:jihu_gitlab_app/modules/issues/list/project_issues_fetcher.dart';
import 'package:jihu_gitlab_app/modules/iteration/list/iterations_view.dart';
import 'package:jihu_gitlab_app/modules/iteration/models/cadence_iteration.dart';

class IterationModel {
  late CadenceIteration _iteration;
  late ValueNotifier<GroupedIssues> _notifier;
  late String _fullPath;
  late IterationListViewSource _source;
  GroupedIssues _groupedIssues = GroupedIssues.init();

  IterationModel(String fullPath, String title, CadenceIteration iteration, IterationListViewSource source) {
    _fullPath = fullPath;
    _iteration = iteration;
    _source = source;
    _notifier = ValueNotifier(_groupedIssues);
  }

  Future<void> getIterationInfo() async {
    try {
      First100Issues first100issues = await _getIssues();
      if (first100issues.isEmpty) {
        _groupedIssues = GroupedIssues.empty();
      } else {
        var body = (await ProjectRequestSender.instance().post(Api.graphql(), requestBodyOfIterationInfo(_fullPath, _iteration.id))).body();
        var iterationJson = body['data']?['iteration'];
        TimeBoxReport? timeBoxReport;
        String? descriptionHtml;
        if (iterationJson != null) {
          descriptionHtml = iterationJson['descriptionHtml'];
          var reportJson = iterationJson['report'];
          if (reportJson != null) {
            timeBoxReport = TimeBoxReport.fromJson(reportJson, _iteration.startDate, _iteration.dueDate);
          }
        }
        _groupedIssues = GroupedIssues.success(description: descriptionHtml, timeBoxReport: timeBoxReport, first100Issues: first100issues);
      }
    } catch (e) {
      _groupedIssues = GroupedIssues.fail();
    } finally {
      _notifier.value = _groupedIssues;
    }
  }

  Future<First100Issues> _getIssues() async {
    List<Issue> issues;
    if (_source == IterationListViewSource.project) {
      issues = (await ProjectIssuesFetcher().fetchIssues(() => iterationIssuesRequestBody(_fullPath, _iteration.id))).list;
    } else {
      issues = (await GroupIssuesFetcher(_fullPath).fetchIssues(() => subgroupIterationIssuesRequestBody(_fullPath, _iteration.id))).list;
    }
    return First100Issues(issues, canGroupByEpic: true);
  }

  CadenceIteration get iteration => _iteration;

  ValueNotifier<GroupedIssues> get notifier => _notifier;

  IterationListViewSource get source => _source;
}

Map<String, dynamic> requestBodyOfIteration(String iterationId) => {
      "variables": {"iterationId": iterationId},
      "query": """
          query getIteration(\$iterationId:IterationID!)
          {
            iteration(id: \$iterationId){
              descriptionHtml
            }
          }
          """
    };

Map<String, dynamic> iterationIssuesRequestBody(String fullPath, String iterationId) => {
      "query": """
        query iterationIssues(\$fullPath: ID!, \$id: ID!, \$afterCursor: String = "", \$firstPageSize: Int) {
          project(fullPath: \$fullPath) {
            id
            path
            fullPath
            name
            nameWithNamespace
            issues(
              iterationId: [\$id]
              sort: RELATIVE_POSITION_ASC,
              after: \$afterCursor
              first: \$firstPageSize
            ) {
              ...IterationIssues
            }
          }
        }
        
        fragment IterationIssues on IssueConnection {
          count
          pageInfo {
            ...PageInfo
          }
          nodes {
            id
            iid
            title
            webUrl
            state
            projectId
            epic {
              title
            }
            weight
            author {
              id
              name
              username
              avatarUrl
            }
            assignees {
              nodes {
                id
                name
                username
                avatarUrl
              }
            }
          }
        }
        
        fragment PageInfo on PageInfo {
          hasNextPage
          endCursor
        }
""",
      "variables": {"afterCursor": "", "fullPath": fullPath, "id": iterationId, "firstPageSize": 100}
    };

Map<String, dynamic> subgroupIterationIssuesRequestBody(String fullPath, String iterationId) => {
      "query": """
        query iterationIssues(\$fullPath: ID!, \$id: ID!, \$afterCursor: String = "", \$firstPageSize: Int) {
          group(fullPath: \$fullPath) {
            id
            path
            fullPath
            name
            issues(
              iterationId: [\$id]
              sort: RELATIVE_POSITION_ASC,
              after: \$afterCursor
              first: \$firstPageSize
            ) {
              ...IterationIssues
            }
          }
        }
        
        fragment IterationIssues on IssueConnection {
          count
          pageInfo {
            ...PageInfo
          }
          nodes {
            id
            iid
            title
            webUrl
            state
            projectId
            epic {
              title
            }
            weight
            author {
              id
              name
              username
              avatarUrl
            }
            assignees {
              nodes {
                id
                name
                username
                avatarUrl
              }
            }
          }
        }
        
        fragment PageInfo on PageInfo {
          hasNextPage
          endCursor
        }
""",
      "variables": {"afterCursor": "", "fullPath": fullPath, "id": iterationId, "firstPageSize": 100}
    };

Map<String, dynamic> requestBodyOfIterationInfo(String fullPath, String iterationId) => {
      "variables": {"iterationId": iterationId, "fullPath": fullPath},
      "query": """
              query getBurnupTimesSeriesIterationData(\$iterationId: IterationID!, \$fullPath: String) {
                iteration(id: \$iterationId) {
                  id
                  descriptionHtml
                  report(fullPath: \$fullPath) {
                    burnupTimeSeries {
                      date
                      completedCount
                      scopeCount
                      completedWeight
                      scopeWeight
                    }
                  }
                }
              }
              """
    };
