import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/modules/issues/grouped_issues/grouped_issues_model.dart';
import 'package:jihu_gitlab_app/modules/issues/grouped_issues/grouped_issues_view.dart';
import 'package:jihu_gitlab_app/modules/iteration/details/iteration_model.dart';
import 'package:jihu_gitlab_app/modules/iteration/list/iterations_view.dart';
import 'package:jihu_gitlab_app/modules/iteration/models/cadence_iteration.dart';

class IterationPage extends StatefulWidget {
  static const String routeName = "IterationPage";

  const IterationPage({required this.arguments, super.key});

  final Map arguments;

  @override
  State<IterationPage> createState() => _IterationPageState();
}

class _IterationPageState extends State<IterationPage> with TickerProviderStateMixin {
  late IterationModel _model;

  @override
  void initState() {
    CadenceIteration iteration = widget.arguments['iteration'];
    _model = IterationModel(widget.arguments['fullPath'], widget.arguments['title'], iteration, widget.arguments['source'] as IterationListViewSource)..getIterationInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<GroupedIssues>(
        valueListenable: _model.notifier,
        builder: (context, data, child) {
          return GroupedIssuesView(
            onRetry: _model.getIterationInfo,
            state: _model.iteration.state,
            startDate: _model.iteration.startDate,
            dueDate: _model.iteration.dueDate,
            title: widget.arguments['title'],
            groupingDefault: true,
            fromGroup: _model.source == IterationListViewSource.group,
            groupedIssues: data,
          );
        });
  }
}
