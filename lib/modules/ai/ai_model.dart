import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/ai/message/human_message.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message_repository.dart';
import 'package:jihu_gitlab_app/modules/ai/message/robot_message.dart';

class AIModel {
  final FocusNode _focusNode = FocusNode();
  bool _onAsking = false;
  final TextEditingController _askingController = TextEditingController();
  final List<Message> _messages = [];
  final ScrollController _scrollController = ScrollController();
  late ValueNotifier<List<Message>> _notifier;
  static const int _pageSize = 20;
  int _page = 0;
  bool _isLoadingHistories = false;
  bool _hasMoreHistories = true;

  FocusNode get focusNode => _focusNode;

  bool get onAsking => _onAsking;

  TextEditingController get askingController => _askingController;

  ScrollController get scrollController => _scrollController;

  void init(void Function(VoidCallback fn) setState) {
    _notifier = ValueNotifier(_messages);
    _focusNode.addListener(() {
      setState(() => _onAsking = !onAsking);
    });
    _scrollController.addListener(() {
      if (_scrollController.offset == _scrollController.position.maxScrollExtent && !_isLoadingHistories && _hasMoreHistories) {
        loadHistories();
      }
    });
  }

  void sendQuestion(void Function(VoidCallback fn) setState, {String? defaultQuestion}) async {
    _focusNode.unfocus();
    var message = HumanMessage(defaultQuestion ?? _askingController.text);
    await _addNewMessage(message);
    scrollDown();
    message.requestAnswer().then((res) {
      if (res.type.isNon) {
        setState(() {});
        return;
      }
      message.state.done();
      _saveMessage(message);
      _addNewMessage(res);
      setState(() {});
      scrollDown();
    }).catchError((err) {
      message.state.error();
      _saveMessage(message);
    });
    _askingController.text = '';
  }

  void scrollDown() {
    // TODO: Do not use delay
    // Future.delayed(const Duration(milliseconds: 450), () => _scrollController.jumpTo(_scrollController.position.maxScrollExtent));
  }

  Future<void> loadHistories() async {
    _isLoadingHistories = true;
    _page++;
    var list = await MessageRepository.instance().query(_page, _pageSize);
    _hasMoreHistories = list.length >= _pageSize;
    _messages.addAll(list);
    if (_messages.isEmpty) {
      var welcomeMessage = _initWelcomeWords();
      _addNewMessage(welcomeMessage);
    } else {
      _notifier.value = List.from(_messages);
    }
    _isLoadingHistories = false;
  }

  Message _initWelcomeWords() {
    return RobotMessage(AppLocalizations.dictionary().aiWelcome);
  }

  Future<int> _saveMessage(Message message) async {
    return MessageRepository.instance().save(message);
  }

  Future<void> _addNewMessage(Message message) async {
    message.id = await _saveMessage(message);
    _messages.insert(0, message);
    _notifier.value = List.from(_messages);
  }

  ValueNotifier<List<Message>> get notifier => _notifier;
}
