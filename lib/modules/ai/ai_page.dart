import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/ai/ai_model.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message.dart';

class AIPage extends StatefulWidget {
  static const routeName = "AIPage";

  const AIPage({required this.arguments, super.key});

  final Map arguments;

  @override
  State<AIPage> createState() => _AIPageState();
}

class _AIPageState extends State<AIPage> {
  final AIModel _aiModel = AIModel();

  @override
  void initState() {
    super.initState();

    _aiModel.init(setState);
    _aiModel.loadHistories();
    String defaultMessage = widget.arguments['search'] ?? '';
    if (defaultMessage.isNotEmpty) {
      _aiModel.sendQuestion(setState, defaultQuestion: defaultMessage);
    }
  }

  @override
  Widget build(BuildContext context) {
    final colorScheme = Theme.of(context).colorScheme;
    return Scaffold(
      appBar: CommonAppBar(title: Text(AppLocalizations.dictionary().ai), showLeading: true),
      body: SafeArea(
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Stack(
            children: [
              Container(
                margin: const EdgeInsets.only(bottom: 56),
                child: ValueListenableBuilder(builder: (BuildContext context, List<Message> data, Widget? child) => _buildMessageList(data), valueListenable: _aiModel.notifier),
              ),
              Align(alignment: Alignment.bottomCenter, child: _questionAskingBox()),
            ],
          ),
        ),
      ),
      backgroundColor: colorScheme.background,
    );
  }

  Widget _buildMessageList(List<Message> messages) {
    return ListView.builder(
      key: const Key("list-view"),
      reverse: true,
      shrinkWrap: true,
      controller: _aiModel.scrollController,
      itemBuilder: (context, index) => messages[index].toView(),
      itemCount: messages.length,
    );
  }

  Widget _questionAskingBox() {
    return Container(
      constraints: const BoxConstraints(maxHeight: 56, minHeight: 56),
      margin: const EdgeInsets.only(top: 10),
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 10),
        child: TextField(
          controller: _aiModel.askingController,
          style: _aiModel.onAsking ? const TextStyle(fontSize: 14, color: Colors.black) : const TextStyle(fontSize: 14, color: Color(0xFF95979A)),
          decoration: InputDecoration(
            fillColor: const Color(0xFFEAEAEA),
            filled: true,
            hintText: AppLocalizations.dictionary().aiEnterQuestion,
            contentPadding: const EdgeInsets.all(8),
            border: OutlineInputBorder(gapPadding: 0, borderRadius: BorderRadius.circular(4), borderSide: BorderSide.none),
          ),
          focusNode: _aiModel.focusNode,
          onSubmitted: (String text) => _aiModel.sendQuestion(setState),
        ),
      ),
    );
  }
}
