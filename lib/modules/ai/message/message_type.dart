class MessageType {
  String _value;

  MessageType._(this._value);

  static final Map<String, MessageType> _map = {
    "human": MessageType.human(),
    "robot": MessageType.robot(),
    "non": MessageType.non(),
  };

  factory MessageType.human() {
    return MessageType._('human');
  }

  factory MessageType.robot() {
    return MessageType._('robot');
  }

  factory MessageType.non() {
    return MessageType._('non');
  }

  factory MessageType.valueOf(String value) {
    return _map[value] ?? MessageType.non();
  }

  bool get isNon => _value == 'non';

  String get value => _value;

  @override
  bool operator ==(Object other) => identical(this, other) || other is MessageType && runtimeType == other.runtimeType && _value == other._value;

  @override
  int get hashCode => _value.hashCode;
}
