import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message.dart';

abstract class MessageBox extends StatelessWidget {
  final Message message;

  const MessageBox({required this.message, super.key});
}
