import 'package:jihu_gitlab_app/core/domain/global_time.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message_box.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message_state.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message_type.dart';

class NonResponseMessage extends Message {
  NonResponseMessage([int? id, MessageState? state, DateTime? time]) : super(id, state ?? MessageState.done(), '', time ?? GlobalTime.now(), MessageType.non());

  @override
  Future<Message> requestAnswer() async {
    throw Exception();
  }

  @override
  MessageBox toView() {
    throw Exception();
  }
}
