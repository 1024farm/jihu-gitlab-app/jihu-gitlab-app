import 'package:jihu_gitlab_app/core/db_manager.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message_entity.dart';

class MessageRepository {
  static final MessageRepository _instance = MessageRepository._internal();

  MessageRepository._internal();

  factory MessageRepository.instance() => _instance;

  Future<int> save(Message message) async {
    var map = MessageEntity.create(message).toMap();
    if (map['id'] != null) {
      return await DbManager.instance().update(MessageEntity.tableName, map, where: " id = ? ", whereArgs: [map['id']]);
    }
    return await DbManager.instance().insert(MessageEntity.tableName, map);
  }

  Future<List<Message>> query(int page, int pageSize) async {
    var list = await DbManager.instance().find(MessageEntity.tableName, orderBy: "created_at desc", limit: pageSize, offset: (page - 1) * pageSize);
    return list.map((e) => MessageEntity.restore(e).asDomain()).toList();
  }
}
