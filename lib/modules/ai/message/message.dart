import 'package:jihu_gitlab_app/core/utils/date_time_formatter.dart';
import 'package:jihu_gitlab_app/modules/ai/message/human_message.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message_box.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message_state.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message_type.dart';
import 'package:jihu_gitlab_app/modules/ai/message/non_response_message.dart';
import 'package:jihu_gitlab_app/modules/ai/message/robot_message.dart';

abstract class Message {
  int? id;
  MessageState state;
  final String content;
  final DateTime time;
  final MessageType type;

  Message(this.id, this.state, this.content, this.time, this.type);

  Future<Message> requestAnswer();

  MessageBox toView();

  factory Message.restore(int? id, String content, MessageState state, MessageType type, DateTime time) {
    if (type == MessageType.human()) {
      return HumanMessage(content, id, state, time);
    }
    if (type == MessageType.robot()) {
      return RobotMessage(content, id, state, time);
    }
    return NonResponseMessage(id, state, time);
  }

  String get createdAt => DateTimeFormatter.withPattern(['mm', '-', 'dd', " ", 'HH', ':', 'nn']).format(time);
}
