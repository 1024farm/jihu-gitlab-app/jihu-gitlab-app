import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/markdown_view.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message_box.dart';

class RobotMessageBox extends MessageBox {
  const RobotMessageBox({required super.message, super.key});

  @override
  Widget build(BuildContext context) {
    return Row(children: [messageBox(context)]);
  }

  Widget messageBox(BuildContext context) {
    return Expanded(
      child: Container(
        padding: const EdgeInsets.all(8),
        margin: EdgeInsets.fromLTRB(12, 6, MediaQuery.of(context).size.width * 0.2, 6),
        decoration: const BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.only(topLeft: Radius.zero, topRight: Radius.circular(8), bottomLeft: Radius.circular(8), bottomRight: Radius.circular(8))),
        child: MarkdownView(
          context: context,
          data: message.content,
          physics: const NeverScrollableScrollPhysics(),
          padding: const EdgeInsets.all(0),
        ),
      ),
    );
  }
}
