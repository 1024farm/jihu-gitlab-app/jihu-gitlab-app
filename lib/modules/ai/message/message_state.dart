class MessageState {
  String _value;

  MessageState._(this._value);

  static final Map<String, MessageState> _map = {"pending": MessageState.pending(), "done": MessageState.done(), "error": MessageState.error()};

  factory MessageState.pending() {
    return MessageState._('pending');
  }

  factory MessageState.done() {
    return MessageState._('done');
  }

  factory MessageState.error() {
    return MessageState._('error');
  }

  factory MessageState.valueOf(String value) {
    return _map[value] ?? MessageState.done();
  }

  void done() {
    _value = 'done';
  }

  void error() {
    _value = 'error';
  }

  bool isPending() => _value == 'pending';

  bool isError() => _value == 'error';

  String get value => _value;

  @override
  bool operator ==(Object other) => identical(this, other) || other is MessageState && runtimeType == other.runtimeType && _value == other._value;

  @override
  int get hashCode => _value.hashCode;
}
