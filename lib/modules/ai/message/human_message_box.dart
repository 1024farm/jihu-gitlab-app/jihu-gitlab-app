import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/layout/adaptive.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message_box.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message_state.dart';

class HumanMessageBox extends MessageBox {
  const HumanMessageBox({required super.message, super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(12, 6, 12, 6),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Row(
            children: [
              const Expanded(child: SizedBox()),
              loading(message.state),
              error(message.state),
              messageBox(context),
            ],
          ),
          const SizedBox(height: 4),
          Text(
            message.createdAt,
            style: const TextStyle(fontSize: 12),
          )
        ],
      ),
    );
  }

  Widget messageBox(BuildContext context) {
    var isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;
    double maxWidth = isDesktopLayout(context) ? (isPortrait ? 0.48 : 0.54) : 0.72;
    return Container(
      padding: const EdgeInsets.all(8),
      margin: const EdgeInsets.only(left: 12),
      constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width * maxWidth),
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        gradient: const LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [Color(0xfffca326), AppThemeData.primaryColor],
        ),
        borderRadius: const BorderRadius.only(topLeft: Radius.circular(8), topRight: Radius.circular(8), bottomLeft: Radius.circular(8), bottomRight: Radius.zero),
      ),
      child: _userBox(message.content),
    );
  }

  Widget loading(MessageState state) => state.isPending() ? const SizedBox(width: 16, height: 16, child: CircularProgressIndicator(color: Color.fromRGBO(0, 0, 0, 0.3), strokeWidth: 2)) : Container();

  Widget error(MessageState state) => state.isError() ? const Icon(Icons.error, size: 22, color: Colors.red) : Container();

  Widget _userBox(String content) {
    return Text(content, style: const TextStyle(color: Colors.white, fontSize: 14));
  }
}
