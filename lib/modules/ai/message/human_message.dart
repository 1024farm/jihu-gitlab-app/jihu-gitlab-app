import 'dart:async';

import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/domain/global_time.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/widgets/toast.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/ai/message/human_message_box.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message_box.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message_state.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message_type.dart';
import 'package:jihu_gitlab_app/modules/ai/message/non_response_message.dart';
import 'package:jihu_gitlab_app/modules/ai/message/robot_message.dart';

class HumanMessage extends Message {
  HumanMessage(String content, [int? id, MessageState? state, DateTime? time]) : super(id, state ?? MessageState.pending(), content, time ?? GlobalTime.now(), MessageType.human());

  @override
  Future<Message> requestAnswer() async {
    try {
      var res = await HttpClient.instance()
          .postWithHeader('https://gitlablang.cognitiveservices.azure.com/language/:query-knowledgebases?projectName=jihu&api-version=2021-10-01&deploymentName=production', {
        "top": 1,
        "question": content,
        "includeUnstructuredSources": true,
        "confidenceScoreThreshold": "0.5",
        "answerSpanRequest": {"enable": true, "topAnswersWithSpan": 1, "confidenceScoreThreshold": "0.5"}
      }, {
        'Ocp-Apim-Subscription-Key': const String.fromEnvironment("OCP_APIM_SUBSCURIPTION_KEY")
      });
      state.done();
      var responseMessage = res.body()['answers'][0]["answer"];
      debugPrint(responseMessage);
      return RobotMessage(responseMessage);
    } catch (e) {
      Toast.show(AppLocalizations.dictionary().aiNetworkError);
      state.error();
      return NonResponseMessage();
    }
  }

  @override
  MessageBox toView() {
    return HumanMessageBox(message: this);
  }
}
