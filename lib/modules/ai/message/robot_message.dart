import 'package:jihu_gitlab_app/core/domain/global_time.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message_box.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message_state.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message_type.dart';
import 'package:jihu_gitlab_app/modules/ai/message/robot_message_box.dart';

class RobotMessage extends Message {
  RobotMessage(String content, [int? id, MessageState? state, DateTime? time]) : super(id, state ?? MessageState.done(), content, time ?? GlobalTime.now(), MessageType.robot());

  @override
  Future<Message> requestAnswer() async {
    throw Exception();
  }

  @override
  MessageBox toView() {
    return RobotMessageBox(message: this);
  }
}
