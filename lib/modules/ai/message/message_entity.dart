import 'package:jihu_gitlab_app/modules/ai/message/message.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message_state.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message_type.dart';

class MessageEntity {
  static const tableName = "ai_messages";

  int? id;
  String type;
  String state;
  final String content;
  final int createdAt;

  MessageEntity._internal(this.id, this.type, this.state, this.content, this.createdAt);

  factory MessageEntity.restore(Map<String, dynamic> map) {
    return MessageEntity._internal(map["id"], map['type'], map['state'], map['content'], map['created_at']);
  }

  factory MessageEntity.create(Message message) {
    return MessageEntity._internal(message.id, message.type.value, message.state.value, message.content, message.time.millisecondsSinceEpoch);
  }

  Message asDomain() {
    return Message.restore(id, content, MessageState.valueOf(state), MessageType.valueOf(type), DateTime.fromMillisecondsSinceEpoch(createdAt));
  }

  Map<String, dynamic> toMap() {
    return {"id": id, "type": type, "state": state, "content": content, "created_at": createdAt};
  }

  static String get createTableSql => """
    create table $tableName(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      type TEXT NOT NULL,
      state TEXT ,
      content TEXT ,
      created_at INTEGER
    );    
  """;
}
