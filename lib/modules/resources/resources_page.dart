import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:webview_flutter_android/webview_flutter_android.dart';
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';

class ResourcesPage extends StatefulWidget {
  static const String routeName = "ResourcesPage";

  const ResourcesPage({Key? key}) : super(key: key);

  @override
  State<ResourcesPage> createState() => _ResourcesPageState();
}

class _ResourcesPageState extends State<ResourcesPage> {
  static const String _initialUrl = 'https://app.jingsocial.com/microFrontend/contentCenterH5/center/gtqi5Notay5VAiNE5eVFUY?appid=wxa3af8f525446f3de&tabid=9TX4Q8x2B63ihab5hmiN6B';
  late WebViewController _controller;

  @override
  void initState() {
    PlatformWebViewControllerCreationParams params = const PlatformWebViewControllerCreationParams();
    if (WebViewPlatform.instance is WebKitWebViewPlatform) {
      params = WebKitWebViewControllerCreationParams(allowsInlineMediaPlayback: true, mediaTypesRequiringUserAction: const <PlaybackMediaTypes>{});
    } else if (WebViewPlatform.instance is AndroidWebViewPlatform) {
      params = AndroidWebViewControllerCreationParams.fromPlatformWebViewControllerCreationParams(params);
    }
    _controller = WebViewController.fromPlatformCreationParams(params)
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(Colors.white)
      ..loadRequest(Uri.parse(_initialUrl));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ConnectionProvider>(builder: (context, _, child) {
      return WillPopScope(
          onWillPop: () async {
            var canGoBack = await _controller.canGoBack();
            if (canGoBack) {
              _controller.goBack();
              return false;
            }
            return true;
          },
          child: Scaffold(appBar: CommonAppBar(showLeading: true, title: Text(AppLocalizations.dictionary().resources)), body: SafeArea(child: WebViewWidget(controller: _controller))));
    });
  }
}
