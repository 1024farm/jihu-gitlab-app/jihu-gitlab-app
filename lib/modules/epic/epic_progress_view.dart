import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/gradient_progress_indicator.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/svg_text.dart';

class EpicProgressView extends StatelessWidget {
  const EpicProgressView({required this.name, required this.closed, required this.total, required this.first, required this.second, required this.svgAssetPath, Key? key}) : super(key: key);
  final String name;
  final int closed;
  final int total;
  final Color first;
  final Color second;
  final String svgAssetPath;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SvgText(svgAssetPath: svgAssetPath, svgSize: const Size(14, 14), text: name, textStyle: const TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: AppThemeData.secondaryColor)),
            Text.rich(
              TextSpan(children: [TextSpan(text: "$closed", style: const TextStyle(color: Colors.black)), TextSpan(text: " / $total")]),
              style: const TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: AppThemeData.secondaryColor),
            ),
          ],
        ),
        const SizedBox(height: 6),
        GradientProgressIndicator(progress: progress, first: first, second: second, height: 4),
      ],
    );
  }

  double get progress => closed / total;
}
