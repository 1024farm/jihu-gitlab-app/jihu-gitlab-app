import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/domain/issue.dart';
import 'package:jihu_gitlab_app/core/iterable_extension.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/epic/epic_progress_view.dart';
import 'package:jihu_gitlab_app/modules/issues/details/issue_details_page.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_view.dart';

class EpicIssuesView extends StatefulWidget {
  const EpicIssuesView({required this.epicIssues, required this.formGroup, Key? key}) : super(key: key);
  final EpicIssues epicIssues;
  final bool formGroup;

  @override
  State<EpicIssuesView> createState() => _EpicIssuesViewState();
}

class _EpicIssuesViewState extends State<EpicIssuesView> {
  @override
  Widget build(BuildContext context) {
    String epicName = widget.epicIssues.epicName;
    List<Issue> issues = widget.epicIssues.issues;
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 12),
      decoration: BoxDecoration(color: const Color(0xFFF8F8FA), borderRadius: BorderRadius.circular(4.0)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(epicName, style: const TextStyle(fontSize: 17, fontWeight: FontWeight.w600)),
          const SizedBox(height: 12),
          EpicProgressView(
              name: AppLocalizations.dictionary().projectsIterationWeight,
              svgAssetPath: 'assets/images/weight.svg',
              closed: _closedIssueWeightCount(issues),
              total: _allIssueWeightCount(issues),
              first: const Color(0xFFFEA64E),
              second: Theme.of(context).primaryColor),
          const SizedBox(height: 12),
          EpicProgressView(
              name: AppLocalizations.dictionary().projectsIterationIssueCounts,
              svgAssetPath: 'assets/images/issues.svg',
              closed: issues.where((issue) => issue.state == 'closed').length,
              total: issues.length,
              first: const Color(0xFF6C9FE0),
              second: const Color(0xFF6666C4)),
          const SizedBox(height: 12),
          _issuesView(issues),
        ],
      ),
    );
  }

  int _allIssueWeightCount(List<Issue> issues) => issues.map((issue) => issue.weight ?? 0).reduceOr((value, element) => value += element, 0);

  int _closedIssueWeightCount(List<Issue> issues) => issues.where((issue) => issue.state == 'closed').map((issue) => issue.weight ?? 0).reduceOr((value, element) => value += element, 0);

  Widget _issuesView(List<Issue> issues) {
    return IssuesView(
        issuesFetcher: () => issues,
        isLoading: () => false,
        isEmpty: () => false,
        onRefresh: () async => {},
        scrollable: false,
        onListItemTap: (index) {
          final issue = issues[index];
          var params = <String, dynamic>{
            'projectId': issue.project.id,
            'issueId': issue.id.id,
            'issueIid': issue.iid,
            'targetId': issue.target.id,
            'targetIid': issue.target.iid,
            'pathWithNamespace': issue.project.pathWithNamespace,
            'issue': issue,
            'showLeading': true,
          };
          Navigator.of(context).pushNamed(IssueDetailsPage.routeName, arguments: params);
        },
        selectedIndex: -1,
        selectedColor: Colors.transparent,
        displayOptions: IssuesViewDisplayOptions(),
        displayProject: widget.formGroup);
  }
}

class EpicIssues {
  String epicName;
  List<Issue> issues;

  EpicIssues(this.epicName, this.issues);
}
