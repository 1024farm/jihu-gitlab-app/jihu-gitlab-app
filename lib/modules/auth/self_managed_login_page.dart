import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_text_input/flutter_native_text_input.dart';
import 'package:flutter_svg/svg.dart';
import 'package:jihu_gitlab_app/core/app_platform.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/loader.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/native_text_field.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/toast.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class SelfManagedLoginPage extends StatefulWidget {
  static const routeName = "SelfManagedLoginPage";

  const SelfManagedLoginPage({super.key});

  @override
  State<SelfManagedLoginPage> createState() => _SelfManagedLoginPageState();
}

class _SelfManagedLoginPageState extends State<SelfManagedLoginPage> {
  String? _host;
  String? _accessToken;
  bool _isHttps = true;
  late TextEditingController controller = TextEditingController();
  late TextEditingController passwordController = TextEditingController();

  @override
  void dispose() {
    controller.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final colorScheme = Theme.of(context).colorScheme;
    return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Scaffold(
          appBar: CommonAppBar(title: Text(AppLocalizations.dictionary().gitlabSelfManaged), showLeading: true),
          backgroundColor: colorScheme.background,
          body: Column(
            children: [
              AppPlatform.isAndroid
                  ? Container(
                      height: 50,
                      margin: const EdgeInsets.only(left: 16, right: 16, top: 16),
                      padding: const EdgeInsets.only(left: 12, right: 12),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: const Color(0xFFEAEAEA)),
                        borderRadius: BorderRadius.circular(4.0),
                      ),
                      child: TextField(
                          onChanged: (String text) {
                            _host = text;
                            setState(() {});
                          },
                          keyboardType: TextInputType.url,
                          controller: controller,
                          maxLines: 1,
                          autocorrect: false,
                          cursorColor: Theme.of(context).primaryColor,
                          style: const TextStyle(fontSize: 14),
                          decoration: InputDecoration(
                            hintText: '${AppLocalizations.dictionary().host}: gitlab.example.com',
                            border: InputBorder.none,
                            suffixIconConstraints: const BoxConstraints(minHeight: 18),
                            suffixIcon: _buildSuffixIcon(controller),
                          )))
                  // coverage:ignore-start
                  : Container(
                      height: 50,
                      margin: const EdgeInsets.only(left: 16, right: 16, top: 16),
                      padding: const EdgeInsets.only(left: 4, right: 4),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: const Color(0xFFEAEAEA)),
                        borderRadius: BorderRadius.circular(4.0),
                      ),
                      child: NativeTextField(
                          onChanged: (String text) {
                            _host = text;
                            setState(() {});
                          },
                          textCapitalization: TextCapitalization.none,
                          keyboardType: KeyboardType.url,
                          autoCorrect: false,
                          controller: controller,
                          maxLines: 1,
                          minHeightPadding: 30,
                          paddingLeft: 0,
                          paddingRight: 0,
                          cursorColor: Theme.of(context).primaryColor,
                          style: const TextStyle(color: Color(0xFF03162F), fontSize: 14, fontWeight: FontWeight.w400),
                          placeHolder: '${AppLocalizations.dictionary().host}: gitlab.example.com',
                          placeHolderStyle: const TextStyle(fontSize: 14, color: Color.fromRGBO(154, 154, 154, 1.0), fontWeight: FontWeight.w400),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.transparent),
                          ))),
              // coverage:ignore-end
              Container(
                height: 50,
                margin: const EdgeInsets.only(left: 16, right: 16, top: 16),
                padding: const EdgeInsets.only(left: 12, right: 8),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: const Color(0xFFEAEAEA)),
                  borderRadius: BorderRadius.circular(4.0),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text('HTTPS'),
                    CupertinoSwitch(
                        value: _isHttps,
                        activeColor: colorScheme.primary,
                        onChanged: (value) {
                          setState(() {
                            _isHttps = value;
                          });
                        }),
                  ],
                ),
              ),
              Container(
                  height: 50,
                  margin: const EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 10),
                  padding: const EdgeInsets.only(left: 12, right: 12),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: const Color(0xFFEAEAEA)),
                    borderRadius: BorderRadius.circular(4.0),
                  ),
                  child: TextField(
                      controller: passwordController,
                      onChanged: (String text) {
                        _accessToken = text;
                        setState(() {});
                      },
                      keyboardType: TextInputType.visiblePassword,
                      maxLines: 1,
                      autocorrect: false,
                      cursorColor: Theme.of(context).primaryColor,
                      style: const TextStyle(fontSize: 14),
                      obscureText: true,
                      decoration: InputDecoration(
                        hintText: AppLocalizations.dictionary().accessToken,
                        hintStyle: AppPlatform.isAndroid ? null : const TextStyle(color: Color.fromRGBO(180, 180, 180, 1.0)),
                        border: InputBorder.none,
                        suffixIconConstraints: const BoxConstraints(minHeight: 18),
                        suffixIcon: _buildSuffixIcon(passwordController),
                      ))),
              InkWell(
                onTap: _onSubmitTapped,
                child: Container(
                    height: 40,
                    width: MediaQuery.of(context).size.width,
                    margin: const EdgeInsets.all(16),
                    decoration: BoxDecoration(color: colorScheme.primary, borderRadius: BorderRadius.circular(4.0)),
                    child: Center(child: Text(AppLocalizations.dictionary().submit, style: const TextStyle(color: Colors.white, fontSize: 15)))),
              )
            ],
          ),
        ));
  }

  Widget? _buildSuffixIcon(TextEditingController controller) {
    return controller.text.isNotEmpty
        ? InkWell(
            onTap: () {
              controller.clear();
              setState(() {});
            },
            child: SvgPicture.asset('assets/images/clear.svg', colorFilter: const ColorFilter.mode(Color(0xFFC0C0C0), BlendMode.srcIn), width: 18, key: const Key("clear")),
          )
        : null;
  }

  void showAlertWith({required String message}) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return CupertinoAlertDialog(title: Text(AppLocalizations.dictionary().warning), content: Text(message), actions: <Widget>[
          TextButton(
            child: Text(AppLocalizations.dictionary().ok),
            onPressed: () {
              Navigator.pop(context);
            },
          )
        ]);
      },
    );
  }

  Future<void> _onSubmitTapped() async {
    if (_host == null || _host!.isEmpty) {
      showAlertWith(message: AppLocalizations.dictionary().loginRequireHost);
      return;
    }

    if (_accessToken == null || _accessToken!.isEmpty) {
      showAlertWith(message: AppLocalizations.dictionary().loginRequireAccessToken);
      return;
    }
    Loader.showProgress(context, barrierDismissible: false, text: AppLocalizations.dictionary().inProgress);
    ConnectionProvider().initSelfManagedConfigs(host: _host!, token: _accessToken!, isHttps: _isHttps).then((result) {
      LogHelper.debug('SelfManagedLoginPage UserProvider().refresh()');
      Loader.hideProgress(context);
      if (result["success"]) {
        setState(() {
          Navigator.pop(context);
        });
      } else {
        showAlertWith(message: AppLocalizations.dictionary().loginHostOrAccessTokenError);
      }
    }).onError((error, stackTrace) {
      Loader.hideProgress(context);
      Toast.error(context, AppLocalizations.dictionary().sameTypeAccountOnlyLoginOne);
    });
  }
}
