import 'dart:async';

import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/toast.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/auth/login_model.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';

class LoginPage extends StatefulWidget {
  static const routeName = "LoginPage";
  final Map arguments;

  const LoginPage({required this.arguments, Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final LoginModel _loginModel = LoginModel();
  late WebViewController _controller;
  late final WebViewCookieManager cookieManager = WebViewCookieManager();

  @override
  void initState() {
    super.initState();
    _loginModel.configuration(host: widget.arguments['host']);
    late final PlatformWebViewControllerCreationParams params;
    if (WebViewPlatform.instance is WebKitWebViewPlatform) {
      params = WebKitWebViewControllerCreationParams(allowsInlineMediaPlayback: true, mediaTypesRequiringUserAction: const <PlaybackMediaTypes>{});
    } else {
      params = const PlatformWebViewControllerCreationParams();
    }
    _controller = WebViewController.fromPlatformCreationParams(params)
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setNavigationDelegate(NavigationDelegate(
        onNavigationRequest: (NavigationRequest request) {
          if (request.url.startsWith(_loginModel.callbackUrl!)) {
            var uri = Uri.parse(request.url);
            String code = uri.queryParameters["code"] as String;
            _loginModel.exchangeToken(code).then((bool success) {
              if (success) Navigator.pop(context, true);
            }).catchError((e) {
              _clearCookies();
              Navigator.pop(context, true);
              Toast.error(context, AppLocalizations.dictionary().sameTypeAccountOnlyLoginOne);
              throw e;
            });
            return NavigationDecision.prevent;
          }
          return NavigationDecision.navigate;
        },
      ));
    if (ConnectionProvider.isLoggedOutByUser) {
      _controller.clearCache();
      _clearCookies();
    }
    if (_loginModel.authorizeUrl != null) {
      _controller.loadRequest(Uri.parse(_loginModel.authorizeUrl!));
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          var canGoBack = await _controller.canGoBack();
          if (canGoBack) {
            _controller.goBack();
            return false;
          }
          return true;
        },
        child: Scaffold(
            appBar: CommonAppBar(
              title: const Text(""),
              showLeading: true,
              backgroundColor: Colors.white,
              leading: BackButton(
                  color: Colors.black,
                  onPressed: () {
                    _controller.canGoBack().then((value) => {
                          if (value) {_controller.goBack()} else {Navigator.pop(context)}
                        });
                  }),
            ),
            body: _loginModel.authorizeUrl == null ? Container() : WebViewWidget(controller: _controller)));
  }

  Future<void> _clearCookies() async {
    await cookieManager.clearCookies();
  }
}
