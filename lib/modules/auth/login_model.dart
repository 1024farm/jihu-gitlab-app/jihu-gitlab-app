import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/connection_provider/login_type_same_exception.dart';
import 'package:jihu_gitlab_app/core/domain/token.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';

enum LoginClientIdSet { jihulab, gitlab }

extension LoginClientIdSetExtension on LoginClientIdSet {
  String get id => () {
        switch (this) {
          case LoginClientIdSet.jihulab:
            return '705fc68eab482c32db1ecc6ad6f9ae7bff4a949c72043c7e51aefbb51ec4d1e4';
          case LoginClientIdSet.gitlab:
            return '1c8d515890351a4b14d9e97565800407612624e19ba212b7abbda4de7a511cec';
        }
      }();
}

class LoginModel {
  static const _path = "/oauth/token";
  static const _grantTypeExchange = "authorization_code";
  static const _redirectUri = "jihu-gitlab://authorization/callback";

  static LoginModel _instance = LoginModel._internal();

  LoginModel._internal();

  factory LoginModel() => _instance;

  LoginClientIdSet? _clientId;
  String? _authorizeUrl;
  String? _baseUrl;
  bool _onAuthTokenRequest = false;

  void configuration({required String host}) {
    _clientId = host.contains(LoginClientIdSet.jihulab.name) ? LoginClientIdSet.jihulab : LoginClientIdSet.gitlab;
    _authorizeUrl = "https://$host/oauth/authorize?client_id=${_clientId!.id}&"
        "redirect_uri=$_redirectUri&response_type=code";
    _baseUrl = "https://$host";
  }

  Future<bool> exchangeToken(String code) async {
    if (_onAuthTokenRequest) return false;
    _onAuthTokenRequest = true;
    Map<String, dynamic> data = {"client_id": _clientId!.id, "grant_type": _grantTypeExchange, "redirect_uri": _redirectUri, "code": code};
    try {
      await ConnectionProvider().addUserByToken(await _doRequest(data, _baseUrl!), _baseUrl!);
      _onAuthTokenRequest = false;
      return Future.value(true);
    } on LoginTypeSameException {
      rethrow;
    } catch (e) {
      LogHelper.err('LoginModel exchangeToken error', e);
      _onAuthTokenRequest = false;
      return Future.value(false);
    }
  }

  Future<Token> _doRequest(Map<String, dynamic> data, String baseUrl) async {
    var response = await HttpClient.instance().post<Map<String, dynamic>>('$baseUrl$_path', data);
    return Token.fromJson(response.body());
  }

  @visibleForTesting
  void fullReset() {
    _instance = LoginModel._internal();
  }

  String? get authorizeUrl => _authorizeUrl;

  String? get callbackUrl => _redirectUri;
}
