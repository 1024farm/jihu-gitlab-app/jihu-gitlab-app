import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl_standalone.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/db_manager.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/package_device_info.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/stars_provider.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/generated/l10n.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/l10n/current_locale.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';
import 'package:jihu_gitlab_app/routers.dart';
import 'package:provider/provider.dart';

// coverage:ignore-start
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await LocalStorage.init();
  await PackageDeviceInfo.loadInfo();
  await RootStore().restore();
  await ConnectionProvider().restore();
  await DbManager.instance().open();
  await findSystemLocale();
  setupLocator();
  runApp(const App());
}

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
// coverage:ignore-end

class App extends StatefulWidget {
  const App({super.key});

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> with RestorationMixin {
  final _RestorableStarterState _appState = _RestorableStarterState();

  @override
  String get restorationId => 'jiHuAppState';

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {
    registerForRestoration(_appState, 'state');
  }

  @override
  void dispose() {
    _appState.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<RootStore>.value(value: _appState.value),
          ChangeNotifierProvider(create: (context) => ConnectionProvider()),
          ChangeNotifierProvider(create: (context) => LocaleProvider()),
          ChangeNotifierProvider(create: (context) => StarsProvider()),
          ChangeNotifierProvider(create: (context) => ProjectProvider())
        ],
        child: Consumer<LocaleProvider>(builder: (context, currentLocale, child) {
          return MaterialApp(
            onGenerateTitle: (BuildContext context) => AppLocalizations.dictionary().appName,
            debugShowCheckedModeBanner: false,
            useInheritedMediaQuery: true,
            theme: AppThemeData.lightThemeData,
            darkTheme: AppThemeData.darkThemeData,
            home: const RootPage(),
            onGenerateRoute: onGenerateRoute,
            localizationsDelegates: const [GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate, GlobalWidgetsLocalizations.delegate, S.delegate],
            supportedLocales: const [Locale('en'), Locale('zh')],
            navigatorKey: navigatorKey,
            locale: LocalStorage.get("language", "auto") == "auto" ? currentLocale.value : Locale(LocalStorage.get("language", "auto")),
          );
        }));
  }
}

// coverage:ignore-start
class _RestorableStarterState extends RestorableListenable<RootStore> {
  @override
  RootStore createDefaultValue() {
    return RootStore();
  }

  @override
  RootStore fromPrimitives(Object? data) {
    final appState = RootStore();
    final appData = Map<String, dynamic>.from(data as Map);
    final pageIndex = appData['selectedPage'] as int;
    appState.selectedIndex = pageIndex;
    return appState;
  }

  @override
  Object toPrimitives() {
    return <String, dynamic>{'selectedPage': value.selectedIndex};
  }
}
// coverage:ignore-end
