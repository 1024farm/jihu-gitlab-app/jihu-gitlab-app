import 'package:jihu_gitlab_app/core/graph_ql_page_info.dart';

class PaginatedGraphQLResponse<T> {
  final GraphQLPageInfo _pageInfo;
  final List<T> _list;

  PaginatedGraphQLResponse(this._pageInfo, this._list);

  List<T> get list => _list;

  GraphQLPageInfo get pageInfo => _pageInfo;
}
