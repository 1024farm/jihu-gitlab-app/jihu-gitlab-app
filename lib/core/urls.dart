class Urls {
  final String _impl;

  Urls(this._impl);

  List<String> get urls {
    List<String> result = [];
    String mid = _impl;
    while (mid.contains('http://') || mid.contains('https://')) {
      int start = _getFirstUrlStartIndex(mid);
      mid = mid.substring(start);
      int end = _getFirstUrlEndIndex(mid);
      result.add(mid.substring(0, end));
      mid = mid.substring(end);
    }
    return result;
  }

  int _getFirstUrlStartIndex(String str) {
    var httpsIndex = str.indexOf('https://');
    var httpIndex = str.indexOf('http://');
    if (httpsIndex == -1 && httpIndex == -1) return -1;
    if (httpsIndex == -1) return httpIndex;
    if (httpIndex == -1) return httpsIndex;
    return httpsIndex > httpIndex ? httpIndex : httpsIndex;
  }

  int _getFirstUrlEndIndex(String str) {
    const tags = [',', '，', '。', ';', '；', '{', '}', '「', '」', '<', '>', '《', '》', '(', ')', '（', '）', '|', '｜', '[', ']', '【', '】', '"', '“', '”', '’', '‘', "'", ' ', '\n'];
    int result = -1;
    for (var tag in tags) {
      int index = str.indexOf(tag);
      if (index == -1) continue;
      if (result == -1 || index < result) result = index;
    }
    return result == -1 ? str.length : result;
  }
}
