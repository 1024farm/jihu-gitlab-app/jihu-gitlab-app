import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';

class AssetSvg {
  final String assetName;
  final Color color;
  final Size? size;
  final String? semanticsLabel;

  AssetSvg(this.assetName, {this.color = AppThemeData.iconColor, this.size = const Size(16, 16), this.semanticsLabel});

  SvgPicture asPicture() {
    return SvgPicture.asset(
      assetName,
      colorFilter: ColorFilter.mode(color, BlendMode.srcIn),
      width: size?.width,
      height: size?.height,
      semanticsLabel: semanticsLabel,
    );
  }
}
