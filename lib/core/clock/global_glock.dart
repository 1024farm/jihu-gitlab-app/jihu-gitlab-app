import 'package:jihu_gitlab_app/core/clock/global_glock_setter.dart';

class GlobalClock {
  static DateTime now() {
    return GlobalClockSetter.dateTime ?? DateTime.now();
  }
}
