class GlobalClockSetter {
  static DateTime? _dateTime;

  static void fixAt(DateTime dateTime) {
    _dateTime = dateTime;
  }

  static void reset() {
    _dateTime = null;
  }

  static DateTime? get dateTime => _dateTime;
}
