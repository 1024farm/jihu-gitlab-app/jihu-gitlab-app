import 'dart:io';

import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';

class InstallApkPlugin {
  static final InstallApkPlugin _instance = InstallApkPlugin._internal();

  static const _methodChannel = MethodChannel('app_installer');

  factory InstallApkPlugin() {
    return _instance;
  }

  InstallApkPlugin._internal();

  Future<void> install() async {
    if (Platform.isAndroid) {
      Directory tempDir = await getTemporaryDirectory();
      Directory updateDirectory = Directory('${tempDir.path}/Updater/');
      File file = File('${updateDirectory.path}/app_complete_update.apk');
      _methodChannel.invokeMethod('installApk', {'apkPath': file.path});
    }
  }
}
