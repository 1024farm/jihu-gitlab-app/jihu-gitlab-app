import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_highlighter/flutter_highlighter.dart';
import 'package:flutter_highlighter/themes/atom-one-light.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:jihu_gitlab_app/core/browser_launcher.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/file_uploader.dart';
import 'package:jihu_gitlab_app/core/widgets/markdown/markdown_style.dart';
import 'package:jihu_gitlab_app/core/widgets/toast.dart';
import 'package:jihu_gitlab_app/core/widgets/video_player_page.dart';
import 'package:markdown/markdown.dart' as md;
import 'package:path/path.dart';
import 'package:photo_view/photo_view.dart';

class MarkdownView extends Markdown {
  MarkdownView({
    required String data,
    required BuildContext context,
    EdgeInsets padding = const EdgeInsets.only(top: 16),
    ScrollPhysics? physics,
    List<md.InlineSyntax>? inlineSyntaxes,
    Map<String, MarkdownElementBuilder>? builders,
    String? specificHost,
    super.key,
  }) : super(
            shrinkWrap: true,
            data: data,
            selectable: true,
            padding: padding,
            physics: physics,
            onTapLink: (String text, String? href, String title) => _linkOnTapHandler(context, text, href, title),
            styleSheet: MarkdownStyle(),
            builders: builders ?? {'code': CodeElementBuilder()},
            inlineSyntaxes: inlineSyntaxes,
            imageBuilder: (Uri uri, String? title, String? alt) => _imageBuilder(context, uri, title, alt, specificHost));

  static Future<void> _linkOnTapHandler(
    BuildContext context,
    String text,
    String? href,
    String title,
  ) async {
    if (text.startsWith("^")) {
      Toast.show(href ?? '');
      return;
    }
    UrlLauncher.open(context, href ?? '');
  }
}

class CodeElementBuilder extends MarkdownElementBuilder {
  @override
  Widget? visitElementAfter(element, TextStyle? preferredStyle) {
    var language = '';
    if (element.attributes['class'] == null) {
      return super.visitElementAfter(element, preferredStyle);
    }
    String lg = element.attributes['class'] as String;
    language = lg.substring(9);
    return SizedBox(
      width: MediaQueryData.fromWindow(WidgetsBinding.instance.window).size.width,
      child: HighlightView(
        element.textContent,
        language: language,
        theme: atomOneLightTheme,
        padding: const EdgeInsets.all(8),
      ),
    );
  }
}

var _imageBuilder = (BuildContext context, Uri uri, String? title, String? alt, String? specificHost) {
  String ext = extension(uri.toString());
  var fileType = FileType.match(ext);
  if (fileType == FileType.video) {
    return Container(
      margin: const EdgeInsets.all(4),
      height: 100,
      width: 150,
      decoration: const BoxDecoration(color: Color(0xFF45465E)),
      child: Center(
        child: IconButton(iconSize: 48, onPressed: () => _startVideo(context, uri.toString()), icon: const Icon(Icons.play_circle_fill_outlined, color: Colors.grey)),
      ),
    );
  }
  return _buildImage(context, uri, title, alt, specificHost);
};

void _startVideo(BuildContext context, String url) {
  Navigator.of(context).pushNamed(VideoPlayerPage.routeName, arguments: {"url": url});
}

Widget _buildImage(BuildContext context, Uri uri, String? title, String? alt, String? specificHost) {
  var url = (specificHost ?? (ConnectionProvider.currentBaseUrl.get == '' ? 'https://jihulab.com' : ConnectionProvider.currentBaseUrl.get)) + uri.path;
  return InkWell(
    onTap: () {
      showGeneralDialog(
          context: context,
          barrierDismissible: true,
          transitionDuration: const Duration(milliseconds: 300),
          barrierLabel: '',
          barrierColor: Colors.transparent,
          pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) {
            return Material(
              type: MaterialType.transparency,
              child: InkWell(onTap: () => Navigator.pop(context), child: PhotoView(imageProvider: CachedNetworkImageProvider(url))),
            );
          });
    },
    child: Padding(
      padding: const EdgeInsets.all(4),
      child: Image.network(url, errorBuilder: (buildContext, object, trace) => Row()),
    ),
  );
}
