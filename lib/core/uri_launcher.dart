import 'package:flutter/cupertino.dart';
import 'package:url_launcher/url_launcher.dart';

class UriLauncher {
  static UriLauncher _instance = UriLauncher._internal();

  UriLauncher._internal();

  factory UriLauncher.instance() => _instance;

  Future<bool> canLaunch(Uri uri) {
    return canLaunchUrl(uri);
  }

  Future<bool> launch(
    Uri uri, {
    LaunchMode mode = LaunchMode.platformDefault,
    WebViewConfiguration webViewConfiguration = const WebViewConfiguration(),
    String? webOnlyWindowName,
  }) {
    return launchUrl(uri, mode: mode, webOnlyWindowName: webOnlyWindowName, webViewConfiguration: webViewConfiguration);
  }

  @visibleForTesting
  void fullReset() {
    _instance = UriLauncher._internal();
  }

  @visibleForTesting
  void injectInstanceForTesting(UriLauncher mockUriLauncher) {
    _instance = mockUriLauncher;
  }
}
