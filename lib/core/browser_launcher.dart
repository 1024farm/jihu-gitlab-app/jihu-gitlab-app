import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/toast.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:url_launcher/url_launcher.dart';

class UrlLauncher {
  static void open(BuildContext context, String url, {bool shouldLaunchBrowserAlert = true}) async {
    if (shouldLaunchBrowserAlert) {
      UrlLauncher._showDialog(context, url);
    } else {
      UrlLauncher._openBrowser(url);
    }
  }

  static void _showDialog(BuildContext context, String url) async {
    await showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
              content: Text(
                AppLocalizations.dictionary().openBrowserAlertContent,
                style: const TextStyle(color: Color(0XFF03162F), fontSize: 14),
              ),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text(
                    AppLocalizations.dictionary().cancel,
                    style: const TextStyle(color: AppThemeData.secondaryColor, fontSize: 14),
                  ),
                ),
                TextButton(
                  onPressed: () {
                    Navigator.pop(context, AppLocalizations.dictionary().ok);
                    _openBrowser(url);
                  },
                  child: Text(
                    AppLocalizations.dictionary().open,
                    style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 14),
                  ),
                )
              ]);
        });
  }

  static Future _openBrowser(String url) async {
    isCalledUrlLauncherSuccess = true;
    // coverage:ignore-start
    if (!await launchUrl(Uri.parse(url), mode: LaunchMode.externalApplication)) {
      Toast.show(AppLocalizations.dictionary().openUrlFailed(url));
      isCalledUrlLauncherSuccess = false;
    }
    // coverage:ignore-end
  }

  @visibleForTesting
  static bool isCalledUrlLauncherSuccess = false;

  @visibleForTesting
  static void reset() {
    isCalledUrlLauncherSuccess = false;
  }
}
