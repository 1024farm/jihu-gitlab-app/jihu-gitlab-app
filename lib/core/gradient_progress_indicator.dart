import 'package:flutter/material.dart';

class GradientProgressIndicator extends StatelessWidget {
  final double progress;
  final Color first;
  final Color second;
  final double height;

  const GradientProgressIndicator({required this.progress, required this.first, required this.second, required this.height, super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          gradient: LinearGradient(colors: [
            first,
            second,
            const Color(0xFFF2F2F2),
          ], stops: [
            progress / 2,
            progress,
            progress
          ])),
      child: SizedBox(height: height),
    );
  }
}
