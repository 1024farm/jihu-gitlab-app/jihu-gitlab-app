import 'package:flutter/cupertino.dart';
import 'package:jihu_gitlab_app/core/system_version_detector.dart';
import 'package:jihu_gitlab_app/core/widgets/warning_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class LowSystemVersionView extends StatelessWidget {
  final SystemType systemType;
  final bool isForGeek;

  const LowSystemVersionView({required this.systemType, required this.isForGeek, super.key});

  @override
  Widget build(BuildContext context) {
    return WarningView(title: title, description: systemType.description(isForGeek: isForGeek));
  }

  String get title => isForGeek && systemType == SystemType.maintainable ? AppLocalizations.dictionary().geekExclusive : AppLocalizations.dictionary().securityWarning;
}
