import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:jihu_gitlab_app/core/app_platform.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:permission_handler/permission_handler.dart';

class PhotoPicker {
  static final ImagePicker _picker = ImagePicker();

  Future<bool> checkAccess() async {
    Permission permission = AppPlatform.isAndroid ? Permission.storage : Permission.photos;
    final status = await permission.request();
    return status != PermissionStatus.denied;
  }

  Future<void> requireAccess(BuildContext context) async {
    await showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
              content: Column(
                children: [
                  Text(AppLocalizations.dictionary().accessDenied, style: const TextStyle(color: Color(0XFF03162F), fontSize: 14)),
                  const SizedBox(height: 4),
                  Text(AppLocalizations.dictionary().chooseImageMessage, style: const TextStyle(color: AppThemeData.secondaryColor, fontSize: 12))
                ],
              ),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text(AppLocalizations.dictionary().cancel, style: const TextStyle(color: AppThemeData.secondaryColor, fontSize: 14)),
                ),
                TextButton(
                  onPressed: () {
                    openAppSettings();
                  },
                  child: Text(AppLocalizations.dictionary().confirm, style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 14)),
                )
              ]);
        });
  }

  Future<PhotoPickerResult> pickImage() async {
    try {
      final XFile? pickedFile = await _picker.pickImage(source: ImageSource.gallery);
      return PhotoPickerResult.success(pickedFile);
    } catch (e) {
      LogHelper.err('Pick photo from system error', e);
      return PhotoPickerResult.fail(e);
    }
  }

  Future<PhotoPickerResult> pickVideo() async {
    try {
      final XFile? pickedFile = await _picker.pickVideo(source: ImageSource.gallery);
      return PhotoPickerResult.success(pickedFile);
    } catch (e) {
      LogHelper.err('Pick photo from system error', e);
      return PhotoPickerResult.fail(e);
    }
  }

  Future<PhotoPickerResult> retrieveLostData() async {
    if (AppPlatform.isIOS) {
      return PhotoPickerResult.success(null);
    }
    final LostDataResponse response = await _picker.retrieveLostData();
    if (response.isEmpty) return PhotoPickerResult.success(null);
    if (response.file == null) {
      return PhotoPickerResult.fail(response.exception?.code ?? response);
    }
    return PhotoPickerResult.success(response.file);
  }
}

class PhotoPickerResult {
  List<XFile> files;
  Object? error;

  PhotoPickerResult._internal({required this.files, this.error});

  factory PhotoPickerResult.success(XFile? file) {
    return PhotoPickerResult._internal(files: file == null ? [] : [file]);
  }

  factory PhotoPickerResult.fail(Object? error) {
    return PhotoPickerResult._internal(files: [], error: error);
  }

  bool get isPhotoAccessDeniedError {
    if (error == null) return false;
    if (error is! PlatformException) return false;
    var exception = error as PlatformException;
    if (exception.code != 'photo_access_denied') return false;
    return true;
  }
}
