import 'package:flutter/material.dart';

class StreamingContainer extends StatefulWidget {
  final List<Widget> children;
  final double spacing;
  final double? runSpacing;

  const StreamingContainer({required this.children, this.spacing = 8, this.runSpacing, Key? key}) : super(key: key);

  @override
  State<StreamingContainer> createState() => _StreamingContainerState();
}

class _StreamingContainerState extends State<StreamingContainer> {
  @override
  Widget build(BuildContext context) {
    return Wrap(
      spacing: widget.spacing,
      runSpacing: widget.runSpacing ?? widget.spacing,
      alignment: WrapAlignment.start,
      crossAxisAlignment: WrapCrossAlignment.center,
      direction: Axis.horizontal,
      children: widget.children,
    );
  }
}
