import 'package:flutter/material.dart';

class RoundRectContainer extends Container {
  RoundRectContainer({
    super.margin,
    super.padding,
    super.child,
    super.decoration = const BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(4))),
    super.key,
  });
}
