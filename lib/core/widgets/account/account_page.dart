import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/account_list_view.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class AccountPage extends StatefulWidget {
  static String routeName = "AccountPage";

  const AccountPage({super.key});

  @override
  State<AccountPage> createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  @override
  Widget build(BuildContext context) {
    return Consumer<ConnectionProvider>(builder: (context, _, child) {
      return Scaffold(
        appBar: CommonAppBar(title: Text(AppLocalizations.dictionary().accounts), showLeading: true),
        body: SafeArea(
          child: _buildBody(),
        ),
        backgroundColor: Theme.of(context).colorScheme.background,
      );
    });
  }

  Widget _buildBody() {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
          decoration: const BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(4))),
          child: Column(children: accountItems(context, isAccountsPage: true)),
        ),
        const Expanded(child: SizedBox()),
      ],
    );
  }
}
