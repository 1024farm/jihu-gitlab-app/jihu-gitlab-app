import 'package:flutter/cupertino.dart';
import 'package:jihu_gitlab_app/core/domain/low_permission_information.dart';
import 'package:jihu_gitlab_app/core/widgets/warning_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class PermissionLowView extends StatelessWidget {
  final String featureName;

  const PermissionLowView({required this.featureName, super.key});

  @override
  Widget build(BuildContext context) {
    return WarningView(
      title: AppLocalizations.dictionary().warningNoticeTitle,
      description: '${LowPermissionInformation.warningNotice(featureName)}\n${LowPermissionInformation.warningNoticeSupportLine()}',
      email: LowPermissionInformation.email(featureName),
    );
  }
}
