import 'package:flutter/material.dart';

void renderBottomSheet(Widget child, BuildContext context) {
  showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (context) {
        return StatefulBuilder(
          builder: (context1, setBottomState) {
            return GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                FocusScope.of(context).requestFocus(FocusNode());
              },
              child: SafeArea(
                child: AnimatedPadding(
                  padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
                  duration: const Duration(milliseconds: 100),
                  child: SingleChildScrollView(child: child),
                ),
              ),
            );
          },
        );
      });
}
