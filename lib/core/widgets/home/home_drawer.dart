import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/widgets/user/user_info_view.dart';

class HomeDrawer extends Drawer {
  const HomeDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return const Drawer(backgroundColor: Color(0xFFF8F8FA), child: UserInfoView());
  }
}
