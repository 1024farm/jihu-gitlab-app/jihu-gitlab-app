import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/global_keys.dart';
import 'package:jihu_gitlab_app/core/layout/adaptive.dart';
import 'package:jihu_gitlab_app/core/widgets/home/home_app_bar.dart';

class HomeScaffold extends Scaffold {
  const HomeScaffold({Key? key, Widget? drawer, AppBar? appBar, Widget? body, Color? backgroundColor})
      : super(key: key, resizeToAvoidBottomInset: false, drawer: drawer, appBar: appBar, body: body, backgroundColor: backgroundColor);

  factory HomeScaffold.withAppBar({required HomeAppBarRequiredParams params, Widget? body, Color? backgroundColor}) {
    HomeAppBar appBar = params.toAppBar();
    return HomeScaffold(appBar: appBar, body: body, backgroundColor: backgroundColor);
  }
}

class HomeAppBarRequiredParams {
  BuildContext context;
  String? title;
  bool? canGoBack;
  Widget? goBackLeading;
  TextStyle? titleTextStyle;
  List<Widget>? actions;
  Widget? flexibleSpace;

  HomeAppBarRequiredParams({required this.context, this.title, this.canGoBack, this.goBackLeading, this.titleTextStyle, this.actions, this.flexibleSpace});

  HomeAppBar toAppBar() {
    return HomeAppBar.create(
        title: title != null ? Text(title!) : null,
        canGoBack: canGoBack,
        goBackLeading: goBackLeading,
        titleTextStyle: titleTextStyle,
        actions: [...?actions, const SizedBox(width: 18)],
        flexibleSpace: flexibleSpace,
        orElseLeading: isDesktopLayout(context)
            ? null
            : InkWell(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset("assets/images/menu.svg", width: 20, height: 20, key: Key('menu_$title')),
                  ],
                ),
                onTap: () {
                  GlobalKeys.rootScaffoldKey.currentState?.openDrawer();
                },
              ));
  }
}
