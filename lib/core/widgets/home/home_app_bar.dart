import 'package:flutter/material.dart';

typedef WebViewCreatedCallback = void Function();

class HomeAppBar extends AppBar {
  HomeAppBar({required double elevation, Key? key, Widget? title, Widget? leading, TextStyle? titleTextStyle, List<Widget>? actions, Widget? flexibleSpace})
      : super(
            key: key,
            title: title,
            leading: leading,
            centerTitle: true,
            titleTextStyle: titleTextStyle,
            elevation: elevation,
            actions: actions,
            iconTheme: const IconThemeData(color: Colors.black87),
            flexibleSpace: flexibleSpace);

  factory HomeAppBar.create({Widget? title, bool? canGoBack, Widget? goBackLeading, TextStyle? titleTextStyle, List<Widget>? actions, Widget? orElseLeading, Widget? flexibleSpace}) {
    Widget? leading;

    if (canGoBack ?? false) {
      leading = goBackLeading;
    } else {
      leading = orElseLeading;
    }
    return HomeAppBar(
      title: title,
      leading: leading,
      titleTextStyle: titleTextStyle ?? const TextStyle(color: Color(0xFF171321), fontSize: 16, fontWeight: FontWeight.w600),
      elevation: 0,
      actions: actions,
      flexibleSpace: flexibleSpace,
    );
  }
}
