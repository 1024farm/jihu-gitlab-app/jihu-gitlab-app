import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/svg_text.dart';

class ToggleButton extends StatefulWidget {
  const ToggleButton({
    required this.svgPath,
    required this.title,
    required this.selectedProvider,
    required this.onChange,
    super.key,
  });

  final String svgPath;
  final String title;
  final bool Function() selectedProvider;
  final Function() onChange;

  @override
  State<ToggleButton> createState() => _ToggleButtonState();
}

class _ToggleButtonState extends State<ToggleButton> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        widget.onChange();
        setState(() {});
      },
      child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
          decoration: BoxDecoration(
            color: widget.selectedProvider() ? const Color(0xFFFFEBE2) : Colors.white,
            borderRadius: BorderRadius.circular(4),
            border: widget.selectedProvider() ? Border.all(color: const Color(0xFFFFEBE2), width: 1) : Border.all(color: const Color(0xFFEAEAEA), width: 1),
          ),
          child: SvgText(
            svgAssetPath: widget.svgPath,
            svgColor: widget.selectedProvider() ? Theme.of(context).primaryColor : AppThemeData.secondaryColor,
            text: widget.title,
            textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: widget.selectedProvider() ? Theme.of(context).primaryColor : const Color(0xFF03162F)),
          )),
    );
  }
}
