import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/asset_svg.dart';

class SvgButton extends StatelessWidget {
  const SvgButton({required this.assetSvg, this.onTap, this.boxDecoration, this.padding, super.key});

  final AssetSvg assetSvg;
  final BoxDecoration? boxDecoration;
  final GestureTapCallback? onTap;
  final EdgeInsetsGeometry? padding;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(decoration: boxDecoration, padding: padding, child: assetSvg.asPicture()),
    );
  }
}
