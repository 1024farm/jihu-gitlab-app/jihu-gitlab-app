import 'package:flutter/material.dart';

class DelimitedColumn extends StatelessWidget {
  const DelimitedColumn({required this.divider, required this.children, this.crossAxisAlignment = CrossAxisAlignment.center, Key? key}) : super(key: key);
  final Widget divider;
  final List<Widget> children;
  final CrossAxisAlignment crossAxisAlignment;

  @override
  Widget build(BuildContext context) {
    var list = children.map((e) => [e, divider]).fold<List<Widget>>(<Widget>[], (previousValue, element) {
      previousValue.addAll(element);
      return previousValue;
    });
    if (list.isNotEmpty) {
      list.removeLast();
    }
    return Column(
      crossAxisAlignment: crossAxisAlignment,
      children: list,
    );
  }
}
