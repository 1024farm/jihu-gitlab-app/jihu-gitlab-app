import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ClosableView extends StatelessWidget {
  final Widget child;
  final VoidCallback? onClosed;

  const ClosableView({required this.child, this.onClosed, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 4),
      decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(4)), color: Color(0XFFF8F8F8)),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Flexible(child: child),
          InkWell(
            onTap: onClosed,
            child: Container(
              // decoration: BoxDecoration(color: Colors.red),
              padding: const EdgeInsets.all(8.0),
              child: SvgPicture.asset(
                "assets/images/clear.svg",
                height: 24,
                width: 24,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
