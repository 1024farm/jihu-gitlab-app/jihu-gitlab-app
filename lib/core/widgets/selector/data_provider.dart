abstract class DataProvider<T> {
  Future<List<T>> loadFromLocal();

  Future<bool> syncFromRemote();
}
