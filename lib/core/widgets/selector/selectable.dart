import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/selector/closable_view.dart';
import 'package:jihu_gitlab_app/core/widgets/selector/data_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/selector/selector.dart';

typedef OnSelected<T> = void Function(List<T> data);
typedef SelectedItemBuilder<T> = Widget Function(T t);
typedef MultiSelection = Future<bool> Function();
typedef WarningNotice = Future<String> Function();
typedef DataProviderProvider<T> = DataProvider<T> Function();

// TODO Rename
class Selectable<T> extends StatefulWidget {
  const Selectable(
      {required this.onSelected,
      required this.selectedItemBuilder,
      required this.selectorItemBuilder,
      required this.dataProviderProvider,
      required this.keyMapper,
      required this.title,
      required this.pageTitle,
      required this.placeHolder,
      this.projectId,
      this.filter,
      this.multiSelection,
      this.warningNotice,
      this.initialData,
      Key? key})
      : super(key: key);

  final OnSelected<T> onSelected;
  final SelectedItemBuilder<T> selectedItemBuilder;
  final SelectorItemBuilder<T> selectorItemBuilder;
  final DataProviderProvider<T> dataProviderProvider;
  final KeyMapper<int, T> keyMapper;
  final String title;
  final String pageTitle;
  final String placeHolder;
  final int? projectId;
  final Filter<T>? filter;
  final MultiSelection? multiSelection;
  final WarningNotice? warningNotice;
  final List<T>? initialData;

  @override
  State<Selectable> createState() => _SelectableState<T>();
}

class _SelectableState<T> extends State<Selectable<T>> {
  List<T> _selectedData = [];
  bool _initialized = false;

  bool get hasNoData => _selectedData.isEmpty;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        bool multiSelection = await widget.multiSelection?.call() ?? false;
        String warningNotice = await widget.warningNotice?.call() ?? '';
        List<T>? selectedData = await _showSelector(multiSelection, warningNotice);
        if (selectedData != null) {
          setState(() {
            _selectedData.clear();
            _selectedData.addAll(selectedData);
          });
          _onSelectedCallback();
        }
      },
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(widget.title, style: const TextStyle(color: Color(0XFF03162F), fontSize: 16, fontWeight: FontWeight.w600)),
          ),
          _buildSelector()
        ],
      ),
    );
  }

  Widget _buildSelector() {
    var iconButton = IconButton(
        key: const Key("gotoSelector"),
        onPressed: () async {
          bool multiSelection = await widget.multiSelection?.call() ?? false;
          String warningNotice = await widget.warningNotice?.call() ?? '';
          List<T>? selectedData = await _showSelector(multiSelection, warningNotice);
          if (selectedData != null) {
            setState(() {
              _selectedData.clear();
              _selectedData.addAll(selectedData);
            });
            _onSelectedCallback();
          }
        },
        icon: const Icon(Icons.add, size: 22, color: Color(0xFF87878C)));
    final List<Widget> children = [];
    if (hasNoData) {
      children.add(Text(widget.placeHolder, style: const TextStyle(color: AppThemeData.secondaryColor, fontSize: 14, fontWeight: FontWeight.w400)));
    } else {
      for (var e in _selectedData) {
        children.add(_buildSelectedItem(e));
      }
    }
    children.add(Container(transform: Matrix4.translationValues(12, 0, 0), child: iconButton));
    return Container(
      width: double.infinity,
      decoration: const BoxDecoration(border: Border.fromBorderSide(BorderSide(color: Color(0XFFEAEAEA), width: 1)), borderRadius: BorderRadius.all(Radius.circular(4)), color: Colors.white),
      padding: EdgeInsets.symmetric(vertical: hasNoData ? 0 : 8, horizontal: 8),
      margin: const EdgeInsets.only(left: 16, right: 16),
      child: Wrap(
        spacing: 8,
        runSpacing: 8,
        alignment: hasNoData ? WrapAlignment.spaceBetween : WrapAlignment.start,
        crossAxisAlignment: WrapCrossAlignment.center,
        direction: Axis.horizontal,
        children: children,
      ),
    );
  }

  Future<List<T>?> _showSelector(bool multiSelection, String warningNotice) async {
    var result = await Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return Selector<int, T>(
          dataProvider: widget.dataProviderProvider(),
          itemBuilder: widget.selectorItemBuilder,
          filter: widget.filter,
          title: widget.pageTitle,
          multiSelection: multiSelection,
          warningNotice: warningNotice,
          keyMapper: widget.keyMapper,
          projectId: widget.projectId,
          selectedKeysProvider: () => _selectedData.map((t) => widget.keyMapper(t)).toList());
    }));
    return Future(() => result == null ? null : result as List<T>);
  }

  Widget _buildSelectedItem(T t) {
    return ClosableView(
        onClosed: () {
          setState(() {
            _selectedData.remove(t);
          });
          _onSelectedCallback();
        },
        child: widget.selectedItemBuilder(t));
  }

  @override
  void didUpdateWidget(covariant Selectable<T> oldWidget) {
    if (!_initialized && (widget.initialData ?? []).isNotEmpty) {
      _selectedData.addAll(widget.initialData!);
      _selectedData = _selectedData.toSet().toList();
      _initialized = true;
    }
    super.didUpdateWidget(oldWidget);
  }

  void _onSelectedCallback() {
    widget.onSelected(_selectedData);
  }
}
