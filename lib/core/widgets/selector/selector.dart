import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/domain/low_permission_information.dart';
import 'package:jihu_gitlab_app/core/domain/url_redictor.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:jihu_gitlab_app/core/widgets/linker_text.dart';
import 'package:jihu_gitlab_app/core/widgets/search_box.dart';
import 'package:jihu_gitlab_app/core/widgets/search_no_result_view.dart';
import 'package:jihu_gitlab_app/core/widgets/selector/data_provider.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label_creation_page.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label_manage_page.dart';
import 'package:jihu_gitlab_app/core/domain/project.dart';

typedef Filter<Domain> = bool Function(String keyword, Domain domain);
typedef SelectorItemBuilder<Domain> = Widget Function(BuildContext context, int index, Domain domain);
typedef SelectedKeysProvider<Identity> = List<Identity> Function();
typedef KeyMapper<Identity, Domain> = Identity Function(Domain domain);

class Selector<Identity, Domain> extends StatefulWidget {
  final SelectorItemBuilder<Domain> itemBuilder;
  final bool multiSelection;
  final String warningNotice;
  final String title;
  final DataProvider<Domain> dataProvider;
  final KeyMapper<Identity, Domain> keyMapper;
  final Filter<Domain>? filter;
  final int? projectId;
  final SelectedKeysProvider<Identity>? selectedKeysProvider;

  const Selector(
      {required this.itemBuilder,
      required this.dataProvider,
      required this.keyMapper,
      this.filter,
      this.selectedKeysProvider,
      this.title = '',
      this.multiSelection = false,
      this.warningNotice = '',
      this.projectId,
      Key? key})
      : super(key: key);

  @override
  State<Selector> createState() => _SelectorState<Identity, Domain>();
}

class _SelectorState<Identity, Domain> extends State<Selector<Identity, Domain>> {
  final TextEditingController _textEditingController = TextEditingController();
  final Map<Identity, bool> _selectedItems = {};
  List<Domain> _data = <Domain>[];
  late ValueNotifier<List<Domain>> _notifier;
  bool _showWarningNotice = false;
  late Project project;

  @override
  void initState() {
    super.initState();
    _notifier = ValueNotifier(_data);
    _loadDataAndRebuild();
    widget.dataProvider.syncFromRemote().then((value) => _loadDataAndRebuild());
    _loadProject();
  }

  void _loadDataAndRebuild() {
    widget.dataProvider.loadFromLocal().then((value) {
      _data = value;
      List<Identity>? selectedKeys = widget.selectedKeysProvider?.call();
      if (selectedKeys != null && selectedKeys.isNotEmpty) {
        _data.where((element) => selectedKeys.contains(widget.keyMapper(element))).forEach((element) {
          _selectedItems[widget.keyMapper(element)] = true;
        });
      }
      _notifier.value = _data;
      setState(() {
        _showWarningNotice = !widget.multiSelection && selectedData.isNotEmpty;
      });
    });
  }

  void _loadProject() {
    if (widget.title == AppLocalizations.dictionary().selectLabels) {
      Project.getById(widget.projectId ?? 0).then((project) {
        setState(() {
          this.project = project;
        });
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    _textEditingController.dispose();
  }

  List<Domain> get selectedData {
    List<Identity> selectedKeys = _selectedItems.entries.where((element) => element.value).map((e) => e.key).toList();
    return _data.where((element) => selectedKeys.contains(widget.keyMapper(element))).toList();
  }

  @override
  Widget build(BuildContext context) {
    double bottomNoticeBoxHeight = 70.0;
    return Scaffold(
        appBar: CommonAppBar(
          showLeading: true,
          title: Text(widget.title),
          actions: [
            Offstage(
              offstage: !widget.multiSelection && widget.warningNotice.isEmpty,
              child: TextButton(
                  onPressed: () {
                    Navigator.pop(context, selectedData);
                  },
                  child: Text(AppLocalizations.dictionary().done, style: TextStyle(color: Theme.of(context).primaryColor))),
            )
          ],
        ),
        body: !_showWarningNotice
            ? _buildListView()
            : Stack(alignment: AlignmentDirectional.bottomCenter, children: [
                Container(
                  padding: EdgeInsets.only(bottom: bottomNoticeBoxHeight + MediaQuery.of(context).padding.bottom),
                  child: _buildListView(),
                ),
                Align(alignment: Alignment.bottomCenter, child: _buildBottomWarningBox(bottomNoticeBoxHeight))
              ]));
  }

  Column _buildBottomWarningBox(double bottomNoticeBoxHeight) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Container(
          height: bottomNoticeBoxHeight,
          decoration: BoxDecoration(color: Theme.of(context).primaryColor.withAlpha(30)),
          padding: const EdgeInsets.only(left: 16, right: 16),
          child: Row(
            children: [
              SvgPicture.asset('assets/images/warning.svg', height: 20, width: 20),
              const SizedBox(width: 10),
              Expanded(
                child: LinkerText(
                  text: widget.warningNotice,
                  options: const LinkifyOptions(humanize: false),
                  style: const TextStyle(color: Colors.black87),
                  onOpen: (link) async => UrlRedirector().redirect(link, context, LowPermissionInformation.email(AppLocalizations.dictionary().multiSelection)),
                ),
              )
            ],
          ),
        ),
        Container(
          height: MediaQuery.of(context).padding.bottom,
          color: Colors.white,
        )
      ],
    );
  }

  Widget _buildSearchBox() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
      child: SearchBox(
        searchController: _textEditingController,
        onClear: () {
          _notifier.value = _data;
          _textEditingController.clear();
        },
        onChanged: (keyword) => _notifier.value = _data.where((element) => widget.filter!.call(keyword, element)).toList(),
      ),
    );
  }

  Widget _buildListView() {
    return Column(
      children: [
        widget.filter != null ? _buildSearchBox() : const Divider(),
        Expanded(
          child: Container(
            decoration: const BoxDecoration(color: Colors.white),
            padding: const EdgeInsets.all(12.0),
            margin: const EdgeInsets.symmetric(vertical: 0, horizontal: 12.0),
            child: ValueListenableBuilder<List<Domain>>(
              valueListenable: _notifier,
              builder: (BuildContext context, List<Domain> data, Widget? child) {
                String search = _textEditingController.text;
                if (search.isNotEmpty && data.isEmpty) {
                  return SearchNoResultView(message: '${AppLocalizations.dictionary().noMatch}\'$search\'');
                }
                return ListView.separated(
                    itemBuilder: (context, index) => _buildListItem(index, data[index]),
                    separatorBuilder: (BuildContext context, int index) => const Divider(thickness: 12, height: 12, color: Colors.white),
                    itemCount: data.length);
              },
            ),
          ),
        ),
        if (widget.title == AppLocalizations.dictionary().selectLabels)
          Container(
            color: Colors.white,
            padding: const EdgeInsets.all(12.0),
            margin: const EdgeInsets.only(top: 12.0),
            width: double.infinity,
            alignment: Alignment.topCenter,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                InkWell(
                  onTap: _navigateToManageLabelPage,
                  child: Row(
                    children: [
                      const SizedBox(width: 10),
                      SvgPicture.asset("assets/images/edit_pencil.svg", semanticsLabel: "labels_edit", height: 16, width: 16),
                      const SizedBox(width: 15),
                      Text(AppLocalizations.dictionary().manageLabel),
                      const SizedBox(width: 10)
                    ],
                  ),
                ),
                const SizedBox(
                  width: 1,
                  height: 14,
                  child: DecoratedBox(
                    decoration: BoxDecoration(color: Colors.grey),
                  ),
                ),
                InkWell(
                  onTap: _navigateToCreateLabelPage,
                  child: Row(
                    children: [IconButton(onPressed: _navigateToCreateLabelPage, icon: const Icon(Icons.add, size: 22, color: Color(0xFF87878C))), Text(AppLocalizations.dictionary().createLabel)],
                  ),
                )
              ],
            ),
          ),
      ],
    );
  }

  void _navigateToCreateLabelPage() {
    Navigator.of(context)
        .push(MaterialPageRoute(
      builder: (context) => LabelCreationPage(projectId: widget.projectId, from: 'project'),
    ))
        .then((value) {
      if (value != null) {
        _loadDataAndRebuild();
        widget.dataProvider.syncFromRemote().then((value) => _loadDataAndRebuild());
      }
    });
  }

  void _navigateToManageLabelPage() {
    Navigator.of(context)
        .push(MaterialPageRoute(
      builder: (context) => LabelManagePage(
        projectId: widget.projectId,
        projectNameSpace: project.nameWithNamespace,
      ),
    ))
        .then((value) {
      _loadDataAndRebuild();
      widget.dataProvider.syncFromRemote().then((value) => _loadDataAndRebuild());
    });
  }

  Widget _buildListItem(int index, Domain domain) {
    bool isSelected = _selectedItems[widget.keyMapper(domain)] ?? false;
    void onItemTapped() => (_showWarningNotice && !isSelected) ? null : _onListTileTaped(domain);
    return InkWell(
      onTap: onItemTapped,
      child: Container(
        width: double.infinity,
        constraints: const BoxConstraints(minHeight: 44),
        margin: const EdgeInsets.only(left: 4),
        child: Row(
          children: <Widget>[
            Offstage(
                offstage: !widget.multiSelection && widget.warningNotice.isEmpty,
                child: Container(
                  margin: const EdgeInsets.only(right: 16),
                  height: 20,
                  width: 20,
                  decoration: (_showWarningNotice && !isSelected) ? BoxDecoration(color: Colors.grey.withAlpha(95), borderRadius: const BorderRadius.all(Radius.circular(10))) : null,
                  child: Transform.scale(
                    scale: 1.2,
                    child: Checkbox(
                      shape: const CircleBorder(),
                      side: const BorderSide(color: Colors.black38, width: 1),
                      activeColor: Theme.of(context).primaryColor,
                      value: isSelected,
                      onChanged: (value) => onItemTapped(),
                    ),
                  ),
                )),
            Flexible(child: widget.itemBuilder.call(context, index, domain))
          ],
        ),
      ),
    );
  }

  void _onListTileTaped(Domain domain) {
    if (!widget.multiSelection && widget.warningNotice.isEmpty) {
      Navigator.pop(context, [domain]);
      return;
    }
    if (widget.multiSelection) {
      _onTileTapped(domain);
      return;
    }
    Identity id = widget.keyMapper(domain);
    if (_itemDidSelect(id) && !_isCurrentTileSelected(id) && _hasOtherTileSelected()) return;
    _onTileTapped(domain);
    _showWarningNotice = _selectedItems[id] ?? false;
  }

  bool _isCurrentTileSelected(Identity id) => (_selectedItems[id] ?? false);

  bool _hasOtherTileSelected() => _selectedItems.values.where((element) => element).isNotEmpty;

  bool _itemDidSelect(Identity id) => _selectedItems.containsKey(id);

  void _onTileTapped(Domain domain) {
    setState(() {
      Identity id = widget.keyMapper(domain);
      _selectedItems[id] = !_isCurrentTileSelected(id);
    });
  }
}
