import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/clipboard.dart';
import 'package:jihu_gitlab_app/core/uri_launcher.dart';
import 'package:jihu_gitlab_app/core/widgets/toast.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_info.dart';
import 'package:url_launcher/url_launcher_string.dart';

class IssueShareView extends StatefulWidget {
  final String title;
  final String link;
  final IssueInfo? issueInfo;
  final VoidCallback? onCopyFinished;
  final VoidCallback? onEditIssue;
  final VoidCallback? onCloseOrReopenIssue;
  final VoidCallback? onUpdateIssueConfidential;
  final bool isFile;

  const IssueShareView(
      {required this.title,
      required this.link,
      required this.issueInfo,
      this.onCopyFinished,
      this.onCloseOrReopenIssue,
      Key? key,
      this.isFile = false,
      this.onEditIssue,
      this.onUpdateIssueConfidential})
      : super(key: key);

  @override
  State<IssueShareView> createState() => _IssueShareViewState();
}

class _IssueShareViewState extends State<IssueShareView> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (widget.issueInfo!.state != "closed")
          InkWell(
            onTap: widget.onEditIssue,
            child: Container(
              padding: const EdgeInsets.only(left: 10, right: 12, top: 6, bottom: 6),
              child: Row(
                children: [
                  SizedBox(
                      height: 40,
                      child: SvgPicture.asset(
                        'assets/images/edit_pencil.svg',
                        width: 18,
                        height: 18,
                      )),
                  const SizedBox(width: 10),
                  Expanded(child: Text(AppLocalizations.dictionary().editIssue, style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w400), overflow: TextOverflow.ellipsis)),
                ],
              ),
            ),
          ),
        if (widget.issueInfo!.state != "closed") Container(color: Colors.black12, height: 1),
        InkWell(
          onTap: widget.onCloseOrReopenIssue,
          child: Container(
            padding: const EdgeInsets.only(left: 12, right: 12, top: 6, bottom: 6),
            child: Row(
              children: [
                SizedBox(
                    height: 40,
                    child: SvgPicture.asset(widget.issueInfo!.state == "closed" ? 'assets/images/issues.svg' : 'assets/images/issue-closed.svg',
                        colorFilter: const ColorFilter.mode(Color(0xFF676A6E), BlendMode.srcIn), width: 16, height: 16)),
                const SizedBox(width: 10),
                Expanded(
                    child: Text(widget.issueInfo!.state == "closed" ? AppLocalizations.dictionary().reopen : AppLocalizations.dictionary().closeIssue,
                        style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w400), overflow: TextOverflow.ellipsis)),
              ],
            ),
          ),
        ),
        Container(color: Colors.black12, height: 4),
        InkWell(
          onTap: widget.onUpdateIssueConfidential,
          child: Container(
            padding: const EdgeInsets.only(left: 12, right: 12, top: 6, bottom: 6),
            child: Row(
              children: [
                SizedBox(
                    height: 40,
                    child: SvgPicture.asset(widget.issueInfo!.confidential ? 'assets/images/eye.svg' : 'assets/images/eye-slash.svg',
                        colorFilter: const ColorFilter.mode(Color(0xFF676A6E), BlendMode.srcIn), width: 16, height: 16)),
                const SizedBox(width: 10),
                Expanded(
                    child: Text(widget.issueInfo!.confidential ? AppLocalizations.dictionary().makePublic : AppLocalizations.dictionary().makeConfidential,
                        style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w400), overflow: TextOverflow.ellipsis)),
              ],
            ),
          ),
        ),
        Container(color: Colors.black12, height: 4),
        _buildItem('assets/images/link.svg', AppLocalizations.dictionary().copyLink, widget.link),
        Container(color: Colors.black12, height: 1),
        _buildItem(widget.isFile ? 'assets/images/copy_filename_and_link.svg' : 'assets/images/copy_title_and_link.svg',
            widget.isFile ? AppLocalizations.dictionary().copyFileLinkAndTitle : AppLocalizations.dictionary().copyLinkAndTitle, "${widget.title}\n${widget.link}"),
        Container(color: Colors.black12, height: 1),
        InkWell(
          child: Container(
            padding: const EdgeInsets.only(left: 12, right: 12, top: 6, bottom: 6),
            child: Row(
              children: [
                SizedBox(height: 40, child: SvgPicture.asset('assets/images/browser.svg', width: 16, height: 16)),
                const SizedBox(width: 8),
                Expanded(child: Text(AppLocalizations.dictionary().openInBrowser, style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w400), overflow: TextOverflow.ellipsis)),
              ],
            ),
          ),
          onTap: () => UriLauncher.instance().launch(Uri.parse(widget.link), mode: LaunchMode.externalApplication),
        )
      ],
    );
  }

  Widget _buildItem(String svgPath, String text, String targetText) {
    return InkWell(
      child: Container(
        padding: const EdgeInsets.only(left: 12, right: 12, top: 6, bottom: 6),
        child: Row(
          children: [
            SizedBox(height: 40, child: SvgPicture.asset(svgPath, width: 16, height: 16, colorFilter: const ColorFilter.mode(Color(0xFF676A6E), BlendMode.srcIn))),
            const SizedBox(width: 10),
            Expanded(child: Text(text, style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w400), overflow: TextOverflow.ellipsis)),
          ],
        ),
      ),
      onTap: () {
        Clipboard().setData(targetText);
        Toast.success(context, AppLocalizations.dictionary().copyToast);
        Navigator.pop(context);
        widget.onCopyFinished?.call();
      },
    );
  }
}
