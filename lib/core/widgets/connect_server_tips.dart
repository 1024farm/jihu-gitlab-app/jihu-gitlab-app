import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/themes/project_style.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/auth/login_page.dart';

class ConnectServerTips extends StatelessWidget {
  final String targetUrl;

  const ConnectServerTips({required this.targetUrl, super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(12),
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(borderRadius: const BorderRadius.all(Radius.circular(4)), border: Border.all(color: const Color(0xFFEAEAEA))),
      child: Center(
          child: Text.rich(
              TextSpan(text: '${AppLocalizations.dictionary().connectWith} ', children: [
                TextSpan(
                  text: targetUrl,
                  recognizer: TapGestureRecognizer()..onTap = onTap(context),
                  style: ProjectStyle.build(Theme.of(context).textTheme.bodySmall, Theme.of(context).colorScheme.primary),
                ),
                TextSpan(text: ' ${AppLocalizations.dictionary().toContinueDiscussion}'),
              ]),
              style: ProjectStyle.build(Theme.of(context).textTheme.bodySmall, Colors.black87))),
    );
  }

  GestureTapCallback? onTap(BuildContext context) {
    if (targetUrl == 'jihulab.com') {
      return () {
        Navigator.of(context).pushNamed(LoginPage.routeName, arguments: {'host': 'jihulab.com'});
      };
    }
    return () {
      Navigator.of(context).pushNamed(LoginPage.routeName, arguments: {'host': 'gitlab.com'});
    };
  }
}
