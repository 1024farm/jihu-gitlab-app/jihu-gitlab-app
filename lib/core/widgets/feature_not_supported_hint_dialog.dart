import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/permission_low_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class FeatureNotSupportedHintDialog {
  static Future<void> show(BuildContext context, String featureName) {
    return showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(content: PermissionLowView(featureName: featureName), actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: Text(AppLocalizations.dictionary().ok, style: const TextStyle(color: AppThemeData.secondaryColor, fontSize: 14)),
            ),
          ]);
        });
  }
}
