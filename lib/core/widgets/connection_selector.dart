import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/account_list_view.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar/avatar.dart';
import 'package:provider/provider.dart';

class ConnectionSelector extends StatefulWidget {
  const ConnectionSelector({super.key});

  @override
  State<ConnectionSelector> createState() => _ConnectionSelectorState();
}

class _ConnectionSelectorState extends State<ConnectionSelector> {
  @override
  Widget build(BuildContext context) {
    return Consumer<ConnectionProvider>(builder: (context, _, child) {
      if (ConnectionProvider.connection == null) return Container();
      return Center(
        child: PopupMenuButton<String>(
          padding: EdgeInsets.zero,
          position: PopupMenuPosition.under,
          itemBuilder: (context) => accountItems(context),
          child: Avatar(key: Key(ConnectionProvider.connection!.userInfo.username), avatarUrl: ConnectionProvider.connection!.userInfo.avatarUrl, size: 24),
        ),
      );
    });
  }
}
