import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/app_platform.dart';
import 'package:jihu_gitlab_app/core/native_text_field.dart';

class InputFieldWithTitle extends StatefulWidget {
  final String title;
  final String placeHolder;
  final TextEditingController controller;
  final int? maxLines;
  final ValueChanged<String>? onChanged;
  final EdgeInsetsGeometry? margin;

  const InputFieldWithTitle(
      {required this.title, required this.placeHolder, required this.controller, this.maxLines = 1, this.onChanged, this.margin = const EdgeInsets.only(left: 16.0, right: 16.0), Key? key})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _InputFieldWithTitleState();
}

class _InputFieldWithTitleState extends State<InputFieldWithTitle> {
  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Column(children: [
      Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: widget.margin == null ? 0 : 16),
        child: Text(widget.title, style: textTheme.titleLarge),
      ),
      Container(
        margin: widget.margin,
        decoration: BoxDecoration(color: Colors.white, border: Border.all(color: const Color(0xFFEAEAEA)), borderRadius: BorderRadius.circular(4.0)),
        alignment: Alignment.center,
        child: AppPlatform.isAndroid
            ? TextField(
                key: widget.key,
                controller: widget.controller,
                style: const TextStyle(color: Color(0xFF03162F), fontSize: 14, fontWeight: FontWeight.w400),
                maxLines: widget.maxLines,
                cursorColor: Theme.of(context).primaryColor,
                onChanged: widget.onChanged,
                decoration: InputDecoration(
                    hintText: widget.placeHolder,
                    hintStyle: const TextStyle(fontSize: 14),
                    border: const OutlineInputBorder(borderSide: BorderSide.none),
                    contentPadding: const EdgeInsets.only(left: 8)))
            // coverage:ignore-start
            : NativeTextField(
                key: widget.key,
                controller: widget.controller,
                style: const TextStyle(color: Color(0xFF03162F), fontSize: 14, fontWeight: FontWeight.w400),
                maxLines: widget.maxLines ?? 1,
                minHeightPadding: 30,
                paddingLeft: 8,
                paddingRight: 8,
                cursorColor: Theme.of(context).primaryColor,
                onChanged: widget.onChanged,
                placeHolder: widget.placeHolder,
                placeHolderStyle: const TextStyle(fontSize: 14, color: Color.fromRGBO(154, 154, 154, 1.0), fontWeight: FontWeight.w400),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.transparent),
                ),
              ),
        // coverage:ignore-end
      )
    ]);
  }
}
