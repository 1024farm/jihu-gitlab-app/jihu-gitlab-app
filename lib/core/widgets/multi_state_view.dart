import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/load_state.dart';
import 'package:jihu_gitlab_app/core/widgets/http_fail_view.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_view.dart';
import 'package:jihu_gitlab_app/core/widgets/no_data_view.dart';
import 'package:jihu_gitlab_app/core/widgets/permission_low_view.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_blocked_view.dart';

class MultiStateView extends StatelessWidget {
  const MultiStateView({
    required this.loadState,
    required this.child,
    this.errorView,
    this.emptyView,
    this.unavailableView = const PermissionLowView(featureName: ""),
    this.loadingView = const LoadingView(),
    this.onRetry,
    super.key,
  });

  final LoadState loadState;
  final Widget child;
  final VoidFutureCallBack? onRetry;
  final Widget loadingView;
  final Widget unavailableView;
  final Widget? errorView;
  final Widget? emptyView;

  @override
  Widget build(BuildContext context) {
    if (loadState == LoadState.loadingState) {
      return loadingView;
    }
    if (loadState == LoadState.errorState) {
      return errorView ?? HttpFailView(onRefresh: onRetry);
    }
    if (loadState == LoadState.noItemState) {
      return emptyView ?? NoDataView(onRefresh: onRetry);
    }
    if (loadState == LoadState.authorizedDeniedState) {
      return unavailableView;
    }
    return child;
  }
}
