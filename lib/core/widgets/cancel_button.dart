import 'package:flutter/material.dart';

class CancelButton extends StatefulWidget {
  final Widget? loadingWidget;
  final VoidCallback? onPressed;
  final ButtonStyle? style;
  final BoxDecoration? decoration;
  final Text text;
  final bool isLoading;
  final bool disabled;

  const CancelButton({required this.text, this.loadingWidget, this.onPressed, this.style, this.decoration, this.isLoading = false, this.disabled = false, Key? key}) : super(key: key);

  @override
  State<CancelButton> createState() => _CancelButtonState();
}

class _CancelButtonState extends State<CancelButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
      decoration: widget.decoration ?? BoxDecoration(color: const Color(0xFFFFFFFF).withOpacity(widget.disabled ? 0.5 : 1), borderRadius: const BorderRadius.all(Radius.circular(4))),
      child: GestureDetector(
          onTap: widget.isLoading || widget.disabled ? null : widget.onPressed,
          child: widget.isLoading ? Row(children: [widget.loadingWidget ?? _buildDefaultLoadingWidget(), const VerticalDivider(width: 8), widget.text]) : Row(children: [widget.text])),
    );
  }

  Widget _buildDefaultLoadingWidget() {
    return const SizedBox(width: 16, height: 16, child: CircularProgressIndicator(color: Colors.white, strokeWidth: 2));
  }
}
