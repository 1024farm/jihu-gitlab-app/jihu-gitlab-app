import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:jihu_gitlab_app/core/widgets/label_view.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label.dart';

class LabelManageView extends StatelessWidget {
  const LabelManageView({required this.label, required this.projectId, required this.projectNameSpace, Key? key}) : super(key: key);

  final Label label;
  final int projectId;
  final String projectNameSpace;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _buildLabelAndEditIcon(),
        if (label.description.isNotEmpty)
          Container(
            margin: const EdgeInsets.only(top: 8),
            child: Text(
              label.description,
              softWrap: true,
              style: const TextStyle(fontSize: 13),
            ),
          ),
        if (label.isProjectLabel)
          Container(
            margin: const EdgeInsets.only(top: 8),
            child: Text(
              projectNameSpace,
              softWrap: true,
              style: const TextStyle(fontSize: 12, color: Colors.black54),
            ),
          ),
        const SizedBox(
          height: 8,
        ),
        const Divider(height: 8, thickness: 1),
      ],
    );
  }

  Widget _buildLabelAndEditIcon() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.max,
      children: [
        Flexible(
            child: LabelView(
          color: label.color,
          labelName: label.name,
          textColor: label.textColor,
        )),
        Container(
          width: 40,
          height: 20,
          alignment: Alignment.centerRight,
          child: SvgPicture.asset("assets/images/edit_pencil.svg", semanticsLabel: "edit_issue_milestone", height: 16, width: 16),
        ),
      ],
    );
  }
}
