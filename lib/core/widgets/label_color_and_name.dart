import 'package:flutter/material.dart';

class LabelColorAndName extends StatelessWidget {
  const LabelColorAndName({required this.color, required this.name, Key? key}) : super(key: key);

  final Color? color;
  final String name;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(width: 16, height: 16, margin: const EdgeInsets.only(right: 12), decoration: BoxDecoration(color: color ?? Colors.black)),
        Flexible(child: Text(name, style: const TextStyle(fontWeight: FontWeight.w400, fontSize: 16, color: Color(0XFF03162F))))
      ],
    );
  }
}
