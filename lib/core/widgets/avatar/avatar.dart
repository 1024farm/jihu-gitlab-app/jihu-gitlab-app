import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';

class Avatar extends StatelessWidget {
  final String avatarUrl;
  final double size;
  final BoxBorder? boxBorder;

  const Avatar({required this.avatarUrl, required this.size, this.boxBorder, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final placeHolderImage = ClipOval(child: Image.asset("assets/images/no_avatar.png", height: size, width: size));
    try {
      return _avatarUrl().isEmpty
          ? placeHolderImage
          // coverage:ignore-start
          : CachedNetworkImage(
              imageUrl: _avatarUrl(),
              imageBuilder: (context, imageProvider) => Container(
                height: size,
                width: size,
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(90), image: DecorationImage(image: imageProvider, fit: BoxFit.cover), border: boxBorder),
              ),
              placeholder: (context, url) => placeHolderImage,
              errorWidget: (context, url, error) => placeHolderImage,
            );
    } catch (e) {
      return placeHolderImage;
    }
    // coverage:ignore-end
  }

  String _avatarUrl() => avatarUrl.isNotEmpty ? (avatarUrl.contains('http') ? avatarUrl : ConnectionProvider.currentBaseUrl.get + avatarUrl) : '';
}
