import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class NoDataView extends StatelessWidget {
  const NoDataView({this.onRefresh, super.key});

  final Future<void> Function()? onRefresh;

  @override
  Widget build(BuildContext context) {
    return TipsView(icon: 'assets/images/no_data.svg', message: AppLocalizations.dictionary().noData, onRefresh: onRefresh);
  }
}
