import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class IssueStateView extends StatelessWidget {
  const IssueStateView({required this.state, Key? key}) : super(key: key);
  final String state;

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(
      state == "closed" ? "assets/images/issue-closed.svg" : "assets/images/issues.svg",
      colorFilter: ColorFilter.mode(state == "closed" ? const Color(0xFF005BE9) : const Color(0xFF13AB06), BlendMode.srcIn),
      width: 16,
      height: 16,
    );
  }
}
