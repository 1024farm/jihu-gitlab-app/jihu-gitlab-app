import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/themes/project_style.dart';

typedef PopupStringMenuButtonSelectedItemCallback = void Function(String value);

class PopupStringMenuButton extends StatefulWidget {
  const PopupStringMenuButton({required this.menuItems, this.selectedItem, this.onItemSelected, super.key});

  final List<PopupStringMenuItem> menuItems;
  final PopupStringMenuItem? selectedItem;
  final PopupStringMenuButtonSelectedItemCallback? onItemSelected;

  @override
  State<PopupStringMenuButton> createState() => _PopupStringMenuButtonState();
}

class _PopupStringMenuButtonState extends State<PopupStringMenuButton> {
  PopupStringMenuItem? _defaultSelectedItem;
  late PopupStringMenuItem _selectedItem;

  @override
  void initState() {
    _defaultSelectedItem = widget.selectedItem;
    _selectedItem = widget.selectedItem ?? widget.menuItems.first;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (_defaultSelectedItem != widget.selectedItem) {
      _selectedItem = widget.selectedItem ?? widget.menuItems.first;
      _defaultSelectedItem = widget.selectedItem;
    }
    return PopupMenuButton<String>(
      padding: EdgeInsets.zero,
      position: PopupMenuPosition.under,
      itemBuilder: (context) => _items(),
      child: Container(
        padding: const EdgeInsets.all(8),
        child: Row(
          children: [Expanded(child: Text(_selectedItem.name, style: Theme.of(context).textTheme.titleSmall)), const Icon(Icons.arrow_drop_down, color: Colors.black)],
        ),
      ),
    );
  }

  List<PopupMenuEntry<String>> _items() {
    List<PopupMenuEntry<String>> list = [];
    for (var i = 0; i < widget.menuItems.length; i++) {
      if (i > 0) {
        list.add(const PopupMenuDivider());
      }
      final item = widget.menuItems[i];
      list.add(PopupMenuItem<String>(
        onTap: () {
          widget.onItemSelected?.call(item.value);
          setState(() {
            _selectedItem = item;
          });
        },
        value: item.value,
        height: 32,
        child: Text(item.name, style: ProjectStyle.build(Theme.of(context).textTheme.bodySmall, item.value == _selectedItem.value ? Theme.of(context).colorScheme.primary : Colors.black)),
      ));
    }
    return list;
  }
}

class PopupStringMenuItem {
  String value;
  String name;

  PopupStringMenuItem(this.value, this.name);
}
