import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connections.dart';
import 'package:jihu_gitlab_app/core/themes/project_style.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class UnauthorizedView extends StatefulWidget {
  const UnauthorizedView({super.key});

  @override
  State<UnauthorizedView> createState() => _UnauthorizedViewState();
}

class _UnauthorizedViewState extends State<UnauthorizedView> {
  final double _iconSize = 40.0;
  final double _actionButtonWidth = 172.0;
  final double _actionButtonHeight = 42.0;

  final Map<String, LoginConfig> _menuItems = loginMenuList;

  late LoginConfig _selectedItem = _menuItems.values.first;

  @override
  Widget build(BuildContext context) {
    final colorScheme = Theme.of(context).colorScheme;
    final textTheme = Theme.of(context).textTheme;
    return Center(
      heightFactor: 100,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            "assets/images/not_login.png",
            height: _iconSize,
            width: _iconSize,
          ),
          Container(
            margin: const EdgeInsets.only(top: 10, bottom: 40),
            child: Text(
              AppLocalizations.dictionary().connectServerFirst,
              style: textTheme.titleSmall,
            ),
          ),
          Container(
            width: _actionButtonWidth,
            height: _actionButtonHeight,
            decoration: BoxDecoration(color: colorScheme.primary, borderRadius: BorderRadius.circular(4.0), border: Border.all(color: colorScheme.primary, width: 1)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: InkWell(
                      onTap: () {
                        Navigator.of(context).pushNamed(_selectedItem.pageRouteName, arguments: {'host': _selectedItem.host});
                      },
                      child: SizedBox(
                        height: _actionButtonHeight,
                        child: Center(
                          child: Text(_selectedItem.displayTitle, style: ProjectStyle.build(textTheme.titleSmall, Colors.white)),
                        ),
                      )),
                ),
                PopupMenuButton<String>(
                  padding: EdgeInsets.zero,
                  constraints: BoxConstraints(minWidth: _actionButtonWidth),
                  position: PopupMenuPosition.under,
                  itemBuilder: (context) => _menuItems.keys
                      .map((item) => PopupMenuItem<String>(
                            onTap: () {
                              setState(() {
                                _selectedItem = _menuItems[item]!;
                              });
                            },
                            value: item,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(_menuItems[item]!.displayTitle, style: textTheme.titleSmall),
                                if (_selectedItem.name == item)
                                  Icon(
                                    Icons.check,
                                    color: colorScheme.primary,
                                  ),
                              ],
                            ),
                          ))
                      .toList(),
                  child: Container(
                    height: _actionButtonHeight,
                    width: _actionButtonHeight,
                    decoration: BoxDecoration(color: const Color(0xFFFCA326).withAlpha(60)),
                    child: const Icon(Icons.arrow_drop_down, color: Colors.white),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
