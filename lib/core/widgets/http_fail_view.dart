import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class HttpFailView extends StatelessWidget {
  final Future<void> Function()? onRefresh;

  const HttpFailView({this.onRefresh, super.key});

  @override
  Widget build(BuildContext context) {
    return TipsView(icon: 'assets/images/refresh_exception.svg', message: AppLocalizations.dictionary().refreshException, onRefresh: onRefresh);
  }
}
