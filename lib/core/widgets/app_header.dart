import 'package:easy_refresh/easy_refresh.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class AppHeader extends ClassicHeader {
  AppHeader()
      : super(
          dragText: AppLocalizations.dictionary().scrollDownToRefresh,
          armedText: AppLocalizations.dictionary().releaseToLoad,
          readyText: AppLocalizations.dictionary().loading,
          processingText: AppLocalizations.dictionary().loading,
          processedText: AppLocalizations.dictionary().successful,
          failedText: AppLocalizations.dictionary().loadFail,
          noMoreText: AppLocalizations.dictionary().hasNoData,
          showMessage: false,
        );
}
