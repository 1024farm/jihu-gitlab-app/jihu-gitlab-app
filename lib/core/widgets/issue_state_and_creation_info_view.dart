import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/domain/avatar_url.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/time.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar_and_name.dart';
import 'package:jihu_gitlab_app/core/widgets/changeable_time.dart';
import 'package:jihu_gitlab_app/core/widgets/issue_state_view.dart';

class IssueStateAndCreationInfoView extends StatefulWidget {
  const IssueStateAndCreationInfoView({
    required this.state,
    required this.confidential,
    required this.authorName,
    required this.authorAvatarUrl,
    required this.recentActivities,
    required this.recentActivityTimes,
    required this.onTimeFormatChanged,
    super.key,
  });

  final String state;
  final bool confidential;
  final String authorName;
  final String authorAvatarUrl;
  final String recentActivities;
  final Time recentActivityTimes;
  final VoidCallback onTimeFormatChanged;

  @override
  State<IssueStateAndCreationInfoView> createState() => _IssueStateAndCreationInfoViewState();
}

class _IssueStateAndCreationInfoViewState extends State<IssueStateAndCreationInfoView> {
  @override
  Widget build(BuildContext context) {
    return FittedBox(
        fit: BoxFit.fitWidth,
        child: Row(
          children: [
            Padding(padding: const EdgeInsets.only(right: 12), child: IssueStateView(state: widget.state)),
            widget.confidential
                ? Padding(
                    padding: const EdgeInsets.only(right: 12),
                    child: SvgPicture.asset("assets/images/visibility_off_primary.svg", width: 16, height: 16),
                  )
                : const SizedBox(),
            AvatarAndName(
                username: widget.authorName,
                avatarUrl: AvatarUrl(widget.authorAvatarUrl).realUrl(ProjectProvider().specifiedHost),
                avatarSize: 16,
                nameStyle: const TextStyle(color: Color(0xFF87878C), fontSize: 12),
                gap: 2),
            Text(widget.recentActivities, style: const TextStyle(color: Color(0xFF87878C), fontWeight: FontWeight.w400, fontSize: 12)),
            ChangeableTime.show(widget.onTimeFormatChanged, widget.recentActivityTimes, const Color(0xFF87878C))
          ],
        ));
  }
}
