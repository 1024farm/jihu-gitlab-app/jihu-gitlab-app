import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/app_platform.dart';

class ActivityIndicator extends StatelessWidget {
  const ActivityIndicator({super.key, this.radius = 20});

  final double radius;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: AppPlatform.isIOS ? const CupertinoActivityIndicator() : SizedBox(width: radius, height: radius, child: const CircularProgressIndicator()),
    );
  }
}
