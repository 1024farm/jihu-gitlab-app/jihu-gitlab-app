import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/color_parser.dart';

class LabelView extends StatelessWidget {
  final String left;
  final String right;
  final String textColor;
  final double fontSize;
  final EdgeInsets padding;
  final String color;

  LabelView({required String labelName, required this.textColor, required this.color, Key? key, this.fontSize = 13, this.padding = const EdgeInsets.symmetric(vertical: 4, horizontal: 8)})
      : left = labelName.contains("::") ? labelName.substring(0, labelName.lastIndexOf("::")) : '',
        right = labelName.contains("::") ? labelName.substring(labelName.lastIndexOf("::") + "::".length) : labelName,
        super(key: key);

  Color _parseColor(String hexString) {
    return ColorParser(hexString).parse() ?? Colors.black;
  }

  @override
  Widget build(BuildContext context) {
    final Color borderColor = _parseColor(color);
    return Container(
      decoration: BoxDecoration(border: Border.all(color: borderColor), borderRadius: const BorderRadius.all(Radius.circular(4))),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: items(),
      ),
    );
  }

  List<Widget> items() {
    final Color borderColor = _parseColor(color);
    if (left == '') {
      return [
        Flexible(
          child: Container(
            padding: padding,
            decoration: BoxDecoration(color: borderColor),
            child: Text(right,
                style: TextStyle(
                  color: _parseColor(textColor),
                  fontSize: fontSize,
                  fontWeight: FontWeight.w500,
                  overflow: TextOverflow.ellipsis,
                )),
          ),
        )
      ];
    }
    return [
      Flexible(
        child: Container(
          padding: padding,
          decoration: BoxDecoration(color: borderColor),
          child: Text(left,
              style: TextStyle(
                color: _parseColor(textColor),
                fontSize: fontSize,
                fontWeight: FontWeight.w500,
                overflow: TextOverflow.ellipsis,
              )),
        ),
      ),
      Flexible(
        child: Container(
          padding: padding,
          child: Text(right,
              style: TextStyle(
                color: const Color(0xFF03162F),
                fontSize: fontSize,
                fontWeight: FontWeight.w500,
                overflow: TextOverflow.ellipsis,
              )),
        ),
      )
    ];
  }
}
