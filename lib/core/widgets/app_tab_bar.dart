import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';

class AppTabBar extends TabBar {
  const AppTabBar({
    required tabs,
    required TabController? controller,
    labelColor = Colors.black,
    labelStyle = const TextStyle(fontWeight: FontWeight.w600, fontSize: 14.0),
    indicatorWeight = 0.01,
    unselectedLabelColor = AppThemeData.secondaryColor,
    unselectedLabelStyle = const TextStyle(fontWeight: FontWeight.w500, fontSize: 14.0),
    bool isScrollable = false,
    indicatorPadding = const EdgeInsets.symmetric(vertical: 8),
    padding = EdgeInsets.zero,
    labelPadding = const EdgeInsets.symmetric(horizontal: 10),
    splashFactory = NoSplash.splashFactory,
  }) : super(
            key: null,
            tabs: tabs,
            controller: controller,
            labelColor: labelColor,
            labelStyle: labelStyle,
            indicatorSize: TabBarIndicatorSize.label,
            indicatorWeight: 3,
            indicatorColor: AppThemeData.primaryColor,
            indicatorPadding: indicatorPadding,
            isScrollable: isScrollable,
            padding: padding,
            labelPadding: labelPadding,
            splashFactory: splashFactory,
            unselectedLabelColor: unselectedLabelColor,
            unselectedLabelStyle: unselectedLabelStyle);
}
