import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/clipboard.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/id.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/urls.dart';
import 'package:jihu_gitlab_app/core/widgets/toast.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/details/issue_details_page.dart';
import 'package:jihu_gitlab_app/modules/mr/merge_request_page.dart';
import 'package:provider/provider.dart';

class PasteTypeUrlNavigation extends StatefulWidget {
  final Widget child;

  const PasteTypeUrlNavigation({required this.child, super.key});

  @override
  State<PasteTypeUrlNavigation> createState() => _PasteTypeUrlNavigationState();
}

class _PasteTypeUrlNavigationState extends State<PasteTypeUrlNavigation> {
  final PasteTypeUrlNavigationModel _model = PasteTypeUrlNavigationModel();

  @override
  Widget build(BuildContext context) {
    return Consumer<ConnectionProvider>(builder: (context, _, child) {
      if (!ConnectionProvider.authorized) return Container();
      return InkWell(
        radius: 0.0,
        child: widget.child,
        onTap: () {
          Clipboard().fromClipboard().then((value) {
            _model.parseAndToward(value, context);
          });
        },
      );
    });
  }
}

class PasteTypeUrlNavigationModel {
  void parseAndToward(String value, BuildContext context) {
    ProjectProvider().clear();
    try {
      var url = Urls(value).urls[0];
      String host = ConnectionProvider.connection!.baseUrl.get;
      var urlWithoutHost = url.replaceFirst('$host/', '');
      var projectFullPath = urlWithoutHost.split('/-/')[0];
      RegExp reg = RegExp('\\d+');

      if (urlWithoutHost.contains('/-/issues')) {
        _parseIssue(reg, urlWithoutHost, projectFullPath, url, context);
        return;
      }
      if (urlWithoutHost.contains('/-/merge_requests')) {
        _parseMergeRequest(reg, urlWithoutHost, projectFullPath, url, context);
      }
    } catch (e) {
      debugPrint('Url parse error: $e');
      Toast.error(context, AppLocalizations.dictionary().unrecognizedUrl);
    }
  }

  void _parseIssue(RegExp reg, String urlWithoutHost, String projectFullPath, String url, BuildContext context) {
    var issueIid = int.parse(reg.firstMatch(urlWithoutHost.split('/-/issues')[1])?[0] ?? '0');
    HttpClient.instance().post(Api.graphql(), {
      "variables": {"fullPath": projectFullPath, "iid": "$issueIid"},
      "query": """
        query (\$fullPath: ID!, \$iid: String!) {
          project(fullPath: \$fullPath) {
            id
            fullPath
            issue(iid: \$iid) {
              id
              iid
            }
          }
        }
      """,
    }).then((res) {
      final params = <String, dynamic>{
        'projectId': Id.fromGid(res.body()['data']['project']['id']).id,
        'issueId': Id.fromGid(res.body()['data']['project']['issue']['id']).id,
        'issueIid': int.parse(res.body()['data']['project']['issue']['iid']),
        'targetId': Id.fromGid(res.body()['data']['project']['issue']['id']).id,
        'targetIid': int.parse(res.body()['data']['project']['issue']['iid']),
        'pathWithNamespace': res.body()['data']['project']['fullPath'],
        'targetUrl': url,
        'showLeading': true
      };
      Navigator.pushNamed(context, IssueDetailsPage.routeName, arguments: params);
    });
  }

  void _parseMergeRequest(RegExp reg, String urlWithoutHost, String projectFullPath, String url, BuildContext context) {
    var mrIid = int.parse(reg.firstMatch(urlWithoutHost.split('/-/merge_requests')[1])?[0] ?? '0');
    HttpClient.instance().post(Api.graphql(), {
      "variables": {"fullPath": projectFullPath},
      "query": """
        query (\$fullPath: ID!) {
          project(fullPath: \$fullPath) {
            id
            name
            fullPath
          }
        }
      """,
    }).then((res) {
      final params = <String, dynamic>{
        'projectId': Id.fromGid(res.body()['data']['project']['id']).id,
        'projectName': res.body()['data']['project']['name'],
        'mergeRequestIid': mrIid,
        'fullPath': projectFullPath,
      };
      Navigator.pushNamed(context, MergeRequestPage.routeName, arguments: params);
    });
  }
}
