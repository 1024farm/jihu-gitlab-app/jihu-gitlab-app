import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/time.dart';
import 'package:jihu_gitlab_app/core/widgets/charts/app_line_chart_explain_view.dart';
import 'package:jihu_gitlab_app/core/widgets/charts/app_line_chart_line.dart';
import 'package:jihu_gitlab_app/core/widgets/charts/app_line_chart_view.dart';
import 'package:jihu_gitlab_app/core/widgets/charts/time_box_report.dart';
import 'package:jihu_gitlab_app/core/widgets/choice_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class BurnChartView extends StatefulWidget {
  final TimeBoxReport timeBoxReport;

  const BurnChartView({required this.timeBoxReport, super.key});

  @override
  State<StatefulWidget> createState() => BurnChartViewState();
}

class BurnChartViewState extends State<BurnChartView> {
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      _BurndownChartView(
        countPoints: widget.timeBoxReport.asBurndownPoints(BurnChartDataType.count),
        weightPoints: widget.timeBoxReport.asBurndownPoints(BurnChartDataType.weight),
      ),
      const Divider(height: 24, thickness: 1),
      _BurnupChartView(
          totalCountPoints: widget.timeBoxReport.asBurnupTotalPoints(BurnChartDataType.count),
          totalWeightPoints: widget.timeBoxReport.asBurnupTotalPoints(BurnChartDataType.weight),
          completedCountPoints: widget.timeBoxReport.asBurnupCompletedPoints(BurnChartDataType.count),
          completedWeightPoints: widget.timeBoxReport.asBurnupCompletedPoints(BurnChartDataType.weight))
    ]);
  }
}

class _BurnupChartView extends StatefulWidget {
  final List<AppLineChartPoint> totalCountPoints;
  final List<AppLineChartPoint> totalWeightPoints;
  final List<AppLineChartPoint> completedCountPoints;
  final List<AppLineChartPoint> completedWeightPoints;

  const _BurnupChartView({required this.totalCountPoints, required this.totalWeightPoints, required this.completedCountPoints, required this.completedWeightPoints});

  @override
  State<_BurnupChartView> createState() => _BurnupChartViewState();
}

class _BurnupChartViewState extends State<_BurnupChartView> {
  BurnChartDataType _type = BurnChartDataType.count;

  List<AppLineChartPoint> get _totalPoints => _type == BurnChartDataType.count ? widget.totalCountPoints : widget.totalWeightPoints;

  List<AppLineChartPoint> get _completedPoints => _type == BurnChartDataType.count ? widget.completedCountPoints : widget.completedWeightPoints;

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1.3,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          _BurnChartTitleView(title: AppLocalizations.dictionary().burnupChart),
          const SizedBox(height: 12),
          ChoiceView<BurnChartDataType>(
            candidates: BurnChartDataType.values,
            selected: [_type],
            onChoiceChange: (e) {
              setState(() => _type = e);
              return true;
            },
            itemBuilder: <Widget>(type, selected) {
              return Container(
                padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
                decoration: BoxDecoration(color: selected ? const Color(0xFFFFECE2) : const Color(0xFFEAEAEA), borderRadius: const BorderRadius.all(Radius.circular(15))),
                child: Text(type.label, style: TextStyle(color: selected ? Theme.of(context).primaryColor : const Color(0xFF03162F), fontSize: 13, fontWeight: FontWeight.normal)),
              );
            },
          ),
          const SizedBox(height: 16),
          Expanded(
              child: Padding(
            padding: const EdgeInsets.only(right: 16, left: 0),
            child: AppLineChartView(
                lines: [
                  AppLineChartLine(const Color(0xFF0063E9), AppLineChartLineStyle.solid, _totalPoints, showGuideLine: false),
                  AppLineChartLine(Theme.of(context).primaryColor, AppLineChartLineStyle.solid, _completedPoints, showGuideLine: false)
                ],
                onChartTouched: (points) {
                  String text = _type == BurnChartDataType.count
                      ? AppLocalizations.dictionary().burnupChartIssueCountTip(points[0].value ?? 0, points[1].value ?? 0)
                      : AppLocalizations.dictionary().burnupChartIssueWeightTip(points[0].value ?? 0, points[1].value ?? 0);
                  return _buildTooltipText(Time.init(points[0].date).formatAsDate() ?? '', text);
                }),
          )),
          const SizedBox(height: 8),
          const _BurnupChartExplainView()
        ],
      ),
    );
  }
}

class _BurnChartTitleView extends StatelessWidget {
  const _BurnChartTitleView({required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Text(title, style: const TextStyle(color: Color(0xFF03162F), fontSize: 15, fontWeight: FontWeight.bold));
  }
}

class _BurndownChartView extends StatefulWidget {
  const _BurndownChartView({required this.countPoints, required this.weightPoints});

  final List<AppLineChartPoint> countPoints;
  final List<AppLineChartPoint> weightPoints;

  @override
  State<_BurndownChartView> createState() => _BurndownChartViewState();
}

class _BurndownChartViewState extends State<_BurndownChartView> {
  BurnChartDataType _type = BurnChartDataType.count;

  List<AppLineChartPoint> get _points => _type == BurnChartDataType.count ? widget.countPoints : widget.weightPoints;

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1.3,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          _BurnChartTitleView(title: AppLocalizations.dictionary().burndownChart),
          const SizedBox(height: 12),
          ChoiceView<BurnChartDataType>(
            candidates: BurnChartDataType.values,
            selected: [_type],
            onChoiceChange: (e) {
              setState(() => _type = e);
              return true;
            },
            itemBuilder: <Widget>(type, selected) {
              return Container(
                padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
                decoration: BoxDecoration(color: selected ? const Color(0xFFFFECE2) : const Color(0xFFEAEAEA), borderRadius: const BorderRadius.all(Radius.circular(15))),
                child: Text(type.label, style: TextStyle(color: selected ? Theme.of(context).primaryColor : const Color(0xFF03162F), fontSize: 13, fontWeight: FontWeight.normal)),
              );
            },
          ),
          const SizedBox(height: 16),
          Expanded(
              child: Padding(
            padding: const EdgeInsets.only(right: 16, left: 0),
            child: AppLineChartView(
              lines: [AppLineChartLine(Theme.of(context).primaryColor, AppLineChartLineStyle.solid, _points, showGuideLine: true)],
              onChartTouched: (points) {
                String text = _type == BurnChartDataType.count
                    ? AppLocalizations.dictionary().burndownChartIssueCountTip(points[0].value.toString())
                    : AppLocalizations.dictionary().burndownChartIssueWeightTip(points[0].value.toString());
                return _buildTooltipText(Time.init(points[0].date).formatAsDate() ?? '', text);
              },
            ),
          )),
          const SizedBox(height: 8),
          const _BurndownChartExplainView()
        ],
      ),
    );
  }
}

class _BurndownChartExplainView extends StatelessWidget {
  const _BurndownChartExplainView();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        AppLineChartExplainView(name: AppLocalizations.dictionary().remaining, lineColor: Theme.of(context).primaryColor),
        const SizedBox(width: 20),
        AppLineChartExplainView(name: AppLocalizations.dictionary().guideline, lineColor: const Color(0xFFCECECE), dash: true),
      ],
    );
  }
}

class _BurnupChartExplainView extends StatelessWidget {
  const _BurnupChartExplainView();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        AppLineChartExplainView(name: AppLocalizations.dictionary().total, lineColor: const Color(0xFF0063E9)),
        const SizedBox(width: 20),
        AppLineChartExplainView(name: AppLocalizations.dictionary().completed, lineColor: Theme.of(context).primaryColor),
      ],
    );
  }
}

List<TextSpan> _buildTooltipText(String date, String content) {
  return [
    TextSpan(text: "$date\n", style: const TextStyle(fontSize: 13, fontWeight: FontWeight.w500)),
    const TextSpan(text: "\n", style: TextStyle(fontSize: 4)),
    TextSpan(text: content, style: const TextStyle(fontSize: 12, fontWeight: FontWeight.w400, height: 1.5)),
  ];
}
