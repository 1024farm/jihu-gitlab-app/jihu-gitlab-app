import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/clock/global_glock.dart';
import 'package:jihu_gitlab_app/core/utils/date_time_formatter.dart';
import 'package:jihu_gitlab_app/core/widgets/charts/app_line_chart_line.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

enum BurnChartDataType {
  count,
  weight;

  String get label {
    if (this == BurnChartDataType.count) {
      return AppLocalizations.dictionary().issues;
    }
    return AppLocalizations.dictionary().issueWeight;
  }
}

class TimeboxMetrics {
  final int _count;
  final int _weight;

  TimeboxMetrics(this._count, this._weight);

  int get weight => _weight;

  int get count => _count;

  factory TimeboxMetrics.fromJson([Map<String, dynamic> json = const {}]) {
    return TimeboxMetrics(json['count'] ?? 0, json['weight'] ?? 0);
  }
}

class BurnupChartDailyTotals {
  final String _date;
  final TimeboxMetrics? _scope;
  final TimeboxMetrics? _completed;

  BurnupChartDailyTotals(this._date, this._scope, this._completed);

  factory BurnupChartDailyTotals.none(String date) {
    return BurnupChartDailyTotals(date, null, null);
  }

  factory BurnupChartDailyTotals.empty(String date) {
    return BurnupChartDailyTotals(date, TimeboxMetrics(0, 0), TimeboxMetrics(0, 0));
  }

  BurnupChartDailyTotals copyWith(String date) {
    return BurnupChartDailyTotals(date, _scope, _completed);
  }

  TimeboxMetrics? get completed => _completed;

  TimeboxMetrics? get scope => _scope;

  String get date => _date;

  factory BurnupChartDailyTotals.fromJson(Map<String, dynamic> json) {
    var completedCount = json['completedCount'] ?? 0;
    var scopeCount = json['scopeCount'] ?? 0;
    var completedWeight = json['completedWeight'] ?? 0;
    var scopeWeight = json['scopeWeight'] ?? 0;
    return BurnupChartDailyTotals(json['date'], TimeboxMetrics(scopeCount, scopeWeight), TimeboxMetrics(completedCount, completedWeight));
  }

  AppLineChartPoint get asBurndownCountPoint {
    int? value;
    if (scope != null && _completed != null) {
      value = _scope!.count - _completed!.count;
    }
    return AppLineChartPoint(_date, value);
  }

  AppLineChartPoint get asBurndownWeightPoint {
    int? value;
    if (scope != null && _completed != null) {
      value = _scope!.weight - _completed!.weight;
    }
    return AppLineChartPoint(_date, value);
  }

  AppLineChartPoint get asBurnupTotalCountPoint {
    return AppLineChartPoint(_date, _scope?.count);
  }

  AppLineChartPoint get asBurnupTotalWeightPoint {
    return AppLineChartPoint(_date, _scope?.weight);
  }

  AppLineChartPoint get asBurnupCompletedCountPoint {
    return AppLineChartPoint(_date, _completed?.count);
  }

  AppLineChartPoint get asBurnupCompletedWeightPoint {
    return AppLineChartPoint(_date, _completed?.weight);
  }
}

class TimeBoxReport {
  final List<BurnupChartDailyTotals> _burnupTimeSeries;
  final DateTime? _startDate;
  final DateTime? _dueDate;
  final List<BurnupChartDailyTotals> _correctedData = [];

  TimeBoxReport(this._burnupTimeSeries, this._startDate, this._dueDate) {
    _correctBurnupTimeSeries();
  }

  factory TimeBoxReport.fromJson(Map<String, dynamic> reportJson, String? startDate, String? dueDate) {
    DateTime? start = DateTime.tryParse(startDate ?? '');
    DateTime? due = DateTime.tryParse(dueDate ?? '');
    List<dynamic> burnupTimeSeriesJson = reportJson['burnupTimeSeries'] ?? [];
    List<BurnupChartDailyTotals> burnupTimeSeries = burnupTimeSeriesJson.map((e) => BurnupChartDailyTotals.fromJson(e)).toList();
    return TimeBoxReport(burnupTimeSeries, start, due);
  }

  void _correctBurnupTimeSeries() {
    if (_burnupTimeSeries.isEmpty) {
      return;
    }
    var dateSeries = generateDateSeries();
    if (dateSeries.isEmpty) {
      return;
    }
    var dateMap = convertToMap();
    for (var date in dateSeries) {
      var chartDailyTotals = dateMap.putIfAbsent(date, () => paddingChartDaily(date, dateMap));
      _correctedData.add(chartDailyTotals);
    }
  }

  List<String> generateDateSeries() {
    if (_startDate == null || _dueDate == null) {
      return [];
    }
    Duration duration = DateTimeRange(start: _startDate!, end: _dueDate!).duration;
    var inDays = duration.inDays + 1;
    return List.generate(inDays, (index) => dateTimeToString(_startDate!.add(Duration(days: index))));
  }

  Map<String, BurnupChartDailyTotals> convertToMap() {
    return {for (var item in _burnupTimeSeries) item.date: item};
  }

  static String dateTimeToString(DateTime dateTime) {
    return DateTimeFormatter.withPattern(['yyyy', '-', 'mm', '-', 'dd']).format(dateTime);
  }

  BurnupChartDailyTotals? findPreviousOne(DateTime startDate, DateTime dateTime, Map<String, BurnupChartDailyTotals?> map) {
    DateTime dt = dateTime;
    do {
      dt = dt.subtract(const Duration(days: 1));
      var burnupChartDailyTotals = map[dateTimeToString(dt)];
      if (burnupChartDailyTotals != null) {
        return burnupChartDailyTotals;
      }
    } while (dt.isAfter(startDate));
    return null;
  }

  BurnupChartDailyTotals paddingChartDaily(String date, Map<String, BurnupChartDailyTotals?> map) {
    var dateTime = DateTime.parse(date);
    if (dateTime.isAfter(GlobalClock.now())) {
      return BurnupChartDailyTotals.none(date);
    }
    var previousOne = findPreviousOne(_startDate!, dateTime, map);
    return previousOne != null ? previousOne.copyWith(date) : BurnupChartDailyTotals.empty(date);
  }

  List<AppLineChartPoint> asBurndownPoints(BurnChartDataType type) {
    return _correctedData.map((e) => type == BurnChartDataType.count ? e.asBurndownCountPoint : e.asBurndownWeightPoint).toList();
  }

  List<AppLineChartPoint> asBurnupTotalPoints(BurnChartDataType type) {
    return _correctedData.map((e) => type == BurnChartDataType.count ? e.asBurnupTotalCountPoint : e.asBurnupTotalWeightPoint).toList();
  }

  List<AppLineChartPoint> asBurnupCompletedPoints(BurnChartDataType type) {
    return _correctedData.map((e) => type == BurnChartDataType.count ? e.asBurnupCompletedCountPoint : e.asBurnupCompletedWeightPoint).toList();
  }

  bool get isNotEmpty => _burnupTimeSeries.isNotEmpty;
}
