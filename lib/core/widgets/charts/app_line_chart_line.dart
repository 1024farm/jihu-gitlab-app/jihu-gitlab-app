import 'dart:math';

import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/iterable_extension.dart';

enum AppLineChartLineStyle { solid, dash }

class AppLineChartLine {
  final Color _color;
  final AppLineChartLineStyle _style;
  final List<AppLineChartPoint> _points;
  bool showGuideLine;

  AppLineChartLine(this._color, this._style, this._points, {required this.showGuideLine});

  List<AppLineChartPoint> get points => _points;

  AppLineChartLineStyle get style => _style;

  Color get color => _color;

  double get maxValue => _points.where((e) => e.value != null).map((e) => e.value!.toDouble()).reduceOr(max, 0);

  int get pointsLength => _points.length;

  bool get valid => _points.isNotEmpty;
}

class AppLineChartPoint {
  final String _date;
  final int? _value;

  String get date => _date;

  int? get value => _value;

  double get nonNullValue => (_value ?? 0).toDouble();

  AppLineChartPoint(this._date, this._value);

  @override
  bool operator ==(Object other) => identical(this, other) || other is AppLineChartPoint && runtimeType == other.runtimeType && _date == other._date && _value == other._value;

  @override
  int get hashCode => _date.hashCode ^ _value.hashCode;
}
