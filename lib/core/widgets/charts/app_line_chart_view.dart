import 'dart:math';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/iterable_extension.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/time.dart';
import 'package:jihu_gitlab_app/core/widgets/charts/app_line_chart_line.dart';

typedef OnChartTouched = List<TextSpan> Function(List<AppLineChartPoint> pointsOnOneDay);

class AppLineChartView extends StatefulWidget {
  const AppLineChartView({required this.lines, required this.onChartTouched, Key? key}) : super(key: key);
  final List<AppLineChartLine> lines;
  final OnChartTouched onChartTouched;

  @override
  State<AppLineChartView> createState() => _AppLineChartViewState();
}

class _AppLineChartViewState extends State<AppLineChartView> {
  static const double _yPoints = 7.0;
  List<int> _xAxisScales = [];

  @override
  void initState() {
    initXAxisScales();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return LineChart(lineChartData, swapAnimationDuration: const Duration(milliseconds: 250));
  }

  void initXAxisScales() {
    var maxLength = maxPointsLength;
    if (maxLength <= 7) {
      _xAxisScales = List.generate(7, (index) => index);
    } else {
      int last = maxLength - 1;
      _xAxisScales.add(0);
      int mid = (last / 2).ceil();
      int second = (mid / 2).floor();
      int fourth = ((last - mid) / 2).ceil() + mid;
      _xAxisScales.insert(1, second);
      _xAxisScales.insert(2, mid);
      _xAxisScales.insert(3, fourth);
      _xAxisScales.add(last);
    }
  }

  double get realMaxY => widget.lines.map((e) => e.maxValue).reduceOr(max, 0);

  double get yInterval => max(1, (realMaxY / _yPoints).ceilToDouble());

  double get maxY {
    double r = realMaxY / yInterval;
    return r.ceilToDouble() * yInterval;
  }

  int get maxPointsLength => widget.lines.map((e) => e.pointsLength).reduceOr(max, 0);

  LineChartData get lineChartData => LineChartData(
        lineTouchData: touchData,
        gridData: gridData,
        titlesData: titles,
        borderData: borderData,
        lineBarsData: lineBarsData,
        minX: 0,
        maxX: (maxPointsLength - 1).toDouble(),
        maxY: maxY,
        minY: 0,
      );

  LineTouchData get touchData => LineTouchData(
        handleBuiltInTouches: true,
        touchTooltipData: LineTouchTooltipData(
          tooltipBgColor: Colors.white,
          showOnTopOfTheChartBoxArea: false,
          fitInsideHorizontally: true,
          tooltipHorizontalAlignment: FLHorizontalAlignment.center,
          tooltipBorder: BorderSide(color: Colors.grey.withOpacity(0.5)),
          getTooltipItems: (List<LineBarSpot> touchedSpots) {
            var textStyle = const TextStyle(color: Colors.black, fontSize: 12);
            var list = touchedSpots.map((e) {
              if (e.barIndex != 0) {
                return null;
              }
              List<AppLineChartPoint> points = widget.lines.map((line) => line.points[e.x.toInt()]).toList();
              List<TextSpan> text = widget.onChartTouched(points);
              if (text.isEmpty) {
                return null;
              }
              return LineTooltipItem("", textStyle, textAlign: TextAlign.left, children: [TextSpan(children: text)]);
            }).toList();
            return list;
          },
        ),
      );

  FlTitlesData get titles => FlTitlesData(
        bottomTitles: AxisTitles(sideTitles: bottomTitles),
        rightTitles: AxisTitles(sideTitles: SideTitles(showTitles: false)),
        topTitles: AxisTitles(sideTitles: SideTitles(showTitles: false)),
        leftTitles: AxisTitles(sideTitles: leftTitles()),
      );

  List<LineChartBarData> get lineBarsData {
    List<LineChartBarData> result = [];
    for (var line in widget.lines) {
      var lineChartBarData = LineChartBarData(
        isCurved: false,
        color: line.color,
        barWidth: 2,
        isStrokeJoinRound: true,
        isStrokeCapRound: true,
        dotData: FlDotData(
            show: line.style == AppLineChartLineStyle.solid,
            getDotPainter: (spot, xPercentage, bar, index) => FlDotCirclePainter(color: line.color, strokeColor: Colors.transparent, radius: 3, strokeWidth: 3)),
        belowBarData: BarAreaData(show: false),
        spots: line.points.asMap().entries.where((e) => e.value.value != null).map((e) => FlSpot(e.key.toDouble(), e.value.nonNullValue)).toList(),
      );
      result.add(lineChartBarData);
      if (line.showGuideLine) {
        result.add(guideData(line));
      }
    }
    return result;
  }

  Widget leftTitleWidgets(double value, TitleMeta meta) {
    const style = TextStyle(color: AppThemeData.secondaryColor, fontWeight: FontWeight.normal, fontSize: 12);
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Text(value.toInt().toString(), style: style, textAlign: TextAlign.center),
        const SizedBox(width: 8),
        Container(height: 1, width: 8, color: const Color(0xFFCECECE)),
      ],
    );
  }

  SideTitles leftTitles() => SideTitles(getTitlesWidget: leftTitleWidgets, showTitles: true, interval: yInterval, reservedSize: 40);

  Widget bottomTitleWidgets(double value, TitleMeta meta) {
    const style = TextStyle(color: AppThemeData.secondaryColor, fontWeight: FontWeight.normal, fontSize: 12);
    var index = value.toInt();
    if (_xAxisScales.contains(index)) {
      var point = widget.lines[0].points[index];
      return SideTitleWidget(
          axisSide: meta.axisSide,
          space: 0,
          child: Column(
            children: [
              Container(height: 8, width: 1, color: const Color(0xFFCECECE)),
              const SizedBox(height: 8),
              Text(Time.init(point.date).format(['m', '.', 'd']) ?? '', style: style),
            ],
          ));
    }
    return SideTitleWidget(axisSide: meta.axisSide, space: 10, child: const Text('', style: style));
  }

  SideTitles get bottomTitles => SideTitles(showTitles: true, reservedSize: 32, interval: 1, getTitlesWidget: bottomTitleWidgets);

  FlGridData get gridData => FlGridData(show: false);

  FlBorderData get borderData => FlBorderData(
        show: true,
        border: const Border(
            bottom: BorderSide(color: Color(0xFFCECECE), width: 1),
            left: BorderSide(color: Color(0xFFCECECE), width: 1),
            right: BorderSide(color: Colors.transparent),
            top: BorderSide(color: Colors.transparent)),
      );

  LineChartBarData guideData(AppLineChartLine line) {
    return LineChartBarData(
      isCurved: false,
      color: Colors.grey.withOpacity(0.5),
      barWidth: 2,
      isStrokeCapRound: true,
      dotData: FlDotData(show: false),
      belowBarData: BarAreaData(show: false),
      dashArray: [5, 5],
      spots: [
        FlSpot(0, line.maxValue),
        FlSpot((line.points.length - 1).toDouble(), 0),
      ],
    );
  }
}
