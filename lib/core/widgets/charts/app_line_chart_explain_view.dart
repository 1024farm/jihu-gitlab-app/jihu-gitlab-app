import 'package:flutter/material.dart';

class AppLineChartExplainView extends StatelessWidget {
  const AppLineChartExplainView({required this.name, required this.lineColor, this.dash = false, Key? key}) : super(key: key);
  final String name;
  final Color lineColor;
  final bool dash;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        if (dash) ...[
          Container(color: lineColor, width: 7, height: 2),
          Container(color: Colors.white, width: 2, height: 2),
          Container(color: lineColor, width: 7, height: 2),
        ],
        if (!dash) Container(color: lineColor, width: 16, height: 2),
        const SizedBox(width: 4),
        Text(name, style: const TextStyle(color: Color(0xFF1A1B36), fontSize: 12, fontWeight: FontWeight.normal)),
      ],
    );
  }
}
