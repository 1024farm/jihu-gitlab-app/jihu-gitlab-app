import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';

class LoadingButton extends StatefulWidget {
  final Text text;
  final bool isLoading;
  final bool disabled;
  final Widget loadingWidget;
  final Decoration? decoration;
  final EdgeInsetsGeometry? padding;
  final VoidCallback? onPressed;

  const LoadingButton({required this.text, required this.loadingWidget, this.isLoading = false, this.disabled = false, this.decoration, this.padding, this.onPressed, super.key});

  @override
  State<LoadingButton> createState() => _LoadingButtonState();

  const LoadingButton.asOutlinedButton(
      {required this.text,
      this.isLoading = false,
      this.disabled = false,
      this.decoration = const BoxDecoration(
          color: Colors.white, border: Border.fromBorderSide(BorderSide(color: AppThemeData.primaryColor, width: 1.0, style: BorderStyle.solid)), borderRadius: BorderRadius.all(Radius.circular(4))),
      this.padding = const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      this.loadingWidget = const SizedBox(width: 12, height: 12, child: CircularProgressIndicator(color: AppThemeData.primaryColor, strokeWidth: 1)),
      this.onPressed,
      super.key});

  const LoadingButton.asElevatedButton(
      {required this.text,
      this.isLoading = false,
      this.disabled = false,
      this.decoration = const BoxDecoration(color: AppThemeData.primaryColor, borderRadius: BorderRadius.all(Radius.circular(4))),
      this.padding = const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
      this.loadingWidget = const SizedBox(width: 12, height: 12, child: CircularProgressIndicator(color: Colors.white, strokeWidth: 1)),
      this.onPressed,
      super.key});
}

class _LoadingButtonState extends State<LoadingButton> {
  @override
  Widget build(BuildContext context) {
    bool enabled = !widget.isLoading && !widget.disabled;
    return InkWell(
      onTap: enabled ? widget.onPressed : null,
      child: Opacity(
        opacity: enabled ? 1 : 0.5,
        child: Container(
          padding: widget.padding,
          decoration: widget.decoration,
          child: Row(mainAxisSize: MainAxisSize.min, mainAxisAlignment: MainAxisAlignment.center, children: [
            if (widget.isLoading) widget.loadingWidget,
            if (widget.isLoading) const SizedBox(width: 8),
            widget.text,
          ]),
        ),
      ),
    );
  }
}
