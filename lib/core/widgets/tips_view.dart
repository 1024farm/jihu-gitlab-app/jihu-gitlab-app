import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/domain/email.dart';
import 'package:jihu_gitlab_app/core/domain/url_redictor.dart';
import 'package:jihu_gitlab_app/core/widgets/linker_text.dart';
import 'package:jihu_gitlab_app/core/widgets/loading_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class TipsView extends StatefulWidget {
  const TipsView({super.key, this.icon, this.message, this.line2, this.email, this.onRefresh});

  final String? icon;
  final String? message;
  final String? line2;
  final Email? email;
  final Future<void> Function()? onRefresh;

  @override
  State<TipsView> createState() => _TipsViewState();
}

class _TipsViewState extends State<TipsView> {
  bool inLoad = false;

  @override
  Widget build(BuildContext context) {
    return inLoad
        ? const LoadingView()
        : SafeArea(
            child: InkWell(
              highlightColor: Colors.transparent,
              // 0.0: forbidden water ripples
              // null: default water ripples
              radius: widget.onRefresh == null ? 0.0 : null,
              onTap: () {
                if (widget.onRefresh == null) return;
                if (mounted) setState(() => inLoad = true);
                widget.onRefresh?.call().then((value) {
                  Future.delayed(const Duration(seconds: 1)).then(
                    (_) {
                      if (mounted) setState(() => inLoad = false);
                    },
                  );
                });
              },
              child: Container(
                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.2),
                child: Column(
                  children: [
                    _picture(),
                    Container(
                      height: 5,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: Text(
                          widget.message ?? AppLocalizations.dictionary().noData,
                          style: const TextStyle(fontSize: 14, color: Color.fromRGBO(23, 19, 33, 1)),
                        )),
                    Padding(padding: const EdgeInsets.only(left: 20, right: 20), child: line2Widget())
                  ],
                ),
              ),
            ),
          );
  }

  Widget line2Widget() {
    return widget.line2 == null
        ? Container()
        : Column(children: [
            Container(
              height: 3,
            ),
            LinkerText(
              text: widget.line2!,
              options: const LinkifyOptions(humanize: false),
              style: const TextStyle(fontSize: 12, color: Color.fromRGBO(23, 19, 33, 1)),
              onOpen: (link) async => UrlRedirector().redirect(link, context, widget.email),
            )
          ]);
  }

  Widget _picture() {
    if (widget.icon == null || widget.icon!.endsWith('.png')) {
      return Image.asset(
        widget.icon ?? "assets/images/no_groups.png",
        height: 40,
        width: 40,
      );
    }
    return SvgPicture.asset(
      widget.icon!,
      height: 40,
      width: 40,
    );
  }
}
