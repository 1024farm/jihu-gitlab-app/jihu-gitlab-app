import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';

class SingleChoiceInput extends StatelessWidget {
  const SingleChoiceInput({
    required this.onAdd,
    required this.onClear,
    this.placeHolder,
    this.value,
    this.clearable = true,
    Key? key,
  }) : super(key: key);
  final String? placeHolder;
  final String? value;
  final VoidCallback onAdd;
  final VoidCallback onClear;
  final bool clearable;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: const BoxDecoration(border: Border.fromBorderSide(BorderSide(color: Color(0XFFEAEAEA), width: 1)), borderRadius: BorderRadius.all(Radius.circular(4)), color: Colors.white),
      padding: const EdgeInsets.only(left: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            flex: 1,
            child: Text(value ?? placeHolder ?? '',
                overflow: TextOverflow.ellipsis,
                style: value == null
                    ? const TextStyle(color: AppThemeData.secondaryColor, fontSize: 14, fontWeight: FontWeight.w400)
                    : const TextStyle(color: Color(0xFF03162F), fontSize: 14, fontWeight: FontWeight.w400)),
          ),
          IconButton(
              alignment: Alignment.centerRight,
              onPressed: () {
                if (value != null && clearable) {
                  onClear();
                } else {
                  onAdd();
                }
              },
              icon: value != null && clearable
                  ? SvgPicture.asset('assets/images/clear.svg', colorFilter: const ColorFilter.mode(Color(0xFF95979A), BlendMode.srcIn), width: 18)
                  : const Icon(Icons.add, size: 22, color: Color(0xFF87878C))),
        ],
      ),
    );
  }
}
