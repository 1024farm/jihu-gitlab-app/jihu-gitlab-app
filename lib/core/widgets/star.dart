import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/stars_provider.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project_repository.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/subgroups/subgroup_list_model.dart';

typedef ItemDataProvider = GroupAndProject Function();

class Star extends StatefulWidget {
  final ItemDataProvider itemDataProvider;
  final VoidCallback? starChanged;

  const Star({required this.itemDataProvider, this.starChanged, Key? key}) : super(key: key);

  @override
  State<Star> createState() => _StarState();
}

class _StarState extends State<Star> {
  late StarModel _model;
  bool _loading = false;

  @override
  void initState() {
    super.initState();
    var data = widget.itemDataProvider();
    _model = StarModel(data);
  }

  @override
  Widget build(BuildContext context) {
    Icon notStar = const Icon(Icons.star_border_rounded, color: Colors.grey);
    Icon starred = Icon(Icons.star_rounded, color: Theme.of(context).primaryColor);
    return Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
      _buildLoadingView(),
      InkWell(
          child: _model.starred ? starred : notStar,
          onTap: () async {
            if (_model.type == SubgroupItemType.project) {
              setState(() => _loading = true);
            }
            await _model.toggleStar();
            setState(() => {_loading = false});
            widget.starChanged?.call();
            StarsProvider().changeStarred();
          }),
    ]);
  }

  SizedBox _buildLoadingView() {
    return _loading ? const SizedBox(width: 12, height: 12, child: CircularProgressIndicator(strokeWidth: 1)) : const SizedBox(width: 0);
  }

  @override
  void didUpdateWidget(covariant Star oldWidget) {
    _model = StarModel(widget.itemDataProvider());
    super.didUpdateWidget(oldWidget);
  }
}

class StarModel {
  late GroupAndProjectRepository _repository;
  GroupAndProject data;

  StarModel(this.data) {
    _repository = GroupAndProjectRepository.instance();
  }

  Future<bool> toggleStar() async {
    bool star = !data.starred;
    bool result = false;
    if (data.type == SubgroupItemType.project) {
      String path = Api.join("projects/${data.iid}/${star ? 'star' : 'unstar'}");
      try {
        await HttpClient.instance().post(path, {});
        result = true;
      } catch (error) {
        var e = error as DioError;
        if (304 == e.response?.statusCode) {
          result = true;
        }
      }
    } else {
      result = true;
    }
    if (result) {
      data.starred = star;
      _repository.toggleStar(data.id!, starred: star);
    }
    return Future.value(result);
  }

  bool get starred => data.starred;

  SubgroupItemType? get type => data.type;
}
