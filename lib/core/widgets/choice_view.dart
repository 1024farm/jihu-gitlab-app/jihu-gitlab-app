import 'dart:async';

import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/widgets/streaming_container.dart';

typedef ChoiceLabelMapper<T> = String Function(T t);

typedef OnChoiceChange<T> = FutureOr<bool?> Function(T t);

typedef ChoiceItemBuilder<T> = Widget Function(T t, bool selected);

class ChoiceView<T> extends StatefulWidget {
  const ChoiceView({
    required this.candidates,
    required this.onChoiceChange,
    required this.itemBuilder,
    this.selected,
    this.spacing,
    this.multi = false,
    Key? key,
  }) : super(key: key);
  final List<T> candidates;
  final bool multi;
  final ChoiceItemBuilder<T> itemBuilder;
  final List<T>? selected;
  final OnChoiceChange? onChoiceChange;
  final double? spacing;

  @override
  State<ChoiceView> createState() => _ChoiceViewState();
}

class _ChoiceViewState<T> extends State<ChoiceView<T>> {
  Set<T> _selectedIndex = {};

  @override
  void initState() {
    _selectedIndex = (widget.selected ?? []).toSet();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamingContainer(
      spacing: widget.spacing ?? 8,
      children: widget.candidates.map((e) {
        bool selected = _selectedIndex.contains(e);
        var child = widget.itemBuilder(e, selected);
        return InkWell(
          onTap: () async {
            bool canChange = await widget.onChoiceChange?.call(e) ?? true;
            if (canChange) {
              _changeSelection(e, selected: selected);
            }
          },
          child: child,
        );
      }).toList(),
    );
  }

  void _changeSelection(T t, {required bool selected}) {
    setState(() {
      if (selected) {
        if (widget.multi) _selectedIndex.remove(t);
      } else {
        if (!widget.multi) {
          _selectedIndex.clear();
        }
        _selectedIndex.add(t);
      }
    });
  }
}
