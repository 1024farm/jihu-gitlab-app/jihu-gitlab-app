import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

typedef OnSubmitted = ValueChanged<String>?;
typedef OnClear = VoidCallback?;

class SearchBox extends StatefulWidget {
  final TextEditingController searchController;
  final OnSubmitted onSubmitted;
  final OnClear onClear;
  final ValueChanged<String>? onChanged;

  const SearchBox({required this.searchController, this.onSubmitted, this.onClear, this.onChanged, super.key});

  @override
  State<StatefulWidget> createState() => _SearchBoxState();
}

class _SearchBoxState extends State<SearchBox> {
  late FocusNode focusNode;
  bool onSearchBox = false;

  @override
  void initState() {
    focusNode = FocusNode();
    focusNode.addListener(() {
      setState(() => onSearchBox = !onSearchBox);
    });
    super.initState();
  }

  @override
  void dispose() {
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        constraints: const BoxConstraints(maxHeight: 42),
        child: TextField(
            style: TextStyle(fontSize: 16, color: onSearchBox ? Colors.black : const Color(0xFF95979A), fontWeight: FontWeight.w400),
            focusNode: focusNode,
            onSubmitted: widget.onSubmitted,
            onChanged: widget.onChanged,
            controller: widget.searchController,
            decoration: InputDecoration(
                hintText: AppLocalizations.dictionary().search,
                fillColor: onSearchBox ? Colors.white : const Color(0xFFEAEAEA),
                filled: true,
                contentPadding: const EdgeInsets.all(0),
                suffixIcon: onSearchBox
                    ? Padding(
                        padding: const EdgeInsets.all(0),
                        child: IconButton(
                            onPressed: () {
                              focusNode.unfocus();
                              (widget.onClear ?? () {})();
                            },
                            icon: SvgPicture.asset(
                              'assets/images/clear.svg',
                              colorFilter: const ColorFilter.mode(Color(0xFF95979A), BlendMode.srcIn),
                              width: 18,
                              key: const Key('close'),
                            )))
                    : null,
                suffixIconConstraints: const BoxConstraints(minHeight: 18),
                prefixIcon: Padding(
                    padding: const EdgeInsets.only(top: 0, left: 10, right: 7, bottom: 1),
                    child: SvgPicture.asset(
                      'assets/images/search.svg',
                      colorFilter: const ColorFilter.mode(Color(0xFF95979A), BlendMode.srcIn),
                      width: 18,
                    )),
                prefixIconConstraints: const BoxConstraints(minHeight: 18),
                border: OutlineInputBorder(gapPadding: 0, borderRadius: BorderRadius.circular(4), borderSide: onSearchBox ? const BorderSide(color: Color(0xFFEAEAEA)) : BorderSide.none))));
  }
}
