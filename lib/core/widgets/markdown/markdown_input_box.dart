import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:jihu_gitlab_app/core/app_platform.dart';
import 'package:jihu_gitlab_app/core/dependency_injector.dart';
import 'package:jihu_gitlab_app/core/domain/issue.dart';
import 'package:jihu_gitlab_app/core/file_uploader.dart';
import 'package:jihu_gitlab_app/core/loader.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/markdown_view.dart';
import 'package:jihu_gitlab_app/core/string_extension.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar_and_name.dart';
import 'package:jihu_gitlab_app/core/widgets/markdown/number_sign_syntax.dart';
import 'package:jihu_gitlab_app/core/widgets/photo_picker.dart';
import 'package:jihu_gitlab_app/core/widgets/selector/selector.dart';
import 'package:jihu_gitlab_app/core/widgets/toast.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/issue_description_view.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issue_selector.dart';
import 'package:jihu_gitlab_app/modules/modules.dart';

class MarkdownInputBox extends StatefulWidget {
  const MarkdownInputBox(
      {required this.controller,
      required this.projectId,
      required this.onChanged,
      this.placeholder = '',
      this.minHeight = 280,
      this.scrollable = false,
      this.issueSelectable = true,
      this.memberSelectable = true,
      this.videoSelectable = true,
      this.photoSelectable = true,
      super.key});

  final TextEditingController controller;

  final int projectId;
  final Function() onChanged;
  final String placeholder;
  final double minHeight;
  final bool scrollable;
  final bool issueSelectable;
  final bool memberSelectable;
  final bool videoSelectable;
  final bool photoSelectable;

  @override
  State<MarkdownInputBox> createState() => _MarkdownInputBoxState();
}

class _MarkdownInputBoxState extends State<MarkdownInputBox> with TickerProviderStateMixin {
  int _selectedTabBarIndex = 0;
  UploadedFileInfo? _uploadedFileInfo;
  int _lastSelectionOffset = 0;
  PhotoPicker? _photoPicker;

  @override
  Widget build(BuildContext context) {
    final tabs = <String>[AppLocalizations.dictionary().write, AppLocalizations.dictionary().preview];
    return Container(
      padding: const EdgeInsets.all(12),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: const Color(0xFFEAEAEA)),
        borderRadius: BorderRadius.circular(4.0),
      ),
      constraints: BoxConstraints(minHeight: widget.minHeight, maxHeight: widget.scrollable ? widget.minHeight : double.infinity),
      child: DefaultTabController(
        initialIndex: 0,
        length: 2,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Row(
            children: [
              Expanded(
                  flex: 20,
                  child: Container(
                      alignment: Alignment.centerLeft,
                      constraints: const BoxConstraints(maxWidth: 140),
                      child: TabBar(
                        isScrollable: true,
                        padding: EdgeInsets.zero,
                        indicatorSize: TabBarIndicatorSize.label,
                        indicatorWeight: 2,
                        indicatorColor: Theme.of(context).primaryColor,
                        indicatorPadding: const EdgeInsets.only(left: -6, right: 6),
                        labelColor: Colors.black,
                        labelStyle: Theme.of(context).textTheme.titleLarge,
                        unselectedLabelColor: Colors.black54,
                        labelPadding: const EdgeInsets.only(top: 8, bottom: 8, right: 12),
                        splashFactory: NoSplash.splashFactory,
                        tabs: [for (String tab in tabs) FittedBox(fit: BoxFit.scaleDown, child: Text(tab, overflow: TextOverflow.ellipsis))],
                        onTap: (index) => setState(() => _selectedTabBarIndex = index),
                      ))),
              const Expanded(child: SizedBox()),
              if (widget.issueSelectable) _buildImageButton(assetName: "assets/images/numbers.svg", onTap: () => _onNumbersButtonPressed()),
              if (widget.memberSelectable) _buildImageButton(assetName: "assets/images/alternate_email.svg", onTap: () => _onAtButtonPressed()),
              if (widget.videoSelectable) _buildImageButton(assetName: "assets/images/video_picker.svg", onTap: () => _onImageButtonPressed("video")),
              if (widget.photoSelectable) _buildImageButton(assetName: "assets/images/photo_picker.svg", onTap: () => _onImageButtonPressed("image")),
            ],
          ),
          const Divider(height: 0, thickness: 0.5),
          widget.scrollable ? Expanded(child: _buildContent()) : _buildContent(),
          if (AppPlatform.isAndroid && _photoPicker != null)
            FutureBuilder<PhotoPickerResult>(
              future: _photoPicker!.retrieveLostData(),
              builder: (BuildContext context, AsyncSnapshot<PhotoPickerResult> snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                  case ConnectionState.waiting:
                    break;
                  case ConnectionState.done:
                    LogHelper.info("ConnectionState.done, data size: ${(snapshot.data?.files ?? []).length}");
                    _uploadFiles(snapshot.data?.files ?? []);
                    break;
                  default:
                    if (snapshot.hasError) {
                      Toast.show('${AppLocalizations.dictionary().imageErrorToast} ${snapshot.error}');
                    }
                }
                return Container();
              },
            )
        ]),
      ),
    );
  }

  Widget _buildImageButton({required String assetName, required GestureTapCallback onTap}) {
    return InkWell(
        onTap: onTap,
        child: Container(
            alignment: Alignment.centerRight,
            height: 44,
            width: 38,
            child: SvgPicture.asset(
              assetName,
              width: 20,
              height: 20,
            )));
  }

  Widget _buildContent() {
    return Builder(builder: (_) {
      if (_selectedTabBarIndex == 0) {
        return TextField(
          scrollController: widget.scrollable ? ScrollController() : null,
          scribbleEnabled: widget.scrollable,
          controller: widget.controller,
          maxLines: widget.scrollable ? 10 : null,
          onChanged: (text) async {
            widget.onChanged();
            if (text == '') return;
            var index = widget.controller.selection.base.offset;
            if (text[index - 1] == '@' && index >= _lastSelectionOffset && widget.memberSelectable) {
              List<Member> result = await _openMemberSelector();
              if (result.isNotEmpty) {
                var selectedUsername = '${result[0].username} ';
                var finalText = text.addByIndex(selectedUsername, index);
                widget.controller.text = finalText;
                widget.controller.selection = TextSelection(
                  baseOffset: index + selectedUsername.length,
                  extentOffset: index + selectedUsername.length,
                );
              }
            }
            _lastSelectionOffset = index;
            setState(() {});
          },
          style: Theme.of(context).textTheme.bodyMedium,
          cursorColor: Theme.of(context).primaryColor,
          decoration: InputDecoration(
            hintText: widget.placeholder == '' ? AppLocalizations.dictionary().writePlaceholder : widget.placeholder,
            hintStyle: const TextStyle(fontSize: 14),
            border: InputBorder.none,
          ),
        );
      }
      return widget.scrollable ? SizedBox(height: 210, child: _buildMarkdownView()) : _buildMarkdownView(physics: const NeverScrollableScrollPhysics());
    });
  }

  MarkdownView _buildMarkdownView({ScrollPhysics? physics}) {
    return MarkdownView(
      context: context,
      data: widget.controller.text.isEmpty ? AppLocalizations.dictionary().noPreview : widget.controller.text,
      physics: physics,
      builders: {"#": NumberSignBuilder(projectId: widget.projectId, context: context)},
      inlineSyntaxes: [NumberSignSyntax()],
    );
  }

  Future<void> _onImageButtonPressed(String type) async {
    _photoPicker = _photoPicker ?? locator<PhotoPicker>();
    var accessible = await _photoPicker!.checkAccess();
    if (!accessible) {
      setState(() => _photoPicker!.requireAccess(context));
      return;
    }
    PhotoPickerResult result = type == "video" ? await _photoPicker!.pickVideo() : await _photoPicker!.pickImage();
    if (result.files.isEmpty) {
      setState(() {
        if (result.isPhotoAccessDeniedError) _photoPicker!.requireAccess(context);
      });
      return;
    }
    _uploadFiles(result.files);
  }

  void _onNumbersButtonPressed() async {
    var text = widget.controller.text;
    var index = widget.controller.selection.base.offset;
    Issue? result = await _openIssueSelector();
    if (result != null) {
      var selectedUsername = '#${result.iid} ';
      var finalText = text.addByIndex(selectedUsername, index);
      widget.controller.text = finalText;
      widget.controller.selection = TextSelection(
        baseOffset: index + selectedUsername.length,
        extentOffset: index + selectedUsername.length,
      );
      widget.onChanged();
    }
    setState(() {});
  }

  Future<void> _onAtButtonPressed() async {
    var text = widget.controller.text;
    var index = widget.controller.selection.base.offset;
    List<Member> result = await _openMemberSelector();
    if (result.isNotEmpty) {
      var selectedUsername = '@${result[0].username} ';
      var finalText = text.addByIndex(selectedUsername, index);
      widget.controller.text = finalText;
      widget.controller.selection = TextSelection(
        baseOffset: index + selectedUsername.length,
        extentOffset: index + selectedUsername.length,
      );
      widget.onChanged();
    }
    setState(() {});
  }

  Future<void> _uploadFiles(List<XFile> files) async {
    if (files.isNotEmpty) {
      LogHelper.info('_startUploadFiles not null');
      var file = files[0];
      Loader.showFutureProgress(context, text: AppLocalizations.dictionary().uploadToast);
      FileUploadResult result = await FileUploader.instance().upload(projectId: widget.projectId, file: file);
      if (result.success) {
        _uploadedFileInfo = result.uploadedFileInfo;
        var text = widget.controller.text;
        var index = widget.controller.selection.base.offset;
        text = text.addByIndex(_uploadedFileInfo!.markdown, index);
        widget.controller.value = TextEditingValue(text: text, selection: TextSelection.fromPosition(TextPosition(affinity: TextAffinity.downstream, offset: text.length)));
        widget.onChanged();
      } else {
        Toast.show('${AppLocalizations.dictionary().markdownUploadFileError}${result.error}');
      }
      setState(() {
        Loader.hideFutureProgress(context);
      });
    }
  }

  Future<List<Member>> _openMemberSelector() async {
    return (await Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          return Selector<int, Member>(
            itemBuilder: <Member>(context, index, member) => AvatarAndName(username: member.username, avatarUrl: member.avatarUrl),
            filter: (keyword, member) => member.username.toLowerCase().contains(keyword.toLowerCase()),
            dataProvider: MemberProvider(projectId: widget.projectId),
            keyMapper: (member) => member.id,
            title: AppLocalizations.dictionary().selectContact,
            key: const Key('member-selector'),
          );
        })) ??
        <Member>[]) as List<Member>;
  }

  Future<Issue?> _openIssueSelector() async {
    var result = await Navigator.of(context).pushNamed(IssueSelector.routeName, arguments: {"projectId": widget.projectId});
    return Future(() => result == null ? null : (result as Issue));
  }
}
