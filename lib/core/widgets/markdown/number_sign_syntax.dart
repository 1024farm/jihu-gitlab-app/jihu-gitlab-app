import 'package:markdown/markdown.dart' as md;

class NumberSignSyntax extends md.DelimiterSyntax {
  NumberSignSyntax()
      : super(
          '#[0-9]+',
          requiresDelimiterRun: true,
          allowIntraWord: true,
        );

  @override
  bool onMatch(md.InlineParser parser, Match match) {
    md.Element ulTag = md.Element.text('#', match[0] ?? "#0");
    parser.addNode(ulTag);
    return true;
  }
}
