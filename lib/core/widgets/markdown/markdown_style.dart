import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';

class MarkdownStyle extends MarkdownStyleSheet {
  MarkdownStyle()
      : super(
          h1: const TextStyle(fontSize: 28, fontWeight: FontWeight.w800, decoration: TextDecoration.underline),
          h1Padding: const EdgeInsets.only(top: 10, bottom: 10),
          h2: const TextStyle(fontSize: 24, fontWeight: FontWeight.w800, decoration: TextDecoration.underline),
          h2Padding: const EdgeInsets.only(top: 6, bottom: 6),
          h3: const TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
          h4: const TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
          h5: const TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
          h6: const TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
          a: const TextStyle(color: Color(0xFF0D10FC)),
          code: const TextStyle(backgroundColor: Color.fromRGBO(250, 250, 250, 1.0)),
          codeblockDecoration: const BoxDecoration(
            color: Color.fromRGBO(250, 250, 250, 1.0),
          ),
          blockquoteDecoration: const BoxDecoration(
            border: Border(left: BorderSide(color: Colors.black45, width: 3)),
          ),
          blockquotePadding: const EdgeInsets.only(top: 8, bottom: 8, left: 16, right: 0),
        );
}
