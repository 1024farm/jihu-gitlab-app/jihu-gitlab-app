import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';

class SearchResult extends StatelessWidget {
  final String keyword;
  final String text;
  final TextStyle? hitStyle;
  final TextStyle? keyWordStyle;

  const SearchResult({required this.keyword, required this.text, this.hitStyle, this.keyWordStyle, super.key});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Text.rich(TextSpan(
        children: children(),
      )),
    );
  }

  List<TextSpan> children() {
    return TextSpans.init(hitStyle, keyWordStyle).splitKeywordFromTextAndGet(text, keyword);
  }
}

class TextSpans {
  final List<TextSpan> _impl;
  final TextStyle? hitStyle;
  final TextStyle? keyWordStyle;

  TextSpans._(this._impl, this.hitStyle, this.keyWordStyle);

  factory TextSpans.init(TextStyle? hitStyle, TextStyle? keyWordStyle) {
    return TextSpans._([], hitStyle, keyWordStyle);
  }

  List<TextSpan> splitKeywordFromTextAndGet(String textString, String keywordString) {
    var text = SearchingString(textString, hitStyle, keyWordStyle);
    var keyword = SearchingString(keywordString, hitStyle, keyWordStyle);
    if (!text.contains(keyword)) {
      _impl.add(text._toNotHitTextSpan());
      return _impl;
    }
    while (text.contains(keyword)) {
      _impl.addAll(text.keywordAndBeforeToTextSpansAndKeepLeftText(keyword));
    }
    _impl.add(text._toNotHitTextSpan());
    return _impl;
  }
}

class SearchingString {
  String _impl;
  final TextStyle? hitStyle;
  final TextStyle? keyWordStyle;

  SearchingString(this._impl, this.hitStyle, this.keyWordStyle);

  List<TextSpan> keywordAndBeforeToTextSpansAndKeepLeftText(SearchingString keyword) {
    var index = _impl.toLowerCase().indexOf(keyword._impl.toLowerCase());
    var keywordText = substring(index, index + keyword.length);
    List<TextSpan> result = [];
    result.add(substring(0, index)._toNotHitTextSpan());
    result.add(keywordText._toHitTextSpan());
    _impl = _impl.substring(index + keywordText.length);
    return result;
  }

  bool contains(SearchingString that) {
    return that._impl.isNotEmpty && _impl.toLowerCase().contains(that._impl.toLowerCase());
  }

  SearchingString substring(int start, [int? end]) {
    return SearchingString(_impl.substring(start, end), hitStyle, keyWordStyle);
  }

  TextSpan _toHitTextSpan() {
    return TextSpan(
      text: _impl,
      style: hitStyle ?? const TextStyle(fontSize: 15, color: AppThemeData.primaryColor, fontWeight: FontWeight.w600),
    );
  }

  TextSpan _toNotHitTextSpan() {
    return TextSpan(
      text: _impl,
      style: keyWordStyle ?? const TextStyle(fontSize: 15, color: Color(0xFF1A1B36), fontWeight: FontWeight.w600),
    );
  }

  int get length => _impl.length;
}
