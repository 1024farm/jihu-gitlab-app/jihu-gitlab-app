import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar/avatar.dart';

class AvatarAndName extends StatelessWidget {
  final String username;
  final String avatarUrl;
  final double avatarSize;
  final TextStyle nameStyle;
  final double gap;

  const AvatarAndName(
      {required this.username,
      required this.avatarUrl,
      this.avatarSize = 32,
      this.nameStyle = const TextStyle(fontWeight: FontWeight.w400, fontSize: 16, color: Color(0XFF03162F)),
      this.gap = 12,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _buildWidget();
  }

  Widget _buildWidget() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(right: gap),
          child: Avatar(avatarUrl: avatarUrl, size: avatarSize),
        ),
        Flexible(child: Text(username, style: nameStyle))
      ],
    );
  }
}
