import 'package:flutter/cupertino.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';

class SearchNoResultView extends StatelessWidget {
  final String? message;
  final Future<void> Function()? onRefresh;

  const SearchNoResultView({required this.message, this.onRefresh, super.key});

  @override
  Widget build(BuildContext context) {
    return TipsView(icon: 'assets/images/search.svg', message: message, onRefresh: onRefresh);
  }
}
