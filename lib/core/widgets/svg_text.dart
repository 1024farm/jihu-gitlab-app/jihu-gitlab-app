import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';

class SvgText extends StatelessWidget {
  const SvgText({
    required this.svgAssetPath,
    this.svgColor = AppThemeData.iconColor,
    this.spacing = 4,
    this.svgSize,
    this.text,
    this.textStyle,
    this.semanticsLabel,
    super.key,
  });

  final String svgAssetPath;
  final String? text;
  final Color svgColor;
  final Size? svgSize;
  final TextStyle? textStyle;
  final String? semanticsLabel;
  final double spacing;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        SvgPicture.asset(
          svgAssetPath,
          colorFilter: ColorFilter.mode(svgColor, BlendMode.srcIn),
          width: svgSize?.width,
          height: svgSize?.height,
          semanticsLabel: semanticsLabel,
        ),
        SizedBox(width: spacing),
        if (text != null) Flexible(child: Text(text!, style: textStyle, overflow: TextOverflow.ellipsis))
      ],
    );
  }
}
