import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/auth/auth.dart';

class ConnectJiHuPage extends StatefulWidget {
  const ConnectJiHuPage({super.key});

  @override
  State<ConnectJiHuPage> createState() => _ConnectJiHuPageState();
}

class _ConnectJiHuPageState extends State<ConnectJiHuPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Text.rich(
                  TextSpan(children: [
                    TextSpan(text: AppLocalizations.dictionary().connectWith),
                    TextSpan(text: " jihulab.com ", style: TextStyle(color: Theme.of(context).primaryColor)),
                    TextSpan(text: AppLocalizations.dictionary().toParticipate),
                  ]),
                  textAlign: TextAlign.center,
                  style: const TextStyle(fontSize: 16, color: Color(0xFF03162F), fontWeight: FontWeight.w600),
                )),
            const SizedBox(height: 4),
            Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Text(AppLocalizations.dictionary().inDiscussionsAndCreatePosts,
                    textAlign: TextAlign.center, style: const TextStyle(fontSize: 16, color: Color(0xFF03162F), fontWeight: FontWeight.w600))),
            Padding(padding: const EdgeInsets.only(left: 20, right: 20), child: line2Widget()),
            Padding(padding: const EdgeInsets.only(left: 20, right: 20), child: toConnectButton()),
          ],
        ),
      ),
    );
  }

  Widget line2Widget() {
    return Column(children: [
      Container(
        height: 8,
      ),
      Text(AppLocalizations.dictionary().otherConnectionNotAllow, style: const TextStyle(fontSize: 12, color: AppThemeData.secondaryColor))
    ]);
  }

  Widget toConnectButton() {
    var colorScheme = Theme.of(context).colorScheme;
    return Column(
      children: [
        const SizedBox(height: 16),
        InkWell(
          onTap: () {
            Navigator.of(context).pushNamed(LoginPage.routeName, arguments: {'host': 'jihulab.com'});
          },
          child: Container(
              height: 40,
              width: 140,
              margin: const EdgeInsets.all(16),
              decoration: BoxDecoration(color: colorScheme.primary, borderRadius: BorderRadius.circular(4.0)),
              child: Center(child: Text(AppLocalizations.dictionary().toConnect, style: const TextStyle(color: Colors.white, fontSize: 15)))),
        ),
      ],
    );
  }
}
