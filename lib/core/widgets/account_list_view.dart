import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connections.dart';
import 'package:jihu_gitlab_app/core/themes/project_style.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';
import 'package:jihu_gitlab_app/core/widgets/avatar/avatar.dart';
import 'package:jihu_gitlab_app/core/widgets/toast.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

List<PopupMenuEntry<String>> accountItems(BuildContext context, {bool isAccountsPage = false}) {
  List<PopupMenuEntry<String>> results = [];
  for (var i = 0; i < ConnectionProvider.connections.length; i++) {
    if (i > 0) results.add(const PopupMenuDivider(height: 1));
    final connection = ConnectionProvider.connections[i];
    results.add(_buildMenuItem(connection, isAccountsPage));
  }
  if (loginMenuList.length > ConnectionProvider.connections.length) {
    results.add(const PopupMenuDivider(height: 1));
    results.add(_buildAddAccountButton(context, isAccountsPage));
  }
  return results;
}

PopupMenuItem<String> _buildMenuItem(Connection connection, bool isAccountsPage) {
  return PopupMenuItem<String>(
    padding: EdgeInsets.zero,
    onTap: () => ConnectionProvider().changeAccount(connection),
    value: connection.userInfo.name,
    child: isAccountsPage ? _buildSlidableItem(connection) : _buildAccountItem(connection),
  );
}

Slidable _buildSlidableItem(Connection connection) {
  return Slidable(
    closeOnScroll: false,
    key: Key(UniqueKey().toString()),
    endActionPane: ActionPane(
      motion: const ScrollMotion(),
      extentRatio: 0.2,
      children: [
        SlidableAction(
          onPressed: (context) => ConnectionProvider().loggedOutSelectedConnection(connection),
          backgroundColor: AppThemeData.primaryColor,
          foregroundColor: Colors.white,
          icon: Icons.exit_to_app,
          borderRadius: const BorderRadius.only(bottomRight: Radius.circular(4), topRight: Radius.circular(4)),
        ),
      ],
    ),
    child: _buildAccountItem(connection),
  );
}

Container _buildAccountItem(Connection connection) {
  return Container(
    padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
    child: Row(
      children: [
        Container(
            decoration: BoxDecoration(border: Border.all(color: const Color(0xFFCECECE)), borderRadius: BorderRadius.circular(20.0)),
            child: Avatar(avatarUrl: connection.userInfo.avatarUrl, size: 40)),
        const SizedBox(width: 8),
        Expanded(
          child: _buildNameWithAccountType(connection),
        ),
        if (connection.active) SvgPicture.asset('assets/images/accounts_active.svg', width: 16, height: 16),
      ],
    ),
  );
}

Column _buildNameWithAccountType(Connection connection) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text('@${connection.userInfo.username}', overflow: TextOverflow.ellipsis, style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w600, color: Color(0xFF03162F))),
      const SizedBox(height: 8),
      Container(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
        decoration: const BoxDecoration(color: Color(0xFFEAEAEA), borderRadius: BorderRadius.all(Radius.circular(4))),
        child: Text(connection.serverType, style: const TextStyle(color: AppThemeData.secondaryColor, fontSize: 14)),
      ),
    ],
  );
}

PopupMenuItem<String> _buildAddAccountButton(BuildContext context, bool isAccountsPage) {
  return PopupMenuItem<String>(
      key: const Key('AddAccountButton'),
      padding: EdgeInsets.zero,
      onTap: () {},
      value: 'add_account',
      child: MenuAnchor(
        alignmentOffset: const Offset(10.0, 0.0),
        menuChildren: _buildAccountTypeList(context, isAccountsPage),
        builder: (BuildContext context, MenuController controller, Widget? child) {
          return InkWell(
            onTap: () {
              if (controller.isOpen) {
                controller.close();
              } else {
                controller.open();
              }
            },
            child: _buildAddAccountTitle(),
          );
        },
      ));
}

Widget _buildAddAccountTitle() {
  return Padding(
    padding: const EdgeInsets.all(8),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          margin: const EdgeInsets.only(right: 8),
          child: SvgPicture.asset('assets/images/add_account.svg', width: 40, height: 40),
        ),
        Expanded(child: Text(AppLocalizations.dictionary().addAccount, style: const TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Colors.black87)))
      ],
    ),
  );
}

List<Widget> _buildAccountTypeList(BuildContext context, bool isAccountsPage) {
  return loginMenuList.keys
      .map((key) => _buildMenuItemButton(context, loginMenuList.keys.toList().indexOf(key), key, () {
            var item = loginMenuList[key];
            if (ConnectionProvider().isUsed(key)) {
              Toast.error(context, AppLocalizations.dictionary().sameTypeAccountOnlyLoginOne);
              return;
            }
            if (item == null) return;
            if (!isAccountsPage) Navigator.pop(context);
            Navigator.of(context).pushNamed(item.pageRouteName, arguments: {'host': item.host});
          }))
      .toList();
}

Widget _buildMenuItemButton(BuildContext context, int index, String key, VoidCallback? onPressed) {
  return Column(
    children: [
      if (index > 0) const PopupMenuDivider(height: 1),
      MenuItemButton(
        onPressed: onPressed,
        child: SizedBox(
            width: 160,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(key, style: ProjectStyle.build(Theme.of(context).textTheme.titleSmall, ConnectionProvider().isUsed(key) ? const Color(0xFF87878C) : Colors.black87)),
                if (ConnectionProvider().isUsed(key))
                  Text(AppLocalizations.dictionary().alreadyUsed, style: const TextStyle(color: AppThemeData.secondaryColor, fontSize: 10, fontWeight: FontWeight.w400))
              ],
            )),
      )
    ],
  );
}
