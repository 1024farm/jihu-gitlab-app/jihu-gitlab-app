import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/domain/email.dart';
import 'package:jihu_gitlab_app/core/domain/url_redictor.dart';
import 'package:jihu_gitlab_app/core/widgets/linker_text.dart';

class WarningView extends StatelessWidget {
  final String title;
  final String description;
  final Email? email;

  const WarningView({required this.title, required this.description, this.email, super.key});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: const FractionalOffset(0.5, 0.2),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const SizedBox(height: 14),
          SvgPicture.asset("assets/images/warning.svg", height: 30, width: 30),
          const SizedBox(height: 4),
          Text(title, style: const TextStyle(color: Color(0xFF1A1B36), fontSize: 16, fontWeight: FontWeight.w600), textAlign: TextAlign.center),
          const SizedBox(height: 8),
          SizedBox(
            width: 268,
            child: LinkerText(
              text: description,
              options: const LinkifyOptions(humanize: false),
              style: const TextStyle(color: Color(0xFF1A1B36), fontSize: 14, fontWeight: FontWeight.w400),
              textAlign: TextAlign.center,
              onOpen: (link) async => UrlRedirector().redirect(link, context, email),
            ),
          )
        ],
      ),
    );
  }
}
