import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class ProjectArchivedNoticeView extends StatelessWidget {
  const ProjectArchivedNoticeView({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(horizontal: 12),
        decoration: const BoxDecoration(color: Color(0xFFFEF1DD), borderRadius: BorderRadius.all(Radius.circular(4))),
        child: Row(
          children: [
            SvgPicture.asset('assets/images/warning.svg', height: 20, width: 20, colorFilter: const ColorFilter.mode(Color(0xFF9D610D), BlendMode.srcIn)),
            const SizedBox(width: 8),
            Expanded(
                child: Text(
              AppLocalizations.dictionary().projectArchivedWarning,
              style: const TextStyle(color: Color(0xFF9D610D), fontSize: 14),
            ))
          ],
        ));
  }
}
