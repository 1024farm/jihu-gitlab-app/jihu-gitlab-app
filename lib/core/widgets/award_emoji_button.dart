import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AwardEmojiButton extends StatelessWidget {
  const AwardEmojiButton({required this.inProgress, required this.svgAssetPath, required this.backgroundColor, required this.emojiColor, this.title, this.onTap, super.key});

  final bool inProgress;
  final String svgAssetPath;
  final Color backgroundColor;
  final Color emojiColor;
  final String? title;
  final GestureTapCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Opacity(
          opacity: inProgress ? 0.5 : 1,
          child: Container(
              decoration: BoxDecoration(color: backgroundColor, borderRadius: const BorderRadius.all(Radius.circular(4))),
              padding: const EdgeInsets.all(8),
              child: Row(
                children: [
                  if (inProgress)
                    const Padding(
                      padding: EdgeInsets.only(right: 4),
                      child: CupertinoActivityIndicator(radius: 5),
                    ),
                  SvgPicture.asset(
                    svgAssetPath,
                    colorFilter: ColorFilter.mode(emojiColor, BlendMode.srcIn),
                    width: 16,
                    height: 16,
                  ),
                  if (title != null)
                    Container(
                      padding: const EdgeInsets.only(left: 6),
                      child: Text(title!, style: TextStyle(color: emojiColor, fontSize: 12)),
                    )
                ],
              ))),
    );
  }
}
