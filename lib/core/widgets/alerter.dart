import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class Alerter {
  static void showEmailLaunchFailNotice(BuildContext context, String email) {
    showCupertinoAlert(context,
        title: AppLocalizations.dictionary().notice,
        content: AppLocalizations.dictionary().noEmailError,
        cancelTitle: AppLocalizations.dictionary().cancel,
        onCancelPressed: () => Navigator.pop(context),
        okTitle: AppLocalizations.dictionary().copyAddress,
        onOkPressed: () {
          Clipboard.setData(ClipboardData(text: email));
          Navigator.pop(context);
        });
  }

  static void showCupertinoAlert(BuildContext context, {String? title, String? content, String? okTitle, String? cancelTitle, VoidCallback? onOkPressed, VoidCallback? onCancelPressed}) {
    final colorScheme = Theme.of(context).colorScheme;
    okTitle ??= AppLocalizations.dictionary().yes;
    cancelTitle ??= AppLocalizations.dictionary().cancel;
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return CupertinoAlertDialog(title: title != null ? Text(title) : null, content: content != null ? Text(content) : null, actions: <Widget>[
          CupertinoDialogAction(onPressed: onCancelPressed, child: Text(cancelTitle!, style: const TextStyle(color: Colors.black54, fontSize: 14))),
          CupertinoDialogAction(onPressed: onOkPressed, child: Text(okTitle!, style: TextStyle(color: colorScheme.primary, fontSize: 14)))
        ]);
      },
    );
  }
}
