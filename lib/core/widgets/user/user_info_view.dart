import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/domain/email.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/package_device_info.dart';
import 'package:jihu_gitlab_app/core/recommend_project.dart';
import 'package:jihu_gitlab_app/core/recommend_projects.dart';
import 'package:jihu_gitlab_app/core/themes/project_style.dart';
import 'package:jihu_gitlab_app/core/widgets/account/account_page.dart';
import 'package:jihu_gitlab_app/core/widgets/alerter.dart';
import 'package:jihu_gitlab_app/core/widgets/paste_type_url_navigation.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/home/language_settings_page.dart';
import 'package:jihu_gitlab_app/modules/home/post_sales_service_page.dart';
import 'package:jihu_gitlab_app/modules/privacy_policy/privacy_policy_page.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/project/project_page.dart';
import 'package:jihu_gitlab_app/modules/resources/resources_page.dart';
import 'package:provider/provider.dart';

class UserInfoView extends StatefulWidget {
  const UserInfoView({super.key});

  @override
  State<UserInfoView> createState() => _UserInfoViewState();
}

class _UserInfoViewState extends State<UserInfoView> {
  static const feedbackEmail = "contact-project+ultimate-plan-jihu-gitlab-app-jihu-gitlab-app-59893-issue-@mg.jihulab.com";

  @override
  void initState() {
    super.initState();
    if (ConnectionProvider.authorized) _onRefresh();
  }

  void _onRefresh() async {
    var result = await ConnectionProvider().refresh();
    if (result["success"]) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Consumer<ConnectionProvider>(builder: (context, _, child) {
      bool isZh = Localizations.localeOf(context) == const Locale("zh");
      return SafeArea(
        child: Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
          Expanded(
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                if (isZh)
                  _buildListTile(context, () => Navigator.of(context).pushNamed(ResourcesPage.routeName, arguments: {}),
                      SvgPicture.asset("assets/images/resources_icon.svg", semanticsLabel: 'Resources', width: 18, height: 18), AppLocalizations.dictionary().resources),
                _buildListTile(context, () => Navigator.of(context).pushNamed(PostSalesServicePage.routeName),
                    SvgPicture.asset("assets/images/support.svg", semanticsLabel: 'Support', width: 18, height: 18), AppLocalizations.dictionary().support),
                _buildListTile(context, () => Navigator.pushNamed(context, PrivacyPolicyPage.routeName, arguments: {}),
                    SvgPicture.asset("assets/images/privacy_policy.svg", semanticsLabel: 'Privacy Policy', width: 18, height: 18), AppLocalizations.dictionary().privacyTitle),
                if (ConnectionProvider.authorized) ..._buildDivider(),
                _buildPasteAndGoButtonTile(
                    context, SvgPicture.asset("assets/images/paste_and_go.svg", semanticsLabel: 'paste_and_go', width: 18, height: 18), AppLocalizations.dictionary().pasteAndGo),
                ..._buildDivider(),
                _buildListTile(
                  context,
                  () async => await _openEmail(),
                  const Icon(Icons.email_outlined, size: 20),
                  AppLocalizations.dictionary().feedbackViaEmail,
                ),
                _buildListTile(
                  context,
                  () async {
                    RecommendProjects.changeToJiHuProject();
                    var params = RecommendProject.jihu.toParams();
                    params['feedback'] = true;
                    Navigator.of(context).pushNamed(ProjectPage.routeName, arguments: params);
                  },
                  SvgPicture.asset("assets/images/feedback.svg", semanticsLabel: 'Feedback via issue', width: 18, height: 18),
                  AppLocalizations.dictionary().feedbackViaIssue,
                ),
                ..._buildDivider(),
                _buildListTile(context, () => Navigator.pushNamed(context, AccountPage.routeName).then((value) => setState(() {})),
                    SvgPicture.asset("assets/images/accounts.svg", semanticsLabel: 'accounts', width: 18, height: 18), AppLocalizations.dictionary().accounts),
                _buildLanguageChangeTile(),
              ],
            ),
          ),
          ListTile(
            dense: true,
            contentPadding: const EdgeInsets.only(left: 16, right: 16),
            horizontalTitleGap: -16,
            title: Text("${AppLocalizations.dictionary().version} ${PackageDeviceInfo.versionNumber}(${PackageDeviceInfo.buildNumber})", style: textTheme.bodySmall),
            subtitle: Text(
              "ICP备案号：鄂ICP备2021008419号-7A",
              style: textTheme.bodySmall,
            ),
          ),
        ]),
      );
    });
  }

  List<Widget> _buildDivider() {
    return [
      const SizedBox(height: 10),
      const Divider(thickness: 1, height: 1, indent: 16, endIndent: 16, color: Color(0xFFEAEAEA)),
      const SizedBox(height: 10),
    ];
  }

  Widget _buildLanguageChangeTile() {
    return ListTile(
      visualDensity: const VisualDensity(horizontal: -2, vertical: -2),
      enableFeedback: false,
      onTap: () => Navigator.pushNamed(context, LanguageSettingsPage.routeName).then((value) => setState(() {})),
      leading: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[SvgPicture.asset("assets/images/language.svg", semanticsLabel: 'language')],
      ),
      title: Text(AppLocalizations.dictionary().language, style: ProjectStyle.build(Theme.of(context).textTheme.bodySmall, const Color(0xFF03162F))),
      trailing: Container(
        constraints: const BoxConstraints(maxWidth: 150),
        child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          Text(getCurrentLanguage(), style: ProjectStyle.build(Theme.of(context).textTheme.bodySmall, const Color(0xFF87878C)), overflow: TextOverflow.ellipsis),
          const Icon(Icons.keyboard_arrow_right, size: 20)
        ]),
      ),
      contentPadding: const EdgeInsets.only(left: 16, right: 16),
      horizontalTitleGap: -8,
    );
  }

  String getCurrentLanguage() {
    if (LocalStorage.get("language", "auto") == "auto") {
      return AppLocalizations.dictionary().automatic;
    }
    if (LocalStorage.get("language", "auto") == "zh") {
      return "简体中文";
    }
    return "English";
  }

  Widget _buildPasteAndGoButtonTile(BuildContext context, Widget leading, String title) {
    return PasteTypeUrlNavigation(
      child: ListTile(
        visualDensity: const VisualDensity(horizontal: -2, vertical: -2),
        enableFeedback: false,
        leading: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[leading],
        ),
        title: Text(title, style: ProjectStyle.build(Theme.of(context).textTheme.bodySmall, const Color(0xFF03162F))),
        trailing: const Icon(Icons.keyboard_arrow_right, size: 20),
        contentPadding: const EdgeInsets.only(left: 16, right: 16),
        horizontalTitleGap: -8,
      ),
    );
  }

  ListTile _buildListTile(BuildContext context, GestureTapCallback onTap, Widget leading, String title) {
    return ListTile(
      visualDensity: const VisualDensity(horizontal: -2, vertical: -2),
      enableFeedback: false,
      onTap: onTap,
      leading: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[leading],
      ),
      title: Text(title, style: ProjectStyle.build(Theme.of(context).textTheme.bodySmall, const Color(0xFF03162F))),
      trailing: const Icon(Icons.keyboard_arrow_right, size: 20),
      contentPadding: const EdgeInsets.only(left: 16, right: 16),
      horizontalTitleGap: -8,
    );
  }

  Future<void> _openEmail() async {
    Email(feedbackEmail).send().then((value) {
      if (!value) Future.delayed(Duration.zero, () => Alerter.showEmailLaunchFailNotice(context, feedbackEmail));
    });
  }
}
