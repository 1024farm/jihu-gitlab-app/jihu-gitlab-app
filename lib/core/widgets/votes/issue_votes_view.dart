import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/award_emoji_button.dart';
import 'package:jihu_gitlab_app/core/widgets/toast.dart';
import 'package:jihu_gitlab_app/core/widgets/votes/issue_votes_model.dart';
import 'package:jihu_gitlab_app/core/widgets/votes/votes_info.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class IssueVotesView extends StatefulWidget {
  const IssueVotesView({required this.issueIid, required this.projectIid, required this.fullPath, this.connection, super.key});

  final int issueIid;
  final int projectIid;
  final String fullPath;
  final Connection? connection;

  @override
  State<IssueVotesView> createState() => _IssueVotesViewState();
}

class _IssueVotesViewState extends State<IssueVotesView> {
  late IssueVotesModel _model;

  @override
  void initState() {
    _model = IssueVotesModel(widget.issueIid, widget.projectIid, widget.fullPath, connection: widget.connection);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var votesInfo = _model.votesInfo;
    return Consumer<ConnectionProvider>(
      builder: (context, _, child) {
        return Consumer<ProjectProvider>(builder: (context, _, child) {
          _model.connection = widget.connection ?? (ProjectProvider().specifiedHost == null ? ConnectionProvider.connection : ConnectionProvider().connectionOfSpecifiedHost);
          _model.getVotes();
          return ValueListenableBuilder<VotesInfo>(
              valueListenable: _model.notifier,
              builder: (BuildContext context, VotesInfo data, Widget? child) {
                return Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    AwardEmojiButton(
                      inProgress: votesInfo.upVoting,
                      svgAssetPath: 'assets/images/thumb-up.svg',
                      emojiColor: votesInfo.didUpVote ? Theme.of(context).primaryColor : const Color(0xFF87878C),
                      backgroundColor: votesInfo.didUpVote ? const Color(0xFFFFEAE0) : const Color(0xFFEAEAEA),
                      title: votesInfo.upVotes > 0 ? '${votesInfo.upVotes}' : null,
                      onTap: () async {
                        if (ProjectProvider().archived) {
                          Toast.error(context, AppLocalizations.dictionary().projectArchivedWarning);
                          return;
                        }
                        if (_model.connection == null || !(_model.connection?.authorized ?? false)) {
                          Toast.error(context, AppLocalizations.dictionary().loginFirst);
                          return;
                        }
                        setState(() {
                          _model.votesInfo.upVoting = true;
                        });
                        await _model.didTapThumbsUp();
                        setState(() {});
                      },
                    ),
                    const SizedBox(width: 8),
                    AwardEmojiButton(
                      inProgress: votesInfo.downVoting,
                      svgAssetPath: 'assets/images/thumb-down.svg',
                      emojiColor: votesInfo.didDownVote ? Theme.of(context).primaryColor : const Color(0xFF87878C),
                      backgroundColor: votesInfo.didDownVote ? const Color(0xFFFFEAE0) : const Color(0xFFEAEAEA),
                      title: votesInfo.downVotes > 0 ? '${votesInfo.downVotes}' : null,
                      onTap: () async {
                        if (ProjectProvider().archived) {
                          Toast.error(context, AppLocalizations.dictionary().projectArchivedWarning);
                          return;
                        }
                        if (_model.connection == null || !(_model.connection?.authorized ?? false)) {
                          Toast.error(context, AppLocalizations.dictionary().loginFirst);
                          return;
                        }
                        setState(() {
                          _model.votesInfo.downVoting = true;
                        });
                        await _model.didTapThumbsDown();
                        setState(() {});
                      },
                    ),
                  ],
                );
              });
        });
      },
    );
  }
}
