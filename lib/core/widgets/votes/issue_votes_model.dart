import 'package:flutter/cupertino.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/votes/votes_info.dart';

class IssueVotesModel {
  static const String thumbsUpName = 'thumbsup';
  static const String thumbsDownName = 'thumbsdown';

  final int _issueIid;
  final int _projectIid;
  final String _fullPath;
  late final VotesInfo _votesInfo;
  late ValueNotifier<VotesInfo> notifier = ValueNotifier(_votesInfo);
  late Connection? connection;
  late final Connection? _preConnection;

  IssueVotesModel(this._issueIid, this._projectIid, this._fullPath, {Connection? connection}) {
    _votesInfo = VotesInfo();
    _preConnection = connection;
    connection = connection ?? (ProjectProvider().specifiedHost == null ? ConnectionProvider.connection : ConnectionProvider().connectionOfSpecifiedHost);
  }

  Future getVotes() async {
    var response = _preConnection != null
        ? await HttpClient.instance().postWithConnection((ProjectProvider().specifiedHost ?? '') + Api.graphql(), getVotesGraphQLRequestBody(_fullPath, _issueIid), connection)
        : await ProjectRequestSender.instance().post(Api.graphql(), getVotesGraphQLRequestBody(_fullPath, _issueIid));
    var errors = response.body()["errors"];
    if (errors != null) throw Exception(errors);
    var info = response.body()?['data']?['project']?['issue'] ?? {};
    _votesInfo.upVotes = info['upvotes'] ?? 0;
    _votesInfo.downVotes = info['downvotes'] ?? 0;
    await getAwardEmojis();
    notifier.value = _votesInfo;
  }

  Future getAwardEmojis() async {
    var response = _preConnection != null
        ? await HttpClient.instance().getWithConnection((ProjectProvider().specifiedHost ?? '') + Api.join('/projects/$_projectIid/issues/$_issueIid/award_emoji?per_page=100&page=1'), connection)
        : await ProjectRequestSender.instance().get(Api.join('/projects/$_projectIid/issues/$_issueIid/award_emoji?per_page=100&page=1'));
    List<dynamic> list = response.body() ?? [];
    List<dynamic> thumbsDownItems = list.where((element) {
      return element['name'] == thumbsDownName && element['user']?['id'] == connection?.userInfo.id;
    }).toList();
    List<dynamic> thumbsUpItems = list.where((element) => element['name'] == thumbsUpName && element['user']?['id'] == connection?.userInfo.id).toList();

    _votesInfo.didUpVote = thumbsUpItems.isNotEmpty;
    _votesInfo.upVoteEmojiId = thumbsUpItems.isNotEmpty ? thumbsUpItems.first['id'] : null;
    _votesInfo.didDownVote = thumbsDownItems.isNotEmpty;
    _votesInfo.downVoteEmojiId = thumbsDownItems.isNotEmpty ? thumbsDownItems.first['id'] : null;
  }

  Future _awardEmoji(String emojiName) async {
    try {
      _preConnection != null
          ? await HttpClient.instance()
              .postWithConnection<dynamic>((ProjectProvider().specifiedHost ?? '') + Api.join("projects/$_projectIid/issues/$_issueIid/award_emoji?name=$emojiName"), {}, connection)
          : await ProjectRequestSender.instance().post<dynamic>(Api.join("projects/$_projectIid/issues/$_issueIid/award_emoji?name=$emojiName"), {});
    } catch (e) {
      LogHelper.err('CommunityPostDetailsModel _awardEmoji error', e);
    }
  }

  Future _deleteAwardEmoji(int awardId) async {
    try {
      _preConnection != null
          ? await HttpClient.instance().delete<dynamic>((ProjectProvider().specifiedHost ?? '') + Api.join("projects/$_projectIid/issues/$_issueIid/award_emoji/$awardId"), connection: connection)
          : await ProjectRequestSender.instance().delete<dynamic>(Api.join("projects/$_projectIid/issues/$_issueIid/award_emoji/$awardId"));
    } catch (e) {
      LogHelper.err('CommunityPostDetailsModel _deleteAwardEmoji error', e);
    }
  }

  Future<void> didTapThumbsUp() async {
    bool didUpVote = !_votesInfo.didUpVote;
    _votesInfo.upVotes = await vote(thumbsUpName, _votesInfo.upVotes, _votesInfo.upVoteEmojiId, isAward: didUpVote);
    _votesInfo.didUpVote = didUpVote;
    _votesInfo.upVoting = false;
  }

  Future<void> didTapThumbsDown() async {
    bool didDownVote = !_votesInfo.didDownVote;
    _votesInfo.downVotes = await vote(thumbsDownName, _votesInfo.downVotes, _votesInfo.downVoteEmojiId, isAward: didDownVote);
    _votesInfo.didDownVote = didDownVote;
    _votesInfo.downVoting = false;
  }

  Future<int> vote(String awardEmojiName, int votes, int? emojiId, {required bool isAward}) async {
    if (isAward) {
      votes += 1;
      await _awardEmoji(awardEmojiName);
    } else if (emojiId != null) {
      votes -= 1;
      await _deleteAwardEmoji(emojiId);
    }
    await getVotes();
    return votes;
  }

  VotesInfo get votesInfo => _votesInfo;
}

Map<String, dynamic> getVotesGraphQLRequestBody(String fullPath, int issueIid) {
  return {
    "query": """
           {
              project(fullPath: "$fullPath") {
                issue(iid: "$issueIid") {
                  downvotes
                  upvotes
                }
              }
           }
       """
  };
}
