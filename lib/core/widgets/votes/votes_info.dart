class VotesInfo {
  int upVotes = 0;
  int downVotes = 0;
  bool didUpVote = false;
  bool didDownVote = false;
  int? upVoteEmojiId;
  int? downVoteEmojiId;
  bool upVoting = false;
  bool downVoting = false;
}
