import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/widgets/common_app_bar.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerPage extends StatefulWidget {
  static const routeName = "VideoPlayerPage";
  final Map arguments;

  const VideoPlayerPage({required this.arguments, Key? key}) : super(key: key);

  @override
  State<VideoPlayerPage> createState() => _VideoPlayerPageState();
}

class _VideoPlayerPageState extends State<VideoPlayerPage> {
  late String _url;
  late VideoPlayerController _videoPlayerController;
  late ChewieController _chewieController;

  @override
  void initState() {
    super.initState();
    _url = widget.arguments['url'];
    _videoPlayerController = VideoPlayerController.network(_url);
    _videoPlayerController.initialize().then((value) => setState(() {
          _chewieController =
              ChewieController(showControls: true, videoPlayerController: _videoPlayerController, autoPlay: true, autoInitialize: true, aspectRatio: _videoPlayerController.value.aspectRatio);
        }));
  }

  @override
  void dispose() {
    _videoPlayerController.dispose();
    _chewieController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CommonAppBar(showLeading: true, backgroundColor: Colors.black, leading: const BackButton(color: Colors.white)),
        backgroundColor: Colors.black,
        body: Center(child: _videoPlayerController.value.isInitialized ? Chewie(controller: _chewieController) : const CircularProgressIndicator()));
  }
}
