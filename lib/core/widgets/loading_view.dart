import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/widgets/tips_view.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class LoadingView extends StatelessWidget {
  const LoadingView({super.key});

  @override
  Widget build(BuildContext context) {
    return TipsView(icon: "assets/images/loading.png", message: AppLocalizations.dictionary().loading);
  }
}
