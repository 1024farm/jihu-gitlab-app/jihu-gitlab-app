import 'package:flutter/cupertino.dart';
import 'package:jihu_gitlab_app/core/settings/settings.dart';
import 'package:jihu_gitlab_app/core/time.dart';

class ChangeableTime {
  static GestureDetector show(Function onTap, Time time, Color color, {bool editable = true, FontWeight weight = FontWeight.w400, double fontSize = 12}) {
    return GestureDetector(
        onTap: editable
            ? () {
                onTap2(onTap)();
                onTap();
              }
            : () {},
        child: Text(time.value, style: TextStyle(fontSize: fontSize, color: color, fontWeight: weight)));
  }

  static Function onTap2(Function onTap) {
    return () => Settings.instance().precisionTime().change();
  }
}
