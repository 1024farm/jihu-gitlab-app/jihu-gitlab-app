import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/domain/assignee/assignee_entity.dart';
import 'package:jihu_gitlab_app/modules/ai/message/message_entity.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/discussion/discussion_entity.dart';
import 'package:jihu_gitlab_app/modules/issues/details/models/note/note_entity.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label_entity.dart';
import 'package:jihu_gitlab_app/modules/issues/manage/models/issue_draft_entity.dart';
import 'package:jihu_gitlab_app/modules/issues/member/member_entity.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DbManager {
  static const int _version = 13;
  static const String _name = "jihu.db";
  static final DbManager _instance = DbManager._internal();
  Database? _db;

  DbManager._internal();

  factory DbManager.instance() => _instance;

  Future<Database> open() async {
    var databasesPath = await getDatabasesPath();
    return await openDatabase(join(databasesPath, _name), version: _version, onCreate: _onCreate, onUpgrade: _onUpgrade);
  }

  Future<Database> getDb() async {
    _db ??= await open();
    return Future.value(_db);
  }

  Future<void>? close() {
    return _db?.close();
  }

  void _onCreate(Database db, int version) {
    debugPrint('version: $version');
    _onUpgrade(db, 1, version);
  }

  // TODO Refactor
  void _onUpgrade(Database db, int oldVersion, int newVersion) {
    debugPrint('onUpgrade:oldVersion -> $oldVersion, newVersion -> $newVersion');
    for (int i = oldVersion + 1; i <= newVersion; i++) {
      switch (i) {
        case 2:
          db.execute(MemberEntity.createTableSql());
          db.execute("DROP TABLE IF EXISTS assignees");
          break;
        case 3:
          db.execute(IssueDraftEntity.createTableSql());
          break;
        case 4:
          db.execute(GroupAndProjectEntity.createTableSql);
          break;
        case 5:
          db.execute(GroupAndProjectEntity.addStarredColumn);
          break;
        case 6:
          db.execute(GroupAndProjectEntity.addLastActivityAtColumn);
          db.execute(GroupAndProjectEntity.addStarredAtColumn);
          break;
        case 7:
          db.execute(IssueDraftEntity.addTemplateColumn);
          break;
        case 8:
          db.execute(DiscussionEntity.createTableSql());
          db.execute(AssigneeEntity.createTableSql());
          db.execute(NoteEntity.createTableSql());
          break;
        case 9:
          db.execute(IssueDraftEntity.addConfidentialColumn);
          break;
        case 10:
          db.execute(NoteEntity.addInternalColumn);
          break;
        case 11:
          db.execute(MessageEntity.createTableSql);
          break;
        case 12:
          db.execute(LabelEntity.createTableSql);
          break;
        case 13:
          db.execute(LabelEntity.addIsProjectLabelColumn);
          break;
        default:
          break;
      }
    }
  }

  Future<int> insert(String table, Map<String, Object?> values, {String? nullColumnHack, ConflictAlgorithm? conflictAlgorithm}) async {
    var db = await getDb();
    return db.insert(table, values, nullColumnHack: nullColumnHack, conflictAlgorithm: conflictAlgorithm);
  }

  Future<List<int>> batchInsert(String tableName, List<Map<String, dynamic>> values, {String? nullColumnHack, ConflictAlgorithm? conflictAlgorithm}) async {
    var db = await getDb();
    return db.transaction((txn) async {
      List<int> result = [];
      for (var element in values) {
        int id = await txn.insert(tableName, element, nullColumnHack: nullColumnHack, conflictAlgorithm: conflictAlgorithm);
        result.add(id);
      }
      return result;
    });
  }

  Future<int> batchUpdate(String tableName, List<Map<String, dynamic>> values, {String? nullColumnHack, ConflictAlgorithm? conflictAlgorithm}) async {
    var db = await getDb();
    return await db.transaction<int>((txn) async {
      int result = 0;
      for (var value in values) {
        int count = await txn.update(tableName, value, where: " id = ? ", whereArgs: [value['id']]);
        result += count;
      }
      return result;
    });
  }

  Future<Map<String, dynamic>?> findOne(String tableName, {bool? distinct, List<String>? columns, String? where, List<Object?>? whereArgs}) async {
    var db = await getDb();
    var list = await db.query(tableName, distinct: distinct, columns: columns, where: where, whereArgs: whereArgs, limit: 1);
    return list.isNotEmpty ? list[0] : null;
  }

  Future<List<Map<String, dynamic>>> find(String tableName,
      {bool? distinct, List<String>? columns, String? where, List<Object?>? whereArgs, String? groupBy, String? having, String? orderBy, int? limit, int? offset}) async {
    var db = await getDb();
    return await db.query(tableName, distinct: distinct, columns: columns, where: where, whereArgs: whereArgs, groupBy: groupBy, having: having, orderBy: orderBy, limit: limit, offset: offset);
  }

  Future<List<Map<String, dynamic>>> rawFind(String sql) async {
    var db = await getDb();
    return await db.rawQuery(sql);
  }

  Future<int> delete(String tableName, {String? where, List<Object?>? whereArgs}) async {
    return (await getDb()).delete(tableName, where: where, whereArgs: whereArgs);
  }

  Future<int> rawDelete(String sql) async {
    var db = await getDb();
    return await db.rawDelete(sql);
  }

  Future<int> update(String tableName, Map<String, Object?> values, {String? where, List<Object?>? whereArgs, ConflictAlgorithm? conflictAlgorithm}) async {
    return (await getDb()).update(tableName, values, where: where, whereArgs: whereArgs, conflictAlgorithm: conflictAlgorithm);
  }

  Future<int> rawUpdate(String sql) async {
    return (await getDb()).rawUpdate(sql);
  }

  Future<int> count(String tableName, {String? where, List<Object?>? whereArgs}) async {
    List<Map<String, Object?>> query = await (await getDb()).query(tableName, columns: ['id'], where: where, whereArgs: whereArgs);
    return Future(() => query.length);
  }

  @visibleForTesting
  void injectDatabaseForTesting(Database mockDatabase) {
    _db = mockDatabase;
  }
}
