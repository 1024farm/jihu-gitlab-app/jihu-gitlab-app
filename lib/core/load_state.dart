enum LoadState {
  successState,
  loadingState,
  noNetworkState,
  noItemState,
  notLoginState,
  errorState,
  authorizedDeniedState;
}
