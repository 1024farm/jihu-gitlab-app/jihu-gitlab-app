import 'package:jihu_gitlab_app/core/domain/project.dart';
import 'package:jihu_gitlab_app/modules/projects/group_and_project.dart';
import 'package:jihu_gitlab_app/modules/projects/group_details/subgroups/subgroup_list_model.dart';

class RecommendProject {
  final int _projectId;
  final String _name;
  final String _relativePath;
  final int _groupId;
  final String _destinationHost;

  const RecommendProject._(this._projectId, this._name, this._relativePath, this._groupId, this._destinationHost);

  Map<String, Object> toParams() => {'projectId': _projectId, 'name': _name, 'relativePath': _relativePath, "groupId": _groupId, "destinationHost": _destinationHost};

  GroupAndProject toGroupAndProject() {
    var groupAndProject = GroupAndProject(_projectId, _projectId, _name, SubgroupItemType.project, _relativePath, _groupId, starred: false, recommend: true);
    groupAndProject.destinationHost = _destinationHost;
    return groupAndProject;
  }

  Project toProject() {
    return Project(_projectId, _name, _relativePath, '', '');
  }

  String get destinationHost => _destinationHost;

  static const RecommendProject jihu = RecommendProject._(59893, "极狐 GitLab App", "ultimate-plan/jihu-gitlab-app/jihu-gitlab-app", 88966, "https://jihulab.com");
  static const RecommendProject gitlab = RecommendProject._(13953, "极狐 GitLab", "gitlab-cn/gitlab", 845, "https://jihulab.com");
  static const RecommendProject gitlabMain = RecommendProject._(278964, "GitLab", "gitlab-org/gitlab", 9970, "https://gitlab.com");
}
