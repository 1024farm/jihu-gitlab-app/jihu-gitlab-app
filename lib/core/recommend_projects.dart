import 'package:flutter/cupertino.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/core/recommend_project.dart';

class RecommendProjects {
  List<RecommendProject> get(Locale locale) {
    return locale == const Locale("zh") ? [RecommendProject.jihu, RecommendProject.gitlab] : [RecommendProject.gitlabMain];
  }

  static void changeToJiHuProject() {
    ProjectProvider().changeProject(RecommendProject.jihu.toProject(), RecommendProject.jihu.destinationHost);
  }
}
