const String pageInfoFragment = """
  fragment PageInfo on PageInfo {
      startCursor
      hasPreviousPage
      hasNextPage
      endCursor
  }
""";
