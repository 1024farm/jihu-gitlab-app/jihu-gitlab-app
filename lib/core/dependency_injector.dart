import 'package:get_it/get_it.dart';
import 'package:jihu_gitlab_app/core/widgets/photo_picker.dart';
import 'package:jihu_gitlab_app/modules/issues/index.dart';
import 'package:jihu_gitlab_app/modules/projects/projects.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerFactory<SubgroupListModel>(() => SubgroupListModel());
  locator.registerFactory<ProjectsGroupsModel>(() => ProjectsGroupsModel());
  locator.registerFactory<ProjectsStarredModel>(() => ProjectsStarredModel());
  locator.registerFactory<IssueDetailsModel>(() => IssueDetailsModel());
  locator.registerFactory<PhotoPicker>(() => PhotoPicker());
}

void unregister() {
  locator.unregister<SubgroupListModel>();
  locator.unregister<ProjectsGroupsModel>();
  locator.unregister<ProjectsStarredModel>();
  locator.unregister<IssueDetailsModel>();
  locator.unregister<PhotoPicker>();
}
