import 'package:date_format/date_format.dart';

class DateTimeFormatter {
  List<String> pattern;

  DateTimeFormatter._internal(this.pattern);

  factory DateTimeFormatter.withPattern(List<String> pattern) {
    return DateTimeFormatter._internal(pattern);
  }

  String format(DateTime time) {
    return formatDate(time, pattern);
  }
}
