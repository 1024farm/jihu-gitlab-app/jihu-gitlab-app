import 'package:flutter/cupertino.dart';
import 'package:jihu_gitlab_app/core/domain/global_time.dart';
import 'package:jihu_gitlab_app/core/settings/settings.dart';
import 'package:jihu_gitlab_app/core/utils/date_time_formatter.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class Time {
  String _time;

  Time._(this._time);

  factory Time.init(String time) {
    return Time._(time);
  }

  String get value {
    if (_time != '') return ' ${Settings.instance().precisionTime().isEnabled() ? _preciseFormat() : _recentFormat()}';
    return ' ';
  }

  String _recentFormat() {
    try {
      var local = DateTime.parse(_time).toLocal();
      var difference = GlobalTime.now().difference(local);
      return calculateDisplayFormat(difference.inSeconds, difference.inMinutes, difference.inHours, difference.inDays);
    } catch (e) {
      debugPrint("time error: $e");
      return '';
    }
  }

  String calculateDisplayFormat(int seconds, int minutes, int hours, int days) {
    if (seconds < 1) {
      return AppLocalizations.dictionary().justNow;
    }
    if (seconds <= 60) {
      return "$seconds${AppLocalizations.dictionary().seconds}";
    }
    if (minutes <= 60) {
      return "$minutes${AppLocalizations.dictionary().minutes}";
    }
    if (hours <= 24) {
      return "$hours${AppLocalizations.dictionary().hours}";
    }
    if (days <= 30) {
      return "$days${AppLocalizations.dictionary().days}";
    }
    int month = days ~/ 30;
    return "$month${AppLocalizations.dictionary().month}";
  }

  String _preciseFormat() {
    try {
      var time = DateTime.parse(_time).toLocal();
      return "${_months[time.month]!()}${time.day}${AppLocalizations.dictionary().day}, ${time.year} ${time.hour.toString().padLeft(2, "0")}:${time.minute.toString().padLeft(2, "0")}";
    } catch (e) {
      debugPrint("time error: $e");
      return '';
    }
  }

  String? formatAsDate() {
    var dateTime = DateTime.tryParse(_time);
    if (dateTime == null) {
      return null;
    }
    var localDateTime = dateTime.toLocal();
    return "${_months[localDateTime.month]!()}${localDateTime.day}${AppLocalizations.dictionary().day}, ${localDateTime.year}";
  }

  String? format(List<String> pattern) {
    var dateTime = DateTime.tryParse(_time);
    if (dateTime == null) {
      return null;
    }
    var localDateTime = dateTime.toLocal();
    return DateTimeFormatter.withPattern(pattern).format(localDateTime);
  }

  static final _months = {
    1: () => AppLocalizations.dictionary().jan,
    2: () => AppLocalizations.dictionary().feb,
    3: () => AppLocalizations.dictionary().mar,
    4: () => AppLocalizations.dictionary().apr,
    5: () => AppLocalizations.dictionary().may,
    6: () => AppLocalizations.dictionary().jun,
    7: () => AppLocalizations.dictionary().jul,
    8: () => AppLocalizations.dictionary().aug,
    9: () => AppLocalizations.dictionary().sep,
    10: () => AppLocalizations.dictionary().oct,
    11: () => AppLocalizations.dictionary().nov,
    12: () => AppLocalizations.dictionary().dec
  };

  @visibleForTesting
  String get time => _time;
}
