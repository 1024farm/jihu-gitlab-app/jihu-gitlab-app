import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';
import 'package:jihu_gitlab_app/core/synchronizer.dart';

class ProjectSynchronizer<T> extends Synchronizer<T> {
  ProjectSynchronizer({required super.url, required super.dataProcessor, super.connection, super.pageSize});

  @override
  Future<Response<dynamic>> call() async =>
      connection == null ? await ProjectRequestSender.instance().get<T>(contactPagingArguments()) : await HttpClient.instance().getWithConnection<T>(contactPagingArguments(), connection!);
}
