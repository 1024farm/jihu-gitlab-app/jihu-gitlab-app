import 'package:flutter/cupertino.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/domain/project.dart';

class ProjectProvider extends ChangeNotifier {
  static ProjectProvider _instance = ProjectProvider._internal();
  Project? _project;
  bool archived = false;
  String? _specifiedHost;

  ProjectProvider._internal();

  factory ProjectProvider() => _instance;

  void clear() {
    changeProject(null, null);
  }

  Future<void> changeProject(Project? project, String? specifiedHost) async {
    _project = project;
    _specifiedHost = specifiedHost;
    if (project == null) {
      archived = false;
    } else {
      var archived = await project.archived();
      this.archived = archived;
    }
  }

  bool get isSpecifiedHostHasConnection => specifiedHost == null || ConnectionProvider().isConnectedToSpecifiedHost;

  Project? get project => _project;

  String? get specifiedHost => _specifiedHost;

  @visibleForTesting
  void fullReset() {
    _instance = ProjectProvider._internal();
  }
}
