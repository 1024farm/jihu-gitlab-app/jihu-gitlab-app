import 'package:jihu_gitlab_app/core/local_storage.dart';

class Setting {
  final String _key;

  Setting._(this._key);

  factory Setting.find(String key) {
    return Setting._(key);
  }

  void enable() {
    LocalStorage.save(_key, 'enabled');
  }

  void disable() {
    LocalStorage.save(_key, 'disabled');
  }

  void change() {
    if (isEnabled()) {
      disable();
    } else {
      enable();
    }
  }

  String get _value => LocalStorage.get(_key, '');

  bool isEnabled() {
    return _value == 'enabled';
  }
}
