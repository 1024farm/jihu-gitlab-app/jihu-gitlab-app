import 'package:jihu_gitlab_app/core/settings/setting.dart';

class Settings {
  static final Settings _instance = Settings._();

  static Settings instance() {
    return Settings._instance;
  }

  Settings._();

  Setting precisionTime() {
    return Setting.find('precisionTime');
  }
}
