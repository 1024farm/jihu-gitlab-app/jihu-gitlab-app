class GraphQLPageInfo {
  String _endCursor;
  bool _hasNextPage;

  GraphQLPageInfo._(this._endCursor, this._hasNextPage);

  factory GraphQLPageInfo.fromJson(Map<String, dynamic> json) {
    return GraphQLPageInfo._(json['endCursor'] ?? '', json['hasNextPage'] ?? false);
  }

  factory GraphQLPageInfo.init() {
    return GraphQLPageInfo._('', true);
  }

  void reset() {
    _endCursor = '';
    _hasNextPage = true;
  }

  bool get hasNextPage => _hasNextPage;

  String get endCursor => _endCursor;
}
