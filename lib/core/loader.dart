import 'package:flutter/material.dart';

class Loader {
  static void showProgress(BuildContext context, {bool barrierDismissible = false, String text = "Loading ...", int width = 160}) {
    var screenWidth = MediaQuery.of(context).size.width;
    AlertDialog alert = AlertDialog(
      insetPadding: EdgeInsets.symmetric(horizontal: (screenWidth - width) / 2),
      content: SizedBox(
        height: 84,
        child: Column(
          children: [
            Image.asset("assets/images/loading.png", height: 40, width: 40),
            Container(margin: const EdgeInsets.only(top: 12), child: Text(text)),
          ],
        ),
      ),
    );

    showDialog(
      barrierDismissible: barrierDismissible,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  static void hideProgress(BuildContext context) {
    Navigator.pop(context);
  }

  static void showFutureProgress(BuildContext context, {bool barrierDismissible = false, String text = "Loading ...", int width = 160}) {
    Future.delayed(Duration.zero, () {
      showProgress(context, barrierDismissible: barrierDismissible, text: text, width: width);
    });
  }

  static void hideFutureProgress(BuildContext context) {
    Future.delayed(Duration.zero, () {
      Navigator.pop(context);
    });
  }
}
