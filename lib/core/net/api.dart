import 'package:path/path.dart';

class Api {
  static const _version = "v4";
  static const _prefix = "/api/$_version";
  static const _graphql = "/api/graphql";
  static const communityHost = "https://jihulab.com";

  static String join(String path) {
    return normalize('$_prefix/$path');
  }

  static String graphql() => _graphql;
}
