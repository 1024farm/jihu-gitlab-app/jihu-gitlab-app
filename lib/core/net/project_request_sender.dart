import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart' as r;
import 'package:jihu_gitlab_app/core/project_provider.dart';

class ProjectRequestSender {
  static final ProjectRequestSender _instance = ProjectRequestSender._internal();

  ProjectRequestSender._internal();

  factory ProjectRequestSender.instance() => _instance;

  Future<r.Response<T>> get<T>(String path) async {
    if (ProjectProvider().specifiedHost == null) return HttpClient.instance().get<T>(path);
    if (ProjectProvider().isSpecifiedHostHasConnection) return HttpClient.instance().get<T>(path, connection: ConnectionProvider().connectionOfSpecifiedHost);
    return HttpClient.instance().get<T>(ProjectProvider().specifiedHost! + path, useActiveConnection: false);
  }

  Future<r.Response<T>> post<T>(String path, data) async {
    if (ProjectProvider().specifiedHost == null) return HttpClient.instance().post<T>(path, data);
    if (ProjectProvider().isSpecifiedHostHasConnection) return HttpClient.instance().post<T>(path, data, connection: ConnectionProvider().connectionOfSpecifiedHost);
    return HttpClient.instance().post<T>(ProjectProvider().specifiedHost! + path, data, useActiveConnection: false);
  }

  Future<r.Response<T>> put<T>(String path, data) async {
    if (ProjectProvider().specifiedHost == null) return HttpClient.instance().put<T>(path, data);
    if (ProjectProvider().isSpecifiedHostHasConnection) return HttpClient.instance().put<T>(path, data, connection: ConnectionProvider().connectionOfSpecifiedHost);
    return HttpClient.instance().put<T>(ProjectProvider().specifiedHost! + path, data, useActiveConnection: false);
  }

  Future<r.Response<T>> delete<T>(String path) async {
    if (ProjectProvider().specifiedHost == null) return HttpClient.instance().delete<T>(path);
    if (ProjectProvider().isSpecifiedHostHasConnection) return HttpClient.instance().delete<T>(path, connection: ConnectionProvider().connectionOfSpecifiedHost);
    return HttpClient.instance().delete<T>(ProjectProvider().specifiedHost! + path, useActiveConnection: false);
  }
}
