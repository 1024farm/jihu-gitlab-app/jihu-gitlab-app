import 'package:dio/dio.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';

class RefreshTokenInterceptor extends Interceptor {
  final Dio _dio;

  Connection? connection;

  RefreshTokenInterceptor(this._dio, this.connection);

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) async {
    if (connection == null) {
      LogHelper.info("onRequest path ${options.method}: ${options.path}");
      handler.next(options);
      return;
    }
    options.baseUrl = connection!.baseUrl.get;
    LogHelper.info("onRequest path ${options.method}: ${options.baseUrl}, ${options.path}, "
        "${connection!.serverType == 'jihulab.com' || connection!.serverType == 'gitlab.com' ? 'Saas' : 'Self-Managed'}, ${connection!.serverType}");
    _addAuthorizationHeader(options);
    if (ConnectionProvider.onAuthTokenRequest) {
      handler.next(options);
      return;
    }
    if (connection!.shouldRefreshToken && connection!.canRefreshToken && !options.path.contains("/oauth/token")) {
      LogHelper.info("onRequest refreshToken path ${options.method}: ${options.baseUrl}, ${options.path}");
      if (await connection!.refreshToken()) {
        _addAuthorizationHeader(options);
        LogHelper.info("token 即将过期，并自动刷新成功");
      } else if (!ConnectionProvider.onAuthTokenRequest) {
        LogHelper.debug("onRequest refreshToken failed path ${options.method}: ${options.baseUrl}, ${options.path}");
        ConnectionProvider().clearActive();
        ConnectionProvider().loggedOut(byUser: false);
      }
    }
    handler.next(options);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) async {
    var response = err.response;
    LogHelper.err("onError path ${response?.requestOptions.method}: ${response?.requestOptions.baseUrl}, ${response?.requestOptions.path}", err);
    if (ConnectionProvider.onAuthTokenRequest) {
      handler.next(err);
      return;
    }
    if (connection != null && err.type == DioErrorType.response && response != null && response.statusCode == 401) {
      LogHelper.info("onError refreshToken path ${response.requestOptions.method}: ${response.requestOptions.baseUrl}, ${response.requestOptions.path}");
      if (connection!.canRefreshToken && await connection!.refreshToken()) {
        LogHelper.info("token 已过期，并自动刷新成功");
        handler.resolve(await _retry(response.requestOptions));
      } else if (!ConnectionProvider.onAuthTokenRequest) {
        LogHelper.debug("onError refreshToken failed path ${response.requestOptions.method}: ${response.requestOptions.baseUrl}, ${response.requestOptions.path}");
        ConnectionProvider().clearActive();
        ConnectionProvider().loggedOut(byUser: false);
        handler.next(err);
      }
    } else {
      handler.next(err);
    }
  }

  Future<Response> _retry(RequestOptions options) async {
    return await _dio.request(options.path,
        data: options.data,
        queryParameters: options.queryParameters,
        options: Options(
          method: options.method,
          headers: options.headers,
        ));
  }

  void _addAuthorizationHeader(RequestOptions options) {
    options.headers.addAll(connection!.authHeaders);
  }
}
