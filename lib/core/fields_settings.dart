import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/modules/issues/list/issues_view.dart';

class FieldsSettings {
  bool issueId;
  bool weight;
  bool epic;
  bool labels;
  bool iteration;
  bool assigneeAvatar;
  bool milestone;

  FieldsSettings._(this.issueId, this.weight, this.epic, this.labels, this.iteration, this.assigneeAvatar, this.milestone);

  static Future<FieldsSettings> read() async {
    return FieldsSettings._(
      await LocalStorage.get("fieldSettings_issueId", true),
      await LocalStorage.get("fieldSettings_weight", false),
      await LocalStorage.get("fieldSettings_epic", false),
      await LocalStorage.get("fieldSettings_labels", false),
      await LocalStorage.get("fieldSettings_iteration", false),
      await LocalStorage.get("fieldSettings_assigneeAvatar", true),
      await LocalStorage.get("fieldSettings_milestone", false),
    );
  }

  IssuesViewDisplayOptions toDisplayOptions() {
    return IssuesViewDisplayOptions(
        displayIssueId: issueId, displayWeight: weight, displayEpic: epic, displayLabels: labels, displayIteration: iteration, displayAssigneeAvatar: assigneeAvatar, displayMilestone: milestone);
  }

  Future<void> save() async {
    await LocalStorage.save("fieldSettings_issueId", issueId);
    await LocalStorage.save("fieldSettings_weight", weight);
    await LocalStorage.save("fieldSettings_epic", epic);
    await LocalStorage.save("fieldSettings_labels", labels);
    await LocalStorage.save("fieldSettings_iteration", iteration);
    await LocalStorage.save("fieldSettings_assigneeAvatar", assigneeAvatar);
    await LocalStorage.save("fieldSettings_milestone", milestone);
  }
}
