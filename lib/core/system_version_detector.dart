import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:version/version.dart';

enum SystemType {
  latest(VersionRange(name: "iOS", min: "16.6.1"), VersionRange(name: "Android", min: "13")),
  maintainable(VersionRange(name: "iOS", min: "15.7.8", max: "16.6.1"), VersionRange(name: "Android", min: "10", max: "13")),
  unmaintainable(VersionRange(name: "iOS", max: "15.7.8"), VersionRange(name: "Android", max: "10"));

  final VersionRange iosVersionRange;
  final VersionRange androidVersionRange;

  static String version = '';
  static String maxVersion = '';
  static String maintainVersion = '';

  const SystemType(this.iosVersionRange, this.androidVersionRange);

  static SystemType matchIos(String? version) => _match(version, (systemType) => systemType.iosVersionRange);

  static SystemType matchAndroid(String? version) => _match(version, (systemType) => systemType.androidVersionRange);

  static SystemType _match(String? version, VersionRange Function(SystemType systemType) mapper) {
    SystemType.maxVersion = '${mapper(SystemType.latest).name} ${mapper(SystemType.latest).min}';
    SystemType.maintainVersion = '${mapper(SystemType.latest).name} ${mapper(SystemType.maintainable).min}';
    if (version == null || version.isEmpty) {
      SystemType.version = '';
      return SystemType.unmaintainable;
    }
    SystemType.version = '${mapper(SystemType.values.first).name} $version';
    List<SystemType> matched = SystemType.values.where((element) => mapper(element).match(version)).toList();
    return matched.isNotEmpty ? matched.first : SystemType.unmaintainable;
  }

  String description({required bool isForGeek}) {
    String version = SystemType.version == '' ? AppLocalizations.dictionary().defaultVersion : SystemType.version;
    if (this == SystemType.maintainable && isForGeek) {
      return AppLocalizations.dictionary().geekExclusiveHint(version, SystemType.maxVersion);
    }
    return AppLocalizations.dictionary().securityWarningHint(version, SystemType.maintainVersion);
  }
}

class SystemVersionDetector {
  static SystemVersionDetector _instance = SystemVersionDetector._internal();

  SystemVersionDetector._internal();

  factory SystemVersionDetector.instance() => _instance;

  Future<SystemType> detect() async {
    var deviceInfoPlugin = DeviceInfoPlugin();
    if (Platform.isIOS) {
      var iosDeviceInfo = await deviceInfoPlugin.iosInfo;
      return SystemType.matchIos(iosDeviceInfo.systemVersion);
    }
    if (Platform.isAndroid) {
      var androidDeviceInfo = await deviceInfoPlugin.androidInfo;
      return SystemType.matchAndroid(androidDeviceInfo.version.release);
    }
    return Future(() => SystemType.unmaintainable);
  }

  @visibleForTesting
  static void injectInstanceForTesting(SystemVersionDetector instance) {
    _instance = instance;
  }

  @visibleForTesting
  static void reset() {
    _instance = SystemVersionDetector._internal();
  }
}

class VersionRange {
  final String? min;
  final String? max;
  final String name;

  const VersionRange({required this.name, this.min, this.max});

  bool match(String version) {
    if (version.isEmpty) {
      return false;
    }
    var v = Version.parse(version);
    bool leftMatch = min == null || v >= Version.parse(min!);
    bool rightMatch = max == null || v < Version.parse(max!);
    return leftMatch && rightMatch;
  }
}
