import 'package:flutter/cupertino.dart';

extension StringExtension on String {
  String get capitalize {
    if (this == '') return '';
    if (length == 1) return toUpperCase();
    return "${substring(0, 1).toUpperCase()}${substring(1).toLowerCase()}";
  }

  String get disableAutoWrap {
    return Characters(this).join('\u{200B}');
  }

  String addByIndex(String target, int atIndex) {
    var index = atIndex;
    if (index < 0) {
      index = isNotEmpty ? length : 0;
    }
    return substring(0, index) + target + substring(index);
  }

  String catchSubstring({String? from, bool includeFrom = true, String? to, bool includeTo = true}) {
    if (from == null && to == null) {
      throw Exception('from == null && to == null');
    }
    if (from != null && to != null) {
      int start = indexOf(from);
      if (start < 0) return '';
      if (from.contains(to)) {
        String next = replaceFirst(substring(0, start) + from, '');
        int end = next.indexOf(to);
        if (end < 0) return '';
        String content = next.substring(0, end);
        if (includeFrom && includeTo) return from + content + to;
        if (includeFrom) return from + content;
        if (includeTo) return content + to;
        return content;
      }

      int end = indexOf(to);
      if (end < 0 || end < start) return '';
      String content = substring(start, end);
      if (includeFrom && includeTo) return content + to;
      if (includeFrom) return content;
      if (includeTo) return content.replaceFirst(from, '') + to;
      return content.replaceFirst(from, '');
    }

    if (from != null) {
      int start = indexOf(from);
      if (start < 0) return '';
      String content = substring(start);
      if (includeFrom) return content;
      return content.replaceFirst(from, '');
    }

    int end = indexOf(to!);
    if (end < 0) return '';
    String content = substring(0, end);
    if (includeTo) return content + to;
    return content;
  }
}
