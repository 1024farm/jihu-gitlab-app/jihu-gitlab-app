class ListComparator {
  static ListCompareResult<T> compare<T>(List<T> oldValue, List<T> newValue) {
    List<T> shouldBeAdd = [];
    List<Pair<T>> shouldBeUpdate = [];
    for (var value in newValue) {
      var where = oldValue.where((e) => e == value);
      if (where.isEmpty) {
        shouldBeAdd.add(value);
      } else {
        shouldBeUpdate.add(Pair(where.first, value));
      }
    }
    return ListCompareResult(shouldBeAdd, shouldBeUpdate);
  }
}

class ListCompareResult<T> {
  List<T> add;
  List<Pair<T>> update;

  bool get hasAdd => add.isNotEmpty;

  bool get hasUpdate => update.isNotEmpty;

  ListCompareResult(this.add, this.update);
}

class Pair<T> {
  T left;
  T right;

  Pair(this.left, this.right);
}
