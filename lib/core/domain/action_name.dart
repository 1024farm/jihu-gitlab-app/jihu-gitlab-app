import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class ActionName {
  String _actionName;

  ActionName._(this._actionName);

  factory ActionName.init(String actionName) {
    return ActionName._(actionName);
  }

  String actionName() {
    Map<String, String> actions = getActions();
    return actions[_actionName] ?? "";
  }

  Map<String, String> getActions() {
    return {
      "": "",
      "assigned": AppLocalizations.dictionary().assigned,
      "mentioned": AppLocalizations.dictionary().mentioned,
      "build_failed": AppLocalizations.dictionary().buildFailed,
      "marked": AppLocalizations.dictionary().marked,
      "approval_required": AppLocalizations.dictionary().approvalRequired,
      "unmergeable": AppLocalizations.dictionary().unmergeable,
      "directly_addressed": AppLocalizations.dictionary().directlyAddressed,
      "merge_train_removed": AppLocalizations.dictionary().mergeTrainRemoved
    };
  }
}
