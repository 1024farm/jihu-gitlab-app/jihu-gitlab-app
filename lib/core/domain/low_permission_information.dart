import 'package:jihu_gitlab_app/core/connection_provider/connection.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/domain/email.dart';
import 'package:jihu_gitlab_app/core/domain/system_locale.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';

class LowPermissionInformation {
  static String warningNotice(String featureName) {
    if (ConnectionProvider.isSelfManagedGitLab) return AppLocalizations.dictionary().warningNoticeWhenNoLicenseForSelfManagedLogin(featureName);
    return AppLocalizations.dictionary().warningNoticeWhenNoLicenseForSaaSLogin(featureName);
  }

  static String warningNoticeSupportLine() {
    return "${AppLocalizations.dictionary().warningNoticeSuffix}${_recipient()}";
  }

  static Email email(String featureName) {
    if (ConnectionProvider.connection == null) throw Exception("error.not-login");
    return Email(
      _recipient(),
      AppLocalizations.dictionary().permissionLowEmailSubject(featureName),
      AppLocalizations.dictionary().permissionLowEmailContent(featureName),
    );
  }

  static String _recipient() {
    if (ConnectionProvider.connection == null) throw Exception("error.not-login");
    if (ConnectionProvider.connection!.type == ConnectionType.jiHu) return "contact@gitlab.cn";
    if (ConnectionProvider.connection!.type == ConnectionType.gitLab) return "tlindemann@gitlab.com";
    if (SystemLocale().isOnePartOfChina) return "contact@gitlab.cn";
    return "tlindemann@gitlab.com";
  }
}
