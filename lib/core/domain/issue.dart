import 'package:jihu_gitlab_app/core/domain/action_name.dart';
import 'package:jihu_gitlab_app/core/domain/assignee/assignee.dart';
import 'package:jihu_gitlab_app/core/domain/epic.dart';
import 'package:jihu_gitlab_app/core/domain/project.dart';
import 'package:jihu_gitlab_app/core/domain/target.dart';
import 'package:jihu_gitlab_app/core/id.dart';
import 'package:jihu_gitlab_app/modules/issues/label/label.dart';
import 'package:jihu_gitlab_app/modules/iteration/models/cadence_iteration.dart';

class Issue {
  Id id;
  int iid;
  String title;
  String targetUrl;
  String createdAt;
  ActionName actionName;
  String targetType;
  String state;
  Target target;
  Project project;
  Assignee author;
  List<Assignee> assignees;
  List<Label> labels;
  bool confidential;
  String? milestone;
  Epic? epic;
  CadenceIteration? iteration;
  int? weight;

  Issue._(
    this.id,
    this.iid,
    this.title,
    this.targetUrl,
    this.createdAt,
    this.actionName,
    this.target,
    this.project,
    this.author,
    this.targetType,
    this.confidential,
    this.state,
    this.epic,
    this.weight,
    this.assignees,
    this.labels,
    this.iteration,
    this.milestone,
  );

  void setRemainingInformation(Issue remainingInformation) {
    epic = remainingInformation.epic;
    iteration = remainingInformation.iteration;
    weight = remainingInformation.weight;
  }

  factory Issue.fromProjectGraphQLJson(Map<String, dynamic> issueJson, Map<String, dynamic> projectJson) {
    return Issue._(
      Id.fromGid(issueJson['id'] ?? '/0'),
      int.parse(issueJson['iid'] ?? '0'),
      issueJson['title'] ?? '',
      '',
      issueJson['createdAt'] ?? '',
      ActionName.init(""),
      Target.fromJson({'id': Id.fromGid(issueJson['id'] ?? '/0').id, 'iid': int.parse(issueJson['iid'] ?? '0'), 'title': issueJson['title'] ?? '', 'created_at': issueJson['createdAt'] ?? ''}),
      Project.fromGraphQLJson(projectJson),
      Assignee.fromGraphQLJson(issueJson['author'] ?? {}),
      '',
      issueJson['confidential'] ?? false,
      issueJson['state'] ?? '',
      issueJson['epic']?['title'] == null ? null : Epic(issueJson['epic']?['title']),
      issueJson['weight'],
      ((issueJson['assignees']?['nodes'] ?? []) as List).map((e) => Assignee.fromGraphQLJson(e)).toList(),
      ((issueJson['labels']?['nodes'] ?? []) as List).map((e) => Label.fromGraphQLJson(e)).toList(),
      issueJson['iteration'] == null ? null : CadenceIteration.init(issueJson['iteration']),
      issueJson['milestone'] == null ? null : issueJson['milestone']['title'],
    );
  }

  factory Issue.fromGroupGraphQLJson(Map<String, dynamic> issueJson, [Project? project]) {
    return Issue._(
      Id.fromGid(issueJson['id'] ?? '/0'),
      int.parse(issueJson['iid'] ?? '0'),
      issueJson['title'] ?? '',
      '',
      issueJson['createdAt'] ?? '',
      ActionName.init(""),
      Target.fromJson({'id': Id.fromGid(issueJson['id'] ?? '/0').id, 'iid': int.parse(issueJson['iid'] ?? '0'), 'title': issueJson['title'] ?? '', 'created_at': issueJson['createdAt'] ?? ''}),
      project ?? Project.fromJson({}),
      Assignee.fromGraphQLJson(issueJson['author'] ?? {}),
      '',
      issueJson['confidential'] ?? false,
      issueJson['state'] ?? '',
      issueJson['epic']?['title'] == null ? null : Epic(issueJson['epic']?['title']),
      issueJson['weight'],
      ((issueJson['assignees']?['nodes'] ?? []) as List).map((e) => Assignee.fromGraphQLJson(e)).toList(),
      ((issueJson['labels']?['nodes'] ?? []) as List).map((e) => Label.fromGraphQLJson(e)).toList(),
      issueJson['iteration'] == null ? null : CadenceIteration.init(issueJson['iteration']),
      issueJson['milestone'] == null ? null : issueJson['milestone']['title'],
    );
  }

  factory Issue.fromJson(Map<String, dynamic> issueJson) {
    return Issue._(
      Id.from(issueJson['id']),
      issueJson["iid"],
      issueJson['title'] ?? '',
      '',
      issueJson['created_at'] ?? '',
      ActionName.init(""),
      Target.fromJson({}),
      Project.fromJson({"id": issueJson['project_id']}),
      Assignee.fromJson(issueJson['author'] ?? {}),
      '',
      issueJson['confidential'] ?? false,
      issueJson['state'] ?? '',
      issueJson['epic']?['title'] == null ? null : Epic(issueJson['epic']?['title']),
      issueJson['weight'],
      ((issueJson['assignees'] ?? []) as List).map((e) => Assignee.fromJson(e)).toList(),
      [],
      null,
      null,
    );
  }

  Map<String, dynamic> get asDetailParams => {
        'projectId': project.id,
        'issueId': id.id,
        'issueIid': iid,
        'targetId': target.id,
        'targetIid': target.iid,
        'pathWithNamespace': project.pathWithNamespace,
        'issue': this,
      };
}
