import 'package:jihu_gitlab_app/core/domain/email.dart';
import 'package:jihu_gitlab_app/core/uri_launcher.dart';
import 'package:jihu_gitlab_app/core/widgets/alerter.dart';
import 'package:jihu_gitlab_app/core/widgets/linker_text.dart';
import 'package:jihu_gitlab_app/core/widgets/toast.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:url_launcher/url_launcher_string.dart';

class UrlRedirector {
  void redirect(link, context, [Email? email]) async {
    var uri = Uri.parse(link.url);
    if (uri.isScheme('HTTP')) {
      await _redirectUrl(uri, link);
      return;
    }
    if (email == null) {
      await _redirectEmail(uri, context);
      return;
    }
    email.send().then((res) {
      if (!res) Future.delayed(Duration.zero, () => Alerter.showEmailLaunchFailNotice(context, email.recipient));
    });
  }

  Future<void> _redirectUrl(Uri uri, LinkableElement link) async {
    var result = await UriLauncher.instance().launch(uri, mode: LaunchMode.externalApplication);
    if (!result) {
      Toast.show(AppLocalizations.dictionary().openUrlFailed(link.url));
    }
  }

  Future<void> _redirectEmail(Uri uri, context) async {
    var launchResult = await UriLauncher.instance().launch(uri);
    if (!launchResult) {
      Future.delayed(Duration.zero, () => Alerter.showEmailLaunchFailNotice(context, uri.path));
    }
  }
}
