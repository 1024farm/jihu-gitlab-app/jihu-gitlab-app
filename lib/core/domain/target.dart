class Target {
  int id;
  int iid;
  String title;
  String createdAt;

  Target._(this.id, this.iid, this.title, this.createdAt);

  factory Target.fromJson(Map<String, dynamic> json) {
    return Target._(json['id'] ?? 0, json['iid'] ?? 0, json['title'] ?? '', json['created_at'] ?? '');
  }
}
