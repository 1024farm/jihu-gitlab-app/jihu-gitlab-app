import 'package:jihu_gitlab_app/core/id.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';

class Project {
  int id;
  String name;
  String path;
  String pathWithNamespace;
  String nameWithNamespace;

  Project(this.id, this.name, this.path, this.pathWithNamespace, this.nameWithNamespace);

  factory Project.fromJson(Map<String, dynamic> json) {
    return Project(json['id'] ?? 0, json['name'] ?? '', json['path'] ?? '', json['path_with_namespace'] ?? '', json['name_with_namespace'] ?? '');
  }

  factory Project.fromGraphQLJson(Map<String, dynamic> json) {
    return Project(json['id'] != null ? Id.fromGid(json['id'] ?? '/0').id : 0, json['name'] ?? '', json['path'] ?? '', json['fullPath'] ?? '', json['nameWithNamespace'] ?? '');
  }

  static Future<Project> getById(int id) async {
    try {
      var body = (await HttpClient.instance().get(Api.join('/projects/$id'))).body();
      return Project.fromJson(body);
    } catch (e) {
      return Project.fromJson({});
    }
  }

  Future<bool> archived() async {
    try {
      var response = await HttpClient.instance().post(Api.graphql(), getArchivedRequestBody(path));
      return response.body()['data']?['project']?['archived'] ?? false;
    } catch (e) {
      return false;
    }
  }
}

Map<String, String> getArchivedRequestBody(String fullPath) {
  return {
    "query": """
       {
          project(fullPath:"$fullPath"){
            archived
          }
       } 
    """
  };
}
