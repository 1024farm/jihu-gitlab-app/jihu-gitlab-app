import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';

class SystemLocale {
  String get code => Intl.systemLocale.split('_').last;

  bool get isOnePartOfChina {
    // CN -> China Mainland, HK -> HongKong, MO -> Macao
    var chinaLocaleCodes = ['CN', 'HK', 'MO'];
    return chinaLocaleCodes.contains(code);
  }

  @visibleForTesting
  void set(String localeCode) {
    Intl.systemLocale = 'en_$localeCode';
  }
}
