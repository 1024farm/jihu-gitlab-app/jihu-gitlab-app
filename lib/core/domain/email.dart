import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/uri_launcher.dart';

class Email {
  final String _recipient;
  final String? _subject;
  final String? _content;

  Email._(this._recipient, this._subject, this._content);

  factory Email(String recipient, [String? subject, String? content]) {
    return Email._(recipient, subject, content);
  }

  Future<bool> send() async {
    var uri = Uri(scheme: 'mailto', path: _recipient, query: encodeQueryParameters(_emailInformation()));
    LogHelper.info('Email url: $uri');
    return await UriLauncher.instance().canLaunch(uri) && await UriLauncher.instance().launch(uri);
  }

  String get recipient => _recipient;

  Map<String, String> _emailInformation() {
    Map<String, String> information = {};
    if (_subject != null) information['subject'] = _subject!;
    if (_content != null) information['body'] = _content!;
    return information;
  }

  String? encodeQueryParameters(Map<String, String> params) {
    return params.entries.map((MapEntry<String, String> e) => '${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}').join('&');
  }
}
