import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';

class AvatarUrl {
  final String _impl;

  AvatarUrl(this._impl);

  String realUrl(String? host) {
    if (_impl.startsWith("http")) return _impl;
    return (host ?? ConnectionProvider.currentBaseUrl.get) + _impl;
  }
}
