import 'package:jihu_gitlab_app/core/db_manager.dart';
import 'package:jihu_gitlab_app/core/domain/assignee/assignee_entity.dart';
import 'package:sqflite/sqflite.dart';

class AssigneeRepository {
  static final AssigneeRepository _instance = AssigneeRepository._internal();

  AssigneeRepository._internal();

  factory AssigneeRepository.instance() => _instance;

  Future<List<AssigneeEntity>> queryById(int assigneeId) async {
    Database database = await DbManager.instance().getDb();
    List<Map<String, Object?>> query = await database.query(AssigneeEntity.tableName, where: " assignee_id = ? ", whereArgs: [assigneeId]);
    return query.map((e) => AssigneeEntity.restore(e)).toList();
  }

  Future<List<AssigneeEntity>> queryByIds(List<int> assigneeIds) async {
    String sql = "select * from ${AssigneeEntity.tableName} where assignee_id in (${assigneeIds.join(',')})";
    var list = await DbManager.instance().rawFind(sql);
    return list.map((e) => AssigneeEntity.restore(e)).toList();
  }

  Future<List<int>> insert(List<AssigneeEntity> entities) async {
    // TODO: Cannot mock db.transaction, so add try catch for test
    try {
      return await DbManager.instance().batchInsert(AssigneeEntity.tableName, entities.map((e) => e.toMap()).toList());
    } catch (e) {
      return [];
    }
  }

  Future<int> update(List<AssigneeEntity> entities) async {
    return await DbManager.instance().batchUpdate(AssigneeEntity.tableName, entities.map((e) => e.toMap()).toList());
  }

  Future<int> deleteLessThan(List<int> assigneeIds, int version) async {
    String sql = "delete from ${AssigneeEntity.tableName} where assignee_id in (${assigneeIds.join(',')}) and version < $version";
    return await DbManager.instance().rawDelete(sql);
  }
}
