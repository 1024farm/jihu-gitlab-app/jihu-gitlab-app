import 'package:jihu_gitlab_app/core/id.dart';
import 'package:jihu_gitlab_app/modules/issues/member/member.dart';

class Assignee {
  int id;
  String name;
  String username;
  String avatarUrl;

  Assignee(this.id, this.name, this.username, this.avatarUrl);

  factory Assignee.empty() {
    return Assignee.fromJson({});
  }

  factory Assignee.fromJson(Map<String, dynamic> json) {
    return Assignee(json['id'] ?? 0, json['name'] ?? '', json['username'] ?? '', json['avatar_url'] ?? '');
  }

  factory Assignee.fromGraphQLJson(Map<String, dynamic> json) {
    return Assignee(Id.fromGid(json['id'] ?? '/0').id, json['name'] ?? '', json['username'] ?? '', json['avatarUrl'] ?? '');
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is Assignee && runtimeType == other.runtimeType && id == other.id && name == other.name && username == other.username && avatarUrl == other.avatarUrl;

  @override
  int get hashCode => id.hashCode ^ name.hashCode ^ username.hashCode ^ avatarUrl.hashCode;

  Member toMember() {
    var member = Member(id, name, username, avatarUrl);
    member.toggleCheck(check: true);
    return member;
  }
}
