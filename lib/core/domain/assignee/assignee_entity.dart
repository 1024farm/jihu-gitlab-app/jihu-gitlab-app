const maskGroupName = '****';

class AssigneeEntity {
  static const tableName = "assignees";

  int? id;
  int assigneeId;
  String name;
  String username;
  String avatarUrl;
  int version;

  AssigneeEntity._internal(this.id, this.assigneeId, this.name, this.username, this.avatarUrl, this.version);

  static AssigneeEntity restore(Map<String, dynamic> map) {
    return AssigneeEntity._internal(map['id'], map['assignee_id'], map['name'], map['username'], map['avatar_url'], map['version']);
  }

  static AssigneeEntity create(Map<String, dynamic> map, int version) {
    return AssigneeEntity._internal(null, map['id'] ?? 0, map['name'] == maskGroupName ? (map['username'] ?? '') : (map['name'] ?? ''), map['username'] ?? '', map['avatar_url'] ?? '', version);
  }

  Map<String, dynamic> toMap() {
    return {'id': id, 'assignee_id': assigneeId, 'name': name, 'username': username, 'avatar_url': avatarUrl, 'version': version};
  }

  static String createTableSql() {
    return """ 
    create table $tableName(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        assignee_id INTEGER NOT NULL,
        name TEXT ,
        username TEXT ,
        avatar_url TEXT,
        version INTEGER
    );
    """;
  }

  void update(AssigneeEntity newValue) {
    name = newValue.name;
    username = newValue.username;
    version = newValue.version;
    avatarUrl = newValue.avatarUrl;
  }

  @override
  bool operator ==(Object other) => identical(this, other) || other is AssigneeEntity && runtimeType == other.runtimeType && assigneeId == other.assigneeId;

  @override
  int get hashCode => assigneeId.hashCode;
}
