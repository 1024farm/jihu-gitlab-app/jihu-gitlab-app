class SelectorProject {
  int id;
  String name;

  SelectorProject._(this.id, this.name);

  factory SelectorProject.fromJson(Map<String, dynamic> json) {
    return SelectorProject._(buildId(json), json['name'] ?? '');
  }

  static int buildId(Map<String, dynamic> json) {
    String gid = json['id'];
    return int.parse(gid.split("/").last);
  }
}
