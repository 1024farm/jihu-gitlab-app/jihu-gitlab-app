import 'package:flutter/material.dart';

class ProjectStyle {
  static TextStyle build(TextStyle? textStyle, Color textColor) {
    TextStyle text = textStyle ?? const TextStyle(fontWeight: FontWeight.w400, fontSize: 14.0);
    return TextStyle(fontWeight: text.fontWeight, fontSize: text.fontSize, color: textColor);
  }
}
