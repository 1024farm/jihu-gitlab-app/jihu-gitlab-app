import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';

class CustomIcons {
  // coverage:ignore-start
  CustomIcons._();

  // coverage:ignore-end

  static const _kFontFom = 'CustomIcons';

  static const IconData projects = IconData(0xe801, fontFamily: _kFontFom, matchTextDirection: true);

  static const IconData toDos = IconData(0xe802, fontFamily: _kFontFom, matchTextDirection: true);

  static const IconData resources = IconData(0xe803, fontFamily: _kFontFom, matchTextDirection: true);

  static const IconData exit = IconData(0xe800, fontFamily: 'Exit');
}
