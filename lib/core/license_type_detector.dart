import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/project_request_sender.dart';

enum LicenseType { free, premium, ultimate }

class LicenseTypeDetector {
  static final LicenseTypeDetector _instance = LicenseTypeDetector._internal();

  LicenseTypeDetector._internal();

  factory LicenseTypeDetector.instance() => _instance;

  Future<bool> isFree(int groupId) async {
    try {
      await HttpClient.instance().get(Api.join("/groups/$groupId/iterations"));
      return Future.value(false);
    } catch (e) {
      return Future.value(true);
    }
  }

  Future<bool> isFreeProject(String fullPath) async {
    try {
      var response = await ProjectRequestSender.instance().post(Api.graphql(), requestBodyOfLicenseType(fullPath));
      var errors = response.body()['errors'];
      return Future(() => errors != null);
    } catch (e) {
      return Future.value(true);
    }
  }
}

Map<String, dynamic> requestBodyOfLicenseType(String fullPath) {
  return {
    "query": """
          {
            project(fullPath:"$fullPath"){
              group{
                iterations{
                    __typename
                }
              }
            }
          }
        """
  };
}
