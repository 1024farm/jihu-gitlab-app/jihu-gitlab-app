import 'package:flutter/material.dart';

class StarsProvider extends ChangeNotifier {
  static StarsProvider _instance = StarsProvider._internal();

  StarsProvider._internal();

  void changeStarred() {
    notifyListeners();
  }

  factory StarsProvider() => _instance;

  @visibleForTesting
  void fullReset() {
    _instance = StarsProvider._internal();
  }
}
