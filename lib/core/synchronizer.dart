import 'package:jihu_gitlab_app/core/connection_provider/connection.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/core/net/response.dart';

typedef HasNextPage = bool Function();

typedef DataProcessor<T> = Future<bool> Function(T data, int page, int pageSize);

class Synchronizer<T> {
  int _page = 1;
  late int _pageSize;
  bool _hasNextPage = false;
  final String url;
  final DataProcessor<T> dataProcessor;
  Connection? connection;

  Synchronizer({required this.url, required this.dataProcessor, int pageSize = 50, this.connection}) {
    _pageSize = pageSize;
  }

  Future<bool> run() async {
    try {
      do {
        var response = await call();
        _hasNextPage = await dataProcessor.call(response.body(), _page, _pageSize);
        _page++;
      } while (_hasNextPage);
      return true;
    } catch (e) {
      LogHelper.err('Synchronizer run error on $url}', e);
      return false;
    }
  }

  Future<Response<dynamic>> call() async =>
      connection == null ? await HttpClient.instance().get<T>(contactPagingArguments()) : await HttpClient.instance().getWithConnection<T>(contactPagingArguments(), connection!);

  String contactPagingArguments() {
    if (url.contains("?")) {
      return '$url&page=$_page&per_page=$_pageSize';
    }
    return '$url?page=$_page&per_page=$_pageSize';
  }
}
