import 'package:package_info_plus/package_info_plus.dart';

class PackageDeviceInfo {
  static PackageInfo? packageInfo;

  static Future<void> loadInfo() async {
    packageInfo = await PackageInfo.fromPlatform();
  }

  static String? get versionNumber => packageInfo?.version;

  static String? get buildNumber => packageInfo?.buildNumber;
}
