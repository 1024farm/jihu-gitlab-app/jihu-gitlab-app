class RestPageInfo {
  final int _pageSize;
  int _currentPage = 1;

  RestPageInfo._(this._currentPage, this._pageSize);

  factory RestPageInfo.init({int pageSize = 20}) {
    return RestPageInfo._(1, pageSize);
  }

  void reset() {
    _currentPage = 1;
  }

  int get currentPage => _currentPage;

  int get pageSize => _pageSize;

  int get next => _currentPage + 1;

  void nextPage() {
    _currentPage++;
  }
}
