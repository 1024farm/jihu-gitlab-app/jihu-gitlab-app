class GraphqlParam {
  final String _param;

  GraphqlParam._(this._param);

  String get() => _param;

  factory GraphqlParam(String param) {
    return GraphqlParam._(param.replaceAll('\\', '\\\\').replaceAll('"', '\\"'));
  }
}
