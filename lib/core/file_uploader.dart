import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:image_picker/image_picker.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/net/api.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:path/path.dart';

class FileUploader {
  static FileUploader _instance = FileUploader._internal();

  FileUploader._internal();

  factory FileUploader.instance() => _instance;

  Future<FileUploadResult> upload({required int projectId, required XFile file}) async {
    try {
      String ext = extension(file.name);
      var fileType = FileType.match(ext);
      var body = FormData.fromMap({'file': await MultipartFile.fromFile(file.path, filename: "${fileType.label}$ext")});
      var response = await HttpClient.instance().post<Map<String, dynamic>>(Api.join('/projects/$projectId/uploads'), body);
      var resp = response.body();
      LogHelper.info('Upload file resp: $resp');
      return FileUploadResult.success(UploadedFileInfo.fromJson(resp, fileType));
    } catch (e) {
      LogHelper.err('Upload file failed with error', e);
      return FileUploadResult.fail(e);
    }
  }

  @visibleForTesting
  void injectInstanceForTesting(FileUploader mock) {
    _instance = mock;
  }
}

class UploadedFileInfo {
  final String alt;
  final String url;
  final String fullPath;
  final String markdown;

  UploadedFileInfo(this.alt, this.url, this.fullPath, this.markdown);

  factory UploadedFileInfo.fromJson(Map<String, dynamic> json, FileType fileType) {
    String path = json['full_path'] ?? '';
    String markdown = json['markdown'] ?? '';
    if (path.isNotEmpty) {
      String url = ConnectionProvider.currentBaseUrl.get + path;
      markdown = "![${fileType.label}]($url)";
    }
    LogHelper.info('UploadedFileInfo markdown: $markdown');
    return UploadedFileInfo(json['alt'] ?? 0, json['url'] ?? '', json['full_path'] ?? '', markdown);
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{"alt": alt, "url": url, "full_path": fullPath, "markdown": markdown};
  }
}

class FileUploadResult {
  UploadedFileInfo? uploadedFileInfo;
  Object? error;

  FileUploadResult._internal({this.uploadedFileInfo, this.error});

  factory FileUploadResult.success(UploadedFileInfo uploadedFileInfo) {
    return FileUploadResult._internal(uploadedFileInfo: uploadedFileInfo, error: null);
  }

  factory FileUploadResult.fail(Object error) {
    return FileUploadResult._internal(uploadedFileInfo: null, error: error);
  }

  bool get success => error == null;
}

enum FileType {
  image("image", <String>[".jpg", ".png", ".jpeg", ".gif", ".bmp", ".svg"]),
  video("video", <String>[".mp4", '.mov', ".flv"]),
  unknown("file", <String>[]);

  const FileType(this.label, this.extensions);

  final String label;
  final List<String> extensions;

  static FileType match(String extensions) {
    extensions = extensions.toLowerCase();
    if (!extensions.startsWith(".")) {
      extensions = ".$extensions";
    }
    var matches = FileType.values.where((element) => element.extensions.contains(extensions));
    return matches.isNotEmpty ? matches.first : FileType.unknown;
  }
}
