class ProjectIdentity {
  final int _id;
  final String _fullPath;

  ProjectIdentity(this._id, this._fullPath);

  String get fullPath => _fullPath;

  int get id => _id;
}
