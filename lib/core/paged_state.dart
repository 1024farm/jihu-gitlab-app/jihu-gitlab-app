import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/layout/adaptive.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/widgets/http_fail_view.dart';
import 'package:jihu_gitlab_app/core/widgets/unauthorized_view.dart';
import 'package:provider/provider.dart';

abstract class PagedState<T extends StatefulWidget> extends State<T> {
  final EasyRefreshController _refreshController = EasyRefreshController(controlFinishRefresh: true, controlFinishLoad: true);

  bool _inPageRefreshing = false;

  @override
  void initState() {
    super.initState();
    onRefresh();
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return buildBody();
  }

  Widget buildBody() {
    return Scaffold(
        appBar: buildAppBar(),
        body: Consumer<ConnectionProvider>(builder: (context, _, child) {
          if (!ConnectionProvider.authorized) {
            return const UnauthorizedView();
          }
          if (isErrorState()()) {
            return HttpFailView(onRefresh: () => onRefresh());
          }
          return buildRefreshView();
        }));
  }

  Widget buildRefreshView() {
    return SafeArea(
      top: !isDesktopLayout(context),
      bottom: false,
      child: EasyRefresh(controller: _refreshController, header: const CupertinoHeader(), footer: const CupertinoFooter(), onRefresh: onRefresh, onLoad: loadMore, child: buildListView()),
    );
  }

  Future<void> onRefresh() async {
    if (_inPageRefreshing) return;
    _inPageRefreshing = true;
    bool success = await modelRefresh()();
    setState(() {});
    if (success) {
      _refreshController.finishRefresh();
      _refreshController.resetFooter();
    } else {
      _refreshController.finishRefresh(IndicatorResult.fail);
    }
    _inPageRefreshing = false;
  }

  void loadMore() async {
    bool success = await modelLoadMore()();
    setState(() {});
    if (success) {
      _refreshController.finishLoad(hasNextPage()() ? IndicatorResult.success : IndicatorResult.noMore);
      // coverage:ignore-start
    } else {
      _refreshController.finishLoad(IndicatorResult.fail);
    }
    // coverage:ignore-end
  }

  Widget? buildListView();

  AppBar? buildAppBar();

  Future<bool> Function() modelRefresh();

  Function isErrorState();

  Future<bool> Function() modelLoadMore();

  Function hasNextPage();
}
