import 'package:flutter/material.dart';
import 'package:flutter_native_text_input/flutter_native_text_input.dart';
import 'package:jihu_gitlab_app/core/themes/theme_color_data.dart';

class NativeTextField extends StatefulWidget {
  final int maxLines;
  final int minLines;
  final String? placeHolder;
  final TextStyle? placeHolderStyle;
  final bool autoCorrect;
  final TextEditingController? controller;
  final TextContentType? textContentType;
  final TextCapitalization textCapitalization;
  final double minHeightPadding;
  final double paddingLeft;
  final double paddingRight;
  final BoxDecoration? decoration;
  final TextStyle style;
  final Color cursorColor;
  final ValueChanged<String>? onChanged;
  final ValueChanged<String?>? onSubmitted;
  final FocusNode? focusNode;
  final KeyboardType keyboardType;

  const NativeTextField({
    super.key,
    this.maxLines = 1,
    this.minLines = 1,
    this.controller,
    this.placeHolder,
    this.placeHolderStyle,
    this.textContentType,
    this.textCapitalization = TextCapitalization.sentences,
    this.autoCorrect = true,
    this.minHeightPadding = 18,
    this.paddingLeft = 0,
    this.paddingRight = 0,
    this.decoration,
    this.style = const TextStyle(fontSize: 14),
    this.cursorColor = AppThemeData.primaryColor,
    this.onChanged,
    this.onSubmitted,
    this.focusNode,
    this.keyboardType = KeyboardType.defaultType,
  });

  @override
  State<NativeTextField> createState() => _NativeTextFieldState();
}

class _NativeTextFieldState extends State<NativeTextField> {
  @override
  Widget build(BuildContext context) {
    return NativeTextInput(
      decoration: widget.decoration,
      controller: widget.controller,
      minHeightPadding: widget.minHeightPadding,
      paddingLeft: widget.paddingLeft,
      paddingRight: widget.paddingRight,
      textCapitalization: widget.textCapitalization,
      textContentType: widget.textContentType,
      placeholder: widget.placeHolder,
      placeholderColor: widget.placeHolderStyle?.color,
      maxLines: widget.maxLines,
      minLines: widget.minLines,
      returnKeyType: ReturnKeyType.defaultAction,
      style: widget.style,
      iosOptions: IosOptions(
        autocorrect: widget.autoCorrect,
        cursorColor: widget.cursorColor,
        keyboardAppearance: Brightness.light,
        placeholderStyle: widget.placeHolderStyle,
      ),
      keyboardType: widget.keyboardType,
      onChanged: widget.onChanged,
      onSubmitted: widget.onSubmitted,
      focusNode: widget.focusNode,
    );
  }
}
