import 'package:jihu_gitlab_app/core/connection_provider/connection.dart';
import 'package:jihu_gitlab_app/core/domain/token.dart';

class CommunityConnection extends Connection {
  CommunityConnection._(super.userInfo, super.baseUrl, {required super.active});

  factory CommunityConnection.init() {
    return CommunityConnection._(User(0, '', '', '', '', '', ''), BaseUrl('https://jihulab.com'), active: false);
  }

  factory CommunityConnection.fromJson(Map<String, dynamic> json) {
    var connection = CommunityConnection._(User.fromJson(json['info']), BaseUrl(json['base_url']), active: json['active']);
    connection.token = json['token'] == null ? null : Token.fromJson(json['token']);
    connection.personalAccessToken = json['personal_access_token'];
    connection.isSelfManaged = json['is_self_managed'];
    return connection;
  }

  void reset(Connection connection) {
    userInfo = connection.userInfo;
    token = connection.token;
    baseUrl = connection.baseUrl;
    isSelfManaged = connection.isSelfManaged;
  }

  void clear() {
    userInfo = User(0, '', '', '', '', '', '');
    token = null;
    baseUrl = BaseUrl('https://jihulab.com');
    isSelfManaged = false;
  }
}
