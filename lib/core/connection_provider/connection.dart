import 'dart:io';

import 'package:jihu_gitlab_app/core/connection_provider/connection_provider.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connections.dart';
import 'package:jihu_gitlab_app/core/domain/token.dart';
import 'package:jihu_gitlab_app/core/id.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/auth/login_model.dart';

enum ConnectionType {
  selfManaged,
  jiHu,
  gitLab,
  unknown;

  String get displayName {
    if (this == selfManaged) return AppLocalizations.dictionary().selfManaged;
    if (this == gitLab) return 'gitlab.com';
    if (this == jiHu) return 'jihulab.com';
    return '';
  }
}

class Connection {
  User userInfo;
  Token? token;
  bool active;
  String? personalAccessToken;
  BaseUrl baseUrl;
  bool isSelfManaged = false;

  Connection(this.userInfo, this.baseUrl, {required this.active});

  factory Connection.fromJson(Map<String, dynamic> json) {
    var connection = Connection(User.fromJson(json['info']), BaseUrl(json['base_url']), active: json['active']);
    connection.token = json['token'] == null ? null : Token.fromJson(json['token']);
    connection.personalAccessToken = json['personal_access_token'];
    connection.isSelfManaged = json['is_self_managed'];
    return connection;
  }

  bool get shouldRefreshToken => token?.expired ?? false;

  bool get canRefreshToken => token?.refreshToken.isNotEmpty ?? false;

  static const _path = "/oauth/token";
  static const _grantTypeRefresh = "refresh_token";
  static const _redirectUri = "jihu-gitlab://authorization/callback";

  Future<bool> refreshToken() async {
    if (Connections.onAuthTokenRequest) {
      LogHelper.info("Already refreshed, interrupted.");
      return false;
    }
    LogHelper.info("Start refreshing...");
    Connections.onAuthTokenRequest = true;
    try {
      LoginClientIdSet clientId = baseUrl.toLoginClientId;
      Map<String, dynamic> data = {"client_id": clientId.id, "grant_type": _grantTypeRefresh, "redirect_uri": _redirectUri, "refresh_token": token!.refreshToken};
      token = await _doRequest(data, baseUrl.get);
      if (serverType == 'jihulab.com') ConnectionProvider.notifyTokenChanged(token!);
      return Future.value(true);
    } catch (e) {
      LogHelper.err('RefreshToken error', e);
      return Future.value(false);
    } finally {
      Connections.onAuthTokenRequest = false;
      LogHelper.info("Refresh completed.");
      ConnectionProvider().notifyChanged();
    }
  }

  Future<Token> _doRequest(Map<String, dynamic> data, String baseUrl) async {
    var response = await HttpClient.instance().postWithConnection<Map<String, dynamic>>('$baseUrl$_path', data, null);
    return Token.fromJson(response.body());
  }

  Map<String, String> get authHeaders {
    if (personalAccessToken != null) return {"PRIVATE-TOKEN": personalAccessToken!};
    if (token != null) return {HttpHeaders.authorizationHeader: 'Bearer ${token!.accessToken}'};
    return {};
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      "info": userInfo.toJson(),
      "token": token?.toJson(),
      "active": active,
      "personal_access_token": personalAccessToken,
      "base_url": baseUrl.get,
      "is_self_managed": isSelfManaged,
    };
  }

  ConnectionType get type {
    if (personalAccessToken != null) return ConnectionType.selfManaged;
    if (baseUrl.isGitLabUrl()) return ConnectionType.gitLab;
    if (baseUrl.isJiHuUrl()) return ConnectionType.jiHu;
    return ConnectionType.unknown;
  }

  String get serverType {
    return type.displayName;
  }

  bool get authorized => token != null || (isSelfManaged && personalAccessToken != null);
}

class User {
  int id;
  String username;
  String name;
  String state;
  String avatarUrl;
  String webUrl;
  String publicEmail;

  User(this.id, this.username, this.name, this.state, this.avatarUrl, this.webUrl, this.publicEmail);

  factory User.fromJson(Map<String, dynamic> json) {
    return User(json['id'], json['username'], json['name'], json['state'], json['avatar_url'] ?? '', json['web_url'] ?? '', json['public_email'] ?? '');
  }

  factory User.fromGraphQLJson(Map<String, dynamic> json) {
    return User(
      Id.fromGid(json['id'] ?? '/0').id,
      json['username'] ?? '',
      json['name'] ?? '',
      json['state'] ?? '',
      json['avatarUrl'] ?? '',
      json['webUrl'] ?? '',
      json['publicEmail'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{"id": id, "username": username, "name": name, "state": state, "avatar_url": avatarUrl, "web_url": webUrl, "public_email": publicEmail};
  }

  @override
  bool operator ==(Object other) {
    return other is User && id == other.id;
  }

  @override
  int get hashCode => id;
}

class BaseUrl {
  final String _url;

  BaseUrl(this._url);

  LoginClientIdSet get toLoginClientId => get.contains(LoginClientIdSet.jihulab.name) ? LoginClientIdSet.jihulab : LoginClientIdSet.gitlab;

  bool isFree() {
    return isJiHuUrl() || isGitLabUrl();
  }

  bool isGitLabUrl() => _url == 'https://gitlab.com';

  bool isJiHuUrl() => _url == 'https://jihulab.com';

  String get get => _url;
}
