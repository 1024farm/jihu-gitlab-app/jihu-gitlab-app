import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jihu_gitlab_app/core/connection_provider/community_connection.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connections.dart';
import 'package:jihu_gitlab_app/core/connection_provider/login_type_same_exception.dart';
import 'package:jihu_gitlab_app/core/domain/token.dart';
import 'package:jihu_gitlab_app/core/global_keys.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/log_helper.dart';
import 'package:jihu_gitlab_app/core/net/http_client.dart';

class ConnectionProvider extends ChangeNotifier {
  static ConnectionProvider _instance = ConnectionProvider._internal();
  static const String _persistenceLoggedOutKey = "logged-out-by-user-key";
  static final Connections _connections = Connections();

  static bool _isLoggedOutByUser = false;

  ConnectionProvider._internal();

  factory ConnectionProvider() => _instance;

  static String get accessToken => connection?.token?.accessToken ?? '';

  static String get refreshToken => connection?.token?.refreshToken ?? '';

  static bool get canRefreshToken => connection?.token?.refreshToken.isNotEmpty ?? false;

  static bool get shouldRefreshToken => connection?.token?.expired ?? false;

  static bool get onAuthTokenRequest => Connections.onAuthTokenRequest;

  static Future<String> get mergeRequestWebUrl async => await LocalStorage.get(LocalStorageKeys.currentMergeRequestWebUrlKey, '');

  CommunityConnection get communityConnection {
    return _connections.communityConnection;
  }

  bool get isConnectedToSpecifiedHost => _connections.isConnectedToSpecifiedHost;

  Connection? get connectionOfSpecifiedHost => _connections.connectionOfSpecifiedHost;

  Future<Map<String, dynamic>> addUserByToken(Token token, String baseUrl) async {
    try {
      User user = await _fetchUserByToken(baseUrl, token);
      var connection = Connection(user, BaseUrl(baseUrl), active: true);
      connection.token = token;
      _connections.add(connection);
      notifyListeners();
      return Future.value({"success": true, "message": ""});
    } on LoginTypeSameException {
      notifyListeners();
      rethrow;
    } catch (e) {
      LogHelper.err('ConnectionProvider refresh error', e);
      notifyListeners();
      return Future.value({"success": false, "message": "$e"});
    }
  }

  static List<Connection> get connections => _connections.get;

  Future<Map<String, dynamic>> addUserByPersonalToken(String personalToken, String baseUrl) async {
    try {
      User user = await _fetchUserByPersonalToken(baseUrl, personalToken);
      var connection = Connection(user, BaseUrl(baseUrl), active: true);
      connection.personalAccessToken = personalToken;
      connection.isSelfManaged = true;
      _connections.add(connection);
      notifyListeners();
      return Future.value({"success": true, "message": ""});
    } on LoginTypeSameException {
      notifyListeners();
      rethrow;
    } catch (e) {
      LogHelper.err('UserProvider refresh error', e);
      notifyListeners();
      return Future.value({"success": false, "message": "$e"});
    }
  }

  Future<User> _fetchUserByToken(String baseUrl, Token token) async {
    var response = await HttpClient.instance().getWithHeader<Map<String, dynamic>>('$baseUrl/api/v4/user', {HttpHeaders.authorizationHeader: 'Bearer ${token.accessToken}'});
    var resp = response.body();
    return User.fromJson(resp);
  }

  Future<User> _fetchUserByPersonalToken(String baseUrl, String personalToken) async {
    var response = await HttpClient.instance().getWithHeader<Map<String, dynamic>>('$baseUrl/api/v4/user', {"PRIVATE-TOKEN": personalToken});
    var resp = response.body();
    return User.fromJson(resp);
  }

  void resetCurrentToken(Token token) {
    _connections.resetCurrentToken(token);
    ConnectionProvider().notifyChanged();
  }

  Future<Map<String, dynamic>> refresh() async {
    try {
      for (var connection in _connections.get) {
        try {
          if (connection.isSelfManaged) {
            connection.userInfo = await _fetchUserByPersonalToken(connection.baseUrl.get, connection.personalAccessToken!);
          } else {
            connection.userInfo = await _fetchUserByToken(connection.baseUrl.get, connection.token!);
          }
        } catch (e) {
          debugPrint('Refresh error: $e');
        }
      }
      _connections.save();
      notifyListeners();
      return Future.value({"success": true, "message": ""});
    } catch (error) {
      LogHelper.err('UserProvider refresh error', error);
      return Future.value({"success": false, "message": "$error"});
    }
  }

  Future<Map<String, dynamic>> initSelfManagedConfigs({required String host, required String token, isHttps = true}) async {
    String baseUrl = isHttps ? "https://$host" : "http://$host";
    return await addUserByPersonalToken(token, baseUrl);
  }

  static void notifyTokenChanged(Token token) {
    _connections.notifyTokenChanged(token);
  }

  Future<void> restore() async {
    await _connections.restore();
    bool byUser = await LocalStorage.get(_persistenceLoggedOutKey, false);
    _isLoggedOutByUser = byUser;
    notifyListeners();
  }

  Future<void> clearActive({bool notify = true}) async {
    _connections.clearActive();
    if (notify) {
      notifyListeners();
    }
  }

  void loggedOutSelectedConnection(Connection connection) {
    _connections.loggedOutSelected(connection);
    loggedOut(byUser: true);
    notifyChanged();
  }

  void changeAccount(Connection connection) {
    _connections.changeAccount(connection);
    notifyChanged();
  }

  void notifyChanged() {
    _connections.save();
    notifyListeners();
  }

  void loggedOut({required bool byUser}) {
    _isLoggedOutByUser = byUser;
    LocalStorage.save(_persistenceLoggedOutKey, byUser);
  }

  @visibleForTesting
  void reset(Connection connection) {
    _connections.clearActive();
    _connections.add(connection);
    notifyListeners();
  }

  @visibleForTesting
  void injectConnectionForTest(Connection connection) {
    _connections.add(connection);
    changeAccount(connection);
    notifyListeners();
  }

  @visibleForTesting
  void fullReset() {
    clearActive(notify: false);
    _instance = ConnectionProvider._internal();
  }

  static bool _isAuthorized() {
    if (ConnectionProvider.isSelfManagedGitLab && ConnectionProvider.connection?.userInfo.id != null) {
      return true;
    } else {
      return ConnectionProvider.accessToken.isNotEmpty && ConnectionProvider.connection?.userInfo.id != null;
    }
  }

  bool isUsed(String item) {
    return _connections.isUsed(item);
  }

  static BaseUrl get currentBaseUrl => connection?.baseUrl ?? BaseUrl('');

  static bool get authorized => _isAuthorized();

  static Connection? get connection => _connections.activeConnection;

  static bool get isSelfManagedGitLab => connection?.isSelfManaged ?? false;

  static bool get isSaaSLogin => !(connection?.isSelfManaged ?? false) && authorized;

  static String? get personalAccessToken => connection?.personalAccessToken;

  static int? get connectionId => connection?.userInfo.id;

  static bool get isLoggedOutByUser => _isLoggedOutByUser;
}
