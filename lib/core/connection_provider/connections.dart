import 'dart:convert';

import 'package:jihu_gitlab_app/core/connection_provider/community_connection.dart';
import 'package:jihu_gitlab_app/core/connection_provider/connection.dart';
import 'package:jihu_gitlab_app/core/connection_provider/login_type_same_exception.dart';
import 'package:jihu_gitlab_app/core/domain/token.dart';
import 'package:jihu_gitlab_app/core/iterable_extension.dart';
import 'package:jihu_gitlab_app/core/local_storage.dart';
import 'package:jihu_gitlab_app/core/project_provider.dart';
import 'package:jihu_gitlab_app/l10n/app_localizations.dart';
import 'package:jihu_gitlab_app/modules/auth/login_page.dart';
import 'package:jihu_gitlab_app/modules/auth/self_managed_login_page.dart';

enum LoginMenuSet { selfManaged, jihulab, gitlab }

extension LoginMenuSetExtension on LoginMenuSet {
  String get name => () {
        switch (this) {
          case LoginMenuSet.selfManaged:
            return 'Self-Managed';
          case LoginMenuSet.jihulab:
            return 'jihulab.com';
          case LoginMenuSet.gitlab:
            return 'gitlab.com';
        }
      }();
}

class LoginConfig {
  String Function() titleFunction;
  String host;
  String name;
  String pageRouteName;
  Type pageType;

  LoginConfig({required this.titleFunction, required this.host, required this.name, required this.pageRouteName, required this.pageType});

  String get displayTitle {
    return titleFunction();
  }
}

Map<String, LoginConfig> loginMenuList = {
  LoginMenuSet.selfManaged.name: LoginConfig(
      titleFunction: () => AppLocalizations.dictionary().selfManaged, host: 'Self-Managed', name: 'Self-Managed', pageRouteName: SelfManagedLoginPage.routeName, pageType: SelfManagedLoginPage),
  LoginMenuSet.jihulab.name: LoginConfig(titleFunction: () => 'jihulab.com', host: 'jihulab.com', name: 'jihulab.com', pageRouteName: LoginPage.routeName, pageType: LoginPage),
  LoginMenuSet.gitlab.name: LoginConfig(titleFunction: () => 'gitlab.com', host: 'gitlab.com', name: 'gitlab.com', pageRouteName: LoginPage.routeName, pageType: LoginPage),
};

class Connections {
  static const String _userKey = 'app-user';
  static const String _communityConnectionKey = 'community-user';
  static bool onAuthTokenRequest = false;

  List<Connection> _connections = [];
  CommunityConnection _communityConnection = CommunityConnection.init();

  List<Connection> get get => _connections;

  CommunityConnection get communityConnection {
    return _communityConnection;
  }

  bool get isConnectedToSpecifiedHost {
    var host = ProjectProvider().specifiedHost ?? '';
    if (existJihu() && host == 'https://jihulab.com') return true;
    return existGitlab() && host == 'https://gitlab.com';
  }

  Connection? get connectionOfSpecifiedHost {
    var host = ProjectProvider().specifiedHost ?? '';
    if (existJihu() && host == 'https://jihulab.com') return jihuConnection;
    if (existGitlab() && host == 'https://gitlab.com') return gitlabConnection;
    return null;
  }

  Connection? get jihuConnection => _connections.where((element) => element.baseUrl.isJiHuUrl() && element.personalAccessToken == null).toList().firstNullable;

  Connection? get gitlabConnection => _connections.where((element) => element.baseUrl.isGitLabUrl() && element.personalAccessToken == null).toList().firstNullable;

  void add(Connection connection) {
    if (existSelfManaged() && connection.personalAccessToken != null) throw LoginTypeSameException();
    if (connection.baseUrl.isJiHuUrl() && connection.personalAccessToken == null && existJihu()) throw LoginTypeSameException();
    if (connection.baseUrl.isGitLabUrl() && connection.personalAccessToken == null && existGitlab()) throw LoginTypeSameException();
    for (var user in _connections) {
      user.active = false;
    }
    _connections.add(connection);
    if (connection.serverType == 'jihulab.com') {
      _communityConnection.reset(connection);
    }
    save();
  }

  void notifyTokenChanged(Token token) async {
    _communityConnection.token = token;
    _connections.where((o) => o.baseUrl.isJiHuUrl() && o.personalAccessToken == null).toList().first.token = token;
  }

  bool isUsed(String item) {
    if (item == LoginMenuSet.selfManaged.name) return existSelfManaged();
    if (item == LoginMenuSet.jihulab.name) return existJihu();
    if (item == LoginMenuSet.gitlab.name) return existGitlab();
    return true;
  }

  bool existGitlab() => _connections.any((o) => o.baseUrl.isGitLabUrl() && o.personalAccessToken == null);

  bool existJihu() => _connections.any((o) => o.baseUrl.isJiHuUrl() && o.personalAccessToken == null);

  bool existSelfManaged() => _connections.any((o) => o.personalAccessToken != null);

  void resetCurrentToken(Token token) {
    if (activeConnection == null) return;
    activeConnection!.token = token;
    save();
  }

  void clearActive() {
    _connections.removeWhere((o) => o.active == true);
    if (!existJihu()) _communityConnection.clear();
    if (_connections.isEmpty) {
      save();
      return;
    }
    _connections.last.active = true;
    save();
  }

  Connection? get activeConnection {
    if (_connections.where((o) => o.active == true).isEmpty) return null;
    return _connections.firstWhere((o) => o.active == true);
  }

  List<String> _toStoredConnections() {
    return _connections.map((e) => jsonEncode(e)).toList();
  }

  void save() {
    LocalStorage.save(_userKey, _toStoredConnections());
    LocalStorage.save(_communityConnectionKey, jsonEncode(_communityConnection));
  }

  Future<void> restore() async {
    List<String> jsonList = await LocalStorage.get<List<String>>(_userKey, []);
    _connections = jsonList.map((e) => jsonDecode(e)).map((e) => Connection.fromJson(e)).toList();
    var communityConnection = await LocalStorage.get<String>(_communityConnectionKey, "");
    if (communityConnection == "") {
      _communityConnection = CommunityConnection.init();
    } else {
      _communityConnection = CommunityConnection.fromJson(jsonDecode(communityConnection));
    }
  }

  void changeAccount(Connection connection) {
    var index = _connections.indexOf(connection);
    for (var c in _connections) {
      c.active = false;
    }
    _connections[index].active = true;
    save();
  }

  void loggedOutSelected(Connection connection) {
    _connections.remove(connection);
    if (!existJihu()) _communityConnection.clear();
    if (_connections.any((o) => o.active == true) || _connections.isEmpty) {
      save();
      return;
    }
    _connections.last.active = true;
    save();
  }
}
