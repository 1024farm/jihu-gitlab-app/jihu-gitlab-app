import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart' as services;
import 'package:flutter/services.dart';

class Clipboard {
  static Clipboard _clipboard = Clipboard._();

  Clipboard._();

  factory Clipboard() {
    return _clipboard;
  }

  Future<String> fromClipboard() async {
    services.ClipboardData? clipboardData = await services.Clipboard.getData(services.Clipboard.kTextPlain);
    return clipboardData?.text ?? '';
  }

  Future<void> setData(String text) {
    return services.Clipboard.setData(ClipboardData(text: text));
  }

  @visibleForTesting
  static void injectForTest(Clipboard value) {
    _clipboard = value;
  }
}
