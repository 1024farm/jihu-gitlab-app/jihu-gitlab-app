import 'dart:io';

class AppPlatform {
  static bool get isAndroid => Platform.isAndroid || isInTest;

  static bool get isIOS => Platform.isIOS || isInTest;

  static bool get isInTest => Platform.environment.containsKey("FLUTTER_TEST") && Platform.environment["FLUTTER_TEST"] == "true";
}
