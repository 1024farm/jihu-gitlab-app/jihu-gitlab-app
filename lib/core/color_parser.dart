import 'dart:ui';

class ColorParser {
  String _hex;

  ColorParser._(this._hex);

  factory ColorParser(String input) {
    return ColorParser._(input);
  }

  Color? parse() {
    _hex = _hex.replaceFirst('#', '');
    if (_hex.length != 3 && _hex.length != 6) return null;
    if (_hex.length == 3) return Color(int.parse('ff${_hex.split('').map((String e) => e + e).join()}', radix: 16));
    return Color(int.parse('ff$_hex', radix: 16));
  }
}
