#!/bin/sh

#  ci_pre_xcodebuild.sh
#  Runner
#
#  Created by raniys on 11/19/22.
#
echo "/********** Start ci_pre_xcodebuild.sh **********/"

flutter build ipa --dart-define=OCP_APIM_SUBSCURIPTION_KEY=$OCP_APIM_SUBSCURIPTION_KEY
exit 0

echo "/********** End ci_pre_xcodebuild.sh **********/"
