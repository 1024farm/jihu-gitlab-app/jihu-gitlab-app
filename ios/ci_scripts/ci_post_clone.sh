#!/bin/sh

#  ci_post_clone.sh
#  Runner
#
#  Created by raniys on 11/19/22.
#

echo "/********** Start ci_post_clone.sh **********/"

# The default execution directory of this script is the ci_scripts directory.
cd $CI_WORKSPACE # change working directory to the root of your cloned repo.

# Install Flutter using git.
git clone https://github.com/flutter/flutter.git --depth 1 -b 3.7.10 $HOME/flutter
export PATH="$PATH:$HOME/flutter/bin"

# Check flutter version
flutter --version

# Install Flutter artifacts for iOS (--ios), or macOS (--macos) platforms.
flutter precache --ios

# Install Flutter dependencies.
flutter pub get
flutter pub global activate intl_utils 2.8.1
flutter --no-color pub global run intl_utils:generate
flutter pub run build_runner build --delete-conflicting-outputs

# Install CocoaPods using Homebrew.
HOMEBREW_NO_AUTO_UPDATE=1 # disable homebrew's automatic updates.
brew install cocoapods
pod --version

# Install CocoaPods dependencies.
# cd ios && pod install # run `pod install` in the `ios` directory.

flutter build ipa --dart-define=OCP_APIM_SUBSCURIPTION_KEY=$OCP_APIM_SUBSCURIPTION_KEY

exit 0
echo "/********** End ci_post_clone.sh **********/"
