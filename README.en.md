# JiHu GitLab App

[![pipeline status](https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/badges/main/pipeline.svg)](https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/commits/main)
[![coverage report](https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/badges/main/coverage.svg)](https://jihulab.com/ultimate-plan/jihu-gitlab-app/jihu-gitlab-app/-/commits/main)

This repository contains all the materials you need to quickly build a new development environment

## Environment preparation

### Development Platform

#### Flutter installation

This project uses flutter and dart development. Please install flutter and dart first. And dart is
already embedded in flutter. And flutter version is required in [pubspec.yaml](./pubspec.yaml).

- Flutter for Windows:

    1. https://docs.flutter.dev/get-started/install/windows
- Flutter for Mac:

    1. https://docs.flutter.dev/get-started/install/macos
    2. ~~~shell
       brew install flutter
       ~~~

- Flutter for Linux:

    1. https://docs.flutter.dev/get-started/install/linux

#### Run flutter doctor

Run the following command to see if there are any dependencies you need to install to complete the
setup (for verbose output, add the -v flag):

~~~shell
flutter doctor
~~~

This command checks your environment and displays a report to the terminal window. The Dart SDK is
bundled with Flutter; it is not necessary to install Dart separately. Check the output carefully for
other software you might need to install or further tasks to perform (shown in **bold** text).

For example:

~~~shell
[-] Android toolchain - develop for Android devices
• Android SDK at /Users/obiwan/Library/Android/sdk
✗ Android SDK is missing command line tools; download from https://goo.gl/XxQghQ
• Try re-installing or updating your Android SDK,
visit https://docs.flutter.dev/setup/#android-setup for detailed instructions.
~~~

Then you can follow this report to fix your environment.

#### iOS Setup

##### Install Xcode

To develop Flutter apps for iOS, you need a Mac with Xcode installed.

1. Install the latest stable version of Xcode (using web download or the Mac App Store).
2. Configure the Xcode command-line tools to use the newly-installed version of Xcode by running the
   following from the command line:

~~~shell
sudo xcode-select --switch /Applications/Xcode.app/Contents/Developer
sudo xcodebuild -runFirstLaunch
~~~

This is the correct path for most cases, when you want to use the latest version of Xcode. If you
need to use a different version, specify that path instead.

Make sure the Xcode license agreement is signed by either opening Xcode once and confirming or
running sudo xcodebuild -license from the command line. Versions older than the latest stable
version may still work, but are not recommended for Flutter development.

With Xcode, you’ll be able to run Flutter apps on an iOS device or on the simulator.

Then you can use this command to start an iOS simulator.

~~~shell
open -a Simulator
~~~

### Development Tools

For a better development experience and more efficient development, please use IntelliJ IDEA or
Android Studio as the default development tool: IntelliJ IDEA:

- IntelliJ IDEA: https://www.jetbrains.com/idea/
- Android Studio: https://developer.android.com/studio

### Scripts

#### Prepare flutter environment

```shell script
flutter pub get
```

#### Prepare check coverage environment

- lcov for Mac:

```shell script
brew install lcov
```

- lcov for Windows or linux:

```shell script
https://github.com/linux-test-project/lcov
```

#### Run all tests and generate coverage report

```shell
./scripts/check-all
```

#### Run to generate Mockito mocks

```shell
flutter pub run build_runner build --delete-conflicting-outputs
```