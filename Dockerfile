FROM ghcr.io/cirruslabs/flutter:3.7.12

RUN sdkmanager "platforms;android-31"
RUN sdkmanager "platforms;android-33"
RUN sdkmanager "platforms;android-34"
RUN sdkmanager "platform-tools" "build-tools;30.0.3"
RUN flutter precache --android
RUN rm -rf /root/.pub-cache

WORKDIR /root/code

COPY . .
RUN sh scripts/install.sh && \
  flutter build apk -v --debug && \
  rm -rf /root/code /root/.pub-cache /root/.gradle/.tmp
